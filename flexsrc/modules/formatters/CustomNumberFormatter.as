package modules.formatters
{
	import modules.base.BasedMethod;
	import modules.sbase.SiteConst;
	
	import mx.formatters.NumberFormatter;
	
	//******************************************************************************
	// 数値フォーマッタ
	// date   = 09.11.22 SE-254
	// modify = xx.xx.xx SE-xxx
	//******************************************************************************
	public class CustomNumberFormatter
	{
		
		//******************************************************************************
		//name   = numberFormat
		//func   = 数値型format 
		//io     = i : num			: 数値項目
		//         i : comma		: カンマ表示フラグ("1":カンマを表示する)
		//         i : realNum		: 小数桁数(※省略時、ゼロ)
		//         i : mode			: モード    "0":表示、"1":編集(※省略時、"0")
		//         i : round		: 丸め      "0":四捨五入なし,"1":切捨て,"2":切上げ,"3":四捨五入
		//         					  (※省略時、初期値)
		//return = 文字列
		//date   = 09.12.03 SE-254
		//alter  = xx.xx.xx SE-xxx
		//******************************************************************************
		public function numberFormat(num: String, comma: String, realNum: String, mode: String, round: String=null): String
		{
			var w_realNum: String;
			var w_mode: String;
			var w_round: String;
			
			var nfmt: NumberFormatter = new NumberFormatter();

			// 変数セット
			nfmt.decimalSeparatorTo    = ".";
			nfmt.useNegativeSign       = "true";
			nfmt.useThousandsSeparator = "true";
			
			// (小数桁数)
			w_realNum = realNum;
			if (BasedMethod.nvl(w_realNum) == "") {
				w_realNum = "0";
			}
			
			// (モード)
			w_mode = mode;
			if (BasedMethod.nvl(w_mode) == "") {
				w_mode = "0";
			}
			
			// (丸め)
			w_round = round;
			if (BasedMethod.nvl(w_round) == "") {
				w_round = SiteConst.G_ROUND;
			}
					
			// カンマ表示
			nfmt.thousandsSeparatorTo  = "";
			
			// (表示モードでカンマありの時)
			if ((w_mode == "0") && (comma == "1")) {
				nfmt.thousandsSeparatorTo  = ",";
			}
			
			// 小数桁
			nfmt.precision = w_realNum;
			
			// 丸め
			switch (w_round) {
				
				// 四捨五入なし
				case "0":
					nfmt.rounding = "none";
					break;
						
						
				// 切捨て
				case "1":
					nfmt.rounding = "down";
					break;
					
				// 切上げ
				case "2":
					nfmt.rounding = "up";
					break;
					
				// 四捨五入
				case "3":
					nfmt.rounding = "nearest";
					break;
			} 
						
			// 値が空白ならそのまま返す
			if (BasedMethod.nvl(num) == "") {
				return num;
				
			// 空白でなければ、フォーマットをかけて返す
			} else {
				return nfmt.format(Number(num.replace(/,/g, "")));

			}
			
		}
	}
}