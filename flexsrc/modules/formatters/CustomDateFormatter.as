package modules.formatters
{
	import modules.sbase.SiteConst;
	
	import mx.formatters.DateFormatter;
	
	//******************************************************************************
	// 日付フォーマッタ
	// date   = 09.11.22 SE-254
	// modify = xx.xx.xx SE-xxx
	//******************************************************************************
	public class CustomDateFormatter
	{

		//******************************************************************************
		//name   = dateDisplayFormat
		//func   = 日付型format
		//io     = i : dat			: 日付項目
		//return = 文字列
		//date   = 09.11.22 SE-254
		//alter  = xx.xx.xx SE-xxx
		//******************************************************************************
		public function dateDisplayFormat(dat: Date): String {
			
			var dfmt: DateFormatter = new DateFormatter();
			
			dfmt.formatString = SiteConst.G_DATEFMT;
						
			return dfmt.format(dat);
		}

		//******************************************************************************
		//name   = dateDisplayFormat2
		//func   = 日付型format
		//io     = i : dat			: 日付項目
		//return = 
		//date   = 09.11.22 SE-254
		//alter  = xx.xx.xx SE-xxx
		//******************************************************************************
		public function dateDisplayFormat2(dat: Date): String {
			
			var dfmt: DateFormatter = new DateFormatter();
			
			dfmt.formatString = SiteConst.G_DATEFMT2;
						
			return dfmt.format(dat);
		}
		
		//******************************************************************************
		//name   = dateTimeDisplayFormat
		//func   = 日時型format
		//io     = i : dat			: 日付項目
		//return = 
		//date   = 09.11.22 SE-254
		//alter  = xx.xx.xx SE-xxx
		//******************************************************************************
		public function dateTimeDisplayFormat(dat:Date):String {
			
			var dfmt: DateFormatter = new DateFormatter();
			
			dfmt.formatString = SiteConst.G_DATETIMEFMT;
						
			return dfmt.format(dat);
		}

		//******************************************************************************
		//name   = dateTimeDisplayFormat2
		//func   = 日時型format
		//io     = i : dat			: 日付項目
		//return = 
		//date   = 09.11.22 SE-254
		//alter  = xx.xx.xx SE-xxx
		//******************************************************************************
		public function dateTimeDisplayFormat2(dat: Date): String {
			
			var dfmt: DateFormatter = new DateFormatter();
			
			dfmt.formatString = SiteConst.G_DATETIMEFMT2;
						
			return dfmt.format(dat);
		}

		//******************************************************************************
		//name   = dateTimeDisplayFormat3
		//func   = 日時型format
		//io     = i : dat			: 日付項目
		//return = 
		//date   = 11.04.06 SE-362
		//alter  = xx.xx.xx SE-xxx
		//******************************************************************************
		public function dateTimeDisplayFormat3(dat:Date):String {
			
			var dfmt: DateFormatter = new DateFormatter();
			
			dfmt.formatString = SiteConst.G_DATETIMEFMT3;
						
			return dfmt.format(dat);
		}

		//******************************************************************************
		//name   = dateTimeDisplayFormat_Java
		//func   = 日付型format ※Java引数用
		//io     = i : dat			: 日付項目
		//return = 
		//date   = 10.01.13 SE-254
		//alter  = xx.xx.xx SE-xxx
		//******************************************************************************
		public function dateTimeDisplayFormat_Java(dat: Date): String {
			
			var dfmt: DateFormatter = new DateFormatter();
			
			dfmt.formatString = SiteConst.G_JAVADATEFMT;
						
			return dfmt.format(dat);
		}

		//******************************************************************************
		//name   = dateDisplayFormat_Full
		//func   = 日付型format ※フル桁用
		//io     = i : dat			: 日付項目
		//return = 
		//date   = 10.01.20 SE-354
		//alter  = xx.xx.xx SE-xxx
		//******************************************************************************
		public function dateDisplayFormat_Full(dat: Date): String {
			
			var dfmt: DateFormatter = new DateFormatter();
			
			dfmt.formatString = SiteConst.G_FULLDATEFMT;
						
			return dfmt.format(dat);
		}
		
		//******************************************************************************
		//name   = yearmonthDisplayFormat
		//func   = 日付型format ※年月用
		//io     = i : dat			: 日付項目
		//return = 文字列
		//date   = 10.02.03 SE-354
		//alter  = xx.xx.xx SE-xxx
		//******************************************************************************
		public function yearmonthDisplayFormat(dat: Date): String {
			
			var dfmt: DateFormatter = new DateFormatter();
			
			dfmt.formatString = SiteConst.G_YEARMONTHFMT;
						
			return dfmt.format(dat);
		}
		
		//******************************************************************************
		//name   = yearDisplayFormat
		//func   = 日付型format ※年度用
		//io     = i : dat			: 日付項目
		//return = 文字列
		//date   = 10.08.24 SE-354
		//alter  = xx.xx.xx SE-xxx
		//******************************************************************************
		public function yearDisplayFormat(dat: Date): String {
			
			var dfmt: DateFormatter = new DateFormatter();
			
			dfmt.formatString = SiteConst.G_YEARFMT;
						
			return dfmt.format(dat);
		}
		
		//******************************************************************************
		//name   = monthDisplayFormat
		//func   = 日付型format ※月度用
		//io     = i : dat			: 日付項目
		//return = 文字列
		//date   = 10.08.24 SE-354
		//alter  = xx.xx.xx SE-xxx
		//******************************************************************************
		public function monthDisplayFormat(dat: Date): String {
			
			var dfmt: DateFormatter = new DateFormatter();
			
			dfmt.formatString = SiteConst.G_MONTHFMT;
						
			return dfmt.format(dat);
		}
		
		//******************************************************************************
		//name   = dayDisplayFormat
		//func   = 日付型format ※日1桁用
		//io     = i : dat			: 日付項目
		//return = 文字列
		//date   = 10.08.24 SE-354
		//alter  = xx.xx.xx SE-xxx
		//******************************************************************************
		public function dayDisplayFormat(dat: Date): String {
			
			var dfmt: DateFormatter = new DateFormatter();
			
			dfmt.formatString = SiteConst.G_DAYFMT;
						
			return dfmt.format(dat);
		}						

		//******************************************************************************
		//name   = dayDisplayFormat
		//func   = 日付型format ※日1桁用
		//io     = i : dat			: 日付項目
		//return = 文字列
		//date   = 10.08.24 SE-354
		//alter  = xx.xx.xx SE-xxx
		//******************************************************************************
		public function dayDisplayFormat2(dat: Date): String {
			
			var dfmt: DateFormatter = new DateFormatter();
			
			dfmt.formatString = SiteConst.G_DAYFMT2;
						
			return dfmt.format(dat);
		}
		
		/**
		 * 時間フォーマット
		 * @author SE-354
		 * @return HH:MM:SS (String)
		 */ 
		public function timeDisplayFormat(dat: Date): String {
			
			var dfmt: DateFormatter = new DateFormatter();
			
			dfmt.formatString = SiteConst.G_TIMEFMT;
			
			return dfmt.format(dat);
		}

	}
}
