package modules.datagrid
{
	import modules.base.BasedMethod;
	
	import spark.skins.spark.DefaultGridItemRenderer;
	
	// 配置薬履歴用アイテムレンダラ
	public class DataGridColumnRenderer extends DefaultGridItemRenderer
	{
		public function DataGridColumnRenderer()
		{
			super();

		}

		override public function set data(value:Object):void
		{
			super.data = value;
			
			var vlin: Number;
			
			
			if(data){
				vlin = BasedMethod.asCurrency(data.LINNO)
				if(vlin < 6){
					if(vlin > 0){
						align();
					}
				}else{
					if(vlin % 2 == 1){
						align();
					}
				}
			}
		}
		
		private function align(): void
		{
			// 右詰
			setStyle("textAlign", "right");
		}
/*		override public function set data(value:Object):void
		{
			super.data = value;
			
			
		}
*/
	
	}
}