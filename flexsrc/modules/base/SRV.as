package modules.base
{
	//import mx.rpc.remoting.RemoteObject;
	import modules.custom.CustomSingleton;
	
	import mx.messaging.ChannelSet;
	import mx.messaging.channels.AMFChannel;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.remoting.mxml.RemoteObject;

	/**********************************************************
	 * Javaとのやりとりを行うためのリモートオブジェクトを管理するクラス<br>
	 * FLEXと異なり、AIRではサーバと通信する画面は一部しかないためBASEから移動
	 * @author 12.09.04 SE-354
	 **********************************************************/
	final public class SRV
	{
		/**
		 * Java接続用RemoteObject
		 */
		//public var srv: RemoteObject;
		public var srv: mx.rpc.remoting.mxml.RemoteObject;
		
		private var singleton: CustomSingleton = CustomSingleton.getInstance();
		
		/**********************************************************
		 * コンストラクタ
		 * @param destination : 接続先のdestination (例:ub002x)
		 * @author 12.09.04 SE-354
		 **********************************************************/
		public function SRV(destination: String)
		{
			srv = new RemoteObject(destination);
			//srv.destination = destination;
			
			// AIRの場合、endpointを開発時と本番時にどのように動的に切り替えるかが課題。
			//とりあえず丸岡のサーバURLを仮にセットしておく
			//var messageUrl:String = "http://128.31.200.210:8400/fxny_server/messagebroker/amf";
//			var messageUrl:String = "http://10.5.1.2:8400/fxny_server/messagebroker/amf";
			var messageUrl:String = "http://128.7.60.101:8400/fxny_server/messagebroker/amf";
			srv.endpoint = messageUrl;
			
			var cs:ChannelSet = new ChannelSet();
			cs.addChannel(new AMFChannel("my-amf", messageUrl));
			
			srv.channelSet = cs;
		}
		
		/**********************************************************
		 * Java関数実行エラー時 処理.
		 * @param e エラーイベント
		 * @param obj オブジェクト
		 * @author SE-354 
		 * @date 12.05.10
		 **********************************************************/ 
		public function TokenFaultEvent(e: FaultEvent, obj: Object=null): void
		{
			var w_msg: String;
			
			//if (BasedMethod.nvl(w_msg))
			//{
				w_msg = BasedMethod.nvl(e.fault.faultDetail);
				singleton.sitemethod.loadOff();
				// エラーのアラート表示
				singleton.sitemethod.dspJoinErrMsg(w_msg, null, "エラー", "0", null, null, true);
			//}
			
		}
	}
}