package modules.base
{
	import flash.data.SQLConnection;
	import flash.data.SQLResult;
	import flash.data.SQLStatement;
	import flash.display.DisplayObjectContainer;
	import flash.errors.SQLError;
	import flash.events.Event;
	import flash.events.SQLErrorEvent;
	import flash.events.SQLEvent;
	import flash.events.StatusEvent;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	
	import modules.custom.AlertDialog;
	import modules.custom.CustomBusyIndicator;
	import modules.custom.CustomSingleton;
	import modules.formatters.CustomDateFormatter;
	import modules.formatters.CustomNumberFormatter;
	import modules.sbase.SiteConst;
	import modules.sbase.SiteMethod;
	import modules.sbase.SiteModule;
	
	import mx.collections.ArrayCollection;
	import mx.core.FlexGlobals;
	import mx.core.IMXMLObject;
	import mx.events.FlexEvent;
	import mx.messaging.ChannelSet;
	import mx.messaging.channels.AMFChannel;
	import mx.messaging.channels.StreamingAMFChannel;
	import mx.rpc.AsyncResponder;
	import mx.rpc.AsyncToken;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	import mx.rpc.remoting.mxml.RemoteObject;
	
	import spark.components.View;
	import spark.events.PopUpEvent;
	import spark.events.ViewNavigatorEvent;
	
	public class BasedModule implements IMXMLObject
	{
		/**
		 * 画面オブジェクト変数.
		 * MXMLのIDをセットして下さい。
		 */ 
		public var _view: Object;
		public var destination: String;			// Java接続用RemoteObject-destination
		//public var srv: RemoteObject;				// Java接続用RemoteObject
		public var token: AsyncToken;				// Java関数実行結果をセットする変数
		public var Stmt: SQLStatement;
		public var Connection:SQLConnection;
		
//		private var wsid: WSID;
		
		public var arrExecPlsql: ArrayCollection;	// ExecPlsql戻り値保持
		
		public var singleton: CustomSingleton = CustomSingleton.getInstance();
		
//		private var _GetWsid_args: Object;			// WSID取得関数用
		private var _CreateDataBase: Object;		// DB取得関数用
		private var _doQuery_args: Object;			// クエリ実行関数用
		
		public var srv: RemoteObject;				// Java接続用RemoteObject
		
		/**********************************************************
		 * デフォルトコンストラクタ.
		 * @author SE-354 
		 * @date 12.04.24
		 **********************************************************/
		public function BasedModule()
		{
			super();
			
			if(!singleton.sitemethod)
				singleton.sitemethod = new SiteMethod();
		}
		
		/**********************************************************
		 * 初期化処理.
		 * @param document(Object):オブジェクト
		 * @param id(String):プログラムID
		 * @author SE-354 
		 * @date 12.04.24
		 **********************************************************/
		public function initialized(document:Object, id:String):void
		{
			// フォーム情報を変数にセットする
			_view = document as Object;
			
			// イベント追加
			_view.addEventListener(FlexEvent.CREATION_COMPLETE, formCreate, false, 0, true);
			//_view.addEventListener(ViewNavigatorEvent.VIEW_ACTIVATE, formCreate);
			
		}
		
		/**********************************************************
		 * フォーム生成時 処理.
		 * @author SE-354 
		 * @date 12.04.24
		 **********************************************************/
		public function formCreate(event: FlexEvent): void
		{
			//applicationDisabled();
			
			
			// シングルトンにドキュメントをセット
// 12.10.25 Viewのときのみセットするように変更 SE-354 start
			if (_view is View)
			{
				singleton.view = _view;
			}
// 12.10.25 Viewのときのみセットするように変更 SE-354 end
			
			// 砂時計表示
//			singleton.sitemethod.loadOn();

			init();
			//connSrv();
			//setSRV();
		}
		/*
		public function setSRV(): void
		{
			srv = new RemoteObject;
			//srv.showBusyCursor = true;
			srv.destination = destination;
			
			// 11.05.13 ｺﾝﾃｷｽﾄを動的に変更するように対応 SE-357 start
			// ContextRoot取得
			//var url:String = mx.core.Application.application.url;
			//var context:String = url.split("/")[3];
			//			var secureMessageUrl:String = "https://{server.name}:{server.port}/" + context + "/messagebroker/amfsecure";
			//var messageUrl:String = "http://{server.name}:{server.port}/" + "fxny_server" + "/messagebroker/amf";
			var messageUrl:String = "http://128.31.200.210:8400/fxny_server/messagebroker/amf";			
			//srv.endpoint = messageUrl;
			
			// ChannelSetを再設定
			var cs:ChannelSet = new ChannelSet();
			/// SecureAMFChannelの設定追加
			//var samf:SecureAMFChannel = new SecureAMFChannel("my-secure-amf", secureMessageUrl);
			//cs.addChannel( samf ); 			
			/// AMFChannelの設定追加
			cs.addChannel( new AMFChannel("my-amf", messageUrl) ); 
			
			srv.channelSet = cs;
			// 11.05.13 ｺﾝﾃｷｽﾄを動的に変更するように対応 SE-357 end
		}
		*/
		
		/**********************************************************
		 * 初期化処理.
		 * @author SE-362 
		 * @date 12.06.18
		 **********************************************************/
		public function init(): void
		{
			getComboData();
		}
		
		/**********************************************************
		 * コンボボックス用データ作成.
		 * @author SE-362 
		 * @date 12.06.18
		 **********************************************************/
		public function getComboData(): void
		{
			openComboData();
			formShow();
		}
		
		/**********************************************************
		 * コンボボックス用データ取得.
		 * @author SE-362 
		 * @date 12.06.18
		 **********************************************************/
		public function openComboData(): void
		{
		}
		
		/**********************************************************
		 * フォームオープン時処理.
		 * @author SE-362 
		 * @date 12.06.18
		 **********************************************************/
		public function formShow(): void
		{
			/*
			if(singleton.g_query_xml == null || singleton.g_msg_xml == null){
				var file1: File = File.applicationDirectory.resolvePath(SiteConst.G_QUERY_FILE);
				var file2: File = File.applicationDirectory.resolvePath(SiteConst.G_MSG_FILE); 
				
				var stream1: FileStream = new FileStream();
				var stream2: FileStream = new FileStream();
				
				// XMLファイル読み込み
				stream1.open(file1, FileMode.READ);
				stream2.open(file2, FileMode.READ);
				singleton.g_query_xml = XML(stream1.readUTFBytes(stream1.bytesAvailable));
				singleton.g_msg_xml = XML(stream2.readUTFBytes(stream2.bytesAvailable));
				stream1.close();
				stream2.close();
			}
			*/
			
			// ローカルDB作成
			//CreateDataBase(formShow_cld1);
			formShow_cld1();
		}
		
		public function formShow_cld1(): void
		{
			/*
			// WSID取得
			if(singleton.g_wsid == null){
				GetWsid(formShow_cld2);
				return;
			}
			*/
			
			formShow_cld2();
		}
		
		public function formShow_cld2(): void
		{
			formShow_last();	
		}
		
		public function formShow_last(): void
		{
			// 砂時計解除
			singleton.sitemethod.loadOff();
		}
		
		/**********************************************************
		 * サイトURLの取得.
		 * @return URL
		 * @author SE-354 
		 * @date 12.04.24
		 **********************************************************/
		/*
		public function getSiteURL():String
		{
			var w_siteurl: String;
			var w_apptype: String;
			
			var i: int;
			var w_index: int;
			
			// appTypeの取得
			w_apptype = "0";
			
			w_apptype = singleton.g_appType;
			
			// Flexの場合
			if (w_apptype == "0") {
				
				w_siteurl = FlexGlobals.topLevelApplication.url;
				
				for (i=0; i<SiteConst.G_BIN_FOLDER.length; i++) {
					
					w_index = w_siteurl.lastIndexOf(SiteConst.G_BIN_FOLDER[i]);
					
					if (w_index != -1) {
						
						w_siteurl = w_siteurl.substr(0, w_index + 1);
					}
				}
				
				// AIR(オンライン)の場合
			} else if (w_apptype == "1") {
				
				w_siteurl = srv.endpoint;
				
				for (i=0; i<SiteConst.G_MESSAGEBROKER.length; i++) {
					
					w_index = w_siteurl.lastIndexOf(SiteConst.G_MESSAGEBROKER[i]);
					
					if (w_index != -1) {
						
						w_siteurl = w_siteurl.substr(0, w_index + 1);
					}
				}
			}
				// AIR(オフラインの場合)
			else
			{
				w_siteurl = null;
			}
			
			return w_siteurl;	
		}
		*/
		
		/**********************************************************
		 * WSIDの取得.
		 * @param Func: 次関数
		 * @return WSID: 端末ID（ANDROID_ID）
		 * @author SE-362 
		 * @date 12.06.19
		 **********************************************************/
/*
		public function GetWsid(Func: Function): void
		{
			wsid = new WSID();
			
			_GetWsid_args = new Object;
			_GetWsid_args.Func = Func;
			
			// イベント追加
			wsid.addEventListener(StatusEvent.STATUS, GetWsid_last);
			
			// 実行
			wsid.GetWsid();
		}
		
		public function GetWsid_last(event: StatusEvent): void
		{
			var w_args: Object = _GetWsid_args;
			
			// 変数セット
			if(event.code)
			{
				singleton.g_wsid = event.code;
			}
			
			// 次関数実行
			w_args.Func();
		}
*/
		
		/**********************************************************
		 * btnDSP押下時 処理.
		 * @author SE-354 
		 * @date 12.04.27
		 **********************************************************/
		public function doDSPProc(): void
		{
			// 10.03.31 by SE-254 start
			//			headerCheck();	
			
			// オンラインチェック
			//ChkOLF("1", headerCheck, doDSPProc_last);
			
			//doDSPProc_last();
			singleton.sitemethod.loadOn();

			execSetPLSQL();
		}
		
		public function doDSPProc_last(): void
		{
			singleton.sitemethod.loadOff();

			// Applicationを使用可に
			applicationEnabled();
		}
		
		/**********************************************************
		 * btnF10押下時 処理.
		 * @author SE-354 
		 * @date 12.06.11
		 **********************************************************/
		public function doF10Proc(): void
		{
			singleton.sitemethod.loadOn();
			
			doKakutei();
		}
		
		public function doF10Proc_last(): void
		{
			singleton.sitemethod.loadOff();

			// Applicationを使用可に
			applicationEnabled();
		}
		
		/**********************************************************
		 * Setプロシージャの実行.
		 * @author SE-354 
		 * @date 12.04.27
		 **********************************************************/
		public function execSetPLSQL(): void
		{
			openDataSet_H();
		}
		
		
		/**********************************************************
		 * データの取得(ヘッダ).
		 * @author SE-354 
		 * @date 12.04.27
		 **********************************************************/
		public function openDataSet_H(): void
		{
			openDataSet_B();
		}
		
		/**********************************************************
		 * データの取得(ボディ).
		 * @author SE-354 
		 * @date 12.04.27
		 **********************************************************/
		public function openDataSet_B(): void
		{
			setDispObject();
		}
		
		/**********************************************************
		 * 画面項目セット.
		 * @author SE-354 
		 * @date 12.04.27
		 **********************************************************/
		public function setDispObject(): void
		{
			/*
			var i: int;
			
			// ポップアップの場合、カラム情報をセットする
			if (g_popupflg == "1") {
			
			arrFormItems = getArrFormItems(_pnlMAIN);
			
			for (i=0; i<arrFormItems.length; i++) {
			
			// (CustomDataGrid時)
			if (arrFormItems[i].COMP is CustomDataGrid) {
			
			// データが存在するときのみ
			if (CustomDataGrid(arrFormItems[i].COMP).dataProvider.length != 0) {
			multiLangDataGrid(arrFormItems[i].COMP);
			}
			}
			}
			}				
			*/			
			singleton.sitemethod.operateEntry();
			//			BtnCtrl(1);
			
			doDSPProc_last();
		}
		
		//==============================================================================
		// 確定ボタン(btnF10)押下時、共通関数
		//   ※Javaの関数を呼び出す場合、非同期で処理がされるため
		//     いくつかの関数に分けて実行する
		//         1.checkDataSet	… データのチェック
		//         2.updateDataSet	… データの更新(ヘッダ、ボディ用あり)
		//         3.execDecPLSQL	… Decプロシージャの実行
		//     (POPUP時)
		//         1.getRtnData		… 戻り値の取得
		//         2.closePopup		… ポップアップ終了 (※btnF12と共用)
		//==============================================================================
		/**********************************************************
		 * 確定時 処理.
		 * @author SE-354 
		 * @date 12.06.11
		 **********************************************************/
		public function doKakutei(): void
		{
			// オンラインチェック
			//ChkOLF("2", doKakutei_cld1, doKakutei_cld1);
			
			doKakutei_cld1();
		}
		
		private function doKakutei_cld1(): void
		{
			// progressBar表示
			//createProgressBar(SiteConst.G_EXECUTION);
			
			//doF10Proc_last();
			checkDataSet();
		}
		
		/**********************************************************
		 * データのチェック.
		 * @author SE-354 
		 * @date 12.06.11
		 **********************************************************/
		public function checkDataSet(): void
		{
			updateDataSet_H();
		}
		
		/**********************************************************
		 * データの更新(ヘッダ).
		 * @author SE-354 
		 * @date 12.06.11
		 **********************************************************/		
		public function updateDataSet_H(): void
		{
			updateDataSet_B();
		}
		
		/**********************************************************
		 * データの更新(ボディ).
		 * @author SE-354 
		 * @date 12.06.11
		 **********************************************************/		
		public function updateDataSet_B(): void
		{
			execDecPLSQL();
		}
		
		/**********************************************************
		 * Decプロシージャの実行.
		 * @author SE-354 
		 * @date 12.06.11
		 **********************************************************/		
		public function execDecPLSQL(): void
		{
			doF10Proc_last();
		}		
		
		/**********************************************************
		 * アプリケーション制御不可.
		 * @author SE-354 
		 * @date 12.04.27
		 **********************************************************/
		public function applicationDisabled(): void
		{
			FlexGlobals.topLevelApplication.enabled = false;
		}
		
		/**********************************************************
		 * アプリケーション制御可.
		 * @author SE-354 
		 * @date 12.04.27
		 **********************************************************/
		public function applicationEnabled(): void
		{
			FlexGlobals.topLevelApplication.enabled = true;
		}
		
		//==============================================================================
		// SQL関連
		//==============================================================================
		
		/**********************************************************
		 * PLSQL実行
		 * @param p_prop: propertiesファイル名
		 * @param p_sql: 取得する変数名
		 * @param p_arg: PLSQLに渡す引数
		 * @param nextfunc: 次に実行したい関数
		 * @author SE-354 
		 * @date 12.09.21
		 **********************************************************/
		public function execPLSQL(p_prop: String, p_sql: String, p_arg: ArrayCollection, nextfunc: Function=null): void
		{
			var w_res: int;
			var w_msg1: String;
			var w_msg2: String;
			
			var srv: SRV = new SRV(p_prop);
			
			// 戻り値の初期化
			arrExecPlsql = new ArrayCollection();
			
			// 変数の初期化
			token = null;
			
			
			// PLSQLの実行
			token = srv.srv.ExecPLSQL(p_prop, p_sql, p_arg);
			//token = srv.ExecPLSQL(p_prop, p_sql, p_arg);
			
			// 実行結果より、戻り値を取得
			token.addResponder(new AsyncResponder(
				
				// 成功時の処理
				function(e:ResultEvent, obj:Object=null):void
				{
					// 戻り値のセット
					arrExecPlsql = e.result as ArrayCollection;
					
					// 次関数を実行
					BasedMethod.execNextFunction(nextfunc);
					
				},
				
				// 失敗時の処理
				//srv.TokenFaultEvent
				// 失敗時の処理
				function(e:FaultEvent, obj:Object=null): void
				{
					TokenFaultEvent(e, obj);
					
					return;
				}
				//trace("通信エラー")
			));
			
		}
		
		/**
		 * PLSQL実行 非同期(処理が返ってくるのを待たないver)
		 * @param p_prop: propertiesファイル名
		 * @param p_sql: 取得する変数名
		 * @param p_arg: PLSQLに渡す引数
		 * @param nextfunc: 次に実行したい関数
		 * @author SE-354 
		 * @date 12.12.03
		 */ 
		public function execPLSQL2(p_prop: String, p_sql: String, p_arg: ArrayCollection, nextfunc: Function=null): void
		{
			var w_res: int;
			var w_msg1: String;
			var w_msg2: String;
			
			var srv: SRV = new SRV(p_prop);
			
			// 戻り値の初期化
			arrExecPlsql = new ArrayCollection();
			
			// 変数の初期化
			token = null;
			
			
			// PLSQLの実行
			token = srv.srv.ExecPLSQL(p_prop, p_sql, p_arg);
			//token = srv.ExecPLSQL(p_prop, p_sql, p_arg);
			
			// 次の処理
			BasedMethod.execNextFunction(nextfunc);
			
		}
		
		//******************************************************************************
		//name   = TokenFaultEvent
		//func   = Java関数実行エラー時 処理
		//io     = i : FaultEvent	: エラーイベント
		//         i : obj			: オブジェクト
		//return = 
		//date   = 09.11.22 SE-254
		//alter  = xx.xx.xx SE-xxx
		//******************************************************************************
		public function TokenFaultEvent(e: FaultEvent, obj: Object=null): void
		{
			// 10.03.15 by SE-254 start			
			//			dspJoinErrMsg("E0002", e.fault.faultString, 3);
			
			var w_msg: String;
			
			w_msg = BasedMethod.nvl(e.fault.faultDetail);
			singleton.sitemethod.loadOff();
			singleton.sitemethod.dspJoinErrMsg(w_msg, null, "エラー", "0", null, null, true);
			// 10.03.15 by SE-254 end			
			
			// Applicationを使用可に
			//applicationEnabled();
		}
		
		//==============================================================================
		// 個別処理
		//==============================================================================
		
		/**********************************************************
		 * ローカルDB作成処理
		 * @param Func: 次関数
		 * @author SE-362 
		 * @date 12.06.18
		 **********************************************************/
		public function CreateDataBase(Func: Function=null): void
		{
			var orgDB:File = File.applicationDirectory.resolvePath(SiteConst.G_DATABASE_FILE);
			//var copyDB:File = File.applicationStorageDirectory.resolvePath(SiteConst.G_DATABASE_FILE);
			var copyDB:File = File.documentsDirectory.resolvePath(SiteConst.G_DATABASE_FILE);
			
			// 引数セット
			_CreateDataBase = new Object;
			_CreateDataBase.Func = Func;
			
			if(! copyDB.exists){
				var parentD:File = copyDB.parent;
				parentD.createDirectory();
				// デバイスにDBが存在しない場合、新規作成
				try	{
					orgDB.copyTo(copyDB);
				}catch (error:SQLError)	{
				}
			}else{
				orgDB = null;
			}
			
			//SQLConnectionを使用して、ローカルのデータベースとの接続
			singleton.sqlconnection = new SQLConnection();
			
			// イベント追加
			singleton.sqlconnection.addEventListener(SQLEvent.OPEN, ConnectionOpenHandler, false, 0, true);
			singleton.sqlconnection.addEventListener(SQLErrorEvent.ERROR, ConnectionErrorHandler, false, 0, true);
			//singleton.sqlconnection.openAsync(copyDB);
			singleton.sqlconnection.open(copyDB);
			
		}
		
		public function ConnectionOpenHandler(event: SQLEvent): void
		{
			var w_args: Object = _CreateDataBase;
			
			if (w_args.Func != null)
			{
				// 次関数実行
				w_args.Func();
			}
		}
		
		public function ConnectionErrorHandler(event: SQLErrorEvent): void
		{
			var w_args: Object = _CreateDataBase;
			
			// エラー表示
			singleton.sitemethod.dspJoinErrMsg(event.error.message, null, null, "2");
			//BasedMethod.dspJoinErrMsg(event.error.message, null, null, "2");
			
			if (w_args.Func != null)
			{
				// 次関数実行
				w_args.Func();
			}
		}
		
		/**********************************************************
		 * SQL実行（ローカル）.
		 * @param XMLKey: クエリ取得キー 
		 * @param Flag: 0: 取得、1: 更新、削除
		 * @param Dto: 型情報
		 * @param Result: 結果格納配列
		 * @param Param: 引数情報
		 * @param Func: 次関数
		 * @author SE-362 
		 * @date 12.06.18
		 **********************************************************/
		public function doQuery(XMLKey: String, Flag: String, Result: ArrayCollection
								, Param: Array, Func: Function): void
		{
			// 引数セット
			_doQuery_args = new Object;
			_doQuery_args.Key	= XMLKey;
			_doQuery_args.Flag	= Flag;
			_doQuery_args.Result= Result;
			_doQuery_args.Param	= Param;
			_doQuery_args.Func	= Func;
			
			// SQL情報
			Stmt = new SQLStatement();
			Stmt.sqlConnection =  Connection;
			Stmt.text= singleton.g_query_xml.entry.(@key == XMLKey);
			
			// パラメータ設定
			if(Param){
				for(var i: int=0; i < Param.length; i++){
					Stmt.parameters[i] = Param[i];
				}
			}
			
			//イベント登録
			Stmt.addEventListener(SQLEvent.RESULT, doQuery_last, false, 0, true);
			Stmt.addEventListener(SQLErrorEvent.ERROR, doQuery_ErrorHandler, false, 0, true);
			
			//実行
			Stmt.execute();
		}
		
		public function doQuery_last(event: SQLEvent): void
		{
			var w_arg: Object = _doQuery_args;
			
			// 取得時、返り値セット
			if(w_arg.Flag == "0"){
				var result: SQLResult = Stmt.getResult();
				
				// 配列セット
				if(result.data)
					w_arg.Result = new ArrayCollection(result.data);
				
			}
			// 次関数実行
			w_arg.Func();
		}
		
		public function doQuery_ErrorHandler(event: SQLErrorEvent): void
		{
			var w_arg: Object = _doQuery_args;
			
			// エラーメッセージ表示
			singleton.sitemethod.dspJoinErrMsg(event.error.message, null, w_arg.Key, "2");
			//BasedMethod.dspJoinErrMsg(event.error.message, null, w_arg.Key, "2");
		}
		
	}
}