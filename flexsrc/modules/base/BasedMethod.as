package modules.base
{
	import flash.display.DisplayObjectContainer;
	import flash.filesystem.File;
	import flash.net.NetworkInfo;
	import flash.net.NetworkInterface;
	import flash.utils.ByteArray;
	
	import modules.custom.AlertDialog;
	import modules.custom.BigDecimal;
	import modules.custom.CustomBusyIndicator;
	import modules.custom.CustomCheckBox;
	import modules.custom.CustomComboBox;
	import modules.custom.CustomDateField;
	import modules.custom.CustomSingleton;
	import modules.custom.CustomTextArea;
	import modules.custom.CustomTextInput;
	import modules.custom.MathContext;
	import modules.sbase.SiteConst;
	import modules.sbase.SiteMethod;
	import modules.sbase.SiteModule;
	import mx.collections.ArrayCollection;
	import mx.collections.Sort;
	import mx.collections.SortField;
	import mx.core.UIComponent;
	import mx.validators.DateValidator;	

	import spark.events.PopUpEvent;

	public class BasedMethod
	{
		// Global変数
		public var singleton: CustomSingleton = CustomSingleton.getInstance();
		public var Alert: AlertDialog;

		private var _dspJoinErrMsg_args: Object;		// Message表示関数用
		private var _busyInd: CustomBusyIndicator;		// 砂時計

		/**********************************************************
		 * デフォルトコンストラクタ.
		 * @author SE-354 
		 * @date 12.04.24
		 **********************************************************/
		public function BasedMethod()
		{
			super();
		}
		
		/**********************************************************
		 * 次関数の実行.
		 * @param nextfunc:実行したい関数
		 * @author 12.04.24 SE-354
		 **********************************************************/
		static public function execNextFunction(nextfunc: Function): void
		{
			// 次に実行したい関数があれば実行
			if (nextfunc != null) {
				nextfunc();
			}
		}
		
		/**********************************************************
		 * null値の置換.
		 * @param p_val:置換したい項目値
		 * @param p_str:置換文字列
		 * @return 置換後の値
		 * @author 12.04.24 SE-354
		 **********************************************************/
		static public function nvl(p_val: String, p_str: String=""): String
		{
			var w_ret: String;
			
			// 値のセット
			w_ret = p_val;
			
			// nullの場合、置換する
			if (w_ret == null) {
				w_ret = p_str;
			}
			
			return w_ret;
		}

		/**********************************************************
		 * notNullプロパティの取得
		 * @param p_style: スタイルシート名
		 * @return "1"（必須） or "0"
		 * @author 12.04.24 SE-362
		 **********************************************************/
		static public function getNotNull(style: String): String
		{
			var w_ret: String;
			var i: int;
			
			// 戻り値の初期化
			w_ret = "0";
			
			for (i=0; i<SiteConst.G_NOTNULLITEM.length; i++) {
				
				if (style.indexOf(SiteConst.G_NOTNULLITEM[i]) != -1) {
					
					w_ret = "1";
					break;
				}
			}
			
			return w_ret;
		}

		/**********************************************************
		 * numOnlyプロパティの取得
		 * @param p_style: スタイルシート名
		 * @return "1"（数値のみ） or "0"
		 * @author 12.04.24 SE-362
		 **********************************************************/
		static public function getNumOnly(style: String): String
		{
			var w_ret: String;
			var i: int;
			
			// 戻り値の初期化
			w_ret = "0";
			
			for (i=0; i<SiteConst.G_NUMITEM.length; i++) {
				
				if (style.indexOf(SiteConst.G_NUMITEM[i]) != -1) {
					
					w_ret = "1";
					break;
				}
			}
			
			return w_ret;
		}
		
		/**********************************************************
		 * restrictプロパティの取得
		 * @return restrictプロパティ
		 * @author 12.04.24 SE-362
		 **********************************************************/
		static public function getNumRestrict(): String
		{
			var w_ret: String;
			
			w_ret = SiteConst.G_NUM_RESTRICT;
			
			return w_ret;
		}

		/**********************************************************
		 * enabledプロパティの取得
		 * @param p_style: スタイルシート名
		 * @return Boolean
		 * @author 12.04.24 SE-362
		 **********************************************************/
		static public function getEnabled(style: String): Boolean
		{
			var w_ret: Boolean;
			var w_index: int;
			
			// 戻り値の初期化
			w_ret = true;
			
			// 常にenabled=falseの項目かどうかを確認
			w_index = getArrayIndex(new ArrayCollection(SiteConst.G_NO_ENABLED), "", style);
			
			if (w_index != -1) {
				
				w_ret = false;
			}
			
			return w_ret;
		}

		/**********************************************************
		 * 配列要素番号取得
		 * @param arr : 配列
		 * @param item: 項目名
		 * @param val : true（配列の後ろから検索） false（配列の後ろから検索）
		 * @return 要素番号
		 * @author 12.04.24 SE-362
		 **********************************************************/
		static public function getArrayIndex(arr: ArrayCollection, item: String, val: String, back:Boolean = false): int
		{
			var getArrayIndex: int;
			var i: int;
			
			// 戻り値の初期化
			getArrayIndex = -1;
			
			// 配列が存在するとき
			if (arr != null)
			{	
				if (back == false)
				{			
					for (i=0; i<arr.length; i++)
					{	        		
						// 項目名が指定されているとき
						if (nvl(item) != "") {
							
							if (arr[i][item] == val) {
								getArrayIndex = i;
								break;
							}
							
						} else {
							
							if (arr[i] == val) {
								getArrayIndex = i;
								break;
							}
						}
					}
				}
					// 10.08.24 後ろから検索 SE-354 start		  		
				else
				{
					for (i=arr.length-1; i!=0; i--)
					{	        		
						// 項目名が指定されているとき
						if (nvl(item) != "") {
							
							if (arr[i][item] == val) {
								getArrayIndex = i;
								break;
							}
							
						} else {
							
							if (arr[i] == val) {
								getArrayIndex = i;
								break;
							}
						}
					}		  			
				}
				// 10.08.24 後ろから検索 SE-354 end
			}
			return getArrayIndex;
		}

		/**********************************************************
		 * tabEnableプロパティ取得
		 * @param i: スタイルシート名
		 * @return Boolean
		 * @author 12.04.24 SE-362
		 **********************************************************/
		static public function getTabEnabled(style: String): Boolean
		{
			var w_ret: Boolean;
			var w_index: int;
			
			// 戻り値の初期化
			w_ret = true;
			
			// 常にtabEnabled=falseの項目かどうかを確認
			w_index = getArrayIndex(new ArrayCollection(SiteConst.G_NO_TAB), "", style);
			
			if (w_index != -1) {
				
				w_ret = false;
			}
			
			return w_ret;
		}

		/**********************************************************
		 * 文字のバイト数取得
		 * @param chr: 入力文字
		 * @return バイト数
		 * @author 12.04.24 SE-362
		 **********************************************************/
		public function getByteLen(chr: String): Number
		{
			var getByteLen: Number;
			var charCode: Number; 
			
			getByteLen = 0;
			charCode   = chr.charCodeAt(0);
			
			// C-KOPS対応 12.01.26 by SE-254 start
			//			// 半角英数は1バイト
			//			if ((0x20 <= charCode) && (charCode <= 0x7F)) {
			//				getByteLen = 1;
			//
			//			// 半角カナは1バイト
			//			} else if ((0xFF61 <= charCode) && (charCode <= 0xFF9F)) {
			//				getByteLen = 1;
			//
			//			// その他は2バイト
			//			} else {
			//				getByteLen = 2;
			//			}
			
			// 半角英数は1バイト
			if ((0x20 <= charCode) && (charCode <= 0x7F)) {
				getByteLen = SiteConst.G_BYTE_HANKAKU;
				
				// 半角カナは1バイト
			} else if ((0xFF61 <= charCode) && (charCode <= 0xFF9F)) {
				getByteLen = SiteConst.G_BYTE_HANKANA;
				
				// その他は2バイト
			} else {
				getByteLen = SiteConst.G_BYTE_ELSE;
			}
			// C-KOPS対応 12.01.26 by SE-254 end
			
			return getByteLen;
		}
		
		/**********************************************************
		 * 数値への変換（Null時はゼロ）
		 * @param val: 変換したい項目値
		 * @return 値
		 * @author 12.04.24 SE-362
		 **********************************************************/
		static public function asCurrency(p_val: String): Number
		{
			var w_ret: Number;
			var w_val: String;
			
			var myPattern:RegExp = /,/g;
			
			// 値のセット
			if (nvl(p_val) == "") {
				w_ret = 0;
				
			} else {
				w_val = p_val.replace(myPattern, "");
				
				w_ret = Number(w_val);
			}
			
			// 11.04.01 SE-354 start
			if (isNaN(w_ret) == true)
			{
				w_ret = 0;
			}
			// 11.04.01 SE-354 end
			
			return w_ret;
		}

		/**********************************************************
		 * styleNameプロパティの取得
		 * @param numonly: 数値項目フラグ(1:数値項目)
		 * @param notnull: 入力必須フラグ(1:必須項目)
		 * @param focus  : フォーカス状態(0:out、1:in)
		 * @return styleNameプロパティ
		 * @author 12.04.24 SE-362
		 **********************************************************/
		public function getStyleName(numonly: String, notnull: String, focus: String): String
		{
			var w_ret: String;
			
			w_ret = SiteConst.G_STYLE_INPUT;
			
			// 数値項目の時
			if (numonly == "1") {
				w_ret = w_ret + SiteConst.G_STYLE_NUM;
			}
			
			// 必須項目の時
			if (notnull == "1") {
				w_ret = w_ret + SiteConst.G_STYLE_MUST;
			}
			
			// フォーカスインの時
			if (focus == "1") {
				w_ret = w_ret + SiteConst.G_STYLE_FOCUSIN;
			}
			
			return w_ret;
		}

		/**********************************************************
		 * ネットワークが利用出来るかどうかを調べる.<br>
		 * 3Gもwifiもつながっていない場合 false<br>
		 * どちらか1つでも有効な場合 true
		 * @return 利用可能かどうか true/false
		 * @author 12.04.24 SE-354
		 **********************************************************/
		public function checkNetworkEnabled():Boolean
		{
			var isCheck:Boolean = false;
			if(NetworkInfo.isSupported)
			{
				//NetworkInfoを参照します。
				var networkinfo:NetworkInfo = NetworkInfo.networkInfo;
				//NetworkInfoの中に複数のNetworkInterfaceが含まれていて個別に調べます。
				for each (var networkinterface:NetworkInterface in networkinfo.findInterfaces())
				{
					//通信可能なインターフェイスがある場合はそのインターフェイスの情報を表示。
					if(networkinterface.active)
					{
						isCheck = true;
					}
				}
			}
			return isCheck;
		}

		/**********************************************************
		 * 入力制御
		 * @param Mode: モード 1: 入力可能、2: フォーカスセット不可、入力不可
		 * @param Obj: 入力必須フラグ(1:必須項目)
		 * @author 12.06.20 SE-362
		 **********************************************************/
		public function change_ENTRY(Mode: int, Obj: UIComponent): void
		{
			var w_stylename: String;
			
			try {
				w_stylename = Obj.styleName.toString();
				
			} catch (e: Error) {
				w_stylename = "";
			}
			// 入力可能
			if (Mode == 0) {
				if (nvl(w_stylename) == "") {
					Obj.enabled    = true;
					
				} else {
					Obj.enabled    = getEnabled(w_stylename);
					// 11.01.21 SE-354 start
					if (Obj is CustomTextInput)
					{
						Obj["notNull"] = Obj["notNull_org"];
					}
						//					else if (Obj is CustomTextArea)
						//					{
						//						Obj["notNull"] = Obj["notNull_org"];
						//					}
					else if (Obj is CustomDateField)
					{
						Obj["notNull"] = Obj["notNull_org"];
					}
					else if (Obj is CustomComboBox)
					{
						Obj["notNull"] = Obj["notNull_org"];
					}
					else if (Obj is CustomCheckBox)
					{
						Obj["notNull"] = Obj["notNull_org"];
					}
					// 11.01.21 SE-354 end
					// 10.07.05 SE-354 start	            	
					//	            	Obj.tabEnabled = getTabEnabled(w_stylename);
					// 10.07.05 SE-354 end
				}
				
				// フォーカスセット不可、入力不可
			} else if (Mode == 1) {
				if (nvl(w_stylename) == "") {
					Obj.enabled    = false;
					
				} else {
					Obj.enabled    = false;
					// 11.01.21 SE-354 start
					if (Obj is CustomTextInput)
					{
						Obj["notNull"] = "0";
					}
					else if (Obj is CustomTextArea)
					{
						Obj["notNull"] = "0";
					}
					else if (Obj is CustomDateField)
					{
						Obj["notNull"] = "0";
					}
					else if (Obj is CustomComboBox)
					{
						Obj["notNull"] = "0";
					}
					else if (Obj is CustomCheckBox)
					{
						Obj["notNull"] = "0";
					}
					// 11.01.21 SE-354 end
					// 10.07.05 SE-354 start
					//					Obj.tabEnabled = false;
					// 10.07.05 SE-354 end					
				}
			}
		}

		/**********************************************************
		 * 配列を数値の小さい順に並べ替える(引数desc=true時には大きい順に並べ替える)
		 * @param  arr     : ソート対象配列
		 * @param  numField: 数値項目のDB項目名
		 * @param  desc    : true(降順) false(昇順)
		 * @return ArrayCollection: ソート後配列
		 * @author 12.06.20 SE-362
		 **********************************************************/
		public function sortArrayByValue(arr:ArrayCollection, numField:String, desc: Boolean=false): ArrayCollection 
		{
			var ac:ArrayCollection = new ArrayCollection();
			ac.source = arr.source.concat();
			
			var dataSortField:SortField = new SortField(numField,false,desc,true);
			
			var numericDataSort:Sort = new Sort();
			numericDataSort.fields = [dataSortField];
			
			ac.sort = numericDataSort;
			ac.refresh();
			
			return ac;
		}

		/**********************************************************
		 * エラーメッセージクリア
		 * @param  obj : メッセージをクリアするオブジェクト
		 * @author 12.06.20 SE-362
		 **********************************************************/
		public function clearErrMsg(obj: Object): void
		{
			if(obj)
				UIComponent(obj).errorString = "";
		}

		/**********************************************************
		 * エラーメッセージセット
		 * @param  msgCode : メッセージをセットするオブジェクト
		 * @param  obj  : メッセージをセットするオブジェクト
		 * @param  rMsg : 置換用配列
		 * @author 12.06.20 SE-362
		 **********************************************************/
		public function setErrMsg(key: String, obj: Object, rMsg: Array=null): void
		{
			if(obj){
				if(rMsg != null && rMsg.length != 0){
					UIComponent(obj).errorString = replaceMessage(key, rMsg);
				}else{
					UIComponent(obj).errorString = singleton.g_msg_xml.entry.(@key == key);
				}
			}
		}

		/**********************************************************
		 * メッセージ置換処理
		 * @param  msgCode: メッセージコード
		 * @param  replaceMsg: 置換文字配列
		 * @author 12.06.20 SE-362
		 **********************************************************/
		public function replaceMessage(msgCode: String, replaceMsg: Array): String
		{
			if(replaceMsg != null && replaceMsg.length != 0){
				var str: String = singleton.g_msg_xml.entry.(@key == msgCode);
				var indexString: String;

				for(var i: int=0; i < replaceMsg.length; i++){
					
					indexString = "{" + i.toString() + "}";
					if(str.indexOf(indexString) > -1)
						str = str.replace(indexString, replaceMsg[i]);
				}
				return str;
			}else{
				return singleton.g_msg_xml.entry.(@key == msgCode);
			}
		}

		/**********************************************************
		 * メッセージ出力.
		 * @param Msg       : メッセージコード
		 * @param NextFunc  : 次関数（OK時）
		 * @param Title     : 出力タイトルメッセージ
		 * @param rMsg      : 置換メッセージ
		 * @param BtnType   : 0: Ok、1: Ok Cancel
		 * @param NextFunc2 : 次関数（Cancel時）
		 * @param Direct    : false:メッセージコードを利用する, true:直接メッセージを出力する
		 * @author SE-362 
		 * @date 12.06.15
		 * @alter 12.09.27 パラメータにDirect追加
		 **********************************************************/
		public function dspJoinErrMsg(Msg: String, NextFunc: Function=null, Title: String=""  
									  , BtnType: String="0", rMsg: Array=null, NextFunc2: Function=null, Direct: Boolean=false): void
		{
			Alert = new AlertDialog();
			
			// 引数のセット
			_dspJoinErrMsg_args = new Object;
			_dspJoinErrMsg_args.BtnType = BtnType;
			_dspJoinErrMsg_args.NextFunc = NextFunc;
			_dspJoinErrMsg_args.NextFunc2 = NextFunc2;
			
			if (Direct == false)
			{
				// アラート情報セット
				if(rMsg != null && rMsg.length != 0)
					Alert.message = replaceMessage(Msg, rMsg);
				else
					Alert.message = singleton.g_msg_xml.entry.(@key == Msg);
				
				Alert.title = Title;
				Alert.btntype = BtnType;
	
				if(Msg.substr(0, 1) == "I"){
					Alert.type = "0";
				}else if(Msg.substr(0, 1) == "W"){
					Alert.type = "1";
				}else if(Msg.substr(0, 1) == "E"){
					Alert.type = "2";
				}else{
					Alert.type = "";
				}
			}
			else
			{
				Alert.message = Msg;
				Alert.title = Title;
				Alert.btntype = BtnType;
				
				Alert.type = "2";
			}
			
			// アラート表示
			Alert.open(DisplayObjectContainer(singleton.view), true);
			
			// イベント登録
			Alert.addEventListener(PopUpEvent.CLOSE, dspJoinErrMsg_cld1);
		}
		
		private function dspJoinErrMsg_cld1(event: PopUpEvent): void
		{
			var w_arg: Object = _dspJoinErrMsg_args;
			
			// 次関数が指定されている場合、実行
			if(w_arg.NextFunc != null){
				if(w_arg.BtnType == "0" || event.commit == true){
					// Ok
					w_arg.NextFunc();
				}else{
					if(w_arg.NextFunc2 != null){
						// Cancel
						w_arg.NextFunc2();
					}else{
						w_arg.NextFunc();
					}
				}
			}
		}

		/**********************************************************
		 * 画面入力制御.
		 * @author SE-354 
		 * @date 12.04.27
		 **********************************************************/
		public function operateEntry(): void
		{
		}
		
		/**********************************************************
		 * 待ち画面表示.
		 * @author SE-362 
		 * @date 12.06.15
		 **********************************************************/
		public function loadOn(): void
		{
			_busyInd = new CustomBusyIndicator();
		
			_busyInd.open(DisplayObjectContainer(singleton.view));
		}

		/**********************************************************
		 * 待ち画面非表示.
		 * @author SE-362 
		 * @date 12.06.15
		 **********************************************************/
		public function loadOff(): void
		{
			if(_busyInd)
				_busyInd.close();
		}

		/**********************************************************
		 * 現在時刻取得(HH:MM)
		 * @author SE-362 
		 * @date 12.06.15
		 **********************************************************/
		static public function getTime(): String
		{
			var date: Date = new Date();
			
//			return date.hours.toString() + ":" + date.minutes.toString();

			var w_hh: String = "00" + date.hours.toString();
			var w_mm: String = "00" + date.minutes.toString();

			return w_hh.substr(w_hh.length - 2, 2) + ":" + w_mm.substr(w_mm.length - 2, 2);
		}
		
		/**********************************************************
		 * 現在時刻取得(HH:MM:SS)
		 * @author SE-233 
		 * @date 12.12.19
		 **********************************************************/
		static public function getTime2(): String
		{
			var date: Date = new Date();
			
			var w_hh: String = "00" + date.hours.toString();
			var w_mm: String = "00" + date.minutes.toString();
			var w_ss: String = "00" + date.seconds.toString();
			
			return w_hh.substr(w_hh.length - 2, 2) + ":" + w_mm.substr(w_mm.length - 2, 2) + ":" + w_ss.substr(w_ss.length - 2, 2);
		}
		
		/**********************************************************
		 * 数値計算
		 * @author SE-362 
		 * @param 0: add 1: sub 2: mult 3: div 
		 * @param 小数点桁数
		 * @param 0: 四捨五入 1:切捨て 2:切上げ
		 * @date 12.06.15
		 **********************************************************/
		static public function calcNum(p_val1: String, p_val2: String, p_kbn: int=0, p_digit: int=0, p_roundingmode: int=0): String
		//static public function calcNum(p_val1: String, p_val2: String, p_kbn: int=0, p_digit: int=0): String
		{
			var big1: BigDecimal = new BigDecimal(asCurrency(p_val1));
			var big2: BigDecimal = new BigDecimal(asCurrency(p_val2));
			var big3: BigDecimal;

			//var con: MathContext = new MathContext(p_digit);
			var rmode: int;

			if(p_roundingmode == 1){
				rmode = MathContext.ROUND_DOWN;		//切捨て
			}else if(p_roundingmode == 2){
				rmode = MathContext.ROUND_CEILING;	//切上げ
			}else{
				rmode = MathContext.ROUND_HALF_UP;	//四捨五入
			}
			var con: MathContext = new MathContext(p_digit,0,false,rmode);

			if(nvl(p_val1, "") == "" && nvl(p_val2, "") == ""){
				return "";
			}else{
				if(p_kbn == 0){
					big3 = big1.add(big2);	
				}else if(p_kbn == 1){
					big3 = big1.subtract(big2);
				}else if(p_kbn == 2){
					big3 = big1.multiply(big2);
				}else if(p_kbn == 3){
					big3 = big1.divide(big2,con).setScale(p_digit,rmode);
				}
			}
			
			return big3.numberValue().toString();
		}

		/**********************************************************
		 * 帳票用 得意先名1,2取得
		 * @author SE-362 
		 * @param str: 文字 
		 * @date 12.06.15
		 **********************************************************/
		static public function getTKNM(str: String): Object
		{
			var rtnObject: Object = new Object();
			var byteArray: ByteArray = new ByteArray();
			
			byteArray.writeMultiByte(str, "euc-jp");
			
			var byteLen: int = byteArray.length; 
			var charLen: int = str.length;
			var tmpLen: int = 0;
			var tmptknm: String = "";
			var w_tknm1: String = "";
			var w_tknm2: String = "";
			
			rtnObject.tknm1 = "";
			rtnObject.tknm2 = "";

			for(var i:int=0; i<byteLen; i++){
				tmptknm = str.slice(0, i);
				byteArray = new ByteArray();
				byteArray.writeMultiByte(tmptknm, "euc-jp");
				tmpLen = byteArray.length;
				if(tmpLen <= 20){
					w_tknm1 = tmptknm;
				}else{
					w_tknm2 = str.slice(i-1, str.length);
					break;
				}
			}

			// 12.10.19 SE-233 Redmine#3513対応 start
			if(tmpLen <= 20){
				w_tknm1 = w_tknm1 + "　様";
			}else{
				w_tknm2 = w_tknm2 + "　様";
			}
			// 12.10.19 SE-233 Redmine#3513対応 end

			rtnObject.tknm1 = w_tknm1;
			rtnObject.tknm2 = w_tknm2;
			
			return rtnObject;
		}

		/**********************************************************
		 * 数値入力チェック
		 * @author SE-233 
		 * @param p_num: 数値 
		 * @param p_mode: 0:整数　1:小数点以下あり 
		 * @return 数値以外は０を返す
		 * @date 12.11.01
		 **********************************************************/
		static public function NumChk(p_num: String, p_mode: int=0): String
		{
			var w_num: String;
			
			// 数値以外は０に変換
			if (isNaN(Number(p_num.replace(",","")))){
				w_num = "0";
			}else{
				w_num = nvl(p_num.replace(",",""),"0");
			}
			
			if(p_mode==0){
				// 小数点以下切捨て
				w_num = Math.floor(Number(w_num)).toString();
			}
			return w_num;
		}

		/**********************************************************
		 * 日付入力チェック
		 * @author SE-233 
		 * @param p_dat: 入力日付
		 * @return 日付以外はfalseを返す
		 * @date 14.06.16
		 **********************************************************/
		static public function DateChk(p_dat: String): Boolean
		{
			var rtn:Boolean = true;
			var w_dat:DateValidator = new DateValidator();
			var result:Array = new Array();
			
			w_dat.inputFormat = "yyyy/mm/dd";
			w_dat.validateAsString = true;
			// validateDate静的関数を使用する
			result = DateValidator.validateDate(w_dat, p_dat, p_dat);
			
			// エラーがないか判断する
			if(0 < result.length) {
				rtn = false;
			}
			return rtn;			
		}
	}
}