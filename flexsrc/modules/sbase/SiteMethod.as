package modules.sbase
{
	import modules.base.BasedMethod;
	
	import mx.core.UIComponent;
	
	/**********************************************************
	 * サイト共通のチェック処理を記述.
	 * @author SE-362 
	 * @date 12.07.02
	 **********************************************************/
	public class SiteMethod extends BasedMethod
	{
		// 共通
		private var _Check_HISSU_args: Object;
		private var _Check_DAISHOU_args: Object;
		
		// 関数返り値
		public var Check_HISSU_ret: Object;
		public var Check_DAISHOU_ret: Object;

		/**********************************************************
		 * コンストラクタ
		 * @author SE-362 
		 * @date 12.07.02
		 **********************************************************/
		public function SiteMethod()
		{
			super();
		}

		/**********************************************************
		 * 必須チェック
		 * @param p_val   : チェックする値
		 * @param p_mflg  : メッセージ表示フラグ  "1":メッセージを表示する
		 * @param          (省略可の項目)
		 * @param p_obj   : チェックするコンポーネント名
		 * @param p_str   : 項目名
		 * @param nextfunc: 次に実行したい関数
		 * @author SE-362 
		 * @date 12.07.02
		 ***********************************************************/
		public function Check_HISSU(p_val: String, p_mflg: String, 
									p_obj: UIComponent=null, p_str: String="", nextfunc: Function=null): void
		{
			// 引数のセット
			_Check_HISSU_args = new Object;
			_Check_HISSU_args.p_val    = p_val;
			_Check_HISSU_args.p_mflg   = p_mflg;
			_Check_HISSU_args.p_obj    = p_obj;
			_Check_HISSU_args.p_str    = p_str;
			_Check_HISSU_args.nextfunc = nextfunc;
			
			// 戻り値の初期化
			Check_HISSU_ret = new Object;
			Check_HISSU_ret.rtn = false;
			
			// エラーメッセージのクリア
			clearErrMsg(p_obj);
			
			if (nvl(p_val) == "") {
				
				if (p_mflg == "1") {
					setErrMsg("E0004", p_obj);
				}
				
				// 最終関数を実行
				Check_HISSU_last();
				return;
			}
			
			Check_HISSU_ret.rtn = true;
			
			// 最終関数を実行
			Check_HISSU_last();
		}
		
		public function Check_HISSU_last(): void
		{
			// 引数の取得
			var w_arg: Object = _Check_HISSU_args;
			
			// 次関数を実行
			execNextFunction(w_arg.nextfunc);
		}

		/**********************************************************
		 * 大小チェック
		 * @param p_svalue : チェックする値(From)
		 * @param p_evalue : チェックする値(To)
		 * @param p_mflg   : メッセージ表示フラグ  "1":メッセージを表示する
		 * @param          : (省略可の項目)
		 * @param p_sobj   : チェックするコンポーネント名(From)
		 * @param p_str    : 項目名
		 * @param nextfunc : 次に実行したい関数
		 * @param p_eobj   : チェックするコンポーネント名(To)
		 * @param p_numflg : 数値項目かどうかのフラグ
		 * @author SE-362 
		 * @date 12.07.02
		 ***********************************************************/
		public function Check_DAISHOU(p_sval: String, p_eval: String, p_mflg: String, 
									  p_sobj: UIComponent=null, p_str: String="", nextfunc: Function=null,
									  p_eobj: UIComponent=null, p_numflg: Boolean=false): void
		{
			// 引数のセット
			_Check_DAISHOU_args = new Object;
			_Check_DAISHOU_args.p_sval   = p_sval;
			_Check_DAISHOU_args.p_eval   = p_eval;
			_Check_DAISHOU_args.p_mflg   = p_mflg;
			_Check_DAISHOU_args.p_sobj   = p_sobj;
			_Check_DAISHOU_args.p_str    = p_str;
			_Check_DAISHOU_args.nextfunc = nextfunc;
			_Check_DAISHOU_args.p_eobj   = p_eobj;
			
			// 戻り値の初期化
			Check_DAISHOU_ret = new Object;
			Check_DAISHOU_ret.rtn = false;
			
			// エラーメッセージのクリア
			clearErrMsg(p_sobj);
			
			if (p_eobj != null) {
				
				clearErrMsg(p_eobj);
			}
			
			if (nvl(p_sval) != "" && nvl(p_eval) != "") {
				
				// 数値項目のとき
				if (p_numflg == true) {
					
					if (asCurrency(p_sval) > asCurrency(p_eval)) {
						
						if (p_mflg == "1") {
							setErrMsg("E0171", p_sobj);
							
							if (p_eobj != null) {
								
								setErrMsg("E0171", p_eobj);
							}
						}
						
						// 最終関数を実行
						Check_DAISHOU_last();
						return;
					}
					
					// 数値項目以外のとき
				} else {
					
					if (nvl(p_sval) > nvl(p_eval)) {
						
						if (p_mflg == "1") {
							setErrMsg("E0171", p_sobj);
							
							if (p_eobj != null) {
								
								setErrMsg("E0171", p_eobj);
							}
						}
						
						// 最終関数を実行
						Check_DAISHOU_last();
						return;
					}
				}
			}
			
			Check_DAISHOU_ret.rtn = true;
			
			// 最終関数を実行
			Check_DAISHOU_last();			
		}
		
		public function Check_DAISHOU_last(): void
		{
			// 引数の取得
			var w_arg: Object = _Check_DAISHOU_args;
			
			// 次関数を実行
			execNextFunction(w_arg.nextfunc);
		}		
		
	}
}