package modules.sbase
{
	import modules.dto.*;

	[Bindable]
	/**
	 * サイト共通定数
	 * @author SE-354 
	 * @date 12.04.24 
	 */
	public class SiteConst
	{

		//==============================================================================
		// 定数宣言
		//==============================================================================
		
// 12.09.26 SE-362 布村薬品対応 start
//		public static const G_VERSION_URL: String	 = "http://128.31.200.210:8400/estore/fxny/version.xml"; // バージョン確認URL
//		public static const G_VERSION_URL: String	 = "http://10.5.1.2:8400/estore/fxny/version.xml"; // バージョン確認URL
		public static const G_VERSION_URL: String	 = "http://128.7.60.101:8400/estore/fxny/version.xml"; // バージョン確認URL
		public static const G_IMAGE_PATH: String	 = "/IMAGE";		// 画像保存パス
		public static const G_IMAGE_PATH2: String	 = "IMAGE/";		// 画像参照パス
// 12.09.26 SE-362 布村薬品対応 end
		public static const G_SELECTED_COLOR: String		= "#AA8800";	// 選択中明細色
		public static const G_UNSELECTED_COLOR: String	= "#444444";	// 非選択明細色

		// リソース関連
		public static const G_BUNDLE_MSG: String		 = "resource_msg";				// エラー名称
		public static const G_BUNDLE_DCT: String		 = "resource_dct";				// 項目辞書
		public static const G_BUNDLE_PRG: String		 = "resource_prg";				// プログラム名称

// 11.05.12 リソースをXML化 SE-354 start
		public static const G_BUNDLE_MSG_URL: String		 = "resource_msg_url";		// エラーメッセージ辞書のURL
		public static const G_BUNDLE_DCT_URL: String		 = "resource_dct_url";		// 項目辞書のURL
		public static const G_BUNDLE_PRG_URL: String		 = "resource_prg_url";		// プログラム名称のURL
// 11.05.12 リソースをXML化 SE-354 end

// 12.06.18 SE-362 start
		public static const G_MSG_FILE: String			= "xml/msg.xml";
		public static const G_QUERY_FILE: String			= "xml/query.xml";
		public static const G_DATABASE_FILE: String		= "database/FXNY.sqlite";
// 12.06.18 SE-362 end		
// 10.10.21 AD連携有無、辞書使用有無のフラグ化 SE-354 start
		public static const G_DICTFLG: String		= "1";			// 辞書使用有無のフラグ(0:使用しない、1:使用する)
		public static const G_ADFLG: String		= "1";			// AD連携有無のフラグ(0:使用しない、1:使用する)		
// 10.10.21 AD連携有無、辞書使用有無のフラグ化 SE-354 end

		// 日付フォーマット
		public static const G_FULLYEAR: String     = "1";						// DateFieldフォーマット種別
																				// 0:YY/MM/DD、1:YYYY/MM/DD
																				// ※G_DATEFMTの値とあわせること！
		
		public static const G_DATEFMT: String      = "YY/MM/DD";				// 日付項目フォーマット
		public static const G_DATEFMT2: String     = "YYYYMMDD";				// 日付項目フォーマット(区切り記号なし)
		public static const G_DATETIMEFMT: String  = "YY/MM/DD JJ:NN:SS";	// 日時項目フォーマット
		public static const G_DATETIMEFMT2: String = "YYYYMMDDJJNNSS";		// 日時項目フォーマット(区切り記号なし)
		public static const G_DATETIMEFMT3: String = "YYYY/MM/DD JJ:NN:SS";	// 日時項目フォーマット
		public static const G_YEARMONTHFMT: String = "YY/MM";			// 日付項目フォーマット
		public static const G_YEARMONTHFMT2: String = "YYYYMM";			// 日付項目フォーマット
		public static const G_YEARMONTHFMT3: String = "YYYY/MM";			// 日付項目フォーマット
// 10.08.24 SE-354 カレンダマスタ用に追加 start		
		public static const G_YEARFMT: String		 = "YYYY";				// 日付項目フォーマット
		public static const G_MONTHFMT: String	 = "MM";				// 日付項目フォーマット
		public static const G_DAYFMT: String	 	 = "D";					// 日付項目フォーマット
		public static const G_DAYFMT2: String	 	 = "DD";					// 日付項目フォーマット
// 10.08.24 SE-354 カレンダマスタ用に追加 end			
		
		public static const G_JAVADATEFMT: String  = "YYYY-MM-DD JJ:NN:SS";// Java引数用日時項目フォーマット (※変更不可)
		public static const G_FULLDATEFMT: String  = "YYYY/MM/DD";			// フル桁日付項目フォーマット     (※変更不可)
// 12.09.27 SE-354 start
		public static const G_TIMEFMT: String  = "JJ:NN:SS";				// 時間フォーマット
// 12.09.27 SE-354 end

		// 処理区分
		public static const G_SRCLS_UA010X: int   = 1;			// 受注入力
		public static const G_SRCLS_UA080X: int   = 2;			// 直送入力
		public static const G_SRCLS_UA180X: int   = 4;			// 工事売上入力
		public static const G_SRCLS_UA050X: int   = 6;			// 出荷入力
		public static const G_SRCLS_UA070X: int   = 7;			// 出荷確認入力
		public static const G_SRCLS_UA100X: int   = 8;			// 売上単価入力
		public static const G_SRCLS_UA260X: int   = 9;			// 工事完了入力
		public static const G_SRCLS_UA230X: int   = 11;			// 支給材引落入力
		public static const G_SRCLS_UA200X: int   = 12;			// 在庫支給材入力
		public static const G_SRCLS_UA120X: int   = 13;			// 倉庫間移動入力
		public static const G_SRCLS_UA130X: int   = 14;			// 振替入力
		public static const G_SRCLS_UA240X: int   = 15;			// 直送支給・引落入力
		public static const G_SRCLS_UA210X: int   = 16;			// 在庫支給・引落入力
		public static const G_SRCLS_UA300X: int   = 17;			// 工事振替入力
		public static const G_SRCLS_UA310X: int   = 18;			// 工事振替入力（直送）
		public static const G_SRCLS_UA090X: int   = 20;			// 出荷返品入力
		public static const G_SRCLS_UA270X: int   = 21;			// 入金入力
		public static const G_SRCLS_UA280X: int   = 21;			// 入金入力（消込）
		public static const G_SRCLS_UA290X: int   = 22;			// 入金（引当）入力
		public static const G_SRCLS_UA410X: int   = 23;			// 受取手形入力
		public static const G_SRCLS_UA420X: int   = 24;			// 受取手形移動入力
		public static const G_SRCLS_UA020X: int   = 31;			// 受発注入力
		public static const G_SRCLS_UA030X: int   = 32;			// 発注入力
		public static const G_SRCLS_UA170X: int   = 34;			// 工事受注入力
		public static const G_SRCLS_UA190X: int   = 35;			// 物件部材発注入力
		public static const G_SRCLS_UA370X: int   = 42;			// 支払入力
		public static const G_SRCLS_UA380X: int   = 42;			// 支払入力（消込）
		public static const G_SRCLS_UA430X: int   = 43;			// 支払手形入力
		public static const G_SRCLS_UA040X: int   = 51;			// 入荷入力
		public static const G_SRCLS_UA250X: int   = 53;			// 外注加工費入力
		public static const G_SRCLS_UA220X: int   = 54;			// 支給材仕入単価入力
		public static const G_SRCLS_UA510X: int   = 71;			// 見積入力
		public static const G_SRCLS_UA110X: int   = 91;			// 仕入単価入力
		public static const G_SRCLS_UA140X: int   = 81;			// 棚卸入力
		public static const G_SRCLS_UA150X: int   = 63;			// セット品組立入力
		public static const G_SRCLS_UI304X: int   = 1;			// 請求書
		// 江守塗料対応
		public static const G_SRCLS_UA550X: int   = 25;			// 出荷指示入力

		// ゼロ埋め項目桁数
		public static const G_VSEQ_LENGTH: int    = 6;			// 伝票NO桁数
		public static const G_CUCD_LENGTH: int    = 6;			// 得意先桁数
		public static const G_MVSEQ_LENGTH: int   = 6;			// 見積NO桁数
		public static const G_BUVSEQ_LENGTH:int   = 6;			// 物件番号桁数
		public static const G_BUVLIN_LENGTH:int   = 2;			// 物件番号子桁数
		public static const G_BILNO1_FULL_LENGTH:int = 10;    	// 手形番号桁数（受取手形番号用）
		public static const G_BILNO1_AUTO_LENGTH:int = 6;     	// 手形番号桁数（支払手形番号用）

		// その他
		public static const G_JPY: String        = "JPY";		// 日本円の単位コード
		
		// 省略時、初期値
		public static const G_MAXBYTE: String     = "100";		// 最大入力バイト数
		public static const G_INTNUM: String      = "10";		// 整数部桁数
		public static const G_REALNUM: String     = "2";			// 小数部桁数
		public static const G_ROUND: String       = "3";			// 丸め (0:四捨五入なし,1:切捨て,2:切上げ,3:四捨五入)
		public static const G_COMMA: String       = "1";			// カンマ (0:カンマなし,1:カンマあり)

		// プログラム起動関連
		public static const G_SCREEN_WIDTH: Number  = 1024;		// 画面幅
		public static const G_SCREEN_HEIGHT: Number = 768;		// 画面高さ
		public static const G_TIMER_DELAY: Number   = 1000;		// Timer遅延間隔
		public static const G_TIMER_MAXCNT: Number  = 30;			// Timer最大実行回数
		public static const G_DBG_CHGCD: String     = "EMORI";	// デバッグ時担当者コード
	
		// エラーメッセージ(補助文字列)
		public static const G_ERRMSG1:  Array = ["出荷済", "言語2_出荷済", "言語3_出荷済"];
		public static const G_ERRMSG2:  Array = ["入荷済", "言語2_入荷済", "言語3_入荷済"];
		public static const G_ERRMSG3:  Array = [" と ", "言語2_ と ", "言語3_ と "];
		public static const G_ERRMSG4:  Array = ["整数", "言語2_整数", "言語3_整数"];
		public static const G_ERRMSG5:  Array = ["小数", "言語2_小数", "言語3_小数"];
		public static const G_ERRMSG6:  Array = ["桁", "言語2_桁", "言語3_桁"];
		public static const G_ERRMSG7:  Array = ["参照指定時、", "言語2_参照指定時、", "言語3_参照指定時、"];
		public static const G_ERRMSG8:  Array = ["(型不正)", "言語2_(型不正)", "言語3_(型不正)"];
		public static const G_ERRMSG9:  Array = ["メッセージマスタに存在しません。", "言語2_メッセージマスタに存在しません。", "言語3_メッセージマスタに存在しません。"];
		public static const G_ERRMSG10: Array = ["在庫管理商品", "言語2_在庫管理商品", "言語3_在庫管理商品"];
		public static const G_ERRMSG11: Array = ["単価未決", "言語2_単価未決", "言語3_単価未決"];
		public static const G_ERRMSG12: Array = ["を", "言語2_を", "言語3_を"];
		public static const G_ERRMSG13: Array = ["起動に失敗しました", "言語2_起動に失敗しました", "言語3_起動に失敗しました"];
		public static const G_ERRMSG14: Array = ["業務を終了します。よろしいですか？", "言語2_業務を終了します。よろしいですか？", "言語3_業務を終了します。よろしいですか？"];
		public static const G_ERRMSG15: Array = ["または", "言語2_または", "言語3_または"];
		public static const G_ERRMSG16: Array = ["キャンセルしてよろしいですか？", "言語2_キャンセルしてよろしいですか？", "言語3_キャンセルしてよろしいですか？"];
		public static const G_ERRMSG17: Array = ["削除してよろしいですか？", "言語2_削除してよろしいですか？", "言語3_削除してよろしいですか？"];

// 10.03.31 by SE-254 start
		public static const G_ERRMSG18: Array = ["生産システムに接続出来ません。オンライン停止中かネットワークが切断されております。", 
												   "言語2_生産システムに接続出来ません。オンライン停止中かネットワークが切断されております。", 
												   "言語3_生産システムに接続出来ません。オンライン停止中かネットワークが切断されております。"];
		public static const G_ERRMSG19: Array = ["生産システムのオンライン停止中です。;業務終了を行って下さい。", 
												   "言語2_生産システムのオンライン停止中です。;業務終了を行って下さい。", 
												   "言語3_生産システムのオンライン停止中です。;業務終了を行って下さい。"];
		public static const G_ERRMSG20: Array = ["権限がありません。", "言語2_権限がありません。", "言語3_権限がありません。"];
		public static const G_ERRMSG21: Array = ["使用可能なメニューがありません。起動できません。", 
												   "言語2_使用可能なメニューがありません。起動できません。", 
												   "言語3_使用可能なメニューがありません。起動できません。"];
// 10.03.31 by SE-254 end
// 10.07.02 SE-354 start
		public static const G_ERRMSG22: Array = ["、", "言語2_、", "言語3_、"];
// 10.07.02 SE-354 end

		// エラーメッセージ(補助文字列)江守関連対応
		public static const G_ERRMSG101: Array = ["業務日", "言語2_業務日", "言語3_業務日"];
	
		// ボタンラベル
		// 画面ボタンデフォルト値
		public static const G_F8DFT: Array  = ["ｷｬﾝｾﾙ", "言語2_ｷｬﾝｾﾙ", "言語3_ｷｬﾝｾﾙ"];
		public static const G_F10DFT: Array = ["確定", "言語2_確定", "言語3_確定"];
		public static const G_F12DFT: Array = ["戻る", "言語2_戻る", "言語3_戻る"];
		
		// コンテキストメニュー文字列
		public static const G_CNXTMENU1: String = "PrintScreen";
// 11.06.02 キャプチャ機能追加 SE-354 start
		public static const G_CNXTMENU2: String = "Capture";
// 11.06.02 キャプチャ機能追加 SE-354 end
		public static const G_CNXTMENU3: String = "FlashPlayerVersion";
		
		// ポップアップタイトル
		public static const G_POPUP001A: Array = ["取引先検索", "言語2_取引先検索", "言語3_取引先検索"];
		
		public static const G_POPUP003A: Array = ["受注伝票検索", "言語2_受注伝票検索", "言語3_受注伝票検索"];
		public static const G_POPUP003B: Array = ["発注伝票検索", "言語2_発注伝票検索", "言語3_発注伝票検索"];
		public static const G_POPUP003C: Array = ["出荷伝票検索", "言語2_出荷伝票検索", "言語3_出荷伝票検索"];
		public static const G_POPUP003D: Array = ["入荷伝票検索", "言語2_入荷伝票検索", "言語3_入荷伝票検索"];
		public static const G_POPUP003E: Array = ["出庫伝票検索", "言語2_出庫伝票検索", "言語3_出庫伝票検索"];
		public static const G_POPUP003E_8: Array = ["セット品組立検索", "言語2_セット品組立検索", "言語3_セット品組立検索"];
		public static const G_POPUP003F: Array = ["入金伝票検索", "言語2_入金伝票検索", "言語3_入金伝票検索"];
		public static const G_POPUP003G: Array = ["支払伝票検索", "言語2_支払伝票検索", "言語3_支払伝票検索"];
		public static const G_POPUP003H: Array = ["見積伝票検索", "言語2_見積伝票検索", "言語3_見積伝票検索"];
		public static const G_POPUP003I: Array = ["物件番号検索", "言語2_物件番号検索", "言語3_物件番号検索"];
		
		// 10.07.02 SE-354 start
		public static const G_UA020X1: Array   = ["発注番号", "言語2_発注番号", "言語3_発注番号"];
		// 10.07.02 SE-354 end

		public static const G_UA060X1: Array   = ["ロット引当明細入力", "言語2_ロット引当明細入力", "言語3_ロット引当明細入力"];
		public static const G_UA060X2: Array   = ["ロット払出明細入力", "言語2_ロット払出明細入力", "言語3_ロット払出明細入力"];
		public static const G_UA061X1: Array   = ["ロット払出明細入力", "言語2_ロット払出明細入力", "言語3_ロット払出明細入力"];
		
		public static const G_UB002X1: Array   = ["在庫管理品の時は、諸口", "言語2_在庫管理品の時は、諸口", "言語3_在庫管理品の時は、諸口"];
		public static const G_UB002X2: Array   = ["個別原価法の時は、ﾛｯﾄ管理以外", "言語2_個別原価法の時は、ﾛｯﾄ管理以外", "言語3_個別原価法の時は、ﾛｯﾄ管理以外"];
		public static const G_UB002X3: Array   = ["在庫管理品の時は、員数・数量計算以外", "言語2_在庫管理品の時は、員数・数量計算以外", "言語3_在庫管理品の時は、員数・数量計算以外"];
		public static const G_UB002X4: Array   = ["在庫管理品以外の時は、ﾛｯﾄ管理有", "言語2_在庫管理品の以外の時は、ﾛｯﾄ管理有", "言語3_在庫管理品以外の時は、ﾛｯﾄ管理有"];
		public static const G_UB002X5: Array   = ["在庫管理品以外の時は、個別原価法", "言語2_在庫管理品の以外の時は、個別原価法", "言語3_在庫管理品以外の時は、個別原価法"];
		public static const G_UB002X6: Array   = ["数量自動計算時は、入数がゼロ", "言語2_数量自動計算時は、入数がゼロ", "言語3_数量自動計算時は、入数がゼロ"];
		public static const G_UB002X7: Array   = ["NET変動品の時は、員数管理", "言語2_NET変動品の時は、員数管理", "言語3_NET変動品の時は、員数管理"];
		public static const G_UB002X8: Array   = ["NET変動品の時は、ﾛｯﾄ管理有", "言語2_NET変動品の時は、ﾛｯﾄ管理有", "言語3_NET変動品の時は、ﾛｯﾄ管理有"];
		public static const G_UB002X9: Array   = ["NET変動品の時は、数量計算する", "言語2_NET変動品の時は、数量計算する", "言語3_NET変動品の時は、数量計算する"];
				
		public static const G_UB003X1: Array   = ["債権管理先", "言語2_債権管理先", "言語3_債権管理先"];
		public static const G_UB003X2: Array   = ["取引先", "言語2_取引先", "言語3_取引先"];
		public static const G_UB003X3: Array   = ["得意先", "言語2_得意先", "言語3_得意先"];
		public static const G_UB003X4: Array   = ["仕入先", "言語2_仕入先", "言語3_仕入先"];
		public static const G_UB003X5: Array   = ["その他", "言語2_その他", "言語3_その他"];
		public static const G_UB003X6: Array   = ["請求管理", "言語2_請求管理", "言語3_請求管理"];
		public static const G_UB003X7: Array   = ["支払管理", "言語2_支払管理", "言語3_支払管理"];
		public static const G_UB003X8: Array   = ["内税の時、この消費税算出単位", "言語2_内税の時、この消費税算出単位", "言語3_内税の時、この消費税算出単位"];
		
		public static const G_UB0035X1: Array   = ["諸口の商品", "言語2_諸口の商品", "言語3_諸口の商品"];
		public static const G_UB0035X2: Array   = ["諸口の取引先", "言語2_諸口の取引先", "言語3_諸口の取引先"];
		
		// 江守関連対応 10.03.11 SE-354 start
		public static const G_UI010X1: Array   = ["再発行", "言語2_再発行", "言語3_再発行"];
		public static const G_UI010X2: Array   = ["業務日", "言語2_業務日", "言語3_業務日"];
		// 江守関連対応 10.03.11 SE-354 end
		
		public static const G_UH036X1: Array   = ["契約単価一括報告書を印刷します。","言語2_契約単価一括報告書を印刷します。","言語3_契約単価一括報告書を印刷します。"];
		public static const G_UH036X2: Array   = ["契約単価処理を実行します。","言語2_契約単価処理を実行します。","言語3_契約単価処理を実行します。"];
		
		public static const G_UH831X1: Array   = ["開始売上日", "言語2_開始売上日", "言語3_開始売上日"];
		public static const G_UH831X2: Array   = ["終了売上日", "言語2_終了売上日", "言語3_終了売上日"];
				
		public static const G_UH885X1: Array   = ["処理対象データ", "言語2_処理対象データ", "言語3_処理対象データ"];
		
		public static const G_XB099X1: Array   = ["処理済", "言語2_処理済", "言語3_処理済"];
		public static const G_XB099X2: Array   = ["処理中", "言語2_処理中", "言語3_処理中"];
		
		// CSV・PDF生成関連
		public static const G_CSVMSG1: Array = ["CSVを生成しました。", "言語2_CSVを生成しました。", "言語3_CSVを生成しました。"];
		public static const G_PDFMSG1: Array = ["PDFを作成しました。", "言語2_PDFを作成しました。", "言語3_PDFを作成しました。"];
		
		// バッチ共通
		public static const G_BATMSG1: Array = ["実行中・・・", "言語2_実行中・・・", "言語3_実行中・・・"];		
		
		// 与信チ限度額チェック対象外
		public static const G_YSZANMSG: Array = ["<< ﾁｪｯｸ対象外 >>", "言語2_<< ﾁｪｯｸ対象外 >>", "言語3_<< ﾁｪｯｸ対象外 >>"];		

		// 行コピー対象外項目情報
		public static const G_NO_LINE_CPY: Array = ["_DFULL", "_D", "_DTFULL", "_COMMA", "_ERRORSTRING", "_PLAIN"];
		
		// 項目制御関連情報
		public static const G_EDITITEM: Array    = ["chk", "lcmb", "edt"];
		public static const G_NOTNULLITEM: Array = ["Must"]; 							
		public static const G_NUMITEM: Array     = ["Num"];						 
		public static const G_NO_ENABLED: Array  = ["HeaderLabel", "HeaderLabelPK", "BodyLabel", "DisplayItem", "LabelItem", "DisplayItemNum",
												 	  "pnlTTL", "pnlTITLE", "pnlCHGCD", "pnlNOW", "edtVLIN", "UpperLabel", "UpperLabelPK", "DiffStatusLabel", "NowStatusLabel"];
		public static const G_NO_TAB: Array      = ["HeaderLabel", "HeaderLabelPK", "BodyLabel", "DisplayItem", "LabelItem", "DisplayItemNum",
													  "pnlTTL", "pnlTITLE", "pnlCHGCD", "pnlNOW", "edtVLIN", "UpperLabel", "UpperLabelPK", "DiffStatusLabel", "NowStatusLabel"];
													  
		public static const G_NUM_RESTRICT: String  = "0-9\\-\\.";
		public static const G_STYLE_INPUT: String   = "InputItem";
		public static const G_STYLE_NUM: String     = "Num";
		public static const G_STYLE_MUST: String    = "Must";
		public static const G_STYLE_FOCUSIN: String = "FocusIn";
		
		public static const G_FORMAT_DFULL: String = "_DFULL";
		public static const G_FORMAT_PLAIN: String = "_PLAIN";
		
		// 実行フォルダ情報
		public static const G_BIN_FOLDER: Array    = ["/bin/", "/bin-debug/", "/bin-release/"];
		public static const G_MESSAGEBROKER: Array = ["/messagebroker/"];
		public static const G_DEBUG_URL: String    = "DEBUG";
		
		// ProgressBar情報
		public static const G_NOW_LOADING: String = "Now Loading...";
		public static const G_EXECUTION: String   = "be in Execution";
		 
// アクセスログ対応 10.03.09 by SE-254 start
		// アクセスログ情報
		public static const G_ACLOG_CSV: String = "CSV出力";
// アクセスログ対応 10.03.09 by SE-254 end

// 10.03.11 SE-354 start
		public static const G_SKBN1NM: String = "現在";
		public static const G_SKBN2NM: String = "過去";
// 10.03.11 SE-354 end
		 
// ｺﾏﾂ対応 11.04.29 by SE-254 start
		// CustomText部ﾃﾞﾌｫﾙﾄ文字列
		public static const G_CUSTOMTEXT_BR: String = "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n";
// ｺﾏﾂ対応 11.04.29 by SE-254 end

// Redmine#1943 11.12.15 by SE-254 start
		// 条件保存情報を削除する一覧画面のprgid
// 12.02.13 SE-362 金太対応 start
		public static const G_PRGID_CLEAR: Array = [
											"UF120X", "UF080X", "UF070X", "UF500X",
											"UF510X", "UF520X", "UF530X", "UF540X"
// 12.04.03 SE-362 start
											,"UF550X","UF560X","UF570X"
// 12.04.03 SE-362 end
												];
// 12.02.13 SE-362 金太対応 end
// Redmine#1943 11.12.15 by SE-254 end
		
// Redmine#1977 11.12.15 by SE-254 start
		// 文字バイト数(getByteLenにて使用)
		public static const G_BYTE_HANKAKU: int = 1;			// 半角英数
		public static const G_BYTE_HANKANA: int = 3;			// 半角カナ
		public static const G_BYTE_ELSE: int = 3;				// その他(全角など)
// Redmine#1977 11.12.15 by SE-254 end
		 
		//==============================================================================
		// getter ･･･ MXMLより定数を取得
		//==============================================================================
			
		// (日付項目フォーマット)
		public function getDatefmt(): String
		{
			return G_DATEFMT;
		}

		public function getFullDatefmt(): String
		{
			return G_FULLDATEFMT;
		}
				
	}
}
															
