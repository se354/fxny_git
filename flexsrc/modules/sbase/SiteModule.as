package modules.sbase
{
	import flash.data.SQLResult;
	import flash.data.SQLStatement;
	import flash.events.SQLErrorEvent;
	import flash.events.SQLEvent;
	
	import modules.base.BasedMethod;
	import modules.base.BasedModule;
	import modules.custom.CustomSingleton;
	import modules.dto.LF013;
	import modules.dto.LM007;
	import modules.dto.LM010;
	import modules.dto.LS001;
	import modules.dto.LS006;
	import modules.dto.LS014;
	import modules.formatters.CustomDateFormatter;
	import modules.formatters.CustomNumberFormatter;
	
	import mx.collections.ArrayCollection;
	
	/**********************************************************
	 * サイト共通のデータ取得メソッドや、SQLiteのメソッドを記述.
	 * @author SE-354 
	 * @date 12.06.12
	 **********************************************************/
	public class SiteModule extends BasedModule
	{
		// データ取得用配列
		public var arrSEIGYO: ArrayCollection;
		public var arrCHGCD2: ArrayCollection;
		public var arrTAX: ArrayCollection;
		public var arrERA: ArrayCollection;
		public var arrTORI: ArrayCollection;
		
		public var getTax_ret: Object;
		public var getEra_ret: Object;
		
		// 引数情報
		private var cdsSEIGYO_arg: Object;
		private var cdsCHGCD_arg: Object;
		private var cdsTAX_arg: Object;
		private var getTax_arg: Object;
		private var cdsERA_arg: Object;
		private var getEra_arg: Object;
		private var creLF013_arg: Object;
		private var updTNO_arg: Object;
		private var cdsTORI_arg: Object;
		
		public function SiteModule()
		{
			super();
		}
		
		override public function init():void
		{
			super.init();
			
			singleton.sitemodule = new SiteModule();
		}
		
		/**********************************************************
		 * 制御マスタ取得
		 * @author 12.09.06 SE-362
		 **********************************************************/
		public function cdsSEIGYO(func: Function=null): void
		{
			// 引数情報
			cdsSEIGYO_arg = new Object;
			cdsSEIGYO_arg.func = func;
			
			Stmt = new SQLStatement();
			Stmt.sqlConnection =  singleton.sqlconnection;
			Stmt.itemClass = LS001;
			Stmt.text= singleton.g_query_xml.entry.(@key == "qrySEIGYO");
			
			
			//イベント登録
			Stmt.addEventListener(SQLEvent.RESULT, cdsSEIGYOResult, false, 0, true);
			Stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler, false, 0, true);
			
			//実行
			Stmt.execute();
		}
		
		private function cdsSEIGYOResult(event: SQLEvent): void
		{
			var w_arg: Object = cdsSEIGYO_arg;
			var result:SQLResult = Stmt.getResult();
			
			//arrSEIGYO = new ArrayCollection();
			//arrSEIGYO.addItem(new LS001());
			arrSEIGYO = new ArrayCollection(result.data);
			
			Stmt.removeEventListener(SQLEvent.RESULT, cdsSEIGYOResult);				
			Stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);				
			Stmt=null;
			
			// 次関数実行
			if(w_arg.func)
				w_arg.func();
		}
		
		/**********************************************************
		 * 担当マスタ取得
		 * @author 12.09.06 SE-362
		 **********************************************************/
		public function cdsCHGCD2(p_chgcd: String, func: Function=null): void
		{
			// 引数情報
			cdsCHGCD_arg = new Object;
			cdsCHGCD_arg.func = func;
			
			Stmt = new SQLStatement();
			Stmt.sqlConnection =  singleton.sqlconnection;
			Stmt.itemClass = LM007;
			Stmt.text= singleton.g_query_xml.entry.(@key == "qryCHGCD2");
			Stmt.parameters[0] = p_chgcd;
			
			//イベント登録
			Stmt.addEventListener(SQLEvent.RESULT, cdsCHGCDResult, false, 0, true);
			Stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler, false, 0, true);
			
			//実行
			Stmt.execute();
		}
		
		private function cdsCHGCDResult(event: SQLEvent): void
		{
			var w_arg: Object = cdsCHGCD_arg;
			var result:SQLResult = Stmt.getResult();
			
			//arrCHGCD = new ArrayCollection();
			//arrCHGCD.addItem(new LS001());
			arrCHGCD2 = new ArrayCollection(result.data);
			
			// 次関数実行
			if(w_arg.func)
				w_arg.func();
		}
		
		/**********************************************************
		 * 税率取得
		 * @author SE-362 
		 * @date 12.07.02
		 **********************************************************/
		public function getTax(date: String, func: Function=null): void
		{
			// 引数セット
			getTax_arg = new Object;
			getTax_arg.func = func;
			getTax_arg.date = date;
			
			cdsTAX(getTax_last);		
		}
		
		private function getTax_last(): void
		{
			var w_arg: Object = getTax_arg;
			var w_parcent: String = "100";
			
			getTax_ret = new Object;
			getTax_ret.tax = "0";
			
			if(arrTAX != null && arrTAX.length != 0){
				if(w_arg.date < arrTAX[0].KJNDY || BasedMethod.nvl(arrTAX[0].KJNDY) == ""){
					// 改訂前
					getTax_ret.tax = BasedMethod.calcNum(arrTAX[0].TAXRT, w_parcent, 2); 
				}else{
					// 改訂後
					getTax_ret.tax = BasedMethod.calcNum(arrTAX[0].TAXRTN, w_parcent, 2);
				}
			}
			
			BasedMethod.execNextFunction(w_arg.func);
		}
		
		/**********************************************************
		 * 税マスタ
		 * @author SE-362 
		 * @date 12.07.02
		 **********************************************************/
		public function cdsTAX(func: Function=null): void
		{
			// 引数セット
			cdsTAX_arg = new Object;
			cdsTAX_arg.func = func;
			
			Stmt = new SQLStatement();
			Stmt.sqlConnection =  singleton.sqlconnection;
			Stmt.itemClass = LS006;
			Stmt.text= singleton.g_query_xml.entry.(@key == "qryTAX");
			
			//イベント登録
			Stmt.addEventListener(SQLEvent.RESULT, cdsTAXResult, false, 0, true);
			Stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler, false, 0, true);
			
			//実行
			Stmt.execute();
		}
		
		private function cdsTAXResult(event: SQLEvent): void
		{
			var w_arg: Object = cdsTAX_arg;
			var result:SQLResult = Stmt.getResult();
			
			arrTAX = new ArrayCollection(result.data);
			
			// 次関数実行
			BasedMethod.execNextFunction(w_arg.func);
		}
		
		/**********************************************************
		 * 和暦元号取得
		 * @author SE-233 
		 * @date 12.11.06
		 **********************************************************/
		public function getEra(date: String, func: Function=null): void
		{
			// 引数セット
			getEra_arg = new Object;
			getEra_arg.func = func;
			getEra_arg.date = date;
			
			cdsERA(getEra_last);		
		}
		
		private function getEra_last(): void
		{
			var w_arg: Object = getEra_arg;
			var w_dat1: String;
			var w_dat2: String;
			
			getEra_ret = new Object;
			getEra_ret.era = "";		// 元号
			getEra_ret.year = "";		// 年（和暦）
			getEra_ret.dat = "";		// 日付
			
			w_dat1 = w_arg.date.substr(0,4) + w_arg.date.substr(5,2) + w_arg.date.substr(8,2);
			
			if(arrERA != null && arrERA.length != 0){
				for(var i: int=0; i < arrERA.length; i++){
					w_dat2 = arrERA[i].SDAT.substr(0,4) + arrERA[i].SDAT.substr(5,2) + arrERA[i].SDAT.substr(8,2);
					if(w_dat1 >= w_dat2){
						getEra_ret.era = arrERA[i].ERA;
						getEra_ret.year = BasedMethod.calcNum(BasedMethod.calcNum(w_arg.date.substr(0,4), arrERA[i].SDAT.substr(0,4), 1), "1"); 
						getEra_ret.dat = getEra_ret.era + getEra_ret.year + "年"
						if(w_arg.date.substr(5,1)=="0"){
							getEra_ret.dat = getEra_ret.dat + w_arg.date.substr(6,1) + "月"
						}else{
							getEra_ret.dat = getEra_ret.dat + w_arg.date.substr(5,2) + "月"
						}
						if(w_arg.date.substr(8,1)=="0"){
							getEra_ret.dat = getEra_ret.dat + w_arg.date.substr(9,1) + "日"
						}else{
							getEra_ret.dat = getEra_ret.dat + w_arg.date.substr(8,2) + "日"
						}
					}
				}
			}
			
			BasedMethod.execNextFunction(w_arg.func);
		}
		
		/**********************************************************
		 * 和暦年号マスタ
		 * @author SE-233 
		 * @date 12.11.06
		 **********************************************************/
		public function cdsERA(func: Function=null): void
		{
			// 引数セット
			cdsERA_arg = new Object;
			cdsERA_arg.func = func;
			
			Stmt = new SQLStatement();
			Stmt.sqlConnection =  singleton.sqlconnection;
			Stmt.itemClass = LS014;
			Stmt.text= singleton.g_query_xml.entry.(@key == "qryERA");
			
			//イベント登録
			Stmt.addEventListener(SQLEvent.RESULT, cdsERAResult, false, 0, true);
			Stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler, false, 0, true);
			
			//実行
			Stmt.execute();
		}
		
		private function cdsERAResult(event: SQLEvent): void
		{
			var w_arg: Object = cdsERA_arg;
			var result:SQLResult = Stmt.getResult();
			
			arrERA = new ArrayCollection(result.data);
			
			// 次関数実行
			BasedMethod.execNextFunction(w_arg.func);
		}
		
		/**********************************************************
		 * 伝票NO取得処理
		 * @author SE-233 
		 * @date 12.11.20
		 **********************************************************/
		public function getVNO(kbn: String): String
		{
			var w_htid: String;
			var w_vno: int;
			var w_vno1: String;
			
			w_htid = arrSEIGYO[0].HTID;
			if(kbn == "1"){
				w_vno = parseInt(arrSEIGYO[0].HVNO.substr(3, 3));
			}else if(kbn == "2"){
				w_vno = parseInt(arrSEIGYO[0].BVNO.substr(3, 3));
			}else if(kbn == "3"){
				w_vno = parseInt(arrSEIGYO[0].NVNO.substr(3, 3));
			}
			
			if(w_vno == 999){
				w_vno = 1;
			}else{
				w_vno++;
			}
			w_vno1 = "000" + w_vno.toString();
			
			return w_htid + kbn + w_vno1.substr(w_vno1.length - 3, 3);
		}
		
		/**********************************************************
		 * 行動履歴F（存在しない場合は作成）
		 * @author 12.12.19 SE-233
		 **********************************************************/
		public function creLF013(func: Function=null): void
		{
			// 引数セット
			creLF013_arg = new Object;
			creLF013_arg.func = func;
			
			Stmt = new SQLStatement();
			Stmt.sqlConnection =  singleton.sqlconnection;
			Stmt.itemClass = LF013;
			Stmt.text= singleton.g_query_xml.entry.(@key == "creLF013");
			
			//イベント登録
			Stmt.addEventListener(SQLEvent.RESULT, creLF013Result, false, 0, true);
			Stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler, false, 0, true);
			
			//実行
			Stmt.execute();
		}
		
		private function creLF013Result(event: SQLEvent): void
		{	
			var w_arg: Object = creLF013_arg;
			var result:SQLResult = Stmt.getResult();
			
			// 次関数実行
			if(w_arg.func)
				w_arg.func();
		}
		
		/**********************************************************
		 * 行動履歴F追加
		 * @param tkcd  : 得意先コード 
		 * @param com   : コメント 
		 * @param vno   : 伝票NO 
		 * @param func  : 次関数
		 * @author 12.12.19 SE-233
		 **********************************************************/
		public function insLF013(tkcd: String, com: String, vno: String=null, func: Function=null): void
		{
			// 引数セット
			creLF013_arg = new Object;
			creLF013_arg.func = func;
			
			Stmt = new SQLStatement();
			Stmt.sqlConnection =  singleton.sqlconnection;
			Stmt.itemClass = LF013;
			Stmt.text= singleton.g_query_xml.entry.(@key == "insLF013");
			
			// パラメタ情報
			Stmt.parameters[0]   = arrSEIGYO[0].CHGCD;
			Stmt.parameters[1]   = arrSEIGYO[0].SDDAY;
			Stmt.parameters[2]   = BasedMethod.getTime2();
			Stmt.parameters[3]   = tkcd;
			Stmt.parameters[4]   = com;
			Stmt.parameters[5]   = vno;
			if(vno != null) {
				//				Stmt.parameters[6] = arrSEIGYO[0].TNO;
				Stmt.parameters[6] = getTNO();
			}else{				
				Stmt.parameters[6] = "";
			}
			
			//イベント登録
			Stmt.addEventListener(SQLEvent.RESULT, insLF013Result, false, 0, true);
			Stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler, false, 0, true);
			
			//実行
			Stmt.execute();
		}
		
		private function insLF013Result(event: SQLEvent): void
		{	
			var w_arg: Object = creLF013_arg;
			var result:SQLResult = Stmt.getResult();
			
			Stmt.removeEventListener(SQLEvent.RESULT, insLF013Result);				
			Stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);				
			Stmt=null;
			
			// 次関数実行
			if(w_arg.func)
				w_arg.func();
		}
		
		private function stmtErrorHandler(event: SQLErrorEvent): void
		{
			// エラーの表示
			trace("SQLerr");
		}
		
		/**********************************************************
		 * 通しNO取得処理
		 * @author SE-233 
		 * @date 13.03.13
		 **********************************************************/
		public function getTNO(): String
		{
			var w_length: int;
			var w_cut: int = 3;
			var w_rtnStr: String;
			
			// 現在通しNOを取得
			w_rtnStr = arrSEIGYO[0].TNO;
			
			// 桁数取得
			w_length = w_rtnStr.length;
			
			// 3桁未満なら0埋める
			if(w_length < w_cut){
				for(var i:int = w_length; i < w_cut; i++){
					w_rtnStr = "0" + w_rtnStr;
				}
			}else if(w_length > w_cut){
				w_rtnStr = w_rtnStr.substr(w_length - w_cut, w_cut);
			}
			
			return arrSEIGYO[0].HTID + w_rtnStr;
		}
		
		/**********************************************************
		 * 制御マスタ（通しNO）更新
		 * @author 13.03.13 SE-233
		 **********************************************************/
		public function updTNO(func: Function=null): void
		{
			// 引数情報
			updTNO_arg = new Object;
			updTNO_arg.func = func;
			
			Stmt = new SQLStatement();
			Stmt.sqlConnection =  singleton.sqlconnection;
			Stmt.text= singleton.g_query_xml.entry.(@key == "updTNO");
			
			// パラメタ情報
			// 通しNo
			if(arrSEIGYO[0].TNO == "999"){
				Stmt.parameters[0] = "1";
			}else{
				Stmt.parameters[0] = (Number(arrSEIGYO[0].TNO) + 1).toString();
			}
			
			//イベント登録
			Stmt.addEventListener(SQLEvent.RESULT, updTNOResult, false, 0, true);
			Stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler, false, 0, true);
			
			//実行
			Stmt.execute();
		}
		
		public function updTNOResult(event: SQLEvent): void
		{	
			// 制御ﾏｽﾀ情報再取得
			cdsSEIGYO(updTNOResult2);
		}
		
		public function updTNOResult2(): void
		{	
			var w_arg: Object = updTNO_arg;
			var result:SQLResult = Stmt.getResult();
			
			// 次関数実行
			if(w_arg.func)
				w_arg.func();
		}
		
		/**********************************************************
		 * 得意先マスタ取得
		 * @author 13.03.14 SE-233
		 **********************************************************/
		public function cdsTORI(p_tkcd: String, func: Function=null): void
		{
			// 引数情報
			cdsTORI_arg = new Object;
			cdsTORI_arg.func = func;
			
			Stmt = new SQLStatement();
			Stmt.sqlConnection =  singleton.sqlconnection;
			Stmt.itemClass = LM010;
			Stmt.text= singleton.g_query_xml.entry.(@key == "qryTORI");
			Stmt.parameters[0] = p_tkcd;
			
			//イベント登録
			Stmt.addEventListener(SQLEvent.RESULT, cdsTORIResult, false, 0, true);
			Stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler, false, 0, true);
			
			//実行
			Stmt.execute();
		}
		
		private function cdsTORIResult(event: SQLEvent): void
		{
			var w_arg: Object = cdsTORI_arg;
			var result:SQLResult = Stmt.getResult();
			
			arrTORI = new ArrayCollection(result.data);
			
			// 次関数実行
			if(w_arg.func)
				w_arg.func();
		}
	}
}