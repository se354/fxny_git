package modules.custom
{
	import flash.events.Event;
	import flash.events.FocusEvent;
	import flash.events.MouseEvent;
	import flash.text.StageText;
	
	import modules.base.BasedMethod;
	import modules.custom.skins.CustomLabelSkin;
	import modules.formatters.CustomNumberFormatter;
	
	import mx.core.UIComponent;
	import mx.events.FlexEvent;
	
	import spark.components.BorderContainer;
	import spark.components.Button;
	import spark.components.Label;

	// 数値入力項目クラス 12.08.17 SE-362 start
	[Bindable(event="textChange")]
	public class CustomTextInputNumeric extends Button
	{
		public var NextComp: UIComponent;
		public var comma: String = "1";	// カンマ表示フラグ("1":カンマを表示する)
		public var realNum: String;		// 小数桁数(※省略時、ゼロ)
		public var intNum: int;
		public var popFlg:	String = "0";
		
		/**
		 * true時、先頭の0も表示する
		 * @default false
		 */
		public var asString: Boolean = false;
		
		public var use_doublezero: Boolean = true;
		public var use_dot: Boolean = true;
		public var use_minus: Boolean = true;
		
		private var callout: CustomCalloutNumeric;
		private var _text: String;
		private var _numberformatter: CustomNumberFormatter;
		
		public function CustomTextInputNumeric()
		{
			super();

			_numberformatter = new CustomNumberFormatter();
		}
		
		override protected function createChildren(): void
		{
			super.createChildren();

			callout = new CustomCalloutNumeric();
			
			callout.use_doublezero = use_doublezero;
			callout.use_dot = use_dot;
			callout.use_minus = use_minus;
			callout.intNum = intNum;
			
			callout.addEventListener(MouseEvent.CLICK, calClick, false, 0, true);
			callout.addEventListener(Event.CLOSE, calClose, false, 0, true);
			
			addEventListener(MouseEvent.MOUSE_DOWN, down, false, 0, true);
			addEventListener(Event.REMOVED_FROM_STAGE, removed, false, 0, true);
			setStyle("skinClass", CustomLabelSkin);
			
		}
		
		private function down(e: MouseEvent): void
		{
			//var e: MouseEvent = new MouseEvent(MouseEvent.MOUSE_DOWN);
			if(parent){
				if(parent.parent){
					if(parent.parent.parent){
						if(parent.parent.parent is BorderContainer){
						
							parent.parent.parent.dispatchEvent(new Event("NumericDown"));
						}
					}
				}
			}

			if(popFlg != "1"){
				callout.NextComp = NextComp;
				callout.textDefault = text;
				callout.RtnData = "";
				callout.open(this, true);
			}
			
		}

		private function calClick(event: MouseEvent): void
		{
			text = callout.RtnData;
		}

		private function calClose(event: Event): void
		{
			var e: FlexEvent = new FlexEvent(FlexEvent.VALUE_COMMIT);
			
			dispatchEvent(e);	

			if(parent){
				if(parent.parent){
					if(parent.parent.parent){
						if(parent.parent.parent is BorderContainer){
							
							parent.parent.parent.dispatchEvent(new Event("NumericUp"));
						}
					}
				}
			}
		}
		
		public function get text(): String
		{
			return _text;
		}
		
		public function set text(value: String): void
		{
			var w_value: String;

			if(BasedMethod.nvl(value, "") == ""){
				w_value = "0";
			}else{
				w_value = value;
			}
				
			_text = w_value;

			if (asString == false)
			{
				// ラベル表示
				label = _numberformatter.numberFormat(w_value, comma, BasedMethod.nvl(realNum, ""), "0");
			}
			else
			{
				label = w_value;
			}

			// イベント発行
			dispatchEvent(new Event("textChange"));
			
		}
		
		private function removed(e: Event): void
		{
			removeEventListener(MouseEvent.MOUSE_DOWN, down);
			removeEventListener(Event.REMOVED_FROM_STAGE, removed);
		}
	}
}