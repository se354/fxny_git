package modules.custom
{
	import spark.components.View;
	
	/**
	 * ビュークラス
	 * 
	 * @author SE-362 
	 * @date 12.05.01
	 */
	public class CustomView extends View
	{
		/**
		 * コンストラクタ
		 * 
		 * @author SE-362 
		 * @date 12.05.01
		 */
		public function CustomView()
		{
			super();
		}
	}
}