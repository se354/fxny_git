package modules.custom
{
	import flash.events.Event;
	import flash.events.FocusEvent;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.ui.Keyboard;
	
	import mx.collections.IList;
	
	import spark.components.DataGrid;
	import spark.events.ListEvent;
	
	/**
	 * データグリッドクラス
	 * 
	 * @author SE-362 
	 * @date 12.04.26
	 */
	public class CustomDataGrid extends DataGrid
	{
		// Public変数
		public var customComp: String;					// カスタムコンポーネントかどうか
		public var selectDataFunc: Function;			// 明細選択時に実行する関数
		public var lockedGrid: CustomDataGrid;			// 行固定されるデータグリッド
		
		/**
		 * コンストラクタ
		 * 
		 * @author SE-362 
		 * @date 12.04.26
		 */
		public function CustomDataGrid()
		{
			super();
		}

		override protected function partAdded(partName:String, instance:Object):void
		{
			super.partAdded(partName, instance);

			// イベント追加
			if(grid != null){
				grid.addEventListener("invalidateDisplayList", verticalScrollHandler, false, 0, true);
			}
		}

		override protected function partRemoved(partName:String, instance:Object):void
		{
			super.partRemoved(partName, instance);

			// イベント追加
			if(grid != null){
				grid.addEventListener("invalidateDisplayList", verticalScrollHandler, false, 0, true);
			}
		}
		
		/**
		 * 初期設定
		 * 
		 * @author SE-362 
		 * @date 12.04.26
		 */
		override protected function createChildren():void
		{
			super.createChildren();
			
			// 背景色を白色に設定
			this.setStyle("contentBackgroundColor", "#FFFFFF");
			
			// 選択行設定
			this.setStyle("selectionColor", "#A8C6EE");
			
			// マウスオーバー時色設定
			this.setStyle("rollOverColor", "#CEDBEF");

			// ダブルクリックは可にする
			doubleClickEnabled = true;
			
			// 縦横スクロールを出す
			//this.setStyle("horizontalScrollPolicy", "on");
			this.setStyle("verticalScrollPolicy", "on");
			
			// イベント追加
//			this.addEventListener(ListEvent.CHANGE, changeHandler);
			if(lockedGrid != null){
				this.addEventListener(MouseEvent.MOUSE_DOWN, mouseDownHandler, false, 0, true);
			}
		}

		/**
		 * マウス押下時処理
		 * 
		 * @author SE-362 
		 * @date 12.04.26
		 */
		public function mouseDownHandler(event: MouseEvent): void
		{
			// 選択行の同期を合わせる
			if(lockedGrid != null){
				this.validateNow();
				lockedGrid.grid.selectedIndex = this.grid.selectedIndex;
			}
		}
		
		/**
		 * スクロール変更時処理
		 * 
		 * @author SE-362 
		 * @date 12.04.26
		 */
		public function verticalScrollHandler(event: Event): void
		{
			// 固定データグリッドもスクロールさせる
			if(lockedGrid != null && lockedGrid.grid != null){
//				this.validateNow();
				lockedGrid.grid.verticalScrollPosition = this.grid.verticalScrollPosition;
			}
		}
		
		/**
		 * キーダウン時処理
		 * 
		 * @author SE-362 
		 * @date 12.04.26
		 */
		override protected function keyDownHandler(event: KeyboardEvent): void 
		{
			var tabKeyEvent: FocusEvent;
			
			// Enterキー押下時
			if (event.keyCode == Keyboard.ENTER) {
				
				// 次の項目にフォーカスを移す
				tabKeyEvent = new FocusEvent(FocusEvent.KEY_FOCUS_CHANGE,
					true, false, null, event.shiftKey, Keyboard.TAB);
				dispatchEvent(tabKeyEvent);        	
			}
			
			super.keyDownHandler(event);
		}

		/**
		 * dataProvider設定時
		 * 
		 * @author SE-362 
		 * @date 12.04.26
		 */
		override public function set dataProvider(value:IList):void
		{
			super.dataProvider  =value;
			
			// 0件出なければ、1行目を選択状態にする
			if (this.dataProvider){
				if(this.dataProvider.length != 0){
					this.selectedIndex = 0;
				}
			}
		}

		/**
		 * selectedIndex設定時　処理
		 * 
		 * @author SE-362 
		 * @date 12.04.26
		 */
		override public function set selectedIndex(value:int):void
		{
			super.selectedIndex = value;
			
			// 明細が選択されていたら、関数を実行する
			if (this.selectedIndex != -1) {
				
				if (selectDataFunc != null) {
					selectDataFunc();
				}
			}
		}
		
		/**
		 * データ変更時処理
		 * 
		 * @author SE-362 
		 * @date 12.04.26
		 */
		public function changeHandler(event: ListEvent): void
		{
			// selectedIndexをセットする(selectDataFuncを通すため)
			this.selectedIndex = this.selectedIndex;
		}

		/**
		 * データ内アイテム取得
		 * 
		 * @author SE-362 
		 * @date 12.04.26
		 */
		/*		public function getItemRenderer(rowIndex:int, colIndex:int):IListItemRenderer
		{
			var firstItemIndex:int = verticalScrollPosition - offscreenExtraRowsTop;

			return listItems[rowIndex][colIndex];
		}
		*/
		/**
		 * 列移動時処理（モバイルでは列移動不可）
		 * 
		 * @author SE-362 
		 * @date 12.04.26
		 */
		/*		override mx_internal function shiftColumns(oldIndex:int, newIndex:int, trigger:Event = null):void
		{
			if (_BasedMethod.nvl(lockColumn) == "")
			{
				super.shiftColumns(oldIndex,newIndex,trigger);
			}
			else
			{
				if(newIndex <= Number(lockColumn) || oldIndex <= Number(lockColumn))
				{
					// 何もしない
				}
				else
				{
					super.shiftColumns(oldIndex,newIndex,trigger);
				}
			}
		}*/
}
}