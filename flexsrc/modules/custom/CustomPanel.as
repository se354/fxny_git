package modules.custom
{
	import spark.components.Panel;
	
	/**
	 * パネルクラス
	 * 
	 * @author SE-362 
	 * @date 12.05.07
	 */
	public class CustomPanel extends Panel
	{
		/**
		 * コンストラクタ
		 * 
		 * @author SE-362 
		 * @date 12.05.07
		 */
		public function CustomPanel()
		{
			super();
		}
	}
}