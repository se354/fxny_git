package modules.custom.skins
{
	import mx.core.UIComponent;
	
	import spark.components.Button;
	import spark.skins.mobile.CalloutSkin;
	
	// 数値入力用スキン
	// 12.10.10 SE-362 start
	public class CalloutNumericSkin extends CalloutSkin
	{
		public function CalloutNumericSkin()
		{
			super();
			
			// 境界線
//			dropShadowVisible = false;
//			useBackgroundGradient = false;
//			borderColor = 0xFFCC00;
//			borderThickness = 3;
		}
		
		override protected function createChildren():void
		{
			super.createChildren();

			// 矢印の背景色設定
			arrow.setStyle("backgroundColor", "#FFFFFF");
			arrow.setStyle("backgroundAlpha", 1.0);
			arrow.alpha = 1.0;
		}
	}
}