package modules.custom
{
	import flash.data.SQLConnection;
	
	import modules.formatters.CustomDateFormatter;
	import modules.formatters.CustomNumberFormatter;
	import modules.sbase.SiteMethod;
	import modules.sbase.SiteModule;
	
	import mx.collections.ArrayCollection;
	import mx.rpc.remoting.RemoteObject;
	
	
	/**********************************************************
	 * シングルトンクラス.<br>
	 * いつどこでインスタンス化しても同じインスタンスにアクセス可能なクラス。(全画面で値を共有)
	 * 
	 * @author SE-354 
	 * @date 12.04.24
	 **********************************************************/
    public class CustomSingleton
    {
    	private var _g_wsid:String;		// WSID
		
		private var _g_appType:String;		// "0":FLEX, "1":AIR(オンライン), "2":AIR(オフライン)
		
		private var _sitemethod: SiteMethod;
		private var _sitemodule: SiteModule;
		private var _g_customnumberformatter: CustomNumberFormatter = new CustomNumberFormatter();
		private var _g_customdateformatter: CustomDateFormatter = new CustomDateFormatter();
		private var _g_query_xml: XML;
		private var _g_msg_xml: XML;
		private var _g_dct_xml: XML;
		private var _view: Object;
		
		private var _sqlconnection: SQLConnection;

		private var _srv: RemoteObject = new RemoteObject;
// Timezone不具合対応 12.01.19 by SE-254 start
//	private var _g_srv_timezone: int;		// Webサーバのタイムゾーン(ミリ秒)
// Timezone不具合対応 12.01.19 by SE-254 end

        public function CustomSingleton(internally:SingletonInternal)
        {
            super();
            // newでのインスタンス化を抑止
            if(internally == null)
            {
                throw new Error("getInstance()メソッドを使って下さい。");
            }
        }
        public static function getInstance():CustomSingleton
        {
            return SingletonInternal.instance;
        }
        
        public function set g_wsid(value:String):void
        {
        	_g_wsid = value;
        } 
        
        public function get g_wsid():String
        {
        	return _g_wsid; 
        }
		
		public function set g_appType(value:String):void
		{
			_g_appType = value;
		} 
		
		public function get g_appType():String
		{
			return _g_appType; 
		}
        
		public function set sitemethod(value:SiteMethod):void
		{
			_sitemethod = value;
		} 
		
		public function get sitemethod():SiteMethod
		{
			return _sitemethod; 
		}

		public function set sitemodule(value: SiteModule): void
		{
			_sitemodule = value;
		}
		
		public function get sitemodule(): SiteModule
		{
			return _sitemodule;	
		}
		
		public function set srv(value:RemoteObject):void
		{
			_srv = value;
		} 
		
		public function get srv():RemoteObject
		{
			return _srv; 
		}
        
        public function set g_customnumberformatter(value:CustomNumberFormatter):void
        {
        	_g_customnumberformatter = value;
        } 
        
        public function get g_customnumberformatter():CustomNumberFormatter
        {
        	return _g_customnumberformatter; 
        }
        
        public function set g_customdateformatter(value:CustomDateFormatter):void
        {
        	_g_customdateformatter = value;
        } 
        
        public function get g_customdateformatter():CustomDateFormatter
        {
        	return _g_customdateformatter; 
        }
 
		public function set g_query_xml(value: XML):void
		{
			_g_query_xml = value;
		} 
		
		public function get g_query_xml(): XML
		{
			return _g_query_xml; 
		}
		
		public function set g_msg_xml(value: XML):void
		{
			_g_msg_xml = value;
		} 
		
		public function get g_msg_xml(): XML
		{
			return _g_msg_xml; 
		}
		
		public function set sqlconnection(value: SQLConnection): void
		{
			_sqlconnection = value;
		}
		
		public function get sqlconnection(): SQLConnection
		{
			return _sqlconnection;
		}
		
		public function set view(value: Object): void
		{
			_view = value;
		}
		
		public function get view(): Object
		{
			return _view;
		}
	}
}
	import modules.custom.CustomSingleton;
	
	
	
class SingletonInternal{
    public static var instance:CustomSingleton
        = new CustomSingleton(new SingletonInternal());
    public function SingletonInternal(){}
}