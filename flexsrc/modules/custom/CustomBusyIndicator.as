package modules.custom
{
	import flash.events.Event;
	
	import modules.custom.skins.CustomBusyIndicatorSkin;
	
	import spark.components.SkinnablePopUpContainer;
	
	/************************************************************
	 * 待ち画面用クラス
	 * @author SE-362 
	 * @date 12.06.15
	 ************************************************************/
	public class CustomBusyIndicator extends SkinnablePopUpContainer
	{
		public function CustomBusyIndicator()
		{
			super();
			
			setStyle("skinClass", CustomBusyIndicatorSkin);

			addEventListener(Event.ADDED_TO_STAGE, onAddedToStage, false, 0, true);
		}
		
		protected function onAddedToStage(event:Event):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			
			this.width = stage.stageWidth;
			this.height = stage.stageHeight;
		}
		
		override public function close(commit:Boolean = false, data:* = undefined):void
		{
			
			this.width = 0;
			this.height = 0;
			
			super.close();
		}
	}
}