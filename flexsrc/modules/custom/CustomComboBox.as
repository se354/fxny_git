/* Copyright (c) 2011, PIA. All rights reserved.
*
* This file is part of Eskimo.
*
* Eskimo is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Eskimo is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with Eskimo.  If not, see <http://www.gnu.org/licenses/>.
*/
package modules.custom
{
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.ui.Keyboard;
	
	import modules.custom.sub.UniqueChoiceList;
	
	import mx.collections.IList;
	import mx.events.ItemClickEvent;
	import mx.managers.IFocusManagerComponent;
	import mx.managers.PopUpManager;
	
	import spark.components.Label;
	import spark.components.supportClasses.SkinnableComponent;
	
	/**
	 * Skinclass of popup
	 */
	[Style(name = "popupSkinClass", inherit = "no", type = "Class")]
	
	/**
	 *  リストから項目選択時に発生.
	 *
	 *  @eventType mx.events.ItemClickEvent.ITEM_CLICK
	 */
	[Event(name = "itemClick", type = "mx.events.ItemClickEvent")]
	
	/**************************************
	 * コンボボックスクラス.<br>
	 * 例：&lt;custom:CustomComboBox id="lcmbCHGNM" x="650" y="100" width="300" height="33" labelField="CHGNM" itemClick="{ctrl.lcmbCHGNMExit()}" /&gt;
	 * 
	 * 
	 * @author SE-354 
	 * @date 12.08.31
	 **************************************/
	public class CustomComboBox extends SkinnableComponent implements IFocusManagerComponent
	{
		
		/**
		 * @private
		 */
		protected static const POPUP_PADDING_PERCENT:Number = 4;
		
		
		[SkinPart(required = "true")]
		public var selectedLabelDisplay:Label;
		
		/**
		 * @private
		 */
		protected var _dataProvider:IList;
		
		/**
		 * @private
		 */
		protected var _selectedItem:Object;
		
		/**
		 * @private
		 */
		private var _selectedItemChange:Boolean = true;
		
		/**
		 * @private
		 */
		private var _defaultSelectedLabel:String = "";
		
		/**
		 * @private
		 */
		private var _labelField:String = "label";
		
		/**
		 * @private
		 */
		protected var popUp:UniqueChoiceList = new UniqueChoiceList();
		
		// 12.10.03 SE-362 start		
		public var dspField: String;
		// 12.10.03 SE-362 end
		/****************************************
		 * コンストラクタ
		 * 
		 * @author SE-354 
		 * @date 12.09.03
		 ****************************************/
		public function CustomComboBox()
		{
			super();
			this.addEventListener(MouseEvent.CLICK, popUpList, false, 0, true);
		}
		
		public function get labelField():String
		{
			return _labelField;
		}
		
		public function set labelField(value:String):void
		{
			_labelField = value;
		}
		
		public function set dataProvider(value:IList):void
		//public function set dataProvider(value:Object):void
		{
			if (value != _dataProvider)
			{
				_dataProvider = value;
				invalidateProperties();
			}
		}
		
		public function get dataProvider():IList
		//public function get dataProvider():Object
		{
			return _dataProvider;
		}
		
		protected function popUpList(event:MouseEvent):void
		{
			focusManager.setFocus(this);
			var popupSkinClass:Object = getStyle("popupSkinClass");
			if (popupSkinClass != null)
			{
				popUp.setStyle("skinClass", popupSkinClass);
			}
			popUp.dataProvider = dataProvider;
			popUp.addEventListener(ItemClickEvent.ITEM_CLICK, onItemClick, false, 0, true);
			popUp.labelField = labelField;
			
			// center popup
			popUp.width = systemManager.getSandboxRoot().width * (100 - 2 * POPUP_PADDING_PERCENT) / 100;
			popUp.maxHeight = systemManager.getSandboxRoot().height * (100 - 2 * POPUP_PADDING_PERCENT) / 100;
			
			popUp.x = systemManager.getSandboxRoot().width * POPUP_PADDING_PERCENT / 100;
			PopUpManager.addPopUp(popUp, this, true);
			popUp.y = systemManager.getSandboxRoot().height / 2 - popUp.height / 2;
			
			systemManager.getSandboxRoot().addEventListener(KeyboardEvent.KEY_DOWN, onKeyDown, false, 0, true);
			
		}
		
		protected function onItemClick(event:ItemClickEvent):void
		{
			var selecteditem:Object = popUp.selectedItem;
			
			_selectedItem = selecteditem;
			
			_selectedItemChange = true;
			
			invalidateProperties();
			
			var evt:Event = event.clone();
			dispatchEvent(evt);
			
			PopUpManager.removePopUp(popUp);
			
			systemManager.getSandboxRoot().removeEventListener(KeyboardEvent.KEY_DOWN, onKeyDown);
		}
		
		public function get selectedItem():Object
		{
			return _selectedItem;
		}
		
		public function set selectedItem(value:Object):void
		{
			popUp.selectedItem = value;
			if (value != _selectedItem)
			{
				_selectedItem = value;
				
				_selectedItemChange = true;
				
				invalidateProperties();
			}
		}
		
		override protected function commitProperties():void
		{
			super.commitProperties();
			
			if (_selectedItemChange && _selectedItem)
			{
				if (_selectedItem.hasOwnProperty(labelField))
				{
					selectedLabelDisplay.text = _selectedItem[labelField];
				}
				else
				{
					selectedLabelDisplay.text = _selectedItem.toString();
				}
				// 12.10.03 SE-362 start
				if(_selectedItem.hasOwnProperty(dspField))
					selectedLabelDisplay.text = _selectedItem[dspField];
				// 12.10.03 SE-362 end
				_selectedItemChange = false;
			}
			else if (selectedLabelDisplay != null)
			{
				//selectedLabelDisplay.text = _defaultSelectedLabel;
			}
		}
		
		override protected function partAdded(partName:String, instance:Object):void
		{
			super.partAdded(partName, instance);
			
			if (instance == selectedLabelDisplay)
			{
				//selectedLabelDisplay.text = _defaultSelectedLabel;
			}
		}
		
		protected function onKeyDown(event:KeyboardEvent):void
		{
			if (event.keyCode == Keyboard.BACK)
			{
				event.preventDefault();
				
				PopUpManager.removePopUp(popUp);
				
				systemManager.getSandboxRoot().removeEventListener(KeyboardEvent.KEY_DOWN, onKeyDown);
			}
		}
		
		/**
		 * 何も選択されていない時のラベル.
		 * @default Select
		 */
		public function get prompt():String
		{
			return _defaultSelectedLabel;
		}
		
		public function set prompt(value:String):void
		{
			_defaultSelectedLabel = value;
			
			invalidateProperties();
		}
	}
}