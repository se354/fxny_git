package modules.custom
{
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	
	import modules.formatters.CustomDateFormatter;
	
	import mx.events.FlexEvent;
	
	import spark.components.HGroup;
	
	public class CustomDateField extends HGroup
	{
		//private var _text: String;
		
		/**
		 * 1:YY/MM/DD<br>
		 * 2:YYYY/MM/DD
		 * 
		 */
		public var DateFormat: String;
		private var IText: CustomTextInput;
		private var IconBtn: IconButton;
		private var callout: CustomCalloutDate = new CustomCalloutDate();

		// 2012.12.04 SE-233 start
		/**
		 * 日付選択後実行する関数を定義
		 * 
		 */
		public var NextFunc: Function=null;		
		// 2012.12.04 SE-233 end

		public function CustomDateField()
		{
			//TODO: implement function
			super();
		}
		
		/**********************************************************
		 * 初期設定
		 * 
		 * @author SE-362 
		 * @date 12.04.24
		 **********************************************************/
		override protected function createChildren(): void
		{
			
			super.createChildren();
			
			IText = new CustomTextInput;
//			IText.text = this.text;
			IText.setStyle("styleName", "InputItem");
			IText.width = this.width - 60;
			
			this.addElement(IText);
			
			IconBtn = new IconButton();
			IconBtn.width = 50;
			IconBtn.height = 50;
			IconBtn.setStyle("icon", "../../icons/CalenderMonth.png");
			IconBtn.addEventListener(MouseEvent.CLICK, IconBtnClick, false, 0, true);
			this.addEventListener(Event.REMOVED_FROM_STAGE, removed, false, 0, true);
			
			this.addElement(IconBtn);
			
		}
		
		private function IconBtnClick(event: MouseEvent): void
		{
			callout.selectedDate = new Date(this.text);
			
			callout.open(this, true);
			
			callout.addEventListener(FlexEvent.REMOVE, closeCallout, false, 0, true);
			//stage.addEventListener(KeyboardEvent.KEY_DOWN, keyDownBack, false, 2);
		}
		
		private function closeCallout(event: FlexEvent): void
		{
			var f: CustomDateFormatter = new CustomDateFormatter();
			
			// 日付情報返還
			if(DateFormat == "1"){
				IText.text = f.dateDisplayFormat(callout.selectedDate);
				this.text = IText.text;
			}else{
				IText.text = f.dateDisplayFormat_Full(callout.selectedDate);
				this.text = IText.text;
			}
			
			closeCallout_last();
		}
		private function closeCallout_last(): void
		{			
			callout.removeEventListener(FlexEvent.REMOVE, closeCallout);

			// 2012.12.04 SE-233 start
			if(NextFunc!=null)
				NextFunc();
			// 2012.12.04 SE-233 end
		}
		
		public function get text(): String
		{
			return IText.text;
		}
		
		public function set text(value: String): void
		{
			IText.text = value;
		}
		
		private function removed(e: Event): void
		{
			IconBtn.removeEventListener(MouseEvent.CLICK, IconBtnClick);
			removeEventListener(Event.REMOVED_FROM_STAGE, removed);
		}
	}
}