package modules.custom
{
	import modules.custom.sub.Iterator;
	
	import mx.collections.ArrayCollection;
	
	/**
	 * イテレータクラス.
	 * SQLiteのINSERTやUPDATEを複数行実行するために作成。
	 * 
	 * @author SE-354 
	 * @date 12.04.24
	 */
	public class CustomIterator implements Iterator
	{
		private var dataArray:ArrayCollection;
		private var index:int = 0;
		
		/**
		 * コンストラクタでArrayCollectionオブジェクトを受け取る.<br>
		 * 例)<br>
		 * var iterator:Iterator = new CustomIterator(arrUB010X);<br>
		 * while (iterator.hasNext())<br>
		 * {<br>
		 *      trace(iterator.next());<br>
		 * }
		 * @author SE-354 
		 * @date 12.04.24
		 */
		public function CustomIterator(dataArray: ArrayCollection): void
		{
			this.dataArray = dataArray;
		}
		
		/**
		 * データを返却し、インデックスをひとつ進める.
		 * <br>
		 * 例)
		 * w_W_ORD = W_ORD(iterator.next());
		 * 
		 * @return ArrayCollectionの次index要素。
		 * @author SE-354 
		 * @date 12.04.24
		 */
		public function next():*
		{
			return dataArray[index++];
		}
		
		/**
		 * まだデータがあるかどうか.
		 * <br>
		 * 例)<br>
		 * while (iterator.hasNext())<br>
		 * {<br>
		 * 		// iterator.next()で次要素を取得してINSERT処理<br>
		 * }
		 * @return ArrayCollectionの次index要素。
		 * @author SE-354 
		 * @date 12.04.24
		 */
		public function hasNext():Boolean
		{
			return index < dataArray.length;
		}
	}
}