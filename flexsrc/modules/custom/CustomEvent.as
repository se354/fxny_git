package modules.custom
{
	import flash.events.Event;
	
	public class CustomEvent extends Event
	{
		public static const CHENGE_CHECK: String = "CHENGE_CHECK";
		public static const CLICK: String = "CLICK";
		public static const BUTTON_CLICK: String = "BUTTON_CLICK";
		public static const INFO_BUTTON_CLICK: String = "INFO_BUTTON_CLICK";
		
		public function CustomEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
	}
}