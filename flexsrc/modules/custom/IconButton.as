package modules.custom
{
	import flash.events.MouseEvent;
	
	import spark.components.Button;
	
	[Style(name="icon", inherit="no", type="Class")]
	
	public class IconButton extends Button
	{
		public function IconButton()
		{
			super();
			
			setStyle("skinClass", IcnSkin);
		}
		
	}
}