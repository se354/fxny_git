package modules.custom
{
	import flash.events.Event;
	
	import modules.base.BasedMethod;
	
	import spark.components.gridClasses.GridColumn;
	
	/**
	 * グリッドカラムクラス
	 * 
	 * @author SE-362 
	 * @date 12.04.26
	 */
	public class CustomGridColumn extends GridColumn
	{

		private var _Align_Flg: String;	// 文字揃え(0:左詰 1:右詰)
		private var _sortFunc: String;		// 数値用のソートを用いるかどうか

		/**
		 * コンストラクタ
		 * 
		 * @author SE-362 
		 * @date 12.04.26
		 */
		public function CustomGridColumn(columnName:String=null)
		{
			super(columnName);
		}

		/**
		 * 変数_Align_Flg設定/取得
		 * 
		 * @author SE-362 
		 * @date 12.04.26
		 */
		[Bindable("AlignChanged")]
		public function get Align_Flg(): String
		{
			return _Align_Flg;	
		}
		
		public function set Align_Flg(value: String): void
		{
			_Align_Flg = value;
			/*
			// 文字位置が指定されている場合、設定
			if(BasedMethod.nvl(_Align_Flg, "0") == "1"){
				// 右詰め
				this.itemRenderer = new ClassFactory(ColumnRight);
				this.headerRenderer = new ClassFactory(ColumnHeaderRight);
			}else if(BasedMethod.nvl(_Align_Flg, "0") == "2"){
				// 中央揃え
				this.itemRenderer = new ClassFactory(ColumnCenter);
				this.headerRenderer = new ClassFactory(ColumnHeaderCenter); 
			}
			*/
			
			dispatchEvent(new Event("AlignChanged"));
		}

		/**
		 * 変数_sortFunc設定/取得
		 * 
		 * @author SE-362 
		 * @date 12.04.26
		 */
		[Bindable("sortFuncChanged")]
		public function get sortFunc(): String
		{
			return _sortFunc;
		}
		
		public function set sortFunc(value: String): void
		{
			_sortFunc = value;
			
			if (_sortFunc == "1")
			{
				this.sortCompareFunction = sortAsNumber;
			}        	
			
			dispatchEvent(new Event("sortFuncChanged"));
		}        
		
		/**
		 * フォーマットされた数値を、数値とみなしてソート
		 * 
		 * @author SE-362 
		 * @date 12.04.26
		 */
		public function sortAsNumber(obj1:Object, obj2:Object, column: GridColumn):int
		{
			var value1:Number = BasedMethod.asCurrency(obj1[dataField]);
			var value2:Number = BasedMethod.asCurrency(obj2[dataField]);
			
			if(value1 < value2) 
			{
				return -1;
			}
			else if(value1 > value2)
			{
				return 1;
			}
			else 
			{
				return 0;
			}
		}
	}
}