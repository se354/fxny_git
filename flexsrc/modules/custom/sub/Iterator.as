package modules.custom.sub
{
	/**
	 * SQLiteのINSERTやUPDATEを複数行実行するためのイテレータインターフェースです.
	 * CustomIteratorクラスを使用して下さい。
	 * @author 12.04.24 SE-354
	 */
	public interface Iterator
	{
		function next():*;
		function hasNext():Boolean;
	}
}