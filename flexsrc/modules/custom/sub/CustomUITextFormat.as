package modules.custom.sub
{
    import flash.text.TextField;
    import flash.text.TextLineMetrics;
    
    import mx.core.UITextFormat;
    import mx.managers.ISystemManager;
    
	//******************************************************************************
	//name   = CustomUITextFormat
	//func   = カスタムUITextFormat (CustomComboButton.asにて使用)
	//           ・Buttonラベルの複数行表示
	//date   = 09.12.04 SE-254
	//alter  = xx.xx.xx SE-xxx
	//******************************************************************************
    public class CustomUITextFormat extends UITextFormat {

    	// Private変数
        private static var measurementTextField: TextField;
        private var systemManager: ISystemManager;
        
		//******************************************************************************
		//name   = CustomComboBoxSub
		//func   = コンストラクタ
		//io     = 
		//return = 
		//date   = 09.11.22 SE-254
		//alter  = xx.xx.xx SE-xxx
		//******************************************************************************
        public function CustomUITextFormat(systemManager: ISystemManager, font: String = null,
											 size: Object = null, color: Object = null, bold: Object = null,
											 italic: Object = null, underline: Object = null, url: String = null,
											 target: String = null, align: String = null, leftMargin: Object = null,
											 rightMargin: Object = null, indent: Object = null, leading: Object = null)
		{
            super(
                systemManager,
                font,
                size,
                color,
                bold,
                italic,
                underline,
                url,
                target,
                align,
                leftMargin,
                rightMargin,
                indent,
                leading
            );
      
            this.systemManager = systemManager;
        }
        
		//******************************************************************************
		//name   = getMultilineTextLineMetrics
		//func   = テキストフィールドのメトリック情報 取得
		//io     = i : measurementTextField: テキストフィールド
		//return = メトリック情報 
		//date   = 09.12.04 SE-254
		//alter  = xx.xx.xx SE-xxx
		//******************************************************************************
        private static function getMultilineTextLineMetrics(measurementTextField: TextField): TextLineMetrics
        {
			var i: int;        	
            var lineMetrics: TextLineMetrics;
            var metrics: TextLineMetrics;
            
            lineMetrics = measurementTextField.getLineMetrics(0);
            
            // 幅・高さの最大値を取得する
            for (i=1; i<measurementTextField.numLines; i++) {
                metrics = measurementTextField.getLineMetrics(i);
                
                lineMetrics.width  = Math.max(lineMetrics.width, metrics.width);
                lineMetrics.height = lineMetrics.height + metrics.height;
            }
            
            // 値を切り上げする
            lineMetrics.width  = Math.ceil(lineMetrics.width);
            lineMetrics.height = Math.ceil(lineMetrics.height);
            
            return lineMetrics;
        }
        
		//******************************************************************************
		//name   = getMeasurementTextField
		//func   = 複数行テキストフィールド 取得
		//io     = 
		//return = テキストフィールド
		//date   = 09.12.04 SE-254
		//alter  = xx.xx.xx SE-xxx
		//******************************************************************************
        private static function getMeasurementTextField(): TextField
        {
            var measurementTextField: TextField;
            
            measurementTextField = CustomUITextFormat.measurementTextField;
            
            // テキストフィールドが存在すれば、フォーマット設定をする
            if (!measurementTextField){
                CustomUITextFormat.measurementTextField = new TextField();
                measurementTextField = CustomUITextFormat.measurementTextField;
                measurementTextField.multiline = true;
            }
            
            // テキストフィールドをクリアする
            measurementTextField.htmlText = "";
            measurementTextField.text     = "";
            
            return measurementTextField;
        }

		//******************************************************************************
		//name   = measureText
		//func   = テキストのメトリック情報 取得
		//io     = i : text			: テキスト
		//         i : roundUp		: 測定値を切り上げるかどうか
		//return = メトリック情報 
		//date   = 09.12.04 SE-254
		//alter  = xx.xx.xx SE-xxx
		//******************************************************************************
        public override function measureText(text: String, roundUp: Boolean=true): TextLineMetrics
        {
            return multilineMeasure(text, false);
        }
    
		//******************************************************************************
		//name   = measureHTMLText
		//func   = HTMLテキストのメトリック情報 取得
		//io     = i : htmlText		: HTMLテキスト
		//         i : roundUp		: 測定値を切り上げるかどうか
		//return = メトリック情報 
		//date   = 09.12.04 SE-254
		//alter  = xx.xx.xx SE-xxx
		//******************************************************************************
        public override function measureHTMLText(htmlText: String, roundUp: Boolean=true): TextLineMetrics
        {
            return multilineMeasure(htmlText, true);
        }
    
		//******************************************************************************
		//name   = multilineMeasure
		//func   = テキストのメトリック情報 取得
		//io     = i : s			: 設定する文字列
		//         i : html			: HTMLテキストかどうか
		//return = メトリック情報 
		//date   = 09.12.04 SE-254
		//alter  = xx.xx.xx SE-xxx
		//******************************************************************************
        private function multilineMeasure(s: String, html: Boolean): TextLineMetrics 
        {
            var measurementTextField: TextField;
            
            if (!s){
                s = "";
            }
            
            measurementTextField = getMeasurementTextField();
            setupMeasurementTextFormat(measurementTextField);
            
            if (html) {
                measurementTextField.htmlText = s;
            } else {
                measurementTextField.text     = s;
            }

            return getMultilineTextLineMetrics(measurementTextField);
        }
        
		//******************************************************************************
		//name   = setupMeasurementTextFormat
		//func   = テキストフィールドのフォーマット
		//io     = i : measurementTextField: テキストフィールド 
		//return =  
		//date   = 09.12.04 SE-254
		//alter  = xx.xx.xx SE-xxx
		//******************************************************************************
        private function setupMeasurementTextFormat(measurementTextField: TextField): void
        {
            var systemManager: ISystemManager;

            measurementTextField.defaultTextFormat = this;

            systemManager = systemManager;
            
            if (font) {
                measurementTextField.embedFonts = 
                    systemManager != null && systemManager.isFontFaceEmbedded(this);
            } else {
                measurementTextField.embedFonts = false;
            }
    
            measurementTextField.antiAliasType = antiAliasType;
            measurementTextField.gridFitType   = gridFitType;
            measurementTextField.sharpness     = sharpness;
            measurementTextField.thickness     = thickness;
        }
    }
}
