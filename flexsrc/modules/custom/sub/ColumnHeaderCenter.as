package modules.custom.sub
{
	import mx.rpc.events.HeaderEvent;
	
	import spark.components.Label;
	import spark.components.TextInput;
	
	/**
	 * 中央揃えカラムクラス
	 * 
	 * @author SE-362 
	 * @date 12.04.26
	 */
	public class ColumnHeaderCenter extends DefaultGridHeaderRenderer2
	{
		/**
		 * コンストラクタ
		 * 
		 * @author SE-362 
		 * @date 12.04.26
		 */
		public function ColumnHeaderCenter()
		{
			super();

			// 中央揃え宣言
			this.setStyle("textAlign", "center");
		}
	}
}