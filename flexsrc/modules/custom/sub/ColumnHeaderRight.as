package modules.custom.sub
{
	import mx.rpc.events.HeaderEvent;
	
	import spark.components.Label;
	import spark.components.TextInput;
	
	/**
	 * 右詰めカラムクラス
	 * 
	 * @author SE-362 
	 * @date 12.04.26
	 */
	public class ColumnHeaderRight extends DefaultGridHeaderRenderer2
	{
		/**
		 * コンストラクタ
		 * 
		 * @author SE-362 
		 * @date 12.04.26
		 */
		public function ColumnHeaderRight()
		{
			super();

			// 右詰め宣言
			this.setStyle("textAlign", "right");
		}
	}
}