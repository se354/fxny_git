package modules.custom.sub
{
	import spark.skins.spark.DefaultGridItemRenderer;
	
	/**
	 * 右詰めカラムクラス
	 * 
	 * @author SE-362 
	 * @date 12.04.26
	 */
	public class ColumnRight extends DefaultGridItemRenderer
	{
		/**
		 * コンストラクタ
		 * 
		 * @author SE-362 
		 * @date 12.04.26
		 */
		public function ColumnRight()
		{
			super();
			
			// 右詰め宣言
			this.setStyle("textAlign", "right");
		}
	}
}