package modules.custom.sub
{
	import spark.skins.spark.DefaultGridItemRenderer;
	
	/**
	 * 中央揃えカラムクラス
	 * 
	 * @author SE-362 
	 * @date 12.04.26
	 */
	public class ColumnCenter extends DefaultGridItemRenderer
	{
		/**
		 * コンストラクタ
		 * 
		 * @author SE-362 
		 * @date 12.04.26
		 */
		public function ColumnCenter()
		{
			super();
			
			// 右詰め宣言
			this.setStyle("textAlign", "center");
		}
	}
}