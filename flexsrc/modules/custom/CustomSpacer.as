package modules.custom
{
	import mx.controls.Spacer;
	
	/**
	 * スペースクラス
	 * 
	 * @author SE-362 
	 * @date 12.05.07
	 */
	public class CustomSpacer extends Spacer
	{
		/**
		 * コンストラクタ
		 * 
		 * @author SE-362 
		 * @date 12.05.07
		 */
		public function CustomSpacer()
		{
			super();
		}
	}
}