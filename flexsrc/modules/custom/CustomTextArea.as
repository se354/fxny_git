package modules.custom
{

    import flash.events.Event;
    import flash.events.FocusEvent;
    import flash.events.KeyboardEvent;
    import flash.events.TextEvent;
    import flash.ui.*;
    
    import modules.base.BasedMethod;
    import modules.sbase.SiteConst;
    
    import mx.core.ScrollPolicy;
    import mx.core.UIComponent;
    import mx.core.mx_internal;
    
    import spark.components.TextArea;

    use namespace mx_internal;
// 11.02.04 SE-354 end

	//******************************************************************************
	//name   = CustomTextArea
	//func   = カスタムTextArea
	//           ・Enter時に項目移動(11.02.21 改行も同時に走ってしまうため、Enterで移動しないように修正)
	//           ・フォーカスin/out時に背景色変更
	//           ・バイト数で入力制限
	//           ・改行の行数制限(11.02.04 追加)
	//date   = 09.11.22 SE-254
	//alter  = 11.02.04 SE-354	改行の行数制限機能追加
	//******************************************************************************
    public class CustomTextArea extends TextArea {

		// 別class宣言
		private var singleton: CustomSingleton = CustomSingleton.getInstance();
    	
		// Private変数
		
		// Public変数    	
        public var customComp: String;				// カスタムコンポーネントかどうか
// 11.01.21 SE-354 start
//    	public var notNull: String;				// 必須入力フラグ
//    												// ("1":必須)
    	private var _notNull: String;				// 必須入力フラグ
    												// ("1":必須)
		private var _notNull_org: String;			// 必須入力フラグ退避用
// 11.01.21 SE-354 end
// 10.07.07 by SE-354 start
//    	public var modified: String;				// チェック処理判断フラグ
//    												// ("0":なし,"1":チェックのみ,"2":＋値セット)
    	private var _modified: String;				// チェック処理判断フラグ
    												// ("0":なし,"1":チェックのみ,"2":＋値セット)
// 10.07.07 by SE-354 end
    	public var checkFlg: String;				// 項目チェック関数があるかどうか
    												// ("1":関数あり)

    	public var maxByte: String;				// 最大入力バイト数
// 11.02.04 改行の行数制限機能追加 SE-354 start
    	public var maxLin: String;					// 最大入力行数
    	private var noOfLines: int;				// 現在の行数
// 11.02.04 改行の行数制限機能追加 SE-354 end
    	    	
		public var nextFocusFunc: Function;		// 次フォーカス項目制御用関数
    	    	
    	public var dispatch: String;				// dispatchValidateによるチェック処理判断フラグ
    												// ("1":dispatchValidate時)
    	    	
		// (多明細 現選択行判断用)
//		public var gridId: CustomMultiDataGrid;	// 自項目が乗っているCustomMultiDataGrid
		public var gridIndex: int;					// 現選択index
		
// 10.07.07 by SE-354 start
		public var grid: String;					// grid内項目の場合IDをセット
		public var nextFocusItem: UIComponent;		// validate時、次にフォーカスが移る項目
// 10.07.07 by SE-354 end
// 11.03.14 SE-354 start
		public var fullLetter: String = "0";		// 全角/半角識別用プロパティ
													// ("1":全角)
// 11.03.14 SE-354 end
    	    	
		//******************************************************************************
		// ↓↓↓ ここから触らないこと！
		//******************************************************************************
    	    	
		//******************************************************************************
		//name   = CustomTextArea
		//func   = デフォルトコンストラクタ
		//io     = 
		//return = 
		//date   = 09.11.22 SE-254
		//alter  = xx.xx.xx SE-xxx
		//******************************************************************************
        public function CustomTextArea()
        {
            super();
        }

		//******************************************************************************
		//name   = initialize
		//func   = 初期化処理
		//io     = 
		//return = 
		//date   = 09.11.22 SE-254
		//alter  = xx.xx.xx SE-xxx
		//******************************************************************************
        public override function initialize(): void
        {   
            super.initialize();   
        }   

		//******************************************************************************
		//name   = updateDisplayList
		//func   = 表示リストの更新
		//io     = i : unscaledWidth: 幅
		//         i : unscaledHeight: 高さ
		//return = 
		//date   = 09.11.22 SE-254
		//alter  = xx.xx.xx SE-xxx
		//******************************************************************************
        protected override function updateDisplayList(unscaledWidth: Number, unscaledHeight: Number): void
        {   
            super.updateDisplayList(unscaledWidth, unscaledHeight);   
        }   

		//******************************************************************************
		// ↑↑↑ ここまで触らないこと！
		//******************************************************************************
        
		//******************************************************************************
		//name   = createChildren
		//func   = 初期設定
		//io     = 
		//return = 
		//date   = 09.11.22 SE-254
		//alter  = xx.xx.xx SE-xxx
		//******************************************************************************
        override protected function createChildren(): void
        {
        	var w_stylename: String = "";
        	
    		super.createChildren();
    		
			// プロパティ未指定時の初期値設定
			if (BasedMethod.nvl(maxByte) == "") {
				maxByte = SiteConst.G_MAXBYTE;
			}
			
// 11.02.04 改行の行数制限機能追加 SE-354 start
			// プロパティ未指定時の初期値設定
			//if (singleton.sitemethod.nvl(maxLin) == "") {
			//	maxLin = "8";
			//}
// 11.02.04 改行の行数制限機能追加 SE-354 end

            // styleNameに応じたプロパティの設定
            try {
				w_stylename = this.styleName.toString();
            } catch (e: Error) {
            }
			
			// (変数セット)
// 11.01.21 SE-354 start
//			notNull = singleton.sitemethod.getNotNull(w_stylename);
			_notNull = BasedMethod.getNotNull(w_stylename);
			_notNull_org = BasedMethod.getNotNull(w_stylename);
// 11.01.21 SE-354 end
			
            // (編集モード)
            this.editable   = BasedMethod.getEnabled(w_stylename);
            this.tabEnabled = BasedMethod.getTabEnabled(w_stylename);
// 11.02.04 改行の行数制限機能追加 SE-354 start

// 11.03.24 "と,は入力できないように制御 SE-354 start
			this.restrict = '^",';
// 11.03.24 "と,は入力できないように制御 SE-354 end

// 11.04.25 コマツでは折り返し有 SE-354 start
			// テキストを行末で自動的に折り返さないようにする
            //this.wordWrap = false;
// 11.04.25 コマツでは折り返し有 SE-354 end
            
//            this.horizontalScrollPolicy = ScrollPolicy.OFF;
// 11.02.04 改行の行数制限機能追加 SE-354 end
            
            // modifiedの初期値設定
            modified = "0";
            
            // gridId/gridIndexの初期値設定
//            gridId    = singleton.sitemethod.getGridId(this);
//			gridIndex = -1;				
            
            // イベント追加
            this.addEventListener(Event.CHANGE, changeHandler, false, 0, true);
			this.addEventListener(Event.REMOVED_FROM_STAGE, removed, false, 0, true);
// 11.02.04 SE-354 start            
//			this.addEventListener(TextEvent.TEXT_INPUT, textFieldInputHandler);
// 11.02.04 SE-354 end
        }
        
		//******************************************************************************
		//name   = notNull
		//func   = 変数_notNull設定/取得
		//io     = 
		//return = 
		//date   = 11.01.21 SE-354
		//alter  = xx.xx.xx SE-xxx
		//******************************************************************************
		[Bindable("notNullChanged")]
        public function get notNull(): String
        {
        	return _notNull;
        }
        
        public function set notNull(value: String): void
        {
        	_notNull = value;

        	dispatchEvent(new Event("notNullChanged"));
        }
        
        public function get notNull_org(): String
        {
        	return _notNull_org;
        }
        
		//******************************************************************************
		//name   = keyDownHandler
		//func   = キーダウン時 処理
		//io     = i : event		: キーボードイベント
		//return = 
		//date   = 09.11.22 SE-254
		//alter  = xx.xx.xx SE-xxx
		//******************************************************************************
        override protected function keyDownHandler(event: KeyboardEvent): void 
        {
        	var tabKeyEvent: FocusEvent;
        	
// 11.01.21 TextAreaではEnterキーで移動できないように修正 SE-354 start
/*
        	// Enterキー押下時
	        if (event.keyCode == Keyboard.ENTER) {

	        	// itemRenderer上の項目で無いとき
	        	if (event.target.toString().indexOf("inlineComponent") == -1) {

		        	// 次の項目にフォーカスを移す
		        	tabKeyEvent = new FocusEvent(FocusEvent.KEY_FOCUS_CHANGE,
		        								 true, false, null, event.shiftKey, Keyboard.TAB);
		        	dispatchEvent(tabKeyEvent);
		        }        	
	        }
*/
// 11.01.21 TextAreaではEnterキーで移動できないように修正 SE-354 end
	        
	        super.keyDownHandler(event);
    	}
        
		//******************************************************************************
		//name   = focusInHandler
		//func   = フォーカス取得時 処理
		//io     = i : event		: フォーカスイベント
		//return = 
		//date   = 09.11.22 SE-254
		//alter  = xx.xx.xx SE-xxx
		//******************************************************************************
        override protected function focusInHandler(event: FocusEvent): void
        {
            super.focusInHandler(event);
            
            // フラグのセット
            modified  = "0";   

            // 編集可能モード時のみ、背景色を変更する
            if (this.editable == true) {
// 11.01.21 SE-354 start
//            	this.styleName = singleton.sitemethod.getStyleName("", notNull, "1");
            	this.styleName = singleton.sitemethod.getStyleName("", _notNull, "1");
// 11.01.21 SE-354 end
            }
        }

		//******************************************************************************
		//name   = focusOutHandler
		//func   = フォーカス喪失時 処理
		//io     = i : event		: フォーカスイベント
		//return = 
		//date   = 09.11.22 SE-254
		//alter  = xx.xx.xx SE-xxx
		//******************************************************************************
        override protected function focusOutHandler(event:FocusEvent):void
        {
            super.focusOutHandler(event);
            
            // 編集可能モード時のみ、背景色を変更する
            if (this.editable == true) {
// 11.01.21 SE-354 start
//            	this.styleName = singleton.sitemethod.getStyleName("", notNull, "0");
            	this.styleName = singleton.sitemethod.getStyleName("", _notNull, "0");
// 11.01.21 SE-354 end
            }
        }
        
// 11.04.25 コマツでは改行制限無し SE-354 start
        /*
		//******************************************************************************
		//name   = textFieldInputHandler
		//func   = データ入力時 処理
		//io     = i : event		: イベント
		//return = 
		//date   = 11.02.04 SE-354
		//alter  = xx.xx.xx SE-xxx
		//******************************************************************************
        private function textFieldInputHandler(event: TextEvent): void
        {
        	noOfLines = this.mx_internal::getTextField().numLines;
        	
			if (Number(maxLin) <= noOfLines)
			{
				if (event.text.search(/\n/g) > -1)
				{
					event.preventDefault();
				}
			}
        }
        */
// 11.04.25 コマツでは改行制限無し SE-354 end
        
		//******************************************************************************
		//name   = changeHandler
		//func   = データ入力時 処理
		//io     = i : event		: イベント
		//return = 
		//date   = 09.11.22 SE-254
		//alter  = xx.xx.xx SE-xxx
		//******************************************************************************
		public function changeHandler(event: Event): void
		{
			var strByteLen: Number;
			var strTemp: String;
			var tempChar: String;
			var str: String;
			
			var i: Number;
						
			// 入力最大値をバイト数で制限
			strByteLen = 0;
			strTemp    = "";
			str        = this.text;

			for (i=0; i<str.length; i++) {
				
				tempChar = str.substr(i, 1);
				
				// i文字目のバイト数を取得して加算する
				strByteLen = strByteLen + singleton.sitemethod.getByteLen(str.substr(i, 1));
				
				// 最大バイト数を超えるまでの文字は画面に戻す
				if (strByteLen <= Number(maxByte)) {
					strTemp = strTemp + tempChar;

				} else{
					break;
				}
			}
			
			this.text = strTemp;

			// gridIndexのセット
			// 10.03.31 SE-308
			//if (gridId != null) {
//			if ((gridId != null) && (checkFlg == "1")) {
//				gridIndex = gridId.selectedIndex;
//			}

            // フラグのセット
            modified = "2";   
		}  
		  	
		//******************************************************************************
		//name   = errorString
		//func   = エラーメッセージセット時 処理
		//io     = i : value		: エラー値
		//return = 
		//date   = 09.12.24 SE-254
		//alter  = xx.xx.xx SE-xxx
		//******************************************************************************
        override public function set errorString(value: String): void
        {
        	super.errorString = value;
        	
        	// 枠線の太さを変更(エラー時の赤枠は太く)
        	if (BasedMethod.nvl(value) != "") {
        		
        		this.setStyle("borderStyle", "solid");
        		this.setStyle("borderThickness", 2);
        		
        	} else {

        		clearStyle("borderStyle");
// 10.07.12 SE-354 start
				clearStyle("borderThickness");
// 10.07.12 SE-354 end        		
        	}
        }
        
// 10.07.07 by SE-354 start

		//******************************************************************************
		//name   = modified
		//func   = 変数_modified設定/取得
		//io     = 
		//return = 
		//date   = 10.07.07 SE-354
		//alter  = xx.xx.xx SE-xxx
		//******************************************************************************
		[Bindable("modifiedChanged")]
        public function get modified(): String
        {
        	return _modified;
        }
        
        public function set modified(value: String): void
        {
        	var w_modified: String;
        	
        	w_modified = _modified;
        	
        	_modified = value;
        	
        	if (BasedMethod.nvl(grid) != "") {
	        	if (w_modified != _modified) {
		        	if (_modified == "2") {
// 10.11.09 gridId,disableFlgが常に親画面にしかセットされなかった不具合修正 SE-354 start		        		
//		        		Application.application.gridId = grid;
//		        		Application.application.disableFlg = "1";
		        		
		        		this.parentDocument.gridId = grid;
		        		this.parentDocument.disableFlg = "1";
// 10.11.09 gridId,disableFlgが常に親画面にしかセットされなかった不具合修正 SE-354 end		        		
		        	} else {
// 10.11.09 gridId,disableFlgが常に親画面にしかセットされなかった不具合修正 SE-354 start		        		
//		        		Application.application.gridId = "";
//		        		Application.application.disableFlg = "";
		        		
		        		this.parentDocument.gridId = "";
		        		this.parentDocument.disableFlg = "";
// 10.11.09 gridId,disableFlgが常に親画面にしかセットされなかった不具合修正 SE-354 end		
		        	}
		        }
        	}

        	dispatchEvent(new Event("modifiedChanged"));
        }        

// 10.07.07 by SE-354 end
		private function removed(e: Event): void
		{
			removeEventListener(Event.CHANGE, changeHandler);
			removeEventListener(Event.REMOVED_FROM_STAGE, removed);
		}
	}
}
