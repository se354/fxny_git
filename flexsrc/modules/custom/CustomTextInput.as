package modules.custom
{
	import flash.events.Event;
	import flash.events.FocusEvent;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.ui.Keyboard;
	
	import modules.base.BasedMethod;
	import modules.sbase.SiteConst;
	
	import mx.core.UIComponent;
	
	import spark.components.Group;
	import spark.components.Label;
	import spark.components.TextInput;
	import spark.skins.mobile.TextInputSkin;
	
	/**********************************************************
	 * テキストインプットクラス
	 * Enter時に項目移動
	 * フォーカスin/out時に背景色変更
	 * バイト数で入力制限
	 * 数値入力対応
	 * 
	 * @author SE-362 
	 * @date 12.04.24
	 **********************************************************/
	public class CustomTextInput extends TextInput
	{
		// 別class宣言
		private var singleton: CustomSingleton = CustomSingleton.getInstance();
		
		// Private変数
		// (全般)
		private var _focusIn: Boolean;				// フォーカスインフラグ
		// (true:フォーカスイン)
		
		// (数値項目)
		private var _numOnly: String;				// 数値項目フラグ
		// ("1":数値項目)
		
		// Public変数    	
		public var customComp: String;				// カスタムコンポーネントかどうか
		private var _notNull: String;				// 必須入力フラグ
		// ("1":必須)
		private var _notNull_org: String;			// 必須入力フラグ退避用
		//    	public var notNull: String;				// 必須入力フラグ
		//    												// ("1":必須)
		//    												// ("0":なし,"1":チェックのみ,"2":＋値セット)
		private var _modified: String;					// チェック繁多尿フラグ
		// ("0":なし,"1":チェックのみ,"2":＋値セット)
		// 10.07.05 by SE-254 end
		public var checkFlg: String;				// 項目チェック関数があるかどうか
		// ("1":関数あり)
		
		public var maxByte: String;				// 最大入力バイト数
		
		public var dispatch: String;				// dispatchValidateによるチェック処理判断フラグ
		// ("1":dispatchValidate時)
		
		public var focusInFormat: Boolean;			// フォーカスインフラグ(数値フォーマット制御用)
		// (true:フォーカスイン)
		
		// (数値項目)
		public var intNum: String;					// 整数部桁数
		public var realNum: String;				// 小数部桁数
		public var comma: String;					// カンマ表示フラグ
		// ("0":表示しない,"1":表示する)
		public var round: String					// 丸めモード
		// ("0":四捨五入なし,"1":切捨て,"2":切上げ,"3":四捨五入)
		
		// (POPUPからの戻り値判断)
		public var popRtn: String;					// ポップアップからの戻り値フラグ
		// ("1":ポップアップからの戻り値)
		
		public var nextFocusFunc: Function;		// 次フォーカス項目制御用関数
		
		// (多明細 現選択行判断用)
		
		public var grid: String;					// grid内項目の場合IDをセット
		public var nextFocusItem: UIComponent;		// validate時、次にフォーカスが移る項目
		public var fullLetter: String = "0";		// 全角/半角識別用プロパティ
		// ("1":全角)

		/**********************************************************
		 * コンストラクタ
		 * 
		 * @author SE-362 
		 * @date 12.04.24
		 **********************************************************/
		public function CustomTextInput()
		{
			super();
		}
		
		/**********************************************************
		 * textプロパティ設定時処理
		 * 
		 * @author SE-362 
		 * @date 12.04.24
		 **********************************************************/
		override public function set text(value:String):void
		{
			super.text = value;
			
			// 数値項目の時は表示文字列にフォーマットをかける
			//			if ((_numOnly == "1") && (modified != "2")) {
			if ((_numOnly == "1") && (modified != "2" || editable == false)) {
				
				super.text = textNumberFormat();
			}
		}        

		/**********************************************************
		 * 初期化処理
		 * 
		 * @author SE-362 
		 * @date 12.04.24
		 **********************************************************/
		public override function initialize(): void
		{   
			super.initialize();   
		}   
		
		/**********************************************************
		 * 表示リストの更新
		 * 
		 * @author SE-362 
		 * @date 12.04.24
		 **********************************************************/
		protected override function updateDisplayList(unscaledWidth: Number, unscaledHeight: Number): void
		{   
			super.updateDisplayList(unscaledWidth, unscaledHeight);   
		}   

		/**********************************************************
		 * 初期設定
		 * 
		 * @author SE-362 
		 * @date 12.04.24
		 **********************************************************/
		override protected function createChildren(): void
		{
			var w_stylename: String;
			
			super.createChildren();

			// プロパティ未指定時の初期値設定
			if (BasedMethod.nvl(maxByte) == "") {
				maxByte = SiteConst.G_MAXBYTE;
			}
			if (BasedMethod.nvl(intNum) == "") {
				intNum = SiteConst.G_INTNUM;
			}
			if (BasedMethod.nvl(realNum) == "") {
				realNum = SiteConst.G_REALNUM;
			}
			if (BasedMethod.nvl(comma) == "") {
				comma = SiteConst.G_COMMA;
			}
			
			// リターンキーのデフォルト表示を’次へ’
			if (this.returnKeyLabel == "default"){
				this.returnKeyLabel = "next";
			}
			
			// styleNameに応じたプロパティの設定
			if(this.styleName != null){
				w_stylename = this.styleName.toString();
			}else{
				return;
			}
			// (変数セット)
			// 11.01.20 SE-354 start
			//			notNull  = _BasedMethod.getNotNull(w_stylename);
			_notNull  = BasedMethod.getNotNull(w_stylename);
			_notNull_org = BasedMethod.getNotNull(w_stylename);
			// 11.01.20 SE-354 end
			_numOnly = BasedMethod.getNumOnly(w_stylename);
			
			// (編集モード)
			this.editable   = BasedMethod.getEnabled(w_stylename);
			this.tabEnabled = BasedMethod.getTabEnabled(w_stylename);
			
			// (数値入力制御)
			if (_numOnly == "1") {
				this.restrict = BasedMethod.getNumRestrict();
				
				this.softKeyboardType = "number";
			}
			
			// modifiedの初期値設定
			modified = "0";

			// スキンクラス設定
			this.setStyle("skinClass", TextInputSkin);

			// イベント追加
			this.addEventListener(Event.CHANGE, changeHandler, false, 0, true);
// 12.06.13 SE-362 start
			this.addEventListener(MouseEvent.MOUSE_OUT, MouseOutHandler, false, 0, true);
// 12.06.13 SE-362 end
			this.addEventListener(Event.REMOVED_FROM_STAGE, removed, false, 0, true);
		}

		/**********************************************************
		 * 変数notNull設定／取得
		 * @author SE-362 
		 * @date 12.04.24
		 **********************************************************/
		[Bindable("notNullChanged")]
		public function get notNull(): String
		{
			return _notNull;
		}
		
		public function set notNull(value: String): void
		{
			_notNull = value;
			
			dispatchEvent(new Event("notNullChanged"));
		}
		
		public function get notNull_org(): String
		{
			return _notNull_org;
		}
		
		/**********************************************************
		 * キーダウン時処理
		 * @param i: event
		 * @author SE-362 
		 * @date 12.04.24
		 **********************************************************/
		override protected function keyDownHandler(event: KeyboardEvent): void 
		{
			var tabKeyEvent: FocusEvent;
			
			// Enterキー押下時
			if (event.keyCode == Keyboard.ENTER) {
				
				// itemRenderer上の項目で無いとき
				if (event.target.toString().indexOf("inlineComponent") == -1) {
					
					// 次の項目にフォーカスを移す
					tabKeyEvent = new FocusEvent(FocusEvent.KEY_FOCUS_CHANGE,
						true, false, null, event.shiftKey, Keyboard.TAB);
					dispatchEvent(tabKeyEvent);
				}
			}
			
			super.keyDownHandler(event);
		}
		
		/**********************************************************
		 * フォーカス取得時処理
		 * @param i: event
		 * @author SE-362 
		 * @date 12.04.24
		 **********************************************************/
		override protected function focusInHandler(event: FocusEvent): void
		{
			super.focusInHandler(event);
			
			// フラグのセット
			_focusIn  = true;   
			modified  = "0"; 
			focusInFormat = true;
			
			// 数値項目の時、フォーマットを変更する
			if (_numOnly == "1") {
				this.text = textNumberFormat();
			}       
		}
		
		/**********************************************************
		 * フォーカス喪失時処理
		 * @param i: event
		 * @author SE-362 
		 * @date 12.04.24
		 **********************************************************/
		override protected function focusOutHandler(event:FocusEvent):void
		{
			super.focusOutHandler(event);
			
			// フラグのセット
			_focusIn = false;
			focusInFormat = false;     
			
			// 数値項目の時は表示文字列にフォーマットをかける
			if ((_numOnly == "1") && (modified != "2")) {
				
				this.text = textNumberFormat();
			}
		
		}

		/**********************************************************
		 * マウスアウト時処理
		 * @param i: event
		 * @author SE-362 
		 * @date 12.06.13
		 **********************************************************/
		public function MouseOutHandler(event: MouseEvent): void
		{
			// 仮想キーボードを閉じる
			if(stage)
				stage.focus = null;
		}
		
		/**
		 * editable変更時　処理
		 * @param value: editable値
		 * @author SE-362 
		 * @date 12.04.24
		 */
		override public function set editable(value: Boolean): void
		{
			super.editable = value;
			/*
			if(this.styleName){
				// 背景色を変更(editable == false時には背景色をグレーにする)
				if (value == false) {
					
					// 入力可能なスタイルの時のみ、背景色を変更
					if (_BasedMethod.getEnabled(this.styleName.toString()) == true)
					{
						this.setStyle("backgroundColor", 0xdddddd);
						this.tabEnabled = false;
					}
					
				} else {
					
					// 入力可能なスタイルの時のみ、背景色を変更
					if (_BasedMethod.getEnabled(this.styleName.toString()) == true)
					{
						clearStyle("backgroundColor");
						this.tabEnabled = _BasedMethod.getTabEnabled(this.styleName.toString());
					}
				}
			}
			*/
		}

		/**
		 * データ入力時処理
		 * @param　i: イベント 
		 * @author SE-362 
		 * @date 12.04.24
		 */
		public function changeHandler(event: Event): void
		{
			var strByteLen: Number;
			var strTemp: String;
			var tempChar: String;
			var str: String;
			var numMaxByte: Number;
			
			var i: Number;
			
			// 入力最大値をバイト数で制限(数値項目でないときのみ実行)
			if (_numOnly != "1") {
				strByteLen = 0;
				strTemp    = "";
				str        = this.text;
				
				for (i=0; i<str.length; i++) {
					
					tempChar = str.substr(i, 1);
					
					// i文字目のバイト数を取得して加算する
					strByteLen = strByteLen + singleton.sitemethod.getByteLen(str.substr(i, 1));
					
					// 最大バイト数を超えるまでの文字は画面に戻す
					if (strByteLen <= Number(maxByte)) {
						strTemp = strTemp + tempChar;
						
					} else{
						break;
					}
				}
				
				this.text = strTemp;
				
				// 数値項目時は算出した最大入力桁数にて制限
			} else {
				
				// 最大入力桁数の算出
				numMaxByte = 0;
				numMaxByte = numMaxByte + 1;													// マイナス記号
				numMaxByte = numMaxByte + BasedMethod.asCurrency(this.intNum);				// 整数桁
				numMaxByte = numMaxByte + int((BasedMethod.asCurrency(this.intNum)-1)/3);	// カンマ
				
				if (BasedMethod.asCurrency(this.realNum) != 0) {
					
					numMaxByte = numMaxByte + 1;												// 小数点
					numMaxByte = numMaxByte + BasedMethod.asCurrency(this.realNum);		// 小数桁
				}
				
				if (this.text.length > numMaxByte) {
					
					this.text = this.text.substr(0, numMaxByte);
				}
			}
			
			// フラグのセット
			modified = "2";   
		}    	

		/**
		 * 数値型フォーマット
		 * 
		 * @author SE-362 
		 * @date 12.04.24
		 */
		public function textNumberFormat(): String
		{			
			var mode: String;
			var _realNum: String;	// formatterに渡す用の小数部桁数			
			
			if (focusInFormat == true) {
				mode = "1";
				
			} else {
				mode = "0";
			}
			
			// 小数桁 一旦デフォルト値をセット
			_realNum = this.realNum;
			
			// 桁数チェックはvalueCommit時に行うため、
			// デフォルト値以上の桁数が入力されている場合はその桁を残す
			if (this.text.lastIndexOf(".") != -1) {
				if (this.text.length - (this.text.lastIndexOf(".") + 1) > Number(this.realNum)) {
					
					_realNum = (this.text.length - (this.text.lastIndexOf(".") + 1)).toString();
				}
			}
			
			return singleton.g_customnumberformatter.numberFormat(this.text, this.comma, _realNum, mode, this.round);
		}

		/**
		 * 数値型format (初期表示用)
		 * @param val: 値
		 * @return 文字列
		 * 
		 * @author SE-362 
		 * @date 12.04.24
		 */
		public function textNumberFormatObj(val: String): String
		{
			this.text = val;
			
			return textNumberFormat();
		}

		/**
		 * 変数_numOnly設定/取得
		 * @return _numOnlyプロパティ
		 * 
		 * @author SE-362 
		 * @date 12.04.24
		 */
		public function get numOnly(): String
		{
			return _numOnly;
		}
		
		/**
		 * modified
		 * @return modifiedプロパティ
		 * 
		 * @author SE-362 
		 * @date 12.04.24
		 */
		[Bindable("modifiedChanged")]
		public function get modified(): String
		{
			return _modified;
		}
		
		public function set modified(value: String): void
		{
			var w_modified: String;
			
			w_modified = _modified;
			
			_modified = value;
			
			if (BasedMethod.nvl(grid) != "") {
				if (w_modified != _modified) {
					if (_modified == "2") {
						// 10.11.09 gridId,disableFlgが常に親画面にしかセットされなかった不具合修正 SE-354 start		        		
						//		        		Application.application.gridId = grid;
						//		        		Application.application.disableFlg = "1";
						
						this.parentDocument.gridId = grid;
						this.parentDocument.disableFlg = "1";
						// 10.11.09 gridId,disableFlgが常に親画面にしかセットされなかった不具合修正 SE-354 end
					} else {
						// 10.11.09 gridId,disableFlgが常に親画面にしかセットされなかった不具合修正 SE-354 start		        		
						//		        		Application.application.gridId = "";
						//		        		Application.application.disableFlg = "";
						
						this.parentDocument.gridId = "";
						this.parentDocument.disableFlg = "";
						// 10.11.09 gridId,disableFlgが常に親画面にしかセットされなかった不具合修正 SE-354 end	
					}
				}
			}
			
			dispatchEvent(new Event("modifiedChanged"));
		}        

		/**
		 * 数値型format(項目チェック時)
		 * 
		 * @author SE-362 
		 * @date 12.04.24
		 */
		public function set setNumberFormat(value: String): void
		{
			if (BasedMethod.nvl(value) == "1")
			{
				this.text = textNumberFormat();
			}
		}
		
		private function removed(e: Event): void
		{
			removeEventListener(Event.CHANGE, changeHandler);
			removeEventListener(MouseEvent.MOUSE_OUT, MouseOutHandler);
			removeEventListener(Event.REMOVED_FROM_STAGE, removed);
		}
	}
}