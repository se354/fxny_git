package modules.custom
{
	import flash.events.MouseEvent;
	
	import modules.base.BasedMethod;
	import modules.formatters.CustomDateFormatter;
	
	import mx.resources.Locale;
	
	import spark.components.DateSpinner;
	
	/**
	 * 日付選択（DateSpinner）クラス
	 * 
	 * @author SE-362 
	 * @date 12.04.24
	 */
	public class CustomDateSpinner extends DateSpinner
	{
		// フォーマット形式（0:yyyy/mm/dd） 
		public var DateFormat: String;
		public var DateString: String;
		
		// 別クラス宣言
		private var _BasedMethod: BasedMethod;
		private var _CustomDateFormatter: CustomDateFormatter;

		/**
		 * コンストラクタ
		 * 
		 * @author SE-362 
		 * @date 12.04.24
		 */
		public function CustomDateSpinner()
		{
			super();
			
			// 別class宣言
			_BasedMethod = new BasedMethod();
			_CustomDateFormatter = new CustomDateFormatter();
			
			// イベント作成
			this.addEventListener(MouseEvent.CLICK, MouseDownHandler);

			// 日本語表記に設定
			this.setStyle("locale", "ja_JP");
		}

		/**
		 * 押下時
		 * 
		 * @author SE-362 
		 * @date 12.04.24
		 */
		public function MouseDownHandler(event: MouseEvent): void
		{
			if(_BasedMethod.nvl(DateFormat, "0") == "0"){
				DateString = _CustomDateFormatter.dateDisplayFormat_Full(super.selectedDate);
			}else{
				DateString = _CustomDateFormatter.dateDisplayFormat(super.selectedDate);
			}
		}
	}
}