package modules.custom
{
	import modules.base.BasedMethod;
	
	import spark.components.Label;
	
	/**
	 * ラベルクラス
	 * 
	 * @author SE-362 
	 * @date 12.05.07
	 */
	public class CustomLabel extends Label
	{

		private var _numOnly: String;
		private var singleton: CustomSingleton = CustomSingleton.getInstance();
		
		public var realNum: String;
		/**
		 * コンストラクタ
		 * 
		 * @author SE-362 
		 * @date 12.05.07
		 */
		public function CustomLabel()
		{
			super();
		}

		override protected function createChildren():void
		{
			if(this.styleName)
				_numOnly = BasedMethod.getNumOnly(this.styleName.toString());
			
			if(!height)
				height = 33;
			
		}
		
		override public function set text(value:String):void
		{
			super.text = value;
			
			// 数値項目の時は表示文字列にフォーマットをかける
			//			if ((_numOnly == "1") && (modified != "2")) {
			if (_numOnly == "1"){
				super.text = textNumberFormat();
			}
		}

		/**
		 * 数値型フォーマット
		 * 
		 * @author SE-362 
		 * @date 12.04.24
		 */
		public function textNumberFormat(): String
		{			
			var mode: String;
			var _realNum: String;	// formatterに渡す用の小数部桁数			
			
			// 小数桁 一旦デフォルト値をセット
			_realNum = this.realNum;
			
			// 桁数チェックはvalueCommit時に行うため、
			// デフォルト値以上の桁数が入力されている場合はその桁を残す
			if (this.text.lastIndexOf(".") != -1) {
				if (this.text.length - (this.text.lastIndexOf(".") + 1) > Number(this.realNum)) {
					
					_realNum = (this.text.length - (this.text.lastIndexOf(".") + 1)).toString();
				}
			}
			
			return singleton.g_customnumberformatter.numberFormat(this.text, "1", _realNum, mode);
		}
	}
}