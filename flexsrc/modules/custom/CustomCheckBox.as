package modules.custom
{
	import spark.components.CheckBox;
	
	/**************************************
	 * チェックボックスクラス
	 * 
	 * @author SE-362 
	 * @date 12.05.16
	 **************************************/
	public class CustomCheckBox extends CheckBox
	{
		/****************************************
		 * コンストラクタ
		 * 
		 * @author SE-362 
		 * @date 12.05.16
		 ****************************************/
		public function CustomCheckBox()
		{
			super();
		}
	}
}