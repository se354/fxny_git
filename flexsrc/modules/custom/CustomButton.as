package modules.custom
{
	import flash.events.KeyboardEvent;
	import flash.ui.Keyboard;
	
	import modules.custom.sub.CustomUITextFormat;
	
	import mx.core.IUITextField;
	import mx.core.UITextFormat;
	
	import spark.components.Button;
	
	/**
	 * ボタンクラス
	 * Enterキーで押下可
	 * 
	 * @author SE-362 
	 * @date 12.04.24
	 */
	public class CustomButton extends Button
	{
		// 別コンポーネント宣言
		private var _customuitextformat: CustomUITextFormat;		// 複数行表示フォーマット
		
		// Public変数
		public var customComp: String;								// カスタムコンポーネントかどうか
		
		public var multiLen1: String;								// ボタン文字列折り返しバイト数(1行目) 
		public var multiLen2: String;								// ボタン文字列折り返しバイト数(2行目) 
		public var multiLen3: String;								// ボタン文字列折り返しバイト数(3行目) 
		
		// 10.12.02 ToolTipにラベル以外の文字列も表示できるよう修正 SE-354 start
		private var _hintText: String;								// ToolTipに表示する文字列
		// 10.12.02 ToolTipにラベル以外の文字列も表示できるよう修正 SE-354 end
		
		/**
		 * コンストラクタ
		 * 
		 * @author SE-362 
		 * @date 12.04.24
		 */
		public function CustomButton()
		{
			super();
		}        
		
		/**
		 * 初期化処理
		 * 
		 * @author SE-362 
		 * @date 12.04.24
		 */
		public override function initialize(): void
		{   
			super.initialize();   
		}   
		
		/**
		 * 表示リスト更新
		 * 
		 * @author SE-362 
		 * @date 12.04.24
		 */
		protected override function updateDisplayList(unscaledWidth: Number, unscaledHeight: Number): void
		{   
			var labeltextField: IUITextField;
			
//			labeltextField = this.textField;
			
			super.updateDisplayList(unscaledWidth, unscaledHeight);   
		}   

		/**
		 * UITextFormatの取得
		 * 
		 * @author SE-362 
		 * @date 12.04.24
		 */
		public override function determineTextFormatFromStyles(): UITextFormat
		{
			var mtextFormat: CustomUITextFormat;
			
			mtextFormat = _customuitextformat;
			
			if (!mtextFormat) {
				mtextFormat = getCustomUITextFormat();
				_customuitextformat = mtextFormat;
			}
			
			return mtextFormat;
		}
		
		/**
		 * キーダウン時 処理
		 * @param event: キーボードイベント 
		 * 
		 * @author SE-362 
		 * @date 12.04.24
		 */
		override protected function keyDownHandler(event: KeyboardEvent): void 
		{
			// Enterキー押下時
			if (event.keyCode == Keyboard.ENTER) {
				
				// Spaceキー押下に置き換え
				event.keyCode = Keyboard.SPACE;
			}
			
			super.keyDownHandler(event);
		}
		
		/**
		 * キーアップ時 処理
		 * @param event: キーボードイベント 
		 * 
		 * @author SE-362 
		 * @date 12.04.24
		 */
		override protected function keyUpHandler(event: KeyboardEvent): void 
		{
			// Enterキー押下時
			if (event.keyCode == Keyboard.ENTER) {
				
				// Spaceキー押下に置き換え	
				event.keyCode = Keyboard.SPACE;
			}
			
			super.keyUpHandler(event);	
		} 
		
		/**
		 * フォーカスセット
		 * 
		 * @author SE-362 
		 * @date 12.04.24
		 */
		override public function setFocus():void
		{
			super.setFocus();
			
			// フォーカス自体は行くが、外枠が表示されない点を改善
			if (this.visible) {
				this.drawFocus(true);
			} else {
				this.drawFocus(false);
			}
		}
		
		/**
		 * カスタムフォーマット情報の取得
		 * 
		 * @author SE-362 
		 * @date 12.04.24
		 */
		private function getCustomUITextFormat(): CustomUITextFormat
		{
			var mtextFormat: CustomUITextFormat;
			
			mtextFormat = new CustomUITextFormat(systemManager);
			
			mtextFormat.align = inheritingStyles.textAlign;
			mtextFormat.bold  = inheritingStyles.fontWeight == "bold";
			mtextFormat.color = enabled ?
				inheritingStyles.color :
				inheritingStyles.disabledColor;
			mtextFormat.font = inheritingStyles.fontFamily;
			mtextFormat.indent = inheritingStyles.textIndent;
			mtextFormat.italic = inheritingStyles.fontStyle == "italic";
			mtextFormat.leading = nonInheritingStyles.leading;
			mtextFormat.leftMargin = nonInheritingStyles.paddingLeft;
			mtextFormat.rightMargin = nonInheritingStyles.paddingRight;
			mtextFormat.size = inheritingStyles.fontSize;
			mtextFormat.underline = nonInheritingStyles.textDecoration == "underline";
			
			mtextFormat.antiAliasType = inheritingStyles.fontAntiAliasType;
			mtextFormat.gridFitType = inheritingStyles.fontGridFitType;
			mtextFormat.sharpness = inheritingStyles.fontSharpness;
			mtextFormat.thickness = inheritingStyles.fontThickness;
			
			return     mtextFormat;
		}
}
}