package modules.dto
{
	[Bindable]
	[RemoteClass(alias="modules.entity.LF010")]

	/**********************************************************
	 * 訪問計画実績F 項目定義.
	 * @name LF010
	 * @author 12.09.19 SE-354
	 **********************************************************/ 
	public class LF010
	{
		public var CHGCD :String;			// CHGCD(VARCHAR2)
		public var HDAT :String;			// HDAT(VARCHAR2)
		public var TKCD :String;			// TKCD(VARCHAR2)
		public var ABSENT :String;			// ABSENT(VARCHAR2)
		public var AHTIME :String;			// ATIME(VARCHAR2)
		public var HHTIME :String;			// HTIME(VARCHAR2)
		public var BHTIME :String;			// BTIME(VARCHAR2)
		public var NHTIME :String;			// NTIME(VARCHAR2)
		public var COM:String;				// COM(VARCHAR2)
		// 12.09.26 Add
		public var ABTIME1 :String;		// ABTIME1(VARCHAR2)
		public var ABTIME2 :String;		// ABTIME2(VARCHAR2)
		public var ABTIME3 :String;		// ABTIME3(VARCHAR2)
		// 12.09.28 Add
		public var UPDFLG :String;			// UPDFLG(VARCHAR2)
	}
}