package modules.dto
{
	/**********************************************************
	 * 和暦年号Ｆ.
	 * @name LS014
	 * @author 12.11.05 SE-300
	 **********************************************************/ 
	public class LS014
	{
		public var CLS :String;				// CLS(VARCHAR2)
		public var ERA :String;				// ERA(VARCHAR2)
		public var SDAT :String;			// SDAT(VARCHAR2)
		public var FLG :String;				// FLG(VARCHAR2)
		public var ERAS :String;			// ERAS(VARCHAR2)
	}
}