package modules.dto
{
	import modules.base.BasedMethod;

	/**********************************************************
	 * 地区詳細マスタ 項目定義.
	 * @name LM016
	 * @author 12.10.16 SE-300
	 **********************************************************/ 
	public class LM016
	{
		public var AREA :String;			// AREA(VARCHAR2)
		public var DTAREA :String;			// DTAREA(VARCHAR2)
		public var DTAREANM :String;		// DTAREANM(VARCHAR2)
		
		public function get AREADTAREA(): String
		{
			return BasedMethod.nvl(AREA) + "-" + BasedMethod.nvl(DTAREA);
		}
	}
}