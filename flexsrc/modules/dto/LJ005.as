package modules.dto
{
	[Bindable]
	[RemoteClass(alias="modules.entity.LJ005")]

	/**********************************************************
	 * 入金J
	 * @name LJ005
	 * @author 12.09.28 SE-354
	 **********************************************************/ 
	public class LJ005
	{
		public var VNO :String;			// VNO(VARCHAR2)
		public var TKCD :String;			// TKCD(VARCHAR2)
		public var HDAT :String;			// HDAT(VARCHAR2)
		public var HTIME :String;			// HTIME(VARCHAR2)
		public var CHGCD :String;			// CHGCD(VARCHAR2)
		public var NCLS :String;			// NCLS(VARCHAR2)
		public var HTID :String;			// HTID(VARCHAR2)
		public var MEDIBAL :String;		// MEDIBAL(VARCHAR2)
		public var MEDIMONEY :String;		// MEDIMONEY(VARCHAR2)
		public var MEDIDIS :String;		// MEDIDIS(VARCHAR2)
		public var MEDIKAS :String;		// MEDIKAS(VARCHAR2)
		public var MEDIRTAX :String;		// MEDIRTAX(VARCHAR2)
		public var MEDINBAL :String;		// MEDINBAL(VARCHAR2)
		public var STOREBAL :String;		// STOREBAL(VARCHAR2)
		public var STOREMONEY :String;		// STOREMONEY(VARCHAR2)
		public var STOREDIS :String;		// STOREDIS(VARCHAR2)
		public var STOREKAS :String;		// STOREKAS(VARCHAR2)
		public var STORERTAX :String;		// STORERTAX(VARCHAR2)
		public var STORENBAL :String;		// STORENBAL(VARCHAR2)
		public var DEVBAL :String;			// DEVBAL(VARCHAR2)
		public var DEVMONEY :String;		// DEVMONEY(VARCHAR2)
		public var DEVDIS :String;			// DEVDIS(VARCHAR2)
		public var DEVKAS :String;			// DEVKAS(VARCHAR2)
		public var DEVRTAX :String;		// DEVRTAX(VARCHAR2)
		public var DEVNBAL :String;		// DEVNBAL(VARCHAR2)
		public var MEDIBAL2 :String;		// MEDIBAL2(VARCHAR2)
		public var MEDIMONEY2 :String;		// MEDIMONEY2(VARCHAR2)
		public var MEDIDIS2 :String;		// MEDIDIS2(VARCHAR2)
		public var MEDIKAS2 :String;		// MEDIKAS2(VARCHAR2)
		public var MEDIRTAX2 :String;		// MEDIRTAX2(VARCHAR2)
		public var MEDINBAL2 :String;		// MEDINBAL2(VARCHAR2)
		public var STOREBAL2 :String;		// STOREBAL2(VARCHAR2)
		public var STOREMONEY2 :String;		// STOREMONEY2(VARCHAR2)
		public var STOREDIS2 :String;		// STOREDIS2(VARCHAR2)
		public var STOREKAS2 :String;		// STOREKAS2(VARCHAR2)
		public var STORERTAX2 :String;		// STORERTAX2(VARCHAR2)
		public var STORENBAL2 :String;		// STORENBAL2(VARCHAR2)
		public var DEVBAL2 :String;			// DEVBAL2(VARCHAR2)
		public var DEVMONEY2 :String;		// DEVMONEY2(VARCHAR2)
		public var DEVDIS2 :String;			// DEVDIS2(VARCHAR2)
		public var DEVKAS2 :String;			// DEVKAS2(VARCHAR2)
		public var DEVRTAX2 :String;		// DEVRTAX2(VARCHAR2)
		public var DEVNBAL2 :String;		// DEVNBAL2(VARCHAR2)
		public var HYDAY :String;
	}
}