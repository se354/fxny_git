package modules.dto
{
	import modules.custom.CustomSingleton;

	/**********************************************************
	 * 商品別配置履歴
	 * @name MA042V01
	 * @author 12.09.10 SE-362
	 **********************************************************/ 
	public class MA042V01
	{
		public var HDAT: String;
		public var VPNO: String;

		private var singleton: CustomSingleton = CustomSingleton.getInstance();
		
		// 数量
		public function get VPNO_COMMA(): String
		{
			return singleton.g_customnumberformatter.numberFormat(VPNO, "1", "0", "0"); 
		}
	}
}