package modules.dto
{
	import modules.custom.CustomSingleton;

	/**********************************************************
	 * 入金入力（別売履歴） 項目定義.
	 * @name MA050V02
	 * @author 12.09.10 SE-362
	 **********************************************************/ 
	public class MA050V02
	{
		public var TKCD: String;
		public var HDAT: String;
		public var VJAN: String;
		public var GDNM: String;
		public var VPNO: String;
		public var AMT: String;
		public var NAMT: String;
		public var DISC: String;
		public var SAMT: String;

		private var singleton: CustomSingleton = CustomSingleton.getInstance();
		
		// 日付
		public function get HDAT_D(): String
		{
			//return singleton.g_customdateformatter.dateDisplayFormat(HDAT);
			return HDAT;
		}
		
		// 売上額
		public function get AMT_COMMA(): String
		{
			return singleton.g_customnumberformatter.numberFormat(AMT, "1", "0", "0"); 
		}
		
		// 数量
		public function get VPNO_COMMA(): String
		{
			return singleton.g_customnumberformatter.numberFormat(VPNO, "1", "0", "0"); 
		}
		
		// 集金額
		public function get NAMT_COMMA(): String
		{
			return singleton.g_customnumberformatter.numberFormat(NAMT, "1", "0", "0"); 
		}
		
		// 値引
		public function get DISC_COMMA(): String
		{
			return singleton.g_customnumberformatter.numberFormat(DISC, "1", "0", "0"); 
		}
		
		// 売掛額
		public function get SAMT_COMMA(): String
		{
			return singleton.g_customnumberformatter.numberFormat(SAMT, "1", "0", "0"); 
		}
		
	}
}