package modules.dto
{
	[Bindable]
	[RemoteClass(alias="modules.entity.LJ003")]

	/**********************************************************
	 * 別売売上JA(ヘッダ).
	 * @name LJ003
	 * @author 12.09.28 SE-354
	 **********************************************************/ 
	public class LJ003
	{
		public var VNO :String;			// VNO(VARCHAR2)
		public var TKCD :String;			// TKCD(VARCHAR2)
		public var HDAT :String;			// HDAT(VARCHAR2)
		public var HTIME :String;			// HTIME(VARCHAR2)
		public var SCLS :String;			// SCLS(VARCHAR2)
		public var CHGCD :String;			// CHGCD(VARCHAR2)
		public var HTID :String;			// HTID(VARCHAR2)
		public var MEDIBAL :String;		// MEDIBAL(VARCHAR2)
		public var MEDIWITHTAX :String;	// MEDIWITHTAX(VARCHAR2)
		public var MEDIWITHOUTTAX :String;			// MEDIWITHOUTTAX(VARCHAR2)
		public var MEDIAMT :String;		// MEDIAMT(VARCHAR2)
		public var MEDITAX :String;		// MEDITAX(VARCHAR2)
		public var MEDIMONEY :String;		// MEDIMONEY(VARCHAR2)
		public var MEDIDIS :String;		// MEDIDIS(VARCHAR2)
		public var MEDIKAS :String;		// MEDIKAS(VARCHAR2)
		public var MEDIRTAX :String;		// MEDIRTAX(VARCHAR2)
		public var MEDINBAL :String;		// MEDINBAL(VARCHAR2)
		public var STOREBAL :String;		// STOREBAL(VARCHAR2)
		public var STOREWITHTAX :String;	// STOREWITHTAX(VARCHAR2)
		public var STOREWITHOUTTAX :String;	// STOREWITHOUTTAX(VARCHAR2)
		public var STOREAMT :String;		// STOREAMT(VARCHAR2)
		public var STORETAX :String;		// STORETAX(VARCHAR2)
		public var STOREMONEY :String;		// STOREMONEY(VARCHAR2)
		public var STOREDIS :String;		// STOREDIS(VARCHAR2)
		public var STOREKAS :String;		// STOREKAS(VARCHAR2)
		public var STORERTAX :String;		// STORERTAX(VARCHAR2)
		public var STORENBAL :String;		// STORENBAL(VARCHAR2)
		public var DEVBAL :String;			// DEVBAL(VARCHAR2)
		public var DEVWITHTAX :String;		// DEVWITHTAX(VARCHAR2)
		public var DEVWITHOUTTAX :String;	// DEVWITHOUTTAX(VARCHAR2)
		public var DEVAMT :String;			// DEVAMT(VARCHAR2)
		public var DEVTAX :String;			// DEVTAX(VARCHAR2)
		public var DEVMONEY :String;		// DEVMONEY(VARCHAR2)
		public var DEVDIS :String;			// DEVDIS(VARCHAR2)
		public var DEVKAS :String;			// DEVKAS(VARCHAR2)
		public var DEVRTAX :String;		// DEVRTAX(VARCHAR2)
		public var DEVNBAL :String;		// DEVNBAL(VARCHAR2)
		public var MEDIZMNY :String;		// MEDIZMNY(VARCHAR2)
		public var STOREZMNY :String;		// STOREZMNY(VARCHAR2)
		public var DEVZMNY :String;		// DEVZMNY(VARCHAR2)
		public var HYDAY :String;
		
	}
}