package modules.dto
{
	import modules.custom.CustomSingleton;
	/**********************************************************
	 * 商品マスタ 項目定義.
	 * @name LM006
	 * @author 12.08.08 SE-354
	 **********************************************************/ 
	public class LM006
	{
		public var VJAN :String;			// VJAN(VARCHAR2)
		public var GDNM :String;			// GDNM(VARCHAR2)
		public var STCST :String;			// STCST(VARCHAR2)
		public var DCLS :String;			// DCLS(VARCHAR2)
		public var GDCLS :String;			// GDCLS(VARCHAR2)
		public var MDCLS :String;			// MDCLS(VARCHAR2)
		public var DSCLS :String;			// DSCLS(VARCHAR2)
		public var OFCLS :String;			// OFCLS(VARCHAR2)
		public var GDKNM :String;			// GDKNM(VARCHAR2)
		public var GDCOM :String;			// GDCOM(VARCHAR2)
		public var ATTENT :String;			// ATTENT(VARCHAR2)
		// Add 12.09.26
		public var MEMO :String;			// MEMO(VARCHAR2)
		public var DISRT :String;			// DISRT(VARCHAR2)
		// Add 12.10.03
		public var GDCLSNM: String;
		public var MDCLSNM: String;
		public var MDCLSNMS: String;
		// 12.10.29 SE-233 Redmine#3561 対応 start
		public var STCST1: String;
		// Add 12.11.05
		public var DISCST :String;

		private var singleton: CustomSingleton = CustomSingleton.getInstance();
		// 数量
		public function get STCST1_COMMA(): String
		{
			return singleton.g_customnumberformatter.numberFormat(STCST1, "1", "0", "0"); 
		}
		// 12.10.29 SE-233 Redmine#3561 対応 end
	}
}