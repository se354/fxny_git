package modules.dto
{
	/**********************************************************
	 * 月別平均実績F 項目定義.
	 * @name LF007
	 * @author 12.09.26 SE-354
	 **********************************************************/ 
	public class LF007
	{
		public var TKCD :String;			// TKCD(VARCHAR2)
		public var SMON :String;			// SMON(VARCHAR2)
		public var AMT :String;			// AMT(VARCHAR2)
	}
}