package modules.dto
{
	/**********************************************************
	 * 別売販売履歴F 項目定義.
	 * @name LF003
	 * @author 12.09.26 SE-354
	 **********************************************************/ 
	public class LF003
	{
		public var TKCD :String;			// TKCD(VARCHAR2)
		public var HDAT :String;			// HDAT(VARCHAR2)
		public var HTIME :String;			// HTIME(VARCHAR2)
		public var VJAN :String;			// VJAN(VARCHAR2)
		public var GDNM :String;			// GDNM(VARCHAR2)
		public var VPNO :String;			// VPNO(VARCHAR2)
		public var AMT :String;				// AMT(VARCHAR2)
		public var DISC :String;			// DISC(VARCHAR2)
		public var SAMT :String;			// SAMT(VARCHAR2)
		public var NAMT :String;			// NAMT(VARCHAR2)
		public var NKAMT :String;			// NKAMT(VARCHAR2)
		public var BAL:String;				// BAL(VARCHAR2)
		public var VLIN :String;			// VLIN(VARCHAR2)
	}
}