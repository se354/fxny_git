package modules.dto
{
	[Bindable]
	[RemoteClass(alias="modules.entity.S013")]

	/**********************************************************
	 * 通しNOF
	 * @name S013
	 * @author 12.11.19 SE-300
	 **********************************************************/ 
	public class LS013
	{
		public var HTID :String;			// HTID(VARCHAR2)
		public var CHGCD :String;			// CHGCD(VARCHAR2)
		public var UPDDAT :String;			// UPDDAT(VARCHAR2)
		public var UPDTIME :String;			// UPDTIME(VARCHAR2)
		public var TNO :String;				// TNO(VARCHAR2)
	}
}