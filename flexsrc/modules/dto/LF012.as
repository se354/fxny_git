package modules.dto
{
	/**********************************************************
	 * 配置薬販売履歴F(照会用) 項目定義.
	 * @name LF012
	 * @author 12.09.19 SE-354
	 **********************************************************/ 
	public class LF012
	{
		public var TKCD    : String;
		public var LINNO   : String;
		public var TITNM   : String;
		public var HBAYI01 : String;
		public var HBAYI02 : String;
		public var HBAYI03 : String;
		public var HBAYI04 : String;
		public var HBAYI05 : String;
		public var HBAYI06 : String;
		public var HBAYI07 : String;
		public var HBAYI08 : String;
		public var HBAYI09 : String;
		public var HBAYI10 : String;
		public var HBAYI11 : String;
		public var HBAYI12 : String;
		public var HBAYI13 : String;
		public var HBAYI14 : String;
		public var HBAYI15 : String;
		public var HBAYI16 : String;
		public var HBAYI17 : String;
		public var HBAYI18 : String;
		public var HBAYI19 : String;
		public var HBAYI20 : String;
		public var HBAYI21 : String;
		public var HBAYI22 : String;
		public var HBAYI23 : String;
		public var HBAYI24 : String;
		public var HBAYI25 : String;
		public var HBAYI26 : String;
		public var HBAYI27 : String;
		public var HBAYI28 : String;
		public var HBAYI29 : String;
		public var HBAYI30 : String;
	}
}