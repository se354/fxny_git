package modules.dto
{
	/**********************************************************
	 * 別売入力(ヘッダ) 項目定義.
	 * @name W_MA030X
	 * @author 12.08.16 SE-362
	 **********************************************************/ 
	public class W_MA030X
	{
		public var VNO :String;	// 伝票NO
		public var TKCD :String;	// 得意先コード
		public var CHGCD :String;	// 担当者コード
		public var MEDIBAL :String;	// 前回繰越(医薬品)
		public var STOREBAL :String;	// 前回繰越(薬店)
		public var DEVBAL :String;		// 前回繰越(医療具)
		public var WITHTAX :String;	// 使用金額(税込)
		public var WITHOUTTAX :String;	// 使用金額(税抜)
		public var DIS :String;
		public var AMT :String;
		public var TAX :String;
	}
}