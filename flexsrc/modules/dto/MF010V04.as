package modules.dto
{
	import modules.custom.CustomSingleton;

	/**********************************************************
	 * 顧客元帳（別売履歴） 項目定義.
	 * @name MF010V04
	 * @author 12.09.13 SE-233
	 **********************************************************/ 
	public class MF010V04
	{
		public var TKCD: String;
		public var HDAT: String;
		public var GDNM: String;
		public var VPNO: String;
		public var AMT: String;
		public var NAMT: String;
		public var DISC: String;
		public var UAMT: String;

		private var singleton: CustomSingleton = CustomSingleton.getInstance();
		
		// 日付
		public function get HDAT_D(): String
		{
			return HDAT;
		}
		
		// 数量
		public function get VPNO_COMMA(): String
		{
			return singleton.g_customnumberformatter.numberFormat(VPNO, "1", "0", "0"); 
		}
		
		// 売上額
		public function get AMT_COMMA(): String
		{
			return singleton.g_customnumberformatter.numberFormat(AMT, "1", "0", "0"); 
		}
		
		// 集金額
		public function get NAMT_COMMA(): String
		{
			return singleton.g_customnumberformatter.numberFormat(NAMT, "1", "0", "0"); 
		}
		
		// 値引額
		public function get DISC_COMMA(): String
		{
			return singleton.g_customnumberformatter.numberFormat(DISC, "1", "0", "0"); 
		}
		
		// 売掛額
		public function get UAMT_COMMA(): String
		{
			return singleton.g_customnumberformatter.numberFormat(UAMT, "1", "0", "0"); 
		}
		
	}
}