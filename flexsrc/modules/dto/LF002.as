package modules.dto
{
	/**********************************************************
	 * 配置薬販売履歴FB 項目定義.
	 * @name LF002
	 * @author 12.09.26 SE-354
	 **********************************************************/ 
	public class LF002
	{
		public var TKCD :String;			// TKCD(VARCHAR2)
		public var HDAT :String;			// HDAT(VARCHAR2)
		public var HTIME :String;			// HTIME(VARCHAR2)
		public var VJAN :String;			// VJAN(VARCHAR2)
		public var GDNM :String;			// GDNM(VARCHAR2)
		public var VPNO :String;			// VPNO(VARCHAR2)
	}
}