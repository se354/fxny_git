package modules.dto
{
	/**********************************************************
	 * 重点区分マスタ 項目定義.
	 * @name LM015
	 * @author 12.10.16 SE-300
	 **********************************************************/ 
	public class LM015
	{
		public var IMPCLS :String;			// IMPCLS(VARCHAR2)
		public var IMPCLSNM :String;		// IMPCLSNM(VARCHAR2)
	}
}