package modules.dto
{
	/**********************************************************
	 * 商品別配置薬実績F 項目定義.
	 * @name LF011
	 * @author 12.09.19 SE-354
	 **********************************************************/ 
	public class LF011
	{
		public var VJAN :String;			// VJAN(VARCHAR2)
		public var SMON :String;			// SMON(VARCHAR2)
		public var REPVPNO :String;		// REPVPNO(VARCHAR2)
	}
}