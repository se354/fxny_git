package modules.dto
{
	import modules.base.BasedMethod;

	/**********************************************************
	 * 訪問時間マスタ 項目定義.
	 * @name LM017
	 * @author 12.11.12 SE-300
	 **********************************************************/ 
	public class LM017
	{
		public var HTIMECD :String;			// HTIMECD(VARCHAR2)
		public var HTIMENM :String;			// HTIMENM(VARCHAR2)
		
	}
}