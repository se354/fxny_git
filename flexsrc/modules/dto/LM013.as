package modules.dto
{
	/**********************************************************
	 * 商品分類マスタ 項目定義.
	 * @name LM013
	 * @author 12.08.10 SE-354
	 **********************************************************/ 
	public class LM013
	{
		public var GDCLS :String;			// GDCLS(VARCHAR2)
		public var GDCLSNM :String;		// GDCLSNM(VARCHAR2)
	}
}