package modules.dto
{
	/**********************************************************
	 * 担当者別在庫マスタ 項目定義.
	 * @name LM008
	 * @author 12.08.08 SE-354
	 **********************************************************/ 
	public class LM008
	{
		public var CHGCD :String;			// CHGCD(VARCHAR2)
		public var VJAN :String;			// VJAN(VARCHAR2)
		public var VPNO :String;			// VPNO(VARCHAR2)
		public var MVPNO :String;			// MVPNO(VARCHAR2)
	}
}