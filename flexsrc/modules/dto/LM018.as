package modules.dto
{
	import modules.base.BasedMethod;

	/**********************************************************
	 * 得意先区分マスタ 項目定義.
	 * @name LM018
	 * @author 12.11.12 SE-300
	 **********************************************************/ 
	public class LM017
	{
		public var TKCLS :String;			// TKCLS(VARCHAR2)
		public var TKCLSNM :String;			// TKCLSNM(VARCHAR2)
		
	}
}