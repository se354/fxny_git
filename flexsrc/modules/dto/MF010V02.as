package modules.dto
{
	import modules.custom.CustomSingleton;

	/**********************************************************
	 * 顧客元帳（訪問履歴_明細部） 項目定義.
	 * @name MF010V02
	 * @author 12.09.13 SE-233
	 **********************************************************/ 
	public class MF010V02
	{
		public var TKCD: String;
		public var HDAT: String;
		public var WITHTAX: String;
		public var AMT1: String;
		public var DIS: String;
		public var MONEY: String;
		public var UAMT1: String;
		public var AMT2: String;
		public var SAMT: String;
		public var DISC: String;
		public var NAMT: String;
		public var UAMT2: String;

		private var singleton: CustomSingleton = CustomSingleton.getInstance();
		
		// 日付
		public function get HDAT_D(): String
		{
			return HDAT;
		}
		
		// 配置薬_売上額
		public function get WITHTAX_COMMA(): String
		{
			return singleton.g_customnumberformatter.numberFormat(WITHTAX, "1", "0", "0"); 
		}
		
		// 配置薬_請求額
		public function get AMT1_COMMA(): String
		{
			return singleton.g_customnumberformatter.numberFormat(AMT1, "1", "0", "0"); 
		}
		
		// 配置薬_値引額
		public function get DIS_COMMA(): String
		{
			return singleton.g_customnumberformatter.numberFormat(DIS, "1", "0", "0"); 
		}
		
		// 配置薬_集金額
		public function get MONEY_COMMA(): String
		{
			return singleton.g_customnumberformatter.numberFormat(MONEY, "1", "0", "0"); 
		}
		
		// 配置薬_売掛額
		public function get UAMT1_COMMA(): String
		{
			return singleton.g_customnumberformatter.numberFormat(UAMT1, "1", "0", "0"); 
		}
		
		// 別売_売上額
		public function get AMT2_COMMA(): String
		{
			return singleton.g_customnumberformatter.numberFormat(AMT2, "1", "0", "0"); 
		}
		
		// 別売_請求額
		public function get SAMT_COMMA(): String
		{
			return singleton.g_customnumberformatter.numberFormat(SAMT, "1", "0", "0"); 
		}
		
		// 別売_値引額
		public function get DISC_COMMA(): String
		{
			return singleton.g_customnumberformatter.numberFormat(DISC, "1", "0", "0"); 
		}
		
		// 別売_集金額
		public function get NAMT_COMMA(): String
		{
			return singleton.g_customnumberformatter.numberFormat(NAMT, "1", "0", "0"); 
		}
		
		// 別売_売掛額
		public function get UAMT2_COMMA(): String
		{
			return singleton.g_customnumberformatter.numberFormat(UAMT2, "1", "0", "0"); 
		}
		
	}
}