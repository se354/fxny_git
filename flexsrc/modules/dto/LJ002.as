package modules.dto
{
	[Bindable]
	[RemoteClass(alias="modules.entity.LJ002")]

	/**********************************************************
	 * 配置薬売上JB(明細).
	 * @name LJ002
	 * @author 12.08.30 SE-354
	 **********************************************************/ 
	public class LJ002
	{
		public var VNO :String;			// VNO(VARCHAR2)
		public var VSEQ :String;			// VSEQ(VARCHAR2)
		public var VJAN :String;			// VJAN(VARCHAR2)
		public var SAL :String;			// SAL(VARCHAR2)
		public var BCLS :String;			// BCLS(VARCHAR2)
		public var SCLS :String;			// SCLS(VARCHAR2)
		public var ZHVPNO :String;			// ZHVPNO(VARCHAR2)
		public var KZAN :String;			// KZAN(VARCHAR2)
		public var KUVPNO :String;			// KUVPNO(VARCHAR2)
		public var KNVPNO :String;			// KNVPNO(VARCHAR2)
		public var KRVPNO :String;			// KRVPNO(VARCHAR2)
		public var KLVPNO :String;			// KLVPNO(VARCHAR2)
		public var KHVPNO :String;			// KHVPNO(VARCHAR2)
		public var BHISK :String;			// BHISK(VARCHAR2)
	}
}