package modules.dto
{
	/**********************************************************
	 * 得意先区分Ｆ.
	 * @name LS010
	 * @author 12.09.14 SE-233
	 **********************************************************/ 
	public class LS010
	{
		public var CLS :String;				// CLS(VARCHAR2)
		public var CLSNM1 :String;			// CLSNM1(VARCHAR2)
		public var CLSNM2 :String;			// CLSNM2(VARCHAR2)
		public var CLSNM3 :String;			// CLSNM3(VARCHAR2)
		public var CLSNM4 :String;			// CLSNM4(VARCHAR2)
		public var CLSNM5 :String;			// CLSNM5(VARCHAR2)
		public var CLSNM6 :String;			// CLSNM6(VARCHAR2)
		public var CLSNM7 :String;			// CLSNM7(VARCHAR2)
		public var CLSNM8 :String;			// CLSNM8(VARCHAR2)
	}
}