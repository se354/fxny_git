package modules.dto
{
	import modules.custom.CustomSingleton;
	
	/**********************************************************
	 * 提供数ポップアップ 項目定義.
	 * @name MP060V01
	 * @author 12.09.10 SE-362
	 **********************************************************/
	[Bindable]
	public class MP060V01
	{
		public var GDCLS: String;
		public var VJAN: String;
		public var GDNM: String;
		public var TVPNO: String;
	}
}