package modules.dto
{
	import modules.custom.CustomSingleton;

	/**********************************************************
	 * 顧客元帳（訪問履歴_ヘッダ部） 項目定義.
	 * @name MF010V01
	 * @author 12.09.13 SE-233
	 **********************************************************/ 
	public class MF010V01
	{
		public var DOWDAT: String;
		public var TKCD: String;
		public var HMEDIKURI: String;
		public var HSTOREKURI: String;
		public var HDEVKURI: String;
		public var GMEDIKURI: String;
		public var GSTOREKURI: String;
		public var GDEVKURI: String;

		private var singleton: CustomSingleton = CustomSingleton.getInstance();
		
		// 日付
		public function get DOWDAT_D(): String
		{
			return DOWDAT;
		}
		
		// 配置薬_医薬品
		public function get HMEDIKURI_COMMA(): String
		{
			return singleton.g_customnumberformatter.numberFormat(HMEDIKURI, "1", "0", "0"); 
		}
		
		// 配置薬_薬店
		public function get HSTOREKURI_COMMA(): String
		{
			return singleton.g_customnumberformatter.numberFormat(HSTOREKURI, "1", "0", "0"); 
		}
		
		// 配置薬_医療具
		public function get HDEVKURI_COMMA(): String
		{
			return singleton.g_customnumberformatter.numberFormat(HDEVKURI, "1", "0", "0"); 
		}
		
		// 別売_医薬品
		public function get GMEDIKURI_COMMA(): String
		{
			return singleton.g_customnumberformatter.numberFormat(GMEDIKURI, "1", "0", "0"); 
		}
		
		// 別売_薬店
		public function get GSTOREKURI_COMMA(): String
		{
			return singleton.g_customnumberformatter.numberFormat(GSTOREKURI, "1", "0", "0"); 
		}
		
		// 別売_医療具
		public function get GDEVKURI_COMMA(): String
		{
			return singleton.g_customnumberformatter.numberFormat(GDEVKURI, "1", "0", "0"); 
		}
		
	}
}