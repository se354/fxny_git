package modules.dto
{
	import modules.custom.CustomSingleton;

	/**********************************************************
	 * 配置薬入替　グラフ用
	 * @name MA042V02
	 * @author 12.09.10 SE-362
	 **********************************************************/ 
	public class MA042V02
	{
		public var VJAN: String;
		public var SMON: String;
		public var REPVPNO: String;

		private var singleton: CustomSingleton = CustomSingleton.getInstance();
		
		// 数量
		public function get REPVPNO_COMMA(): String
		{
			return singleton.g_customnumberformatter.numberFormat(REPVPNO, "1", "0", "0"); 
		}
	}
}