package modules.dto
{
	import modules.base.BasedMethod;

	/**********************************************************
	 * 汎用 項目定義.
	 * @name LM006
	 * @author 12.08.08 SE-354
	 **********************************************************/ 
	public class HANYOU
	{
		public var CODE :String;			// VJAN(VARCHAR2)
		public var NAME :String;			// GDNM(VARCHAR2)

		// コンボ汎用表示用
		public function get COMBFIELD(): String
		{
			return BasedMethod.nvl(CODE) + " | " + BasedMethod.nvl(NAME); 
		}
		// コード桁数1桁
		public function get COMBFIELD1(): String
		{
			if(BasedMethod.nvl(CODE) == "" && BasedMethod.nvl(NAME) == ""){
				return "未選択";	
			}else{
				return (BasedMethod.nvl(CODE) + " ").substr(0, 1) + " | " + BasedMethod.nvl(NAME); 
			}
		}
		// コード桁数2桁
		public function get COMBFIELD2(): String
		{
			if(BasedMethod.nvl(CODE) == "" && BasedMethod.nvl(NAME) == ""){
				return "未選択";	
			}else{
				return (BasedMethod.nvl(CODE) + "  ").substr(0, 2) + " | " + BasedMethod.nvl(NAME);
			}
		}
		
		// コード桁数3桁
		public function get COMBFIELD3(): String
		{
			if(BasedMethod.nvl(CODE) == "" && BasedMethod.nvl(NAME) == ""){
				return "未選択";	
			}else{
				return (BasedMethod.nvl(CODE) + "   ").substr(0, 3) + " | " + BasedMethod.nvl(NAME);
			}
		}
	}
}