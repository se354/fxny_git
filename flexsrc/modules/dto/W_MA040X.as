package modules.dto
{
	/**********************************************************
	 * 配置薬入替 ヘッダ 項目定義.
	 * @name W_MA040X
	 * @author 12.08.16 SE-362
	 **********************************************************/
	[Bindable]
	public class W_MA040X
	{
		public var VNO: String;
		public var TKCD: String;
		public var HDAT: String;
		public var HTIME: String;
		public var LHDAT: String;
		public var SCLS: String;
		public var CHGCD: String;
		public var HTID: String;
		public var MEDIBAL: String;
		public var MEDIWITHTAX: String;
		public var MEDIWITHOUTTAX: String;
		public var MEDIAMT: String;
		public var MEDITAX: String;
		public var MEDIMONEY: String;
		public var MEDIDIS: String;
		public var MEDIDISF: String;
		public var MEDIDISRT: String;
		public var MEDIKAS: String;
		public var MEDIRTAX: String;
		public var MEDINBAL: String;
		public var STOREBAL: String;
		public var STOREWITHTAX: String;
		public var STOREWITHOUTTAX: String;
		public var STOREAMT: String;
		public var STORETAX: String;
		public var STOREMONEY: String;
		public var STOREDIS: String;
		public var STOREDISF: String;
		public var STOREDISRT: String;
		public var STOREKAS: String;
		public var STORERTAX: String;
		public var STORENBAL: String;
		public var DEVBAL: String;
		public var DEVWITHTAX: String;
		public var DEVWITHOUTTAX: String;
		public var DEVAMT: String;
		public var DEVTAX: String;
		public var DEVMONEY: String;
		public var DEVDIS: String;
		public var DEVDISF: String;
		public var DEVDISRT: String;
		public var DEVKAS: String;
		public var DEVRTAX: String;
		public var DEVNBAL: String;
		public var HYDAY: String;
	}
}