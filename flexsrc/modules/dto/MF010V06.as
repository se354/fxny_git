package modules.dto
{
	import modules.custom.CustomSingleton;
	
	/**********************************************************
	 * 顧客元帳（配置薬一覧） 項目定義.
	 * @name MF010V06
	 * @author 12.11.16 SE-233
	 **********************************************************/ 
	public class MF010V06
	{
		public var MDCLSNMS :String;
		public var VJAN :String;
		public var GDNM :String;
		public var VPNO :String;
		public var CST :String;

		private var singleton: CustomSingleton = CustomSingleton.getInstance();
		
		// 数量
		public function get VPNO_COMMA(): String
		{
			return singleton.g_customnumberformatter.numberFormat(VPNO, "1", "0", "0"); 
		}
		
		// 単価
		public function get CST_COMMA(): String
		{
			return singleton.g_customnumberformatter.numberFormat(CST, "1", "0", "0"); 
		}
	}
}