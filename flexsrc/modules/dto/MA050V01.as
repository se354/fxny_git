package modules.dto
{
	import modules.custom.CustomSingleton;

	/**********************************************************
	 * 入金入力（配置薬履歴） 項目定義.
	 * @name MA050V01
	 * @author 12.09.10 SE-362
	 **********************************************************/ 
	public class MA050V01
	{
		public var TKCD: String;
		public var HDAT: String;
		public var VJAN: String;
		public var GDNM: String;
		public var VPNO: String;
		public var WITHTAX: String;
		public var WITHOUTTAX: String;
		public var AMT: String;
		public var TAX: String;
		public var MONEY: String;
		public var DIS: String;
		public var KAS: String;
		public var RTAX: String;

		private var singleton: CustomSingleton = CustomSingleton.getInstance();
		
		// 日付
		public function get HDAT_D(): String
		{
	//		return singleton.g_customdateformatter.dateDisplayFormat(HDAT);
			return HDAT;
		}
		
		// 売上額
		public function get WITHTAX_COMMA(): String
		{
			return singleton.g_customnumberformatter.numberFormat(WITHTAX, "1", "0", "0"); 
		}
		
		// 数量
		public function get VPNO_COMMA(): String
		{
			return singleton.g_customnumberformatter.numberFormat(VPNO, "1", "0", "0"); 
		}
		
		// 集金額
		public function get MONEY_COMMA(): String
		{
			return singleton.g_customnumberformatter.numberFormat(MONEY, "1", "0", "0"); 
		}
		
		// 値引
		public function get DIS_COMMA(): String
		{
			return singleton.g_customnumberformatter.numberFormat(DIS, "1", "0", "0"); 
		}
		
		// 売掛額
		public function get AMT_COMMA(): String
		{
			return singleton.g_customnumberformatter.numberFormat(AMT, "1", "0", "0"); 
		}
		
	}
}