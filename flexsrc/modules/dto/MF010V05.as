package modules.dto
{
	import modules.custom.CustomSingleton;
	
	/**********************************************************
	 * 顧客元帳（提供履歴） 項目定義.
	 * @name MF010V05
	 * @author 12.11.14 SE-233
	 **********************************************************/ 
	public class MF010V05
	{
		public var TKCD :String;
		public var HDAT :String;
		public var HTIME :String;
		public var VJAN :String;
		public var GDNM :String;
		public var VPNO :String;

		private var singleton: CustomSingleton = CustomSingleton.getInstance();
		
		// 日付
		public function get HDAT_D(): String
		{
			return HDAT;
		}
		
		// 数量
		public function get VPNO_COMMA(): String
		{
			return singleton.g_customnumberformatter.numberFormat(VPNO, "1", "0", "0"); 
		}
		
	}
}