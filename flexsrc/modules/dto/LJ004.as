package modules.dto
{
	[Bindable]
	[RemoteClass(alias="modules.entity.LJ004")]

	/**********************************************************
	 * 別売売上JB(明細).
	 * @name LJ004
	 * @author 12.09.28 SE-354
	 **********************************************************/ 
	public class LJ004
	{
		public var VNO :String;			// VNO(VARCHAR2)
		public var VSEQ :String;			// VSEQ(VARCHAR2)
		public var VJAN :String;			// VJAN(VARCHAR2)
		public var SAL :String;			// SAL(VARCHAR2)
		public var BCLS :String;			// BCLS(VARCHAR2)
		public var SCLS :String;			// SCLS(VARCHAR2)
		public var SVPNO :String;			// SVPNO(VARCHAR2)
		public var WITHTAX :String;		// WITHTAX(VARCHAR2)
		public var WITHOUTTAX :String;		// WITHOUTTAX(VARCHAR2)
		public var AMT :String;			// AMT(VARCHAR2)
		public var TAX :String;			// TAX(VARCHAR2)
		public var MONEY :String;			// MONEY(VARCHAR2)
		public var DIS :String;			// DIS(VARCHAR2)
		public var RTAX :String;			// RTAX(VARCHAR2)
	}
}