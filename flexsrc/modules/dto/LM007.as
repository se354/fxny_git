package modules.dto
{
	/**********************************************************
	 * 担当者マスタ 項目定義.
	 * @name LM007
	 * @author 12.08.08 SE-354
	 **********************************************************/ 
	public class LM007
	{
		public var CHGCD :String;			// CHGCD(VARCHAR2)
		public var CHGNM :String;			// CHGNM(VARCHAR2)
	}
}