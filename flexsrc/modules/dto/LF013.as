package modules.dto
{
	[Bindable]
	[RemoteClass(alias="modules.entity.LF013")]
	
	/**********************************************************
	 * 行動履歴F 項目定義.
	 * @name LF013
	 * @author 12.12.19 SE-233
	 **********************************************************/ 
	public class LF013
	{
		public var CHGCD   : String;
		public var HDAT    : String;
		public var HTIME   : String;
		public var TKCD    : String;
		public var COM     : String;
		public var VNO     : String;
		public var TNO     : String;
	}
}