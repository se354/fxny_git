package modules.dto
{
	import modules.custom.CustomSingleton;

	/**********************************************************
	 * 制御マスタ.
	 * @name LS001
	 * @author 12.08.30 SE-354
	 **********************************************************/ 
	public class LS001
	{
		public var ID :String;				// ID(VARCHAR2)
		public var HTID :String;			// HTID(VARCHAR2)
		public var CHGCD :String;			// CHGCD(VARCHAR2)
		//public var SDDAY :String;			// SDDAY(VARCHAR2)
		public function get SDDAY(): String
		{
			var w_date: Date = new Date();
			//singleton.g_customdateformatter.dateDisplayFormat_Full(w_date)
			return singleton.g_customdateformatter.dateDisplayFormat_Full(w_date);
		}
		public function set SDDAY(value: String): void
		{
			
		}
		public var HVNO :String;			// HVNO(VARCHAR2)
		public var BVNO :String;			// BVNO(VARCHAR2)
		public var NVNO :String;			// NVNO(VARCHAR2)
		public var TNO :String;			// TNO(VARCHAR2)
		public var DOWDAT :String;			// DOWDAT(VARCHAR2)
		public var DOWTIM :String;			// DOWTIM(VARCHAR2)
		public var UPDDAT :String;			// UPDDAT(VARCHAR2)
		public var UPDTIM :String;			// UPDTIM(VARCHAR2)
		public var COSNM1 :String;			// COSNM1(VARCHAR2)
		public var COSNM2 :String;			// COSNM2(VARCHAR2)
		public var COSNM3 :String;			// COSNM3(VARCHAR2)
		public var ADD1 :String;			// ADD1(VARCHAR2)
		public var ADD2 :String;			// ADD2(VARCHAR2)
		public var ADD3 :String;			// ADD3(VARCHAR2)
		public var TEL1 :String;			// TEL1(VARCHAR2)
		public var TEL2 :String;			// TEL2(VARCHAR2)
		public var TEL3 :String;			// TEL3(VARCHAR2)
		public var BNKCD1 :String;			// BNKCD1(VARCHAR2)
		public var BNKSCD1 :String;		// BNKSCD1(VARCHAR2)
		public var VBNO1 :String;			// VBNO1(VARCHAR2)
		public var BNKMEMO1 :String;		// BNKMEMO1(VARCHAR2)
		public var BNKCD2 :String;			// BNKCD2(VARCHAR2)
		public var BNKSCD2 :String;		// BNKSCD2(VARCHAR2)
		public var VBNO2 :String;			// VBNO2(VARCHAR2)
		public var BNKMEMO2 :String;		// BNKMEMO2(VARCHAR2)
		public var BNKCD3 :String;			// BNKCD3(VARCHAR2)
		public var BNKSCD3 :String;		// BNKSCD3(VARCHAR2)
		public var VBNO3 :String;			// VBNO3(VARCHAR2)
		public var BNKMEMO3 :String;		// BNKMEMO3(VARCHAR2)
		public var HTCHGCD :String;			// HTCHGCD(VARCHAR2)
		public var ADD21 :String;			// ADD21(VARCHAR2)
		public var ADD22 :String;			// ADD22(VARCHAR2)
		public var ADD23 :String;			// ADD23(VARCHAR2)
		
		private var singleton: CustomSingleton = CustomSingleton.getInstance();
	}
}