package modules.dto
{
	/**********************************************************
	 * 配置薬入替 明細 項目定義.
	 * @name W_MA041X
	 * @author 12.08.16 SE-362
	 **********************************************************/
	[Bindable]
	public class W_MA041X
	{
		public var TKCD: String;
		public var VNO: String;
		public var VSEQ: String;
		public var VJAN: String;
		public var SAL: String;
		public var TAXSAL: String;
		public var AMT: String;
		public var TAXAMT: String;
		public var BCLS: String;
		public var SCLS: String;
		public var GDCLS: String;
		public var MDCLS: String;
		public var MDCLSNMS: String;
		public var GDNM: String;
		public var ZHVPNO: String;
		public var KZAN: String;
		public var KUVPNO: String;
		public var KNVPNO: String;
		public var KRVPNO: String;
		public var KLVPNO: String;
		public var KHVPNO: String;
		public var GDCOM: String;
		public var BHISK: String;
	}
}