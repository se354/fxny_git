package modules.dto
{
	[Bindable]
	[RemoteClass(alias="modules.entity.LF001")]

	/**********************************************************
	 * 配置薬販売履歴FA 項目定義.
	 * @name LF001
	 * @author 12.09.26 SE-354
	 **********************************************************/ 
	public class LF001
	{
		public var TKCD :String;			// TKCD(VARCHAR2)
		public var HDAT :String;			// HDAT(VARCHAR2)
		public var HTIME :String;			// HTIME(VARCHAR2)
		public var WITHTAX :String;		// WITHTAX(VARCHAR2)
		public var WITHOUTTAX :String;		// WITHOUTTAX(VARCHAR2)
		public var AMT :String;			// AMT(VARCHAR2)
		public var TAX :String;			// TAX(VARCHAR2)
		public var MONEY :String;			// MONEY(VARCHAR2)
		public var DIS :String;			// DIS(VARCHAR2)
		public var KAS :String;			// KAS(VARCHAR2)
		public var RTAX:String;			// RTAX(VARCHAR2)
	}
}