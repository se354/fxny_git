package modules.dto
{
	/**********************************************************
	 * サービス品提供履歴F 項目定義.
	 * @name LF004
	 * @author 12.11.12 SE-300
	 **********************************************************/ 
	public class LF003
	{
		public var TKCD :String;			// TKCD(VARCHAR2)
		public var HDAT :String;			// HDAT(VARCHAR2)
		public var HTIME :String;			// HTIME(VARCHAR2)
		public var VJAN :String;			// VJAN(VARCHAR2)
		public var GDNM :String;			// GDNM(VARCHAR2)
		public var VPNO :String;			// VPNO(VARCHAR2)
	}
}