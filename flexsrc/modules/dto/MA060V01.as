package modules.dto
{
	import modules.custom.CustomSingleton;
	
	/**********************************************************
	 * 提供数入力 項目定義.
	 * @name MA060V01
	 * @author 12.09.10 SE-362
	 **********************************************************/
	[Bindable]
	public class MA060V01
	{
		public var TKCD: String;
		public var TKNMS: String;
		public var TKZIP: String;
		public var TKADD1: String;
		public var TKADD2: String;
		public var HDAT: String;
		public var ATIME: String;
		public var AMT: String;
		public var MONEY: String;
		public var SFLG: String;
	}
}