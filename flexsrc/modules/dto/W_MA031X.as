package modules.dto
{
	//import modules.base.BasedMethod;
	import modules.custom.CustomSingleton;

	/**********************************************************
	 * 別売入力(明細) 項目定義.
	 * @name W_MA031X
	 * @author 12.09.24 SE-354
	 **********************************************************/ 
	public class W_MA031X
	{
		public var TKCD :String;
		public var VNO :String;	// 伝票NO
		public var VSEQ :String;	// 伝票SEQ
		public var BCLS :String;	// 大分類
		public var GDCLS :String;	// 分類
		public var VJAN :String;	// 商品コード
		public var GDNM :String;	// 商品名
		public var SVPNO :String;	// 数量
		public var SAL :String;	// 単価
		public var WITHTAX :String;	// 使用(込)
		public var WITHOUTTAX :String;	// 使用(別)
		public var AMT :String;	// 請求額
		public var DIS :String;	// 値引額
		public var TAX :String;	// 消費税
		public var MONEY :String;	// 現金集金額(入金)
		public var PDIS :String;	// 値引率
		public var DISCST :String;
/*		
		// 値引率
		public function get PDIS(): String
		{
			var pdis: Number;
			var amt_num: Number = BasedMethod.asCurrency(AMT);
			var dis_num: Number = BasedMethod.asCurrency(DIS);
			
			if (amt_num <= 0 || dis_num == 0)
			{
				return "0";
			}
			else
			{
				return (dis_num/amt_num * 100).toString();
			}
		}
*/		
		private var singleton: CustomSingleton = CustomSingleton.getInstance();

		// 数量
		public function get SVPNO_COMMA(): String
		{
			return singleton.g_customnumberformatter.numberFormat(SVPNO, "1", "0", "0"); 
		}

		// 請求額
		public function get AMT_COMMA(): String
		{
			return singleton.g_customnumberformatter.numberFormat(AMT, "1", "0", "0"); 
		}
		
		// 現金集金額(入金)
		public function get MONEY_COMMA(): String
		{
			return singleton.g_customnumberformatter.numberFormat(MONEY, "1", "0", "0"); 
		}
	}
}