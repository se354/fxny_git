package modules.dto
{
	/**********************************************************
	 * 配置薬マスタ 項目定義.
	 * @name LM012
	 * @author 12.08.30 SE-354
	 **********************************************************/ 
	public class LM012
	{
		public var TKCD :String;			// TKCD(VARCHAR2)
		public var VJAN :String;			// VJAN(VARCHAR2)
		public var VPNO :String;			// VPNO(VARCHAR2)
		public var IRRADD :String;			// IRRADO(VARCHAR2)
		public var BHISK :String;			// BHISK(VARCHAR2)
	}
}