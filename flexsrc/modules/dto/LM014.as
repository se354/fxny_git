package modules.dto
{
	/**********************************************************
	 * 医薬分類マスタ 項目定義.
	 * @name LM014
	 * @author 12.08.10 SE-354
	 **********************************************************/ 
	public class LM014
	{
		public var MDCLS :String;			// MDCLS(VARCHAR2)
		public var MDCLSNM :String;		// MDCLSNM(VARCHAR2)
		public var MDCLSNMS :String;		// MDCLSNMS(VARCHAR2)
	}
}