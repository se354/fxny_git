package modules.dto
{
	/**********************************************************
	 * 地区マスタ 項目定義.
	 * @name LM009
	 * @author 12.08.10 SE-354
	 **********************************************************/ 
	public class LM009
	{
		public var AREA :String;			// AREA(VARCHAR2)
		public var AREANM :String;			// AREANM(VARCHAR2)
	}
}