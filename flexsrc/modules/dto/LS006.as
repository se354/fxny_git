package modules.dto
{
	/**********************************************************
	 * 税マスタ 項目定義.
	 * @name LS006
	 * @author 12.08.30 SE-354
	 **********************************************************/ 
	public class LS006
	{
		public var TAXCLS :String;			// TAXCLS(VARCHAR2)
		public var TAXCLSNM :String;		// TAXCLSNM(VARCHAR2)
		public var TAXRT :String;			// TAXRT(VARCHAR2)
		public var KJNDY :String;			// KJNDY(VARCHAR2)
		public var TAXRTN :String;			// TAXRTN(VARCHAR2)
	}
}