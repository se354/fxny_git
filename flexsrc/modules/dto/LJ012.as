package modules.dto
{
	[Bindable]
	[RemoteClass(alias="modules.entity.LJ012")]

	/**********************************************************
	 * 提供J
	 * @name LJ012
	 * @author 12.09.28 SE-354
	 **********************************************************/ 
	public class LJ012
	{
		public var VNO :String;			// VNO(VARCHAR2)
		public var VSEQ :String;			// VSEQ(VARCHAR2)
		public var TKCD :String;			// TKCD(VARCHAR2)
		public var HDAT :String;			// HDAT(VARCHAR2)
		public var CHGCD :String;			// CHGCD(VARCHAR2)
		public var HTID :String;			// HTID(VARCHAR2)
		public var VJAN :String;			// VJAN(VARCHAR2)
		public var TVPNO :String;			// TVPNO(VARCHAR2)
		public var HYDAY :String;
	}
}