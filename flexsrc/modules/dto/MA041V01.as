package modules.dto
{
	import modules.custom.CustomSingleton;

	/**********************************************************
	 * 配置薬入金履歴
	 * @name MA041V01
	 * @author 12.09.10 SE-362
	 **********************************************************/ 
	public class MA041V01
	{
		public var TKCD: String;
		public var HDAT: String;
		public var WITHTAX: String;
		public var AMT: String;
		public var DIS: String;
		public var MONEY: String;
		public var SAMT: String;

		private var singleton: CustomSingleton = CustomSingleton.getInstance();
		
		// 売上額
		public function get WITHTAX_COMMA(): String
		{
			return singleton.g_customnumberformatter.numberFormat(WITHTAX, "1", "0", "0"); 
		}
		
		// 売掛金
		public function get SAMT_COMMA(): String
		{
			return singleton.g_customnumberformatter.numberFormat(SAMT, "1", "0", "0"); 
		}
		
		// 集金額
		public function get MONEY_COMMA(): String
		{
			return singleton.g_customnumberformatter.numberFormat(MONEY, "1", "0", "0"); 
		}
		
		// 値引
		public function get DIS_COMMA(): String
		{
			return singleton.g_customnumberformatter.numberFormat(DIS, "1", "0", "0"); 
		}
		
		// 請求額
		public function get AMT_COMMA(): String
		{
			return singleton.g_customnumberformatter.numberFormat(AMT, "1", "0", "0"); 
		}
		
	}
}