package modules.dto
{
	import modules.base.BasedMethod;
	import modules.custom.CustomSingleton;

	/**********************************************************
	 * 得意先マスタ 項目定義.
	 * @name LM010
	 * @author 12.08.08 SE-354
	 **********************************************************/ 
	public class LM010
	{
		public var TKCD :String;			// TKCD(VARCHAR2)
		public var TKNM :String;			// TKNM(VARCHAR2)
		public var TKKNM :String;			// TKNM(VARCHAR2)
		public var TKNMS :String;			// TKNMS(VARCHAR2)
		public var TKZIP :String;			// TKZIP(VARCHAR2)
		public var TKADD1 :String;			// TKADD1(VARCHAR2)
		public var TKADD2 :String;			// TKADD2(VARCHAR2)
		public var TKADD3 :String;			// TKADD3(VARCHAR2)
		public var TKTEL :String;			// TKTEL(VARCHAR2)
		public var TKMTEL :String;			// TKMTEL(VARCHAR2)
		public var TKFAX :String;			// TKFAX(VARCHAR2)
		public var TKMAIL1 :String;		// TKMAIL1(VARCHAR2)
		public var TKAREA :String;			// TKAREA(VARCHAR2)
		public var TKCHGCD :String;		// TKCHGCD(VARCHAR2)
		public var MEMO :String;			// MEMO(VARCHAR2)
		public var BOXINF :String;			// BOXINF(VARCHAR2)
		public var HSPAN :String;			// HSPAN(VARCHAR2)
		public var LHDAT :String;			// LHDAT(VARCHAR2)
		public var LHTIME :String;			// LHTIME(VARCHAR2)
		public var KBN1 :String;			// KBN1(VARCHAR2)
		public var KBN2 :String;			// KBN2(VARCHAR2)
		public var KBN3 :String;			// KBN3(VARCHAR2)
		public var KBN4 :String;			// KBN4(VARCHAR2)
		public var KBN5 :String;			// KBN5(VARCHAR2)
		public var KBN6 :String;			// KBN6(VARCHAR2)
		public var KBN7 :String;			// KBN7(VARCHAR2)
		public var KBN8 :String;			// KBN8(VARCHAR2)
		public var HMEDIYAMT :String;		// HMEDIYAMT(VARCHAR2)
		public var HMEDIZAMT :String;		// HMEDIZAMT(VARCHAR2)
		public var HMEDIKURI :String;		// HMEDIKURI(VARCHAR2)
		public var HMEDIAMT :String;		// HMEDIAMT(VARCHAR2)
		public var HMEDITAX :String;		// HMEDITAX(VARCHAR2)
		public var HMEDINY :String;		// HMEDINY(VARCHAR2)
		public var HMEDIDIS :String;		// HMEDIDIS(VARCHAR2)
		public var HMEDIKAS :String;		// HMEDIKAS(VARCHAR2)
		public var HMEDIRTAX :String;		// HMEDIRTAX(VARCHAR2)
		public var HSTOREYAMT :String;		// HSTOREAMT(VARCHAR2)
		public var HSTOREZAMT :String;		// HSTOREAMT(VARCHAR2)
		public var HSTOREKURI :String;		// HSTOREAMT(VARCHAR2)
		public var HSTOREAMT :String;		// HSTOREAMT(VARCHAR2)
		public var HSTORETAX :String;		// HSTORETAX(VARCHAR2)
		public var HSTORENY :String;		// HSTORENY(VARCHAR2)
		public var HSTOREDIS :String;		// HSTOREDIS(VARCHAR2)
		public var HSTOREKAS :String;		// HSTOREKAS(VARCHAR2)
		public var HSTORERTAX :String;		// HSTORERTAX(VARCHAR2)
		public var HDEVYAMT :String;		// HDEVYAMT(VARCHAR2)
		public var HDEVZAMT :String;		// HDEVZAMT(VARCHAR2)
		public var HDEVKURI :String;		// HDEVKURI(VARCHAR2)
		public var HDEVAMT :String;		// HDEVAMT(VARCHAR2)
		public var HDEVTAX :String;		// HDEVTAX(VARCHAR2)
		public var HDEVNY :String;			// HDEVNY(VARCHAR2)
		public var HDEVDIS :String;		// HDEVDIS(VARCHAR2)
		public var HDEVKAS :String;		// HDEVKAS(VARCHAR2)
		public var HDEVRTAX :String;		// HDEVRTAX(VARCHAR2)
		public var GMEDIYAMT :String;		// GMEDIYAMT(VARCHAR2)
		public var GMEDIZAMT :String;		// GMEDIZAMT(VARCHAR2)
		public var GMEDIKURI :String;		// GMEDIKURI(VARCHAR2)
		public var GMEDIAMT :String;		// GMEDIAMT(VARCHAR2)
		public var GMEDITAX :String;		// GMEDITAX(VARCHAR2)
		public var GMEDINY :String;		// GMEDINY(VARCHAR2)
		public var GMEDIDIS :String;		// GMEDIDIS(VARCHAR2)
		public var GMEDIKAS :String;		// GMEDIKAS(VARCHAR2)
		public var GMEDIRTAX :String;		// GMEDIRTAX(VARCHAR2)
		public var GSTOREYAMT :String;		// GSTOREYAMT(VARCHAR2)
		public var GSTOREZAMT :String;		// GSTOREZAMT(VARCHAR2)
		public var GSTOREKURI :String;		// GSTOREKURI(VARCHAR2)
		public var GSTOREAMT :String;		// GSTOREAMT(VARCHAR2)
		public var GSTORETAX :String;		// GSTORETAX(VARCHAR2)
		public var GSTORENY :String;		// GSTORENY(VARCHAR2)
		public var GSTOREDIS :String;		// GSTOREDIS(VARCHAR2)
		public var GSTOREKAS :String;		// GSTOREKAS(VARCHAR2)
		public var GSTORERTAX :String;		// GSTORERTAX(VARCHAR2)
		public var GDEVYAMT :String;		// GDEVYAMT(VARCHAR2)
		public var GDEVZAMT :String;		// GDEVZAMT(VARCHAR2)
		public var GDEVKURI :String;		// GDEVZKURI(VARCHAR2)
		public var GDEVAMT :String;		// GDEVAMT(VARCHAR2)
		public var GDEVTAX :String;		// GDEVTAX(VARCHAR2)
		public var GDEVNY :String;			// GDEVNY(VARCHAR2)
		public var GDEVDIS :String;		// GDEVDIS(VARCHAR2)
		public var GDEVKAS :String;		// GDEVKAS(VARCHAR2)
		public var GDEVRTAX :String;		// GDEVRTAX(VARCHAR2)
		public var HPYM :String;			// HPYM(VARCHAR2)
		public var HPW :String;			// HPW(VARCHAR2)
		public var VNO :String;			// VNO(VARCHAR2)
		public var LATEF :String;			// LATEF(VARCHAR2)
		public var ENDF :String;			// ENDF(VARCHAR2)
		public var EDAT :String;			// EDAT(VARCHAR2)
		public var PLANF :String;			// PLANF(VARCHAR2)
		public var LAT :String;			// LAT(VARCHAR2)
		public var LNG :String;			// LNG(VARCHAR2)
		public var GEOHASH :String;		// GEOHASH(VARCHAR2)
		public var IMPCLS :String;		// IMPCLS1(VARCHAR2)
		//public var IMPCLS2 :String;		// IMPCLS2(VARCHAR2)
		public var TKCLS :String;			// TKCLS(VARCHAR2)
		// add 10.09.26 
		public var BIRTH :String;			// BIRTH(VARCHAR2)
		public var MAP :String;			// MAP(VARCHAR2)
		public var KKB :String;			// KKB(VARCHAR2)
		public var KKBCOM :String;			// KKBCOM(VARCHAR2)
		public var HMEDIZREZ :String;		// HMEDIZREZ(VARCHAR2)
		public var HSTOREZREZ :String;		// HSTOREZREZ(VARCHAR2)
		public var HDEVZREZ :String;		// HDEVZREZ(VARCHAR2)
		public var GMEDIZREZ :String;		// GMEDIZREZ(VARCHAR2)
		public var GSTOREZREZ :String;		// GSTOREZREZ(VARCHAR2)
		public var GDEVZREZ :String;		// GDEVZREZ(VARCHAR2)
		public var HTCLS :String;			// HTCLS(VARCHAR2)
		public var DTAREA :String;			// DTAREA(VARCHAR2)
		public var MEDIDISRT :String;		// MEDIDISRT(VARCHAR2)
		public var STOREDISRT :String;		// STOREDISRT(VARCHAR2)
		public var DEVDISRT :String;		// DEVDISRT(VARCHAR2)
		public var TKRANK :String;			// TKRANK(VARCHAR2)
		public var ESTAMT :String;			// ESTAMT(VARCHAR2)

		// 訪問計画実績F参照
		public var ABSENT :String;
		public var AHTIME :String;
		
		/**
		 * TKADD1+TKADD2+TKADD3
		 */ 
		public function get TKADD(): String
		{
//			return TKADD1 + TKADD2 + TKADD3;
			return BasedMethod.nvl(TKADD1) + BasedMethod.nvl(TKADD2) + BasedMethod.nvl(TKADD3);
		}

		private var singleton: CustomSingleton = CustomSingleton.getInstance();
		
		// 配置薬_医薬品_前回繰越
		public function get HMEDIKURI_COMMA(): String
		{
			return singleton.g_customnumberformatter.numberFormat(HMEDIKURI, "1", "0", "0"); 
		}
		
		// 配置薬_薬店_前回繰越
		public function get HSTOREKURI_COMMA(): String
		{
			return singleton.g_customnumberformatter.numberFormat(HSTOREKURI, "1", "0", "0"); 
		}
		
		// 配置薬_医療具_前回繰越
		public function get HDEVKURI_COMMA(): String
		{
			return singleton.g_customnumberformatter.numberFormat(HDEVKURI, "1", "0", "0"); 
		}
		
		// 現売_医薬品_前回繰越
		public function get GMEDIKURI_COMMA(): String
		{
			return singleton.g_customnumberformatter.numberFormat(GMEDIKURI, "1", "0", "0"); 
		}
		
		// 現売_薬店_前回繰越
		public function get GSTOREKURI_COMMA(): String
		{
			return singleton.g_customnumberformatter.numberFormat(GSTOREKURI, "1", "0", "0"); 
		}
		
		// 現売_医療具_前回繰越
		public function get GDEVKURI_COMMA(): String
		{
			return singleton.g_customnumberformatter.numberFormat(GDEVKURI, "1", "0", "0"); 
		}
		
		// イメージ
		public function get IMAGE(): String
		{
			return "icons/Button-Info-icon.png"; 
		}		
		
	}
}