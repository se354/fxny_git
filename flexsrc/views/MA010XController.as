package views
{
	import flash.data.SQLResult;
	import flash.data.SQLStatement;
	import flash.events.IEventDispatcher;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.events.SQLErrorEvent;
	import flash.events.SQLEvent;
	import flash.system.System;
	import flash.ui.Keyboard;
	
	import jp.co.emori.android.Camera.Camera;
	
	import modules.base.BasedMethod;
	import modules.custom.CustomIterator;
	import modules.custom.sub.Iterator;
	import modules.dto.MF010V01;
	import modules.dto.MF010V02;
	import modules.dto.MF010V03;
	import modules.dto.MF010V04;
	import modules.dto.MF010V05;
	import modules.dto.MF010V06;
	import modules.formatters.CustomNumberFormatter;
	import modules.sbase.SiteConst;
	import modules.sbase.SiteModule;
	
	import mx.collections.ArrayCollection;
	
	public class MA010XController extends SiteModule
	{
		private var view:MA010X;
		private var stmt: SQLStatement;
		public var tabdata: ArrayCollection;
		
		private var g_tkcd: String;			// 得意先コード
		
		private var arrMF010X1: ArrayCollection;
		private var arrMF010X2: ArrayCollection;
		private var arrMF010X3: ArrayCollection;
		private var arrMF010X3H: ArrayCollection;
		private var arrMF010X4: ArrayCollection;
		private var arrMF010X5: ArrayCollection;
		private var arrMF010X6: ArrayCollection;
		
		private var cdsMF010X1_arg: Object;
		private var cdsMF010X2_arg: Object;
		// 12.10.19 SE-233 Redmine#3529 対応 start
		private var cdsMF010X3_arg: Object;
		// 12.10.19 SE-233 Redmine#3529 対応 end
		private var cdsMF010X4_arg: Object;
		private var cdsMF010X5_arg: Object;
		private var cdsMF010X6_arg: Object;
		
		public function MA010XController()
		{
			super();
		}
		
		override public function init():void
		{
			//super.init();
			
			// フォーム宣言
			view = _view as MA010X;
			
			// 初期表示設定
			if (view.data == null)
			{
				//				view.edtCUNM.text = "江守　太郎";
				//				view.edtCUCD.text = "999999";
			}
			else
			{
				view.edtCUNM.text = view.data.TKNM;
				view.edtCUCD.text = view.data.TKCD;
				view.edtTKADD1.text = view.data.TKADD1;
				view.edtTKADD2.text = view.data.TKADD2;
				view.edtTKADD3.text = view.data.TKADD3;
				view.edtTKAREA.text = view.data.TKAREA;
				view.edtTKTEL.text = view.data.TKTEL;
				view.edtTKFAX.text = view.data.TKFAX;
				view.edtTKMAIL.text = view.data.TKMAIL1;
				view.edtLHDAT.text = view.data.LHDAT;
				view.edtHSPAN.text = view.data.HSPAN;
				view.edtBOXINF.text = view.data.BOXINF;
				
				if(view.data.ENDF == 1)
					view.lblENDF.text = "引上";
				
				g_tkcd = view.data.TKCD;
			}
			
			tabdata = new ArrayCollection([
				"訪問履歴","配置薬履歴","別売履歴","提供履歴","配置薬一覧"
			]);
			
			view.tab1.dataProvider = tabdata;
			view.tab1.selectedIndex = 0;
			tab1Click();
			
			// タブ制御
			view.group2.visible = false;
			view.group3.visible = false;
			view.group5.visible = false;
			view.group6.visible = false;
			
			//view.grdW01.dataProvider = arr;
			
			// 12.10.12 SE-362 Redmine#3435 対応 start
			//view.stage.addEventListener("keyDown", keyDownBack, false, 1);
			view.stage.addEventListener(KeyboardEvent.KEY_DOWN, onKeyDownHandler, false, 0, true);
			// 12.10.12 SE-362 Redmine#3435 対応 end
			
			System.gc();
			
			super.init();
		}
		
		override public function formShow_last():void
		{
			super.formShow_last();
			
			doDSPProc();
		}
		
		/**********************************************************
		 * ヘッダデータ取得
		 * @author 12.09.13 SE-233
		 **********************************************************/
		override public function openDataSet_H(): void
		{
			// 制御ﾏｽﾀ情報取得
			cdsSEIGYO(openDataSet_H_cld1);
		}
		
		private function openDataSet_H_cld1(): void
		{
			cdsMF010X1(openDataSet_H_last);
		}
		
		private function openDataSet_H_last(): void
		{
			// 担当者名取得
			cdsCHGCD2(arrSEIGYO[0].CHGCD, super.openDataSet_H);
		}
		
		/**********************************************************
		 * ボディデータ取得
		 * @author 12.09.13 SE-233
		 **********************************************************/
		override public function openDataSet_B(): void
		{
			cdsMF010X2(openDataSet_B_cld1);
		}
		
		// 12.10.19 SE-233 Redmine#3529 対応 start
		private function openDataSet_B_cld1(): void
		{
			cdsMF010X3H(openDataSet_B_cld2);
		}
		
		private function openDataSet_B_cld2(): void
		{
			cdsMF010X3(openDataSet_B_cld3);
		}
		
		private function openDataSet_B_cld3(): void
			// 12.10.19 SE-233 Redmine#3529 対応 end
		{
			cdsMF010X4(openDataSet_B_cld4);
		}
		
		private function openDataSet_B_cld4(): void
		{
			cdsMF010X5(openDataSet_B_cld5);
		}
		
		private function openDataSet_B_cld5(): void
		{
			cdsMF010X6(openDataSet_B_last);
		}
		
		private function openDataSet_B_last(): void
		{
			// 14.03.03 SE-233 start
			//			getTax(arrSEIGYO[0].SDDAY, super.openDataSet_B);
			var w_date:Date = new Date();
			var w_sysdate:String;
			w_sysdate = singleton.g_customdateformatter.dateDisplayFormat(w_date);
			getTax(w_sysdate, super.openDataSet_B);
			// 14.03.03 SE-233 end
			
			
		}
		
		/**********************************************************
		 * 画面項目セット
		 * @author 12.09.13 SE-233
		 **********************************************************/
		override public function setDispObject(): void
		{
			super.setDispObject();
			
			//売掛時点
			view.lblDOWDAT.text = arrSEIGYO[0].DOWDAT;
			
			// ヘッダ項目表示
			if(arrMF010X1 != null && arrMF010X1.length != 0){
				view.lblHMEDIKURI.text = arrMF010X1[0].HMEDIKURI_COMMA;
				view.lblHSTOREKURI.text = arrMF010X1[0].HSTOREKURI_COMMA;
				view.lblHDEVKURI.text = arrMF010X1[0].HDEVKURI_COMMA;
				view.lblGMEDIKURI.text = arrMF010X1[0].GMEDIKURI_COMMA;
				view.lblGSTOREKURI.text = arrMF010X1[0].GSTOREKURI_COMMA;
				view.lblGDEVKURI.text = arrMF010X1[0].GDEVKURI_COMMA;
			}
			
			// 明細情報
			view.grdW02.dataProvider = arrMF010X2;
			// 12.10.19 SE-233 Redmine#3529 対応 start
			view.grdW03.dataProvider = arrMF010X3;
			// 12.10.19 SE-233 Redmine#3529 対応 end
			view.grdW04.dataProvider = arrMF010X4;
			view.grdW05.dataProvider = arrMF010X5;
			view.grdW06.dataProvider = arrMF010X6;
			
			if(arrMF010X3H != null && arrMF010X3H.length != 0){
				// 配置薬履歴ヘッダ設定
				view.lblHBAYI01.headerText = BasedMethod.nvl(arrMF010X3H[0].HBAYI01);
				view.lblHBAYI02.headerText = BasedMethod.nvl(arrMF010X3H[0].HBAYI02);
				view.lblHBAYI03.headerText = BasedMethod.nvl(arrMF010X3H[0].HBAYI03);
				view.lblHBAYI04.headerText = BasedMethod.nvl(arrMF010X3H[0].HBAYI04);
				view.lblHBAYI05.headerText = BasedMethod.nvl(arrMF010X3H[0].HBAYI05);
				view.lblHBAYI06.headerText = BasedMethod.nvl(arrMF010X3H[0].HBAYI06);
				view.lblHBAYI07.headerText = BasedMethod.nvl(arrMF010X3H[0].HBAYI07);
				view.lblHBAYI08.headerText = BasedMethod.nvl(arrMF010X3H[0].HBAYI08);
				view.lblHBAYI09.headerText = BasedMethod.nvl(arrMF010X3H[0].HBAYI09);
				view.lblHBAYI10.headerText = BasedMethod.nvl(arrMF010X3H[0].HBAYI10);
				view.lblHBAYI11.headerText = BasedMethod.nvl(arrMF010X3H[0].HBAYI11);
				view.lblHBAYI12.headerText = BasedMethod.nvl(arrMF010X3H[0].HBAYI12);
				view.lblHBAYI13.headerText = BasedMethod.nvl(arrMF010X3H[0].HBAYI13);
				view.lblHBAYI14.headerText = BasedMethod.nvl(arrMF010X3H[0].HBAYI14);
				view.lblHBAYI15.headerText = BasedMethod.nvl(arrMF010X3H[0].HBAYI15);
				view.lblHBAYI16.headerText = BasedMethod.nvl(arrMF010X3H[0].HBAYI16);
				view.lblHBAYI17.headerText = BasedMethod.nvl(arrMF010X3H[0].HBAYI17);
				view.lblHBAYI18.headerText = BasedMethod.nvl(arrMF010X3H[0].HBAYI18);
				view.lblHBAYI19.headerText = BasedMethod.nvl(arrMF010X3H[0].HBAYI19);
				view.lblHBAYI20.headerText = BasedMethod.nvl(arrMF010X3H[0].HBAYI20);
				view.lblHBAYI21.headerText = BasedMethod.nvl(arrMF010X3H[0].HBAYI21);
				view.lblHBAYI22.headerText = BasedMethod.nvl(arrMF010X3H[0].HBAYI22);
				view.lblHBAYI23.headerText = BasedMethod.nvl(arrMF010X3H[0].HBAYI23);
				view.lblHBAYI24.headerText = BasedMethod.nvl(arrMF010X3H[0].HBAYI24);
				view.lblHBAYI25.headerText = BasedMethod.nvl(arrMF010X3H[0].HBAYI25);
				view.lblHBAYI26.headerText = BasedMethod.nvl(arrMF010X3H[0].HBAYI26);
				view.lblHBAYI27.headerText = BasedMethod.nvl(arrMF010X3H[0].HBAYI27);
				view.lblHBAYI28.headerText = BasedMethod.nvl(arrMF010X3H[0].HBAYI28);
				view.lblHBAYI29.headerText = BasedMethod.nvl(arrMF010X3H[0].HBAYI29);
				view.lblHBAYI30.headerText = BasedMethod.nvl(arrMF010X3H[0].HBAYI30);
			}
		}		
		
		// 12.10.12 SE-362 Redmine#3435 対応 start
		protected function onKeyDownHandler(e:KeyboardEvent):void
		{
			//e.preventDefault();
			
			if(e.keyCode == Keyboard.BACK){
				e.preventDefault();
				btnRTNClick();
			}
		}
		// 12.10.12 SE-362 Redmine#3435 対応 end
		
		/**********************************************************
		 * btnCAMERA クリック時処理.
		 * @author 12.07.03 SE-354
		 **********************************************************/
		public function btnCAMERAClick(): void
		{
			var camera: Camera = new Camera();
			camera.startCamera(SiteConst.G_IMAGE_PATH, view.edtCUCD.text);
		}
		
		/**********************************************************
		 * btnRTN クリック時処理.
		 * @author 12.07.03 SE-354
		 **********************************************************/
		public function btnRTNClick(): void
		{
			// 12.10.12 SE-362 Redmine#3435 対応 start
			// 確認画面の表示
			singleton.sitemethod.dspJoinErrMsg("Q0027", btnRTNClick_last, "", "1", null, btnRTNClick_last2);
			// 12.10.12 SE-362 Redmine#3435 対応 end
		}
		
		// 12.10.12 SE-362 Redmine#3435 対応 start
		private function btnRTNClick_last(): void
		{
			view.stage.removeEventListener(KeyboardEvent.KEY_DOWN, onKeyDownHandler);
			
			// 16.01.14 SE-233 start
			insLF013(g_tkcd,"訪問終了");
			// 16.01.14 SE-233 end

			arrMF010X1 = null;
			arrMF010X2 = null;
			arrMF010X3H = null;
			arrMF010X3 = null;
			arrMF010X4 = null;
			arrMF010X5 = null;
			arrMF010X6 = null;
			
			view.grdW02.dataProvider = null;
			view.grdW03.dataProvider = null;
			view.grdW04.dataProvider = null;
			view.grdW05.dataProvider = null;
			view.grdW06.dataProvider = null;
			cdsMF010X1_arg = null;
			cdsMF010X2_arg = null;
			cdsMF010X3_arg = null;
			cdsMF010X4_arg = null;
			cdsMF010X5_arg = null;
			cdsMF010X6_arg = null;
			tabdata = null;
			view.btnMA040X.removeEventListener(MouseEvent.CLICK, btnMA040XClick);
			
			// 前画面に遷移
			view.navigator.popView();
			
			view = null;
		}
		
		private function btnRTNClick_last2(): void
		{
			// 無処理
		}
		// 12.10.12 SE-362 Redmine#3435 対応 end
		
		
		/**********************************************************
		 * tabBar クリック時処理.
		 * @author 12.08.20 SE-354
		 **********************************************************/
		public function tab1Click(): void
		{
			if (view.tab1.selectedIndex == 0)
			{
				view.group1.visible = true;
				view.group2.visible = false;
				view.group3.visible = false;
				view.group5.visible = false;
				view.group6.visible = false;
			}
			else if(view.tab1.selectedIndex == 1)
			{
				view.group1.visible = false;
				view.group2.visible = true;
				view.group3.visible = false;
				view.group5.visible = false;
				view.group6.visible = false;
			}
			else if(view.tab1.selectedIndex == 2)
			{
				view.group1.visible = false;
				view.group2.visible = false;
				view.group3.visible = true;
				view.group5.visible = false;
				view.group6.visible = false;
			}
			else if(view.tab1.selectedIndex == 3)
			{
				view.group1.visible = false;
				view.group2.visible = false;
				view.group3.visible = false;
				view.group5.visible = true;
				view.group6.visible = false;
			}
			else
			{
				view.group1.visible = false;
				view.group2.visible = false;
				view.group3.visible = false;
				view.group5.visible = false;
				view.group6.visible = true;
			}
		}
		
		/**********************************************************
		 * btnMA030X クリック時処理.
		 * @author 12.08.20 SE-354
		 **********************************************************/
		public function btnMA030XClick(): void
		{
			/*
			var obj: Object = new Object();
			
			obj.cucd = view.edtCUCD.text;
			obj.cunm = view.edtCUNM.text;
			// ↓得意先TEL渡してください
			obj.tktel = "090-07-2231";
			*/
			
			view.stage.removeEventListener(KeyboardEvent.KEY_DOWN, onKeyDownHandler);
			
			// 別売入力に遷移
			view.navigator.pushView(MA030X, view.data);
		}
		
		/**********************************************************
		 * btnMA040X クリック時処理.
		 * @author 12.08.20 SE-354
		 **********************************************************/
		public function btnMA040XClick(): void
		{
			var obj: Object = new Object();
			
			obj.cucd = view.edtCUCD.text;
			obj.cunm = view.edtCUNM.text;
			
			view.stage.removeEventListener(KeyboardEvent.KEY_DOWN, onKeyDownHandler);
			
			// 別売入力に遷移
			view.navigator.pushView(MA040X, view.data);
		}
		
		/**********************************************************
		 * btnMA050X クリック時処理.
		 * @author 12.08.20 SE-354
		 **********************************************************/
		public function btnMA050XClick(): void
		{
			var obj: Object = new Object();
			
			obj.cucd = view.edtCUCD.text;
			obj.cunm = view.edtCUNM.text;
			
			view.stage.removeEventListener(KeyboardEvent.KEY_DOWN, onKeyDownHandler);
			
			// 入金入力に遷移
			view.navigator.pushView(MA050X, view.data);
		}
		
		/**********************************************************
		 * btnMA041X クリック時処理.
		 * @author 12.10.17 SE-300
		 **********************************************************/
		public function btnMA041XClick(): void
		{
			var obj: Object = new Object();
			var o: Object = new Object();
			
			obj.cucd = view.edtCUCD.text;
			obj.cunm = view.edtCUNM.text;
			
			o.modifyFlag = 2;
			
			view.stage.removeEventListener(KeyboardEvent.KEY_DOWN, onKeyDownHandler);
			
			// 入金入力に遷移
			view.navigator.pushView(MA041X, view.data, o);
		}
		
		/**********************************************************
		 * 訪問履歴（ヘッダ部）情報取得
		 * @author 12.09.13 SE-233
		 **********************************************************/
		public function cdsMF010X1(func: Function=null): void
		{
			// 引数情報
			cdsMF010X1_arg = new Object;
			cdsMF010X1_arg.func = func;
			
			stmt = new SQLStatement();
			stmt.sqlConnection =  singleton.sqlconnection;
			stmt.parameters[0] = g_tkcd;
			stmt.itemClass = MF010V01;
			stmt.text= singleton.g_query_xml.entry.(@key == "qryMF010X1");
			
			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, cdsMF010X1Result, false, 0, true);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler, false, 0, true);
			
			//実行
			stmt.execute();
		}
		
		public function cdsMF010X1Result(event: SQLEvent): void
		{
			var w_arg: Object = cdsMF010X1_arg;
			var result:SQLResult = stmt.getResult();
			
			stmt.removeEventListener(SQLEvent.RESULT, cdsMF010X1Result);				
			stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);				
			stmt=null;
			
			if (arrMF010X1 != null){
				arrMF010X1.removeAll();
			}
			arrMF010X1 = new ArrayCollection(result.data);
			
			// 次関数実行
			if(w_arg.func)
				w_arg.func();
		}
		
		/**********************************************************
		 * 訪問履歴（明細部）情報取得
		 * @author 12.09.13 SE-233
		 **********************************************************/
		public function cdsMF010X2(func: Function=null): void
		{
			// 引数情報
			cdsMF010X2_arg = new Object;
			cdsMF010X2_arg.func = func;
			
			stmt = new SQLStatement();
			stmt.sqlConnection =  singleton.sqlconnection;
			stmt.parameters[0] = g_tkcd;
			stmt.itemClass = MF010V02;
			stmt.text= singleton.g_query_xml.entry.(@key == "qryMF010X2");
			
			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, cdsMF010X2Result, false, 0, true);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler, false, 0, true);
			
			//実行
			stmt.execute();
		}
		
		public function cdsMF010X2Result(event: SQLEvent): void
		{
			var w_arg: Object = cdsMF010X2_arg;
			var result:SQLResult = stmt.getResult();
			
			stmt.removeEventListener(SQLEvent.RESULT, cdsMF010X2Result);				
			stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);				
			stmt=null;
			
			if (arrMF010X2 != null){
				arrMF010X2.removeAll();
			}
			arrMF010X2 = new ArrayCollection(result.data);
			
			// 次関数実行
			if(w_arg.func)
				w_arg.func();
		}
		
		/**********************************************************
		 * 配置薬履歴（明細部）情報取得
		 * @author 12.11.22 SE-233
		 **********************************************************/
		public function cdsMF010X3H(func: Function=null): void
		{
			// 引数情報
			cdsMF010X3_arg = new Object;
			cdsMF010X3_arg.func = func;
			
			stmt = new SQLStatement();
			stmt.sqlConnection =  singleton.sqlconnection;
			stmt.parameters[0] = g_tkcd;
			stmt.itemClass = MF010V03;
			stmt.text= singleton.g_query_xml.entry.(@key == "qryMF010X3H");
			
			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, cdsMF010X3HResult, false, 0, true);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler, false, 0, true);
			
			//実行
			stmt.execute();
		}
		
		public function cdsMF010X3HResult(event: SQLEvent): void
		{
			var w_arg: Object = cdsMF010X3_arg;
			var result:SQLResult = stmt.getResult();
			
			stmt.removeEventListener(SQLEvent.RESULT, cdsMF010X3HResult);				
			stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);				
			stmt=null;
			
			if (arrMF010X3H != null){
				arrMF010X3H.removeAll();
			}
			arrMF010X3H = new ArrayCollection(result.data);
			
			// 次関数実行
			if(w_arg.func)
				w_arg.func();
		}
		
		// 12.10.19 SE-233 Redmine#3529 対応 start
		/**********************************************************
		 * 配置薬履歴（明細部）情報取得
		 * @author 12.09.13 SE-233
		 **********************************************************/
		public function cdsMF010X3(func: Function=null): void
		{
			// 引数情報
			cdsMF010X3_arg = new Object;
			cdsMF010X3_arg.func = func;
			
			stmt = new SQLStatement();
			stmt.sqlConnection =  singleton.sqlconnection;
			stmt.parameters[0] = g_tkcd;
			stmt.itemClass = MF010V03;
			stmt.text= singleton.g_query_xml.entry.(@key == "qryMF010X3");
			
			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, cdsMF010X3Result, false, 0, true);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler, false, 0, true);
			
			//実行
			stmt.execute();
		}
		
		public function cdsMF010X3Result(event: SQLEvent): void
		{
			var w_arg: Object = cdsMF010X3_arg;
			var result:SQLResult = stmt.getResult();
			
			stmt.removeEventListener(SQLEvent.RESULT, cdsMF010X3Result);				
			stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);				
			stmt=null;
			
			if (arrMF010X3 != null){
				arrMF010X3.removeAll();
			}
			arrMF010X3 = new ArrayCollection(result.data);
			
			// 次関数実行
			if(w_arg.func)
				w_arg.func();
		}
		// 12.10.19 SE-233 Redmine#3529 対応 end
		
		/**********************************************************
		 * 別売履歴情報取得
		 * @author 12.09.13 SE-233
		 **********************************************************/
		public function cdsMF010X4(func: Function=null): void
		{
			// 引数情報
			cdsMF010X4_arg = new Object;
			cdsMF010X4_arg.func = func;
			
			stmt = new SQLStatement();
			stmt.sqlConnection =  singleton.sqlconnection;
			stmt.parameters[0] = g_tkcd;
			stmt.itemClass = MF010V04;
			stmt.text= singleton.g_query_xml.entry.(@key == "qryMF010X4");
			
			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, cdsMF010X4Result, false, 0, true);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler, false, 0, true);
			
			//実行
			stmt.execute();
		}
		
		public function cdsMF010X4Result(event: SQLEvent): void
		{
			var w_arg: Object = cdsMF010X4_arg;
			var result:SQLResult = stmt.getResult();
			
			stmt.removeEventListener(SQLEvent.RESULT, cdsMF010X4Result);				
			stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);				
			stmt=null;
			
			if (arrMF010X4 != null){
				arrMF010X4.removeAll();
			}
			arrMF010X4 = new ArrayCollection(result.data);
			
			// 次関数実行
			if(w_arg.func)
				w_arg.func();
		}
		
		/**********************************************************
		 * 提供履歴情報取得
		 * @author 12.11.14 SE-233
		 **********************************************************/
		public function cdsMF010X5(func: Function=null): void
		{
			// 引数情報
			cdsMF010X5_arg = new Object;
			cdsMF010X5_arg.func = func;
			
			stmt = new SQLStatement();
			stmt.sqlConnection =  singleton.sqlconnection;
			stmt.parameters[0] = g_tkcd;
			stmt.itemClass = MF010V05;
			stmt.text= singleton.g_query_xml.entry.(@key == "qryMF010X5");
			
			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, cdsMF010X5Result, false, 0, true);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler, false, 0, true);
			
			//実行
			stmt.execute();
		}
		
		public function cdsMF010X5Result(event: SQLEvent): void
		{
			var w_arg: Object = cdsMF010X5_arg;
			var result:SQLResult = stmt.getResult();
			
			stmt.removeEventListener(SQLEvent.RESULT, cdsMF010X5Result);				
			stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);				
			stmt=null;
			
			if (arrMF010X5 != null){
				arrMF010X5.removeAll();
			}
			arrMF010X5 = new ArrayCollection(result.data);
			
			// 次関数実行
			if(w_arg.func)
				w_arg.func();
		}
		
		/**********************************************************
		 * 配置薬一覧情報取得
		 * @author 12.11.16 SE-233
		 **********************************************************/
		public function cdsMF010X6(func: Function=null): void
		{
			// 引数情報
			cdsMF010X6_arg = new Object;
			cdsMF010X6_arg.func = func;
			
			stmt = new SQLStatement();
			stmt.sqlConnection =  singleton.sqlconnection;
			stmt.parameters[0] = g_tkcd;
			stmt.itemClass = MF010V06;
			stmt.text= singleton.g_query_xml.entry.(@key == "qryMF010X6");
			
			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, cdsMF010X6Result, false, 0, true);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler, false, 0, true);
			
			//実行
			stmt.execute();
		}
		
		public function cdsMF010X6Result(event: SQLEvent): void
		{
			var w_arg: Object = cdsMF010X6_arg;
			var result:SQLResult = stmt.getResult();
			
			stmt.removeEventListener(SQLEvent.RESULT, cdsMF010X6Result);				
			stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);				
			stmt=null;
			
			if (arrMF010X6 != null){
				arrMF010X6.removeAll();
			}
			arrMF010X6 = new ArrayCollection(result.data);
			
			// 次関数実行
			if(w_arg.func)
				w_arg.func();
		}
		
		private function stmtErrorHandler(event: SQLErrorEvent): void
		{
			// エラーの表示
			//trace(event.error.message);
			doDSPProc_last();
		}
		
	}
}