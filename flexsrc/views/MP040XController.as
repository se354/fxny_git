package views
{
	import flash.data.SQLResult;
	import flash.data.SQLStatement;
	import flash.events.Event;
	import flash.events.GeolocationEvent;
	import flash.events.KeyboardEvent;
	import flash.events.LocationChangeEvent;
	import flash.events.SQLErrorEvent;
	import flash.events.SQLEvent;
	import flash.geom.Rectangle;
	import flash.media.StageWebView;
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	import flash.sensors.Geolocation;
	import flash.ui.Keyboard;
	
	import modules.base.BasedMethod;
	import modules.custom.CustomIterator;
	import modules.custom.sub.Iterator;
	import modules.dto.LM009;
	import modules.dto.W_MA040X;
	import modules.formatters.CustomNumberFormatter;
	import modules.sbase.SiteModule;
	
	import mx.collections.ArrayCollection;
	import mx.managers.PopUpManager;
	
	/**********************************************************
	 * MP040X 配置薬入替(請求)ポップアップ
	 * @author 12.09.06 SE-362
	 **********************************************************/
	public class MP040XController extends SiteModule
	{
		private var view:MP040X;

		// 引数情報
		public var argWITHOUTTAX: Number;		// 使用金額（別）
		public var argWITHTAX: Number;			// 使用金額（込）
		public var argDISC: Number;			// 値引額
		public var argAMT: Number;				// 請求額
		public var argDISCF: Number;			// 値引率
		public var argDISCRT: Number;			// 前回値引率
		public var argTAX: Number;				// 税率
		
		// 返値情報
		public var rtnDISC: Number;			// 値引額
		public var rtnDISCF: Number;			// 値引率
		public var rtnAMT: Number;				// 請求額
		public var rtnDISCRT: Number;			// 手入力値引率
		
		public function MP040XController()
		{
			super();
		}
		
		/**********************************************************
		 * 初期処理
		 * @author  12.09.06 SE-362
		 **********************************************************/
		override public function init():void
		{
			super.init();
			
			// フォーム宣言
			view = _view as MP040X;

			// 初期値設定
			view.edtKUVPNO.text = argWITHOUTTAX.toString();
			view.edtAMT.text = argAMT.toString();
			view.edtDISC.text = argDISC.toString();
			view.edtDISCF2.text = argDISCF.toString();
			view.edtWITHTAX.text = argWITHTAX.toString();
			view.edtDISCF.text = argDISCRT.toString();
			// 前回値引きが0なら請求額に使用額
//			if(BasedMethod.nvl(argDISCRT.toString(), "0") == "0"){
//				if(BasedMethod.nvl(argAMT.toString(), "0") == "0"){
//					view.edtAMT.text = argWITHTAX.toString();
//				}
//			}else{
//				calcDis();
//			}
			
			
			view.rdg5.selected = true;
		}
		
		/**********************************************************
		 * 画面立ち上げ処理
		 * @author  12.09.06 SE-362
		 **********************************************************/
		override public function formShow_last():void
		{
			super.formShow_last();
			
		}

		/**********************************************************
		 * 値引額計算
		 * @author  12.09.06 SE-362
		 **********************************************************/
		public function calcDis(kbn: String="0"): void
		{
			//var w_amt: Number;
			var w_amt: String;
			var w_disc: Number;
			var w_ceil: String;
			var f: CustomNumberFormatter = new CustomNumberFormatter();

			// 12.11.01 SE-233 Redmine#3600対応 start
			if(kbn == "0"){
				view.edtDISCF.text = BasedMethod.NumChk(view.edtDISCF.text,1);
			}else{
				view.edtAMT.text = BasedMethod.NumChk(view.edtAMT.text);
			}
			// 12.11.01 SE-233 Redmine#3600対応 end
			
			// 値引額
//			w_disc = Number(BasedMethod.calcNum(view.edtKUVPNO.text, BasedMethod.calcNum(view.edtDISCF.text, "100", 3, 4), 2));
			//w_disc = Number(BasedMethod.calcNum(view.edtKUVPNO.text, (BasedMethod.asCurrency(view.edtDISCF.text)/100 ).toString(), 2));
//			w_amt = Number(BasedMethod.calcNum(view.edtKUVPNO.text, w_disc.toString(), 1));
			/*			w_amt = Number(BasedMethod.calcNum(
			BasedMethod.calcNum(
			view.edtKUVPNO.text
			, BasedMethod.calcNum(					
			BasedMethod.calcNum("100", w_disc.toString(), 1)
			, BasedMethod.calcNum("100", argTAX.toString())
			, 2)
			, 2)
			, "100", 3));
			*/

//			w_amt = Math.floor(Number(BasedMethod.calcNum(
//				BasedMethod.calcNum(
//					view.edtWITHTAX.text
//					,BasedMethod.calcNum("100", view.edtDISCF.text, 1)
//					, 2)
//				, "100", 3,0,1)));

			// 単位によって値変更
			if(view.rdg1.selected){
				w_ceil = "100";
			}else if(view.rdg2.selected){
				w_ceil = "50";
			}else if(view.rdg3.selected){
				w_ceil = "10";
//			}else if(view.rdg4.selected){
//				w_ceil = "5";
			}else{
				w_ceil = "1";
			}

			// 請求額算出
//			if(kbn == "0"){
//				view.edtAMT.text = BasedMethod.calcNum(BasedMethod.calcNum(w_amt.toString(), w_ceil, 3), w_ceil, 2);
//			}

			if(kbn == "0"){

				// 値引率入力時処理

				// 値引額
				view.edtDISC.text = BasedMethod.calcNum(BasedMethod.calcNum(view.edtKUVPNO.text,view.edtDISCF.text,2),"100",3,0,1);
				// 値引率
				view.edtDISCF2.text = view.edtDISCF.text;
				// 請求額
				w_amt = BasedMethod.calcNum(
						BasedMethod.calcNum(BasedMethod.calcNum(view.edtKUVPNO.text,view.edtDISC.text,1),
						BasedMethod.calcNum(BasedMethod.nvl(argTAX.toString(), "0"),"100"),2),"100",3,0,1);
				view.edtAMT.text = BasedMethod.calcNum(BasedMethod.calcNum(w_amt, w_ceil, 3), w_ceil, 2);

				// 使用額を超えた場合は使用額をセット
				if(BasedMethod.asCurrency(view.edtWITHTAX.text) < BasedMethod.asCurrency(view.edtAMT.text)){
					view.edtDISC.text = BasedMethod.calcNum(view.edtAMT.text,view.edtWITHTAX.text,1);
					view.edtAMT.text = view.edtWITHTAX.text;
				}
			}
	
			if(kbn == "1" || w_ceil != "1"){

				// 請求額入力時処理

				// 値引額
				view.edtDISC.text = BasedMethod.calcNum(view.edtKUVPNO.text,
						BasedMethod.calcNum(BasedMethod.calcNum(view.edtAMT.text, "100", 2),
						BasedMethod.calcNum("100", argTAX.toString()), 3,0,2), 1); 
				
				// 値引率
				if(BasedMethod.asCurrency(view.edtKUVPNO.text) != 0){
						view.edtDISCF2.text = f.numberFormat(
						BasedMethod.calcNum(Math.floor(Number(BasedMethod.calcNum(BasedMethod.calcNum(view.edtDISC.text, view.edtKUVPNO.text, 3,4), "10000", 2))).toString(),"100",3,4,1)
						, "0", "2", "0");
				}else{
						view.edtDISCF2.text = "0";
				}
			}
		}
		
		/**********************************************************
		 * 決定
		 * @author  12.09.06 SE-362
		 **********************************************************/
		public function btnDECClick():void
		{
			// 返値セット
			rtnDISC = BasedMethod.asCurrency(view.edtDISC.text);
			rtnDISCF = BasedMethod.asCurrency(view.edtDISCF2.text);
			rtnAMT = BasedMethod.asCurrency(view.edtAMT.text);
			rtnDISCRT = BasedMethod.asCurrency(view.edtDISCF.text);
			
			
			// 画面クローズ
			PopUpManager.removePopUp(view);
		}

		/**********************************************************
		 * キャンセル
		 * @author  12.11.13 SE-233
		 **********************************************************/
		public function btnCANCELClick():void
		{
			rtnDISC = argDISC;
			rtnDISCF = argDISCF;
			rtnAMT = argAMT;
			rtnDISCRT = argDISCRT;
			
			// 画面クローズ
			PopUpManager.removePopUp(view);
		}
	}
}