package views
{
	import flash.data.SQLConnection;
	import flash.data.SQLResult;
	import flash.data.SQLStatement;
	import flash.display.DisplayObjectContainer;
	import flash.events.KeyboardEvent;
	import flash.events.SQLErrorEvent;
	import flash.events.SQLEvent;
	import flash.events.IEventDispatcher;
	import flash.globalization.NumberFormatter;
	import flash.ui.Keyboard;
	
	import jp.co.emori.android.Camera.Camera;
	
	import modules.base.BasedMethod;
	import modules.custom.AlertDialog;
	import modules.custom.CustomIterator;
	import modules.custom.sub.Iterator;
	import modules.dto.MA042V01;
	import modules.dto.MA042V02;
	import modules.dto.W_MA040X;
	import modules.dto.W_MA041X;
	import modules.formatters.CustomNumberFormatter;
	import modules.sbase.SiteConst;
	import modules.sbase.SiteMethod;
	import modules.sbase.SiteModule;
	
	import mx.charts.LinearAxis;
	import mx.collections.ArrayCollection;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	import mx.rpc.http.HTTPService;
	
	import spark.skins.spark.StackedFormHeadingSkin;
	
	/**********************************************************
	 * MA042X 配置薬入替(入替)
	 * @author 12.09.06 SE-362
	 **********************************************************/
	public class MA042XController extends SiteModule
	{
		private var view:MA042X;
		private var g_cucd: String;			// 客先コード
		// 12.10.31 SE-233 Redmine#3578 対応 start
		private var g_date: String;			// 訪問日付
		// 12.10.31 SE-233 Redmine#3578 対応 start
		private var g_time: String;			// 訪問時間
		private var g_vseq: String;			// 伝票NO
		private var g_tno: String;			// 通しNO
		private var g_tktel: String;			// 得意先電話番号
		private var pop: POPUP002;				// 商品ポップアップ
		private var g_updateCount: int;
		private var g_insertCount: int;
		private var g_updateIndex: int;
		private var g_vlin: int;
		private var g_erahdat: String;		// 和暦訪問日付
		private var g_eralhdat: String;		// 和暦前回訪問日付

		private var stmt: SQLStatement;
		
		private var arrMA042X1: ArrayCollection;
		private var arrMA042X2: ArrayCollection;
		private var arrMA042X3: ArrayCollection;	// 商品別配置履歴
		private var arrMA042X4: ArrayCollection;	// 商品別配置履歴
		private var print_arg: Object;
		
		// 帳票用配列
		private var arrPRINT: ArrayCollection;
		
		// 帳票用変数
		private var reportCount: int=0;				// 帳票明細数
		private var currentCount: int=0;				// 現在印刷明細番号

		private var cdsMA042X_arg: Object;
		
		public  var g_selectedIndex: int = 0;
		private var modifyFlag: int;

		public function MA042XController()
		{
			super();
		}
		
		/**********************************************************
		 * 初期処理
		 * @author  12.09.06 SE-362
		 **********************************************************/
		override public function init():void
		{
			// フォーム宣言
			view = _view as MA042X;

			// 変数設定
			g_cucd = String(view.data.TKCD);
			g_tktel = String(view.data.TKTEL);
			g_vseq = String(view.navigator.context.vseq);
			g_tno = String(view.navigator.context.tno);
			modifyFlag = int(view.navigator.context.modifyFlag);
			// 12.10.31 SE-233 Redmine#3578 対応 start
			g_date = String(view.navigator.context.date);
			g_time = String(view.navigator.context.time);
			// 12.10.31 SE-233 Redmine#3578 対応 start

			// 12.11.06 SE-233 Redmine#3628 対応 start
//			getEra(g_date);
			// 12.11.06 SE-233 Redmine#3628 対応 start
			
			// 初期表示設定
			view.edtCUNM.text = String(view.data.TKNM);
			view.edtVSEQ.text = g_vseq;
			
			// ボタンに色を付ける
			view.btnPRINT.setStyle("chromeColor", 0x55aaff);
			
			// 以下、暫定
//			view.grdW01.visible = false;
//			view.lineChart.visible = false;
			// 12.10.12 SE-362 Redmine#3435 対応 start
			//view.stage.addEventListener("keyDown", keyDownBack, false, 1);
			view.stage.addEventListener(KeyboardEvent.KEY_DOWN, onKeyDownHandler, false, 0, true);
			// 12.10.12 SE-362 Redmine#3435 対応 end

			if(modifyFlag >0)
				view.lblMODIFY.text = "修正";
				
			super.init();

		}
		

		/**********************************************************
		 * 画面立ち上げ処理
		 * @author  12.09.06 SE-362
		 **********************************************************/
		override public function formShow_last():void
		{
			super.formShow_last();
			
			// 画面データ表示
			doDSPProc();
		}

		/**********************************************************
		 * ヘッダデータ取得
		 * @author  12.09.06 SE-362
		 **********************************************************/
		override public function openDataSet_H(): void
		{
			// ヘッダ情報
			cdsMA042X(openDataSet_H_cld1);
		}

		private function openDataSet_H_cld1(): void
		{
			// 制御ﾏｽﾀ情報取得
			cdsSEIGYO(openDataSet_H_cld2);
		}
		
		private function openDataSet_H_cld2(): void
		{
			// 担当者名取得
			// 12.10.11 SE-362 Redmine#3423 対応 start
//			cdsCHGCD2(arrSEIGYO[0].CHGCD, super.openDataSet_H);
//			cdsCHGCD2(view.data.TKCHGCD, super.openDataSet_H);
			// 12.10.11 SE-362 Redmine#3423 対応 end
			cdsCHGCD2(view.data.TKCHGCD, openDataSet_H_cld3);
		}
		private function openDataSet_H_cld3(): void
		{
			// 訪問日和暦変換
			getEra(g_date, openDataSet_H_cld4);
		}
		private function openDataSet_H_cld4(): void
		{
			// 和暦訪問日取得
			g_erahdat = getEra_ret.dat + " " + g_time;
			// 得意先情報取得
			cdsTORI(g_cucd, openDataSet_H_cld5);
		}
		private function openDataSet_H_cld5(): void
		{
			// 前回訪問日和暦変換
			getEra(arrTORI[0].LHDAT, openDataSet_H_cld6);
		}
		private function openDataSet_H_cld6(): void
		{
			// 和暦前回訪問日取得
			g_eralhdat = getEra_ret.dat + " " + arrTORI[0].LHTIME;

			super.openDataSet_H();
		}
		
		/**********************************************************
		 * ボディデータ取得
		 * @author  12.09.06 SE-362
		 **********************************************************/
		override public function openDataSet_B(): void
		{
			// 明細情報
			cdsMA042X1(openDataSet_B_cld1);
		}
		
		private function openDataSet_B_cld1(): void
		{
			// 税率取得
// 14.03.03 SE-233 start
//			getTax(arrMA042X2[0].LHDAT, super.openDataSet_B);
// 14.04.01 SE-233 start
//			getTax(arrMA042X2[0].HDAT, super.openDataSet_B);
			getTax(g_date, super.openDataSet_B);
// 14.04.01 SE-233 end
// 14.03.03 SE-233 end
		}

		/**********************************************************
		 * 画面項目セット
		 * @author SE-354 
		 * @date 12.04.27
		 **********************************************************/
		override public function setDispObject(): void
		{
			super.setDispObject();
			
			// 商品追加前の情報を退避
			g_updateCount = arrMA042X1.length;
			
			// 明細項目表示
			if(arrMA042X1 != null && arrMA042X1.length != 0){
				view.grp1.dataProvider = arrMA042X1;
				
				calcSum();
			}
		}

		/**********************************************************
		 * ヘッダ情報取得
		 * @author 12.09.06 SE-362
		 **********************************************************/
		public function cdsMA042X(func: Function=null): void
		{
			// 引数情報
			cdsMA042X_arg = new Object;
			cdsMA042X_arg.func = func;
			
			stmt = new SQLStatement();
			stmt.sqlConnection =  singleton.sqlconnection;
			stmt.text= singleton.g_query_xml.entry.(@key == "cdsMA040X1");
			stmt.itemClass = W_MA040X;
			stmt.parameters[0] = g_cucd;
			
			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, cdsMA042XResult, false, 0, true);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler, false, 0, true);
			
			//実行
			stmt.execute();
		}
		
		public function cdsMA042XResult(event: SQLEvent): void
		{	
			var w_arg: Object = cdsMA042X_arg;
			var result:SQLResult = stmt.getResult();
			
			stmt.removeEventListener(SQLEvent.RESULT, cdsMA042XResult);				
			stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);				
			stmt=null;
			
			arrMA042X2 = new ArrayCollection(result.data);
			
			// 次関数実行
			if(w_arg.func)
				w_arg.func();
		}

		/**********************************************************
		 * 明細情報取得
		 * @author 12.09.06 SE-362
		 **********************************************************/
		public function cdsMA042X1(func: Function=null): void
		{
			// 引数情報
			cdsMA042X_arg = new Object;
			cdsMA042X_arg.func = func;
			
			stmt = new SQLStatement();
			stmt.sqlConnection =  singleton.sqlconnection;
			stmt.text= singleton.g_query_xml.entry.(@key == "cdsMA040X2");
			stmt.itemClass = W_MA041X;
			stmt.parameters[0] = g_cucd;
			
			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, cdsMA042X1Result, false, 0, true);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler, false, 0, true);
			
			//実行
			stmt.execute();
		}
		
		public function cdsMA042X1Result(event: SQLEvent): void
		{	
			var w_arg: Object = cdsMA042X_arg;
			var result:SQLResult = stmt.getResult();
			
			stmt.removeEventListener(SQLEvent.RESULT, cdsMA042X1Result);				
			stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);				
			stmt=null;
			
			arrMA042X1 = new ArrayCollection(result.data);
			
			// 次関数実行
			if(w_arg.func)
				w_arg.func();
		}
		
		/**********************************************************
		 * 商品別配置履歴取得
		 * @author 12.09.06 SE-362
		 **********************************************************/
		public function cdsMA042X2(vjan: String, gdnm: String, func: Function=null): void
		{
			// 商品情報表示
			view.lblVJAN.text = vjan;
			view.lblGDNM.text = gdnm;
			
			// 引数情報
			cdsMA042X_arg = new Object;
			cdsMA042X_arg.func = func;
			cdsMA042X_arg.vjan = vjan;
			
			stmt = new SQLStatement();
			stmt.sqlConnection =  singleton.sqlconnection;
//			stmt.text= singleton.g_query_xml.entry.(@key == "cdsMA040X3");
			stmt.text = "SELECT HDAT, SUM(VPNO) VPNO FROM (SELECT * FROM LF002 WHERE TKCD = '" + g_cucd + "' AND VJAN = '" + vjan + "' ) GROUP BY HDAT ORDER BY HDAT DESC ";
			stmt.itemClass = MA042V01;
			
			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, cdsMA042X2Result, false, 0, true);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler, false, 0, true);
			
			//実行
			stmt.execute();
		}
		
		public function cdsMA042X2Result(event: SQLEvent): void
		{	
			var w_arg: Object = cdsMA042X_arg;
			var result:SQLResult = stmt.getResult();
			
			stmt.removeEventListener(SQLEvent.RESULT, cdsMA042X2Result);				
			stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);				
			stmt=null;
			
			arrMA042X3 = new ArrayCollection(result.data);
			
			view.grdW01.dataProvider = arrMA042X3;
			
			// 次関数実行
			cdsMA042X3(w_arg.vjan)
		}

		/**********************************************************
		 * グラフ
		 * @author 12.09.06 SE-362
		 **********************************************************/
		public function cdsMA042X3(vjan: String, func: Function=null): void
		{
			// 引数情報
			cdsMA042X_arg = new Object;
			cdsMA042X_arg.func = func;
			
			stmt = new SQLStatement();
			stmt.sqlConnection =  singleton.sqlconnection;
			//			stmt.text= singleton.g_query_xml.entry.(@key == "cdsMA040X3");
			stmt.text= singleton.g_query_xml.entry.(@key == "qryMA042X3");
			stmt.itemClass = MA042V02;
			stmt.parameters[0] = vjan;
			
			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, cdsMA042X3Result, false, 0, true);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler, false, 0, true);
			
			//実行
			stmt.execute();
		}
		
		public function cdsMA042X3Result(event: SQLEvent): void
		{	
			var w_arg: Object = cdsMA042X_arg;
			var result:SQLResult = stmt.getResult();
			
			stmt.removeEventListener(SQLEvent.RESULT, cdsMA042X3Result);				
			stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);				
			stmt=null;
			
			arrMA042X4 = new ArrayCollection(result.data);
			
			view.lineChart.dataProvider = arrMA042X4;
			
			// 次関数実行
			if(w_arg.func)
				w_arg.func();
		}

		private function stmtErrorHandler(event: SQLErrorEvent): void
		{
			//IEventDispatcher(event.currentTarget).removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);				
			
			// エラーの表示
			trace(event.error.message);
		}

		/**********************************************************
		 * チェック
		 * @author 12.09.06 SE-362
		 **********************************************************/
		override public function checkDataSet():void
		{
			if(arrMA042X1 != null && arrMA042X1.length != 0){
				// 配置数が0以下はエラー
				for(var i:int=0; i < arrMA042X1.length; i++){
					if(BasedMethod.asCurrency(arrMA042X1[i].KHVPNO) < 0){
						singleton.sitemethod.dspJoinErrMsg("E0611");
						doF10Proc_last();
						return;
					}
				}
			}
			
			super.checkDataSet();
		}

		/**********************************************************
		 * 合計金額算出
		 * @author 12.09.06 SE-362
		 **********************************************************/
		public function calcSum(): void
		{
			var sumamt: String;
			
			for(var i:int=0; i<arrMA042X1.length; i++){
//				sumamt = BasedMethod.calcNum(sumamt, arrMA042X1[i].TAXAMT);
				sumamt = BasedMethod.calcNum(sumamt, BasedMethod.calcNum(arrMA042X1[i].KHVPNO, arrMA042X1[i].TAXSAL,2));
			}
			
			view.edtSUM.text = sumamt;
		}

		/**********************************************************
		 * btnDEC クリック時処理.
		 * @author 13.01.25 SE-233
		 **********************************************************/
		public function btnDECClick(): void
		{
			// 入力チェック
			singleton.sitemethod.dspJoinErrMsg("I0007", btnDECClick_cld1, "", "1", [], btnDECClick_cld2);
		}
		private function btnDECClick_cld1(): void 
		{
			doF10Proc();
		}
		private function btnDECClick_cld2(): void 
		{
			return;
		}
		
		/**********************************************************
		 * 登録処理
		 * @author 12.09.06 SE-362
		 **********************************************************/
		override public function execDecPLSQL():void
		{
			g_updateIndex = 0;
			g_vlin = 0;

			// 挿入のみのレコード数
			g_updateCount = arrMA042X1.length;

// 13.04.16 SE-233 start
			// 明細ワーク更新処理(前回データ削除)
//			delMA042X2(execDecPLSQL_cld1);
			cdsSEIGYO(execDecPLSQL_cld0);
// 13.04.16 SE-233 end
		}

// 13.04.16 SE-233 start
		private function execDecPLSQL_cld0(): void
		{
			// 明細ワーク更新処理(前回データ削除)
			delMA042X2(execDecPLSQL_cld1);
		}
// 13.04.16 SE-233 end
		
		private function execDecPLSQL_cld1(): void
		{
			// 商品追加で数量0の明細は登録しない
			if((arrMA042X1[g_updateIndex].ZHVPNO == 0) && (arrMA042X1[g_updateIndex].KRVPNO == 0) &&
				(arrMA042X1[g_updateIndex].KLVPNO == 0) && (arrMA042X1[g_updateIndex].KNVPNO == 0)){
				execDecPLSQL_cld2();
			}else{
				// 行ＮＯカウントアップ
				g_vlin++;
				
				// 明細ワーク更新処理(追加)
				insMA042X2(execDecPLSQL_cld2);
			}
		}
		
		private function execDecPLSQL_cld2(): void
		{
			// カウントアップ
			g_updateIndex++;

			if(g_updateIndex == g_updateCount){
				execDecPLSQL_cld3();
			}else{
				execDecPLSQL_cld1();
			}
		}
		
/*		private function execDecPLSQL_cld3(): void
		{
			if(g_insertCount != g_updateCount){
				// 明細ワーク挿入
				insMA042X1(execDecPLSQL_cld4);
				return;
			}
			execDecPLSQL_cld5();
		}

		private function execDecPLSQL_cld4(): void
		{
			// カウントアップ
			g_updateIndex++;
			
			if(g_updateIndex == g_insertCount){
				execDecPLSQL_cld5();
			}else{
				execDecPLSQL_cld3();
			}
		}

		private function execDecPLSQL_cld1(): void
		{
			// 明細データ更新処理
			updMA042X2(execDecPLSQL_cld5);
		}
*/		
		private function execDecPLSQL_cld3(): void
		{
			// 訪問時間取得
			//g_time = BasedMethod.getTime();

			// ヘッダ更新
			updMA042X(execDecPLSQL_cld4);
		}
		
		private function execDecPLSQL_cld4(): void
		{
			if(modifyFlag > 0){
				execDecPLSQL_cld51();
				return;
			}
				
			// 制御マスタ更新(伝票NO)
			updMA042X3(execDecPLSQL_cld5);
		}
		
		private function execDecPLSQL_cld5(): void
		{
			// 訪問計画実績F更新
			updMA042X4(execDecPLSQL_cld51);
		}

		private function execDecPLSQL_cld51(): void
		{
			// 通しNOが空なら++
			if(g_tno == ""){
				g_tno = (Number(arrSEIGYO[0].TNO) + 1).toString()
			}
			
			// 制御マスタ更新(通しNO)
			//updMA042X6(execDecPLSQL_cld52);
			execDecPLSQL_cld52();
		}
		
		private function execDecPLSQL_cld52(): void
		{
			// 入力ワーク更新
			updMA042X5(execDecPLSQL_cld6);
		}
		
		private function execDecPLSQL_cld6(): void
		{
			// 伝票NoXXXXXXXが登録されました。
			//singleton.sitemethod.dspJoinErrMsg("I0004", execDecPLSQL_cld61, "", "0", ["伝票番号" + g_vseq]);
			execDecPLSQL_cld61();
		}
		
		private function execDecPLSQL_cld61(): void
		{
			// 帳票用配列作成処理
			createPrintArray(execDecPLSQL_cld7);
		}
		
		private function execDecPLSQL_cld7(): void
		{
			if(BasedMethod.nvl(arrMA042X2[0].SCLS, "0") == "13"){
				singleton.sitemethod.dspJoinErrMsg("I0005", execDecPLSQL_cld71, "", "0", ["配置薬引上確認書"]);
			}else{
				//singleton.sitemethod.dspJoinErrMsg("I0005", execDecPLSQL_cld71, "", "0", ["納品書(配置薬控)"]);
				singleton.sitemethod.dspJoinErrMsg("I0005", execDecPLSQL_cld71, "", "0", ["配置薬控(お客様用)"]);
			}
		}

		private function execDecPLSQL_cld71(): void
		{
			// 通しNO更新
			updTNO(execDecPLSQL_cld711);
		}
		
		private function execDecPLSQL_cld711(): void
		{
			// 帳票発行(納品書)
			if(BasedMethod.nvl(arrMA042X2[0].SCLS, "0") == "13"){
				// ログ出力
				insLF013(g_cucd,"配置薬引上確認書",g_vseq);
				// 引上の場合
				printMA042X3_header(execDecPLSQL_cld72)
			}else{
				// ログ出力
				insLF013(g_cucd,"配置薬控（お客様用）",g_vseq);
				
				printMA042X1_header(execDecPLSQL_cld72)
			}
		}
		
		// 12.10.22 SE-233 Redmine#3526 対応 start
		private function execDecPLSQL_cld72(): void
		{
			// 帳票用配列作成処理(会社控用)
			createPrintArray2(execDecPLSQL_cld8);
		}
		// 12.10.22 SE-233 Redmine#3526 対応 end
		
		private function execDecPLSQL_cld8(): void
		{
			//singleton.sitemethod.dspJoinErrMsg("I0005", execDecPLSQL_cld9, "", "0", ["配置薬(会社控)"]);
			singleton.sitemethod.dspJoinErrMsg("I0005", execDecPLSQL_cld9, "", "0", ["配置薬控(会社用)"]);
		}
		
		private function execDecPLSQL_cld9(): void
		{
			if(reportCount == 0){
				execDecPLSQL_cld10();
				return;
			}
			// 通しNO更新
			updTNO(execDecPLSQL_cld91);
		}

		private function execDecPLSQL_cld91(): void
		{
			// ログ出力
			insLF013(g_cucd,"配置薬控（会社用）",g_vseq);
			// 帳票発行(配置薬)
			printMA042X2_header(execDecPLSQL_cld10);
		}
		
		private function execDecPLSQL_cld10(): void
		{
			insLF013(g_cucd,"配置薬入替終了");

			// メニューに遷移
			// 12.10.11 SE-362 Redmine#3459 start
			view.stage.removeEventListener(KeyboardEvent.KEY_DOWN, onKeyDownHandler);
			
			view.navigator.popAll();
			//view.navigator.popToFirstView();
			view.navigator.pushView(mainHomeView);
			view.navigator.pushView(MA009X);
			view.navigator.pushView(MA010X, view.data);
			// 12.10.11 SE-362 Redmine#3459 end
			
			super.execDecPLSQL();
		}

		/**********************************************************
		 * ヘッダワーク更新
		 * @author 12.09.06 SE-362
		 **********************************************************/
		public function updMA042X(func: Function=null): void
		{
			// 引数情報
			cdsMA042X_arg = new Object;
			cdsMA042X_arg.func = func;

			stmt = new SQLStatement();
			stmt.sqlConnection =  singleton.sqlconnection;
			stmt.text= singleton.g_query_xml.entry.(@key == "delMA042X1");
			stmt.parameters[0] = g_vseq;

			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, updMA042X1, false, 0, true);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler, false, 0, true);
			
			//実行
			stmt.execute();

		}
		
		public function updMA042X1(event: SQLEvent): void
		{
			
			var w_tax: String = BasedMethod.nvl(String(getTax_ret.tax), "0");
			
			stmt.removeEventListener(SQLEvent.RESULT, updMA042X1);				
			stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);				
			stmt=null;
		
			stmt = new SQLStatement();
			stmt.sqlConnection =  singleton.sqlconnection;
			stmt.text= singleton.g_query_xml.entry.(@key == "insMA042X");
			
			// パラメタ情報
			stmt.parameters[0]   = g_vseq;
			stmt.parameters[1]   = g_cucd;
			// 12.10.31 SE-233 Redmine#3578 対応 start
			//stmt.parameters[2]   = arrSEIGYO[0].SDDAY;
			stmt.parameters[2]   = g_date;
			// 12.10.31 SE-233 Redmine#3578 対応 end
			stmt.parameters[3]   = g_time;
			stmt.parameters[4]   = arrMA042X2[0].LHDAT;
			stmt.parameters[5]   = arrMA042X2[0].SCLS;
			stmt.parameters[6]   = arrSEIGYO[0].CHGCD;
			stmt.parameters[7]   = arrSEIGYO[0].HTID;
			stmt.parameters[8]   = arrMA042X2[0].MEDIBAL;
			stmt.parameters[9]   = arrMA042X2[0].MEDIWITHTAX;
			stmt.parameters[10]   = arrMA042X2[0].MEDIWITHOUTTAX;
			stmt.parameters[11]  = arrMA042X2[0].MEDIAMT;
			stmt.parameters[12]  = arrMA042X2[0].MEDITAX;
			stmt.parameters[13]  = arrMA042X2[0].MEDIMONEY;
			stmt.parameters[14]  = arrMA042X2[0].MEDIDIS;
			stmt.parameters[15]  = arrMA042X2[0].MEDIKAS;
			//stmt.parameters[16]  = BasedMethod.calcNum(arrMA042X2[0].MEDIMONEY, w_tax, 2);
			stmt.parameters[16] = BasedMethod.calcNum(arrMA042X2[0].MEDIMONEY,
				BasedMethod.calcNum(arrMA042X2[0].MEDIMONEY,BasedMethod.calcNum(BasedMethod.calcNum(w_tax,"100"),"100",3,3),3,0,2),1);
			stmt.parameters[17]  = arrMA042X2[0].MEDINBAL;
			stmt.parameters[18]  = arrMA042X2[0].STOREBAL;
			stmt.parameters[19]  = arrMA042X2[0].STOREWITHTAX;
			stmt.parameters[20]  = arrMA042X2[0].STOREWITHOUTTAX;
			stmt.parameters[21]  = arrMA042X2[0].STOREAMT;
			stmt.parameters[22]  = arrMA042X2[0].STORETAX;
			stmt.parameters[23]  = arrMA042X2[0].STOREMONEY;
			stmt.parameters[24]  = arrMA042X2[0].STOREDIS;
			stmt.parameters[25]  = arrMA042X2[0].STOREKAS;
			//stmt.parameters[26]  = BasedMethod.calcNum(arrMA042X2[0].STOREMONEY, w_tax, 2);
			stmt.parameters[26] = BasedMethod.calcNum(arrMA042X2[0].STOREMONEY,
				BasedMethod.calcNum(arrMA042X2[0].STOREMONEY,BasedMethod.calcNum(BasedMethod.calcNum(w_tax,"100"),"100",3,3),3,0,2),1);
			stmt.parameters[27]  = arrMA042X2[0].STORENBAL;
			stmt.parameters[28]  = arrMA042X2[0].DEVBAL;
			stmt.parameters[29]  = arrMA042X2[0].DEVWITHTAX;
			stmt.parameters[30]  = arrMA042X2[0].DEVWITHOUTTAX;
			stmt.parameters[31]  = arrMA042X2[0].DEVAMT;
			stmt.parameters[32]  = arrMA042X2[0].DEVTAX;
			stmt.parameters[33]  = arrMA042X2[0].DEVMONEY;
			stmt.parameters[34]  = arrMA042X2[0].DEVDIS;
			stmt.parameters[35]  = arrMA042X2[0].DEVKAS;
			//stmt.parameters[36]  = BasedMethod.calcNum(arrMA042X2[0].DEVMONEY, w_tax, 2);
			stmt.parameters[36] = BasedMethod.calcNum(arrMA042X2[0].DEVMONEY,
				BasedMethod.calcNum(arrMA042X2[0].DEVMONEY,BasedMethod.calcNum(BasedMethod.calcNum(w_tax,"100"),"100",3,3),3,0,2),1);
			stmt.parameters[37]  = arrMA042X2[0].DEVNBAL;
// 今回値引率
			stmt.parameters[38]  = arrMA042X2[0].MEDIDISRT;
			stmt.parameters[39]  = arrMA042X2[0].STOREDISRT;
			stmt.parameters[40]  = arrMA042X2[0].DEVDISRT;
			stmt.parameters[41]  = arrMA042X2[0].MEDIDISF;
			stmt.parameters[42]  = arrMA042X2[0].STOREDISF;
			stmt.parameters[43]  = arrMA042X2[0].DEVDISF;
			stmt.parameters[44]  = view.data.HDAT;
			stmt.parameters[45]  = arrTORI[0].ESTAMT;
			
			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, updMA040XResult, false, 0, true);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler, false, 0, true);
			
			//実行
			stmt.execute();
		}
		
		public function updMA040XResult(event: SQLEvent): void
		{	
			var w_arg: Object = cdsMA042X_arg;
			var result:SQLResult = stmt.getResult();
			
			stmt.removeEventListener(SQLEvent.RESULT, updMA040XResult);				
			stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);				
			stmt=null;
			
			// 次関数実行
			if(w_arg.func)
				w_arg.func();
		}

		/**********************************************************
		 * 明細ワーク更新
		 * @author 12.09.06 SE-362
		 **********************************************************/
		public function delMA042X2(func: Function=null): void
		{
			// 引数情報
			cdsMA042X_arg = new Object;
			cdsMA042X_arg.func = func;

			stmt = new SQLStatement();
			stmt.sqlConnection =  singleton.sqlconnection;
			stmt.text= singleton.g_query_xml.entry.(@key == "delMA042X2");
			stmt.parameters[0] = g_vseq;

			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, delMA042X2Result, false, 0, true);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler, false, 0, true);

			//実行
			stmt.execute();

		}
		
		public function delMA042X2Result(event: SQLEvent): void
		{	
			var w_arg: Object = cdsMA042X_arg;
			var result:SQLResult = stmt.getResult();
			
			stmt.removeEventListener(SQLEvent.RESULT, delMA042X2Result);				
			stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);				
			stmt=null;
			
			// 次関数実行
			if(w_arg.func)
				w_arg.func();
		}

		public function insMA042X2(func: Function=null): void
		{
			// 引数情報
			cdsMA042X_arg = new Object;
			cdsMA042X_arg.func = func;
			
			stmt = new SQLStatement();
			stmt.sqlConnection =  singleton.sqlconnection;
			stmt.text= singleton.g_query_xml.entry.(@key == "insMA042X2");
			
			// パラメタ情報
			stmt.parameters[0]  = g_vseq;
			//stmt.parameters[1] = arrMA042X1[g_updateIndex].VSEQ;
			//stmt.parameters[1] = (g_updateIndex + 1).toString();
			stmt.parameters[1]  = g_vlin.toString();
			stmt.parameters[2]  = arrMA042X1[g_updateIndex].VJAN.slice(0, 1);
			stmt.parameters[3]  = arrMA042X1[g_updateIndex].GDCLS;
			stmt.parameters[4]  = arrMA042X1[g_updateIndex].VJAN;
			stmt.parameters[5]  = arrMA042X1[g_updateIndex].SAL;
			stmt.parameters[6]  = arrMA042X1[g_updateIndex].ZHVPNO;
			stmt.parameters[7]  = arrMA042X1[g_updateIndex].KZAN;
			stmt.parameters[8]  = arrMA042X1[g_updateIndex].KUVPNO;
			stmt.parameters[9]  = arrMA042X1[g_updateIndex].KNVPNO;
			stmt.parameters[10] = arrMA042X1[g_updateIndex].KRVPNO;
			stmt.parameters[11] = arrMA042X1[g_updateIndex].KLVPNO;
			stmt.parameters[12] = arrMA042X1[g_updateIndex].KHVPNO;
			stmt.parameters[13] = arrMA042X1[g_updateIndex].BHISK;
			
			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, insMA042X2Result, false, 0, true);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler, false, 0, true);
			
			//実行
			stmt.execute();
		}
		
		public function insMA042X2Result(event: SQLEvent): void
		{	
			var w_arg: Object = cdsMA042X_arg;
			var result:SQLResult = stmt.getResult();
			
			stmt.removeEventListener(SQLEvent.RESULT, insMA042X2Result);				
			stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);				
			stmt=null;
			
			// 次関数実行
			if(w_arg.func)
				w_arg.func();
		}

		/**********************************************************
		 * 制御マスタ更新
		 * @author 12.09.06 SE-362
		 **********************************************************/
		public function updMA042X3(func: Function=null): void
		{
			// 引数情報
			cdsMA042X_arg = new Object;
			cdsMA042X_arg.func = func;
			
			stmt = new SQLStatement();
			stmt.sqlConnection =  singleton.sqlconnection;
			stmt.text= singleton.g_query_xml.entry.(@key == "updMA042X3");
			
			// パラメタ情報
			// 入替伝票No
			stmt.parameters[0] = g_vseq;
			// 通しNo
			//stmt.parameters[1] = g_tno;
			
			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, updMA042X3Result, false, 0, true);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler, false, 0, true);
			
			//実行
			stmt.execute();
		}
		
		public function updMA042X3Result(event: SQLEvent): void
		{	
			var w_arg: Object = cdsMA042X_arg;
			var result:SQLResult = stmt.getResult();
			
			stmt.removeEventListener(SQLEvent.RESULT, updMA042X3Result);				
			stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);				
			stmt=null;
			
			// 次関数実行
			if(w_arg.func)
				w_arg.func();
		}

		/**********************************************************
		 * 訪問実績F更新
		 * @author 12.09.06 SE-362
		 **********************************************************/
		public function updMA042X4(func: Function=null): void
		{
			// 引数情報
			cdsMA042X_arg = new Object;
			cdsMA042X_arg.func = func;
			
			stmt = new SQLStatement();
			stmt.sqlConnection =  singleton.sqlconnection;
			stmt.text= singleton.g_query_xml.entry.(@key == "updMA042X4");
			
			// パラメタ情報
			// 入替訪問時間
			stmt.parameters[0] = g_time;
			// 担当者コード
			stmt.parameters[1] = arrSEIGYO[0].CHGCD;
			// 訪問日付
			// 12.10.31 SE-233 Redmine#3578 対応 start
			//stmt.parameters[2] = arrSEIGYO[0].SDDAY;
			//stmt.parameters[2] = g_date;
			stmt.parameters[2] = view.data.HDAT;
			// 12.10.31 SE-233 Redmine#3578 対応 end
			// 得意先コード
			stmt.parameters[3] = g_cucd;
			
			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, updMA042X4Result, false, 0, true);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler, false, 0, true);
			
			//実行
			stmt.execute();
		}
		
		public function updMA042X4Result(event: SQLEvent): void
		{	
			var w_arg: Object = cdsMA042X_arg;
			var result:SQLResult = stmt.getResult();
			
			stmt.removeEventListener(SQLEvent.RESULT, updMA042X4Result);				
			stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);				
			stmt=null;

			// 次関数実行
			if(w_arg.func)
				w_arg.func();
		}

		/**********************************************************
		 * 入力ワーク更新(※一旦削除)
		 * @author 12.09.06 SE-362
		 **********************************************************/
		public function updMA042X5(func: Function=null): void
		{
			// 引数情報
			cdsMA042X_arg = new Object;
			cdsMA042X_arg.func = func;
			
			stmt = new SQLStatement();
			stmt.sqlConnection =  singleton.sqlconnection;
			stmt.text= singleton.g_query_xml.entry.(@key == "delMA040X2");
			
			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, updMA042X51, false, 0, true);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler, false, 0, true);
			
			//実行
			stmt.execute();
		}

		public function updMA042X51(e: SQLEvent): void
		{
			var iterator:Iterator = new CustomIterator(arrMA042X1);
			var w_ma041x: W_MA041X = new W_MA041X();
			stmt = new SQLStatement();
			stmt.sqlConnection =  singleton.sqlconnection;

			stmt.removeEventListener(SQLEvent.RESULT, updMA042X51);				
			stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);				
			
			stmt.text= singleton.g_query_xml.entry.(@key == "insMA040X2");

			while (iterator.hasNext())
			{
				w_ma041x = iterator.next();
				
				stmt.parameters[0]  = g_cucd;
				stmt.parameters[1]  = g_vseq;
				stmt.parameters[2] = w_ma041x.VSEQ;
				stmt.parameters[3] = w_ma041x.BCLS;
				stmt.parameters[4] = w_ma041x.SCLS;
				stmt.parameters[5] = w_ma041x.GDCLS;
				stmt.parameters[6] = w_ma041x.MDCLS;
				stmt.parameters[7] = w_ma041x.MDCLSNMS;
				stmt.parameters[8] = w_ma041x.VJAN;
				stmt.parameters[9] = w_ma041x.GDNM;
				stmt.parameters[10] = w_ma041x.SAL;
				stmt.parameters[11] = w_ma041x.TAXSAL;
// 12.10.23 SE-233 Redmine#3492対応 start
//				stmt.parameters[12] = w_ma041x.AMT;
				stmt.parameters[12] = BasedMethod.calcNum(w_ma041x.SAL, w_ma041x.KUVPNO, 2);
//				stmt.parameters[13] = w_ma041x.TAXAMT;
				stmt.parameters[13] = BasedMethod.calcNum(w_ma041x.TAXSAL, w_ma041x.KUVPNO, 2);
// 12.10.23 SE-233 Redmine#3492対応 end
				stmt.parameters[14] = w_ma041x.ZHVPNO;
				stmt.parameters[15] = w_ma041x.KZAN;
				stmt.parameters[16] = w_ma041x.KUVPNO;
				stmt.parameters[17] = w_ma041x.KNVPNO;
				stmt.parameters[18] = w_ma041x.KRVPNO;
				stmt.parameters[19] = w_ma041x.KLVPNO;
				stmt.parameters[20] = w_ma041x.KHVPNO;
				stmt.parameters[21] = w_ma041x.GDCOM;
				stmt.parameters[22] = w_ma041x.BHISK;

				// 実行
				stmt.execute();
			}
			
			BasedMethod.execNextFunction(cdsMA042X_arg.func);
		}
		
		/**********************************************************
		 * 制御マスタ更新(通しNO)
		 * @author 12.10.29 SE-300
		 **********************************************************/
		public function updMA042X6(func: Function=null): void
		{
			// 引数情報
			cdsMA042X_arg = new Object;
			cdsMA042X_arg.func = func;
			
			stmt = new SQLStatement();
			stmt.sqlConnection =  singleton.sqlconnection;
			stmt.text= singleton.g_query_xml.entry.(@key == "updMA042X6");
			
			// パラメタ情報
			// 通しNo
			stmt.parameters[0] = g_tno;
			
			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, updMA042X6Result, false, 0, true);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler, false, 0, true);
			
			//実行
			stmt.execute();
		}
		
		public function updMA042X6Result(event: SQLEvent): void
		{	
			var w_arg: Object = cdsMA042X_arg;
			var result:SQLResult = stmt.getResult();
			
			stmt.removeEventListener(SQLEvent.RESULT, updMA042X6Result);				
			stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);				
			stmt=null;
			
			// 次関数実行
			if(w_arg.func)
				w_arg.func();
		}
		
		/**********************************************************
		 * 明細ワーク挿入
		 * @author 12.09.06 SE-362
		 **********************************************************/
/*		public function insMA042X1(func: Function=null): void
		{
			// 引数情報
			cdsMA042X_arg = new Object;
			cdsMA042X_arg.func = func;
			
			stmt = new SQLStatement();
			stmt.sqlConnection =  singleton.sqlconnection;
			stmt.text= singleton.g_query_xml.entry.(@key == "insMA040X2");
			
			// パラメタ情報
			stmt.parameters[0]  = g_cucd;
			stmt.parameters[1]  = g_vseq;
			stmt.parameters[2]  = arrMA042X1[g_updateIndex].BCLS;
			stmt.parameters[3]  = arrMA042X1[g_updateIndex].SCLS;
			stmt.parameters[4]  = arrMA042X1[g_updateIndex].GDCLS;
			stmt.parameters[5]  = arrMA042X1[g_updateIndex].VJAN;
			stmt.parameters[6]  = arrMA042X1[g_updateIndex].GDNM;
			stmt.parameters[7]  = arrMA042X1[g_updateIndex].SAL;
			stmt.parameters[8]  = arrMA042X1[g_updateIndex].AMT;
			stmt.parameters[9]  = arrMA042X1[g_updateIndex].ZHVPNO;
			stmt.parameters[10] = arrMA042X1[g_updateIndex].KZAN;
			stmt.parameters[11] = arrMA042X1[g_updateIndex].KUVPNO;
			stmt.parameters[12] = arrMA042X1[g_updateIndex].KNVPNO;
			stmt.parameters[13] = arrMA042X1[g_updateIndex].KRVPNO;
			stmt.parameters[14] = arrMA042X1[g_updateIndex].KLVPNO;
			stmt.parameters[15] = arrMA042X1[g_updateIndex].KHVPNO;
			stmt.parameters[16] = arrMA042X1[g_updateIndex].VSEQ;
			
			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, insMA040X2Result);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);
			
			//実行
			stmt.execute();
		}
		
		public function insMA040X2Result(event: SQLEvent): void
		{	
			var w_arg: Object = cdsMA042X_arg;
			var result:SQLResult = stmt.getResult();
			
			// 次関数実行
			if(w_arg.func)
				w_arg.func();
		}
*/
		/**********************************************************
		 * 帳票用配列作成
		 * @author 12.07.03 SE-354
		 **********************************************************/
		private function createPrintArray(func: Function): void
		{
			// 一括発行明細数
			var w_detail: int = 5;
			var w_length: int;
			var w_nodata: int;
			var i: int;
			
			arrPRINT = new ArrayCollection;
			
			if(arrMA042X1 != null && arrMA042X1.length != 0){
				w_length = arrMA042X1.length;
				//				w_nodata = w_length % w_detail;
			}else{
				return;
			}
			
			for(i=0; i < w_length; i++){
//				if(BasedMethod.nvl(arrMA042X1[i].KHVPNO, "0") != "0"){
				if((BasedMethod.nvl(arrMA042X1[i].KHVPNO, "0") != "0") || (BasedMethod.nvl(arrMA042X2[0].SCLS, "0") == "13")){
					// 使用されているもの  or 引上時
					arrPRINT.addItem(arrMA042X1[i]);
				}
			}
		
			if(arrPRINT.length % w_detail != 0){
				w_nodata = w_detail - (arrPRINT.length % w_detail);
			}
			
			reportCount = Math.floor(arrPRINT.length / w_detail) + 1;
			if(arrPRINT.length % w_detail == 0){
				reportCount--;
			}
			
			for(i=0; i < w_nodata; i++){
				arrPRINT.addItem(new W_MA041X());
			}
			
			func();
		}

		/**********************************************************
		 * 納品書発行
		 * @author 12.07.03 SE-354
		 **********************************************************/
		private function printMA042X1_header(func: Function): void
		{
			// 引数
			print_arg = new Object;
			print_arg.func = func;
			
			var hts: HTTPService = new HTTPService();
// 15.03.19 SE-233 start
// 15.01.19 SE-233 start
//			var params:Object = new Object();
//			var pIndex: String;
// 15.01.19 SE-233 end
// 15.03.19 SE-233 end
			var w_tknm1: String;
			var w_tknm2: String;

// 15.03.19 SE-233 start
// 15.01.19 SE-233 start
//			var nowDtail: int;
//			var w_vlin1: String;
//			var w_vlin2: String;
//			var w_vlin3: String;
//			var w_vlin4: String;
//			var w_vlin5: String;
// 15.01.19 SE-233 end
// 15.03.19 SE-233 end
			
			// 明細数セット
			currentCount = 0;
			
			if(reportCount == 0){
				//printMA042X1_last();
				BasedMethod.execNextFunction(print_arg.func);
				return;
			}

/*			
			if(view.edtCUNM.text.length > 20){
				w_tknm1 = view.edtCUNM.text.slice(0, 20);
				w_tknm2 = view.edtCUNM.text.slice(20, view.edtCUNM.text.length);
			}else{
				w_tknm1 = view.edtCUNM.text;
				w_tknm2 = "";
			}
*/
			var tknm: Object = BasedMethod.getTKNM(view.data.TKNM);
			
			hts.method = "GET";
// 15.03.19 SE-233 start
// 15.01.19 SE-233 start
//			hts.addEventListener(ResultEvent.RESULT, printMA042X1_body);
//			hts.addEventListener(ResultEvent.RESULT, printMA042X1_last);
			hts.addEventListener(ResultEvent.RESULT, printMA042X1_body, false, 0, true);
// 15.01.19 SE-233 end
// 15.03.19 SE-233 end
			hts.addEventListener(FaultEvent.FAULT, htsFault, false, 0, true);
			hts.url = "http://localhost:8080/Format/Print";
			
// 15.03.19 SE-233 start
// 15.01.19 SE-233 start
			hts.send({
				"__format_archive_url": "file:///sdcard/print/me040x.spfmtz",
				"__format_archive_update": "updateWithoutError",
				"__format_id_number": "1",
				"TKCD": g_cucd,
				"TKNM1": tknm.tknm1,
				"TKNM2": tknm.tknm2,
				"TKTEL": g_tktel,
				// 12.11.06 SE-233 Redmine#3628 対応 start
//				"HDAT": getEra_ret.dat + " " + g_time,
				// 12.11.06 SE-233 Redmine#3628 対応 end
				"HDAT": g_erahdat,
				"VNO": g_vseq,
				"CHGNM": arrCHGCD2[0].CHGNM,
				"HED1": arrSEIGYO[0].COSNM1,
				"HED2": arrSEIGYO[0].COSNM2,
				"HED3": arrSEIGYO[0].COSNM3,
				"ADR1": arrSEIGYO[0].ADD1,
				"ADR2": arrSEIGYO[0].ADD2,
				"ADR3": arrSEIGYO[0].ADD3,
				"ADR23": arrSEIGYO[0].ADD23,
				"TEL1": arrSEIGYO[0].TEL1,
				"TEL2": arrSEIGYO[0].TEL2,
				"TEL3": arrSEIGYO[0].TEL3,
				"(発行枚数)": "1"
			});
/***
			pIndex = currentCount.toString();
			
			params["__format_archive_url"] = "file:///sdcard/print/me040x.spfmtz";
			params["__format_archive_update"] = "updateWithoutError";
			params["__format_id_number[" + pIndex + "]"] = "1";
			params["__format[" + pIndex + "].TKCD"] = g_cucd;
			params["__format[" + pIndex + "].TKNM1"] = tknm.tknm1;
			params["__format[" + pIndex + "].TKNM2"] = tknm.tknm2;
			params["__format[" + pIndex + "].TKTEL"] = g_tktel;
			params["__format[" + pIndex + "].HDAT"] = g_erahdat;
			params["__format[" + pIndex + "].VNO"] = g_vseq;
			params["__format[" + pIndex + "].CHGNM"] = arrCHGCD2[0].CHGNM;
			params["__format[" + pIndex + "].HED1"] = arrSEIGYO[0].COSNM1;
			params["__format[" + pIndex + "].HED2"] = arrSEIGYO[0].COSNM2;
			params["__format[" + pIndex + "].HED3"] = arrSEIGYO[0].COSNM3;
			params["__format[" + pIndex + "].ADR1"] = arrSEIGYO[0].ADD1;
			params["__format[" + pIndex + "].ADR2"] = arrSEIGYO[0].ADD2;
			params["__format[" + pIndex + "].ADR3"] = arrSEIGYO[0].ADD3;
			params["__format[" + pIndex + "].ADR23"] = arrSEIGYO[0].ADD23;
			params["__format[" + pIndex + "].TEL1"] = arrSEIGYO[0].TEL1;
			params["__format[" + pIndex + "].TEL2"] = arrSEIGYO[0].TEL2;
			params["__format[" + pIndex + "].TEL3"] = arrSEIGYO[0].TEL3;

			for(currentCount=0; currentCount < reportCount; currentCount++){
	
				nowDtail = (currentCount + 1) * 5;
				
				if(BasedMethod.nvl(arrPRINT[nowDtail - 5].VJAN) != ""){
					w_vlin1 = (nowDtail - 4).toString();
				}else{
					w_vlin1 = "";
				}
				if(BasedMethod.nvl(arrPRINT[nowDtail - 4].VJAN) != ""){
					w_vlin2 = (nowDtail - 3).toString();
				}else{
					w_vlin2 = "";
				}
				if(BasedMethod.nvl(arrPRINT[nowDtail - 3].VJAN) != ""){
					w_vlin3 = (nowDtail - 2).toString();
				}else{
					w_vlin3 = "";
				}
				if(BasedMethod.nvl(arrPRINT[nowDtail - 2].VJAN) != ""){
					w_vlin4 = (nowDtail - 1).toString();
				}else{
					w_vlin4 = "";
				}
				if(BasedMethod.nvl(arrPRINT[nowDtail - 1].VJAN) != ""){
					w_vlin5 = (nowDtail).toString();
				}else{
					w_vlin5 = "";
				}
			
				pIndex = (currentCount + 1).toString();
				
				params["__format_id_number[" + pIndex + "]"] = "2";
				params["__format[" + pIndex + "].VLIN1"] = w_vlin1;
				params["__format[" + pIndex + "].VLIN2"] = w_vlin2;
				params["__format[" + pIndex + "].VLIN3"] = w_vlin3;
				params["__format[" + pIndex + "].VLIN4"] = w_vlin4;
				params["__format[" + pIndex + "].VLIN5"] = w_vlin5;
				params["__format[" + pIndex + "].CLS1"] = BasedMethod.nvl(arrPRINT[nowDtail - 5].MDCLSNMS);
				params["__format[" + pIndex + "].CLS2"] = BasedMethod.nvl(arrPRINT[nowDtail - 4].MDCLSNMS);
				params["__format[" + pIndex + "].CLS3"] = BasedMethod.nvl(arrPRINT[nowDtail - 3].MDCLSNMS);
				params["__format[" + pIndex + "].CLS4"] = BasedMethod.nvl(arrPRINT[nowDtail - 2].MDCLSNMS);
				params["__format[" + pIndex + "].CLS5"] = BasedMethod.nvl(arrPRINT[nowDtail - 1].MDCLSNMS);
				params["__format[" + pIndex + "].GDNM1"] = BasedMethod.nvl(arrPRINT[nowDtail - 5].GDNM);
				params["__format[" + pIndex + "].GDNM2"] = BasedMethod.nvl(arrPRINT[nowDtail - 4].GDNM);
				params["__format[" + pIndex + "].GDNM3"] = BasedMethod.nvl(arrPRINT[nowDtail - 3].GDNM);
				params["__format[" + pIndex + "].GDNM4"] = BasedMethod.nvl(arrPRINT[nowDtail - 2].GDNM);
				params["__format[" + pIndex + "].GDNM5"] = BasedMethod.nvl(arrPRINT[nowDtail - 1].GDNM);
				params["__format[" + pIndex + "].CST1"] = BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(arrPRINT[nowDtail - 5].TAXSAL, "1", "0", "0"));
				params["__format[" + pIndex + "].CST2"] = BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(arrPRINT[nowDtail - 4].TAXSAL, "1", "0", "0"));
				params["__format[" + pIndex + "].CST3"] = BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(arrPRINT[nowDtail - 3].TAXSAL, "1", "0", "0"));
				params["__format[" + pIndex + "].CST4"] = BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(arrPRINT[nowDtail - 2].TAXSAL, "1", "0", "0"));
				params["__format[" + pIndex + "].CST5"] = BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(arrPRINT[nowDtail - 1].TAXSAL, "1", "0", "0"));
				params["__format[" + pIndex + "].CNO1"] = BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(arrPRINT[nowDtail - 5].KHVPNO, "1", "0", "0"));
				params["__format[" + pIndex + "].CNO2"] = BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(arrPRINT[nowDtail - 4].KHVPNO, "1", "0", "0"));
				params["__format[" + pIndex + "].CNO3"] = BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(arrPRINT[nowDtail - 3].KHVPNO, "1", "0", "0"));
				params["__format[" + pIndex + "].CNO4"] = BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(arrPRINT[nowDtail - 2].KHVPNO, "1", "0", "0"));
				params["__format[" + pIndex + "].CNO5"] = BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(arrPRINT[nowDtail - 1].KHVPNO, "1", "0", "0"));
				params["__format[" + pIndex + "].GDCOM1"] = BasedMethod.nvl(arrPRINT[nowDtail - 5].GDCOM);
				params["__format[" + pIndex + "].GDCOM2"] = BasedMethod.nvl(arrPRINT[nowDtail - 4].GDCOM);
				params["__format[" + pIndex + "].GDCOM3"] = BasedMethod.nvl(arrPRINT[nowDtail - 3].GDCOM);
				params["__format[" + pIndex + "].GDCOM4"] = BasedMethod.nvl(arrPRINT[nowDtail - 2].GDCOM);
				params["__format[" + pIndex + "].GDCOM5"] = BasedMethod.nvl(arrPRINT[nowDtail - 1].GDCOM);
			}			

			pIndex = (currentCount + 1).toString();
			
			params["__format_id_number[" + pIndex + "]"] = "3";
			params["__format[" + pIndex + "].TNO"] = getTNO();
			
			hts.send(params);
***/		
// 15.01.19 SE-233 end			
// 15.03.19 SE-233 end			
		}
		
		private function printMA042X1_body(e: ResultEvent=null): void
		{
			if(e != null){
				// 印刷成功時処理
				if(e.result.response.result == "OK"){
					// 印刷失敗時 result == NG
				}else{
					singleton.sitemethod.dspJoinErrMsg(e.result.response.message,
						doF10Proc_last, "", "0", null, null, true);
					return;
				}
				//IEventDispatcher(e.currentTarget).removeEventListener(ResultEvent.RESULT, printMA042X1_body);				
				//IEventDispatcher(e.currentTarget).removeEventListener(FaultEvent.FAULT, htsFault);				
			}

			var hts: HTTPService = new HTTPService();
			var nowDtail: int;
			
			hts.method = "GET";
			hts.addEventListener(ResultEvent.RESULT, printLoop, false, 0, true);
			hts.addEventListener(FaultEvent.FAULT, htsFault, false, 0, true);
			hts.url = "http://localhost:8080/Format/Print";
			
			nowDtail = (currentCount + 1) * 5;

			// 12.10.03 SE-362 明細がない場合は行NOを印字しない start
			var w_vlin1: String;
			var w_vlin2: String;
			var w_vlin3: String;
			var w_vlin4: String;
			var w_vlin5: String;
			
			if(BasedMethod.nvl(arrPRINT[nowDtail - 5].VJAN) != ""){
				w_vlin1 = (nowDtail - 4).toString();
			}else{
				w_vlin1 = "";
			}
			if(BasedMethod.nvl(arrPRINT[nowDtail - 4].VJAN) != ""){
				w_vlin2 = (nowDtail - 3).toString();
			}else{
				w_vlin2 = "";
			}
			if(BasedMethod.nvl(arrPRINT[nowDtail - 3].VJAN) != ""){
				w_vlin3 = (nowDtail - 2).toString();
			}else{
				w_vlin3 = "";
			}
			if(BasedMethod.nvl(arrPRINT[nowDtail - 2].VJAN) != ""){
				w_vlin4 = (nowDtail - 1).toString();
			}else{
				w_vlin4 = "";
			}
			if(BasedMethod.nvl(arrPRINT[nowDtail - 1].VJAN) != ""){
				w_vlin5 = (nowDtail).toString();
			}else{
				w_vlin5 = "";
			}
			// 12.10.03 SE-362 明細がない場合は行NOを印字しない end			

			hts.send({
				"__format_archive_url": "file:///sdcard/print/me040x.spfmtz",
				"__format_archive_update": "updateWithoutError",
				"__format_id_number": "2",
				"VLIN1": w_vlin1,
				"VLIN2": w_vlin2,
				"VLIN3": w_vlin3,
				"VLIN4": w_vlin4,
				"VLIN5": w_vlin5,
				"CLS1" : BasedMethod.nvl(arrPRINT[nowDtail - 5].MDCLSNMS),
				"CLS2" : BasedMethod.nvl(arrPRINT[nowDtail - 4].MDCLSNMS),
				"CLS3" : BasedMethod.nvl(arrPRINT[nowDtail - 3].MDCLSNMS),
				"CLS4" : BasedMethod.nvl(arrPRINT[nowDtail - 2].MDCLSNMS),
				"CLS5" : BasedMethod.nvl(arrPRINT[nowDtail - 1].MDCLSNMS),
				"GDNM1": BasedMethod.nvl(arrPRINT[nowDtail - 5].GDNM),
				"GDNM2": BasedMethod.nvl(arrPRINT[nowDtail - 4].GDNM),
				"GDNM3": BasedMethod.nvl(arrPRINT[nowDtail - 3].GDNM),
				"GDNM4": BasedMethod.nvl(arrPRINT[nowDtail - 2].GDNM),
				"GDNM5": BasedMethod.nvl(arrPRINT[nowDtail - 1].GDNM),
				"CST1" : BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(arrPRINT[nowDtail - 5].TAXSAL, "1", "0", "0")),
				"CST2" : BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(arrPRINT[nowDtail - 4].TAXSAL, "1", "0", "0")),
				"CST3" : BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(arrPRINT[nowDtail - 3].TAXSAL, "1", "0", "0")),
				"CST4" : BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(arrPRINT[nowDtail - 2].TAXSAL, "1", "0", "0")),
				"CST5" : BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(arrPRINT[nowDtail - 1].TAXSAL, "1", "0", "0")),
				"CNO1" : BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(arrPRINT[nowDtail - 5].KHVPNO, "1", "0", "0")),
				"CNO2" : BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(arrPRINT[nowDtail - 4].KHVPNO, "1", "0", "0")),
				"CNO3" : BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(arrPRINT[nowDtail - 3].KHVPNO, "1", "0", "0")),
				"CNO4" : BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(arrPRINT[nowDtail - 2].KHVPNO, "1", "0", "0")),
				"CNO5" : BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(arrPRINT[nowDtail - 1].KHVPNO, "1", "0", "0")),
				"GDCOM1" : BasedMethod.nvl(arrPRINT[nowDtail - 5].GDCOM),
				"GDCOM2" : BasedMethod.nvl(arrPRINT[nowDtail - 4].GDCOM),
				"GDCOM3" : BasedMethod.nvl(arrPRINT[nowDtail - 3].GDCOM),
				"GDCOM4" : BasedMethod.nvl(arrPRINT[nowDtail - 2].GDCOM),
				"GDCOM5" : BasedMethod.nvl(arrPRINT[nowDtail - 1].GDCOM),
				"(発行枚数)": "1"
			});
		}
		
		private function printLoop(e: ResultEvent=null): void
		{
			// カウントアップ
			currentCount++;
			
			if(e != null){
				// 印刷成功時処理
				if(e.result.response.result == "OK"){
					// 印刷失敗時 result == NG
				}else{
					singleton.sitemethod.dspJoinErrMsg(e.result.response.message,
						doF10Proc_last, "", "0", null, null, true);
					return;
				}
				//IEventDispatcher(e.currentTarget).removeEventListener(ResultEvent.RESULT, printLoop);				
				//IEventDispatcher(e.currentTarget).removeEventListener(FaultEvent.FAULT, htsFault);				
			}

			if(currentCount != reportCount)
				printMA042X1_body();
			else
				printMA042X1_futter();
		}

		private function printMA042X1_futter(): void
		{
			var hts: HTTPService = new HTTPService();
			
			hts.method = "GET";
			hts.addEventListener(ResultEvent.RESULT, printMA042X1_last, false, 0, true);
			hts.addEventListener(FaultEvent.FAULT, htsFault, false, 0, true);
			hts.url = "http://localhost:8080/Format/Print";
			
			hts.send({
				"__format_archive_url": "file:///sdcard/print/me040x.spfmtz",
				"__format_archive_update": "updateWithoutError",
				"__format_id_number": "3",
				"TNO": getTNO(),
				"(発行枚数)": "1"
			});
		}
		
		private function printMA042X1_last(e: ResultEvent=null): void
		{
			if(e != null){
				// 印刷成功時処理
				if(e.result.response.result == "OK"){
					// 印刷失敗時 result == NG
				}else{
					singleton.sitemethod.dspJoinErrMsg(e.result.response.message,
						doF10Proc_last, "", "0", null, null, true);
					return;
				}
				//IEventDispatcher(e.currentTarget).removeEventListener(ResultEvent.RESULT, printMA042X1_last);				
				//IEventDispatcher(e.currentTarget).removeEventListener(FaultEvent.FAULT, htsFault);				
			}

			BasedMethod.execNextFunction(print_arg.func);
		}
		
		
		/**********************************************************
		 * 引上確認書発行
		 * @author 12.12.21 SE-233
		 **********************************************************/
		private function printMA042X3_header(func: Function): void
		{
			// 引数
			print_arg = new Object;
			print_arg.func = func;
			
			var hts: HTTPService = new HTTPService();
			var w_tknm1: String;
			var w_tknm2: String;
			var tknm: Object = BasedMethod.getTKNM(view.data.TKNM);
			
			hts.method = "GET";
			hts.addEventListener(ResultEvent.RESULT, printMA042X3_last, false, 0, true);
			hts.addEventListener(FaultEvent.FAULT, htsFault, false, 0, true);
			hts.url = "http://localhost:8080/Format/Print";
			
			hts.send({
				"__format_archive_url": "file:///sdcard/print/me041x.spfmtz",
				"__format_archive_update": "updateWithoutError",
				"__format_id_number": "1",
				"TKCD": g_cucd,
				"TKNM1": tknm.tknm1,
				"TKNM2": tknm.tknm2,
				"TKTEL": g_tktel,
//				"HDAT": getEra_ret.dat + " " + g_time,
				"HDAT": g_erahdat,
				"VNO": g_vseq,
				"CHGNM": arrCHGCD2[0].CHGNM,
				"HED1": arrSEIGYO[0].COSNM1,
				"HED2": arrSEIGYO[0].COSNM2,
				"HED3": arrSEIGYO[0].COSNM3,
				"ADR1": arrSEIGYO[0].ADD1,
				"ADR2": arrSEIGYO[0].ADD2,
				"ADR3": arrSEIGYO[0].ADD3,
				"ADR23": arrSEIGYO[0].ADD23,
				"TEL1": arrSEIGYO[0].TEL1,
				"TEL2": arrSEIGYO[0].TEL2,
				"TEL3": arrSEIGYO[0].TEL3,
				"TNO": getTNO(),
				"(発行枚数)": "1"
			});
		}
		
		private function printMA042X3_last(e: ResultEvent=null): void
		{
			if(e != null){
				// 印刷成功時処理
				if(e.result.response.result == "OK"){
					// 印刷失敗時 result == NG
				}else{
					singleton.sitemethod.dspJoinErrMsg(e.result.response.message,
						doF10Proc_last, "", "0", null, null, true);
					return;
				}
				//IEventDispatcher(e.currentTarget).removeEventListener(ResultEvent.RESULT, printMA042X3_last);				
				//IEventDispatcher(e.currentTarget).removeEventListener(FaultEvent.FAULT, htsFault);				
			}

			BasedMethod.execNextFunction(print_arg.func);
		}
		
		private function htsFault(e: FaultEvent): void
		{
			//IEventDispatcher(e.currentTarget).removeEventListener(FaultEvent.FAULT, htsFault);				
			singleton.sitemethod.dspJoinErrMsg("印刷に失敗しました。サーバを起動してから再度実行して下さい。",
				doF10Proc_last, "", "0", null, null, true);
		}
		
		
		// 12.10.22 SE-233 Redmine#3526 対応 start
		/**********************************************************
		 * 帳票用配列作成（会社控）
		 * @author 12.10.22 SE-233
		 **********************************************************/
		private function createPrintArray2(func: Function): void
		{
			// 一括発行明細数
			var w_detail: int = 5;
			var w_length: int;
			var w_nodata: int;
			var i: int;
			
			arrPRINT = new ArrayCollection;
			
			if(arrMA042X1 != null && arrMA042X1.length != 0){
				w_length = arrMA042X1.length;
			}else{
				return;
			}
			
			for(i=0; i < w_length; i++){
				if((BasedMethod.nvl(arrMA042X1[i].ZHVPNO, "0") != "0") || (BasedMethod.nvl(arrMA042X1[i].KHVPNO, "0") != "0")){
					// 使用されているもの
					arrPRINT.addItem(arrMA042X1[i]);
				}
			}
			
			if(arrPRINT.length % w_detail != 0){
				w_nodata = w_detail - (arrPRINT.length % w_detail);
			}
			
			reportCount = Math.floor(arrPRINT.length / w_detail) + 1;
			if(arrPRINT.length % w_detail == 0){
				reportCount--;
			}
			
			for(i=0; i < w_nodata; i++){
				arrPRINT.addItem(new W_MA041X());
			}
			
			func();
		}
		// 12.10.22 SE-233 Redmine#3526 対応 end
		
		/**********************************************************
		 * 配置薬発行
		 * @author 12.07.03 SE-354
		 **********************************************************/
		private function printMA042X2_header(func: Function): void
		{
			// 引数
			print_arg = new Object;
			print_arg.func = func;

			var hts: HTTPService = new HTTPService();
// 15.03.19 SE-233 start
// 15.01.19 SE-233 start
//			var params:Object = new Object();
//			var pIndex: String;
// 15.01.19 SE-233 end
// 15.03.19 SE-233 end
			var w_tknm1: String;
			var w_tknm2: String;

// 15.03.19 SE-233 start
// 15.01.19 SE-233 start
//			var nowDtail: int;
//			var w_vlin1: String;
//			var w_vlin2: String;
//			var w_vlin3: String;
//			var w_vlin4: String;
//			var w_vlin5: String;
//			var w_kunum1: String;
//			var w_kunum2: String;
//			var w_kunum3: String;
//			var w_kunum4: String;
//			var w_kunum5: String;
//			var w_uamt1: String;
//			var w_uamt2: String;
//			var w_uamt3: String;
//			var w_uamt4: String;
//			var w_uamt5: String;
//			var w_nnum1: String;
//			var w_nnum2: String;
//			var w_nnum3: String;
//			var w_nnum4: String;
//			var w_nnum5: String;
//			var w_hnum1: String;
//			var w_hnum2: String;
//			var w_hnum3: String;
//			var w_hnum4: String;
//			var w_hnum5: String;
//			var w_knum1: String;
//			var w_knum2: String;
//			var w_knum3: String;
//			var w_knum4: String;
//			var w_knum5: String;
//			
//			var ttl_bal: String;
//			var w_bal: String;
//			var w_bal1: String;
//			var w_dis: String;
//			var w_amt: String;
//			var w_tax: String;
//			var w_namt: String;
//			var w_zamt: String;
//			var w_uamt: String;
// 15.01.19 SE-233 end
// 15.03.19 SE-233 end
			
			// 明細数セット
			currentCount = 0;
			
			if(reportCount == 0){
				//printMA042X2_last();
				BasedMethod.execNextFunction(print_arg.func);
				return;
			}
/*			
			if(view.edtCUNM.text.length > 20){
				w_tknm1 = view.edtCUNM.text.slice(0, 20);
				w_tknm2 = view.edtCUNM.text.slice(20, view.edtCUNM.text.length);
			}else{
				w_tknm1 = view.edtCUNM.text;
				w_tknm2 = "";
			}
*/
			var tknm: Object = BasedMethod.getTKNM(view.data.TKNM);
			
			hts.method = "GET";
// 15.03.19 SE-233 start
// 15.01.19 SE-233 start
//			hts.addEventListener(ResultEvent.RESULT, printMA042X2_body);
//			hts.addEventListener(ResultEvent.RESULT, printMA042X2_last);
			hts.addEventListener(ResultEvent.RESULT, printMA042X2_body, false, 0, true);
// 15.01.19 SE-233 end
// 15.03.19 SE-233 end
			hts.addEventListener(FaultEvent.FAULT, htsFault, false, 0, true);
			hts.url = "http://localhost:8080/Format/Print";
			
// 15.03.19 SE-233 start
// 15.01.19 SE-233 start
			hts.send({
				"__format_archive_url": "file:///sdcard/print/me045x.spfmtz",
				"__format_archive_update": "updateWithoutError",
				"__format_id_number": "1",
				"TKCD": g_cucd,
				"TKNM1": tknm.tknm1,
				"TKNM2": tknm.tknm2,
				"TKTEL": g_tktel,
				// 12.11.06 SE-233 Redmine#3628 対応 start
//				"HDAT": getEra_ret.dat + " " + g_time,
				// 12.11.06 SE-233 Redmine#3628 対応 end
				"HDAT": g_erahdat,
				"LHDAT": g_eralhdat,
				"VNO": g_vseq,
				"CHGNM": arrCHGCD2[0].CHGNM,
				"HED1": arrSEIGYO[0].COSNM1,
				"HED2": arrSEIGYO[0].COSNM2,
				"HED3": arrSEIGYO[0].COSNM3,
				"ADR1": arrSEIGYO[0].ADD1,
				"ADR2": arrSEIGYO[0].ADD2,
				"ADR3": arrSEIGYO[0].ADD3,
				"TEL1": arrSEIGYO[0].TEL1,
				"TEL2": arrSEIGYO[0].TEL2,
				"TEL3": arrSEIGYO[0].TEL3,
				"(発行枚数)": "1"
			});
/***
			pIndex = currentCount.toString();
			
			params["__format_archive_url"] = "file:///sdcard/print/me045x.spfmtz";
			params["__format_archive_update"] = "updateWithoutError";
			params["__format_id_number[" + pIndex + "]"] = "1";
			params["__format[" + pIndex + "].TKCD"] = g_cucd;
			params["__format[" + pIndex + "].TKNM1"] = tknm.tknm1;
			params["__format[" + pIndex + "].TKNM2"] = tknm.tknm2;
			params["__format[" + pIndex + "].TKTEL"] = g_tktel;
			params["__format[" + pIndex + "].HDAT"] = g_erahdat;
			params["__format[" + pIndex + "].LHDAT"] = g_eralhdat;
			params["__format[" + pIndex + "].VNO"] = g_vseq;
			params["__format[" + pIndex + "].CHGNM"] = arrCHGCD2[0].CHGNM;
			params["__format[" + pIndex + "].HED1"] = arrSEIGYO[0].COSNM1;
			params["__format[" + pIndex + "].HED2"] = arrSEIGYO[0].COSNM2;
			params["__format[" + pIndex + "].HED3"] = arrSEIGYO[0].COSNM3;
			params["__format[" + pIndex + "].ADR1"] = arrSEIGYO[0].ADD1;
			params["__format[" + pIndex + "].ADR2"] = arrSEIGYO[0].ADD2;
			params["__format[" + pIndex + "].ADR3"] = arrSEIGYO[0].ADD3;
			params["__format[" + pIndex + "].TEL1"] = arrSEIGYO[0].TEL1;
			params["__format[" + pIndex + "].TEL2"] = arrSEIGYO[0].TEL2;
			params["__format[" + pIndex + "].TEL3"] = arrSEIGYO[0].TEL3;

			for(currentCount=0; currentCount < reportCount; currentCount++){

				nowDtail = (currentCount + 1) * 5;

				if(BasedMethod.nvl(arrPRINT[nowDtail - 5].VJAN) != ""){
					w_vlin1 = (nowDtail - 4).toString();
				}else{
					w_vlin1 = "";
				}
				if(BasedMethod.nvl(arrPRINT[nowDtail - 4].VJAN) != ""){
					w_vlin2 = (nowDtail - 3).toString();
				}else{
					w_vlin2 = "";
				}
				if(BasedMethod.nvl(arrPRINT[nowDtail - 3].VJAN) != ""){
					w_vlin3 = (nowDtail - 2).toString();
				}else{
					w_vlin3 = "";
				}
				if(BasedMethod.nvl(arrPRINT[nowDtail - 2].VJAN) != ""){
					w_vlin4 = (nowDtail - 1).toString();
				}else{
					w_vlin4 = "";
				}
				if(BasedMethod.nvl(arrPRINT[nowDtail - 1].VJAN) != ""){
					w_vlin5 = (nowDtail).toString();
				}else{
					w_vlin5 = "";
				}
				if(BasedMethod.nvl(arrPRINT[nowDtail - 5].KUVPNO,"0") != "0"){
					w_kunum1 = BasedMethod.nvl(arrPRINT[nowDtail - 5].KUVPNO);
				}else{
					w_kunum1 = "";
				}
				if(BasedMethod.nvl(arrPRINT[nowDtail - 4].KUVPNO,"0") != "0"){
					w_kunum2 = BasedMethod.nvl(arrPRINT[nowDtail - 4].KUVPNO);
				}else{
					w_kunum2 = "";
				}
				if(BasedMethod.nvl(arrPRINT[nowDtail - 3].KUVPNO,"0") != "0"){
					w_kunum3 = BasedMethod.nvl(arrPRINT[nowDtail - 3].KUVPNO);
				}else{
					w_kunum3 = "";
				}
				if(BasedMethod.nvl(arrPRINT[nowDtail - 2].KUVPNO,"0") != "0"){
					w_kunum4 = BasedMethod.nvl(arrPRINT[nowDtail - 2].KUVPNO);
				}else{
					w_kunum4 = "";
				}
				if(BasedMethod.nvl(arrPRINT[nowDtail - 1].KUVPNO,"0") != "0"){
					w_kunum5 = BasedMethod.nvl(arrPRINT[nowDtail - 1].KUVPNO);
				}else{
					w_kunum5 = "";
				}
				if(BasedMethod.nvl(BasedMethod.calcNum(arrPRINT[nowDtail - 5].KUVPNO, arrPRINT[nowDtail - 5].TAXSAL, 2),"0") != "0"){
					w_uamt1 = BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(BasedMethod.calcNum(arrPRINT[nowDtail - 5].KUVPNO, arrPRINT[nowDtail - 5].TAXSAL, 2), "1", "0", "0"));
				}else{
					w_uamt1 = "";
				}
				if(BasedMethod.nvl(BasedMethod.calcNum(arrPRINT[nowDtail - 4].KUVPNO, arrPRINT[nowDtail - 4].TAXSAL, 2),"0") != "0"){
					w_uamt2 = BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(BasedMethod.calcNum(arrPRINT[nowDtail - 4].KUVPNO, arrPRINT[nowDtail - 4].TAXSAL, 2), "1", "0", "0"));
				}else{
					w_uamt2 = "";
				}
				if(BasedMethod.nvl(BasedMethod.calcNum(arrPRINT[nowDtail - 3].KUVPNO, arrPRINT[nowDtail - 3].TAXSAL, 2),"0") != "0"){
					w_uamt3 = BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(BasedMethod.calcNum(arrPRINT[nowDtail - 3].KUVPNO, arrPRINT[nowDtail - 3].TAXSAL, 2), "1", "0", "0"));
				}else{
					w_uamt3 = "";
				}
				if(BasedMethod.nvl(BasedMethod.calcNum(arrPRINT[nowDtail - 2].KUVPNO, arrPRINT[nowDtail - 2].TAXSAL, 2),"0") != "0"){
					w_uamt4 = BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(BasedMethod.calcNum(arrPRINT[nowDtail - 2].KUVPNO, arrPRINT[nowDtail - 2].TAXSAL, 2), "1", "0", "0"));
				}else{
					w_uamt4 = "";
				}
				if(BasedMethod.nvl(BasedMethod.calcNum(arrPRINT[nowDtail - 1].KUVPNO, arrPRINT[nowDtail - 1].TAXSAL, 2),"0") != "0"){
					w_uamt5 = BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(BasedMethod.calcNum(arrPRINT[nowDtail - 1].KUVPNO, arrPRINT[nowDtail - 1].TAXSAL, 2), "1", "0", "0"));
				}else{
					w_uamt5 = "";
				}
				if(BasedMethod.nvl(arrPRINT[nowDtail - 5].KNVPNO,"0") != "0"){
					w_nnum1 = BasedMethod.nvl(arrPRINT[nowDtail - 5].KNVPNO);
				}else{
					w_nnum1 = "";
				}
				if(BasedMethod.nvl(arrPRINT[nowDtail - 4].KNVPNO,"0") != "0"){
					w_nnum2 = BasedMethod.nvl(arrPRINT[nowDtail - 4].KNVPNO);
				}else{
					w_nnum2 = "";
				}
				if(BasedMethod.nvl(arrPRINT[nowDtail - 3].KNVPNO,"0") != "0"){
					w_nnum3 = BasedMethod.nvl(arrPRINT[nowDtail - 3].KNVPNO);
				}else{
					w_nnum3 = "";
				}
				if(BasedMethod.nvl(arrPRINT[nowDtail - 2].KNVPNO,"0") != "0"){
					w_nnum4 = BasedMethod.nvl(arrPRINT[nowDtail - 2].KNVPNO);
				}else{
					w_nnum4 = "";
				}
				if(BasedMethod.nvl(arrPRINT[nowDtail - 1].KNVPNO,"0") != "0"){
					w_nnum5 = BasedMethod.nvl(arrPRINT[nowDtail - 1].KNVPNO);
				}else{
					w_nnum5 = "";
				}
				if(BasedMethod.nvl(arrPRINT[nowDtail - 5].KRVPNO,"0") != "0"){
					w_hnum1 = BasedMethod.nvl(arrPRINT[nowDtail - 5].KRVPNO);
				}else{
					w_hnum1 = "";
				}
				if(BasedMethod.nvl(arrPRINT[nowDtail - 4].KRVPNO,"0") != "0"){
					w_hnum2 = BasedMethod.nvl(arrPRINT[nowDtail - 4].KRVPNO);
				}else{
					w_hnum2 = "";
				}
				if(BasedMethod.nvl(arrPRINT[nowDtail - 3].KRVPNO,"0") != "0"){
					w_hnum3 = BasedMethod.nvl(arrPRINT[nowDtail - 3].KRVPNO);
				}else{
					w_hnum3 = "";
				}
				if(BasedMethod.nvl(arrPRINT[nowDtail - 2].KRVPNO,"0") != "0"){
					w_hnum4 = BasedMethod.nvl(arrPRINT[nowDtail - 2].KRVPNO);
				}else{
					w_hnum4 = "";
				}
				if(BasedMethod.nvl(arrPRINT[nowDtail - 1].KRVPNO,"0") != "0"){
					w_hnum5 = BasedMethod.nvl(arrPRINT[nowDtail - 1].KRVPNO);
				}else{
					w_hnum5 = "";
				}
				if(BasedMethod.nvl(arrPRINT[nowDtail - 5].KLVPNO,"0") != "0"){
					w_knum1 = BasedMethod.nvl(arrPRINT[nowDtail - 5].KLVPNO);
				}else{
					w_knum1 = "";
				}
				if(BasedMethod.nvl(arrPRINT[nowDtail - 4].KLVPNO,"0") != "0"){
					w_knum2 = BasedMethod.nvl(arrPRINT[nowDtail - 4].KLVPNO);
				}else{
					w_knum2 = "";
				}
				if(BasedMethod.nvl(arrPRINT[nowDtail - 3].KLVPNO,"0") != "0"){
					w_knum3 = BasedMethod.nvl(arrPRINT[nowDtail - 3].KLVPNO);
				}else{
					w_knum3 = "";
				}
				if(BasedMethod.nvl(arrPRINT[nowDtail - 2].KLVPNO,"0") != "0"){
					w_knum4 = BasedMethod.nvl(arrPRINT[nowDtail - 2].KLVPNO);
				}else{
					w_knum4 = "";
				}
				if(BasedMethod.nvl(arrPRINT[nowDtail - 1].KLVPNO,"0") != "0"){
					w_knum5 = BasedMethod.nvl(arrPRINT[nowDtail - 1].KLVPNO);
				}else{
					w_knum5 = "";
				}

				pIndex = (currentCount + 1).toString();
				
				params["__format_id_number[" + pIndex + "]"] = "2";
				params["__format[" + pIndex + "].VLIN1"] = w_vlin1;
				params["__format[" + pIndex + "].VLIN2"] = w_vlin2;
				params["__format[" + pIndex + "].VLIN3"] = w_vlin3;
				params["__format[" + pIndex + "].VLIN4"] = w_vlin4;
				params["__format[" + pIndex + "].VLIN5"] = w_vlin5;
				params["__format[" + pIndex + "].CLS1"] = BasedMethod.nvl(arrPRINT[nowDtail - 5].MDCLSNMS);
				params["__format[" + pIndex + "].CLS2"] = BasedMethod.nvl(arrPRINT[nowDtail - 4].MDCLSNMS);
				params["__format[" + pIndex + "].CLS3"] = BasedMethod.nvl(arrPRINT[nowDtail - 3].MDCLSNMS);
				params["__format[" + pIndex + "].CLS4"] = BasedMethod.nvl(arrPRINT[nowDtail - 2].MDCLSNMS);
				params["__format[" + pIndex + "].CLS5"] = BasedMethod.nvl(arrPRINT[nowDtail - 1].MDCLSNMS);
				params["__format[" + pIndex + "].GDNM1"] = BasedMethod.nvl(arrPRINT[nowDtail - 5].GDNM);
				params["__format[" + pIndex + "].GDNM2"] = BasedMethod.nvl(arrPRINT[nowDtail - 4].GDNM);
				params["__format[" + pIndex + "].GDNM3"] = BasedMethod.nvl(arrPRINT[nowDtail - 3].GDNM);
				params["__format[" + pIndex + "].GDNM4"] = BasedMethod.nvl(arrPRINT[nowDtail - 2].GDNM);
				params["__format[" + pIndex + "].GDNM5"] = BasedMethod.nvl(arrPRINT[nowDtail - 1].GDNM);
				params["__format[" + pIndex + "].CST1"] = BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(arrPRINT[nowDtail - 5].TAXSAL, "1", "0", "0"));
				params["__format[" + pIndex + "].CST2"] = BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(arrPRINT[nowDtail - 4].TAXSAL, "1", "0", "0"));
				params["__format[" + pIndex + "].CST3"] = BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(arrPRINT[nowDtail - 3].TAXSAL, "1", "0", "0"));
				params["__format[" + pIndex + "].CST4"] = BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(arrPRINT[nowDtail - 2].TAXSAL, "1", "0", "0"));
				params["__format[" + pIndex + "].CST5"] = BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(arrPRINT[nowDtail - 1].TAXSAL, "1", "0", "0"));
				params["__format[" + pIndex + "].BSNUM1"] = BasedMethod.nvl(arrPRINT[nowDtail - 5].ZHVPNO);
				params["__format[" + pIndex + "].BSNUM2"] = BasedMethod.nvl(arrPRINT[nowDtail - 4].ZHVPNO);
				params["__format[" + pIndex + "].BSNUM3"] = BasedMethod.nvl(arrPRINT[nowDtail - 3].ZHVPNO);
				params["__format[" + pIndex + "].BSNUM4"] = BasedMethod.nvl(arrPRINT[nowDtail - 2].ZHVPNO);
				params["__format[" + pIndex + "].BSNUM5"] = BasedMethod.nvl(arrPRINT[nowDtail - 1].ZHVPNO);
				params["__format[" + pIndex + "].KZNUM1"] = BasedMethod.nvl(arrPRINT[nowDtail - 5].KZAN);
				params["__format[" + pIndex + "].KZNUM2"] = BasedMethod.nvl(arrPRINT[nowDtail - 4].KZAN);
				params["__format[" + pIndex + "].KZNUM3"] = BasedMethod.nvl(arrPRINT[nowDtail - 3].KZAN);
				params["__format[" + pIndex + "].KZNUM4"] = BasedMethod.nvl(arrPRINT[nowDtail - 2].KZAN);
				params["__format[" + pIndex + "].KZNUM5"] = BasedMethod.nvl(arrPRINT[nowDtail - 1].KZAN);
				params["__format[" + pIndex + "].KUNUM1"] = w_kunum1;
				params["__format[" + pIndex + "].KUNUM2"] = w_kunum2;
				params["__format[" + pIndex + "].KUNUM3"] = w_kunum3;
				params["__format[" + pIndex + "].KUNUM4"] = w_kunum4;
				params["__format[" + pIndex + "].KUNUM5"] = w_kunum5;
				params["__format[" + pIndex + "].UAMT1"] = w_uamt1;
				params["__format[" + pIndex + "].UAMT2"] = w_uamt2;
				params["__format[" + pIndex + "].UAMT3"] = w_uamt3;
				params["__format[" + pIndex + "].UAMT4"] = w_uamt4;
				params["__format[" + pIndex + "].UAMT5"] = w_uamt5;
				params["__format[" + pIndex + "].NNUM1"] = w_nnum1;
				params["__format[" + pIndex + "].NNUM2"] = w_nnum2;
				params["__format[" + pIndex + "].NNUM3"] = w_nnum3;
				params["__format[" + pIndex + "].NNUM4"] = w_nnum4;
				params["__format[" + pIndex + "].NNUM5"] = w_nnum5;
				params["__format[" + pIndex + "].HNUM1"] = w_hnum1;
				params["__format[" + pIndex + "].HNUM2"] = w_hnum2;
				params["__format[" + pIndex + "].HNUM3"] = w_hnum3;
				params["__format[" + pIndex + "].HNUM4"] = w_hnum4;
				params["__format[" + pIndex + "].HNUM5"] = w_hnum5;
				params["__format[" + pIndex + "].KNUM1"] = w_knum1;
				params["__format[" + pIndex + "].KNUM2"] = w_knum2;
				params["__format[" + pIndex + "].KNUM3"] = w_knum3;
				params["__format[" + pIndex + "].KNUM4"] = w_knum4;
				params["__format[" + pIndex + "].KNUM5"] = w_knum5;
				params["__format[" + pIndex + "].KHNUM1"] = BasedMethod.nvl(arrPRINT[nowDtail - 5].KHVPNO);
				params["__format[" + pIndex + "].KHNUM2"] = BasedMethod.nvl(arrPRINT[nowDtail - 4].KHVPNO);
				params["__format[" + pIndex + "].KHNUM3"] = BasedMethod.nvl(arrPRINT[nowDtail - 3].KHVPNO);
				params["__format[" + pIndex + "].KHNUM4"] = BasedMethod.nvl(arrPRINT[nowDtail - 2].KHVPNO);
				params["__format[" + pIndex + "].KHNUM5"] = BasedMethod.nvl(arrPRINT[nowDtail - 1].KHVPNO);
			}		
		
			ttl_bal = "前回繰越額";
			w_bal = singleton.g_customnumberformatter.numberFormat(BasedMethod.calcNum(BasedMethod.calcNum(arrMA042X2[0].MEDIBAL, arrMA042X2[0].STOREBAL), arrMA042X2[0].DEVBAL), "1", "0", "0");
			// 計算結果を退避
			w_bal1 = w_bal;
			if(BasedMethod.asCurrency(w_bal) == 0){
				ttl_bal = "";
				w_bal = "";
			}
			w_amt  =  singleton.g_customnumberformatter.numberFormat(BasedMethod.calcNum(BasedMethod.calcNum(arrMA042X2[0].MEDIAMT, arrMA042X2[0].STOREAMT), arrMA042X2[0].DEVAMT), "1", "0", "0");
			w_tax  =  singleton.g_customnumberformatter.numberFormat(BasedMethod.calcNum(BasedMethod.calcNum(arrMA042X2[0].MEDITAX, arrMA042X2[0].STORETAX), arrMA042X2[0].DEVTAX), "1", "0", "0");
			w_namt =  singleton.g_customnumberformatter.numberFormat(BasedMethod.calcNum(BasedMethod.calcNum(arrMA042X2[0].MEDIMONEY, arrMA042X2[0].STOREMONEY), arrMA042X2[0].DEVMONEY), "1", "0", "0");
			w_zamt =  singleton.g_customnumberformatter.numberFormat(BasedMethod.calcNum(BasedMethod.calcNum(arrMA042X2[0].MEDINBAL, arrMA042X2[0].STORENBAL), arrMA042X2[0].DEVNBAL), "1", "0", "0");
			w_uamt =  singleton.g_customnumberformatter.numberFormat(BasedMethod.calcNum(BasedMethod.calcNum(arrMA042X2[0].MEDIWITHTAX, arrMA042X2[0].STOREWITHTAX), arrMA042X2[0].DEVWITHTAX), "1", "0", "0");
			w_dis  =  singleton.g_customnumberformatter.numberFormat(BasedMethod.calcNum(w_uamt, w_amt, 1), "1", "0", "0");
			// 請求額に前回繰越加算
			w_amt  =  singleton.g_customnumberformatter.numberFormat(BasedMethod.calcNum(w_bal1,w_amt), "1", "0", "0");
			
			pIndex = (currentCount + 1).toString();
			
			params["__format_id_number[" + pIndex + "]"] = "3";
			params["__format[" + pIndex + "].UAMT"] = w_uamt;
			params["__format[" + pIndex + "].TTL_BAL"] = ttl_bal;
			params["__format[" + pIndex + "].BAL"] = w_bal;
			params["__format[" + pIndex + "].DIS"] = w_dis;
			params["__format[" + pIndex + "].AMT"] = w_amt;
			params["__format[" + pIndex + "].TAX"] = w_tax;
			params["__format[" + pIndex + "].NAMT"] = w_namt;
			params["__format[" + pIndex + "].ZAMT"] = w_zamt;
			params["__format[" + pIndex + "].TNO"] = getTNO();
			
			hts.send(params);
***/
// 15.01.19 SE-233 end			
// 15.03.19 SE-233 end			
		}

		private function printMA042X2_body(e: ResultEvent=null): void
		{
			// 印刷成功時処理
			if(e != null){
				if(e.result.response.result == "OK"){
					// 印刷失敗時 result == NG
				}else{
					singleton.sitemethod.dspJoinErrMsg(e.result.response.message,
						doF10Proc_last, "", "0", null, null, true);
					return;
				}
				//IEventDispatcher(e.currentTarget).removeEventListener(ResultEvent.RESULT, printMA042X2_body);				
				//IEventDispatcher(e.currentTarget).removeEventListener(FaultEvent.FAULT, htsFault);				
			}
			
			var hts: HTTPService = new HTTPService();
			var nowDtail: int;
			
			hts.method = "GET";
			hts.addEventListener(ResultEvent.RESULT, printLoop2, false, 0, true);
			hts.addEventListener(FaultEvent.FAULT, htsFault, false, 0, true);
			hts.url = "http://localhost:8080/Format/Print";
			
			nowDtail = (currentCount + 1) * 5;

			// 12.10.03 SE-362 明細がない場合は行NOを印字しない start
			var w_vlin1: String;
			var w_vlin2: String;
			var w_vlin3: String;
			var w_vlin4: String;
			var w_vlin5: String;
			
			if(BasedMethod.nvl(arrPRINT[nowDtail - 5].VJAN) != ""){
				w_vlin1 = (nowDtail - 4).toString();
			}else{
				w_vlin1 = "";
			}
			if(BasedMethod.nvl(arrPRINT[nowDtail - 4].VJAN) != ""){
				w_vlin2 = (nowDtail - 3).toString();
			}else{
				w_vlin2 = "";
			}
			if(BasedMethod.nvl(arrPRINT[nowDtail - 3].VJAN) != ""){
				w_vlin3 = (nowDtail - 2).toString();
			}else{
				w_vlin3 = "";
			}
			if(BasedMethod.nvl(arrPRINT[nowDtail - 2].VJAN) != ""){
				w_vlin4 = (nowDtail - 1).toString();
			}else{
				w_vlin4 = "";
			}
			if(BasedMethod.nvl(arrPRINT[nowDtail - 1].VJAN) != ""){
				w_vlin5 = (nowDtail).toString();
			}else{
				w_vlin5 = "";
			}
			// 12.10.03 SE-362 明細がない場合は行NOを印字しない end			

			// 12.10.19 SE-233 ゼロを非表示にする start
			var w_kunum1: String;
			var w_kunum2: String;
			var w_kunum3: String;
			var w_kunum4: String;
			var w_kunum5: String;
			var w_uamt1: String;
			var w_uamt2: String;
			var w_uamt3: String;
			var w_uamt4: String;
			var w_uamt5: String;
			var w_nnum1: String;
			var w_nnum2: String;
			var w_nnum3: String;
			var w_nnum4: String;
			var w_nnum5: String;
			var w_hnum1: String;
			var w_hnum2: String;
			var w_hnum3: String;
			var w_hnum4: String;
			var w_hnum5: String;
			var w_knum1: String;
			var w_knum2: String;
			var w_knum3: String;
			var w_knum4: String;
			var w_knum5: String;

			if(BasedMethod.nvl(arrPRINT[nowDtail - 5].KUVPNO,"0") != "0"){
				w_kunum1 = BasedMethod.nvl(arrPRINT[nowDtail - 5].KUVPNO);
			}else{
				w_kunum1 = "";
			}
			if(BasedMethod.nvl(arrPRINT[nowDtail - 4].KUVPNO,"0") != "0"){
				w_kunum2 = BasedMethod.nvl(arrPRINT[nowDtail - 4].KUVPNO);
			}else{
				w_kunum2 = "";
			}
			if(BasedMethod.nvl(arrPRINT[nowDtail - 3].KUVPNO,"0") != "0"){
				w_kunum3 = BasedMethod.nvl(arrPRINT[nowDtail - 3].KUVPNO);
			}else{
				w_kunum3 = "";
			}
			if(BasedMethod.nvl(arrPRINT[nowDtail - 2].KUVPNO,"0") != "0"){
				w_kunum4 = BasedMethod.nvl(arrPRINT[nowDtail - 2].KUVPNO);
			}else{
				w_kunum4 = "";
			}
			if(BasedMethod.nvl(arrPRINT[nowDtail - 1].KUVPNO,"0") != "0"){
				w_kunum5 = BasedMethod.nvl(arrPRINT[nowDtail - 1].KUVPNO);
			}else{
				w_kunum5 = "";
			}
			if(BasedMethod.nvl(BasedMethod.calcNum(arrPRINT[nowDtail - 5].KUVPNO, arrPRINT[nowDtail - 5].TAXSAL, 2),"0") != "0"){
				w_uamt1 = BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(BasedMethod.calcNum(arrPRINT[nowDtail - 5].KUVPNO, arrPRINT[nowDtail - 5].TAXSAL, 2), "1", "0", "0"));
			}else{
				w_uamt1 = "";
			}
			if(BasedMethod.nvl(BasedMethod.calcNum(arrPRINT[nowDtail - 4].KUVPNO, arrPRINT[nowDtail - 4].TAXSAL, 2),"0") != "0"){
				w_uamt2 = BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(BasedMethod.calcNum(arrPRINT[nowDtail - 4].KUVPNO, arrPRINT[nowDtail - 4].TAXSAL, 2), "1", "0", "0"));
			}else{
				w_uamt2 = "";
			}
			if(BasedMethod.nvl(BasedMethod.calcNum(arrPRINT[nowDtail - 3].KUVPNO, arrPRINT[nowDtail - 3].TAXSAL, 2),"0") != "0"){
				w_uamt3 = BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(BasedMethod.calcNum(arrPRINT[nowDtail - 3].KUVPNO, arrPRINT[nowDtail - 3].TAXSAL, 2), "1", "0", "0"));
			}else{
				w_uamt3 = "";
			}
			if(BasedMethod.nvl(BasedMethod.calcNum(arrPRINT[nowDtail - 2].KUVPNO, arrPRINT[nowDtail - 2].TAXSAL, 2),"0") != "0"){
				w_uamt4 = BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(BasedMethod.calcNum(arrPRINT[nowDtail - 2].KUVPNO, arrPRINT[nowDtail - 2].TAXSAL, 2), "1", "0", "0"));
			}else{
				w_uamt4 = "";
			}
			if(BasedMethod.nvl(BasedMethod.calcNum(arrPRINT[nowDtail - 1].KUVPNO, arrPRINT[nowDtail - 1].TAXSAL, 2),"0") != "0"){
				w_uamt5 = BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(BasedMethod.calcNum(arrPRINT[nowDtail - 1].KUVPNO, arrPRINT[nowDtail - 1].TAXSAL, 2), "1", "0", "0"));
			}else{
				w_uamt5 = "";
			}
			if(BasedMethod.nvl(arrPRINT[nowDtail - 5].KNVPNO,"0") != "0"){
				w_nnum1 = BasedMethod.nvl(arrPRINT[nowDtail - 5].KNVPNO);
			}else{
				w_nnum1 = "";
			}
			if(BasedMethod.nvl(arrPRINT[nowDtail - 4].KNVPNO,"0") != "0"){
				w_nnum2 = BasedMethod.nvl(arrPRINT[nowDtail - 4].KNVPNO);
			}else{
				w_nnum2 = "";
			}
			if(BasedMethod.nvl(arrPRINT[nowDtail - 3].KNVPNO,"0") != "0"){
				w_nnum3 = BasedMethod.nvl(arrPRINT[nowDtail - 3].KNVPNO);
			}else{
				w_nnum3 = "";
			}
			if(BasedMethod.nvl(arrPRINT[nowDtail - 2].KNVPNO,"0") != "0"){
				w_nnum4 = BasedMethod.nvl(arrPRINT[nowDtail - 2].KNVPNO);
			}else{
				w_nnum4 = "";
			}
			if(BasedMethod.nvl(arrPRINT[nowDtail - 1].KNVPNO,"0") != "0"){
				w_nnum5 = BasedMethod.nvl(arrPRINT[nowDtail - 1].KNVPNO);
			}else{
				w_nnum5 = "";
			}
			if(BasedMethod.nvl(arrPRINT[nowDtail - 5].KRVPNO,"0") != "0"){
				w_hnum1 = BasedMethod.nvl(arrPRINT[nowDtail - 5].KRVPNO);
			}else{
				w_hnum1 = "";
			}
			if(BasedMethod.nvl(arrPRINT[nowDtail - 4].KRVPNO,"0") != "0"){
				w_hnum2 = BasedMethod.nvl(arrPRINT[nowDtail - 4].KRVPNO);
			}else{
				w_hnum2 = "";
			}
			if(BasedMethod.nvl(arrPRINT[nowDtail - 3].KRVPNO,"0") != "0"){
				w_hnum3 = BasedMethod.nvl(arrPRINT[nowDtail - 3].KRVPNO);
			}else{
				w_hnum3 = "";
			}
			if(BasedMethod.nvl(arrPRINT[nowDtail - 2].KRVPNO,"0") != "0"){
				w_hnum4 = BasedMethod.nvl(arrPRINT[nowDtail - 2].KRVPNO);
			}else{
				w_hnum4 = "";
			}
			if(BasedMethod.nvl(arrPRINT[nowDtail - 1].KRVPNO,"0") != "0"){
				w_hnum5 = BasedMethod.nvl(arrPRINT[nowDtail - 1].KRVPNO);
			}else{
				w_hnum5 = "";
			}
			if(BasedMethod.nvl(arrPRINT[nowDtail - 5].KLVPNO,"0") != "0"){
				w_knum1 = BasedMethod.nvl(arrPRINT[nowDtail - 5].KLVPNO);
			}else{
				w_knum1 = "";
			}
			if(BasedMethod.nvl(arrPRINT[nowDtail - 4].KLVPNO,"0") != "0"){
				w_knum2 = BasedMethod.nvl(arrPRINT[nowDtail - 4].KLVPNO);
			}else{
				w_knum2 = "";
			}
			if(BasedMethod.nvl(arrPRINT[nowDtail - 3].KLVPNO,"0") != "0"){
				w_knum3 = BasedMethod.nvl(arrPRINT[nowDtail - 3].KLVPNO);
			}else{
				w_knum3 = "";
			}
			if(BasedMethod.nvl(arrPRINT[nowDtail - 2].KLVPNO,"0") != "0"){
				w_knum4 = BasedMethod.nvl(arrPRINT[nowDtail - 2].KLVPNO);
			}else{
				w_knum4 = "";
			}
			if(BasedMethod.nvl(arrPRINT[nowDtail - 1].KLVPNO,"0") != "0"){
				w_knum5 = BasedMethod.nvl(arrPRINT[nowDtail - 1].KLVPNO);
			}else{
				w_knum5 = "";
			}
			// 12.10.19 SE-233 ゼロを非表示にする end
			
			hts.send({
				"__format_archive_url": "file:///sdcard/print/me045x.spfmtz",
				"__format_archive_update": "updateWithoutError",
				"__format_id_number": "2",
				"VLIN1": w_vlin1,
				"VLIN2": w_vlin2,
				"VLIN3": w_vlin3,
				"VLIN4": w_vlin4,
				"VLIN5": w_vlin5,
				"CLS1"		: BasedMethod.nvl(arrPRINT[nowDtail - 5].MDCLSNMS),
				"CLS2"		: BasedMethod.nvl(arrPRINT[nowDtail - 4].MDCLSNMS),
				"CLS3" 		: BasedMethod.nvl(arrPRINT[nowDtail - 3].MDCLSNMS),
				"CLS4" 		: BasedMethod.nvl(arrPRINT[nowDtail - 2].MDCLSNMS),
				"CLS5" 		: BasedMethod.nvl(arrPRINT[nowDtail - 1].MDCLSNMS),
				"GDNM1"	: BasedMethod.nvl(arrPRINT[nowDtail - 5].GDNM),
				"GDNM2"	: BasedMethod.nvl(arrPRINT[nowDtail - 4].GDNM),
				"GDNM3"	: BasedMethod.nvl(arrPRINT[nowDtail - 3].GDNM),
				"GDNM4"	: BasedMethod.nvl(arrPRINT[nowDtail - 2].GDNM),
				"GDNM5"	: BasedMethod.nvl(arrPRINT[nowDtail - 1].GDNM),
				"CST1" 		: BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(arrPRINT[nowDtail - 5].TAXSAL, "1", "0", "0")),
				"CST2" 		: BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(arrPRINT[nowDtail - 4].TAXSAL, "1", "0", "0")),
				"CST3" 		: BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(arrPRINT[nowDtail - 3].TAXSAL, "1", "0", "0")),
				"CST4" 		: BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(arrPRINT[nowDtail - 2].TAXSAL, "1", "0", "0")),
				"CST5" 		: BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(arrPRINT[nowDtail - 1].TAXSAL, "1", "0", "0")),
				"BSNUM1"	:BasedMethod.nvl(arrPRINT[nowDtail - 5].ZHVPNO),
				"BSNUM2"	:BasedMethod.nvl(arrPRINT[nowDtail - 4].ZHVPNO),
				"BSNUM3"	:BasedMethod.nvl(arrPRINT[nowDtail - 3].ZHVPNO),
				"BSNUM4"	:BasedMethod.nvl(arrPRINT[nowDtail - 2].ZHVPNO),
				"BSNUM5"	:BasedMethod.nvl(arrPRINT[nowDtail - 1].ZHVPNO),
				"KZNUM1"	:BasedMethod.nvl(arrPRINT[nowDtail - 5].KZAN),
				"KZNUM2"	:BasedMethod.nvl(arrPRINT[nowDtail - 4].KZAN),
				"KZNUM3"	:BasedMethod.nvl(arrPRINT[nowDtail - 3].KZAN),
				"KZNUM4"	:BasedMethod.nvl(arrPRINT[nowDtail - 2].KZAN),
				"KZNUM5"	:BasedMethod.nvl(arrPRINT[nowDtail - 1].KZAN),
				// 12.10.19 SE-233 ゼロを非表示にする start
				"KUNUM1"	:w_kunum1,
				"KUNUM2"	:w_kunum2,
				"KUNUM3"	:w_kunum3,
				"KUNUM4"	:w_kunum4,
				"KUNUM5"	:w_kunum5,
				"UAMT1"	:w_uamt1,
				"UAMT2"	:w_uamt2,
				"UAMT3"	:w_uamt3,
				"UAMT4"	:w_uamt4,
				"UAMT5"	:w_uamt5,
				"NNUM1"	:w_nnum1,
				"NNUM2"	:w_nnum2,
				"NNUM3"	:w_nnum3,
				"NNUM4"	:w_nnum4,
				"NNUM5"	:w_nnum5,
				"HNUM1"	:w_hnum1,
				"HNUM2"	:w_hnum2,
				"HNUM3"	:w_hnum3,
				"HNUM4"	:w_hnum4,
				"HNUM5"	:w_hnum5,
				"KNUM1"	:w_knum1,
				"KNUM2"	:w_knum2,
				"KNUM3"	:w_knum3,
				"KNUM4"	:w_knum4,
				"KNUM5"	:w_knum5,
//				"KUNUM1"	:BasedMethod.nvl(arrPRINT[nowDtail - 5].KUVPNO),
//				"KUNUM2"	:BasedMethod.nvl(arrPRINT[nowDtail - 4].KUVPNO),
//				"KUNUM3"	:BasedMethod.nvl(arrPRINT[nowDtail - 3].KUVPNO),
//				"KUNUM4"	:BasedMethod.nvl(arrPRINT[nowDtail - 2].KUVPNO),
//				"KUNUM5"	:BasedMethod.nvl(arrPRINT[nowDtail - 1].KUVPNO),
////				"UAMT1"	:singleton.g_customnumberformatter.numberFormat(BasedMethod.calcNum(BasedMethod.nvl(arrPRINT[nowDtail - 5].KUVPNO, "0"), BasedMethod.nvl(arrPRINT[nowDtail - 5].SAL, "0"), 2), "1", "0", "0"),
////				"UAMT2"	:singleton.g_customnumberformatter.numberFormat(BasedMethod.calcNum(BasedMethod.nvl(arrPRINT[nowDtail - 4].KUVPNO, "0"), BasedMethod.nvl(arrPRINT[nowDtail - 4].SAL, "0"), 2), "1", "0", "0"),
////				"UAMT3"	:singleton.g_customnumberformatter.numberFormat(BasedMethod.calcNum(BasedMethod.nvl(arrPRINT[nowDtail - 3].KUVPNO, "0"), BasedMethod.nvl(arrPRINT[nowDtail - 3].SAL, "0"), 2), "1", "0", "0"),
////				"UAMT4"	:singleton.g_customnumberformatter.numberFormat(BasedMethod.calcNum(BasedMethod.nvl(arrPRINT[nowDtail - 2].KUVPNO, "0"), BasedMethod.nvl(arrPRINT[nowDtail - 2].SAL, "0"), 2), "1", "0", "0"),
////				"UAMT5"	:singleton.g_customnumberformatter.numberFormat(BasedMethod.calcNum(BasedMethod.nvl(arrPRINT[nowDtail - 1].KUVPNO, "0"), BasedMethod.nvl(arrPRINT[nowDtail - 1].SAL, "0"), 2), "1", "0", "0"),
//				"UAMT1"	:BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(BasedMethod.calcNum(arrPRINT[nowDtail - 5].KUVPNO, arrPRINT[nowDtail - 5].TAXSAL, 2), "1", "0", "0")),
//				"UAMT2"	:BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(BasedMethod.calcNum(arrPRINT[nowDtail - 4].KUVPNO, arrPRINT[nowDtail - 4].TAXSAL, 2), "1", "0", "0")),
//				"UAMT3"	:BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(BasedMethod.calcNum(arrPRINT[nowDtail - 3].KUVPNO, arrPRINT[nowDtail - 3].TAXSAL, 2), "1", "0", "0")),
//				"UAMT4"	:BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(BasedMethod.calcNum(arrPRINT[nowDtail - 2].KUVPNO, arrPRINT[nowDtail - 2].TAXSAL, 2), "1", "0", "0")),
//				"UAMT5"	:BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(BasedMethod.calcNum(arrPRINT[nowDtail - 1].KUVPNO, arrPRINT[nowDtail - 1].TAXSAL, 2), "1", "0", "0")),
//				"NNUM1"	:BasedMethod.nvl(arrPRINT[nowDtail - 5].KNVPNO),
//				"NNUM2"	:BasedMethod.nvl(arrPRINT[nowDtail - 4].KNVPNO),
//				"NNUM3"	:BasedMethod.nvl(arrPRINT[nowDtail - 3].KNVPNO),
//				"NNUM4"	:BasedMethod.nvl(arrPRINT[nowDtail - 2].KNVPNO),
//				"NNUM5"	:BasedMethod.nvl(arrPRINT[nowDtail - 1].KNVPNO),
//				"HNUM1"	:BasedMethod.nvl(arrPRINT[nowDtail - 5].KRVPNO),
//				"HNUM2"	:BasedMethod.nvl(arrPRINT[nowDtail - 4].KRVPNO),
//				"HNUM3"	:BasedMethod.nvl(arrPRINT[nowDtail - 3].KRVPNO),
//				"HNUM4"	:BasedMethod.nvl(arrPRINT[nowDtail - 2].KRVPNO),
//				"HNUM5"	:BasedMethod.nvl(arrPRINT[nowDtail - 1].KRVPNO),
//				"KNUM1"	:BasedMethod.nvl(arrPRINT[nowDtail - 5].KLVPNO),
//				"KNUM2"	:BasedMethod.nvl(arrPRINT[nowDtail - 4].KLVPNO),
//				"KNUM3"	:BasedMethod.nvl(arrPRINT[nowDtail - 3].KLVPNO),
//				"KNUM4"	:BasedMethod.nvl(arrPRINT[nowDtail - 2].KLVPNO),
//				"KNUM5"	:BasedMethod.nvl(arrPRINT[nowDtail - 1].KLVPNO),
				// 12.10.19 SE-233 ゼロを非表示にする end
				"KHNUM1"	:BasedMethod.nvl(arrPRINT[nowDtail - 5].KHVPNO),
				"KHNUM2"	:BasedMethod.nvl(arrPRINT[nowDtail - 4].KHVPNO),
				"KHNUM3"	:BasedMethod.nvl(arrPRINT[nowDtail - 3].KHVPNO),
				"KHNUM4"	:BasedMethod.nvl(arrPRINT[nowDtail - 2].KHVPNO),
				"KHNUM5"	:BasedMethod.nvl(arrPRINT[nowDtail - 1].KHVPNO),
				"(発行枚数)": "1"
			});
		}

		private function printLoop2(e: ResultEvent=null): void
		{
			// カウントアップ
			currentCount++;
			
			if(e != null){
				// 印刷成功時処理
				if(e.result.response.result == "OK"){
					// 印刷失敗時 result == NG
				}else{
					singleton.sitemethod.dspJoinErrMsg(e.result.response.message,
						doF10Proc_last, "", "0", null, null, true);
					return;
				}
				//IEventDispatcher(e.currentTarget).removeEventListener(ResultEvent.RESULT, printLoop2);				
				//IEventDispatcher(e.currentTarget).removeEventListener(FaultEvent.FAULT, htsFault);				
			}

			if(currentCount != reportCount)
				printMA042X2_body();
			else
				printMA042X2_futter();
		}

		private function printMA042X2_futter(): void
		{
			var hts: HTTPService = new HTTPService();
			var ttl_bal: String;
			var w_bal: String;
			var w_bal1: String;
			var w_dis: String;
			var w_amt: String;
			var w_tax: String;
			var w_namt: String;
			var w_zamt: String;
			var w_uamt: String;
			
			hts.method = "GET";
			hts.addEventListener(ResultEvent.RESULT, printMA042X2_last, false, 0, true);
			hts.addEventListener(FaultEvent.FAULT, htsFault, false, 0, true);
			hts.url = "http://localhost:8080/Format/Print";
			
			ttl_bal = "前回繰越額";
			w_bal = singleton.g_customnumberformatter.numberFormat(BasedMethod.calcNum(BasedMethod.calcNum(arrMA042X2[0].MEDIBAL, arrMA042X2[0].STOREBAL), arrMA042X2[0].DEVBAL), "1", "0", "0");
			// 計算結果を退避
			w_bal1 = w_bal;
			if(BasedMethod.asCurrency(w_bal) == 0){
				ttl_bal = "";
				w_bal = "";
			}
//			w_dis  =  BasedMethod.calcNum(BasedMethod.calcNum(arrMA042X2[0].MEDIDIS, arrMA042X2[0].STOREDIS), arrMA042X2[0].DEVDIS);
//			w_amt  =  BasedMethod.calcNum(BasedMethod.calcNum(arrMA042X2[0].MEDIAMT, arrMA042X2[0].STOREAMT), arrMA042X2[0].DEVAMT);
//			w_tax  =  BasedMethod.calcNum(BasedMethod.calcNum(arrMA042X2[0].MEDITAX, arrMA042X2[0].STORETAX), arrMA042X2[0].DEVTAX);
//			w_namt =  BasedMethod.calcNum(BasedMethod.calcNum(arrMA042X2[0].MEDIMONEY, arrMA042X2[0].STOREMONEY), arrMA042X2[0].DEVMONEY);
//			w_zamt =  BasedMethod.calcNum(BasedMethod.calcNum(arrMA042X2[0].MEDINBAL, arrMA042X2[0].STORENBAL), arrMA042X2[0].DEVNBAL);
//			w_uamt =  BasedMethod.calcNum(BasedMethod.calcNum(arrMA042X2[0].MEDIWITHOUTTAX, arrMA042X2[0].STOREWITHOUTTAX), arrMA042X2[0].DEVWITHOUTTAX);
			w_amt  =  singleton.g_customnumberformatter.numberFormat(BasedMethod.calcNum(BasedMethod.calcNum(arrMA042X2[0].MEDIAMT, arrMA042X2[0].STOREAMT), arrMA042X2[0].DEVAMT), "1", "0", "0");
			w_tax  =  singleton.g_customnumberformatter.numberFormat(BasedMethod.calcNum(BasedMethod.calcNum(arrMA042X2[0].MEDITAX, arrMA042X2[0].STORETAX), arrMA042X2[0].DEVTAX), "1", "0", "0");
			w_namt =  singleton.g_customnumberformatter.numberFormat(BasedMethod.calcNum(BasedMethod.calcNum(arrMA042X2[0].MEDIMONEY, arrMA042X2[0].STOREMONEY), arrMA042X2[0].DEVMONEY), "1", "0", "0");
			w_zamt =  singleton.g_customnumberformatter.numberFormat(BasedMethod.calcNum(BasedMethod.calcNum(arrMA042X2[0].MEDINBAL, arrMA042X2[0].STORENBAL), arrMA042X2[0].DEVNBAL), "1", "0", "0");
//			w_uamt =  singleton.g_customnumberformatter.numberFormat(BasedMethod.calcNum(BasedMethod.calcNum(arrMA042X2[0].MEDIWITHOUTTAX, arrMA042X2[0].STOREWITHOUTTAX), arrMA042X2[0].DEVWITHOUTTAX), "1", "0", "0");
			w_uamt =  singleton.g_customnumberformatter.numberFormat(BasedMethod.calcNum(BasedMethod.calcNum(arrMA042X2[0].MEDIWITHTAX, arrMA042X2[0].STOREWITHTAX), arrMA042X2[0].DEVWITHTAX), "1", "0", "0");
			
			w_dis  =  singleton.g_customnumberformatter.numberFormat(BasedMethod.calcNum(w_uamt, w_amt, 1), "1", "0", "0");
			
			// 請求額に前回繰越加算
			w_amt  =  singleton.g_customnumberformatter.numberFormat(BasedMethod.calcNum(w_bal1,w_amt), "1", "0", "0");

			hts.send({
				"__format_archive_url": "file:///sdcard/print/me045x.spfmtz",
				"__format_archive_update": "updateWithoutError",
				"__format_id_number": "3",
				"UAMT": w_uamt,
				"TTL_BAL": ttl_bal,
				"BAL": w_bal,
				"DIS": w_dis,
				"AMT": w_amt,
				"TAX": w_tax,
				"NAMT": w_namt,
				"ZAMT": w_zamt,
				//"TNO": getTNO(arrSEIGYO[0].TNO),
				"TNO": getTNO(),
				"(発行枚数)": "1"
			});
		}

		private function printMA042X2_last(e: ResultEvent=null): void
		{
			if(e != null){
				// 印刷成功時処理
				if(e.result.response.result == "OK"){
					// 印刷失敗時 result == NG
				}else{
					singleton.sitemethod.dspJoinErrMsg(e.result.response.message,
						doF10Proc_last, "", "0", null, null, true);
					return;
				}
				//IEventDispatcher(e.currentTarget).removeEventListener(ResultEvent.RESULT, printMA042X2_last);				
				//IEventDispatcher(e.currentTarget).removeEventListener(FaultEvent.FAULT, htsFault);				
			}
			
			BasedMethod.execNextFunction(print_arg.func);
		}

		/**********************************************************
		 * btnVJAN クリック時処理.
		 * @author 12.07.03 SE-354
		 **********************************************************/
		public function btnVJANClick(): void
		{
			// 商品ポップアップ呼び出し
			pop = new POPUP002();
			
			// 訪問日
			pop.ctrl.argHDAT = view.data.HDAT;

			// 入力画面立ち上げ					
			PopUpManager.addPopUp(pop, view, true);
			PopUpManager.centerPopUp(pop);
			pop.addEventListener(FlexEvent.REMOVE, btnVJANClick_last, false, 0, true);
		}
		
		private function btnVJANClick_last(e: FlexEvent): void
		{
			var index: int = arrMA042X1.length;
			pop.removeEventListener(FlexEvent.REMOVE, btnVJANClick_last);
			//IEventDispatcher(e.currentTarget).removeEventListener(FlexEvent.REMOVE, btnVJANClick_last);				

			if(pop.ctrl.LM006Record){

				// 商品コード重複チェック
				for(var i:int=0; i < arrMA042X1.length; i++){
					if(arrMA042X1[i].VJAN == pop.ctrl.LM006Record.VJAN){
						singleton.sitemethod.dspJoinErrMsg("E0181", null, "", "0", ["商品コード"]);
						return;
					}
				}
				
				arrMA042X1.addItem(new W_MA041X());
	
				arrMA042X1[index].TKCD   = g_cucd;
				arrMA042X1[index].VNO    = g_vseq;
				arrMA042X1[index].VSEQ   = arrMA042X1.length.toString();
				arrMA042X1[index].BCLS   = null;
				arrMA042X1[index].SCLS   = null;
				arrMA042X1[index].MDCLSNMS  = pop.ctrl.LM006Record.MDCLSNMS;
				arrMA042X1[index].VJAN   = pop.ctrl.LM006Record.VJAN;
				arrMA042X1[index].GDNM   = pop.ctrl.LM006Record.GDNM;
				arrMA042X1[index].SAL    = pop.ctrl.LM006Record.STCST;
				arrMA042X1[index].AMT    = "0";
				arrMA042X1[index].ZHVPNO = "0";
				arrMA042X1[index].KZAN   = "0";
				arrMA042X1[index].KUVPNO = "0";
				arrMA042X1[index].KNVPNO = "0";
				arrMA042X1[index].KRVPNO = "0";
				arrMA042X1[index].KLVPNO = "0";
				arrMA042X1[index].KHVPNO = "0";
				arrMA042X1[index].GDCOM  = pop.ctrl.LM006Record.GDCOM;
				arrMA042X1[index].TAXSAL = Math.floor(Number(BasedMethod.calcNum( 
						BasedMethod.calcNum(pop.ctrl.LM006Record.STCST
						, BasedMethod.calcNum(BasedMethod.nvl(String(getTax_ret.tax), "0"), "100")
						, 2)
						, "100", 3, 4))).toString();
				arrMA042X1[index].TAXAMT = "0";
				arrMA042X1[index].BHISK = "0";
				
				arrMA042X1.refresh();
				
				view.grp1.dataProvider = arrMA042X1;
				
				view.grp1.validateNow();
//				view.grp1.verticalScrollPosition = view.grp1.maxHeight;
//				view.grp1.verticalScrollPosition = 100;
				var maxPosition: Number;
				
				maxPosition = arrMA042X1.length * 41 - view.grp1.height;
				
				if(maxPosition < 0)
					maxPosition = 0;
				
				view.grp1.verticalScrollPosition = maxPosition; 
				
			}
			
			pop=null;

		}

		/**********************************************************
		 * btnATTENT クリック時処理.
		 * @author 12.07.03 SE-354
		 **********************************************************/
		public function btnATTENTClick(): void
		{
			var popup: POPUP006 = new POPUP006();
			
			if(arrMA042X1 != null && arrMA042X1.length != 0){
				popup.ctrl.argVJAN = arrMA042X1[g_selectedIndex].VJAN;
				PopUpManager.addPopUp(popup, view, true);
				PopUpManager.centerPopUp(popup);
			}
			
		}

		/**********************************************************
		 * btnMOTO クリック時処理.
		 * @author 12.07.03 SE-354
		 **********************************************************/
		public function btnMOTOClick(): void
		{
			view.stage.removeEventListener(KeyboardEvent.KEY_DOWN, onKeyDownHandler);
			
			view.navigator.pushView(MF010X, view.data);
		}
		
		/**********************************************************
		 * btnCAMERA クリック時処理.
		 * @author 12.07.03 SE-354
		 **********************************************************/
		public function btnCAMERAClick(): void
		{
			var camera: Camera = new Camera();
//			var day: Date = new Date();
//			var timestamp: String;
			
//			timestamp = day.getFullYear + "/" + day.getMonth + "/" + day.getDate + day.getHours + day.getMinutes + 	day.getSeconds;
			
			camera.startCamera(SiteConst.G_IMAGE_PATH, g_cucd);
		}
		
		// 12.10.12 SE-362 Redmine#3435 対応 start
		protected function onKeyDownHandler(e:KeyboardEvent):void
		{
			//e.preventDefault();			
			if(e.keyCode == Keyboard.BACK)
			{
				e.preventDefault();
				btnRTNClick();
			}
		}
		// 12.10.12 SE-362 Redmine#3435 対応 end

		/**********************************************************
		 * btnRTN クリック時処理.
		 * @author 12.07.03 SE-354
		 **********************************************************/
		public function btnRTNClick(): void
		{
			// 12.10.12 SE-362 Redmine#3435 対応 start
			// 確認画面の表示
			singleton.sitemethod.dspJoinErrMsg("Q0027", btnRTNClick_last, "", "1", null, btnRTNClick_last2);
			// 12.10.12 SE-362 Redmine#3435 対応 end
		}

		// 12.10.12 SE-362 Redmine#3435 対応 start
		private function btnRTNClick_last(): void
		{
			view.stage.removeEventListener(KeyboardEvent.KEY_DOWN, onKeyDownHandler);
			
			// 前画面に遷移
			view.navigator.popView();
		}
		
		private function btnRTNClick_last2(): void
		{
			// 無処理
		}
		// 12.10.12 SE-362 Redmine#3435 対応 end

		/**********************************************************
		 * btnRIREKI1 クリック時処理.
		 * @author 12.07.03 SE-354
		 **********************************************************/
		public function btnRIREKI1Click(): void
		{
			// 配置薬履歴ポップアップ呼び出し
			var pop: MP042X = new MP042X();
			
			pop.ctrl.argTKCD = g_cucd;
				
			// 入力画面立ち上げ					
			PopUpManager.addPopUp(pop, view, true);
			PopUpManager.centerPopUp(pop);
		}
		
		/**********************************************************
		 * btnRIREKI2 クリック時処理.
		 * @author 12.07.03 SE-354
		 **********************************************************/
		public function btnRIREKI2Click(): void
		{
			// 商品別売上履歴ポップアップ呼び出し
			pop = new POPUP002();
			
			// 訪問日
			pop.ctrl.argHDAT = view.data.HDAT;
			
			// 入力画面立ち上げ					
			PopUpManager.addPopUp(pop, view, true);
			PopUpManager.centerPopUp(pop);
		}
		

		public function YLabel(cat:Object,pcat:Object,ax:LinearAxis): String
		{
			var _customnumberformatter: CustomNumberFormatter = new CustomNumberFormatter();
			var n:Number = Number(cat);
			var n_comma: String;

			n_comma = _customnumberformatter.numberFormat(n.toString(), "1", "0", "0");
			
			return  n_comma;
		}
	}
}