package views
{
	import flash.data.SQLResult;
	import flash.data.SQLStatement;
	import flash.events.Event;
	import flash.events.GeolocationEvent;
	import flash.events.KeyboardEvent;
	import flash.events.LocationChangeEvent;
	import flash.events.SQLErrorEvent;
	import flash.events.SQLEvent;
	import flash.geom.Rectangle;
	import flash.media.StageWebView;
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	import flash.sensors.Geolocation;
	import flash.ui.Keyboard;
	
	import modules.custom.CustomIterator;
	import modules.custom.sub.Iterator;
	import modules.dto.LM009;
	import modules.sbase.SiteModule;
	
	import mx.collections.ArrayCollection;
	import mx.managers.PopUpManager;
	
	public class POPUP001Controller extends SiteModule
	{
		private var view:POPUP001;
		private var arrLM009: ArrayCollection;
		private var stmt: SQLStatement;
		
		public function POPUP001Controller()
		{
			super();
		}
		
		override public function init():void
		{
			super.init();
			
			// フォーム宣言
			view = _view as POPUP001;

			// 変数の設定
			//g_prgid     = "MENUX";
		}
		
		override public function formShow_last(): void
		{
			cdsLM009();
		}
		
		/**********************************************************
		 * SQLiteのselect
		 * @author 12.05.10 SE-354
		 **********************************************************/ 
		public function cdsLM009(): void
		{
			// 変数の初期化
			arrLM009 = null;
			
			stmt = new SQLStatement();
			//stmt.sqlConnection =  connection;
			stmt.sqlConnection =  Connection;
			stmt.itemClass = LM009;
			//stmt.text= "SELECT * FROM とくいさき WHERE HDAT=? AND AREA=?";
			//stmt.text= "SELECT * FROM LM010 WHERE TKAREA='01' AND HPYM='2012/06' AND TKCD LIKE ";
			stmt.text= "SELECT * FROM LM009";
			
			//stmt.parameters[0] = p_param1;
			//stmt.parameters[1] = p_param2;
			
			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, cdsLM009Result);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);
			
			//実行
			stmt.execute();
		}
		
		protected function stmtErrorHandler(event:SQLErrorEvent):void
		{
			// TODO Auto-generated method stub
			// エラーの表示
			trace("SQLerr");
		}
		
		/**
		 * SELECT処理結果
		 */
		private function cdsLM009Result(event:SQLEvent):void {
			//データの取得に成功したら
			//getResult()でSQLResultを取得し
			//取得したデータをデータグリッドに反映します。
			//status += "  データグリッド更新";
			var result:SQLResult = stmt.getResult();
			
			// 一旦、ワーク配列にセット 
			//w_arr = new ArrayCollection(result.data);
			arrLM009 = new ArrayCollection(result.data);

			
			view.grdW01.dataProvider = arrLM009;
			//view.grp1.dataProvider = w_arr;
		}
		
		public function insMA020X(nextfunc: Function=null): void
		{
			/*
			stmt = new SQLStatement();
			stmt.sqlConnection =  connection;
			
			var ite: Iterator = new CustomIterator(arrUA010X);
			var w_AUM01: AUM01 = new AUM01();
			
			while (ite.hasNext())
			{
				w_AUM01 = AUM01(ite.next());
				
				// SQL文の取得
				stmt.text= "INSERT INTO AUM01 (ISEQ, WSID, NAME) VALUES (?, ?, ?)";
				
				stmt.parameters[0] = w_AUM01.ISEQ;
				stmt.parameters[1] = w_AUM01.WSID;
				stmt.parameters[2] = w_AUM01.NAME;
				
				//実行
				stmt.execute();
			}
			*/
			// 次の処理？
			
		}

		/**********************************************************
		 * Backキー押下時処理.
		 * @author 12.08.22 SE-354
		 **********************************************************/
		protected function onKeyDownHandler(e:KeyboardEvent):void
		{
			e.preventDefault();
			
			btnCANCELClick();
		}

		/**********************************************************
		 * btnOK クリック時処理.
		 * @author 12.08.22 SE-354
		 **********************************************************/
		public function btnOKClick(): void
		{
			PopUpManager.removePopUp(view);
		}
		
		/**********************************************************
		 * btnCANCEL クリック時処理.
		 * @author 12.08.22 SE-354
		 **********************************************************/
		public function btnCANCELClick(): void
		{
			PopUpManager.removePopUp(view);
		}
	}
}