package views
{
	import modules.custom.DraggableIconItemRenderer;
	
	import mx.states.SetStyle;
	
	import spark.components.Label;
	import spark.components.supportClasses.StyleableTextField;
	
	/**
	 * 
	 * 
	 */ 
	public class MA020GridRenderer extends DraggableIconItemRenderer
	{
		
		//protected var edtESTAMT:StyleableTextField;
		//protected var edtESTAMT:Label;
		//private var edtTKADD3: Label;
		
		private var dataChanged:Boolean;
		
		public function MA020GridRenderer()
		{
			super();
		}
		
		override protected function createChildren() : void
		{
			super.createChildren();
		}
		
		override protected function commitProperties():void
		{
			super.commitProperties();
			
			if (iconDisplay)
			{
			}
			
			if (dataChanged)
			{
				
				dataChanged = false;
				
				/*
				if (!edtESTAMT)
				{
					createESTAMT();
				}
				edtESTAMT.text = data.ESTAMT;
				*/
				invalidateSize();
				invalidateDisplayList();
			}
			
		}
		
		/**
		 *  Creates the labelDisplay component
		 * 
		 *  @langversion 3.0
		 *  @playerversion AIR 2.5
		 *  @productversion Flex 4.5
		 */ 
		/*
		protected function createESTAMT():void
		{
			//edtESTAMT = StyleableTextField(createInFontContext(StyleableTextField));
			//edtESTAMT.styleName = this;
			//edtESTAMT.editable = false;
			//edtESTAMT.selectable = false;
			//edtESTAMT.multiline = false;
			//edtESTAMT.wordWrap = false;
			edtESTAMT = new Label();
			//edtESTAMT.styleName = "DisplayItem";
			edtESTAMT.x = 400;
			edtESTAMT.y = 10;
			edtESTAMT.width = 150;
			edtESTAMT.height = 33;
			
			addChild(edtESTAMT);
		}
		*/
		
		override public function set data(value : Object) : void
		{
			
			super.data = value;
			
			//edtTKADD3.text = data.TKADD3;
			
			// super.data = parentDocument
			dataChanged = true;
			
			if (data != null)
			{
				if (data.TKRANK == "1")
				{
					// 背景色：赤
					setStyle("alternatingItemColors", 0xf4a0bd);
					setStyle("selectionColor", 0xf4a0bd);
				}
				else if (data.TKRANK == "2")
				{
					// 背景色：緑
					setStyle("alternatingItemColors", 0x89ca9d);
					setStyle("selectionColor", 0x89ca9d);
				}
				else if (data.TKRANK == "3")
				{
					// 背景色：黄
					setStyle("alternatingItemColors", 0xfff99d);
					setStyle("selectionColor", 0xfff99d);
				}
				else if (data.TKRANK == "4")
				{
					// 背景色：青
					setStyle("alternatingItemColors", 0x8dcff4);
					setStyle("selectionColor", 0x8dcff4);
				}
				else if (data.TKRANK == "5")
				{
					// 背景色：白
					setStyle("alternatingItemColors", 0xffffff);
					setStyle("selectionColor", 0xffffff);
				}
				else
				{
					// 背景色：茶
					setStyle("alternatingItemColors", 0xe4c098);
					setStyle("selectionColor", 0xe4c098);
				}
				/*
				if (edtESTAMT)
				{
					edtESTAMT.text = data.ESTAMT;
				}
				*/
			}
			
			
			invalidateProperties();
		}
	}
}