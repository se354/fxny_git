package views
{
	import flash.data.SQLConnection;
	import flash.data.SQLResult;
	import flash.data.SQLStatement;
	import flash.events.IEventDispatcher;
	import flash.events.KeyboardEvent;
	import flash.events.SQLErrorEvent;
	import flash.events.SQLEvent;
	import flash.ui.Keyboard;
	
	import modules.base.BasedMethod;
	import modules.custom.CustomIterator;
	import modules.custom.sub.Iterator;
	import modules.dto.LJ003;
	import modules.dto.LM010;
	import modules.dto.W_MA030X;
	import modules.dto.W_MA031X;
	import modules.formatters.CustomNumberFormatter;
	import modules.sbase.SiteModule;
	
	import mx.collections.ArrayCollection;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	import mx.rpc.http.HTTPService;
	
	public class MA030XController extends SiteModule
	{
		private var view:MA030X;
		private var arrMA030X: ArrayCollection;
		private var arrMA031X: ArrayCollection;
		private var arrCUCD2: ArrayCollection;
		private var stmt: SQLStatement;
		private var connection:SQLConnection;
		private var g_updateCount: int;
		private var g_tkcd: String;			// 得意先コード
		private var g_vseq: String;			// 伝票SEQ
		// 12.10.31 SE-233 Redmine#3578 対応 start
		private var g_date: String;			// 訪問日付
		// 12.10.31 SE-233 Redmine#3578 対応 start
		private var g_time: String;			// 訪問時間
		private var g_seq: int;
		private var g_updflg: int;
		private var g_tax: String;
		
		private var popup002: POPUP002;		// 商品ポップアップ
		private var popup009: POPUP009;		// 現売選択ポップアップ
		public var popup: MP040X;			// 請求額ポップアップ
		public var argVJAN: String;
		public var argWITHOUTTAX: String;		// 使用金額（別）
		public var argWITHTAX: String;			// 使用金額（込）
		public var argDISC: String;			// 値引額
		public var argAMT: String;				// 請求額
		public var argDISCF: String;			// 値引率
		public var argDISCRT: String;			// 前回値引率
		public var argTAX: String;				// 税率
		public var argVSEQ: String;				// 伝票SEQ
		
		// 帳票用配列
		private var arrPRINT: ArrayCollection;
		
		private var hts: HTTPService;
		
		private var printHeader_arg: Object;
		private var printBody_arg: Object;
		private var printFutter_arg: Object;
		
		// 帳票用変数
		private var reportCount: int=0;				// 帳票明細数
		private var currentCount: int=0;				// 現在印刷明細番号
		
		// 引数情報
		private var cdsMA030X_arg: Object;
		private var cdsMA030X2_arg: Object;
		private var delMA030X1_arg: Object;
		private var insMA030X1_arg: Object;
		private var updMA030X1_arg: Object;
		private var updMA030X2_arg: Object;
		private var updMA030X3_arg: Object;
		private var delMA030X2_arg: Object;
		private var insMA030X2_arg: Object;
		private var cdsCUCD2_arg: Object;
		
		
		public function MA030XController()
		{
			super();
		}
		
		override public function init():void
		{
			//super.init();
			
			// フォーム宣言
			view = _view as MA030X;
			
			// 初期表示設定
			g_tkcd = view.data.TKCD;
			view.edtCUNM.text = view.data.TKNM;
			
			arrMA030X = new ArrayCollection();	
			arrMA031X = new ArrayCollection();	
			
			// 12.10.12 SE-362 Redmine#3435 対応 start
			view.stage.addEventListener("keyDown", keyDownBack, false, 1, true);
			// 12.10.12 SE-362 Redmine#3435 対応 end
			
			// ボタンに色を付ける
			view.btnPRINT.setStyle("chromeColor", 0x55aaff);
			
			super.init();
		}
		
		override public function formShow_last():void
		{
			super.formShow_last();
			
			// 画面データ表示
			doDSPProc();
		}
		
		/**********************************************************
		 * ヘッダデータ取得
		 * @author  12.10.18 SE-233
		 **********************************************************/
		override public function openDataSet_H(): void
		{
			// 制御ﾏｽﾀ情報取得
			cdsSEIGYO(openDataSet_H_cld1);
		}
		
		private function openDataSet_H_cld1(): void
		{
			// 得意先マスタ情報取得
			cdsCUCD2(openDataSet_H_cld2);
		}
		
		/*
		private function openDataSet_H_cld2(): void
		{
		// 別売情報取得
		cdsMA030X(openDataSet_H_cld3);
		}
		
		private function openDataSet_H_cld3(): void
		{
		// 別売JA情報取得
		cdsMA030X2(openDataSet_H_cld4);
		}
		
		*/
		//private function openDataSet_H_cld4(): void
		private function openDataSet_H_cld2(): void
		{
			// 担当者名取得
			cdsCHGCD2(view.data.TKCHGCD, super.openDataSet_H);
		}
		
		/**********************************************************
		 * 画面項目セット
		 * @author SE-354 
		 * @date 12.04.27
		 **********************************************************/
		override public function setDispObject(): void
		{
			super.setDispObject();
			
			/*			
			// 明細項目表示
			if(arrMA031X != null && arrMA031X.length != 0){
			view.edtVSEQ.text = arrMA031X[0].VNO;
			view.edtMEDIBAL.text = arrMA030X[0].MEDIBAL;
			view.edtSTOREBAL.text = arrMA030X[0].STOREBAL;
			view.edtDEVBAL.text = arrMA030X[0].DEVBAL;
			view.edtMEDIBAL1.text = arrMA030X[0].MEDIZMNY;
			view.edtSTOREBAL1.text = arrMA030X[0].STOREZMNY;
			view.edtDEVBAL1.text = arrMA030X[0].DEVZMNY;
			g_updflg=1;
			} else {
			// 新規得意先
			view.edtVSEQ.text = (Number(arrSEIGYO[0].BVNO) + 1).toString();
			view.edtMEDIBAL.text = arrCUCD2[0].GMEDIKURI;
			view.edtSTOREBAL.text = arrCUCD2[0].GSTOREKURI;
			view.edtDEVBAL.text = arrCUCD2[0].GDEVKURI;
			g_updflg=0;
			}
			
			view.grp1.dataProvider = arrMA031X;
			
			// 伝票番号退避
			g_vseq = view.edtVSEQ.text;
			*/			
			// 税率取得
			//getTax(arrSEIGYO[0].SDDAY);
			getTax(view.data.HDAT);
			
			// ヘッダ合計算出
			//			calcSum();
			
			g_vseq = "";
			
			// 現売データ読込
			openMA030X();
			
			insLF013(g_tkcd,"別売入力開始");
		}
		
		// 12.10.12 SE-362 Redmine#3435 対応 start
		private function keyDownBack(e: KeyboardEvent): void
		{
			e.preventDefault();
			
			if(e.keyCode == Keyboard.BACK){
				btnRTNClick();
			}
		}
		// 12.10.12 SE-362 Redmine#3435 対応 end
		
		
		/**********************************************************
		 * openMA030X 別売データ読込処理.
		 * @author 12.10.29 SE-233
		 **********************************************************/
		public function openMA030X(): void
		{
			// 別売情報取得
			cdsMA030X(openMA030X_cld1);
		}
		
		private function openMA030X_cld1(): void
		{
			// 別売JA情報取得
			cdsMA030X2(openMA030X_cld2);
		}
		
		private function openMA030X_cld2(): void
		{
			// 明細項目表示
			if(arrMA031X != null && arrMA031X.length != 0){
				view.edtVSEQ.text = arrMA031X[0].VNO;
				view.edtMEDIBAL.text = arrMA030X[0].MEDIBAL;
				view.edtSTOREBAL.text = arrMA030X[0].STOREBAL;
				view.edtDEVBAL.text = arrMA030X[0].DEVBAL;
				view.edtMEDIBAL1.text = arrMA030X[0].MEDIZMNY;
				view.edtSTOREBAL1.text = arrMA030X[0].STOREZMNY;
				view.edtDEVBAL1.text = arrMA030X[0].DEVZMNY;
				g_updflg=1;
				// 12.10.31 SE-233 Redmine#3578 対応 start
				g_date = arrMA030X[0].HDAT;	
				g_time = arrMA030X[0].HTIME;
				// 12.10.31 SE-233 Redmine#3578 対応 end
			} else {
				// 新規得意先
				//view.edtVSEQ.text = (Number(arrSEIGYO[0].BVNO) + 1).toString();
				view.edtVSEQ.text = getVNO("2");
				//view.edtMEDIBAL.text = arrCUCD2[0].GMEDIKURI;
				view.edtMEDIBAL.text = String(Number(arrCUCD2[0].GMEDIKURI)
					+ Number(arrCUCD2[0].GMEDIAMT)
					+ Number(arrCUCD2[0].GMEDITAX)
					- Number(arrCUCD2[0].GMEDINY)
					- Number(arrCUCD2[0].GMEDIDIS)
					- Number(arrCUCD2[0].GMEDIKAS));
				//view.edtSTOREBAL.text = arrCUCD2[0].GSTOREKURI;
				view.edtSTOREBAL.text = String(Number(arrCUCD2[0].GSTOREKURI)
					+ Number(arrCUCD2[0].GSTOREAMT)
					+ Number(arrCUCD2[0].GSTORETAX)
					- Number(arrCUCD2[0].GSTORENY)
					- Number(arrCUCD2[0].GSTOREDIS)
					- Number(arrCUCD2[0].GSTOREKAS));
				//view.edtDEVBAL.text = arrCUCD2[0].GDEVKURI;
				view.edtDEVBAL.text	= String(Number(arrCUCD2[0].GDEVKURI)
					+ Number(arrCUCD2[0].GDEVAMT)
					+ Number(arrCUCD2[0].GDEVTAX)
					- Number(arrCUCD2[0].GDEVNY)
					- Number(arrCUCD2[0].GDEVDIS)
					- Number(arrCUCD2[0].GDEVKAS));
				g_updflg=0;
				// 12.10.31 SE-233 Redmine#3578 対応 start
				//g_date = arrSEIGYO[0].SDDAY;
				g_date = view.data.HDAT;
				g_time = BasedMethod.getTime();
				// 12.10.31 SE-233 Redmine#3578 対応 end
			}
			// 14.03.03 SE-233 start
			getTax(g_date);
			// 14.03.03 SE-233 end
			
			// 12.11.06 SE-233 Redmine#3628 対応 start
			getEra(g_date);
			// 12.11.06 SE-233 Redmine#3628 対応 start
			
			
			// 修正時、見出し表示
			if(g_updflg == 1)
				view.lblMODIFY.text = "修正";
			
			view.grp1.dataProvider = arrMA031X;
			
			// 伝票番号退避
			g_vseq = view.edtVSEQ.text;
			
			// ヘッダ合計算出
			calcSum();
		}
		
		/**********************************************************
		 * 得意先マスタ情報取得
		 * @author 12.10.17 SE-233
		 **********************************************************/
		public function cdsCUCD2(func: Function=null): void
		{
			// 引数情報
			cdsCUCD2_arg = new Object;
			cdsCUCD2_arg.func = func;
			
			stmt = new SQLStatement();
			stmt.sqlConnection =  singleton.sqlconnection;
			stmt.parameters[0] = g_tkcd;
			stmt.itemClass = LM010;
			stmt.text= singleton.g_query_xml.entry.(@key == "qryCUCD2");
			
			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, cdsCUCD2Result, false, 0, true);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler, false, 0, true);
			
			//実行
			stmt.execute();
		}
		
		public function cdsCUCD2Result(event: SQLEvent): void
		{
			var w_arg: Object = cdsCUCD2_arg;
			var result:SQLResult = stmt.getResult();
			
			stmt.removeEventListener(SQLEvent.RESULT, cdsCUCD2Result);				
			stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);				
			stmt=null;
			
			arrCUCD2 = new ArrayCollection(result.data);
			
			// 次関数実行
			if(w_arg.func)
				w_arg.func();
		}
		
		/**********************************************************
		 * 別売情報取得
		 * @author 12.10.17 SE-233
		 **********************************************************/
		public function cdsMA030X(func: Function=null): void
		{
			// 引数情報
			cdsMA030X_arg = new Object;
			cdsMA030X_arg.func = func;
			
			stmt = new SQLStatement();
			stmt.sqlConnection =  singleton.sqlconnection;
			stmt.parameters[0] = g_tkcd;
			stmt.parameters[1] = g_vseq;
			stmt.itemClass = W_MA031X;
			stmt.text= singleton.g_query_xml.entry.(@key == "qryMA030X");
			
			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, cdsMA030XResult, false, 0, true);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler, false, 0, true);
			
			//実行
			stmt.execute();
		}
		
		public function cdsMA030XResult(event: SQLEvent): void
		{
			var w_arg: Object = cdsMA030X_arg;
			var result:SQLResult = stmt.getResult();
			
			stmt.removeEventListener(SQLEvent.RESULT, cdsMA030XResult);				
			stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);				
			stmt=null;
			
			arrMA031X = new ArrayCollection(result.data);
			
			// 次関数実行
			if(w_arg.func)
				w_arg.func();
		}
		
		/**********************************************************
		 * 別売JA情報取得
		 * @author 12.10.18 SE-233
		 **********************************************************/
		public function cdsMA030X2(func: Function=null): void
		{
			// 引数情報
			cdsMA030X2_arg = new Object;
			cdsMA030X2_arg.func = func;
			
			stmt = new SQLStatement();
			stmt.sqlConnection =  singleton.sqlconnection;
			stmt.parameters[0] = g_tkcd;
			stmt.parameters[1] = g_vseq;
			stmt.itemClass = LJ003;
			stmt.text= singleton.g_query_xml.entry.(@key == "qryMA030X2");
			
			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, cdsMA030X2Result, false, 0, true);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler, false, 0, true);
			
			//実行
			stmt.execute();
		}
		
		public function cdsMA030X2Result(event: SQLEvent): void
		{
			var w_arg: Object = cdsMA030X2_arg;
			var result:SQLResult = stmt.getResult();
			
			stmt.removeEventListener(SQLEvent.RESULT, cdsMA030X2Result);				
			stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);				
			stmt=null;
			
			arrMA030X = new ArrayCollection(result.data);
			
			// 次関数実行
			if(w_arg.func)
				w_arg.func();
		}
		
		/**********************************************************
		 * btnMOTO クリック時処理.
		 * @author 12.10.19 SE-233
		 **********************************************************/
		public function btnMOTOClick(): void
		{
			view.navigator.pushView(MF010X, view.data);
		}
		
		/**********************************************************
		 * btnRTN クリック時処理.
		 * @author 12.07.03 SE-354
		 **********************************************************/
		public function btnRTNClick(): void
		{
			// 12.10.12 SE-362 Redmine#3435 対応 start
			// 確認画面の表示
			singleton.sitemethod.dspJoinErrMsg("Q0027", btnRTNClick_last, "", "1", null, btnRTNClick_last2);
			// 12.10.12 SE-362 Redmine#3435 対応 end
		}
		
		// 12.10.12 SE-362 Redmine#3435 対応 start
		private function btnRTNClick_last(): void
		{
			insLF013(g_tkcd,"別売入力終了");
			
			// 前画面に遷移
			view.navigator.popView();
		}
		
		private function btnRTNClick_last2(): void
		{
			// 無処理
		}
		// 12.10.12 SE-362 Redmine#3435 対応 end
		
		/**********************************************************
		 * 合計算出処理
		 * @author 12.10.18 SE-233
		 **********************************************************/
		public function calcSum(): void
		{
			var f: CustomNumberFormatter = new CustomNumberFormatter();
			
			var w_withtax1: Number = 0;
			var w_withtax2: Number = 0;
			var w_withtax3: Number = 0;
			var w_dis1: Number = 0;
			var w_dis2: Number = 0;
			var w_dis3: Number = 0;
			var w_amt1: Number = 0;
			var w_amt2: Number = 0;
			var w_amt3: Number = 0;
			var w_namt1: Number = 0;
			var w_namt2: Number = 0;
			var w_namt3: Number = 0;
			
			// 12.11.01 SE-233 Redmine#3600対応 start
			view.edtMEDIBAL.text = BasedMethod.NumChk(view.edtMEDIBAL.text);
			view.edtSTOREBAL.text = BasedMethod.NumChk(view.edtSTOREBAL.text);
			view.edtDEVBAL.text = BasedMethod.NumChk(view.edtDEVBAL.text);
			view.edtMEDIBAL1.text = BasedMethod.NumChk(view.edtMEDIBAL1.text);
			view.edtSTOREBAL1.text = BasedMethod.NumChk(view.edtSTOREBAL1.text);
			view.edtDEVBAL1.text = BasedMethod.NumChk(view.edtDEVBAL1.text);
			// 12.11.01 SE-233 Redmine#3600対応 end
			
			// 集計
			for(var i: int=0; i < arrMA031X.length; i++){
				if(arrMA031X[i].BCLS == "1"){
					// 配置薬
					w_withtax1 += Number(arrMA031X[i].WITHTAX);
					w_dis1 += Number(arrMA031X[i].DIS);
					w_amt1 += Number(arrMA031X[i].AMT);
					w_namt1 += Number(arrMA031X[i].MONEY);
				}else if(arrMA031X[i].BCLS == "2"){				
					// 来店
					w_withtax2 += Number(arrMA031X[i].WITHTAX);
					w_dis2 += Number(arrMA031X[i].DIS);
					w_amt2 += Number(arrMA031X[i].AMT);
					w_namt2 += Number(arrMA031X[i].MONEY);
				}else if(arrMA031X[i].BCLS == "3"){				
					// 医療具
					w_withtax3 += Number(arrMA031X[i].WITHTAX);
					w_dis3 += Number(arrMA031X[i].DIS);
					w_amt3 += Number(arrMA031X[i].AMT);
					w_namt3 += Number(arrMA031X[i].MONEY);
				}
			}
			// 前繰
			view.edtTBAL.text = BasedMethod.calcNum(BasedMethod.calcNum(view.edtMEDIBAL.text,view.edtSTOREBAL.text),view.edtDEVBAL.text);
			// 前繰入金
			view.edtTBAL1.text = BasedMethod.calcNum(BasedMethod.calcNum(view.edtMEDIBAL1.text,view.edtSTOREBAL1.text),view.edtDEVBAL1.text);
			// 使用（込）
			view.edtWITHTAX1.text = w_withtax1.toString();
			view.edtWITHTAX2.text = w_withtax2.toString();
			view.edtWITHTAX3.text = w_withtax3.toString();
			view.edtTWITHTAX.text = BasedMethod.calcNum(BasedMethod.calcNum(view.edtWITHTAX1.text,view.edtWITHTAX2.text),view.edtWITHTAX3.text);
			// 値引額
			view.edtDIS1.text = w_dis1.toString();
			view.edtDIS2.text = w_dis2.toString();
			view.edtDIS3.text = w_dis3.toString();
			view.edtTDIS.text = BasedMethod.calcNum(BasedMethod.calcNum(view.edtDIS1.text,view.edtDIS2.text),view.edtDIS3.text);
			// 請求額
			view.edtAMT1.text = BasedMethod.calcNum(w_amt1.toString(),view.edtMEDIBAL.text);
			view.edtAMT2.text = BasedMethod.calcNum(w_amt2.toString(),view.edtSTOREBAL.text);
			view.edtAMT3.text = BasedMethod.calcNum(w_amt3.toString(),view.edtDEVBAL.text);
			view.edtTAMT.text = BasedMethod.calcNum(BasedMethod.calcNum(view.edtAMT1.text,view.edtAMT2.text),view.edtAMT3.text);
			// 今回入金
			view.edtNAMT1.text = w_namt1.toString();
			view.edtNAMT2.text = w_namt2.toString();
			view.edtNAMT3.text = w_namt3.toString();
			view.edtTNAMT.text = BasedMethod.calcNum(BasedMethod.calcNum(view.edtNAMT1.text,view.edtNAMT2.text),view.edtNAMT3.text);
			// 入金計
			view.edtTNAMT1.text = BasedMethod.calcNum(w_namt1.toString(),view.edtMEDIBAL1.text);
			view.edtTNAMT2.text = BasedMethod.calcNum(w_namt2.toString(),view.edtSTOREBAL1.text);
			view.edtTNAMT3.text = BasedMethod.calcNum(w_namt3.toString(),view.edtDEVBAL1.text);
			view.edtTTNAMT.text = BasedMethod.calcNum(BasedMethod.calcNum(view.edtTNAMT1.text,view.edtTNAMT2.text),view.edtTNAMT3.text);
			// 次回繰越
			view.edtNEXTBAL1.text = BasedMethod.calcNum(view.edtAMT1.text,view.edtTNAMT1.text,1);
			view.edtNEXTBAL2.text = BasedMethod.calcNum(view.edtAMT2.text,view.edtTNAMT2.text,1);
			view.edtNEXTBAL3.text = BasedMethod.calcNum(view.edtAMT3.text,view.edtTNAMT3.text,1);
			view.edtTNEXTBAL.text = BasedMethod.calcNum(BasedMethod.calcNum(view.edtNEXTBAL1.text,view.edtNEXTBAL2.text),view.edtNEXTBAL3.text);
			
			//			arrMA031X.refresh();
			
		}
		
		/**********************************************************
		 * btnVSEQ クリック時処理.
		 * @author 12.10.29 SE-233
		 **********************************************************/
		public function btnVSEQClick(): void
		{
			// 別売伝票選択ポップアップ呼び出し
			popup009 = new POPUP009();
			
			popup009.ctrl.argTKCD = g_tkcd;
			
			// 入力画面立ち上げ					
			PopUpManager.addPopUp(popup009, view, true);
			PopUpManager.centerPopUp(popup009);
			popup009.addEventListener(FlexEvent.REMOVE, btnVSEQClick_last, false, 0, true);
		}
		
		private function btnVSEQClick_last(e: FlexEvent): void
		{
			//IEventDispatcher(e.currentTarget).removeEventListener(FlexEvent.REMOVE, btnVSEQClick_last);				
			
			if(popup009.ctrl.LJ003Record){
				
				g_vseq = popup009.ctrl.LJ003Record.VNO;
				
				// 現売データ読込
				openMA030X();
			}
		}
		
		/**********************************************************
		 * btnVJAN クリック時処理.
		 * @author 12.09.24 SE-354
		 **********************************************************/
		public function btnVJANClick(): void
		{
			// 商品ポップアップ呼び出し
			popup002 = new POPUP002();
			
			// 訪問日
			popup002.ctrl.argHDAT = view.data.HDAT;
			
			// 入力画面立ち上げ					
			PopUpManager.addPopUp(popup002, view, true);
			PopUpManager.centerPopUp(popup002);
			popup002.addEventListener(FlexEvent.REMOVE, btnVJANClick_last, false, 0, true);
		}
		
		private function btnVJANClick_last(e: FlexEvent): void
		{
			//var index: int = arrMA031X2.length;
			var index: int;
			
			//IEventDispatcher(e.currentTarget).removeEventListener(FlexEvent.REMOVE, btnVJANClick_last);				
			
			if(popup002.ctrl.LM006Record){
				
				index = arrMA031X.length;
				
				// 12.10.29 SE-233 Redmine#3567 対応 start
				//// 商品コード重複チェック
				//for(var i:int=0; i < arrMA031X.length; i++){
				//	if(arrMA031X[i].VJAN == popup002.ctrl.LM006Record.VJAN){
				//		singleton.sitemethod.dspJoinErrMsg("E0181", null, "", "0", ["商品コード"]);
				//		return;
				//	}
				//}
				// 12.10.29 SE-233 Redmine#3567 対応 end
				
				// レコード追加
				arrMA031X.addItem(new W_MA031X());
				
				arrMA031X[index].TKCD   = g_tkcd;
				arrMA031X[index].VNO    = g_vseq;
				arrMA031X[index].VSEQ   = arrMA031X.length.toString();
				arrMA031X[index].BCLS   = popup002.ctrl.LM006Record.VJAN.substr(0,1);
				arrMA031X[index].GDCLS  = popup002.ctrl.LM006Record.MDCLSNMS;
				arrMA031X[index].VJAN   = popup002.ctrl.LM006Record.VJAN;
				arrMA031X[index].GDNM   = popup002.ctrl.LM006Record.GDNM;
				arrMA031X[index].SVPNO  = "1";
				arrMA031X[index].DISCST = popup002.ctrl.LM006Record.DISCST;
				
				if(popup002.ctrl.LM006Record.STCST!=0){
					arrMA031X[index].SAL = BasedMethod.calcNum(BasedMethod.calcNum(popup002.ctrl.LM006Record.STCST,
						BasedMethod.calcNum(BasedMethod.nvl(String(getTax_ret.tax), "0"),"100"),2),"100",3,0,1);
					arrMA031X[index].WITHTAX = arrMA031X[index].SAL;
					arrMA031X[index].WITHOUTTAX = popup002.ctrl.LM006Record.STCST;
					arrMA031X[index].AMT = BasedMethod.calcNum(BasedMethod.calcNum(popup002.ctrl.LM006Record.DISCST,
						BasedMethod.calcNum(BasedMethod.nvl(String(getTax_ret.tax), "0"),"100"),2),"100",3,0,1);
					
					// 値引率、値引、消費税計算
					arrMA031X[index].DIS = BasedMethod.calcNum(arrMA031X[index].WITHOUTTAX,BasedMethod.calcNum(BasedMethod.calcNum(arrMA031X[index].AMT, "100", 2),
						BasedMethod.calcNum("100", BasedMethod.nvl(String(getTax_ret.tax), "0")), 3,0,2), 1); 
					arrMA031X[index].PDIS = BasedMethod.calcNum(Math.floor(Number(
						BasedMethod.calcNum(BasedMethod.calcNum(arrMA031X[index].DIS, arrMA031X[index].WITHOUTTAX, 3,4), "10000", 2))).toString(),"100",3,4,1);
					arrMA031X[index].TAX = BasedMethod.calcNum(arrMA031X[index].AMT,
						BasedMethod.calcNum(arrMA031X[index].AMT,BasedMethod.calcNum(BasedMethod.calcNum(BasedMethod.nvl(String(getTax_ret.tax), "0"),"100"),"100",3,3),3,0,2),1);
					arrMA031X[index].MONEY = arrMA031X[index].AMT; 
				}else{
					arrMA031X[index].SAL = 0; 
					arrMA031X[index].WITHTAX = 0;
					arrMA031X[index].WITHOUTTAX = 0;
					arrMA031X[index].AMT = 0; 
					arrMA031X[index].DIS = 0;
					arrMA031X[index].PDIS = 0;
					arrMA031X[index].TAX = 0; 
					arrMA031X[index].MONEY = 0; 
				}
				
				// ヘッダ合計算出
				calcSum();
				
				arrMA031X.refresh();
				view.grp1.dataProvider = arrMA031X;
				view.grp1.validateNow();
				var maxPosition: Number;
				maxPosition = arrMA031X.length * 41 - view.grp1.height;
				if(maxPosition < 0)
					maxPosition = 0;
				view.grp1.verticalScrollPosition = maxPosition; 
			}
		}
		
		/**********************************************************
		 * 請求金額クリック
		 * @author  12.10.16 SE-233
		 **********************************************************/
		/***
		 public function edtAMTClick(): void
		 {
		 // 値引率入力画面立ち上げ
		 popup = new MP040X();
		 
		 // 使用額（税抜）
		 popup.ctrl.argWITHOUTTAX = BasedMethod.asCurrency(argWITHOUTTAX);
		 // 使用額（税込）
		 popup.ctrl.argWITHTAX = BasedMethod.asCurrency(argWITHTAX);
		 // 値引額
		 popup.ctrl.argDISC = BasedMethod.asCurrency(argDISC);
		 // 請求額
		 popup.ctrl.argAMT = BasedMethod.asCurrency(argAMT); 
		 // 値引率
		 popup.ctrl.argDISCF = BasedMethod.asCurrency(argDISCF);
		 // 前回値引率
		 popup.ctrl.argDISCRT = BasedMethod.asCurrency(argDISCRT);
		 // 税率
		 popup.ctrl.argTAX = BasedMethod.asCurrency(getTax_ret.tax);
		 
		 PopUpManager.addPopUp(popup, view, true);
		 PopUpManager.centerPopUp(popup);
		 popup.addEventListener(FlexEvent.REMOVE, edtAMTClick_last);
		 }
		 
		 private function edtAMTClick_last(event: FlexEvent): void
		 {
		 // 対象データ更新
		 for(var i:int=0; i < arrMA031X.length; i++){
		 //if(arrMA031X[i].VJAN == argVJAN){
		 if(arrMA031X[i].VSEQ == argVSEQ){
		 // 請求額
		 arrMA031X[i].AMT = popup.ctrl.rtnAMT.toString();
		 // 値引額
		 arrMA031X[i].DIS = popup.ctrl.rtnDISC.toString();
		 // 値引率
		 arrMA031X[i].PDIS = popup.ctrl.rtnDISCF.toString();
		 // 消費税計算
		 arrMA031X[i].TAX = Math.floor(Number(BasedMethod.calcNum(arrMA031X[i].AMT.toString(),BasedMethod.calcNum(arrMA031X[i].AMT.toString(),BasedMethod.calcNum(BasedMethod.calcNum(BasedMethod.nvl(String(getTax_ret.tax), "0"),"100"),"100",3,5),3,5),1))).toString();
		 // 入金額
		 arrMA031X[i].MONEY = arrMA031X[i].AMT;
		 }
		 }
		 // 合計算出
		 calcSum();
		 }
		 ***/		
		/**********************************************************
		 * btnDEC クリック時処理.
		 * @author 13.01.25 SE-233
		 **********************************************************/
		public function btnDECClick(): void
		{
			// 入力チェック
			if(arrMA031X == null || arrMA031X.length == 0){
				singleton.sitemethod.dspJoinErrMsg("E0010");
				return;
			}else{
				singleton.sitemethod.dspJoinErrMsg("I0007", btnDECClick_cld1, "", "1", [], btnDECClick_cld2);
			}
		}
		private function btnDECClick_cld1(): void 
		{
			doF10Proc();
		}
		private function btnDECClick_cld2(): void 
		{
			return;
		}
		
		/**********************************************************
		 * データ更新
		 * @author 12.10.18 SE-233
		 **********************************************************/
		override public function updateDataSet_H():void
		{
			// 訪問時間取得
			//g_time = BasedMethod.getTime();
			
			// 変数クリア
			g_updateCount = 0;
			g_seq = 0;
			
			// 別売JB削除
			delMA030X2(updateDataSet_H_cld1);
		}
		
		private function updateDataSet_H_cld1(): void
		{
			// 数量＝０の明細は登録しない
			if(Number(arrMA031X[g_updateCount].SVPNO)!=0){
				// 別売JB登録
				g_seq++;
				insMA030X2(updateDataSet_H_countCheck);
			} else {
				updateDataSet_H_countCheck();
			}
		}
		
		private function updateDataSet_H_countCheck(): void
		{
			// カウントアップ
			g_updateCount++;
			
			if(g_updateCount >= arrMA031X.length){
				updateDataSet_H_cld2();
			}else{
				updateDataSet_H_cld1();
			}
		}
		
		private function updateDataSet_H_cld2(): void
		{
			// 別売JA削除
			delMA030X1(updateDataSet_H_cld3);
		}
		
		private function updateDataSet_H_cld3(): void
		{
			// 別売JA登録
			// 有効データなしの場合終了
			if(g_seq != 0){
				insMA030X1(updateDataSet_H_cld4);
			}else{				
				super.updateDataSet_H();
			}
		}
		
		private function updateDataSet_H_cld4(): void
		{
			// 制御マスタ（伝票番号）更新
			if(g_updflg == 0){
				//				updMA030X1(updateDataSet_H_cld5);
				updMA030X1(updateDataSet_H_cld6);
			}else{				
				//				updateDataSet_H_cld5();
				updateDataSet_H_cld6();
			}
		}
		//		private function updateDataSet_H_cld5(): void
		//		{
		//			// 制御マスタ（通し番号）更新
		//			updMA030X2(updateDataSet_H_cld6);
		//		}
		private function updateDataSet_H_cld6(): void
		{
			// 訪問実績F更新
			updMA030X3(super.updateDataSet_H);
		}
		
		
		/**********************************************************
		 * execDecPLSQL
		 * @author 12.10.18 SE-233
		 **********************************************************/
		override public function execDecPLSQL():void
		{
			// 伝票NoXXXXXXXが登録されました。
			//singleton.sitemethod.dspJoinErrMsg("I0004", execDecPLSQL_cld1, "", "0", ["伝票番号" + g_vseq]);
			// 13.04.16 SE-233 start
			//			execDecPLSQL_cld1();
			cdsSEIGYO(execDecPLSQL_cld1);
			// 13.04.16 SE-233 end
		}
		
		private function execDecPLSQL_cld1(): void
		{
			// 帳票用配列作成処理
			// 有効データなしの場合終了
			if(g_seq == 0){
				execDecPLSQL_last();
			} else {
				createPrintArray(execDecPLSQL_cld2);
			}	
		}
		
		private function execDecPLSQL_cld2(): void
		{
			// 帳票用配列作成処理
			singleton.sitemethod.dspJoinErrMsg("I0005", execDecPLSQL_cld3, "", "0", ["請求書 兼 領収書"]);
		}
		
		private function execDecPLSQL_cld3(): void
		{
			if(reportCount == 0){
				execDecPLSQL_cld4();
				return;
			}
			// 通しNO更新
			updTNO(execDecPLSQL_cld31);
		}
		
		private function execDecPLSQL_cld31(): void
		{
			// ログ出力
			insLF013(g_tkcd,"請求書 兼 領収書",g_vseq);
			// 請求書 兼 領収書発行
			printMA030X1_Header(execDecPLSQL_cld4);
		}
		
		private function execDecPLSQL_cld4(): void
		{
			// 帳票用配列作成処理
			singleton.sitemethod.dspJoinErrMsg("I0005", execDecPLSQL_cld5, "", "0", ["請求書 兼 領収書(会社控)"]);
		}
		
		private function execDecPLSQL_cld5(): void
		{
			if(reportCount == 0){
				execDecPLSQL_last();
				return;
			}
			// 通しNO更新
			updTNO(execDecPLSQL_cld51);
		}
		
		private function execDecPLSQL_cld51(): void
		{
			// ログ出力
			insLF013(g_tkcd,"請求書 兼 領収書(会社控)",g_vseq);
			// 請求書 兼 領収書発行
			printMA030X2_Header(execDecPLSQL_last);
		}
		
		private function execDecPLSQL_last(): void
		{
			super.execDecPLSQL();	
			
			insLF013(g_tkcd,"別売入力終了");
			
			view.navigator.popView();
		}
		
		/**********************************************************
		 * 別売JA削除
		 * @author 12.10.17 SE-233
		 **********************************************************/
		private function delMA030X1(func: Function=null): void
		{
			// 引数情報
			delMA030X1_arg = new Object;
			delMA030X1_arg.func = func;
			
			stmt = new SQLStatement();
			stmt.sqlConnection =  singleton.sqlconnection;
			stmt.text= singleton.g_query_xml.entry.(@key == "delMA030X1");
			stmt.parameters[0] = g_vseq;
			
			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, delMA030X1Result, false, 0, true);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler, false, 0, true);
			
			//実行
			stmt.execute();
		}
		
		public function delMA030X1Result(event: SQLEvent): void
		{	
			var w_arg: Object = delMA030X1_arg;
			
			stmt.removeEventListener(SQLEvent.RESULT, delMA030X1Result);				
			stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);				
			stmt=null;
			
			// 次関数実行
			if(w_arg.func)
				w_arg.func();
		}
		
		/**********************************************************
		 * 別売JA更新
		 * @author 12.10.17 SE-233
		 **********************************************************/
		public function insMA030X1(func: Function=null): void
		{
			// 引数情報
			insMA030X1_arg = new Object;
			insMA030X1_arg.func = func;
			
			var w_bal1: Number = 0;
			var w_withtax1: Number = 0;
			var w_withouttax1: Number = 0;
			var w_amt1: Number = 0;
			var w_tax1: Number = 0;
			var w_money1: Number = 0;
			var w_dis1: Number = 0;
			var w_rtax1: Number = 0;
			
			var w_bal2: Number = 0;
			var w_withtax2: Number = 0;
			var w_withouttax2: Number = 0;
			var w_amt2: Number = 0;
			var w_tax2: Number = 0;
			var w_money2: Number = 0;
			var w_dis2: Number = 0;
			var w_rtax2: Number = 0;
			
			var w_bal3: Number = 0;
			var w_withtax3: Number = 0;
			var w_withouttax3: Number = 0;
			var w_amt3: Number = 0;
			var w_tax3: Number = 0;
			var w_money3: Number = 0;
			var w_dis3: Number = 0;
			var w_rtax3: Number = 0;
			
			// 集計
			for(var i:int=0; i < arrMA031X.length; i++){
				if(arrMA031X[i].BCLS == "1"){
					// 配置薬
					w_withtax1 += Number(arrMA031X[i].WITHTAX);
					w_withouttax1 += Number(arrMA031X[i].WITHOUTTAX);
					w_amt1 += Number(arrMA031X[i].AMT);
					w_tax1 += Number(arrMA031X[i].TAX);
					w_money1 += Number(arrMA031X[i].MONEY);
					w_dis1 += Number(arrMA031X[i].DIS);
					//w_rtax1 += Math.floor(Number(BasedMethod.calcNum(arrMA031X[i].MONEY.toString(), BasedMethod.calcNum(BasedMethod.nvl(String(getTax_ret.tax), "0"), "100",3,4),2)));
					//					w_rtax1 += Number(BasedMethod.calcNum(arrMA031X[i].MONEY.toString(),
					//						BasedMethod.calcNum(arrMA031X[i].MONEY.toString(),BasedMethod.calcNum(BasedMethod.calcNum(BasedMethod.nvl(String(getTax_ret.tax), "0"),"100"),"100",3,3),3,0,2),1));
				}else if(arrMA031X[i].BCLS == "2"){				
					// 来店
					w_withtax2 += Number(arrMA031X[i].WITHTAX);
					w_withouttax2 += Number(arrMA031X[i].WITHOUTTAX);
					w_amt2 += Number(arrMA031X[i].AMT);
					w_tax2 += Number(arrMA031X[i].TAX);
					w_money2 += Number(arrMA031X[i].MONEY);
					w_dis2 += Number(arrMA031X[i].DIS);
					//w_rtax2 += Math.floor(Number(BasedMethod.calcNum(arrMA031X[i].MONEY.toString(), BasedMethod.calcNum(BasedMethod.nvl(String(getTax_ret.tax), "0"), "100",3,4),2)));
					//					w_rtax2 += Number(BasedMethod.calcNum(arrMA031X[i].MONEY.toString(),
					//						BasedMethod.calcNum(arrMA031X[i].MONEY.toString(),BasedMethod.calcNum(BasedMethod.calcNum(BasedMethod.nvl(String(getTax_ret.tax), "0"),"100"),"100",3,3),3,0,2),1));
				}else if(arrMA031X[i].BCLS == "3"){				
					// 医療具
					w_withtax3 += Number(arrMA031X[i].WITHTAX);
					w_withouttax3 += Number(arrMA031X[i].WITHOUTTAX);
					w_amt3 += Number(arrMA031X[i].AMT);
					w_tax3 += Number(arrMA031X[i].TAX);
					w_money3 += Number(arrMA031X[i].MONEY);
					w_dis3 += Number(arrMA031X[i].DIS);
					//w_rtax3 += Math.floor(Number(BasedMethod.calcNum(arrMA031X[i].MONEY.toString(), BasedMethod.calcNum(BasedMethod.nvl(String(getTax_ret.tax), "0"), "100",3,4),2)));
					//					w_rtax3 += Number(BasedMethod.calcNum(arrMA031X[i].MONEY.toString(),
					//						BasedMethod.calcNum(arrMA031X[i].MONEY.toString(),BasedMethod.calcNum(BasedMethod.calcNum(BasedMethod.nvl(String(getTax_ret.tax), "0"),"100"),"100",3,3),3,0,2),1));
				}
			}
			
			// 次回繰越額
			w_bal1 = Number(BasedMethod.calcNum(w_money1.toString(),view.edtMEDIBAL1.text));
			w_bal2 = Number(BasedMethod.calcNum(w_money2.toString(),view.edtSTOREBAL1.text));
			w_bal3 = Number(BasedMethod.calcNum(w_money3.toString(),view.edtDEVBAL1.text));
			
			w_rtax1 = Number(BasedMethod.calcNum(w_bal1.toString(),
				BasedMethod.calcNum(w_bal1.toString(),BasedMethod.calcNum(BasedMethod.calcNum(BasedMethod.nvl(String(getTax_ret.tax), "0"),"100"),"100",3,3),3,0,2),1));
			w_rtax2 = Number(BasedMethod.calcNum(w_bal2.toString(),
				BasedMethod.calcNum(w_bal2.toString(),BasedMethod.calcNum(BasedMethod.calcNum(BasedMethod.nvl(String(getTax_ret.tax), "0"),"100"),"100",3,3),3,0,2),1));
			w_rtax3 = Number(BasedMethod.calcNum(w_bal3.toString(),
				BasedMethod.calcNum(w_bal3.toString(),BasedMethod.calcNum(BasedMethod.calcNum(BasedMethod.nvl(String(getTax_ret.tax), "0"),"100"),"100",3,3),3,0,2),1));
			
			stmt = new SQLStatement();
			stmt.sqlConnection =  singleton.sqlconnection;
			stmt.text= singleton.g_query_xml.entry.(@key == "insMA030X1");
			
			// パラメタ情報
			stmt.parameters[0]  = g_vseq;
			stmt.parameters[1]  = g_tkcd;
			// 12.10.31 SE-233 Redmine#3578 対応 start
			//stmt.parameters[2]  = arrSEIGYO[0].SDDAY;
			stmt.parameters[2]  = g_date;
			// 12.10.31 SE-233 Redmine#3578 対応 end
			stmt.parameters[3]  = g_time;
			stmt.parameters[4]  = "14";
			stmt.parameters[5]  = arrSEIGYO[0].CHGCD;
			stmt.parameters[6]  = arrSEIGYO[0].HTID;
			
			stmt.parameters[7]  = Number(view.edtMEDIBAL.text);
			stmt.parameters[8]  = w_withtax1;
			stmt.parameters[9]  = w_withouttax1;
			stmt.parameters[10] = w_amt1;
			stmt.parameters[11] = w_tax1;
			stmt.parameters[12] = w_money1;
			stmt.parameters[13] = w_dis1;
			stmt.parameters[14] = 0;
			stmt.parameters[15] = w_rtax1;
			stmt.parameters[16] = w_amt1-w_money1;
			
			stmt.parameters[17] = Number(view.edtSTOREBAL.text);
			stmt.parameters[18] = w_withtax2;
			stmt.parameters[19] = w_withouttax2;
			stmt.parameters[20] = w_amt2;
			stmt.parameters[21] = w_tax2;
			stmt.parameters[22] = w_money2;
			stmt.parameters[23] = w_dis2;
			stmt.parameters[24] = 0;
			stmt.parameters[25] = w_rtax2;
			stmt.parameters[26] = w_amt2-w_money2;
			
			stmt.parameters[27] = Number(view.edtDEVBAL.text);
			stmt.parameters[28] = w_withtax3;
			stmt.parameters[29] = w_withouttax3;
			stmt.parameters[30] = w_amt3;
			stmt.parameters[31] = w_tax3;
			stmt.parameters[32] = w_money3;
			stmt.parameters[33] = w_dis3;
			stmt.parameters[34] = 0;
			stmt.parameters[35] = w_rtax3;
			stmt.parameters[36] = w_amt3-w_money3;
			
			stmt.parameters[37] = Number(view.edtMEDIBAL1.text);
			stmt.parameters[38] = Number(view.edtSTOREBAL1.text);
			stmt.parameters[39] = Number(view.edtDEVBAL1.text);
			stmt.parameters[40] = view.data.HDAT;
			
			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, insMA030X1Result, false, 0, true);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler, false, 0, true);
			
			//実行
			stmt.execute();
		}
		
		public function insMA030X1Result(event: SQLEvent): void
		{	
			var w_arg: Object = insMA030X1_arg;
			
			stmt.removeEventListener(SQLEvent.RESULT, insMA030X1Result);				
			stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);				
			stmt=null;
			
			// 次関数実行
			if(w_arg.func)
				w_arg.func();
		}
		
		/**********************************************************
		 * 別売JB削除
		 * @author 12.10.17 SE-233
		 **********************************************************/
		public function delMA030X2(func: Function=null): void
		{
			// 引数情報
			delMA030X2_arg = new Object;
			delMA030X2_arg.func = func;
			
			stmt = new SQLStatement();
			stmt.sqlConnection =  singleton.sqlconnection;
			stmt.text= singleton.g_query_xml.entry.(@key == "delMA030X2");
			stmt.parameters[0] = g_vseq;
			
			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, delMA030X2Result, false, 0, true);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler, false, 0, true);
			
			//実行
			stmt.execute();
		}
		
		public function delMA030X2Result(event: SQLEvent): void
		{	
			var w_arg: Object = delMA030X2_arg;
			
			stmt.removeEventListener(SQLEvent.RESULT, delMA030X2Result);				
			stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);				
			stmt=null;
			
			// 次関数実行
			if(w_arg.func)
				w_arg.func();
		}
		
		/**********************************************************
		 * 別売JB更新
		 * @author 12.10.17 SE-233
		 **********************************************************/
		public function insMA030X2(func: Function=null): void
		{
			// 引数情報
			insMA030X2_arg = new Object;
			insMA030X2_arg.func = func;
			
			stmt = new SQLStatement();
			stmt.sqlConnection =  singleton.sqlconnection;
			stmt.text= singleton.g_query_xml.entry.(@key == "insMA030X2");
			
			// パラメタ情報
			stmt.parameters[0]  = g_vseq;
			stmt.parameters[1]  = g_seq;
			stmt.parameters[2]  = arrMA031X[g_updateCount].VJAN;
			stmt.parameters[3]  = arrMA031X[g_updateCount].SAL;
			stmt.parameters[4]  = arrMA031X[g_updateCount].BCLS;
			stmt.parameters[5]  = arrMA031X[g_updateCount].GDCLS;
			stmt.parameters[6]  = arrMA031X[g_updateCount].SVPNO;
			stmt.parameters[7]  = arrMA031X[g_updateCount].WITHTAX;
			stmt.parameters[8]  = arrMA031X[g_updateCount].WITHOUTTAX;
			stmt.parameters[9]  = arrMA031X[g_updateCount].AMT;
			stmt.parameters[10] = arrMA031X[g_updateCount].TAX;
			stmt.parameters[11] = arrMA031X[g_updateCount].MONEY;
			stmt.parameters[12] = arrMA031X[g_updateCount].DIS;
			//stmt.parameters[13] = Math.floor(Number(BasedMethod.calcNum(arrMA031X[g_updateCount].MONEY.toString(), BasedMethod.calcNum(BasedMethod.nvl(String(getTax_ret.tax), "0"), "100",3,4),2)));
			stmt.parameters[13] = BasedMethod.calcNum(arrMA031X[g_updateCount].MONEY.toString(),
				BasedMethod.calcNum(arrMA031X[g_updateCount].MONEY.toString(),BasedMethod.calcNum(BasedMethod.calcNum(BasedMethod.nvl(String(getTax_ret.tax), "0"),"100"),"100",3,3),3,0,2),1);
			
			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, insMA030X2Result, false, 0, true);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler, false, 0, true);
			
			//実行
			stmt.execute();
		}
		
		public function insMA030X2Result(event: SQLEvent): void
		{	
			var w_arg: Object = insMA030X2_arg;
			var result:SQLResult = stmt.getResult();
			
			stmt.removeEventListener(SQLEvent.RESULT, insMA030X2Result);				
			stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);				
			stmt=null;
			
			// 次関数実行
			if(w_arg.func)
				w_arg.func();
		}
		
		/**********************************************************
		 * 制御マスタ更新
		 * @author 12.10.18 SE-233
		 **********************************************************/
		public function updMA030X1(func: Function=null): void
		{
			// 引数情報
			updMA030X1_arg = new Object;
			updMA030X1_arg.func = func;
			
			stmt = new SQLStatement();
			stmt.sqlConnection =  singleton.sqlconnection;
			stmt.text= singleton.g_query_xml.entry.(@key == "updMA030X1");
			
			// パラメタ情報
			// 別売入力用伝票No
			stmt.parameters[0] = view.edtVSEQ.text;
			
			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, updMA030X1Result, false, 0, true);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler, false, 0, true);
			
			//実行
			stmt.execute();
		}
		
		public function updMA030X1Result(event: SQLEvent): void
		{	
			var w_arg: Object = updMA030X1_arg;
			var result:SQLResult = stmt.getResult();
			
			stmt.removeEventListener(SQLEvent.RESULT, updMA030X1Result);				
			stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);				
			stmt=null;
			
			// 次関数実行
			if(w_arg.func)
				w_arg.func();
		}
		
		/**********************************************************
		 * 制御マスタ更新
		 * @author 12.10.18 SE-233
		 **********************************************************/
		/**
		 public function updMA030X2(func: Function=null): void
		 {
		 // 引数情報
		 updMA030X2_arg = new Object;
		 updMA030X2_arg.func = func;
		 
		 stmt = new SQLStatement();
		 stmt.sqlConnection =  singleton.sqlconnection;
		 stmt.text= singleton.g_query_xml.entry.(@key == "updMA030X2");
		 
		 // パラメタ情報
		 // 通しNo
		 stmt.parameters[0] = (Number(arrSEIGYO[0].TNO) + 1).toString();
		 
		 //イベント登録
		 stmt.addEventListener(SQLEvent.RESULT, updMA030X2Result);
		 stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);
		 
		 //実行
		 stmt.execute();
		 }
		 
		 public function updMA030X2Result(event: SQLEvent): void
		 {	
		 var w_arg: Object = updMA030X2_arg;
		 var result:SQLResult = stmt.getResult();
		 
		 // 次関数実行
		 if(w_arg.func)
		 w_arg.func();
		 }
		 **/		
		/**********************************************************
		 * 訪問実績F更新
		 * @author 12.11.09 SE-233
		 **********************************************************/
		public function updMA030X3(func: Function=null): void
		{
			// 引数情報
			updMA030X3_arg = new Object;
			updMA030X3_arg.func = func;
			
			stmt = new SQLStatement();
			stmt.sqlConnection =  singleton.sqlconnection;
			stmt.text= singleton.g_query_xml.entry.(@key == "updMA030X3");
			
			// パラメタ情報
			// 入替訪問時間
			stmt.parameters[0] = g_time;
			// 担当者コード
			stmt.parameters[1] = arrSEIGYO[0].CHGCD;
			// 訪問日付
			//stmt.parameters[2] = g_date;
			stmt.parameters[2] = view.data.HDAT;
			// 得意先コード
			stmt.parameters[3] = g_tkcd;
			
			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, updMA030X3Result, false, 0, true);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler, false, 0, true);
			
			//実行
			stmt.execute();
		}
		
		public function updMA030X3Result(event: SQLEvent): void
		{	
			var w_arg: Object = updMA030X3_arg;
			var result:SQLResult = stmt.getResult();
			
			stmt.removeEventListener(SQLEvent.RESULT, updMA030X3Result);				
			stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);				
			stmt=null;
			
			// 次関数実行
			if(w_arg.func)
				w_arg.func();
		}
		
		private function stmtErrorHandler(event: SQLErrorEvent): void
		{
			// エラーの表示
			trace(event.error.message);
		}
		
		/**********************************************************
		 * 帳票用配列作成
		 * @author 12.07.03 SE-354
		 **********************************************************/
		private function createPrintArray(func: Function): void
		{
			// 一括発行明細数
			var w_detail: int = 5;
			var w_length: int;
			var w_nodata: int;
			var i: int;
			
			arrPRINT = new ArrayCollection;
			
			if(arrMA031X != null && arrMA031X.length != 0){
				w_length = arrMA031X.length;
			}else{
				return;
			}
			
			for(i=0; i < w_length; i++){
				if(BasedMethod.nvl(arrMA031X[i].SVPNO, "0") != "0"){
					// 使用されているもの
					arrPRINT.addItem(arrMA031X[i]);
				}
			}
			
			if(arrPRINT.length % w_detail != 0){
				w_nodata = w_detail - (arrPRINT.length % w_detail);
			}
			
			reportCount = Math.floor(arrPRINT.length / w_detail) + 1;
			if(arrPRINT.length % w_detail == 0){
				reportCount--;
			}
			
			for(i=0; i < w_nodata; i++){
				arrPRINT.addItem(new W_MA031X());
			}
			
			func();
		}
		
		/**********************************************************
		 * 帳票作成
		 * @author 12.10.16 SE-233
		 **********************************************************/
		private function printMA030X1_Header(func: Function): void
		{
			hts = new HTTPService();
			var w_tknm1: String;
			var w_tknm2: String;
			
			printHeader_arg = new Object;
			printHeader_arg.func = func;
			
			// 明細数セット
			currentCount = 0;
			g_tax = '0';
			
			if(reportCount == 0){
				printMA030X1_last();
				return;
			}
			
			var tknm: Object = BasedMethod.getTKNM(view.data.TKNM);
			
			hts.method = "GET";
			// 15.03.19 SE-233 start
			// 15.01.16 SE-233 start
			//			hts.addEventListener(ResultEvent.RESULT, printMA030X1_Body);
			//			hts.addEventListener(ResultEvent.RESULT, printMA030X1_last);
			hts.addEventListener(ResultEvent.RESULT, printMA030X1_Body, false, 0, true);
			// 15.01.16 SE-233 end
			// 15.03.19 SE-233 end
			hts.addEventListener(FaultEvent.FAULT, htsFault, false, 0, true);
			hts.url = "http://localhost:8080/Format/Print";
			
			// 15.03.19 SE-233 start
			// 15.01.16 SE-233 start
			hts.send({
				"__format_archive_url": "file:///sdcard/print/me011x.spfmtz",
				"__format_archive_update": "updateWithoutError",
				"__format_id_number": "1",
				"TKCD": g_tkcd,
				"TKNM1": tknm.tknm1,
				"TKNM2": tknm.tknm2,
				"HDAT": getEra_ret.dat + " " + g_time,
				"VNO": g_vseq,
				"CHGNM": arrCHGCD2[0].CHGNM,
				"HED1": arrSEIGYO[0].COSNM1,
				"HED2": arrSEIGYO[0].COSNM2,
				"HED3": arrSEIGYO[0].COSNM3,
				"ADR1": arrSEIGYO[0].ADD1,
				"ADR2": arrSEIGYO[0].ADD2,
				"ADR3": arrSEIGYO[0].ADD3,
				"ADR23": arrSEIGYO[0].ADD23,
				"TEL1": arrSEIGYO[0].TEL1,
				"TEL2": arrSEIGYO[0].TEL2,
				"TEL3": arrSEIGYO[0].TEL3,
				"(発行枚数)": "1"
			});
		}
		
		private function printMA030X1_Body(e: ResultEvent=null): void
		{
			hts.removeEventListener(ResultEvent.RESULT, printMA030X1_Body);
			hts.removeEventListener(FaultEvent.FAULT, htsFault);
			
			if(e != null){
				// 印刷成功時処理
				if(e.result.response.result == "OK"){
					// 印刷失敗時 result == NG
				}else{
					singleton.sitemethod.dspJoinErrMsg(e.result.response.message,
						doF10Proc_last, "", "0", null, null, true);
					return;
				}
			}
			
			//var hts: HTTPService = new HTTPService();
			var nowDtail: int;
			
			hts.method = "GET";
			hts.addEventListener(ResultEvent.RESULT, printMA030X1_Loop, false, 0, true);
			hts.addEventListener(FaultEvent.FAULT, htsFault, false, 0, true);
			hts.url = "http://localhost:8080/Format/Print";
			
			nowDtail = (currentCount + 1) * 5;
			
			// 12.10.03 SE-362 明細がない場合は行NOを印字しない start
			var w_vlin1: String;
			var w_vlin2: String;
			var w_vlin3: String;
			var w_vlin4: String;
			var w_vlin5: String;
			
			if(BasedMethod.nvl(arrPRINT[nowDtail - 5].TKCD) != ""){
				w_vlin1 = (nowDtail - 4).toString();
				g_tax = BasedMethod.calcNum(g_tax, arrPRINT[nowDtail - 5].TAX);
			}else{
				w_vlin1 = "";
			}
			if(BasedMethod.nvl(arrPRINT[nowDtail - 4].TKCD) != ""){
				w_vlin2 = (nowDtail - 3).toString();
				g_tax = BasedMethod.calcNum(g_tax, arrPRINT[nowDtail - 4].TAX);
			}else{
				w_vlin2 = "";
			}
			if(BasedMethod.nvl(arrPRINT[nowDtail - 3].TKCD) != ""){
				w_vlin3 = (nowDtail - 2).toString();
				g_tax = BasedMethod.calcNum(g_tax, arrPRINT[nowDtail - 3].TAX);
			}else{
				w_vlin3 = "";
			}
			if(BasedMethod.nvl(arrPRINT[nowDtail - 2].TKCD) != ""){
				w_vlin4 = (nowDtail - 1).toString();
				g_tax = BasedMethod.calcNum(g_tax, arrPRINT[nowDtail - 2].TAX);
			}else{
				w_vlin4 = "";
			}
			if(BasedMethod.nvl(arrPRINT[nowDtail - 1].TKCD) != ""){
				w_vlin5 = (nowDtail).toString();
				g_tax = BasedMethod.calcNum(g_tax, arrPRINT[nowDtail - 1].TAX);
			}else{
				w_vlin5 = "";
			}
			// 12.10.03 SE-362 明細がない場合は行NOを印字しない end			
			
			hts.send({
				"__format_archive_url": "file:///sdcard/print/me011x.spfmtz",
				"__format_archive_update": "updateWithoutError",
				"__format_id_number": "2",
				"VLIN1": w_vlin1,
				"VLIN2": w_vlin2,
				"VLIN3": w_vlin3,
				"VLIN4": w_vlin4,
				"VLIN5": w_vlin5,
				"CLS1" : BasedMethod.nvl(arrPRINT[nowDtail - 5].GDCLS),
				"CLS2" : BasedMethod.nvl(arrPRINT[nowDtail - 4].GDCLS),
				"CLS3" : BasedMethod.nvl(arrPRINT[nowDtail - 3].GDCLS),
				"CLS4" : BasedMethod.nvl(arrPRINT[nowDtail - 2].GDCLS),
				"CLS5" : BasedMethod.nvl(arrPRINT[nowDtail - 1].GDCLS),
				"GDNM1": BasedMethod.nvl(arrPRINT[nowDtail - 5].GDNM),
				"GDNM2": BasedMethod.nvl(arrPRINT[nowDtail - 4].GDNM),
				"GDNM3": BasedMethod.nvl(arrPRINT[nowDtail - 3].GDNM),
				"GDNM4": BasedMethod.nvl(arrPRINT[nowDtail - 2].GDNM),
				"GDNM5": BasedMethod.nvl(arrPRINT[nowDtail - 1].GDNM),
				"CST1" : BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(arrPRINT[nowDtail - 5].SAL, "1", "0", "0")),
				"CST2" : BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(arrPRINT[nowDtail - 4].SAL, "1", "0", "0")),
				"CST3" : BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(arrPRINT[nowDtail - 3].SAL, "1", "0", "0")),
				"CST4" : BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(arrPRINT[nowDtail - 2].SAL, "1", "0", "0")),
				"CST5" : BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(arrPRINT[nowDtail - 1].SAL, "1", "0", "0")),
				"VPNO1": BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(arrPRINT[nowDtail - 5].SVPNO, "1", "0", "0")),
				"VPNO2": BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(arrPRINT[nowDtail - 4].SVPNO, "1", "0", "0")),
				"VPNO3": BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(arrPRINT[nowDtail - 3].SVPNO, "1", "0", "0")),
				"VPNO4": BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(arrPRINT[nowDtail - 2].SVPNO, "1", "0", "0")),
				"VPNO5": BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(arrPRINT[nowDtail - 1].SVPNO, "1", "0", "0")),
				"AMT1" : BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(arrPRINT[nowDtail - 5].WITHTAX, "1", "0", "0")),
				"AMT2" : BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(arrPRINT[nowDtail - 4].WITHTAX, "1", "0", "0")),
				"AMT3" : BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(arrPRINT[nowDtail - 3].WITHTAX, "1", "0", "0")),
				"AMT4" : BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(arrPRINT[nowDtail - 2].WITHTAX, "1", "0", "0")),
				"AMT5" : BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(arrPRINT[nowDtail - 1].WITHTAX, "1", "0", "0")),
				"(発行枚数)": "1"
			});
		}
		
		private function printMA030X1_Loop(e: ResultEvent=null): void
		{
			// カウントアップ
			currentCount++;
			
			hts.removeEventListener(ResultEvent.RESULT, printMA030X1_Loop);
			hts.removeEventListener(FaultEvent.FAULT, htsFault);
			
			// 印刷成功時処理
			if(e.result.response.result == "OK"){
				// 印刷失敗時 result == NG
			}else{
				singleton.sitemethod.dspJoinErrMsg(e.result.response.message,
					doF10Proc_last, "", "0", null, null, true);
				return;
			}
			
			if(currentCount != reportCount)
				printMA030X1_Body();
			else
				printMA030X1_Futter();
		}
		
		private function printMA030X1_Futter(): void
		{		
			//var hts: HTTPService = new HTTPService();
			var ttl_bal: String;
			var w_bal: String;
			var w_dis: String;
			var w_tax: String;
			
			hts.method = "GET";
			hts.addEventListener(ResultEvent.RESULT, printMA030X1_last, false, 0, true);
			hts.addEventListener(FaultEvent.FAULT, htsFault, false, 0, true);
			hts.url = "http://localhost:8080/Format/Print";
			
			ttl_bal = "前回繰越額";
			w_bal = BasedMethod.calcNum(view.edtMEDIBAL.text,
				BasedMethod.calcNum(view.edtSTOREBAL.text,view.edtDEVBAL.text));
			w_dis = BasedMethod.calcNum(BasedMethod.calcNum(view.edtTWITHTAX.text,w_bal),view.edtTAMT.text,1);
			w_tax = BasedMethod.calcNum(view.edtTAMT.text,BasedMethod.calcNum(view.edtTAMT.text,BasedMethod.calcNum(BasedMethod.calcNum(BasedMethod.nvl(String(getTax_ret.tax), "0"),"100"),"100",3,3),3,0,2),1);
			
			if(BasedMethod.asCurrency(w_bal) == 0){
				ttl_bal = "";
				w_bal = "";
			}
			
			hts.send({
				"__format_archive_url": "file:///sdcard/print/me011x.spfmtz",
				"__format_archive_update": "updateWithoutError",
				"__format_id_number": "3",
				"UAMT"		: singleton.g_customnumberformatter.numberFormat(view.edtTWITHTAX.text, "1", "0", "0"),
				"TTL_BAL"	: ttl_bal,
				"BAL"		: singleton.g_customnumberformatter.numberFormat(w_bal, "1", "0", "0"),
				"DIS"		: singleton.g_customnumberformatter.numberFormat(w_dis, "1", "0", "0"),
				"AMT"		: singleton.g_customnumberformatter.numberFormat(view.edtTAMT.text, "1", "0", "0"),
				//"TAX"		: singleton.g_customnumberformatter.numberFormat(w_tax, "1", "0", "0"),
				"TAX"		: singleton.g_customnumberformatter.numberFormat(g_tax, "1", "0", "0"),
				"NAMT"		: singleton.g_customnumberformatter.numberFormat(view.edtTTNAMT.text, "1", "0", "0"),
				"ZAMT"		: singleton.g_customnumberformatter.numberFormat(BasedMethod.calcNum(view.edtTAMT.text,view.edtTTNAMT.text,1), "1", "0", "0"),
				"BNKNM1"	: arrSEIGYO[0].BNKCD1,
				"BNKSNM1"	: arrSEIGYO[0].BNKSCD1,
				"VBNO1"	: arrSEIGYO[0].VBNO1,
				"BNKMEMO1"	: arrSEIGYO[0].BNKMEMO1,
				"BNKNM2"	: arrSEIGYO[0].BNKCD2,
				"BNKSNM2"	: arrSEIGYO[0].BNKSCD2,
				"VBNO2"	: arrSEIGYO[0].VBNO2,
				"BNKMEMO2"	: arrSEIGYO[0].BNKMEMO2,
				"BNKNM3"	: arrSEIGYO[0].BNKCD3,
				"BNKSNM3"	: arrSEIGYO[0].BNKSCD3,
				"VBNO3"	: arrSEIGYO[0].VBNO3,
				"BNKMEMO3"	: arrSEIGYO[0].BNKMEMO3,
				"TNO": getTNO(),
				"(発行枚数)": "1"
			});
		}
		
		private function printMA030X1_last(e: ResultEvent=null): void
		{
			hts.removeEventListener(ResultEvent.RESULT, printMA030X1_last);
			hts.removeEventListener(FaultEvent.FAULT, htsFault);
			
			// 印刷成功時処理
			if(e.result.response.result == "OK"){
				// 印刷失敗時 result == NG
			}else{
				singleton.sitemethod.dspJoinErrMsg(e.result.response.message,
					doF10Proc_last, "", "0", null, null, true);
				return;
			}
			
			printHeader_arg.func();
		}
		
		/**********************************************************
		 * 帳票作成
		 * @author 12.10.16 SE-233
		 **********************************************************/
		private function printMA030X2_Header(func: Function): void
		{
			//var hts: HTTPService = new HTTPService();

			var w_tknm1: String;
			var w_tknm2: String;
			
			printHeader_arg = new Object;
			printHeader_arg.func = func;
			
			// 明細数セット
			currentCount = 0;
			g_tax = '0';
			
			if(reportCount == 0){
				printMA030X2_last();
				return;
			}
			
			var tknm: Object = BasedMethod.getTKNM(view.data.TKNM);
			
			hts.method = "GET";
			hts.addEventListener(ResultEvent.RESULT, printMA030X2_Body, false, 0, true);
			hts.addEventListener(FaultEvent.FAULT, htsFault, false, 0, true);
			hts.url = "http://localhost:8080/Format/Print";
			
			// 15.03.19 SE-233 start
			// 15.01.16 SE-233 start
			hts.send({
				"__format_archive_url": "file:///sdcard/print/me015x.spfmtz",
				"__format_archive_update": "updateWithoutError",
				"__format_id_number": "1",
				"TKCD": g_tkcd,
				"TKNM1": tknm.tknm1,
				"TKNM2": tknm.tknm2,
				// 12.11.06 SE-233 Redmine#3628 対応 start
				"HDAT": getEra_ret.dat + " " + g_time,
				// 12.11.06 SE-233 Redmine#3628 対応 end
				"VNO": g_vseq,
				"CHGNM": arrCHGCD2[0].CHGNM,
				"HED1": arrSEIGYO[0].COSNM1,
				"HED2": arrSEIGYO[0].COSNM2,
				"HED3": arrSEIGYO[0].COSNM3,
				"ADR1": arrSEIGYO[0].ADD1,
				"ADR2": arrSEIGYO[0].ADD2,
				"ADR3": arrSEIGYO[0].ADD3,
				"TEL1": arrSEIGYO[0].TEL1,
				"TEL2": arrSEIGYO[0].TEL2,
				"TEL3": arrSEIGYO[0].TEL3,
				"(発行枚数)": "1"
			});
		}
		
		private function printMA030X2_Body(e: ResultEvent=null): void
		{
			hts.removeEventListener(ResultEvent.RESULT, printMA030X2_Body);
			hts.removeEventListener(FaultEvent.FAULT, htsFault);
			
			if(e != null){
				// 印刷成功時処理
				if(e.result.response.result == "OK"){
					// 印刷失敗時 result == NG
				}else{
					singleton.sitemethod.dspJoinErrMsg(e.result.response.message,
						doF10Proc_last, "", "0", null, null, true);
					return;
				}
			}
			
			//var hts: HTTPService = new HTTPService();
			var nowDtail: int;
			
			hts.method = "GET";
			hts.addEventListener(ResultEvent.RESULT, printMA030X2_Loop, false, 0, true);
			hts.addEventListener(FaultEvent.FAULT, htsFault, false, 0, true);
			hts.url = "http://localhost:8080/Format/Print";
			
			nowDtail = (currentCount + 1) * 5;
			
			// 12.10.03 SE-362 明細がない場合は行NOを印字しない start
			var w_vlin1: String;
			var w_vlin2: String;
			var w_vlin3: String;
			var w_vlin4: String;
			var w_vlin5: String;
			
			if(BasedMethod.nvl(arrPRINT[nowDtail - 5].TKCD) != ""){
				w_vlin1 = (nowDtail - 4).toString();
				g_tax = BasedMethod.calcNum(g_tax, arrPRINT[nowDtail - 5].TAX);
			}else{
				w_vlin1 = "";
			}
			if(BasedMethod.nvl(arrPRINT[nowDtail - 4].TKCD) != ""){
				w_vlin2 = (nowDtail - 3).toString();
				g_tax = BasedMethod.calcNum(g_tax, arrPRINT[nowDtail - 4].TAX);
			}else{
				w_vlin2 = "";
			}
			if(BasedMethod.nvl(arrPRINT[nowDtail - 3].TKCD) != ""){
				w_vlin3 = (nowDtail - 2).toString();
				g_tax = BasedMethod.calcNum(g_tax, arrPRINT[nowDtail - 3].TAX);
			}else{
				w_vlin3 = "";
			}
			if(BasedMethod.nvl(arrPRINT[nowDtail - 2].TKCD) != ""){
				w_vlin4 = (nowDtail - 1).toString();
				g_tax = BasedMethod.calcNum(g_tax, arrPRINT[nowDtail - 2].TAX);
			}else{
				w_vlin4 = "";
			}
			if(BasedMethod.nvl(arrPRINT[nowDtail - 1].TKCD) != ""){
				w_vlin5 = (nowDtail).toString();
				g_tax = BasedMethod.calcNum(g_tax, arrPRINT[nowDtail - 1].TAX);
			}else{
				w_vlin5 = "";
			}
			// 12.10.03 SE-362 明細がない場合は行NOを印字しない end			
			
			hts.send({
				"__format_archive_url": "file:///sdcard/print/me015x.spfmtz",
				"__format_archive_update": "updateWithoutError",
				"__format_id_number": "2",
				"VLIN1": w_vlin1,
				"VLIN2": w_vlin2,
				"VLIN3": w_vlin3,
				"VLIN4": w_vlin4,
				"VLIN5": w_vlin5,
				"CLS1" : BasedMethod.nvl(arrPRINT[nowDtail - 5].GDCLS),
				"CLS2" : BasedMethod.nvl(arrPRINT[nowDtail - 4].GDCLS),
				"CLS3" : BasedMethod.nvl(arrPRINT[nowDtail - 3].GDCLS),
				"CLS4" : BasedMethod.nvl(arrPRINT[nowDtail - 2].GDCLS),
				"CLS5" : BasedMethod.nvl(arrPRINT[nowDtail - 1].GDCLS),
				"GDNM1": BasedMethod.nvl(arrPRINT[nowDtail - 5].GDNM),
				"GDNM2": BasedMethod.nvl(arrPRINT[nowDtail - 4].GDNM),
				"GDNM3": BasedMethod.nvl(arrPRINT[nowDtail - 3].GDNM),
				"GDNM4": BasedMethod.nvl(arrPRINT[nowDtail - 2].GDNM),
				"GDNM5": BasedMethod.nvl(arrPRINT[nowDtail - 1].GDNM),
				"CST1" : BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(arrPRINT[nowDtail - 5].SAL, "1", "0", "0")),
				"CST2" : BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(arrPRINT[nowDtail - 4].SAL, "1", "0", "0")),
				"CST3" : BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(arrPRINT[nowDtail - 3].SAL, "1", "0", "0")),
				"CST4" : BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(arrPRINT[nowDtail - 2].SAL, "1", "0", "0")),
				"CST5" : BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(arrPRINT[nowDtail - 1].SAL, "1", "0", "0")),
				"VPNO1": BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(arrPRINT[nowDtail - 5].SVPNO, "1", "0", "0")),
				"VPNO2": BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(arrPRINT[nowDtail - 4].SVPNO, "1", "0", "0")),
				"VPNO3": BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(arrPRINT[nowDtail - 3].SVPNO, "1", "0", "0")),
				"VPNO4": BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(arrPRINT[nowDtail - 2].SVPNO, "1", "0", "0")),
				"VPNO5": BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(arrPRINT[nowDtail - 1].SVPNO, "1", "0", "0")),
				"AMT1" : BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(arrPRINT[nowDtail - 5].WITHTAX, "1", "0", "0")),
				"AMT2" : BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(arrPRINT[nowDtail - 4].WITHTAX, "1", "0", "0")),
				"AMT3" : BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(arrPRINT[nowDtail - 3].WITHTAX, "1", "0", "0")),
				"AMT4" : BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(arrPRINT[nowDtail - 2].WITHTAX, "1", "0", "0")),
				"AMT5" : BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(arrPRINT[nowDtail - 1].WITHTAX, "1", "0", "0")),
				"(発行枚数)": "1"
			});
		}
		
		private function printMA030X2_Loop(e: ResultEvent=null): void
		{
			hts.removeEventListener(ResultEvent.RESULT, printMA030X2_Loop);
			hts.removeEventListener(FaultEvent.FAULT, htsFault);
			
			// カウントアップ
			currentCount++;
			
			// 印刷成功時処理
			if(e.result.response.result == "OK"){
				// 印刷失敗時 result == NG
			}else{
				singleton.sitemethod.dspJoinErrMsg(e.result.response.message,
					doF10Proc_last, "", "0", null, null, true);
				return;
			}
			
			if(currentCount != reportCount)
				printMA030X2_Body();
			else
				printMA030X2_Futter();
		}
		
		private function printMA030X2_Futter(): void
		{
			//var hts: HTTPService = new HTTPService();
			var ttl_bal: String;
			var w_bal: String;
			var w_dis: String;
			var w_tax: String;
			
			hts.method = "GET";
			hts.addEventListener(ResultEvent.RESULT, printMA030X2_last, false, 0, true);
			hts.addEventListener(FaultEvent.FAULT, htsFault, false, 0, true);
			hts.url = "http://localhost:8080/Format/Print";
			
			ttl_bal = "前回繰越額";
			w_bal = BasedMethod.calcNum(view.edtMEDIBAL.text,
				BasedMethod.calcNum(view.edtSTOREBAL.text,view.edtDEVBAL.text));
			w_dis = BasedMethod.calcNum(BasedMethod.calcNum(view.edtTWITHTAX.text,w_bal),view.edtTAMT.text,1);
			w_tax = BasedMethod.calcNum(view.edtTAMT.text,BasedMethod.calcNum(view.edtTAMT.text,BasedMethod.calcNum(BasedMethod.calcNum(BasedMethod.nvl(String(getTax_ret.tax), "0"),"100"),"100",3,3),3,0,2),1);
			
			if(BasedMethod.asCurrency(w_bal) == 0){
				ttl_bal = "";
				w_bal = "";
			}
			
			hts.send({
				"__format_archive_url": "file:///sdcard/print/me015x.spfmtz",
				"__format_archive_update": "updateWithoutError",
				"__format_id_number": "3",
				"UAMT"		: singleton.g_customnumberformatter.numberFormat(view.edtTWITHTAX.text, "1", "0", "0"),
				"TTL_BAL"	: ttl_bal,
				"BAL"		: singleton.g_customnumberformatter.numberFormat(w_bal, "1", "0", "0"),
				"DIS"		: singleton.g_customnumberformatter.numberFormat(w_dis, "1", "0", "0"),
				"AMT"		: singleton.g_customnumberformatter.numberFormat(view.edtTAMT.text, "1", "0", "0"),
				//"TAX"		: singleton.g_customnumberformatter.numberFormat(w_tax, "1", "0", "0"),
				"TAX"		: singleton.g_customnumberformatter.numberFormat(g_tax, "1", "0", "0"),
				"NAMT"		: singleton.g_customnumberformatter.numberFormat(view.edtTTNAMT.text, "1", "0", "0"),
				"ZAMT"		: singleton.g_customnumberformatter.numberFormat(BasedMethod.calcNum(view.edtTAMT.text,view.edtTTNAMT.text,1), "1", "0", "0"),
				"BNKNM1"	: arrSEIGYO[0].BNKCD1,
				"BNKSNM1"	: arrSEIGYO[0].BNKSCD1,
				"VBNO1"	: arrSEIGYO[0].VBNO1,
				"BNKMEMO1"	: arrSEIGYO[0].BNKMEMO1,
				"BNKNM2"	: arrSEIGYO[0].BNKCD2,
				"BNKSNM2"	: arrSEIGYO[0].BNKSCD2,
				"VBNO2"	: arrSEIGYO[0].VBNO2,
				"BNKMEMO2"	: arrSEIGYO[0].BNKMEMO2,
				"BNKNM3"	: arrSEIGYO[0].BNKCD3,
				"BNKSNM3"	: arrSEIGYO[0].BNKSCD3,
				"VBNO3"	: arrSEIGYO[0].VBNO3,
				"BNKMEMO3"	: arrSEIGYO[0].BNKMEMO3,
				"TNO": getTNO(),
				"(発行枚数)": "1"
			});
		}
		
		private function printMA030X2_last(e: ResultEvent=null): void
		{
			hts.removeEventListener(ResultEvent.RESULT, printMA030X2_last);
			hts.removeEventListener(FaultEvent.FAULT, htsFault);
			
			// 印刷成功時処理
			if(e.result.response.result == "OK"){
				// 印刷失敗時 result == NG
			}else{
				singleton.sitemethod.dspJoinErrMsg(e.result.response.message,
					doF10Proc_last, "", "0", null, null, true);
				return;
			}
			
			printHeader_arg.func();
		}
		
		private function htsFault(e: FaultEvent): void
		{
			//IEventDispatcher(e.currentTarget).removeEventListener(FaultEvent.FAULT, htsFault);				
			singleton.sitemethod.dspJoinErrMsg("印刷に失敗しました。サーバを起動してから再度実行して下さい。",
				doF10Proc_last, "", "0", null, null, true);
		}
	}
}