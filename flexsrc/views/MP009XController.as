package views
{
	import flash.events.Event;
	import flash.events.GeolocationEvent;
	import flash.events.KeyboardEvent;
	import flash.events.LocationChangeEvent;
	import flash.events.SQLErrorEvent;
	import flash.events.SQLEvent;
	import flash.geom.Rectangle;
	import flash.media.StageWebView;
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	import flash.sensors.Geolocation;
	import flash.ui.Keyboard;
	
	import modules.base.BasedMethod;
	import modules.custom.CustomIterator;
	import modules.custom.sub.Iterator;
	import modules.sbase.SiteModule;
	
	import mx.collections.ArrayCollection;
	import mx.managers.PopUpManager;
	
	public class MP009XController extends SiteModule
	{
		private var view:MP009X;
		
		public var argABSENT: String;				// 地区
		public var argABTIME1: String;				// 不在時間1
		public var argABTIME2: String;				// 不在時間2
		public var argABTIME3: String;				// 不在時間3
		
		public var okflg: Boolean;
		
		//private var stmt: SQLStatement;
		
		public function MP009XController()
		{
			super();
		}
		
		override public function init():void
		{
			// フォーム宣言
			view = _view as MP009X;
			
			//view.rdgABSENT.selectedValue = argABSENT;
			
			// 訪問済みであれば変更できないようにする
			if (argABSENT == "2")
			{
				view.rdb1.enabled = false;
				view.rdb0.enabled = false;
			}
			else
			{
				view.rdb1.enabled = true;
				view.rdb0.enabled = true;
			}

			// 訪問を初期値にする
			if(argABSENT=="0"){
				view.rdgABSENT.selectedValue = "2";
			}else{
				view.rdgABSENT.selectedValue = argABSENT;
			}

			// 不在履歴
			view.lblABTIME1.text = argABTIME1;
			view.lblABTIME2.text = argABTIME2;
			view.lblABTIME3.text = argABTIME3;
			
			// 変数の設定
			//g_prgid     = "MENUX";
			super.init();
		}
		
		override public function getComboData():void
		{
			super.getComboData();
		}
		
		override public function formShow(): void
		{
			doDSPProc();
		}
		

		override public function setDispObject():void
		{
			super.setDispObject();
		}
		
		/**********************************************************
		 * Backキー押下時処理.
		 * @author 12.08.22 SE-354
		 **********************************************************/
		protected function onKeyDownHandler(e:KeyboardEvent):void
		{
			e.preventDefault();
			
			btnCANCELClick();
		}

		/**********************************************************
		 * btnOK クリック時処理.
		 * @author 12.08.22 SE-354
		 **********************************************************/
		public function btnOKClick(): void
		{
			okflg = true;
			
			argABSENT = view.rdgABSENT.selectedValue.toString();
			/*
			if (singleton.view is MA009X)
			{
				trace("up");
				var ma009x: MA009X = singleton.view as MA009X;
				ma009x.ctrl.arrMA009X[ma009x.ctrl.w_index].ABSENT = argABSENT;
			}*/

			PopUpManager.removePopUp(view);
		}
		
		/**********************************************************
		 * btnCANCEL クリック時処理.
		 * @author 12.08.22 SE-354
		 **********************************************************/
		public function btnCANCELClick(): void
		{
			okflg = false;

			PopUpManager.removePopUp(view);
		}
	}
}