package views
{
	
	import flash.data.SQLResult;
	import flash.data.SQLStatement;
	import flash.events.SQLErrorEvent;
	import flash.events.SQLEvent;
	
	import modules.base.BasedMethod;
	import modules.base.SRV;
	import modules.custom.CustomIterator;
	import modules.custom.sub.Iterator;
	import modules.dto.LM007;
	import modules.dto.LS001;
	import modules.dto.HANYOU;
	import modules.sbase.SiteModule;
	
	import mx.collections.ArrayCollection;
	
	public class UB910XController extends SiteModule
	{
		private var view:UB910X;
		private var arrUB910X: ArrayCollection;
		private var arrCHGCD: ArrayCollection;
		private var arrHTCHGCD: ArrayCollection;
		private var stmt: SQLStatement;
		
		private var isNew: Boolean;
		
		public function UB910XController()
		{
			super();
		}
		
		override public function init():void
		{
			super.init();
			
			// フォーム宣言
			view = _view as UB910X;

			// 変数の設定
			//g_prgid     = "MENUX";
			
			cdsCHGCD();
		}
		
		/**********************************************************
		 * openDataSet_H データの取得.
		 * @author 12.08.30 SE-354
		 **********************************************************/
		override public function openDataSet_H(): void
		{
			cdsUB910X();
		}

		/**********************************************************
		 * setDispObject 画面項目セット.
		 * @author 12.08.30 SE-354
		 **********************************************************/
		override public function setDispObject(): void
		{

		}
		
		/**********************************************************
		 * 端末IDフォーカス喪失処理.
		 * @author 12.07.02 SE-354
		 **********************************************************/ 
		public function edtHTIDExit(): void
		{
			
		}
		
		/**********************************************************
		 * 担当者コードフォーカス喪失処理.
		 * @author 12.08.10 SE-354
		 **********************************************************/ 
		public function edtCHGCDExit(): void
		{
			
		}
		
		/**********************************************************
		 * btnDEC クリック時処理.
		 * @author 12.07.03 SE-354
		 **********************************************************/
		public function btnDECClick(): void
		{
			// 入力チェック
			if(view.ls001.HTID == ""){
				singleton.sitemethod.dspJoinErrMsg("E0612");
				return;
			}
			if(view.lcmbCHGNM.selectedItem == null){
				singleton.sitemethod.dspJoinErrMsg("E0613");
				return;
			}
			if(view.lcmbHTCHGNM.selectedItem == null){
				singleton.sitemethod.dspJoinErrMsg("E0614");
				return;
			}
// 13.11.22 SE-233 start
			if(view.ls001.HTID.length != 2){
				singleton.sitemethod.dspJoinErrMsg("E0616");
				return;
			}
// 13.11.22 SE-233 end
			
			checkDataSet();
		}
		
		override public function updateDataSet_H(): void
		{
			// 確定処理
			updateUB910X();
			
			insLF013("","マスタメンテ確定");

			// XXXXXXXが登録されました。
			singleton.sitemethod.dspJoinErrMsg("I0004", super.updateDataSet_H, "", "0", ["制御マスタ"]);
		}
		
		/**********************************************************
		 * btnRTN クリック時処理.
		 * @author 12.07.03 SE-354
		 **********************************************************/
		public function btnRTNClick(): void
		{
			// 制御マスタメンテを終了します。よろしいですか？
			//singleton.sitemethod.dspJoinErrMsg();
			view.navigator.popView();
		}
		
		public function btnRTNClick_last(): void
		{
			// メニューに戻る
			view.navigator.popView();
		}
		
		/**********************************************************
		 * SQLiteのselect
		 * @author 12.08.30 SE-354
		 **********************************************************/ 
		public function cdsCHGCD(): void
		{
			// 変数の初期化
			arrCHGCD = null;
			
			stmt = new SQLStatement();
			//stmt.sqlConnection =  connection;
			stmt.sqlConnection =  singleton.sqlconnection;
// 15.11.18 SE-233 start
//			stmt.itemClass = LM007;
			stmt.itemClass = HANYOU;
// 15.11.18 SE-233 end
			//stmt.text= "SELECT * FROM とくいさき WHERE HDAT=? AND AREA=?";
			//stmt.text= "SELECT * FROM LM010 WHERE TKAREA='01' AND HPYM='2012/06' AND TKCD LIKE ";
			//stmt.text= "SELECT * FROM LM010 WHERE ROWID <100";
// 15.11.18 SE-233 start
//			stmt.text= singleton.g_query_xml.entry.(@key == "qryCHGCD");
			stmt.text= singleton.g_query_xml.entry.(@key == "qryCHGCD3");
// 15.11.18 SE-233 end
			
			//stmt.parameters[0] = p_param1;
			//stmt.parameters[1] = p_param2;
			
			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, cdsCHGCDResult, false, 0, true);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler, false, 0, true);
			
			//実行
			stmt.execute();
		}
		
		private function cdsCHGCDResult(event:SQLEvent):void 
		{
			//データの取得に成功したら
			//getResult()でSQLResultを取得し
			//取得したデータをデータグリッドに反映します。
			//status += "  データグリッド更新";
			var result:SQLResult = stmt.getResult();
			
			stmt.removeEventListener(SQLEvent.RESULT, cdsCHGCDResult);				
			stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);				
			stmt=null;
			
			arrCHGCD = new ArrayCollection(result.data);
			
			view.lcmbCHGNM.dataProvider = arrCHGCD;
			
			//view.grp1.dataProvider = arrMA020X;
			
			//execSetPLSQL();
			cdsHTCHGCD();
		}
		
		/**********************************************************
		 * SQLiteのselect
		 * @author 12.11.16 SE-300
		 **********************************************************/ 
		public function cdsHTCHGCD(): void
		{
			// 変数の初期化
			arrHTCHGCD = null;
			
			stmt = new SQLStatement();
			//stmt.sqlConnection =  connection;
			stmt.sqlConnection =  singleton.sqlconnection;
// 15.11.18 SE-233 start
//			stmt.itemClass = LM007;
			stmt.itemClass = HANYOU;
// 15.11.18 SE-233 end
			//stmt.text= "SELECT * FROM とくいさき WHERE HDAT=? AND AREA=?";
			//stmt.text= "SELECT * FROM LM010 WHERE TKAREA='01' AND HPYM='2012/06' AND TKCD LIKE ";
			//stmt.text= "SELECT * FROM LM010 WHERE ROWID <100";
// 15.11.18 SE-233 start
//			stmt.text= singleton.g_query_xml.entry.(@key == "qryCHGCD");
			stmt.text= singleton.g_query_xml.entry.(@key == "qryCHGCD3");
// 15.11.18 SE-233 end
			
			//stmt.parameters[0] = p_param1;
			//stmt.parameters[1] = p_param2;
			
			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, cdsHTCHGCDResult, false, 0, true);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler, false, 0, true);
			
			//実行
			stmt.execute();
		}
		
		private function cdsHTCHGCDResult(event:SQLEvent):void 
		{
			//データの取得に成功したら
			//getResult()でSQLResultを取得し
			//取得したデータをデータグリッドに反映します。
			//status += "  データグリッド更新";
			var result:SQLResult = stmt.getResult();
			
			stmt.removeEventListener(SQLEvent.RESULT, cdsHTCHGCDResult);				
			stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);				
			stmt=null;
			
			arrHTCHGCD = new ArrayCollection(result.data);
			
			view.lcmbHTCHGNM.dataProvider = arrHTCHGCD;
			
			//view.grp1.dataProvider = arrMA020X;
			
			execSetPLSQL();
		}
		
		
		/**********************************************************
		 * SQLiteのselect
		 * @author 12.08.30 SE-354
		 **********************************************************/ 
		public function cdsUB910X(nextfunc: Function=null): void
		{
			// 変数の初期化
			arrUB910X = null;
			
			stmt = new SQLStatement();
			//stmt.sqlConnection =  connection;
			stmt.sqlConnection =  singleton.sqlconnection;
			stmt.itemClass = LS001;
			//stmt.text= "SELECT * FROM とくいさき WHERE HDAT=? AND AREA=?";
			//stmt.text= "SELECT * FROM LM010 WHERE TKAREA='01' AND HPYM='2012/06' AND TKCD LIKE ";
			//stmt.text= "SELECT * FROM LM010 WHERE ROWID <100";
			stmt.text= singleton.g_query_xml.entry.(@key == "qryUB910X");
			
			//stmt.parameters[0] = p_param1;
			//stmt.parameters[1] = p_param2;
			
			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, cdsUB910XResult, false, 0, true);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler, false, 0, true);
			
			//実行
			stmt.execute();
		}
		
		private function cdsUB910XResult(event:SQLEvent):void 
		{
			//データの取得に成功したら
			//getResult()でSQLResultを取得し
			//取得したデータをデータグリッドに反映します。
			//status += "  データグリッド更新";
			var result:SQLResult = stmt.getResult();
			
			stmt.removeEventListener(SQLEvent.RESULT, cdsUB910XResult);				
			stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);				
			stmt=null;
			
			if (result.data != null)
			{
				// 修正
				isNew = false;
				
				arrUB910X = new ArrayCollection(result.data);
				
				//view.ls001 = arrUB910X[0];
				view.ls001 = arrUB910X[0];
				
				// コンボボックスの初期表示
				//BasedMethod
				//view.ls001.CHGCD;
				//var srv: SRV = new SRV("a");
				//view.lcmbCHGNM.
				//view.grp1.dataProvider = arrMA020X;
				
				//view.lcmbCHGNM
				
// 15.11.18 SE-233 start
//				var w_index:int = BasedMethod.getArrayIndex(arrCHGCD, "CHGCD", view.ls001.CHGCD); 
				var w_index:int = BasedMethod.getArrayIndex(arrCHGCD, "CODE", view.ls001.CHGCD); 
// 15.11.18 SE-233 end
				if (w_index != -1)
				{
					view.lcmbCHGNM.selectedItem = arrCHGCD[w_index];
				}

// 15.11.18 SE-233 start
//				w_index = BasedMethod.getArrayIndex(arrHTCHGCD, "CHGCD", view.ls001.HTCHGCD); 
				w_index = BasedMethod.getArrayIndex(arrHTCHGCD, "CODE", view.ls001.HTCHGCD); 
// 15.11.18 SE-233 end
				if (w_index != -1)
				{
					view.lcmbHTCHGNM.selectedItem = arrHTCHGCD[w_index];
				}

				view.edtHVNO.text = view.ls001.HVNO.substr(3,3);
				view.edtBVNO.text = view.ls001.BVNO.substr(3,3);
				view.edtNVNO.text = view.ls001.NVNO.substr(3,3);
			}
			else
			{
				// 新規モード
				isNew = true;
				
				arrUB910X = new ArrayCollection();
				arrUB910X.addItem(new LS001());
//				arrUB910X.refresh();
				
				view.ls001 = arrUB910X[0];
			}
		}
		
		protected function stmtErrorHandler(event:SQLErrorEvent):void
		{
			// エラーの表示
			trace("SQLerr");
		}
		
		public function updateUB910X(): void
		{
			stmt = new SQLStatement();
			//stmt.sqlConnection =  connection;
			stmt.sqlConnection =  singleton.sqlconnection;
			//stmt.itemClass = LS001;
			
			// 修正時はUPDATE
			if (isNew == false)
			{ 
				stmt.text= singleton.g_query_xml.entry.(@key == "updUB910X");
				
				stmt.parameters[0] = view.ls001.HTID;
				//stmt.parameters[2] = view.ls001.CHGCD;
// 15.11.18 SE-233 start
//				stmt.parameters[1] = view.lcmbCHGNM.selectedItem.CHGCD;
				stmt.parameters[1] = view.lcmbCHGNM.selectedItem.CODE;
// 15.11.18 SE-233 start
				//stmt.parameters[2] = view.ls001.SDDAY;
				stmt.parameters[2] = view.ls001.HVNO;
				stmt.parameters[3] = view.ls001.BVNO;
				stmt.parameters[4] = view.ls001.NVNO;
// 15.11.18 SE-233 start
//				stmt.parameters[5] = view.lcmbHTCHGNM.selectedItem.CHGCD;
				stmt.parameters[5] = view.lcmbHTCHGNM.selectedItem.CODE;
// 15.11.18 SE-233 end
			}
			// 新規時はINSERT
			else
			{
				stmt.text= singleton.g_query_xml.entry.(@key == "insUB910X");
				
				stmt.parameters[0] = "0";
				stmt.parameters[1] = view.ls001.HTID;
				//stmt.parameters[2] = view.ls001.CHGCD;
// 15.11.18 SE-233 start
//				stmt.parameters[2] = view.lcmbCHGNM.selectedItem.CHGCD;
				stmt.parameters[2] = view.lcmbCHGNM.selectedItem.CODE;
// 15.11.18 SE-233 end
				//stmt.parameters[3] = view.ls001.SDDAY;
				stmt.parameters[3] = view.ls001.HVNO;
				stmt.parameters[4] = view.ls001.BVNO;
				stmt.parameters[5] = view.ls001.NVNO;
// 15.11.18 SE-233 start
//				stmt.parameters[6] = view.lcmbHTCHGNM.selectedItem.CHGCD;
				stmt.parameters[6] = view.lcmbHTCHGNM.selectedItem.CODE;
// 15.11.18 SE-233 end
			}
			
			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, updateUB910XResult, false, 0, true);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler, false, 0, true);
			
			//実行
			stmt.execute();
		}
		
		private function updateUB910XResult(event:SQLEvent):void 
		{
			stmt.removeEventListener(SQLEvent.RESULT, updateUB910XResult);				
			stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);				
			stmt=null;
			
			//super.updateDataSet_H();
			cdsSEIGYO(super.updateDataSet_H);
		}
	}
}