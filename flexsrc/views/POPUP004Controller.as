package views
{
	import flash.data.SQLResult;
	import flash.data.SQLStatement;
	import flash.display.Loader;
	import flash.display.Stage;
	import flash.events.Event;
	import flash.events.GeolocationEvent;
	import flash.events.IOErrorEvent;
	import flash.events.KeyboardEvent;
	import flash.events.LocationChangeEvent;
	import flash.events.SQLErrorEvent;
	import flash.events.SQLEvent;
	import flash.filesystem.File;
	import flash.geom.Rectangle;
	import flash.media.StageWebView;
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	import flash.sensors.Geolocation;
	import flash.ui.Keyboard;
	
	import modules.custom.CustomIterator;
	import modules.custom.sub.Iterator;
	import modules.dto.LM009;
	import modules.dto.LM010;
	import modules.sbase.SiteConst;
	import modules.sbase.SiteModule;
	
	import mx.collections.ArrayCollection;
	import mx.core.UIComponent;
	import mx.managers.PopUpManager;
	import mx.states.AddChild;
	
	public class POPUP004Controller extends SiteModule
	{
		private var view:POPUP004;
		private var loader: Loader;
		private var image: File;

		public var argTKCD: String;
		
		public function POPUP004Controller()
		{
			super();
		}
		
		override public function init():void
		{
			// フォーム宣言
			view = _view as POPUP004;

			super.init();			
		}
		
		override public function formShow_last():void
		{
			super.formShow_last();
		
			var uic: UIComponent = new UIComponent;

			loader = new Loader();
			loader.contentLoaderInfo.addEventListener(Event.COMPLETE, onLoadCompleted);
			loader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, onIOError);
//			view.imageCont.addChild(loader);
			
			uic.addChild(loader);
			view.imageCont.addElement(uic);
			
			
			// 画像表示処理
			doDSPProc();
		}

		override public function setDispObject():void
		{
			var w_extend: String = ".jpg";

			// アプリケーションディレクトリ内のimagesディレクトリ
			image = File.documentsDirectory.resolvePath(SiteConst.G_IMAGE_PATH2 + argTKCD + w_extend);
			
			// imagesディレクトリ内にあるファイルを調べる
//			var imageList:Array = imageD.getDirectoryListing();

			try{
				// 画像データの読み込み
				if(image){
					image.load();
					image.addEventListener(Event.COMPLETE, loadComp);
					image.addEventListener(IOErrorEvent.IO_ERROR, error);
				}else{
					singleton.sitemethod.dspJoinErrMsg("W0001", super.setDispObject);
					return;
				}
					
//				loader.load(new URLRequest(SiteConst.G_IMAGE_PATH2 + argTKCD + ".jpg"));
				// 画像表示 
			}catch(err: Error){
				trace("エラー : " + err.message);
			}
	
			super.setDispObject();
		}

		// 画像を表示
		private function loadComp(evt:Event):void
		{
			loader.loadBytes(image.data);
		}
		
		private function error(e: IOErrorEvent): void
		{
			singleton.sitemethod.dspJoinErrMsg("W0001", btnCLOSEClick);
		}

		private function onLoadCompleted(evt:Event):void
		{
			loader.x = 0;
			loader.y = 0;
			loader.width = view.imageCont.width;
			loader.height = view.imageCont.height;
		}
		
		private function onIOError(evt:IOErrorEvent):void
		{
			trace("エラー : ");
		}
		
		/**********************************************************
		 * btnCLOSE クリック時処理.
		 * @author 12.09.19 SE-233
		 **********************************************************/
		public function btnCLOSEClick(): void
		{
			PopUpManager.removePopUp(view);
		}
	}
}