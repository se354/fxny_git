package views
{
	import flash.data.SQLConnection;
	import flash.data.SQLResult;
	import flash.data.SQLStatement;
	import flash.display.DisplayObjectContainer;
	import flash.events.KeyboardEvent;
	import flash.events.SQLErrorEvent;
	import flash.events.SQLEvent;
	import flash.events.IEventDispatcher;
	import flash.ui.Keyboard;
	
	import modules.base.BasedMethod;
	import modules.custom.AlertDialog;
	import modules.custom.CustomIterator;
	import modules.custom.sub.Iterator;
	import modules.dto.MA041V01;
	import modules.dto.W_MA040X;
	import modules.dto.W_MA041X;
	import modules.dto.LJ001;
	import modules.formatters.CustomNumberFormatter;
	import modules.sbase.SiteModule;
	
	import mx.collections.ArrayCollection;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	import mx.rpc.http.HTTPService;
	
	/**********************************************************
	 * MA041X 配置薬入替(入金)
	 * @author 12.09.06 SE-362
	 **********************************************************/
	public class MA041XController extends SiteModule
	{
		private var view:MA041X;
		private var arrMA041X1: ArrayCollection;
		private var arrMA041X2: ArrayCollection;
		private var arrMA040X1: ArrayCollection; // ヘッダ
		
		private var g_cucd: String;			// 客先コード
		// 12.10.31 SE-233 Redmine#3578 対応 start
		private var g_date: String;			// 訪問日付
		// 12.10.31 SE-233 Redmine#3578 対応 start
		private var g_time: String;			// 訪問時間
		private var g_vseq: String;			// 伝票NO
		private var g_tno: String;			// 通しNO
		private var g_tktel: String;			// 得意先電話番号
		private var modifyFlag: int;			// 修正フラグ
		private var g_erahdat: String;		// 和暦訪問日付
		private var g_eralhdat: String;		// 和暦前回訪問日付
		private var g_sysdate: String;		// システム日付

		private var stmt: SQLStatement;
		
		// 帳票用配列
		private var arrPRINT: ArrayCollection;
		
		// 帳票用変数
		private var reportCount: int=0;				// 帳票明細数
		private var currentCount: int=0;				// 現在印刷明細番号

		// 引数情報
		private var cdsMA041X_arg: Object;
		private var cdsMA040X1_arg: Object;
		private var cdsMA040X2_arg: Object;
		private var print_arg: Object;

		public function MA041XController()
		{
			super();
		}
		
		/**********************************************************
		 * 初期処理
		 * @author  12.09.06 SE-362
		 **********************************************************/
		override public function init():void
		{
			// フォーム宣言
			view = _view as MA041X;

			// 変数設定
			g_cucd = String(view.data.TKCD);
			g_tktel = String(view.data.TKTEL);
			modifyFlag = int(view.navigator.context.modifyFlag);
			// 12.10.31 SE-233 Redmine#3578 対応 start
			g_date = String(view.navigator.context.date);
			g_time = String(view.navigator.context.time);
			// 12.10.31 SE-233 Redmine#3578 対応 start
			// 12.11.06 SE-233 Redmine#3628 対応 start
			getEra(g_date);
			// 12.11.06 SE-233 Redmine#3628 対応 start
			
			// 初期表示設定
			view.edtCUNM.text = String(view.data.TKNM);

			// ボタンに色を付ける
			view.btnSUM.setStyle("chromeColor", 0x55aaff);
			
			// 12.10.12 SE-362 Redmine#3435 対応 start
			//view.stage.addEventListener("keyDown", keyDownBack, false, 1);
			view.stage.addEventListener(KeyboardEvent.KEY_DOWN, onKeyDownHandler, false, 0, true);
			// 12.10.12 SE-362 Redmine#3435 対応 end
			super.init();
			
		}

		/**********************************************************
		 * 画面立ち上げ処理
		 * @author  12.09.06 SE-362
		 **********************************************************/
		override public function formShow_last():void
		{
			super.formShow_last();
			
			// 画面データ表示
			doDSPProc();
		}

		/**********************************************************
		 * ヘッダデータ取得
		 * @author  12.09.06 SE-362
		 **********************************************************/
		override public function openDataSet_H(): void
		{
			// 制御ﾏｽﾀ情報取得
			cdsSEIGYO(openDataSet_H_cld1);
		}
		
		private function openDataSet_H_cld1(): void
		{
			// 入金訂正時　新規  OR 修正判定
			chkMA040X1(openDataSet_H_cld2);
		}
		
		private function openDataSet_H_cld2(): void
		{
			if(modifyFlag == 2){
				delMA040X1(openDataSet_H_cld21);
			}else{
				openDataSet_H_cld3();
			}
		}

		private function openDataSet_H_cld21(): void
		{
			wdsMA040X1(openDataSet_H_cld3);
		}
		
		private function openDataSet_H_cld3(): void
		{
//			cdsMA041X1(openDataSet_H_last);
			cdsMA041X1(openDataSet_H_cld4);
		}
		
//		private function openDataSet_H_last(): void
		private function openDataSet_H_cld4(): void
		{
			// 担当者名取得
			// 12.10.11 SE-362 Redmine#3423 対応 start
//			cdsCHGCD2(arrSEIGYO[0].CHGCD, super.openDataSet_H);
//			cdsCHGCD2(view.data.TKCHGCD, super.openDataSet_H);
			// 12.10.11 SE-362 Redmine#3423 対応 end
			cdsCHGCD2(view.data.TKCHGCD, openDataSet_H_cld5);
		}
		private function openDataSet_H_cld5(): void
		{
			// 訪問日和暦変換
			getEra(g_date, openDataSet_H_cld6);
		}
		private function openDataSet_H_cld6(): void
		{
			// 和暦訪問日取得
			g_erahdat = getEra_ret.dat + " " + g_time;
			// 得意先情報取得
			cdsTORI(g_cucd, openDataSet_H_cld7);
		}
		private function openDataSet_H_cld7(): void
		{
			// 前回訪問日和暦変換
			getEra(arrTORI[0].LHDAT, openDataSet_H_cld8);
		}
		private function openDataSet_H_cld8(): void
		{
			// 和暦前回訪問日取得
			g_eralhdat = getEra_ret.dat + " " + arrTORI[0].LHTIME;

			getEra(g_date);

			super.openDataSet_H();
		}
		
		/**********************************************************
		 * ボディデータ取得
		 * @author  12.09.06 SE-362
		 **********************************************************/
		override public function openDataSet_B(): void
		{
			cdsMA041X2(openDataSet_B_cld1);
		}
		
		private function openDataSet_B_cld1(): void
		{
			if(modifyFlag == 2){
				// 明細ワーク削除
				delMA040X2(openDataSet_B_cld2);
			}else{
				openDataSet_B_last();
			}
		}

		private function openDataSet_B_cld2(): void
		{
			// 明細ワーク作成
			wdsMA040X2(openDataSet_B_last);
		}
		
		private function openDataSet_B_last(): void
		{
			//getTax(arrSEIGYO[0].SDDAY, super.openDataSet_B);
			getTax(view.data.HDAT, super.openDataSet_B);

			if(modifyFlag == 2){
				insLF013(g_cucd,"配置薬入金訂正開始");
			}
		}
		
		/**********************************************************
		 * 画面項目セット
		 * @author SE-354 
		 * @date 12.04.27
		 **********************************************************/
		override public function setDispObject(): void
		{
			super.setDispObject();

			// 配置薬時
			if(arrMA041X1 != null && arrMA041X1.length != 0){
				view.ma041x = arrMA041X1[0];
					
			}else{
				// 該当レコードなし
				singleton.sitemethod.dspJoinErrMsg("E0020", btnRTNClick_last);
				return;
			}

			// 修正時
			if(modifyFlag > 0) {
				// LJ001データ存在チェック
				if(arrMA040X1 == null || arrMA040X1.length == 0){
					// 該当レコードなし
					singleton.sitemethod.dspJoinErrMsg("E0020", btnRTNClick_last);
					return;
				}
			};		
			
			// 伝票番号退避
			g_vseq = arrMA041X1[0].VNO;
			view.edtVSEQ.text = g_vseq;
			// 通しNO退避
			g_tno = "";
			
			// 明細情報
			view.grdW01.dataProvider = arrMA041X2;
			
			if(modifyFlag==0)
			{
				// 新規時のみ入金額は請求額と同額を表示
				//view.ma041x.MEDIMONEY = view.ma041x.MEDIAMT;
				//view.ma041x.STOREMONEY = view.ma041x.STOREAMT;
				//view.ma041x.DEVMONEY = view.ma041x.DEVAMT;
				view.ma041x.MEDIMONEY = BasedMethod.calcNum(view.ma041x.MEDIAMT, view.ma041x.MEDIBAL);
				view.ma041x.STOREMONEY = BasedMethod.calcNum(view.ma041x.STOREAMT, view.ma041x.STOREBAL);
				view.ma041x.DEVMONEY = BasedMethod.calcNum(view.ma041x.DEVAMT, view.ma041x.DEVBAL);
			}
			
			// 合計算出
			calcSum();

			if(modifyFlag > 0)
			{
				view.lblMODIFY.text = "修正";
				//view.btnDEC.label = "登録";
			}
		}
		
		/**********************************************************
		 * btnDEC クリック時処理.
		 * @author 13.01.25 SE-233
		 **********************************************************/
		public function btnDECClick(): void
		{
			// 入力チェック
			singleton.sitemethod.dspJoinErrMsg("I0007", btnDECClick_cld1, "", "1", [], btnDECClick_cld2);
		}
		private function btnDECClick_cld1(): void 
		{
			doF10Proc();
		}
		private function btnDECClick_cld2(): void 
		{
			return;
		}
		
		/**********************************************************
		 * execDecPLSQL
		 * @author 12.07.03 SE-354
		 **********************************************************/
		override public function execDecPLSQL(): void
		{
// 13.04.16 SE-233 start
//			updMA041X1(execDecPLSQL_cld1);
			cdsSEIGYO(execDecPLSQL_cld0);
// 13.04.16 SE-233 end
		}
		
// 13.04.16 SE-233 start
		private function execDecPLSQL_cld0(): void 
		{
			updMA041X1(execDecPLSQL_cld1);
		}
// 13.04.16 SE-233 end
		
		private function execDecPLSQL_cld1(): void 
		{
			var w_date:Date = new Date();
			getEra(singleton.g_customdateformatter.dateDisplayFormat_Full(w_date), execDecPLSQL_cld1a);
		}
		
		private function execDecPLSQL_cld1a(): void 
		{
			g_sysdate = getEra_ret.dat;
			
			if(modifyFlag == 2){
				execDecPLSQL_cld11();
			}else{
				execDecPLSQL_cld2();
			}
		}

		private function execDecPLSQL_cld11(): void
		{
			updMA041X3(execDecPLSQL_cld2);
		}

		private function execDecPLSQL_cld2(): void
		{
			// 入金額がある場合、領収書印刷
			if(BasedMethod.asCurrency(view.edtSUM3.text) != 0){
				singleton.sitemethod.dspJoinErrMsg("I0005", execDecPLSQL_cld21, "", "0", ["領収証"]);
				return;
			}else{
				execDecPLSQL_cld5();
			}
		}
		
		private function execDecPLSQL_cld21(): void 
		{
//			// 通しNO++
//			g_tno = (Number(arrSEIGYO[0].TNO) + 1).toString();
//			// 制御マスタ更新
//			updMA041X2(execDecPLSQL_cld22);
			// 通しNO更新
			updTNO(execDecPLSQL_cld23);
		}
		
//		private function execDecPLSQL_cld22(): void
//		{
//			// 制御ﾏｽﾀ情報再取得
//			cdsSEIGYO(execDecPLSQL_cld23);
//		}
		
		private function execDecPLSQL_cld23(): void
		{
			// ログ出力
			insLF013(g_cucd,"領収証",g_vseq);
			// 領収書の発行
			printHeader(execDecPLSQL_cld3);
		}
		
		private function execDecPLSQL_cld3(): void
		{
			// 次回繰越があれば、売掛請求書発行
			if(BasedMethod.asCurrency(view.edtSUM4.text) != 0){
				singleton.sitemethod.dspJoinErrMsg("I0005", execDecPLSQL_cld4, "", "0", ["売掛請求書"]);
				return;
			}else{
				execDecPLSQL_cld5();
			}
		}
		
		private function execDecPLSQL_cld4(): void
		{
			// 通しNO更新
			updTNO(execDecPLSQL_cld41);
		}
		
		private function execDecPLSQL_cld41(): void
		{
			// ログ出力
			insLF013(g_cucd,"請求書",g_vseq);

			printHeader2(execDecPLSQL_cld5);
		}
		
		private function execDecPLSQL_cld5(): void
		{
			// 入金訂正の場合、会社控え出力
			if(modifyFlag == 2){
				execDecPLSQL_cld51()
			}else{
				super.execDecPLSQL()
			}
		}

		private function execDecPLSQL_cld51(): void 
		{
			// 領収書未発行の場合,通しNO++
			if(g_tno == ""){
				// 通しNO++
				g_tno = (Number(arrSEIGYO[0].TNO) + 1).toString();
				// 制御マスタ更新
				//updMA041X2(execDecPLSQL_cld6);
				execDecPLSQL_cld6();
			}else{
				execDecPLSQL_cld6();
			}
		}
		
		private function execDecPLSQL_cld6(): void
		{
			CreatePrintArray1(execDecPLSQL_cld7);
		}

		private function execDecPLSQL_cld7(): void
		{
			CreatePrintArray2(execDecPLSQL_cld8);
		}

		private function execDecPLSQL_cld8(): void
		{
			singleton.sitemethod.dspJoinErrMsg("I0005", execDecPLSQL_cld9, "", "0", ["配置薬(会社控)"]);
		}
		
		private function execDecPLSQL_cld9(): void
		{
			if(reportCount == 0){
				super.execDecPLSQL();
				return;
			}
			// 通しNO更新
			updTNO(execDecPLSQL_cld91);
		}

		private function execDecPLSQL_cld91(): void
		{
			// ログ出力
			insLF013(g_cucd,"配置薬控（会社用）",g_vseq);

			printMA041X(super.execDecPLSQL);
		}
		
		override public function doF10Proc_last():void
		{
			var obj: Object = new Object;
			
//			obj.cucd = g_cucd;
//			obj.cunm = view.edtCUNM.text;
//			obj.vseq = g_vseq;
//			obj.tktel = String(view.navigator.context.tktel);
	
//			obj = view.navigator.context;
			// リスナー削除
			view.stage.removeEventListener(KeyboardEvent.KEY_DOWN, onKeyDownHandler);
			
			// 入金訂正の場合、会社控え出力
			if(modifyFlag == 2){
				insLF013(g_cucd,"配置薬入金訂正終了");
				view.navigator.popAll();
				view.navigator.pushView(mainHomeView);
				view.navigator.pushView(MA009X);
				view.navigator.pushView(MA010X, view.data);
			}else{
				obj.vseq = g_vseq;
				obj.modifyFlag = modifyFlag;
				obj.tno = g_tno;
				// 12.10.31 SE-233 Redmine#3578 対応 start
				obj.date = g_date;	
				obj.time = g_time;
				// 12.10.31 SE-233 Redmine#3578 対応 end
				
				// 入替に遷移
				view.navigator.pushView(MA042X, view.data, obj);
			}
		}
		
		//////////////////////////個別処理//////////////////////////////
		/**********************************************************
		 * ワークデータ存在チェック
		 * @author 12.09.06 SE-362
		 **********************************************************/
		private function chkMA040X1(func: Function): void
		{
			// 引数情報
			cdsMA040X1_arg = new Object;
			cdsMA040X1_arg.func = func;
			
			stmt = new SQLStatement();
			stmt.sqlConnection =  singleton.sqlconnection;
			stmt.text= singleton.g_query_xml.entry.(@key == "cdsMA040X3");
			stmt.itemClass = LJ001;
			
			// 得意先条件
			stmt.parameters[0] = g_cucd;
			stmt.parameters[1] = view.data.HDAT;
			
			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, chkMA040X1Result, false, 0, true);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler, false, 0, true);
			
			//実行
			stmt.execute();
		}
		
		public function chkMA040X1Result(event: SQLEvent): void
		{	
			var w_arg: Object = cdsMA040X1_arg;
			var result:SQLResult = stmt.getResult();
			
			stmt.removeEventListener(SQLEvent.RESULT, chkMA040X1Result);				
			stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);				
			stmt=null;
			
			arrMA040X1 = new ArrayCollection(result.data);
			
			if ((arrMA040X1 != null) && (arrMA040X1.length != 0)){
				// 入金訂正の場合
				//if(modifyFlag == 2){
					g_date = arrMA040X1[0].HDAT;	
					g_time = arrMA040X1[0].HTIME;
				//} else {
				//	g_date = arrSEIGYO[0].SDDAY;
				//	g_time = BasedMethod.getTime();
				//}
				getEra(g_date);
			}			
			// 次関数実行
			if(w_arg.func)
				w_arg.func();
		}

		/**********************************************************
		 * ヘッダ情報取得
		 * @author 12.09.06 SE-362
		 **********************************************************/
		public function cdsMA041X1(func: Function=null): void
		{
			// 引数情報
			cdsMA041X_arg = new Object;
			cdsMA041X_arg.func = func;
			
			stmt = new SQLStatement();
			stmt.sqlConnection =  singleton.sqlconnection;
			stmt.parameters[0] = g_cucd;
			stmt.itemClass = W_MA040X;
			stmt.text= singleton.g_query_xml.entry.(@key == "qryMA041X1");

			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, cdsMA041X1Result, false, 0, true);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler, false, 0, true);
			
			//実行
			stmt.execute();
		}
		
		public function cdsMA041X1Result(event: SQLEvent): void
		{
			var w_arg: Object = cdsMA041X_arg;
			var result:SQLResult = stmt.getResult();
			
			stmt.removeEventListener(SQLEvent.RESULT, cdsMA041X1Result);				
			stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);				
			stmt=null;
			
			arrMA041X1 = new ArrayCollection(result.data);
			
			// 次関数実行
			if(w_arg.func)
				w_arg.func();
		}
		
		/**********************************************************
		 * ヘッダワーク削除
		 * @author 12.09.06 SE-362
		 **********************************************************/
		public function delMA040X1(func: Function=null): void
		{
			// 引数情報
			cdsMA040X1_arg = new Object;
			cdsMA040X1_arg.func = func;
			
			stmt = new SQLStatement();
			stmt.sqlConnection =  singleton.sqlconnection;
			stmt.text= singleton.g_query_xml.entry.(@key == "delMA040X1");
			
			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, delMA040X1Result, false, 0, true);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler, false, 0, true);
			
			//実行
			stmt.execute();
		}
		
		public function delMA040X1Result(event: SQLEvent): void
		{	
			var w_arg: Object = cdsMA040X1_arg;
			
			stmt.removeEventListener(SQLEvent.RESULT, delMA040X1Result);				
			stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);				
			stmt=null;
			
			// 次関数実行
			if(w_arg.func)
				w_arg.func();
		}

		/**********************************************************
		 * ヘッダワーク作成
		 * @author 12.09.06 SE-362
		 **********************************************************/
		public function wdsMA040X1(func: Function=null): void
		{
			// 引数情報
			cdsMA040X1_arg = new Object;
			cdsMA040X1_arg.func = func;
			
			stmt = new SQLStatement();
			stmt.sqlConnection =  singleton.sqlconnection;
			stmt.text= singleton.g_query_xml.entry.(@key == "wdsMA040X3");
			stmt.parameters[0] = g_cucd;
			stmt.parameters[1] = view.data.HDAT;
			stmt.itemClass = W_MA040X;
			
			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, wdsMA040X1Result, false, 0, true);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler, false, 0, true);
			
			//実行
			stmt.execute();
		}
		
		public function wdsMA040X1Result(event: SQLEvent): void
		{	
			var w_arg: Object = cdsMA040X1_arg;
			var result:SQLResult = stmt.getResult();
			
			stmt.removeEventListener(SQLEvent.RESULT, wdsMA040X1Result);				
			stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);				
			stmt=null;
			
			// 次関数実行
			if(w_arg.func)
				w_arg.func();
		}

		/**********************************************************
		 * ヘッダワーク更新
		 * @author 12.09.06 SE-362
		 **********************************************************/
		public function updMA041X1(func: Function=null): void
		{
			// 引数情報
			cdsMA041X_arg = new Object;
			cdsMA041X_arg.func = func;
			
			stmt = new SQLStatement();
			stmt.sqlConnection =  singleton.sqlconnection;
			stmt.text= singleton.g_query_xml.entry.(@key == "updMA041X1");
			
			// パラメタ情報
			stmt.parameters[0] =  view.ma041x.MEDIBAL;
			stmt.parameters[1] =  view.ma041x.MEDIMONEY;
			//stmt.parameters[2] =  view.ma041x.MEDIRTAX;
			stmt.parameters[2] = BasedMethod.calcNum(view.ma041x.MEDIMONEY,
				BasedMethod.calcNum(view.ma041x.MEDIMONEY,BasedMethod.calcNum(BasedMethod.calcNum(BasedMethod.nvl(String(getTax_ret.tax), "0"),"100"),"100",3,3),3,0,2),1);
			stmt.parameters[3] =  view.ma041x.MEDINBAL;
			stmt.parameters[4] =  view.ma041x.STOREBAL;
			stmt.parameters[5] =  view.ma041x.STOREMONEY;
			//stmt.parameters[6] =  view.ma041x.STORERTAX;
			stmt.parameters[6] = BasedMethod.calcNum(view.ma041x.STOREMONEY,
				BasedMethod.calcNum(view.ma041x.STOREMONEY,BasedMethod.calcNum(BasedMethod.calcNum(BasedMethod.nvl(String(getTax_ret.tax), "0"),"100"),"100",3,3),3,0,2),1);
			stmt.parameters[7] =  view.ma041x.STORENBAL;
			stmt.parameters[8] =  view.ma041x.DEVBAL;
			stmt.parameters[9] =  view.ma041x.DEVMONEY;
			//stmt.parameters[10] = view.ma041x.DEVRTAX;
			stmt.parameters[10] = BasedMethod.calcNum(view.ma041x.DEVMONEY,
				BasedMethod.calcNum(view.ma041x.DEVMONEY,BasedMethod.calcNum(BasedMethod.calcNum(BasedMethod.nvl(String(getTax_ret.tax), "0"),"100"),"100",3,3),3,0,2),1);
			stmt.parameters[11] = view.ma041x.DEVNBAL;
			stmt.parameters[12] = g_cucd;
			
			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, updMA041X1Result, false, 0, true);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler, false, 0, true);
			
			//実行
			stmt.execute();
		}
		
		public function updMA041X1Result(event: SQLEvent): void
		{	
			var w_arg: Object = cdsMA041X_arg;
			var result:SQLResult = stmt.getResult();
			
			stmt.removeEventListener(SQLEvent.RESULT, updMA041X1Result);				
			stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);				
			stmt=null;
			
			// 次関数実行
			if(w_arg.func)
				w_arg.func();
		}

		/**********************************************************
		 * ボディ情報取得
		 * @author 12.09.06 SE-362
		 **********************************************************/
		public function cdsMA041X2(func: Function=null): void
		{
			// 引数情報
			cdsMA041X_arg = new Object;
			cdsMA041X_arg.func = func;
			
			stmt = new SQLStatement();
			stmt.sqlConnection =  singleton.sqlconnection;
			stmt.parameters[0] = g_cucd;
//			stmt.parameters[0] = "ｲ2386";
			stmt.itemClass = MA041V01;
			stmt.text= singleton.g_query_xml.entry.(@key == "qryMA041X2");
			
			
			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, cdsMA041X2Result, false, 0, true);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler, false, 0, true);
			
			//実行
			stmt.execute();
		}
		
		public function cdsMA041X2Result(event: SQLEvent): void
		{
			var w_arg: Object = cdsMA041X_arg;
			var result:SQLResult = stmt.getResult();
			
			stmt.removeEventListener(SQLEvent.RESULT, cdsMA041X2Result);				
			stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);				
			stmt=null;
			
			arrMA041X2 = new ArrayCollection(result.data);
			
			// 次関数実行
			if(w_arg.func)
				w_arg.func();
		}
		
		private function stmtErrorHandler(event: SQLErrorEvent): void
		{
			// エラーの表示
			trace(event.error.message);
		}

		/**********************************************************
		 * 明細ワーク削除
		 * @author 12.09.06 SE-362
		 **********************************************************/
		public function delMA040X2(func: Function=null): void
		{
			// 引数情報
			cdsMA040X2_arg = new Object;
			cdsMA040X2_arg.func = func;
			
			stmt = new SQLStatement();
			stmt.sqlConnection =  singleton.sqlconnection;
			stmt.text= singleton.g_query_xml.entry.(@key == "delMA040X2");
			
			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, delMA040X2Result, false, 0, true);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler, false, 0, true);
			
			//実行
			stmt.execute();
		}
		
		public function delMA040X2Result(event: SQLEvent): void
		{	
			var w_arg: Object = cdsMA040X2_arg;
			
			stmt.removeEventListener(SQLEvent.RESULT, delMA040X2Result);				
			stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);				
			stmt=null;
			
			// 次関数実行
			if(w_arg.func)
				w_arg.func();
		}
		
		/**********************************************************
		 * 明細ワーク作成
		 * @author 12.09.06 SE-362
		 **********************************************************/
		public function wdsMA040X2(func: Function=null): void
		{
			// 引数情報
			cdsMA040X2_arg = new Object;
			cdsMA040X2_arg.func = func;
			
			stmt = new SQLStatement();
			stmt.sqlConnection =  singleton.sqlconnection;
			stmt.text= singleton.g_query_xml.entry.(@key == "wdsMA040X4");
			stmt.itemClass = W_MA041X;
			stmt.parameters[0] = g_cucd;
			
			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, wdsMA040X2Result, false, 0, true);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler, false, 0, true);
			
			//実行
			stmt.execute();
		}
		
		public function wdsMA040X2Result(event: SQLEvent): void
		{	
			var w_arg: Object = cdsMA040X2_arg;
			var result:SQLResult = stmt.getResult();
			
			stmt.removeEventListener(SQLEvent.RESULT, wdsMA040X2Result);				
			stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);				
			stmt=null;
			
			// 次関数実行
			if(w_arg.func)
				w_arg.func();
		}

		/**********************************************************
		 * 制御マスタ更新
		 * @author 12.10.16 SE-300
		 **********************************************************/
		public function updMA041X2(func: Function=null): void
		{
			// 引数情報
			cdsMA041X_arg = new Object;
			cdsMA041X_arg.func = func;
			
			stmt = new SQLStatement();
			stmt.sqlConnection =  singleton.sqlconnection;
			stmt.text= singleton.g_query_xml.entry.(@key == "updMA041X2");
			
			// パラメタ情報
			// 入替伝票No
			//stmt.parameters[0] = g_vseq;
			// 通しNo
			//stmt.parameters[1] = (Number(arrSEIGYO[0].TNO) + 1).toString();
			stmt.parameters[0] = g_tno;
			
			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, updMA041X2Result, false, 0, true);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler, false, 0, true);
			
			//実行
			stmt.execute();
		}
		
		public function updMA041X2Result(event: SQLEvent): void
		{	
			var w_arg: Object = cdsMA041X_arg;
			var result:SQLResult = stmt.getResult();
			
			stmt.removeEventListener(SQLEvent.RESULT, updMA041X2Result);				
			stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);				
			stmt=null;
			
			// 次関数実行
			if(w_arg.func)
				w_arg.func();
		}
		
		/**********************************************************
		 * 配置薬売上JA更新(update)
		 * @author 12.10.16 SE-300
		 **********************************************************/
		public function updMA041X3(func: Function=null): void
		{
			// 引数情報
			cdsMA041X_arg = new Object;
			cdsMA041X_arg.func = func;
			
			stmt = new SQLStatement();
			stmt.sqlConnection =  singleton.sqlconnection;
			stmt.text= singleton.g_query_xml.entry.(@key == "updMA041X3");
			
			// パラメタ情報
			stmt.parameters[0] =  view.ma041x.MEDIBAL;
			stmt.parameters[1] =  view.ma041x.MEDIMONEY;
			//stmt.parameters[2] =  view.ma041x.MEDIRTAX;
			stmt.parameters[2] = BasedMethod.calcNum(view.ma041x.MEDIMONEY,
				BasedMethod.calcNum(view.ma041x.MEDIMONEY,BasedMethod.calcNum(BasedMethod.calcNum(BasedMethod.nvl(String(getTax_ret.tax), "0"),"100"),"100",3,3),3,0,2),1);
			stmt.parameters[3] =  view.ma041x.MEDINBAL;
			stmt.parameters[4] =  view.ma041x.STOREBAL;
			stmt.parameters[5] =  view.ma041x.STOREMONEY;
			//stmt.parameters[6] =  view.ma041x.STORERTAX;
			stmt.parameters[6] = BasedMethod.calcNum(view.ma041x.STOREMONEY,
				BasedMethod.calcNum(view.ma041x.STOREMONEY,BasedMethod.calcNum(BasedMethod.calcNum(BasedMethod.nvl(String(getTax_ret.tax), "0"),"100"),"100",3,3),3,0,2),1);
			stmt.parameters[7] =  view.ma041x.STORENBAL;
			stmt.parameters[8] =  view.ma041x.DEVBAL;
			stmt.parameters[9] =  view.ma041x.DEVMONEY;
			//stmt.parameters[10] = view.ma041x.DEVRTAX;
			stmt.parameters[10] = BasedMethod.calcNum(view.ma041x.DEVMONEY,
				BasedMethod.calcNum(view.ma041x.DEVMONEY,BasedMethod.calcNum(BasedMethod.calcNum(BasedMethod.nvl(String(getTax_ret.tax), "0"),"100"),"100",3,3),3,0,2),1);
			stmt.parameters[11] = view.ma041x.DEVNBAL;
			stmt.parameters[12] = g_vseq;

			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, updMA041X3Result, false, 0, true);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler, false, 0, true);
			
			//実行
			stmt.execute();
		}
		
		public function updMA041X3Result(e: SQLEvent): void
		{
			var w_arg: Object = cdsMA041X_arg;
			var result:SQLResult = stmt.getResult();
			
			stmt.removeEventListener(SQLEvent.RESULT, updMA041X3Result);				
			stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);				
			stmt=null;
			
			// 次関数実行
			if(w_arg.func)
				w_arg.func();
		}
		
		/**********************************************************
		 * 印刷用 明細情報取得
		 * @author 12.10.16 SE-300
		 **********************************************************/
		public function CreatePrintArray1(func: Function=null): void
		{
			// 引数情報
			cdsMA041X_arg = new Object;
			cdsMA041X_arg.func = func;
			
			stmt = new SQLStatement();
			stmt.sqlConnection =  singleton.sqlconnection;
			stmt.text= singleton.g_query_xml.entry.(@key == "cdsMA040X2");
			stmt.itemClass = W_MA041X;
			stmt.parameters[0] = g_cucd;
			
			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, CreatePrintArray1Result, false, 0, true);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler, false, 0, true);
			
			//実行
			stmt.execute();
		}
		
		public function CreatePrintArray1Result(event: SQLEvent): void
		{	
			var w_arg: Object = cdsMA041X_arg;
			var result:SQLResult = stmt.getResult();
			
			stmt.removeEventListener(SQLEvent.RESULT, CreatePrintArray1Result);				
			stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);				
			stmt=null;
			
			arrPRINT = new ArrayCollection(result.data);
			
			// 次関数実行
			if(w_arg.func)
				w_arg.func();
		}
		
		/**********************************************************
		 * 帳票用配列作成
		 * @author 12.10.16 SE-300
		 **********************************************************/
		private function CreatePrintArray2(func: Function): void
		{
			// 一括発行明細数
			var w_detail: int = 5;
			var w_length: int;
			var w_nodata: int;
			var i: int;
			
			if(arrPRINT != null && arrPRINT.length != 0){
				w_length = arrPRINT.length;
			}else{
				func();
				return;
			}
			
			if(arrPRINT.length % w_detail != 0){
				w_nodata = w_detail - (arrPRINT.length % w_detail);
			}
			
			reportCount = Math.floor(arrPRINT.length / w_detail) + 1;
			if(arrPRINT.length % w_detail == 0){
				reportCount--;
			}
			
			for(i=0; i < w_nodata; i++){
				arrPRINT.addItem(new W_MA041X());
			}
			
			func();
		}
		
		/**********************************************************
		 * printHeader
		 * @author 12.09.11 SE-362
		 **********************************************************/
		private function printHeader(func: Function): void
		{
			var hts: HTTPService = new HTTPService();
			var w_tknm1: String;
			var w_tknm2: String;
			
			print_arg = new Object;
			print_arg.func = func;
			
			var tknm: Object = BasedMethod.getTKNM(view.data.TKNM);

			hts.method = "GET";
			hts.addEventListener(ResultEvent.RESULT, htsHeaderResult, false, 0, true);
			hts.addEventListener(FaultEvent.FAULT, htsFault, false, 0, true);
			hts.url = "http://localhost:8080/Format/Print";
			
			var s: String;
			
			if(BasedMethod.asCurrency(view.edtSUM3.text) == 0){
				s = "";
			}else{
				s = "￥" + view.edtSUM3.text + " -";
			}
			
			// 12.10.19 SE-233 Redmine#3479 対応 start
			var w_tax: String;
			w_tax = BasedMethod.calcNum(view.edtSUM3.text,
				BasedMethod.calcNum(view.edtSUM3.text,BasedMethod.calcNum(BasedMethod.calcNum(BasedMethod.nvl(String(getTax_ret.tax), "0"),"100"),"100",3,3),3,0,2),1);
			
			if(BasedMethod.asCurrency(w_tax) == 0){
				w_tax = "";
			}else{
				w_tax = "￥" + w_tax + " -";
			}
			// 12.10.19 SE-233 Redmine#3479 対応 end

			hts.send({
				"__format_archive_url": "file:///sdcard/print/me020x.spfmtz",
				"__format_archive_update": "updateWithoutError",
				"__format_id_number": "1",
				"TKCD": g_cucd,
				"TKNM1": tknm.tknm1,
				"TKNM2": tknm.tknm2,
// 13.4.1 SE-233 start
				// 12.11.06 SE-233 Redmine#3628 対応 start
//				"HDAT": getEra_ret.dat,
				// 12.11.06 SE-233 Redmine#3628 対応 end
				"HDAT": g_sysdate,
// 13.4.1 SE-233 end
				"VNO": g_vseq,
//				"CHGNM": arrCHGCD2[0].CHGNM,
//				"CHGNM": "",
				"HED1": arrSEIGYO[0].COSNM1,
				"HED2": arrSEIGYO[0].COSNM2,
				"HED3": arrSEIGYO[0].COSNM3,
				"ADR1": arrSEIGYO[0].ADD1,
				"ADR2": arrSEIGYO[0].ADD2,
				"ADR3": arrSEIGYO[0].ADD3,
				"ADR23": arrSEIGYO[0].ADD23,
				"TEL1": arrSEIGYO[0].TEL1,
				"TEL2": arrSEIGYO[0].TEL2,
				"TEL3": arrSEIGYO[0].TEL3,
//				"AMT": BasedMethod.asCurrency(view.edtSUM3.text),
				"AMT": s,
				// 12.10.19 SE-233 Redmine#3479 対応 start
				"TAX": w_tax,
				// 12.10.19 SE-233 Redmine#3479 対応 end
				"TNO": getTNO(),
				"(発行枚数)": "1"
			});
		}
		
		private function htsHeaderResult(e: ResultEvent): void
		{
			if((e != null)&&(e.result != ""))
			{
				// 印刷成功時処理
				if(e.result.response.result == "OK"){
					// 印刷失敗時 result == NG
				}else{
					singleton.sitemethod.dspJoinErrMsg(e.result.response.message,
						doF10Proc_last, "", "0", null, null, true);
					return;
				}
				//IEventDispatcher(e.currentTarget).removeEventListener(ResultEvent.RESULT, htsHeaderResult);				
				//IEventDispatcher(e.currentTarget).removeEventListener(FaultEvent.FAULT, htsFault);				
			}

			BasedMethod.execNextFunction(print_arg.func);
		}
		
		/**********************************************************
		 * printHeader 売掛請求書
		 * @author 12.09.11 SE-362
		 **********************************************************/
		private function printHeader2(func: Function): void
		{
			var hts: HTTPService = new HTTPService();
			var w_tknm1: String;
			var w_tknm2: String;
			
			// 引数情報
			print_arg = new Object;
			print_arg.func = func;
			
			var tknm: Object = BasedMethod.getTKNM(view.data.TKNM);
			var w_dis: String;
			var w_withtax: String;

			// 12.11.06 SE-233 Redmine#3628 対応 start
			// 和暦取得
			getEra(g_date);
			// 12.11.06 SE-233 Redmine#3628 対応 start
			
			// お買上金額算出
			w_withtax = BasedMethod.calcNum(BasedMethod.calcNum(view.ma041x.MEDIWITHTAX, view.ma041x.STOREWITHTAX), view.ma041x.DEVWITHTAX);
			w_withtax = singleton.g_customnumberformatter.numberFormat(w_withtax, "1", "0", "0");
			
			// 値引額算出
			//			w_dis = BasedMethod.calcNum(BasedMethod.calcNum(arrMA041X1[0].MEDIDIS, arrMA041X1[0].STOREDIS), arrMA041X1[0].DEVDIS);
			w_dis = BasedMethod.calcNum(w_withtax, view.edtSUM2.text, 1);
			w_dis = singleton.g_customnumberformatter.numberFormat(w_dis, "1", "0", "0"); 
			
			hts.method = "GET";
			hts.addEventListener(ResultEvent.RESULT, htsHeaderResult2, false, 0, true);
			hts.addEventListener(FaultEvent.FAULT, htsFault, false, 0, true);
			hts.url = "http://localhost:8080/Format/Print";
			
			hts.send({
				"__format_archive_url": "file:///sdcard/print/me021x.spfmtz",
				"__format_archive_update": "updateWithoutError",
				"__format_id_number": "1",
				"TKCD": g_cucd,
				"TKNM1": tknm.tknm1,
				"TKNM2": tknm.tknm2,
				// 12.11.06 SE-233 Redmine#3628 対応 start
				"HDAT": getEra_ret.dat + " " + g_time,
				// 12.11.06 SE-233 Redmine#3628 対応 end
				"VNO": g_vseq,
				"CHGNM": arrCHGCD2[0].CHGNM,
				"HED1": arrSEIGYO[0].COSNM1,
				"HED2": arrSEIGYO[0].COSNM2,
				"HED3": arrSEIGYO[0].COSNM3,
				"ADR1": arrSEIGYO[0].ADD1,
				"ADR2": arrSEIGYO[0].ADD2,
				"ADR3": arrSEIGYO[0].ADD3,
				"ADR23": arrSEIGYO[0].ADD23,
				"TEL1": arrSEIGYO[0].TEL1,
				"TEL2": arrSEIGYO[0].TEL2,
				"TEL3": arrSEIGYO[0].TEL3,
				"BAL"		: BasedMethod.nvl(view.edtSUM1.text),
				"UAMT"		: w_withtax,
				"DIS"		: w_dis,
				"NAMT"		: BasedMethod.nvl(view.edtSUM3.text),
				"KAMT"		: BasedMethod.nvl(view.edtSUM4.text),
				"BNKNM1"	: arrSEIGYO[0].BNKCD1,
				"BNKSNM1"	: arrSEIGYO[0].BNKSCD1,
				"VBNO1"	: arrSEIGYO[0].VBNO1,
				"BNKMEMO1"	: arrSEIGYO[0].BNKMEMO1,
				"BNKNM2"	: arrSEIGYO[0].BNKCD2,
				"BNKSNM2"	: arrSEIGYO[0].BNKSCD2,
				"VBNO2"	: arrSEIGYO[0].VBNO2,
				"BNKMEMO2"	: arrSEIGYO[0].BNKMEMO2,
				"BNKNM3"	: arrSEIGYO[0].BNKCD3,
				"BNKSNM3"	: arrSEIGYO[0].BNKSCD3,
				"VBNO3"	: arrSEIGYO[0].VBNO3,
				"BNKMEMO3"	: arrSEIGYO[0].BNKMEMO3,
				"TNO": getTNO(),
				"(発行枚数)": "1"
			});
		}
		
		private function htsHeaderResult2(e: ResultEvent): void
		{
			if((e != null)&&(e.result != ""))
			{
				// 印刷成功時処理
				if(e.result.response.result == "OK"){
					// 印刷失敗時 result == NG
				}else{
					singleton.sitemethod.dspJoinErrMsg(e.result.response.message,
						doF10Proc_last, "", "0", null, null, true);
					return;
				}
				//IEventDispatcher(e.currentTarget).removeEventListener(ResultEvent.RESULT, htsHeaderResult2);				
				//IEventDispatcher(e.currentTarget).removeEventListener(FaultEvent.FAULT, htsFault);				
			}

			// 次関数実行
			BasedMethod.execNextFunction(print_arg.func);
		}
		
		private function htsFault(e: FaultEvent): void
		{
			/*
			var Alert: AlertDialog = new AlertDialog();
			
			Alert.message = "サーバを起動してから再度実行して下さい。";
			Alert.btntype = "0";
			Alert.type = "2";
			Alert.open(DisplayObjectContainer(view), true);
			*/
			//IEventDispatcher(e.currentTarget).removeEventListener(FaultEvent.FAULT, htsFault);				
			singleton.sitemethod.dspJoinErrMsg("印刷に失敗しました。サーバを起動してから再度実行して下さい。",
				doF10Proc_last, "", "0", null, null, true);
		}
		
		/**********************************************************
		 * 配置薬発行
		 * @author 12.10.16 SE-300
		 **********************************************************/
		private function printMA041X(func: Function): void
		{
			// 引数
			print_arg = new Object;
			print_arg.func = func;
			
			var hts: HTTPService = new HTTPService();
// 15.03.19 SE-233 start
// 15.01.19 SE-233 start
//			var params:Object = new Object();
//			var pIndex: String;
// 15.01.19 SE-233 end
// 15.03.19 SE-233 end
			var w_tknm1: String;
			var w_tknm2: String;
			
// 15.03.19 SE-233 start
// 15.01.19 SE-233 start
//			var nowDtail: int;
//			var w_vlin1: String;
//			var w_vlin2: String;
//			var w_vlin3: String;
//			var w_vlin4: String;
//			var w_vlin5: String;
//			var w_kunum1: String;
//			var w_kunum2: String;
//			var w_kunum3: String;
//			var w_kunum4: String;
//			var w_kunum5: String;
//			var w_uamt1: String;
//			var w_uamt2: String;
//			var w_uamt3: String;
//			var w_uamt4: String;
//			var w_uamt5: String;
//			var w_nnum1: String;
//			var w_nnum2: String;
//			var w_nnum3: String;
//			var w_nnum4: String;
//			var w_nnum5: String;
//			var w_hnum1: String;
//			var w_hnum2: String;
//			var w_hnum3: String;
//			var w_hnum4: String;
//			var w_hnum5: String;
//			var w_knum1: String;
//			var w_knum2: String;
//			var w_knum3: String;
//			var w_knum4: String;
//			var w_knum5: String;
//
//			var ttl_bal: String;
//			var w_bal: String;
//			var w_bal1: String;
//			var w_dis: String;
//			var w_amt: String;
//			var w_tax: String;
//			var w_namt: String;
//			var w_zamt: String;
//			var w_uamt: String;
// 15.01.19 SE-233 end
// 15.03.19 SE-233 end
			
			// 明細数セット
			currentCount = 0;
			
			if(reportCount == 0){
				printMA041X_last();
				return;
			}
			/*			
			if(view.edtCUNM.text.length > 20){
			w_tknm1 = view.edtCUNM.text.slice(0, 20);
			w_tknm2 = view.edtCUNM.text.slice(20, view.edtCUNM.text.length);
			}else{
			w_tknm1 = view.edtCUNM.text;
			w_tknm2 = "";
			}
			*/
			var tknm: Object = BasedMethod.getTKNM(view.data.TKNM);
			
			hts.method = "GET";
// 15.03.19 SE-233 start
// 15.01.19 SE-233 start
//			hts.addEventListener(ResultEvent.RESULT, printMA041X_body);
//			hts.addEventListener(ResultEvent.RESULT, printMA041X_last);
			hts.addEventListener(ResultEvent.RESULT, printMA041X_body, false, 0, true);
// 15.01.19 SE-233 end
// 15.03.19 SE-233 end
			hts.addEventListener(FaultEvent.FAULT, htsFault, false, 0, true);
			hts.url = "http://localhost:8080/Format/Print";
			
// 15.03.19 SE-233 start
// 15.01.19 SE-233 start
			hts.send({
				"__format_archive_url": "file:///sdcard/print/me045x.spfmtz",
				"__format_archive_update": "updateWithoutError",
				"__format_id_number": "1",
				"TKCD": g_cucd,
				"TKNM1": tknm.tknm1,
				"TKNM2": tknm.tknm2,
				"TKTEL": g_tktel,
				"HDAT": g_erahdat,
				"LHDAT": g_eralhdat,
				"VNO": g_vseq,
				"CHGNM": arrCHGCD2[0].CHGNM,
				"HED1": arrSEIGYO[0].COSNM1,
				"HED2": arrSEIGYO[0].COSNM2,
				"HED3": arrSEIGYO[0].COSNM3,
				"ADR1": arrSEIGYO[0].ADD1,
				"ADR2": arrSEIGYO[0].ADD2,
				"ADR3": arrSEIGYO[0].ADD3,
				"TEL1": arrSEIGYO[0].TEL1,
				"TEL2": arrSEIGYO[0].TEL2,
				"TEL3": arrSEIGYO[0].TEL3,
				"(発行枚数)": "1"
			});
/***
			pIndex = currentCount.toString();
			
			params["__format_archive_url"] = "file:///sdcard/print/me045x.spfmtz";
			params["__format_archive_update"] = "updateWithoutError";
			params["__format_id_number[" + pIndex + "]"] = "1";
			params["__format[" + pIndex + "].TKCD"] = g_cucd;
			params["__format[" + pIndex + "].TKNM1"] = tknm.tknm1;
			params["__format[" + pIndex + "].TKNM2"] = tknm.tknm2;
			params["__format[" + pIndex + "].TKTEL"] = g_tktel;
			params["__format[" + pIndex + "].HDAT"] = g_erahdat;
			params["__format[" + pIndex + "].LHDAT"] = g_eralhdat;
			params["__format[" + pIndex + "].VNO"] = g_vseq;
			params["__format[" + pIndex + "].CHGNM"] = arrCHGCD2[0].CHGNM;
			params["__format[" + pIndex + "].HED1"] = arrSEIGYO[0].COSNM1;
			params["__format[" + pIndex + "].HED2"] = arrSEIGYO[0].COSNM2;
			params["__format[" + pIndex + "].HED3"] = arrSEIGYO[0].COSNM3;
			params["__format[" + pIndex + "].ADR1"] = arrSEIGYO[0].ADD1;
			params["__format[" + pIndex + "].ADR2"] = arrSEIGYO[0].ADD2;
			params["__format[" + pIndex + "].ADR3"] = arrSEIGYO[0].ADD3;
			params["__format[" + pIndex + "].TEL1"] = arrSEIGYO[0].TEL1;
			params["__format[" + pIndex + "].TEL2"] = arrSEIGYO[0].TEL2;
			params["__format[" + pIndex + "].TEL3"] = arrSEIGYO[0].TEL3;
			
			for(currentCount=0; currentCount < reportCount; currentCount++){

				nowDtail = (currentCount + 1) * 5;
				
				if(BasedMethod.nvl(arrPRINT[nowDtail - 5].VJAN) != ""){
					w_vlin1 = (nowDtail - 4).toString();
				}else{
					w_vlin1 = "";
				}
				if(BasedMethod.nvl(arrPRINT[nowDtail - 4].VJAN) != ""){
					w_vlin2 = (nowDtail - 3).toString();
				}else{
					w_vlin2 = "";
				}
				if(BasedMethod.nvl(arrPRINT[nowDtail - 3].VJAN) != ""){
					w_vlin3 = (nowDtail - 2).toString();
				}else{
					w_vlin3 = "";
				}
				if(BasedMethod.nvl(arrPRINT[nowDtail - 2].VJAN) != ""){
					w_vlin4 = (nowDtail - 1).toString();
				}else{
					w_vlin4 = "";
				}
				if(BasedMethod.nvl(arrPRINT[nowDtail - 1].VJAN) != ""){
					w_vlin5 = (nowDtail).toString();
				}else{
					w_vlin5 = "";
				}
				if(BasedMethod.nvl(arrPRINT[nowDtail - 5].KUVPNO,"0") != "0"){
					w_kunum1 = BasedMethod.nvl(arrPRINT[nowDtail - 5].KUVPNO);
				}else{
					w_kunum1 = "";
				}
				if(BasedMethod.nvl(arrPRINT[nowDtail - 4].KUVPNO,"0") != "0"){
					w_kunum2 = BasedMethod.nvl(arrPRINT[nowDtail - 4].KUVPNO);
				}else{
					w_kunum2 = "";
				}
				if(BasedMethod.nvl(arrPRINT[nowDtail - 3].KUVPNO,"0") != "0"){
					w_kunum3 = BasedMethod.nvl(arrPRINT[nowDtail - 3].KUVPNO);
				}else{
					w_kunum3 = "";
				}
				if(BasedMethod.nvl(arrPRINT[nowDtail - 2].KUVPNO,"0") != "0"){
					w_kunum4 = BasedMethod.nvl(arrPRINT[nowDtail - 2].KUVPNO);
				}else{
					w_kunum4 = "";
				}
				if(BasedMethod.nvl(arrPRINT[nowDtail - 1].KUVPNO,"0") != "0"){
					w_kunum5 = BasedMethod.nvl(arrPRINT[nowDtail - 1].KUVPNO);
				}else{
					w_kunum5 = "";
				}
				if(BasedMethod.nvl(BasedMethod.calcNum(arrPRINT[nowDtail - 5].KUVPNO, arrPRINT[nowDtail - 5].TAXSAL, 2),"0") != "0"){
					w_uamt1 = BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(BasedMethod.calcNum(arrPRINT[nowDtail - 5].KUVPNO, arrPRINT[nowDtail - 5].TAXSAL, 2), "1", "0", "0"));
				}else{
					w_uamt1 = "";
				}
				if(BasedMethod.nvl(BasedMethod.calcNum(arrPRINT[nowDtail - 4].KUVPNO, arrPRINT[nowDtail - 4].TAXSAL, 2),"0") != "0"){
					w_uamt2 = BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(BasedMethod.calcNum(arrPRINT[nowDtail - 4].KUVPNO, arrPRINT[nowDtail - 4].TAXSAL, 2), "1", "0", "0"));
				}else{
					w_uamt2 = "";
				}
				if(BasedMethod.nvl(BasedMethod.calcNum(arrPRINT[nowDtail - 3].KUVPNO, arrPRINT[nowDtail - 3].TAXSAL, 2),"0") != "0"){
					w_uamt3 = BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(BasedMethod.calcNum(arrPRINT[nowDtail - 3].KUVPNO, arrPRINT[nowDtail - 3].TAXSAL, 2), "1", "0", "0"));
				}else{
					w_uamt3 = "";
				}
				if(BasedMethod.nvl(BasedMethod.calcNum(arrPRINT[nowDtail - 2].KUVPNO, arrPRINT[nowDtail - 2].TAXSAL, 2),"0") != "0"){
					w_uamt4 = BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(BasedMethod.calcNum(arrPRINT[nowDtail - 2].KUVPNO, arrPRINT[nowDtail - 2].TAXSAL, 2), "1", "0", "0"));
				}else{
					w_uamt4 = "";
				}
				if(BasedMethod.nvl(BasedMethod.calcNum(arrPRINT[nowDtail - 1].KUVPNO, arrPRINT[nowDtail - 1].TAXSAL, 2),"0") != "0"){
					w_uamt5 = BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(BasedMethod.calcNum(arrPRINT[nowDtail - 1].KUVPNO, arrPRINT[nowDtail - 1].TAXSAL, 2), "1", "0", "0"));
				}else{
					w_uamt5 = "";
				}
				if(BasedMethod.nvl(arrPRINT[nowDtail - 5].KNVPNO,"0") != "0"){
					w_nnum1 = BasedMethod.nvl(arrPRINT[nowDtail - 5].KNVPNO);
				}else{
					w_nnum1 = "";
				}
				if(BasedMethod.nvl(arrPRINT[nowDtail - 4].KNVPNO,"0") != "0"){
					w_nnum2 = BasedMethod.nvl(arrPRINT[nowDtail - 4].KNVPNO);
				}else{
					w_nnum2 = "";
				}
				if(BasedMethod.nvl(arrPRINT[nowDtail - 3].KNVPNO,"0") != "0"){
					w_nnum3 = BasedMethod.nvl(arrPRINT[nowDtail - 3].KNVPNO);
				}else{
					w_nnum3 = "";
				}
				if(BasedMethod.nvl(arrPRINT[nowDtail - 2].KNVPNO,"0") != "0"){
					w_nnum4 = BasedMethod.nvl(arrPRINT[nowDtail - 2].KNVPNO);
				}else{
					w_nnum4 = "";
				}
				if(BasedMethod.nvl(arrPRINT[nowDtail - 1].KNVPNO,"0") != "0"){
					w_nnum5 = BasedMethod.nvl(arrPRINT[nowDtail - 1].KNVPNO);
				}else{
					w_nnum5 = "";
				}
				if(BasedMethod.nvl(arrPRINT[nowDtail - 5].KRVPNO,"0") != "0"){
					w_hnum1 = BasedMethod.nvl(arrPRINT[nowDtail - 5].KRVPNO);
				}else{
					w_hnum1 = "";
				}
				if(BasedMethod.nvl(arrPRINT[nowDtail - 4].KRVPNO,"0") != "0"){
					w_hnum2 = BasedMethod.nvl(arrPRINT[nowDtail - 4].KRVPNO);
				}else{
					w_hnum2 = "";
				}
				if(BasedMethod.nvl(arrPRINT[nowDtail - 3].KRVPNO,"0") != "0"){
					w_hnum3 = BasedMethod.nvl(arrPRINT[nowDtail - 3].KRVPNO);
				}else{
					w_hnum3 = "";
				}
				if(BasedMethod.nvl(arrPRINT[nowDtail - 2].KRVPNO,"0") != "0"){
					w_hnum4 = BasedMethod.nvl(arrPRINT[nowDtail - 2].KRVPNO);
				}else{
					w_hnum4 = "";
				}
				if(BasedMethod.nvl(arrPRINT[nowDtail - 1].KRVPNO,"0") != "0"){
					w_hnum5 = BasedMethod.nvl(arrPRINT[nowDtail - 1].KRVPNO);
				}else{
					w_hnum5 = "";
				}
				if(BasedMethod.nvl(arrPRINT[nowDtail - 5].KLVPNO,"0") != "0"){
					w_knum1 = BasedMethod.nvl(arrPRINT[nowDtail - 5].KLVPNO);
				}else{
					w_knum1 = "";
				}
				if(BasedMethod.nvl(arrPRINT[nowDtail - 4].KLVPNO,"0") != "0"){
					w_knum2 = BasedMethod.nvl(arrPRINT[nowDtail - 4].KLVPNO);
				}else{
					w_knum2 = "";
				}
				if(BasedMethod.nvl(arrPRINT[nowDtail - 3].KLVPNO,"0") != "0"){
					w_knum3 = BasedMethod.nvl(arrPRINT[nowDtail - 3].KLVPNO);
				}else{
					w_knum3 = "";
				}
				if(BasedMethod.nvl(arrPRINT[nowDtail - 2].KLVPNO,"0") != "0"){
					w_knum4 = BasedMethod.nvl(arrPRINT[nowDtail - 2].KLVPNO);
				}else{
					w_knum4 = "";
				}
				if(BasedMethod.nvl(arrPRINT[nowDtail - 1].KLVPNO,"0") != "0"){
					w_knum5 = BasedMethod.nvl(arrPRINT[nowDtail - 1].KLVPNO);
				}else{
					w_knum5 = "";
				}
				
				pIndex = (currentCount + 1).toString();
				
				params["__format_id_number[" + pIndex + "]"] = "2";
				params["__format[" + pIndex + "].VLIN1"] = w_vlin1;
				params["__format[" + pIndex + "].VLIN2"] = w_vlin2;
				params["__format[" + pIndex + "].VLIN3"] = w_vlin3;
				params["__format[" + pIndex + "].VLIN4"] = w_vlin4;
				params["__format[" + pIndex + "].VLIN5"] = w_vlin5;
				params["__format[" + pIndex + "].CLS1"] = BasedMethod.nvl(arrPRINT[nowDtail - 5].MDCLSNMS);
				params["__format[" + pIndex + "].CLS2"] = BasedMethod.nvl(arrPRINT[nowDtail - 4].MDCLSNMS);
				params["__format[" + pIndex + "].CLS3"] = BasedMethod.nvl(arrPRINT[nowDtail - 3].MDCLSNMS);
				params["__format[" + pIndex + "].CLS4"] = BasedMethod.nvl(arrPRINT[nowDtail - 2].MDCLSNMS);
				params["__format[" + pIndex + "].CLS5"] = BasedMethod.nvl(arrPRINT[nowDtail - 1].MDCLSNMS);
				params["__format[" + pIndex + "].GDNM1"] = BasedMethod.nvl(arrPRINT[nowDtail - 5].GDNM);
				params["__format[" + pIndex + "].GDNM2"] = BasedMethod.nvl(arrPRINT[nowDtail - 4].GDNM);
				params["__format[" + pIndex + "].GDNM3"] = BasedMethod.nvl(arrPRINT[nowDtail - 3].GDNM);
				params["__format[" + pIndex + "].GDNM4"] = BasedMethod.nvl(arrPRINT[nowDtail - 2].GDNM);
				params["__format[" + pIndex + "].GDNM5"] = BasedMethod.nvl(arrPRINT[nowDtail - 1].GDNM);
				params["__format[" + pIndex + "].CST1"] = BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(arrPRINT[nowDtail - 5].TAXSAL, "1", "0", "0"));
				params["__format[" + pIndex + "].CST2"] = BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(arrPRINT[nowDtail - 4].TAXSAL, "1", "0", "0"));
				params["__format[" + pIndex + "].CST3"] = BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(arrPRINT[nowDtail - 3].TAXSAL, "1", "0", "0"));
				params["__format[" + pIndex + "].CST4"] = BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(arrPRINT[nowDtail - 2].TAXSAL, "1", "0", "0"));
				params["__format[" + pIndex + "].CST5"] = BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(arrPRINT[nowDtail - 1].TAXSAL, "1", "0", "0"));
				params["__format[" + pIndex + "].BSNUM1"] = BasedMethod.nvl(arrPRINT[nowDtail - 5].ZHVPNO);
				params["__format[" + pIndex + "].BSNUM2"] = BasedMethod.nvl(arrPRINT[nowDtail - 4].ZHVPNO);
				params["__format[" + pIndex + "].BSNUM3"] = BasedMethod.nvl(arrPRINT[nowDtail - 3].ZHVPNO);
				params["__format[" + pIndex + "].BSNUM4"] = BasedMethod.nvl(arrPRINT[nowDtail - 2].ZHVPNO);
				params["__format[" + pIndex + "].BSNUM5"] = BasedMethod.nvl(arrPRINT[nowDtail - 1].ZHVPNO);
				params["__format[" + pIndex + "].KZNUM1"] = BasedMethod.nvl(arrPRINT[nowDtail - 5].KZAN);
				params["__format[" + pIndex + "].KZNUM2"] = BasedMethod.nvl(arrPRINT[nowDtail - 4].KZAN);
				params["__format[" + pIndex + "].KZNUM3"] = BasedMethod.nvl(arrPRINT[nowDtail - 3].KZAN);
				params["__format[" + pIndex + "].KZNUM4"] = BasedMethod.nvl(arrPRINT[nowDtail - 2].KZAN);
				params["__format[" + pIndex + "].KZNUM5"] = BasedMethod.nvl(arrPRINT[nowDtail - 1].KZAN);
				params["__format[" + pIndex + "].KUNUM1"] = w_kunum1;
				params["__format[" + pIndex + "].KUNUM2"] = w_kunum2;
				params["__format[" + pIndex + "].KUNUM3"] = w_kunum3;
				params["__format[" + pIndex + "].KUNUM4"] = w_kunum4;
				params["__format[" + pIndex + "].KUNUM5"] = w_kunum5;
				params["__format[" + pIndex + "].UAMT1"] = w_uamt1;
				params["__format[" + pIndex + "].UAMT2"] = w_uamt2;
				params["__format[" + pIndex + "].UAMT3"] = w_uamt3;
				params["__format[" + pIndex + "].UAMT4"] = w_uamt4;
				params["__format[" + pIndex + "].UAMT5"] = w_uamt5;
				params["__format[" + pIndex + "].NNUM1"] = w_nnum1;
				params["__format[" + pIndex + "].NNUM2"] = w_nnum2;
				params["__format[" + pIndex + "].NNUM3"] = w_nnum3;
				params["__format[" + pIndex + "].NNUM4"] = w_nnum4;
				params["__format[" + pIndex + "].NNUM5"] = w_nnum5;
				params["__format[" + pIndex + "].HNUM1"] = w_hnum1;
				params["__format[" + pIndex + "].HNUM2"] = w_hnum2;
				params["__format[" + pIndex + "].HNUM3"] = w_hnum3;
				params["__format[" + pIndex + "].HNUM4"] = w_hnum4;
				params["__format[" + pIndex + "].HNUM5"] = w_hnum5;
				params["__format[" + pIndex + "].KNUM1"] = w_knum1;
				params["__format[" + pIndex + "].KNUM2"] = w_knum2;
				params["__format[" + pIndex + "].KNUM3"] = w_knum3;
				params["__format[" + pIndex + "].KNUM4"] = w_knum4;
				params["__format[" + pIndex + "].KNUM5"] = w_knum5;
				params["__format[" + pIndex + "].KHNUM1"] = BasedMethod.nvl(arrPRINT[nowDtail - 5].KHVPNO);
				params["__format[" + pIndex + "].KHNUM2"] = BasedMethod.nvl(arrPRINT[nowDtail - 4].KHVPNO);
				params["__format[" + pIndex + "].KHNUM3"] = BasedMethod.nvl(arrPRINT[nowDtail - 3].KHVPNO);
				params["__format[" + pIndex + "].KHNUM4"] = BasedMethod.nvl(arrPRINT[nowDtail - 2].KHVPNO);
				params["__format[" + pIndex + "].KHNUM5"] = BasedMethod.nvl(arrPRINT[nowDtail - 1].KHVPNO);
				
			}		
		
			ttl_bal = "前回繰越額";
			w_bal = singleton.g_customnumberformatter.numberFormat(BasedMethod.calcNum(BasedMethod.calcNum(view.ma041x.MEDIBAL, view.ma041x.STOREBAL), view.ma041x.DEVBAL), "1", "0", "0");
			//　計算結果を退避
			w_bal1 = w_bal;
			if(BasedMethod.asCurrency(w_bal) == 0){
				ttl_bal = "";
				w_bal = "";
			}
			w_amt  =  singleton.g_customnumberformatter.numberFormat(BasedMethod.calcNum(BasedMethod.calcNum(view.ma041x.MEDIAMT, view.ma041x.STOREAMT), view.ma041x.DEVAMT), "1", "0", "0");
			w_tax  =  singleton.g_customnumberformatter.numberFormat(BasedMethod.calcNum(BasedMethod.calcNum(view.ma041x.MEDITAX, view.ma041x.STORETAX), view.ma041x.DEVTAX), "1", "0", "0");
			w_namt =  singleton.g_customnumberformatter.numberFormat(BasedMethod.calcNum(BasedMethod.calcNum(view.ma041x.MEDIMONEY, view.ma041x.STOREMONEY), view.ma041x.DEVMONEY), "1", "0", "0");
			w_zamt =  singleton.g_customnumberformatter.numberFormat(BasedMethod.calcNum(BasedMethod.calcNum(view.ma041x.MEDINBAL, view.ma041x.STORENBAL), view.ma041x.DEVNBAL), "1", "0", "0");
			w_uamt =  singleton.g_customnumberformatter.numberFormat(BasedMethod.calcNum(BasedMethod.calcNum(view.ma041x.MEDIWITHTAX, view.ma041x.STOREWITHTAX), view.ma041x.DEVWITHTAX), "1", "0", "0");
			w_dis  =  singleton.g_customnumberformatter.numberFormat(BasedMethod.calcNum(w_uamt, w_amt, 1), "1", "0", "0");
			
			// 請求額に前回繰越加算
			w_amt  =  singleton.g_customnumberformatter.numberFormat(BasedMethod.calcNum(w_bal1,w_amt), "1", "0", "0");
			
			pIndex = (currentCount + 1).toString();
			
			params["__format_id_number[" + pIndex + "]"] = "3";
			params["__format[" + pIndex + "].UAMT"] = w_uamt;
			params["__format[" + pIndex + "].TTL_BAL"] = ttl_bal;
			params["__format[" + pIndex + "].BAL"] = w_bal;
			params["__format[" + pIndex + "].DIS"] = w_dis;
			params["__format[" + pIndex + "].AMT"] = w_amt;
			params["__format[" + pIndex + "].TAX"] = w_tax;
			params["__format[" + pIndex + "].NAMT"] = w_namt;
			params["__format[" + pIndex + "].ZAMT"] = w_zamt;
			params["__format[" + pIndex + "].TNO"] = getTNO();

			hts.send(params);
***/
// 15.01.19 SE-233 end			
// 15.03.19 SE-233 end			
		}

		private function printMA041X_body(e: ResultEvent=null): void
		{
			// 印刷成功時処理
			if((e != null)&&(e.result != ""))
			{
				if(e.result.response.result == "OK"){
					// 印刷失敗時 result == NG
				}else{
					singleton.sitemethod.dspJoinErrMsg(e.result.response.message,
						doF10Proc_last, "", "0", null, null, true);
					return;
				}
				//IEventDispatcher(e.currentTarget).removeEventListener(ResultEvent.RESULT, printMA041X_body);				
				//IEventDispatcher(e.currentTarget).removeEventListener(FaultEvent.FAULT, htsFault);				
			}
			
			var hts: HTTPService = new HTTPService();
			var nowDtail: int;
			
			hts.method = "GET";
			hts.addEventListener(ResultEvent.RESULT, printLoop2, false, 0, true);
			hts.addEventListener(FaultEvent.FAULT, htsFault, false, 0, true);
			hts.url = "http://localhost:8080/Format/Print";
			
			nowDtail = (currentCount + 1) * 5;
			
			// 12.10.03 SE-362 明細がない場合は行NOを印字しない start
			var w_vlin1: String;
			var w_vlin2: String;
			var w_vlin3: String;
			var w_vlin4: String;
			var w_vlin5: String;
			
			if(BasedMethod.nvl(arrPRINT[nowDtail - 5].VJAN) != ""){
				w_vlin1 = (nowDtail - 4).toString();
			}else{
				w_vlin1 = "";
			}
			if(BasedMethod.nvl(arrPRINT[nowDtail - 4].VJAN) != ""){
				w_vlin2 = (nowDtail - 3).toString();
			}else{
				w_vlin2 = "";
			}
			if(BasedMethod.nvl(arrPRINT[nowDtail - 3].VJAN) != ""){
				w_vlin3 = (nowDtail - 2).toString();
			}else{
				w_vlin3 = "";
			}
			if(BasedMethod.nvl(arrPRINT[nowDtail - 2].VJAN) != ""){
				w_vlin4 = (nowDtail - 1).toString();
			}else{
				w_vlin4 = "";
			}
			if(BasedMethod.nvl(arrPRINT[nowDtail - 1].VJAN) != ""){
				w_vlin5 = (nowDtail).toString();
			}else{
				w_vlin5 = "";
			}
			// 12.10.03 SE-362 明細がない場合は行NOを印字しない end			
			
			// 12.10.19 SE-233 ゼロを非表示にする start
			var w_kunum1: String;
			var w_kunum2: String;
			var w_kunum3: String;
			var w_kunum4: String;
			var w_kunum5: String;
			var w_uamt1: String;
			var w_uamt2: String;
			var w_uamt3: String;
			var w_uamt4: String;
			var w_uamt5: String;
			var w_nnum1: String;
			var w_nnum2: String;
			var w_nnum3: String;
			var w_nnum4: String;
			var w_nnum5: String;
			var w_hnum1: String;
			var w_hnum2: String;
			var w_hnum3: String;
			var w_hnum4: String;
			var w_hnum5: String;
			var w_knum1: String;
			var w_knum2: String;
			var w_knum3: String;
			var w_knum4: String;
			var w_knum5: String;
			
			if(BasedMethod.nvl(arrPRINT[nowDtail - 5].KUVPNO,"0") != "0"){
				w_kunum1 = BasedMethod.nvl(arrPRINT[nowDtail - 5].KUVPNO);
			}else{
				w_kunum1 = "";
			}
			if(BasedMethod.nvl(arrPRINT[nowDtail - 4].KUVPNO,"0") != "0"){
				w_kunum2 = BasedMethod.nvl(arrPRINT[nowDtail - 4].KUVPNO);
			}else{
				w_kunum2 = "";
			}
			if(BasedMethod.nvl(arrPRINT[nowDtail - 3].KUVPNO,"0") != "0"){
				w_kunum3 = BasedMethod.nvl(arrPRINT[nowDtail - 3].KUVPNO);
			}else{
				w_kunum3 = "";
			}
			if(BasedMethod.nvl(arrPRINT[nowDtail - 2].KUVPNO,"0") != "0"){
				w_kunum4 = BasedMethod.nvl(arrPRINT[nowDtail - 2].KUVPNO);
			}else{
				w_kunum4 = "";
			}
			if(BasedMethod.nvl(arrPRINT[nowDtail - 1].KUVPNO,"0") != "0"){
				w_kunum5 = BasedMethod.nvl(arrPRINT[nowDtail - 1].KUVPNO);
			}else{
				w_kunum5 = "";
			}
			if(BasedMethod.nvl(BasedMethod.calcNum(arrPRINT[nowDtail - 5].KUVPNO, arrPRINT[nowDtail - 5].TAXSAL, 2),"0") != "0"){
				w_uamt1 = BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(BasedMethod.calcNum(arrPRINT[nowDtail - 5].KUVPNO, arrPRINT[nowDtail - 5].TAXSAL, 2), "1", "0", "0"));
			}else{
				w_uamt1 = "";
			}
			if(BasedMethod.nvl(BasedMethod.calcNum(arrPRINT[nowDtail - 4].KUVPNO, arrPRINT[nowDtail - 4].TAXSAL, 2),"0") != "0"){
				w_uamt2 = BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(BasedMethod.calcNum(arrPRINT[nowDtail - 4].KUVPNO, arrPRINT[nowDtail - 4].TAXSAL, 2), "1", "0", "0"));
			}else{
				w_uamt2 = "";
			}
			if(BasedMethod.nvl(BasedMethod.calcNum(arrPRINT[nowDtail - 3].KUVPNO, arrPRINT[nowDtail - 3].TAXSAL, 2),"0") != "0"){
				w_uamt3 = BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(BasedMethod.calcNum(arrPRINT[nowDtail - 3].KUVPNO, arrPRINT[nowDtail - 3].TAXSAL, 2), "1", "0", "0"));
			}else{
				w_uamt3 = "";
			}
			if(BasedMethod.nvl(BasedMethod.calcNum(arrPRINT[nowDtail - 2].KUVPNO, arrPRINT[nowDtail - 2].TAXSAL, 2),"0") != "0"){
				w_uamt4 = BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(BasedMethod.calcNum(arrPRINT[nowDtail - 2].KUVPNO, arrPRINT[nowDtail - 2].TAXSAL, 2), "1", "0", "0"));
			}else{
				w_uamt4 = "";
			}
			if(BasedMethod.nvl(BasedMethod.calcNum(arrPRINT[nowDtail - 1].KUVPNO, arrPRINT[nowDtail - 1].TAXSAL, 2),"0") != "0"){
				w_uamt5 = BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(BasedMethod.calcNum(arrPRINT[nowDtail - 1].KUVPNO, arrPRINT[nowDtail - 1].TAXSAL, 2), "1", "0", "0"));
			}else{
				w_uamt5 = "";
			}
			if(BasedMethod.nvl(arrPRINT[nowDtail - 5].KNVPNO,"0") != "0"){
				w_nnum1 = BasedMethod.nvl(arrPRINT[nowDtail - 5].KNVPNO);
			}else{
				w_nnum1 = "";
			}
			if(BasedMethod.nvl(arrPRINT[nowDtail - 4].KNVPNO,"0") != "0"){
				w_nnum2 = BasedMethod.nvl(arrPRINT[nowDtail - 4].KNVPNO);
			}else{
				w_nnum2 = "";
			}
			if(BasedMethod.nvl(arrPRINT[nowDtail - 3].KNVPNO,"0") != "0"){
				w_nnum3 = BasedMethod.nvl(arrPRINT[nowDtail - 3].KNVPNO);
			}else{
				w_nnum3 = "";
			}
			if(BasedMethod.nvl(arrPRINT[nowDtail - 2].KNVPNO,"0") != "0"){
				w_nnum4 = BasedMethod.nvl(arrPRINT[nowDtail - 2].KNVPNO);
			}else{
				w_nnum4 = "";
			}
			if(BasedMethod.nvl(arrPRINT[nowDtail - 1].KNVPNO,"0") != "0"){
				w_nnum5 = BasedMethod.nvl(arrPRINT[nowDtail - 1].KNVPNO);
			}else{
				w_nnum5 = "";
			}
			if(BasedMethod.nvl(arrPRINT[nowDtail - 5].KRVPNO,"0") != "0"){
				w_hnum1 = BasedMethod.nvl(arrPRINT[nowDtail - 5].KRVPNO);
			}else{
				w_hnum1 = "";
			}
			if(BasedMethod.nvl(arrPRINT[nowDtail - 4].KRVPNO,"0") != "0"){
				w_hnum2 = BasedMethod.nvl(arrPRINT[nowDtail - 4].KRVPNO);
			}else{
				w_hnum2 = "";
			}
			if(BasedMethod.nvl(arrPRINT[nowDtail - 3].KRVPNO,"0") != "0"){
				w_hnum3 = BasedMethod.nvl(arrPRINT[nowDtail - 3].KRVPNO);
			}else{
				w_hnum3 = "";
			}
			if(BasedMethod.nvl(arrPRINT[nowDtail - 2].KRVPNO,"0") != "0"){
				w_hnum4 = BasedMethod.nvl(arrPRINT[nowDtail - 2].KRVPNO);
			}else{
				w_hnum4 = "";
			}
			if(BasedMethod.nvl(arrPRINT[nowDtail - 1].KRVPNO,"0") != "0"){
				w_hnum5 = BasedMethod.nvl(arrPRINT[nowDtail - 1].KRVPNO);
			}else{
				w_hnum5 = "";
			}
			if(BasedMethod.nvl(arrPRINT[nowDtail - 5].KLVPNO,"0") != "0"){
				w_knum1 = BasedMethod.nvl(arrPRINT[nowDtail - 5].KLVPNO);
			}else{
				w_knum1 = "";
			}
			if(BasedMethod.nvl(arrPRINT[nowDtail - 4].KLVPNO,"0") != "0"){
				w_knum2 = BasedMethod.nvl(arrPRINT[nowDtail - 4].KLVPNO);
			}else{
				w_knum2 = "";
			}
			if(BasedMethod.nvl(arrPRINT[nowDtail - 3].KLVPNO,"0") != "0"){
				w_knum3 = BasedMethod.nvl(arrPRINT[nowDtail - 3].KLVPNO);
			}else{
				w_knum3 = "";
			}
			if(BasedMethod.nvl(arrPRINT[nowDtail - 2].KLVPNO,"0") != "0"){
				w_knum4 = BasedMethod.nvl(arrPRINT[nowDtail - 2].KLVPNO);
			}else{
				w_knum4 = "";
			}
			if(BasedMethod.nvl(arrPRINT[nowDtail - 1].KLVPNO,"0") != "0"){
				w_knum5 = BasedMethod.nvl(arrPRINT[nowDtail - 1].KLVPNO);
			}else{
				w_knum5 = "";
			}
			// 12.10.19 SE-233 ゼロを非表示にする end

			hts.send({
				"__format_archive_url": "file:///sdcard/print/me045x.spfmtz",
				"__format_archive_update": "updateWithoutError",
				"__format_id_number": "2",
				"VLIN1": w_vlin1,
				"VLIN2": w_vlin2,
				"VLIN3": w_vlin3,
				"VLIN4": w_vlin4,
				"VLIN5": w_vlin5,
				"CLS1"		: BasedMethod.nvl(arrPRINT[nowDtail - 5].MDCLSNMS),
				"CLS2"		: BasedMethod.nvl(arrPRINT[nowDtail - 4].MDCLSNMS),
				"CLS3" 		: BasedMethod.nvl(arrPRINT[nowDtail - 3].MDCLSNMS),
				"CLS4" 		: BasedMethod.nvl(arrPRINT[nowDtail - 2].MDCLSNMS),
				"CLS5" 		: BasedMethod.nvl(arrPRINT[nowDtail - 1].MDCLSNMS),
				"GDNM1"	: BasedMethod.nvl(arrPRINT[nowDtail - 5].GDNM),
				"GDNM2"	: BasedMethod.nvl(arrPRINT[nowDtail - 4].GDNM),
				"GDNM3"	: BasedMethod.nvl(arrPRINT[nowDtail - 3].GDNM),
				"GDNM4"	: BasedMethod.nvl(arrPRINT[nowDtail - 2].GDNM),
				"GDNM5"	: BasedMethod.nvl(arrPRINT[nowDtail - 1].GDNM),
				"CST1" 		: BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(arrPRINT[nowDtail - 5].TAXSAL, "1", "0", "0")),
				"CST2" 		: BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(arrPRINT[nowDtail - 4].TAXSAL, "1", "0", "0")),
				"CST3" 		: BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(arrPRINT[nowDtail - 3].TAXSAL, "1", "0", "0")),
				"CST4" 		: BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(arrPRINT[nowDtail - 2].TAXSAL, "1", "0", "0")),
				"CST5" 		: BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(arrPRINT[nowDtail - 1].TAXSAL, "1", "0", "0")),
				"BSNUM1"	:BasedMethod.nvl(arrPRINT[nowDtail - 5].ZHVPNO),
				"BSNUM2"	:BasedMethod.nvl(arrPRINT[nowDtail - 4].ZHVPNO),
				"BSNUM3"	:BasedMethod.nvl(arrPRINT[nowDtail - 3].ZHVPNO),
				"BSNUM4"	:BasedMethod.nvl(arrPRINT[nowDtail - 2].ZHVPNO),
				"BSNUM5"	:BasedMethod.nvl(arrPRINT[nowDtail - 1].ZHVPNO),
				"KZNUM1"	:BasedMethod.nvl(arrPRINT[nowDtail - 5].KZAN),
				"KZNUM2"	:BasedMethod.nvl(arrPRINT[nowDtail - 4].KZAN),
				"KZNUM3"	:BasedMethod.nvl(arrPRINT[nowDtail - 3].KZAN),
				"KZNUM4"	:BasedMethod.nvl(arrPRINT[nowDtail - 2].KZAN),
				"KZNUM5"	:BasedMethod.nvl(arrPRINT[nowDtail - 1].KZAN),
				// 12.10.19 SE-233 ゼロを非表示にする start
				"KUNUM1"	:w_kunum1,
				"KUNUM2"	:w_kunum2,
				"KUNUM3"	:w_kunum3,
				"KUNUM4"	:w_kunum4,
				"KUNUM5"	:w_kunum5,
				"UAMT1"	:w_uamt1,
				"UAMT2"	:w_uamt2,
				"UAMT3"	:w_uamt3,
				"UAMT4"	:w_uamt4,
				"UAMT5"	:w_uamt5,
				"NNUM1"	:w_nnum1,
				"NNUM2"	:w_nnum2,
				"NNUM3"	:w_nnum3,
				"NNUM4"	:w_nnum4,
				"NNUM5"	:w_nnum5,
				"HNUM1"	:w_hnum1,
				"HNUM2"	:w_hnum2,
				"HNUM3"	:w_hnum3,
				"HNUM4"	:w_hnum4,
				"HNUM5"	:w_hnum5,
				"KNUM1"	:w_knum1,
				"KNUM2"	:w_knum2,
				"KNUM3"	:w_knum3,
				"KNUM4"	:w_knum4,
				"KNUM5"	:w_knum5,
//				"KUNUM1"	:BasedMethod.nvl(arrPRINT[nowDtail - 5].KUVPNO),
//				"KUNUM2"	:BasedMethod.nvl(arrPRINT[nowDtail - 4].KUVPNO),
//				"KUNUM3"	:BasedMethod.nvl(arrPRINT[nowDtail - 3].KUVPNO),
//				"KUNUM4"	:BasedMethod.nvl(arrPRINT[nowDtail - 2].KUVPNO),
//				"KUNUM5"	:BasedMethod.nvl(arrPRINT[nowDtail - 1].KUVPNO),
//				//				"UAMT1"	:singleton.g_customnumberformatter.numberFormat(BasedMethod.calcNum(BasedMethod.nvl(arrPRINT[nowDtail - 5].KUVPNO, "0"), BasedMethod.nvl(arrPRINT[nowDtail - 5].SAL, "0"), 2), "1", "0", "0"),
//				//				"UAMT2"	:singleton.g_customnumberformatter.numberFormat(BasedMethod.calcNum(BasedMethod.nvl(arrPRINT[nowDtail - 4].KUVPNO, "0"), BasedMethod.nvl(arrPRINT[nowDtail - 4].SAL, "0"), 2), "1", "0", "0"),
//				//				"UAMT3"	:singleton.g_customnumberformatter.numberFormat(BasedMethod.calcNum(BasedMethod.nvl(arrPRINT[nowDtail - 3].KUVPNO, "0"), BasedMethod.nvl(arrPRINT[nowDtail - 3].SAL, "0"), 2), "1", "0", "0"),
//				//				"UAMT4"	:singleton.g_customnumberformatter.numberFormat(BasedMethod.calcNum(BasedMethod.nvl(arrPRINT[nowDtail - 2].KUVPNO, "0"), BasedMethod.nvl(arrPRINT[nowDtail - 2].SAL, "0"), 2), "1", "0", "0"),
//				//				"UAMT5"	:singleton.g_customnumberformatter.numberFormat(BasedMethod.calcNum(BasedMethod.nvl(arrPRINT[nowDtail - 1].KUVPNO, "0"), BasedMethod.nvl(arrPRINT[nowDtail - 1].SAL, "0"), 2), "1", "0", "0"),
//				"UAMT1"	:BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(BasedMethod.calcNum(arrPRINT[nowDtail - 5].KUVPNO, arrPRINT[nowDtail - 5].TAXSAL, 2), "1", "0", "0")),
//				"UAMT2"	:BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(BasedMethod.calcNum(arrPRINT[nowDtail - 4].KUVPNO, arrPRINT[nowDtail - 4].TAXSAL, 2), "1", "0", "0")),
//				"UAMT3"	:BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(BasedMethod.calcNum(arrPRINT[nowDtail - 3].KUVPNO, arrPRINT[nowDtail - 3].TAXSAL, 2), "1", "0", "0")),
//				"UAMT4"	:BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(BasedMethod.calcNum(arrPRINT[nowDtail - 2].KUVPNO, arrPRINT[nowDtail - 2].TAXSAL, 2), "1", "0", "0")),
//				"UAMT5"	:BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(BasedMethod.calcNum(arrPRINT[nowDtail - 1].KUVPNO, arrPRINT[nowDtail - 1].TAXSAL, 2), "1", "0", "0")),
//				"NNUM1"	:BasedMethod.nvl(arrPRINT[nowDtail - 5].KNVPNO),
//				"NNUM2"	:BasedMethod.nvl(arrPRINT[nowDtail - 4].KNVPNO),
//				"NNUM3"	:BasedMethod.nvl(arrPRINT[nowDtail - 3].KNVPNO),
//				"NNUM4"	:BasedMethod.nvl(arrPRINT[nowDtail - 2].KNVPNO),
//				"NNUM5"	:BasedMethod.nvl(arrPRINT[nowDtail - 1].KNVPNO),
//				"HNUM1"	:BasedMethod.nvl(arrPRINT[nowDtail - 5].KRVPNO),
//				"HNUM2"	:BasedMethod.nvl(arrPRINT[nowDtail - 4].KRVPNO),
//				"HNUM3"	:BasedMethod.nvl(arrPRINT[nowDtail - 3].KRVPNO),
//				"HNUM4"	:BasedMethod.nvl(arrPRINT[nowDtail - 2].KRVPNO),
//				"HNUM5"	:BasedMethod.nvl(arrPRINT[nowDtail - 1].KRVPNO),
//				"KNUM1"	:BasedMethod.nvl(arrPRINT[nowDtail - 5].KLVPNO),
//				"KNUM2"	:BasedMethod.nvl(arrPRINT[nowDtail - 4].KLVPNO),
//				"KNUM3"	:BasedMethod.nvl(arrPRINT[nowDtail - 3].KLVPNO),
//				"KNUM4"	:BasedMethod.nvl(arrPRINT[nowDtail - 2].KLVPNO),
//				"KNUM5"	:BasedMethod.nvl(arrPRINT[nowDtail - 1].KLVPNO),
				// 12.10.19 SE-233 ゼロを非表示にする end
				"KHNUM1"	:BasedMethod.nvl(arrPRINT[nowDtail - 5].KHVPNO),
				"KHNUM2"	:BasedMethod.nvl(arrPRINT[nowDtail - 4].KHVPNO),
				"KHNUM3"	:BasedMethod.nvl(arrPRINT[nowDtail - 3].KHVPNO),
				"KHNUM4"	:BasedMethod.nvl(arrPRINT[nowDtail - 2].KHVPNO),
				"KHNUM5"	:BasedMethod.nvl(arrPRINT[nowDtail - 1].KHVPNO),
				"(発行枚数)": "1"
			});
		}
		
		private function printLoop2(e: ResultEvent=null): void
		{
			// カウントアップ
			currentCount++;
			
			if((e != null)&&(e.result != ""))
			{
				// 印刷成功時処理
				if(e.result.response.result == "OK"){
					// 印刷失敗時 result == NG
				}else{
					singleton.sitemethod.dspJoinErrMsg(e.result.response.message,
						doF10Proc_last, "", "0", null, null, true);
					return;
				}
				//IEventDispatcher(e.currentTarget).removeEventListener(ResultEvent.RESULT, printLoop2);				
				//IEventDispatcher(e.currentTarget).removeEventListener(FaultEvent.FAULT, htsFault);				
			}
			
			if(currentCount != reportCount)
				printMA041X_body();
			else
				printMA041X_futter();
		}
		
		private function printMA041X_futter(): void
		{
			var hts: HTTPService = new HTTPService();
			var ttl_bal: String;
			var w_bal: String;
			var w_bal1: String;
			var w_dis: String;
			var w_amt: String;
			var w_tax: String;
			var w_namt: String;
			var w_zamt: String;
			var w_uamt: String;
			
			hts.method = "GET";
			hts.addEventListener(ResultEvent.RESULT, printMA041X_last, false, 0, true);
			hts.addEventListener(FaultEvent.FAULT, htsFault, false, 0, true);
			hts.url = "http://localhost:8080/Format/Print";
			
			ttl_bal = "前回繰越額";
			w_bal = singleton.g_customnumberformatter.numberFormat(BasedMethod.calcNum(BasedMethod.calcNum(view.ma041x.MEDIBAL, view.ma041x.STOREBAL), view.ma041x.DEVBAL), "1", "0", "0");
			//　計算結果を退避
			w_bal1 = w_bal;
			if(BasedMethod.asCurrency(w_bal) == 0){
				ttl_bal = "";
				w_bal = "";
			}
			w_amt  =  singleton.g_customnumberformatter.numberFormat(BasedMethod.calcNum(BasedMethod.calcNum(view.ma041x.MEDIAMT, view.ma041x.STOREAMT), view.ma041x.DEVAMT), "1", "0", "0");
			w_tax  =  singleton.g_customnumberformatter.numberFormat(BasedMethod.calcNum(BasedMethod.calcNum(view.ma041x.MEDITAX, view.ma041x.STORETAX), view.ma041x.DEVTAX), "1", "0", "0");
			w_namt =  singleton.g_customnumberformatter.numberFormat(BasedMethod.calcNum(BasedMethod.calcNum(view.ma041x.MEDIMONEY, view.ma041x.STOREMONEY), view.ma041x.DEVMONEY), "1", "0", "0");
			w_zamt =  singleton.g_customnumberformatter.numberFormat(BasedMethod.calcNum(BasedMethod.calcNum(view.ma041x.MEDINBAL, view.ma041x.STORENBAL), view.ma041x.DEVNBAL), "1", "0", "0");
			w_uamt =  singleton.g_customnumberformatter.numberFormat(BasedMethod.calcNum(BasedMethod.calcNum(view.ma041x.MEDIWITHTAX, view.ma041x.STOREWITHTAX), view.ma041x.DEVWITHTAX), "1", "0", "0");
			w_dis  =  singleton.g_customnumberformatter.numberFormat(BasedMethod.calcNum(w_uamt, w_amt, 1), "1", "0", "0");

			// 請求額に前回繰越加算
			w_amt  =  singleton.g_customnumberformatter.numberFormat(BasedMethod.calcNum(w_bal1,w_amt), "1", "0", "0");

			hts.send({
				"__format_archive_url": "file:///sdcard/print/me045x.spfmtz",
				"__format_archive_update": "updateWithoutError",
				"__format_id_number": "3",
				"UAMT": w_uamt,
				"TTL_BAL": ttl_bal,
				"BAL": w_bal,
				"DIS": w_dis,
				"AMT": w_amt,
				"TAX": w_tax,
				"NAMT": w_namt,
				"ZAMT": w_zamt,
				"TNO": getTNO(),
				"(発行枚数)": "1"
			});
		}
		
		private function printMA041X_last(e: ResultEvent=null): void
		{
			if((e != null)&&(e.result != ""))
			{
				// 印刷成功時処理
				if(e.result.response.result == "OK"){
					// 印刷失敗時 result == NG
				}else{
					singleton.sitemethod.dspJoinErrMsg(e.result.response.message,
						doF10Proc_last, "", "0", null, null, true);
					return;
				}
				//IEventDispatcher(e.currentTarget).removeEventListener(ResultEvent.RESULT, printMA041X_last);				
				//IEventDispatcher(e.currentTarget).removeEventListener(FaultEvent.FAULT, htsFault);				
			}
			
			BasedMethod.execNextFunction(print_arg.func);
		}
		
		/**********************************************************
		 * btnMOTO クリック時処理.
		 * @author 12.07.03 SE-354
		 **********************************************************/
		public function btnMOTOClick(): void
		{
			view.stage.removeEventListener(KeyboardEvent.KEY_DOWN, onKeyDownHandler);
			
			view.navigator.pushView(MF010X, view.data);
		}

		// 12.10.12 SE-362 Redmine#3435 対応 start
		protected function onKeyDownHandler(e:KeyboardEvent):void
		{
			//e.preventDefault();			
			if(e.keyCode == Keyboard.BACK)
			{
				e.preventDefault();
				btnRTNClick();
			}
		}
		// 12.10.12 SE-362 Redmine#3435 対応 end

		/**********************************************************
		 * btnRTN クリック時処理.
		 * @author 12.07.03 SE-354
		 **********************************************************/
		public function btnRTNClick(): void
		{
			// 12.10.12 SE-362 Redmine#3435 対応 start
			// 確認画面の表示
			singleton.sitemethod.dspJoinErrMsg("Q0027", btnRTNClick_last, "", "1", null, btnRTNClick_last2);
			// 12.10.12 SE-362 Redmine#3435 対応 end
		}
		
		// 12.10.12 SE-362 Redmine#3435 対応 start
		private function btnRTNClick_last(): void
		{
			view.stage.removeEventListener(KeyboardEvent.KEY_DOWN, onKeyDownHandler);
			
			// 前画面に遷移
			view.navigator.popView();
		}
		
		private function btnRTNClick_last2(): void
		{
			// 無処理
		}
		// 12.10.12 SE-362 Redmine#3435 対応 end

		// 12.10.30 SE-233 Redmine#3576 対応 start
		/**********************************************************
		 * 前繰変更時処理
		 * @author  12.10.30 SE-233
		 **********************************************************/
		public function BALValidate(): void
		{

			// 12.11.01 SE-233 Redmine#3600対応 start
			view.edtMEDI1.text = BasedMethod.NumChk(view.edtMEDI1.text);
			view.edtSTORE1.text = BasedMethod.NumChk(view.edtSTORE1.text);
			view.edtDIV1.text = BasedMethod.NumChk(view.edtDIV1.text);
			// 12.11.01 SE-233 Redmine#3600対応 end
			
			view.edtMEDI3.text = BasedMethod.calcNum(view.edtMEDI1.text, view.edtMEDI2.text);
			view.edtSTORE3.text = BasedMethod.calcNum(view.edtSTORE1.text, view.edtSTORE2.text);
			view.edtDIV3.text = BasedMethod.calcNum(view.edtDIV1.text, view.edtDIV2.text);
			calcSum();
		}
		// 12.10.30 SE-233 Redmine#3576 対応 end

		/**********************************************************
		 * 合計算出
		 * @author 12.09.010 SE-362
		 **********************************************************/
		public function calcSum(): void
		{
			var f: CustomNumberFormatter = new CustomNumberFormatter();

			// 12.11.01 SE-233 Redmine#3600対応 start
			view.edtMEDI3.text = BasedMethod.NumChk(view.edtMEDI3.text);
			view.edtSTORE3.text = BasedMethod.NumChk(view.edtSTORE3.text);
			view.edtDIV3.text = BasedMethod.NumChk(view.edtDIV3.text);
			// 12.11.01 SE-233 Redmine#3600対応 end
			
			// 繰越額
			//			view.edtMEDI4.text = BasedMethod.calcNum(BasedMethod.calcNum(view.edtMEDI1.text, view.edtMEDI2.text), view.edtMEDI3.text, 1);
			//			view.edtSTORE4.text = BasedMethod.calcNum(BasedMethod.calcNum(view.edtSTORE1.text, view.edtSTORE2.text), view.edtSTORE3.text, 1);
			//			view.edtDIV4.text = BasedMethod.calcNum(BasedMethod.calcNum(view.edtDIV1.text, view.edtDIV2.text), view.edtDIV3.text, 1);
			view.ma041x.MEDINBAL = BasedMethod.calcNum(BasedMethod.calcNum(view.edtMEDI1.text, view.edtMEDI2.text), view.edtMEDI3.text, 1);
			view.ma041x.STORENBAL = BasedMethod.calcNum(BasedMethod.calcNum(view.edtSTORE1.text, view.edtSTORE2.text), view.edtSTORE3.text, 1);
			view.ma041x.DEVNBAL = BasedMethod.calcNum(BasedMethod.calcNum(view.edtDIV1.text, view.edtDIV2.text), view.edtDIV3.text, 1);
			
			// 合計項目
			view.edtSUM1.text = BasedMethod.calcNum(BasedMethod.calcNum(view.edtMEDI1.text, view.edtSTORE1.text), view.edtDIV1.text); 
			view.edtSUM2.text = BasedMethod.calcNum(BasedMethod.calcNum(view.edtMEDI2.text, view.edtSTORE2.text), view.edtDIV2.text);
			view.edtSUM3.text = BasedMethod.calcNum(BasedMethod.calcNum(view.edtMEDI3.text, view.edtSTORE3.text), view.edtDIV3.text);
			view.edtSUM4.text = BasedMethod.calcNum(BasedMethod.calcNum(view.edtMEDI4.text, view.edtSTORE4.text), view.edtDIV4.text);
			
		}
	}
}