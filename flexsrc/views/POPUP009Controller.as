package views
{
	import flash.data.SQLResult;
	import flash.data.SQLStatement;
	import flash.events.Event;
	import flash.events.GeolocationEvent;
	import flash.events.KeyboardEvent;
	import flash.events.LocationChangeEvent;
	import flash.events.SQLErrorEvent;
	import flash.events.SQLEvent;
	import flash.geom.Rectangle;
	import flash.media.StageWebView;
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	import flash.sensors.Geolocation;
	import flash.ui.Keyboard;
	
	import modules.base.BasedMethod;
	import modules.custom.CustomIterator;
	import modules.custom.sub.Iterator;
	import modules.dto.LJ003;
	import modules.sbase.SiteModule;
	
	import mx.collections.ArrayCollection;
	import mx.managers.PopUpManager;
	
	public class POPUP009Controller extends SiteModule
	{
		private var view:POPUP009;
		private var arrLJ003: ArrayCollection;

		private var stmt: SQLStatement;
		
		public var LJ003Record: Object;

		public var argTKCD: String;				// 得意先コード
		
		public function POPUP009Controller()
		{
			super();
		}
		
		override public function init():void
		{
			// フォーム宣言
			view = _view as POPUP009;

			cdsLJ003();
			
			super.init();
		}
		
//		override public function openDataSet_B():void
//		{
//			cdsLJ003();
//		}

		override public function setDispObject():void
		{
			view.grdW01.dataProvider = arrLJ003;

			super.setDispObject();
		}
		
		/**********************************************************
		 * SQLiteのselect
		 * @author 12.10.29 SE-233
		 **********************************************************/ 
		public function cdsLJ003(): void
		{
			// 変数の初期化
			arrLJ003 = null;
			
			stmt = new SQLStatement();
			//stmt.sqlConnection =  connection;
			stmt.sqlConnection =  singleton.sqlconnection;
			stmt.itemClass = LJ003;
			
			var w_sql: String = "SELECT A.* FROM LJ003 A WHERE TKCD='" + argTKCD + "'";

			w_sql = w_sql + " ORDER BY A.VNO DESC";
			stmt.text = w_sql;
			
			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, cdsLJ003Result);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);
			
			//実行
			stmt.execute();
		}
		
		protected function stmtErrorHandler(event:SQLErrorEvent):void
		{
			// TODO Auto-generated method stub
			// エラーの表示
			trace("SQLerr");
			doDSPProc_last();
		}
		
		/**
		 * SELECT処理結果
		 */
		private function cdsLJ003Result(event:SQLEvent):void {
			
			var result:SQLResult = stmt.getResult();
			
			stmt.removeEventListener(SQLEvent.RESULT, cdsLJ003Result);
			stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);
			stmt=null;
			
			arrLJ003 = new ArrayCollection(result.data);

			super.openDataSet_B();
		}
		
		/**********************************************************
		 * Backキー押下時処理.
		 * @author 12.10.29 SE-233
		 **********************************************************/
		protected function onKeyDownHandler(e:KeyboardEvent):void
		{
			e.preventDefault();
			
			btnCANCELClick();
		}

		/**********************************************************
		 * btnOK クリック時処理.
		 * @author 12.10.29 SE-233
		 **********************************************************/
		public function btnOKClick(): void
		{
			// 選択行情報を返す
			if(view.grdW01.selectedIndex!=-1){
				LJ003Record = arrLJ003[view.grdW01.selectedIndex];
			}

			PopUpManager.removePopUp(view);
		}
		
		/**********************************************************
		 * btnCANCEL クリック時処理.
		 * @author 12.10.29 SE-233
		 **********************************************************/
		public function btnCANCELClick(): void
		{
			PopUpManager.removePopUp(view);
		}
	}
}