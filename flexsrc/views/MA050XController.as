package views
{
	import flash.data.SQLConnection;
	import flash.data.SQLResult;
	import flash.data.SQLStatement;
	import flash.display.DisplayObjectContainer;
	import flash.events.KeyboardEvent;
	import flash.events.SQLErrorEvent;
	import flash.events.SQLEvent;
	import flash.events.IEventDispatcher;
	import flash.ui.Keyboard;
	import flash.utils.getTimer;
	
	import modules.base.BasedMethod;
	import modules.custom.AlertDialog;
	import modules.custom.CustomIterator;
	import modules.custom.sub.Iterator;
	import modules.datagrid.DataGridColumnRenderer;
	import modules.dto.LF012;
	import modules.dto.LM010;
	import modules.dto.LJ005;
	import modules.dto.MA050V01;
	import modules.dto.MA050V02;
	import modules.formatters.CustomNumberFormatter;
	import modules.sbase.SiteMethod;
	import modules.sbase.SiteModule;
	
	import mx.collections.ArrayCollection;
	import mx.core.ClassFactory;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	import mx.rpc.http.HTTPService;
	
	/**********************************************************
	 * MA050X 入金入力
	 * @author 12.09.06 SE-362
	 **********************************************************/
	public class MA050XController extends SiteModule
	{
		private var view:MA050X;
		private var g_cucd: String;			// 客先コード
		private var g_date: String;			// 訪問日付
		private var g_time: String;			// 訪問時間
		private var g_vseq: String;			// 伝票NO
		private var g_sysdate: String;		// システム日付
		private var arrMA050X1: ArrayCollection;
		private var arrMA050X2: ArrayCollection;
		private var arrMA050X2H: ArrayCollection;
		private var arrMA050X3: ArrayCollection;
		private var arrMA050X4: ArrayCollection;
	
		private var stmt: SQLStatement;

		public var tabdata: ArrayCollection;

		// 引数情報
		private var cdsMA050X1_arg: Object;
		private var cdsMA050X2_arg: Object;
		private var cdsMA050X3_arg: Object;
		private var cdsMA050X4_arg: Object;
		private var insMA050X1_arg: Object;
		private var updMA050X1_arg: Object;
		private var updMA050X2_arg: Object;
		private var print_arg: Object;

		private var kbndata: ArrayCollection;

		private var modifyFlag: int=0;					// 新規：0　修正：1

		public function MA050XController()
		{
			super();
		}
		
		/**********************************************************
		 * 初期処理
		 * @author  12.09.06 SE-362
		 **********************************************************/
		override public function init():void
		{
			// フォーム宣言
			view = _view as MA050X;

			// 変数設定
			g_cucd = view.data.TKCD;

			tabdata = new ArrayCollection([
				"配置薬履歴","別売履歴"
			]);

			// コンボデータ
			kbndata = new ArrayCollection([
				{KBN:"1",KBNNM:"配置薬"},{KBN:"2",KBNNM:"別売"}
			]);
			// 初期表示設定
			view.edtCUNM.text = view.data.TKNM;
			view.lcmbKBN.selectedItem = kbndata[0];
				
			//view.edtVSEQ.text = singleton.g_wsid + "3";
			
			// タブ制御
			view.tab.dataProvider = tabdata;
			view.tab.selectedIndex = 0;
			view.grdW02.visible = false;
			
			view.lcmbKBN.dataProvider = kbndata;
			
			// ボタンに色を付ける
			view.btnPRINT.setStyle("chromeColor", 0x55aaff);
			
			// 12.10.12 SE-362 Redmine#3435 対応 start
			//view.stage.addEventListener("keyDown", keyDownBack, false, 1);
			view.stage.addEventListener(KeyboardEvent.KEY_DOWN, onKeyDownHandler, false, 0, true);
			// 12.10.12 SE-362 Redmine#3435 対応 end

			super.init();
			
		}

		/**********************************************************
		 * 画面立ち上げ処理
		 * @author  12.09.06 SE-362
		 **********************************************************/
		override public function formShow_last():void
		{
			super.formShow_last();
			
			// 画面データ表示
			doDSPProc();
		}

		/**********************************************************
		 * ヘッダデータ取得
		 * @author  12.09.06 SE-362
		 **********************************************************/
		override public function openDataSet_H(): void
		{
			// 制御ﾏｽﾀ情報取得
			cdsSEIGYO(openDataSet_H_cld1);
		}

		private function openDataSet_H_cld1(): void
		{
			//cdsMA050X1(openDataSet_H_last);
			chkMA050X1(openDataSet_H_cld2);
		}
		
		private function openDataSet_H_cld2(): void
		{
			if(modifyFlag > 0){
				view.lblMODIFY.text = "修正";
				openDataSet_H_last();
			}else{
				view.lblMODIFY.text = "";
				cdsMA050X1(openDataSet_H_last);
			}
		}
		
		private function openDataSet_H_last(): void
		{
			// 担当者名取得
			cdsCHGCD2(arrSEIGYO[0].CHGCD, super.openDataSet_H);
		}
		
		/**********************************************************
		 * ボディデータ取得
		 * @author  12.09.06 SE-362
		 **********************************************************/
		override public function openDataSet_B(): void
		{
			cdsMA050X2H(openDataSet_B_cld1);
		}

		private function openDataSet_B_cld1(): void
		{
			cdsMA050X2(openDataSet_B_cld2);
		}
		
		private function openDataSet_B_cld2(): void
		{
			cdsMA050X3(openDataSet_B_last);
		}
		
		private function openDataSet_B_last(): void
		{
			//getTax(arrSEIGYO[0].SDDAY, super.openDataSet_B);
			getTax(view.data.HDAT, super.openDataSet_B);

			insLF013(g_cucd,"入金入力開始");
		}
		
		/**********************************************************
		 * 画面項目セット
		 * @author SE-354 
		 * @date 12.04.27
		 **********************************************************/
		override public function setDispObject(): void
		{
			super.setDispObject();
			
			// 伝票番号
			//view.edtVSEQ.text = (Number(arrSEIGYO[0].NVNO) + 1).toString();
			
			// 伝票番号退避
			//g_vseq = view.edtVSEQ.text;

			// 伝票番号
			view.edtVSEQ.text = g_vseq;
			
			// 12.11.06 SE-233 Redmine#3628 対応 start
			//getEra(arrSEIGYO[0].SDDAY);
			getEra(view.data.HDAT);
			// 12.11.06 SE-233 Redmine#3628 対応 start
			
			// 入力項目表示
			if(modifyFlag > 0){

				// 区分
				if(arrMA050X4[0].NCLS == "2"){
					view.lcmbKBN.selectedItem = kbndata[1];
				}
				
				// 医薬品
				view.edtMEDI1.text = arrMA050X4[0].MEDIBAL;
				view.edtMEDI2.text = arrMA050X4[0].MEDIDIS;
				view.edtMEDI3.text = arrMA050X4[0].MEDIMONEY;
				
				// 薬店
				view.edtSTORE1.text = arrMA050X4[0].STOREBAL;
				view.edtSTORE2.text = arrMA050X4[0].STOREDIS;
				view.edtSTORE3.text = arrMA050X4[0].STOREMONEY;
				
				// 医療具
				view.edtDIV1.text = arrMA050X4[0].DEVBAL;
				view.edtDIV2.text = arrMA050X4[0].DEVDIS;
				view.edtDIV3.text = arrMA050X4[0].DEVMONEY;

			} else {

				if(arrMA050X1 != null && arrMA050X1.length != 0){
					// 配置薬時
					if(view.lcmbKBN.selectedItem.KBN == "1"){
						// 医薬品
						//view.edtMEDI1.text = arrMA050X1[0].HMEDIKURI;
						view.edtMEDI1.text = String(Number(arrMA050X1[0].HMEDIKURI)
													+ Number(arrMA050X1[0].HMEDIAMT)
													+ Number(arrMA050X1[0].HMEDITAX)
													- Number(arrMA050X1[0].HMEDINY)
													- Number(arrMA050X1[0].HMEDIDIS)
													- Number(arrMA050X1[0].HMEDIKAS));
						//view.edtMEDI3.text = arrMA050X1[0].HMEDIKURI;
						view.edtMEDI3.text = String(Number(arrMA050X1[0].HMEDIKURI)
													+ Number(arrMA050X1[0].HMEDIAMT)
													+ Number(arrMA050X1[0].HMEDITAX)
													- Number(arrMA050X1[0].HMEDINY)
													- Number(arrMA050X1[0].HMEDIDIS)
													- Number(arrMA050X1[0].HMEDIKAS));
						// 薬店
						//view.edtSTORE1.text = arrMA050X1[0].HSTOREKURI;
						view.edtSTORE1.text = String(Number(arrMA050X1[0].HSTOREKURI)
													+ Number(arrMA050X1[0].HSTOREAMT)
													+ Number(arrMA050X1[0].HSTORETAX)
													- Number(arrMA050X1[0].HSTORENY)
													- Number(arrMA050X1[0].HSTOREDIS)
													- Number(arrMA050X1[0].HSTOREKAS));
						//view.edtSTORE3.text = arrMA050X1[0].HSTOREKURI;
						view.edtSTORE3.text = String(Number(arrMA050X1[0].HSTOREKURI)
													+ Number(arrMA050X1[0].HSTOREAMT)
													+ Number(arrMA050X1[0].HSTORETAX)
													- Number(arrMA050X1[0].HSTORENY)
													- Number(arrMA050X1[0].HSTOREDIS)
													- Number(arrMA050X1[0].HSTOREKAS));
						// 医療具
						//view.edtDIV1.text = arrMA050X1[0].HDEVKURI;
						view.edtDIV1.text	= String(Number(arrMA050X1[0].HDEVKURI)
													+ Number(arrMA050X1[0].HDEVAMT)
													+ Number(arrMA050X1[0].HDEVTAX)
													- Number(arrMA050X1[0].HDEVNY)
													- Number(arrMA050X1[0].HDEVDIS)
													- Number(arrMA050X1[0].HDEVKAS));
						//view.edtDIV3.text = arrMA050X1[0].HDEVKURI;
						view.edtDIV3.text	= String(Number(arrMA050X1[0].HDEVKURI)
													+ Number(arrMA050X1[0].HDEVAMT)
													+ Number(arrMA050X1[0].HDEVTAX)
													- Number(arrMA050X1[0].HDEVNY)
													- Number(arrMA050X1[0].HDEVDIS)
													- Number(arrMA050X1[0].HDEVKAS));
					// 別売時
					}else if(view.lcmbKBN.selectedItem.KBN == "2"){
						// 医薬品
						//view.edtMEDI1.text = arrMA050X1[0].GMEDIKURI;
						view.edtMEDI1.text = String(Number(arrMA050X1[0].GMEDIKURI)
													+ Number(arrMA050X1[0].GMEDIAMT)
													+ Number(arrMA050X1[0].GMEDITAX)
													- Number(arrMA050X1[0].GMEDINY)
													- Number(arrMA050X1[0].GMEDIDIS)
													- Number(arrMA050X1[0].GMEDIKAS));
						//view.edtMEDI3.text = arrMA050X1[0].GMEDIKURI;
						view.edtMEDI3.text = String(Number(arrMA050X1[0].GMEDIKURI)
													+ Number(arrMA050X1[0].GMEDIAMT)
													+ Number(arrMA050X1[0].GMEDITAX)
													- Number(arrMA050X1[0].GMEDINY)
													- Number(arrMA050X1[0].GMEDIDIS)
													- Number(arrMA050X1[0].GMEDIKAS));
						// 薬店
						//view.edtSTORE1.text = arrMA050X1[0].GSTOREKURI;
						view.edtSTORE1.text = String(Number(arrMA050X1[0].GSTOREKURI)
													+ Number(arrMA050X1[0].GSTOREAMT)
													+ Number(arrMA050X1[0].GSTORETAX)
													- Number(arrMA050X1[0].GSTORENY)
													- Number(arrMA050X1[0].GSTOREDIS)
													- Number(arrMA050X1[0].GSTOREKAS));
						//view.edtSTORE3.text = arrMA050X1[0].GSTOREKURI;
						view.edtSTORE3.text = String(Number(arrMA050X1[0].GSTOREKURI)
													+ Number(arrMA050X1[0].GSTOREAMT)
													+ Number(arrMA050X1[0].GSTORETAX)
													- Number(arrMA050X1[0].GSTORENY)
													- Number(arrMA050X1[0].GSTOREDIS)
													- Number(arrMA050X1[0].GSTOREKAS));
						// 医療具
						//view.edtDIV1.text = arrMA050X1[0].GDEVKURI;
						view.edtDIV1.text	= String(Number(arrMA050X1[0].GDEVKURI)
													+ Number(arrMA050X1[0].GDEVAMT)
													+ Number(arrMA050X1[0].GDEVTAX)
													- Number(arrMA050X1[0].GDEVNY)
													- Number(arrMA050X1[0].GDEVDIS)
													- Number(arrMA050X1[0].GDEVKAS));
						//view.edtDIV3.text = arrMA050X1[0].GDEVKURI;
						view.edtDIV3.text	= String(Number(arrMA050X1[0].GDEVKURI)
													+ Number(arrMA050X1[0].GDEVAMT)
													+ Number(arrMA050X1[0].GDEVTAX)
													- Number(arrMA050X1[0].GDEVNY)
													- Number(arrMA050X1[0].GDEVDIS)
													- Number(arrMA050X1[0].GDEVKAS));
					}else{
						// 1,2を入力してください
						singleton.sitemethod.dspJoinErrMsg("E0073", null, "エラー", "0", ["1", "2"]);
						return;
					}

					// 値引は0
					view.edtMEDI2.text = "0";
					view.edtSTORE2.text = "0";
					view.edtDIV2.text = "0";
					
				}else{
					// 該当レコードなし
					singleton.sitemethod.dspJoinErrMsg("E0020", btnRTNClick);
					return;
				}
			}
			
			// 明細情報
			view.grdW01.dataProvider = arrMA050X2;
			view.grdW02.dataProvider = arrMA050X3;
			
			if(arrMA050X2H != null && arrMA050X2H.length != 0){
				// 配置薬履歴ヘッダ設定
				view.lblHBAYI01.headerText = BasedMethod.nvl(arrMA050X2H[0].HBAYI01);
				view.lblHBAYI02.headerText = BasedMethod.nvl(arrMA050X2H[0].HBAYI02);
				view.lblHBAYI03.headerText = BasedMethod.nvl(arrMA050X2H[0].HBAYI03);
				view.lblHBAYI04.headerText = BasedMethod.nvl(arrMA050X2H[0].HBAYI04);
				view.lblHBAYI05.headerText = BasedMethod.nvl(arrMA050X2H[0].HBAYI05);
				view.lblHBAYI06.headerText = BasedMethod.nvl(arrMA050X2H[0].HBAYI06);
				view.lblHBAYI07.headerText = BasedMethod.nvl(arrMA050X2H[0].HBAYI07);
				view.lblHBAYI08.headerText = BasedMethod.nvl(arrMA050X2H[0].HBAYI08);
				view.lblHBAYI09.headerText = BasedMethod.nvl(arrMA050X2H[0].HBAYI09);
				view.lblHBAYI10.headerText = BasedMethod.nvl(arrMA050X2H[0].HBAYI10);
				view.lblHBAYI11.headerText = BasedMethod.nvl(arrMA050X2H[0].HBAYI11);
				view.lblHBAYI12.headerText = BasedMethod.nvl(arrMA050X2H[0].HBAYI12);
				view.lblHBAYI13.headerText = BasedMethod.nvl(arrMA050X2H[0].HBAYI13);
				view.lblHBAYI14.headerText = BasedMethod.nvl(arrMA050X2H[0].HBAYI14);
				view.lblHBAYI15.headerText = BasedMethod.nvl(arrMA050X2H[0].HBAYI15);
				view.lblHBAYI16.headerText = BasedMethod.nvl(arrMA050X2H[0].HBAYI16);
				view.lblHBAYI17.headerText = BasedMethod.nvl(arrMA050X2H[0].HBAYI17);
				view.lblHBAYI18.headerText = BasedMethod.nvl(arrMA050X2H[0].HBAYI18);
				view.lblHBAYI19.headerText = BasedMethod.nvl(arrMA050X2H[0].HBAYI19);
				view.lblHBAYI20.headerText = BasedMethod.nvl(arrMA050X2H[0].HBAYI20);
				view.lblHBAYI21.headerText = BasedMethod.nvl(arrMA050X2H[0].HBAYI21);
				view.lblHBAYI22.headerText = BasedMethod.nvl(arrMA050X2H[0].HBAYI22);
				view.lblHBAYI23.headerText = BasedMethod.nvl(arrMA050X2H[0].HBAYI23);
				view.lblHBAYI24.headerText = BasedMethod.nvl(arrMA050X2H[0].HBAYI24);
				view.lblHBAYI25.headerText = BasedMethod.nvl(arrMA050X2H[0].HBAYI25);
				view.lblHBAYI26.headerText = BasedMethod.nvl(arrMA050X2H[0].HBAYI26);
				view.lblHBAYI27.headerText = BasedMethod.nvl(arrMA050X2H[0].HBAYI27);
				view.lblHBAYI28.headerText = BasedMethod.nvl(arrMA050X2H[0].HBAYI28);
				view.lblHBAYI29.headerText = BasedMethod.nvl(arrMA050X2H[0].HBAYI29);
				view.lblHBAYI30.headerText = BasedMethod.nvl(arrMA050X2H[0].HBAYI30);
			}
			
//			for(var i:int=0; i<view.grdW01.columnsLength; i++){
//				if(view.grdW01.columns.getItemAt(i).dataField.substr(0, 5) == "HBAYI"){
//					view.grdW01.columns.getItemAt(i).itemRenderer = new ClassFactory(DataGridColumnRenderer);
//				}
//			}

			// 合計算出
			calcSum();

			// 修正時、見出し表示
//			if(modifyFlag > 0){
//				view.lblMODIFY.text = "修正";
//			}
		}
		
		/**********************************************************
		 * btnDEC クリック時処理.
		 * @author 13.01.25 SE-233
		 **********************************************************/
		public function btnDECClick(): void
		{
			// 入力チェック
			singleton.sitemethod.dspJoinErrMsg("I0007", btnDECClick_cld1, "", "1", [], btnDECClick_cld2);
		}
		private function btnDECClick_cld1(): void 
		{
			doF10Proc();
		}
		private function btnDECClick_cld2(): void 
		{
			return;
		}
		
		/**********************************************************
		 * データ更新(ヘッダ)
		 * @author 12.07.03 SE-354
		 **********************************************************/
		override public function updateDataSet_H(): void
		{
			// 訪問時間取得
			//g_time = BasedMethod.getTime();

			// 入金J更新
			//insMA050X1(updateDataSet_H_cld1);
			if(modifyFlag > 0){
				delMA050X1(updateDataSet_H_cld1);
			}else{
				updateDataSet_H_cld1();
			}
		}

		private function updateDataSet_H_cld1(): void
		{
			// 入金J更新
			insMA050X1(updateDataSet_H_cld2);
		}
		
		private function updateDataSet_H_cld2(): void
		{
			// 制御マスタ更新
			if(modifyFlag > 0){
				//updMA050X3(updateDataSet_H_last);
				updateDataSet_H_last();
			}else{
				updMA050X1(updateDataSet_H_last);
			}
		}
		
		private function updateDataSet_H_last(): void
		{
			// 訪問実績F更新
			updMA050X2(super.updateDataSet_H);
		}
		
		/**********************************************************
		 * execDecPLSQL
		 * @author 12.07.03 SE-354
		 **********************************************************/
		override public function execDecPLSQL(): void
		{
			// 伝票NoXXXXXXXが登録されました。
			//singleton.sitemethod.dspJoinErrMsg("I0004", execDecPLSQL_cld1, "", "0", ["伝票番号" + g_vseq]);
// 13.04.16 SE-233 start
//			execDecPLSQL_cld1();
			cdsSEIGYO(execDecPLSQL_cld1);
// 13.04.16 SE-233 end
		}

		private function execDecPLSQL_cld1(): void 
		{
			var w_date:Date = new Date();
			getEra(singleton.g_customdateformatter.dateDisplayFormat_Full(w_date), execDecPLSQL_cld1a);
		}
		

		private function execDecPLSQL_cld1a(): void
		{
			g_sysdate = getEra_ret.dat;

			getEra(g_date);
			
			if(BasedMethod.asCurrency(view.edtSUM3.text) != 0){
				singleton.sitemethod.dspJoinErrMsg("I0005", execDecPLSQL_cld11, "", "0", ["領収証"]);
				return;
			}else{
				execDecPLSQL_cld2();
			}
		}
	
		private function execDecPLSQL_cld11(): void
		{
			// 通しNO更新
			updTNO(execDecPLSQL_cld111);
		}
		
		private function execDecPLSQL_cld111(): void
		{
			// ログ出力
			insLF013(g_cucd,"領収証",g_vseq);
			// 領収書の発行
			printHeader(execDecPLSQL_cld2);
		}
		
		private function execDecPLSQL_cld2(): void
		{
			// 次回繰越があれば、売掛請求書発行
			if(BasedMethod.asCurrency(view.edtSUM4.text) != 0){
				singleton.sitemethod.dspJoinErrMsg("I0005", execDecPLSQL_cld21, "", "0", ["売掛請求書"]);
				return;
			}else{
				execDecPLSQL_cld3();
			}
		}
		
		private function execDecPLSQL_cld21(): void
		{
			// 通しNO更新
			updTNO(execDecPLSQL_cld211);
		}
		
		private function execDecPLSQL_cld211(): void
		{
			// ログ出力
			insLF013(g_cucd,"請求書",g_vseq);
			
			printHeader2(execDecPLSQL_cld3);
		}
		
		private function execDecPLSQL_cld3(): void
		{
//			if(BasedMethod.asCurrency(view.edtSUM3.text) != 0){
				singleton.sitemethod.dspJoinErrMsg("I0005", execDecPLSQL_cld31, "", "0", ["領収証(控)"]);
//				return;
//			}else{
//				execDecPLSQL_last();
//			}
		}
		
		private function execDecPLSQL_cld31(): void
		{
			// 通しNO更新
			updTNO(execDecPLSQL_cld311);
		}
		
		private function execDecPLSQL_cld311(): void
		{
			// ログ出力
			insLF013(g_cucd,"請求書 兼 領収書（会社控）",g_vseq);
			// 領収書控発行
			printHeader3(execDecPLSQL_last);
		}
		
		private function execDecPLSQL_last(): void
		{
			super.execDecPLSQL();

			insLF013(g_cucd,"入金入力終了");
			
			view.stage.removeEventListener(KeyboardEvent.KEY_DOWN, onKeyDownHandler);
			// 実績画面に遷移
			//btnRTNClick();
			view.navigator.popView();

		}
		
		//////////////////////////個別処理//////////////////////////////
		/**********************************************************
		 * 入金データ存在チェック
		 * @author 12.11.09 SE-233
		 **********************************************************/
		private function chkMA050X1(func: Function): void
		{
			// 引数情報
			cdsMA050X4_arg = new Object;
			cdsMA050X4_arg.func = func;
			
			stmt = new SQLStatement();
			stmt.sqlConnection =  singleton.sqlconnection;
			stmt.text= singleton.g_query_xml.entry.(@key == "qryMA050X4");
			stmt.itemClass = LJ005;
			
			// 得意先条件
			stmt.parameters[0] = g_cucd;
			stmt.parameters[1] = view.data.HDAT;
			stmt.parameters[2] = view.lcmbKBN.selectedItem.KBN;
			
			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, chkMA050X1Result, false, 0, true);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler, false, 0, true);
			
			//実行
			stmt.execute();
		}
		
		public function chkMA050X1Result(event: SQLEvent): void
		{	
			var w_arg: Object = cdsMA050X4_arg;
			var result:SQLResult = stmt.getResult();
			
			stmt.removeEventListener(SQLEvent.RESULT, chkMA050X1Result);				
			stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);				
			stmt=null;
			
			arrMA050X4 = new ArrayCollection(result.data);
			
			// データが存在する場合、修正モード
			if(arrMA050X4 != null && arrMA050X4.length != 0){
				modifyFlag = 1;
				g_vseq = arrMA050X4[0].VNO;
				g_date = arrMA050X4[0].HDAT;	
				g_time = arrMA050X4[0].HTIME;
			}else{
				modifyFlag = 0;
				//g_vseq = (Number(arrSEIGYO[0].NVNO) + 1).toString();
				g_vseq = getVNO("3");
				g_date = arrSEIGYO[0].SDDAY;
				g_time = BasedMethod.getTime();
			}
			
			getEra(g_date);
			
			// 次関数実行
			if(w_arg.func)
				w_arg.func();
		}

		/**********************************************************
		 * 入金情報取得
		 * @author 12.09.06 SE-362
		 **********************************************************/
		public function cdsMA050X1(func: Function=null): void
		{
			// 引数情報
			cdsMA050X1_arg = new Object;
			cdsMA050X1_arg.func = func;
			
			stmt = new SQLStatement();
			stmt.sqlConnection =  singleton.sqlconnection;
			stmt.parameters[0] = g_cucd;
			stmt.itemClass = LM010;
			stmt.text= singleton.g_query_xml.entry.(@key == "qryMA050X1");
			
			
			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, cdsMA050X1Result, false, 0, true);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler, false, 0, true);
			
			//実行
			stmt.execute();
		}
		
		public function cdsMA050X1Result(event: SQLEvent): void
		{
			var w_arg: Object = cdsMA050X1_arg;
			var result:SQLResult = stmt.getResult();
			
			stmt.removeEventListener(SQLEvent.RESULT, cdsMA050X1Result);				
			stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);				
			stmt=null;
			
			arrMA050X1 = new ArrayCollection(result.data);
			
			// 次関数実行
			if(w_arg.func)
				w_arg.func();
		}
		
		/**********************************************************
		 * ボディ情報取得(配置薬)
		 * @author 12.11.22 SE-233
		 **********************************************************/
		public function cdsMA050X2H(func: Function=null): void
		{
			// 引数情報
			cdsMA050X2_arg = new Object;
			cdsMA050X2_arg.func = func;
			
			stmt = new SQLStatement();
			stmt.sqlConnection =  singleton.sqlconnection;
			stmt.parameters[0] = g_cucd;
			stmt.itemClass = LF012;
			stmt.text= singleton.g_query_xml.entry.(@key == "qryMF010X3H");
			
			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, cdsMA050X2HResult, false, 0, true);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler, false, 0, true);
			
			//実行
			stmt.execute();
		}
		
		public function cdsMA050X2HResult(event: SQLEvent): void
		{
			var w_arg: Object = cdsMA050X2_arg;
			var result:SQLResult = stmt.getResult();
			
			stmt.removeEventListener(SQLEvent.RESULT, cdsMA050X2HResult);				
			stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);				
			stmt=null;
			
			arrMA050X2H = new ArrayCollection(result.data);
			
			// 次関数実行
			if(w_arg.func)
				w_arg.func();
		}
		
		/**********************************************************
		 * ボディ情報取得(配置薬)
		 * @author 12.09.06 SE-362
		 **********************************************************/
		public function cdsMA050X2(func: Function=null): void
		{
			// 引数情報
			cdsMA050X2_arg = new Object;
			cdsMA050X2_arg.func = func;
			
			stmt = new SQLStatement();
			stmt.sqlConnection =  singleton.sqlconnection;
			stmt.parameters[0] = g_cucd;
			stmt.itemClass = LF012;
			stmt.text= singleton.g_query_xml.entry.(@key == "qryMF010X3");
			
			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, cdsMA050X2Result, false, 0, true);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler, false, 0, true);
			
			//実行
			stmt.execute();
		}
		
		public function cdsMA050X2Result(event: SQLEvent): void
		{
			var w_arg: Object = cdsMA050X2_arg;
			var result:SQLResult = stmt.getResult();
			
			stmt.removeEventListener(SQLEvent.RESULT, cdsMA050X2Result);				
			stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);				
			stmt=null;		
			
			arrMA050X2 = new ArrayCollection(result.data);
			
			// 次関数実行
			if(w_arg.func)
				w_arg.func();
		}
		
		/**********************************************************
		 * ボディ情報取得(別売)
		 * @author 12.09.06 SE-362
		 **********************************************************/
		public function cdsMA050X3(func: Function=null): void
		{
			// 引数情報
			cdsMA050X3_arg = new Object;
			cdsMA050X3_arg.func = func;
			
			stmt = new SQLStatement();
			stmt.sqlConnection =  singleton.sqlconnection;
			stmt.parameters[0] = g_cucd;
			stmt.itemClass = MA050V02;
			stmt.text= singleton.g_query_xml.entry.(@key == "qryMA050X3");
			
			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, cdsMA050X3Result, false, 0, true);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler, false, 0, true);
			
			//実行
			stmt.execute();
		}
		
		public function cdsMA050X3Result(event: SQLEvent): void
		{
			var w_arg: Object = cdsMA050X3_arg;
			var result:SQLResult = stmt.getResult();
			
			stmt.removeEventListener(SQLEvent.RESULT, cdsMA050X3Result);				
			stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);				
			stmt=null;				
			
			arrMA050X3 = new ArrayCollection(result.data);
			
			// 次関数実行
			if(w_arg.func)
				w_arg.func();
		}
		
		/**********************************************************
		 * 入金ファイル削除
		 * @author 12.11.09 SE-233
		 **********************************************************/
		public function delMA050X1(func: Function=null): void
		{
			// 引数情報
			cdsMA050X1_arg = new Object;
			cdsMA050X1_arg.func = func;
			
			stmt = new SQLStatement();
			stmt.sqlConnection =  singleton.sqlconnection;
			stmt.text= singleton.g_query_xml.entry.(@key == "delMA050X1");
			stmt.parameters[0] = g_vseq;
			
			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, delMA050X1Result, false, 0, true);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler, false, 0, true);
			
			//実行
			stmt.execute();
		}
		
		public function delMA050X1Result(event: SQLEvent): void
		{	
			var w_arg: Object = cdsMA050X1_arg;
			
			stmt.removeEventListener(SQLEvent.RESULT, delMA050X1Result);				
			stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);				
			stmt=null;			
			
			// 次関数実行
			if(w_arg.func)
				w_arg.func();
		}
		
		/**********************************************************
		 * 入金ファイル更新
		 * @author 12.09.06 SE-362
		 **********************************************************/
		public function insMA050X1(func: Function=null): void
		{
			var w_tax: Number = Number(getTax_ret.tax);

			// 引数情報
			insMA050X1_arg = new Object;
			insMA050X1_arg.func = func;
			
			stmt = new SQLStatement();
			stmt.sqlConnection =  singleton.sqlconnection;
			stmt.text= singleton.g_query_xml.entry.(@key == "insMA050X1");
			
			// パラメタ情報
			// 伝票No
			stmt.parameters[0] = view.edtVSEQ.text;
			// 得意先コード
			stmt.parameters[1] = g_cucd;
			// 訪問日付			
			//stmt.parameters[2] = arrSEIGYO[0].SDDAY;
			stmt.parameters[2] = g_date;
			// 訪問時間 
			stmt.parameters[3] = g_time;
			// 担当者コード
			stmt.parameters[4] = arrSEIGYO[0].CHGCD;
			// 入金データ区分
			stmt.parameters[5] = view.lcmbKBN.selectedItem.KBN;
			// 端末番号
			stmt.parameters[6] = arrSEIGYO[0].HTID;
			
			// 【配置薬】
			// 前回繰越
			stmt.parameters[7] = BasedMethod.asCurrency(view.edtMEDI1.text);
			// 現金集金額
			stmt.parameters[8] = BasedMethod.asCurrency(view.edtMEDI3.text);
			// 値引額
			stmt.parameters[9] = BasedMethod.asCurrency(view.edtMEDI2.text);
			// 貸倒額
			stmt.parameters[10] = 0; 
			// 実税
			//stmt.parameters[11] = BasedMethod.asCurrency(view.edtMEDI3.text) * w_tax;
			stmt.parameters[11] = BasedMethod.calcNum(view.edtMEDI3.text,
				BasedMethod.calcNum(view.edtMEDI3.text,BasedMethod.calcNum(BasedMethod.calcNum(BasedMethod.nvl(String(getTax_ret.tax),"0"),"100"),"100",3,3),3,0,2),1);
			// 次回繰越
			stmt.parameters[12] = BasedMethod.asCurrency(view.edtMEDI4.text);
			
			// 【薬店】
			// 前回繰越
			stmt.parameters[13] = BasedMethod.asCurrency(view.edtSTORE1.text);
			// 現金集金額
			stmt.parameters[14] = BasedMethod.asCurrency(view.edtSTORE3.text);
			// 値引額
			stmt.parameters[15] = BasedMethod.asCurrency(view.edtSTORE2.text);
			// 貸倒額
			stmt.parameters[16] = 0;
			// 実税
			//stmt.parameters[17] = BasedMethod.asCurrency(view.edtSTORE3.text) * w_tax; 
			stmt.parameters[17] = BasedMethod.calcNum(view.edtSTORE3.text,
				BasedMethod.calcNum(view.edtSTORE3.text,BasedMethod.calcNum(BasedMethod.calcNum(BasedMethod.nvl(String(getTax_ret.tax),"0"),"100"),"100",3,3),3,0,2),1);
			// 次回繰越
			stmt.parameters[18] = BasedMethod.asCurrency(view.edtSTORE4.text);
			
			// 【医療具】
			// 前回繰越
			stmt.parameters[19] = BasedMethod.asCurrency(view.edtDIV1.text);
			// 現金集金額
			stmt.parameters[20] = BasedMethod.asCurrency(view.edtDIV3.text);
			// 値引額
			stmt.parameters[21] = BasedMethod.asCurrency(view.edtDIV2.text);
			// 貸倒額
			stmt.parameters[22] = 0; 
			// 実税
			//stmt.parameters[23] = BasedMethod.asCurrency(view.edtDIV3.text) * w_tax;
			stmt.parameters[23] = BasedMethod.calcNum(view.edtDIV3.text,
				BasedMethod.calcNum(view.edtDIV3.text,BasedMethod.calcNum(BasedMethod.calcNum(BasedMethod.nvl(String(getTax_ret.tax),"0"),"100"),"100",3,3),3,0,2),1);
			// 次回繰越
			stmt.parameters[24] = BasedMethod.asCurrency(view.edtDIV4.text);
			stmt.parameters[25] = view.data.HDAT;
			
			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, insMA050X1Result, false, 0, true);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler, false, 0, true);
			
			//実行
			stmt.execute();
		}
		
		public function insMA050X1Result(event: SQLEvent): void
		{	
			var w_arg: Object = insMA050X1_arg;
			var result:SQLResult = stmt.getResult();
			
			stmt.removeEventListener(SQLEvent.RESULT, insMA050X1Result);				
			stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);				
			stmt=null;	
			
			// 次関数実行
			if(w_arg.func)
				w_arg.func();
		}
		
		/**********************************************************
		 * 制御マスタ更新
		 * @author 12.09.06 SE-362
		 **********************************************************/
		public function updMA050X1(func: Function=null): void
		{
			// 引数情報
			updMA050X1_arg = new Object;
			updMA050X1_arg.func = func;
			
			stmt = new SQLStatement();
			stmt.sqlConnection =  singleton.sqlconnection;
			stmt.text= singleton.g_query_xml.entry.(@key == "updMA050X1");
			
			// パラメタ情報
			// 入金用伝票No
			stmt.parameters[0] = view.edtVSEQ.text;
			// 通しNo
			//stmt.parameters[1] = (Number(arrSEIGYO[0].TNO) + 1).toString();
			
			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, updMA050X1Result, false, 0, true);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler, false, 0, true);
			
			//実行
			stmt.execute();
		}
		
		public function updMA050X1Result(event: SQLEvent): void
		{	
			var w_arg: Object = updMA050X1_arg;
			var result:SQLResult = stmt.getResult();
			
			stmt.removeEventListener(SQLEvent.RESULT, updMA050X1Result);				
			stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);				
			stmt=null;		
			
			// 次関数実行
			if(w_arg.func)
				w_arg.func();
		}

		/**********************************************************
		 * 訪問実績F更新
		 * @author 12.09.06 SE-362
		 **********************************************************/
		public function updMA050X2(func: Function=null): void
		{
			// 引数情報
			updMA050X2_arg = new Object;
			updMA050X2_arg.func = func;
			
			stmt = new SQLStatement();
			stmt.sqlConnection =  singleton.sqlconnection;
			stmt.text= singleton.g_query_xml.entry.(@key == "updMA050X2");
			
			// パラメタ情報
			// 入金訪問時間
			stmt.parameters[0] = g_time;
			// 担当者コード
			stmt.parameters[1] = arrSEIGYO[0].CHGCD;
			// 訪問日付
			//stmt.parameters[2] = arrSEIGYO[0].SDDAY;
			//stmt.parameters[2] = g_date;
			stmt.parameters[2] = view.data.HDAT;
			// 得意先コード
			stmt.parameters[3] = g_cucd;
			
			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, updMA050X2Result, false, 0, true);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler, false, 0, true);
			
			//実行
			stmt.execute();
		}
		
		public function updMA050X2Result(event: SQLEvent): void
		{	
			var w_arg: Object = updMA050X2_arg;
			var result:SQLResult = stmt.getResult();
			
			stmt.removeEventListener(SQLEvent.RESULT, updMA050X2Result);				
			stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);				
			stmt=null;				
			
			// 次関数実行
			if(w_arg.func)
				w_arg.func();
		}

		/**********************************************************
		 * 制御マスタ更新
		 * @author 12.11.09 SE-233
		 **********************************************************/
		public function updMA050X3(func: Function=null): void
		{
			// 引数情報
			updMA050X1_arg = new Object;
			updMA050X1_arg.func = func;
			
			stmt = new SQLStatement();
			stmt.sqlConnection =  singleton.sqlconnection;
			stmt.text= singleton.g_query_xml.entry.(@key == "updMA050X3");
			
			// パラメタ情報
			// 通しNo
			stmt.parameters[0] = (Number(arrSEIGYO[0].TNO) + 1).toString();
			
			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, updMA050X3Result, false, 0, true);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler, false, 0, true);
			
			//実行
			stmt.execute();
		}
		
		public function updMA050X3Result(event: SQLEvent): void
		{	
			var w_arg: Object = updMA050X1_arg;
			var result:SQLResult = stmt.getResult();
			
			stmt.removeEventListener(SQLEvent.RESULT, updMA050X3Result);				
			stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);
			stmt=null;
			
			// 次関数実行
			if(w_arg.func)
				w_arg.func();
		}
		
		private function stmtErrorHandler(event: SQLErrorEvent): void
		{
			// エラーの表示
			trace(event.error.message);
		}
		
		/**********************************************************
		 * printHeader 領収書
		 * @author 12.09.11 SE-362
		 **********************************************************/
		private function printHeader(func: Function): void
		{
			var hts: HTTPService = new HTTPService();
			var w_tknm1: String;
			var w_tknm2: String;
			
			// 引数情報
			print_arg = new Object;
			print_arg.func = func;

/*			
			if(view.edtCUNM.text.length > 20){
				w_tknm1 = view.edtCUNM.text.slice(0, 20);
				w_tknm2 = view.edtCUNM.text.slice(20, view.edtCUNM.text.length);
			}else{
				w_tknm1 = view.edtCUNM.text;
				w_tknm2 = "";
			}
*/
			var tknm: Object = BasedMethod.getTKNM(view.data.TKNM);
			var s: String;
			
			if(BasedMethod.asCurrency(view.edtSUM3.text) == 0){
				s = "";
			}else{
				s = "￥" + view.edtSUM3.text + " -";
			}
			
			// 12.10.19 SE-233 Redmine#3479 対応 start
			var w_tax: String;
			w_tax = BasedMethod.calcNum(view.edtSUM3.text,
				BasedMethod.calcNum(view.edtSUM3.text,BasedMethod.calcNum(BasedMethod.calcNum(BasedMethod.nvl(String(getTax_ret.tax), "0"),"100"),"100",3,3),3,0,2),1);

			if(BasedMethod.asCurrency(w_tax) == 0){
				w_tax = "";
			}else{
				w_tax = "￥" + w_tax + " -";
			}
			// 12.10.19 SE-233 Redmine#3479 対応 end

			hts.method = "GET";
			hts.addEventListener(ResultEvent.RESULT, htsHeaderResult, false, 0, true);
			hts.addEventListener(FaultEvent.FAULT, htsFault, false, 0, true);
			hts.url = "http://localhost:8080/Format/Print";
			
			hts.send({
				"__format_archive_url": "file:///sdcard/print/me020x.spfmtz",
				"__format_archive_update": "updateWithoutError",
				"__format_id_number": "1",
				"TKCD": g_cucd,
				"TKNM1": tknm.tknm1,
				"TKNM2": tknm.tknm2,
// 13.4.1 SE-233 start
				// 12.11.06 SE-233 Redmine#3628 対応 start
//				"HDAT": getEra_ret.dat,
				// 12.11.06 SE-233 Redmine#3628 対応 end
				"HDAT": g_sysdate,
// 13.4.1 SE-233 end
				"VNO": g_vseq,
//				"CHGNM": arrCHGCD2[0].CHGNM,
				"HED1": arrSEIGYO[0].COSNM1,
				"HED2": arrSEIGYO[0].COSNM2,
				"HED3": arrSEIGYO[0].COSNM3,
				"ADR1": arrSEIGYO[0].ADD1,
				"ADR2": arrSEIGYO[0].ADD2,
				"ADR3": arrSEIGYO[0].ADD3,
				"ADR23": arrSEIGYO[0].ADD23,
				"TEL1": arrSEIGYO[0].TEL1,
				"TEL2": arrSEIGYO[0].TEL2,
				"TEL3": arrSEIGYO[0].TEL3,
				"AMT": s,
				// 12.10.19 SE-233 Redmine#3479 対応 start
				"TAX": w_tax,
				// 12.10.19 SE-233 Redmine#3479 対応 end
				"TNO": getTNO(),
				"(発行枚数)": "1"
			});
		}

		private function htsHeaderResult(event: ResultEvent): void
		{
			// 印刷成功時処理
			if(event.result.response.result == "OK"){
				// 印刷失敗時 result == NG
			}else{
				singleton.sitemethod.dspJoinErrMsg(event.result.response.message,
					doF10Proc_last, "", "0", null, null, true);
				return;
			}

			//IEventDispatcher(event.currentTarget).removeEventListener(ResultEvent.RESULT, htsHeaderResult);				
			//IEventDispatcher(event.currentTarget).removeEventListener(FaultEvent.FAULT, htsFault);				
			
			// 次関数実行
			BasedMethod.execNextFunction(print_arg.func);
		}
		

		/**********************************************************
		 * printHeader 売掛請求書
		 * @author 12.09.11 SE-362
		 **********************************************************/
		private function printHeader2(func: Function): void
		{
			var hts: HTTPService = new HTTPService();
			var w_tknm1: String;
			var w_tknm2: String;
			
			// 引数情報
			print_arg = new Object;
			print_arg.func = func;
			
			var tknm: Object = BasedMethod.getTKNM(view.data.TKNM);
			
			hts.method = "GET";
			hts.addEventListener(ResultEvent.RESULT, htsHeaderResult2, false, 0, true);
			hts.addEventListener(FaultEvent.FAULT, htsFault, false, 0, true);
			hts.url = "http://localhost:8080/Format/Print";
			
			hts.send({
				"__format_archive_url": "file:///sdcard/print/me021x.spfmtz",
				"__format_archive_update": "updateWithoutError",
				"__format_id_number": "1",
				"TKCD": g_cucd,
				"TKNM1": tknm.tknm1,
				"TKNM2": tknm.tknm2,
				// 12.11.06 SE-233 Redmine#3628 対応 start
				"HDAT": getEra_ret.dat + " " + g_time,
				// 12.11.06 SE-233 Redmine#3628 対応 end
				"VNO": g_vseq,
				"CHGNM": arrCHGCD2[0].CHGNM,
				"HED1": arrSEIGYO[0].COSNM1,
				"HED2": arrSEIGYO[0].COSNM2,
				"HED3": arrSEIGYO[0].COSNM3,
				"ADR1": arrSEIGYO[0].ADD1,
				"ADR2": arrSEIGYO[0].ADD2,
				"ADR3": arrSEIGYO[0].ADD3,
				"ADR23": arrSEIGYO[0].ADD23,
				"TEL1": arrSEIGYO[0].TEL1,
				"TEL2": arrSEIGYO[0].TEL2,
				"TEL3": arrSEIGYO[0].TEL3,
				"BAL"		: BasedMethod.nvl(view.edtSUM1.text),
				"UAMT"		: "0",
				"DIS"		: BasedMethod.nvl(view.edtSUM2.text),
				"NAMT"		: BasedMethod.nvl(view.edtSUM3.text),
				"KAMT"		: BasedMethod.nvl(view.edtSUM4.text),
				"BNKNM1"	: arrSEIGYO[0].BNKCD1,
				"BNKSNM1"	: arrSEIGYO[0].BNKSCD1,
				"VBNO1"	: arrSEIGYO[0].VBNO1,
				"BNKMEMO1"	: arrSEIGYO[0].BNKMEMO1,
				"BNKNM2"	: arrSEIGYO[0].BNKCD2,
				"BNKSNM2"	: arrSEIGYO[0].BNKSCD2,
				"VBNO2"	: arrSEIGYO[0].VBNO2,
				"BNKMEMO2"	: arrSEIGYO[0].BNKMEMO2,
				"BNKNM3"	: arrSEIGYO[0].BNKCD3,
				"BNKSNM3"	: arrSEIGYO[0].BNKSCD3,
				"VBNO3"	: arrSEIGYO[0].VBNO3,
				"BNKMEMO3"	: arrSEIGYO[0].BNKMEMO3,
				"TNO": getTNO(),
				"(発行枚数)": "1"
			});
		}
		
		private function htsHeaderResult2(event: ResultEvent): void
		{
			//IEventDispatcher(event.currentTarget).removeEventListener(ResultEvent.RESULT, htsHeaderResult2);				
			//IEventDispatcher(event.currentTarget).removeEventListener(FaultEvent.FAULT, htsFault);				
			
			// 次関数実行
			BasedMethod.execNextFunction(print_arg.func);
		}

		/**********************************************************
		 * printHeader 領収書 控
		 * @author 12.09.11 SE-362
		 **********************************************************/
		private function printHeader3(func: Function): void
		{
			var hts: HTTPService = new HTTPService();
			var w_tknm1: String;
			var w_tknm2: String;
			
			// 引数情報
			print_arg = new Object;
			print_arg.func = func;
			
			var tknm: Object = BasedMethod.getTKNM(view.data.TKNM);
			var s: String;
			
			if(BasedMethod.asCurrency(view.edtSUM3.text) == 0){
				s = "";
			}else{
				s = "￥" + view.edtSUM3.text + " -";
			}
			
			// 12.10.19 SE-233 Redmine#3479 対応 start
			var w_tax: String;
			w_tax = BasedMethod.calcNum(view.edtSUM3.text,
				BasedMethod.calcNum(view.edtSUM3.text,BasedMethod.calcNum(BasedMethod.calcNum(BasedMethod.nvl(String(getTax_ret.tax), "0"),"100"),"100",3,3),3,0,2),1);
			
			if(BasedMethod.asCurrency(w_tax) == 0){
				w_tax = "";
			}else{
				w_tax = "￥" + w_tax + " -";
			}
			// 12.10.19 SE-233 Redmine#3479 対応 end

			hts.method = "GET";
			hts.addEventListener(ResultEvent.RESULT, htsHeaderResult3, false, 0, true);
			hts.addEventListener(FaultEvent.FAULT, htsFault, false, 0, true);
			hts.url = "http://localhost:8080/Format/Print";
			
			hts.send({
				"__format_archive_url": "file:///sdcard/print/me025x.spfmtz",
				"__format_archive_update": "updateWithoutError",
				"__format_id_number": "1",
				"TKCD": g_cucd,
				"TKNM1": tknm.tknm1,
				"TKNM2": tknm.tknm2,
				// 12.11.06 SE-233 Redmine#3628 対応 start
				"HDAT": getEra_ret.dat + " " + g_time,
				// 12.11.06 SE-233 Redmine#3628 対応 end
				"VNO": g_vseq,
				"CHGNM": arrCHGCD2[0].CHGNM,
				"BAL"		: BasedMethod.nvl(view.edtSUM1.text),
				"UAMT"		: "0",
				"DIS"		: BasedMethod.nvl(view.edtSUM2.text),
				"NAMT"		: BasedMethod.nvl(view.edtSUM3.text),
				"KAMT"		: BasedMethod.nvl(view.edtSUM4.text),
//				"HED1": arrSEIGYO[0].COSNM1,
//				"HED2": arrSEIGYO[0].COSNM2,
//				"HED3": arrSEIGYO[0].COSNM3,
//				"ADR1": arrSEIGYO[0].ADD1,
//				"ADR2": arrSEIGYO[0].ADD2,
//				"ADR3": arrSEIGYO[0].ADD3,
//				"TEL1": arrSEIGYO[0].TEL1,
//				"TEL2": arrSEIGYO[0].TEL2,
//				"TEL3": arrSEIGYO[0].TEL3,
//				"AMT": s,
				// 12.10.19 SE-233 Redmine#3479 対応 start
//				"TAX": w_tax,
				// 12.10.19 SE-233 Redmine#3479 対応 end
				// 12.10.19 SE-233 Redmine#3480 対応 start
				"TNO": getTNO(),
				// 12.10.19 SE-233 Redmine#3480 対応 end
				"(発行枚数)": "1"
			});
		}
		
		private function htsHeaderResult3(event: ResultEvent): void
		{
			//IEventDispatcher(event.currentTarget).removeEventListener(ResultEvent.RESULT, htsHeaderResult3);				
			//IEventDispatcher(event.currentTarget).removeEventListener(FaultEvent.FAULT, htsFault);				
			
			// 次関数実行
			BasedMethod.execNextFunction(print_arg.func);
		}
		
		private function htsFault(event: FaultEvent): void
		{
			/*
			var Alert: AlertDialog = new AlertDialog();
			
			Alert.message = "サーバを起動してから再度実行して下さい。";
			Alert.btntype = "0";
			Alert.type = "2";
			Alert.open(DisplayObjectContainer(view), true);
			*/
			singleton.sitemethod.dspJoinErrMsg("印刷に失敗しました。サーバを起動してから再度実行して下さい。",
				doF10Proc_last, "", "0", null, null, true);
		}
		
		/**********************************************************
		 * 区分選択時
		 * @author 12.09.010 SE-362
		 **********************************************************/
		public function lcmbKBNClick(): void
		{
			doDSPProc();
		}

		/**********************************************************
		 * btnMOTO クリック時処理.
		 * @author 12.07.03 SE-354
		 **********************************************************/
		public function btnMOTOClick(): void
		{
			view.stage.removeEventListener(KeyboardEvent.KEY_DOWN, onKeyDownHandler);
			
			view.navigator.pushView(MF010X, view.data);
		}

		// 12.10.12 SE-362 Redmine#3435 対応 start
		protected function onKeyDownHandler(e:KeyboardEvent):void
		{
			//e.preventDefault();			
			if(e.keyCode == Keyboard.BACK)
			{
				e.preventDefault();
				btnRTNClick();
			}
		}
		// 12.10.12 SE-362 Redmine#3435 対応 end

		/**********************************************************
		 * btnRTN クリック時処理.
		 * @author 12.07.03 SE-354
		 **********************************************************/
		public function btnRTNClick(): void
		{
			// 12.10.12 SE-362 Redmine#3435 対応 start
			// 確認画面の表示
			singleton.sitemethod.dspJoinErrMsg("Q0027", btnRTNClick_last, "", "1", null, btnRTNClick_last2);
			// 12.10.12 SE-362 Redmine#3435 対応 end
		}
		
		// 12.10.12 SE-362 Redmine#3435 対応 start
		private function btnRTNClick_last(): void
		{
			view.stage.removeEventListener(KeyboardEvent.KEY_DOWN, onKeyDownHandler);
			
			insLF013(g_cucd,"入金入力終了");

			// 前画面に遷移
			view.navigator.popView();
		}
		
		private function btnRTNClick_last2(): void
		{
			// 無処理
		}
		// 12.10.12 SE-362 Redmine#3435 対応 end
		
		
		/**********************************************************
		 * tabClick Tabクリック時処理.
		 * @author 12.07.03 SE-362
		 **********************************************************/
		public function tabClick(): void
		{
			if(view.tab.selectedIndex == 0){
				view.grdW01.visible = true;
				view.grdW02.visible = false;
			}else{
				view.grdW01.visible = false;
				view.grdW02.visible = true;
			}
		}
		
		/**********************************************************
		 * 合計算出
		 * @author 12.09.010 SE-362
		 **********************************************************/
		public function calcSum(): void
		{
			var f: CustomNumberFormatter = new CustomNumberFormatter();

			// 12.11.01 SE-233 Redmine#3600対応 start
			view.edtMEDI1.text = BasedMethod.NumChk(view.edtMEDI1.text);
			view.edtMEDI2.text = BasedMethod.NumChk(view.edtMEDI2.text);
			view.edtMEDI3.text = BasedMethod.NumChk(view.edtMEDI3.text);
			view.edtSTORE1.text = BasedMethod.NumChk(view.edtSTORE1.text);
			view.edtSTORE2.text = BasedMethod.NumChk(view.edtSTORE2.text);
			view.edtSTORE3.text = BasedMethod.NumChk(view.edtSTORE3.text);
			view.edtDIV1.text = BasedMethod.NumChk(view.edtDIV1.text);
			view.edtDIV2.text = BasedMethod.NumChk(view.edtDIV2.text);
			view.edtDIV3.text = BasedMethod.NumChk(view.edtDIV3.text);
			// 12.11.01 SE-233 Redmine#3600対応 end
			
			// 繰越額
			view.edtMEDI4.text = f.numberFormat((BasedMethod.asCurrency(view.edtMEDI1.text) - BasedMethod.asCurrency(view.edtMEDI2.text) - BasedMethod.asCurrency(view.edtMEDI3.text)).toString(),"1","0","0");
			view.edtSTORE4.text = f.numberFormat((BasedMethod.asCurrency(view.edtSTORE1.text) - BasedMethod.asCurrency(view.edtSTORE2.text) - BasedMethod.asCurrency(view.edtSTORE3.text)).toString(),"1","0","0");
			view.edtDIV4.text = f.numberFormat((BasedMethod.asCurrency(view.edtDIV1.text) - BasedMethod.asCurrency(view.edtDIV2.text) - BasedMethod.asCurrency(view.edtDIV3.text)).toString(),"1","0","0");

			// 合計項目
			view.edtSUM1.text = f.numberFormat((BasedMethod.asCurrency(view.edtMEDI1.text) + BasedMethod.asCurrency(view.edtSTORE1.text) +BasedMethod.asCurrency(view.edtDIV1.text)).toString(),"1","0","0"); 
			view.edtSUM2.text = f.numberFormat((BasedMethod.asCurrency(view.edtMEDI2.text) + BasedMethod.asCurrency(view.edtSTORE2.text) +BasedMethod.asCurrency(view.edtDIV2.text)).toString(),"1","0","0");
			view.edtSUM3.text = f.numberFormat((BasedMethod.asCurrency(view.edtMEDI3.text) + BasedMethod.asCurrency(view.edtSTORE3.text) +BasedMethod.asCurrency(view.edtDIV3.text)).toString(),"1","0","0");
			view.edtSUM4.text = f.numberFormat((BasedMethod.asCurrency(view.edtSUM1.text) - BasedMethod.asCurrency(view.edtSUM2.text) - BasedMethod.asCurrency(view.edtSUM3.text)).toString(),"1","0","0");

		}
	}
}