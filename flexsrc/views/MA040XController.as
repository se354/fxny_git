package views
{
	import flash.data.SQLConnection;
	import flash.data.SQLResult;
	import flash.data.SQLStatement;
	import flash.events.KeyboardEvent;
	import flash.events.SQLErrorEvent;
	import flash.events.SQLEvent;
	import flash.events.IEventDispatcher;
	import flash.filesystem.File;
	import flash.globalization.NumberFormatter;
	import flash.ui.Keyboard;
	
	import modules.base.BasedMethod;
	import modules.custom.CustomIterator;
	import modules.custom.sub.Iterator;
	import modules.dto.LJ001;
	import modules.dto.W_MA040X;
	import modules.dto.W_MA041X;
	import modules.formatters.CustomNumberFormatter;
	import modules.sbase.SiteConst;
	import modules.sbase.SiteMethod;
	import modules.sbase.SiteModule;
	
	import mx.collections.ArrayCollection;
	import mx.collections.ModifiedCollectionView;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	import mx.rpc.http.HTTPService;
	
	/**********************************************************
	 * MA040X 配置薬入替(請求)
	 * @author 12.09.06 SE-362
	 **********************************************************/
	public class MA040XController extends SiteModule
	{
		private var view:MA040X;
		private var g_cucd: String;			// 客先コード
		// 12.10.31 SE-233 Redmine#3578 対応 start
		private var g_date: String;			// 訪問日付
		// 12.10.31 SE-233 Redmine#3578 対応 start
		private var g_time: String;			// 訪問時間
		private var g_vseq: String;			// 伝票NO
		private var popup: MP040X;
		private var g_updateCount: int;
		private var g_kuvpno_flag: int;
		
		private var stmt: SQLStatement;
		
		// 表示用配列
		private var arrMA040X1: ArrayCollection; // ヘッダ
		private var arrMA040X2: ArrayCollection; // 明細
		
		// 帳票用配列
		private var arrPRINT: ArrayCollection;
		
		private var printHeader_arg: Object;
		private var printBody_arg: Object;
		private var printFutter_arg: Object;
		
		// 帳票用変数
		private var reportCount: int=0;				// 帳票明細数
		private var currentCount: int=0;				// 現在印刷明細番号
		
		// 引数情報
		private var cdsMA040X1_arg: Object;
		private var cdsMA040X2_arg: Object;
		private var cdsMA040X3_arg: Object;
		// 14.03.19 SE-233 start
		private var updMA040X5_arg: Object;
		// 14.03.19 SE-233 end
		
		private var kbndata: ArrayCollection;
		
		private var modifyFlag: int=0;					// 新規：0　修正：1
		
		public function MA040XController()
		{
			super();
		}
		
		/**********************************************************
		 * 初期処理
		 * @author  12.09.06 SE-362
		 **********************************************************/
		override public function init():void
		{
			// フォーム宣言
			view = _view as MA040X;
			
			// 初期表示設定
			view.edtCUNM.text = view.data.TKNM;
			
			// 変数設定
			g_cucd = view.data.TKCD;
			
			// コンボデータ
			kbndata = new ArrayCollection([
				{KBN:"11",KBNNM:"入替"},{KBN:"13",KBNNM:"引上"}
			]);
			
			// 12.10.22 SE-233 Redmine#3474 対応 start
			view.btnSUM.enabled = true;
			view.btnCLEAR.enabled = false;
			view.btnPRINT.enabled = false;
			
			//view.edtMEDI1.enabled = false;
			//view.edtMEDI3.enabled = false;
			//view.edtSTORE1.enabled = false;
			//view.edtSTORE3.enabled = false;
			//view.edtDIV1.enabled = false;
			//view.edtDIV3.enabled = false;
			view.grp2.enabled = false;
			// 12.10.22 SE-233 Redmine#3474 対応 end
			view.btnMA042X.enabled = false;
			
			// ボタンに色を付ける
			view.btnPRINT.setStyle("chromeColor", 0x55aaff);
			
			// 12.10.12 SE-362 Redmine#3435 対応 start
			//view.stage.addEventListener("keyDown", keyDownBack, false, 1);
			view.stage.addEventListener(KeyboardEvent.KEY_DOWN, onKeyDownHandler, false, 0, true);
			// 12.10.12 SE-362 Redmine#3435 対応 end
			super.init();
			
		}
		
		/**********************************************************
		 * 画面立ち上げ処理
		 * @author  12.09.06 SE-362
		 **********************************************************/
		override public function formShow_last():void
		{
			super.formShow_last();
			
			// 画面データ表示
			doDSPProc();
		}
		
		/**********************************************************
		 * ヘッダデータ取得
		 * @author  12.09.06 SE-362
		 **********************************************************/
		override public function openDataSet_H(): void
		{
			// 制御ﾏｽﾀ情報取得
			cdsSEIGYO(openDataSet_H_cld1);
		}
		
		private function openDataSet_H_cld1(): void
		{
			// ヘッダワーク削除
			// 12.10.12 SE-362 Redmine#3425対応 start
			delMA040X1(openDataSet_H_cld11);
			// 12.10.12 SE-362 Redmine#3425対応 end
		}
		
		private function openDataSet_H_cld11(): void
		{
			// 新規 OR 修正モード判定
			chkMA040X1(openDataSet_H_cld2);
		}
		
		private function openDataSet_H_cld2(): void
		{
			//if(modifyFlag > 0){
			//	// 修正モード時、配置薬マスタからのデータ取得は行わない
			//	openDataSet_H_last();
			//	return;
			//}
			
			// ワーク作成
			wdsMA040X1(openDataSet_H_cld3);
		}
		
		private function openDataSet_H_cld3(): void
		{
			// 14.03.19 SE-233 start
			//			// ヘッダ情報
			//			cdsMA040X1(openDataSet_H_last);
			// 訪問日更新
			updMA040X5(openDataSet_H_cld4);
			// 14.03.19 SE-233 end
		}
		
		// 14.03.19 SE-233 start
		private function openDataSet_H_cld4(): void
		{
			// ヘッダ情報
			cdsMA040X1(openDataSet_H_last);
		}
		// 14.03.19 SE-233 end
		
		private function openDataSet_H_last(): void
		{
			// 担当者名取得
			// 12.10.11 SE-362 Redmine#3423 対応 start
			//			cdsCHGCD2(arrSEIGYO[0].CHGCD, super.openDataSet_H);
			cdsCHGCD2(view.data.TKCHGCD, super.openDataSet_H);
			// 12.10.11 SE-362 Redmine#3423 対応 end
		}
		
		/**********************************************************
		 * ボディデータ取得
		 * @author  12.09.06 SE-362
		 **********************************************************/
		override public function openDataSet_B(): void
		{
			// 明細情報
			// 12.10.12 SE-362 Redmine#3425対応 start
			delMA040X2(openDataSet_B_cld1);
			// 修正時、ワーク取得
			//if(modifyFlag > 0){
			//	cdsMA040X2(openDataSet_B_last);
			//	return;
			//}
			
			//openDataSet_B_cld1();
			// 12.10.12 SE-362 Redmine#3425対応 end
		}
		
		private function openDataSet_B_cld1(): void
		{
			// 明細ワーク作成
			wdsMA040X2(openDataSet_B_cld2);
		}
		
		private function openDataSet_B_cld2(): void
		{
			// 明細情報
			cdsMA040X2(openDataSet_B_last);
		}
		
		private function openDataSet_B_last(): void
		{
			super.openDataSet_B();
		}
		
		/**********************************************************
		 * 画面項目セット
		 * @author SE-354 
		 * @date 12.04.27
		 **********************************************************/
		override public function setDispObject(): void
		{
			super.setDispObject();
			
			// コンボセット
			view.lcmbKBN.selectedItem = kbndata[0];
			view.lcmbKBN.dataProvider = kbndata;
			
			// ヘッダ項目表示
			if(arrMA040X1 != null && arrMA040X1.length != 0){
				// 医薬品
				//				view.edtMEDI1.text = arrMA040X1[0].MEDIBAL;			// 前繰
				//				view.edtMEDI2.text = arrMA040X1[0].MEDIWITHOUTTAX;	// 使用
				view.ma040x = arrMA040X1[0];
				// 12.10.23 SE-233 Redmine#3478対応 start
				if(arrMA040X1[0].SCLS == "13"){
					view.lcmbKBN.selectedItem = kbndata[1];
				}
				// 12.10.23 SE-233 Redmine#3478対応 end
			}
			
			// 明細項目表示
			if(arrMA040X2 != null && arrMA040X2.length != 0){
				view.grp1.dataProvider = arrMA040X2;
			}else{
				singleton.sitemethod.dspJoinErrMsg("E0020", btnRTNClick_last);
				return;
			}
			
			// 伝票番号
			view.edtVSEQ.text = g_vseq;
			
			// 税率取得
			// 14.03.03 SE-233 start
			//			getTax(arrMA040X1[0].LHDAT);
			getTax(arrMA040X1[0].HDAT);
			// 14.03.03 SE-233 end
			
			// ヘッダ合計算出
			calcSum();
			
			// 修正時、見出し表示
			if(modifyFlag > 0){
				view.lblMODIFY.text = "修正";
				view.btnMA042X.enabled = true;
			}
			
			insLF013(g_cucd,"配置薬入替開始");
		}
		
		/**********************************************************
		 * btnMA042X クリック時処理.
		 * @author 12.11.08 SE-233
		 **********************************************************/
		public function doMA042X():void
		{
			var obj: Object = new Object;
			
			if(modifyFlag > 0){
				obj.vseq = g_vseq;
				obj.modifyFlag = modifyFlag;
				obj.tno = "";
				obj.date = g_date;	
				obj.time = g_time;
				
				// 入替に遷移
				view.navigator.pushView(MA042X, view.data, obj);
			}
		}
		
		/**********************************************************
		 * btnDEC クリック時処理.
		 * @author 13.01.25 SE-233
		 **********************************************************/
		public function btnDECClick(): void
		{
			// 入力チェック
			singleton.sitemethod.dspJoinErrMsg("I0007", btnDECClick_cld1, "", "1", [], btnDECClick_cld2);
		}
		private function btnDECClick_cld1(): void 
		{
			doF10Proc();
		}
		private function btnDECClick_cld2(): void 
		{
			return;
		}
		
		/**********************************************************
		 * execDecPLSQL
		 * @author 12.07.03 SE-354
		 **********************************************************/
		override public function execDecPLSQL():void
		{
			// 13.04.16 SE-233 start
			//// 12.10.09 SE-362 Redmine#3444 start
			//			// 使用数が未入力時は請求書を発行させない
			//			g_kuvpno_flag = 0;
			//			for(var i:int=0; i<arrMA040X2.length; i++){
			//				if(BasedMethod.asCurrency(arrMA040X2[i].KUVPNO) != 0){
			//					g_kuvpno_flag = 1;
			//					break;
			//				}
			//			}
			//// 12.10.09 SE-362 Redmine#3444 end
			//			
			//			if(view.grp1.enabled && g_kuvpno_flag > 0){
			//				singleton.sitemethod.dspJoinErrMsg("E0212");
			//				super.doF10Proc_last();
			//				return;
			//			}
			//			// ヘッダワーク更新
			//			updMA040X1(execDecPLSQL_cld1);
			//			//			updMA040X1(execDecPLSQL_last);
			//			
			//			g_updateCount = 0;
			cdsSEIGYO(execDecPLSQL_cld0);
			// 13.04.16 SE-233 end
		}
		
		// 13.04.16 SE-233 start
		private function execDecPLSQL_cld0(): void
		{
			// 使用数が未入力時は請求書を発行させない
			g_kuvpno_flag = 0;
			for(var i:int=0; i<arrMA040X2.length; i++){
				if(BasedMethod.asCurrency(arrMA040X2[i].KUVPNO) != 0){
					g_kuvpno_flag = 1;
					break;
				}
			}
			
			if(view.grp1.enabled && g_kuvpno_flag > 0){
				singleton.sitemethod.dspJoinErrMsg("E0212");
				super.doF10Proc_last();
				return;
			}
			// ヘッダワーク更新
			updMA040X1(execDecPLSQL_cld1);
			
			g_updateCount = 0;
		}
		// 13.04.16 SE-233 end
		
		private function execDecPLSQL_cld1(): void
		{
			// 明細ワーク一旦削除
			delMA040X2(execDecPLSQL_cld11);
		}
		
		private function execDecPLSQL_cld11(): void
		{
			// 明細ワーク更新
			insMA040X2(execDecPLSQL_countCheck);
		}
		
		private function execDecPLSQL_countCheck(): void
		{
			// カウントアップ
			g_updateCount++;
			
			if(g_updateCount == arrMA040X2.length){
				execDecPLSQL_cld2();
			}else{
				execDecPLSQL_cld11();
			}
		}
		
		private function execDecPLSQL_cld2(): void
		{
			// 12.10.09 SE-362 Redmine#3444 start
			if(g_kuvpno_flag == 0){
				execDecPLSQL_last();
				return;
			}
			// 12.10.09 SE-362 Redmine#3444 end
			singleton.sitemethod.dspJoinErrMsg("I0005", execDecPLSQL_cld3, "", "0", ["請求書"]);
		}
		
		private function execDecPLSQL_cld3(): void
		{			
			// 制御マスタ更新
			if(modifyFlag > 0){
				execDecPLSQL_cld4();
			}else{
				updMA040X3(execDecPLSQL_cld4);
			}
		}
		
		private function execDecPLSQL_cld4(): void
		{
			// 帳票用配列作成処理
			createPrintArray(execDecPLSQL_cld5);
		}
		
		private function execDecPLSQL_cld5(): void
		{
			if(reportCount == 0){
				execDecPLSQL_last();
				return;
			}
			// 通しNO更新
			updTNO(execDecPLSQL_cld51);
		}
		
		private function execDecPLSQL_cld51(): void
		{
			// ログ出力
			insLF013(g_cucd,"ご使用明細請求書",g_vseq);
			// 
			printHeader(execDecPLSQL_last);
		}
		
		private function execDecPLSQL_last(): void
		{
			super.execDecPLSQL();	
			
			var o: Object = new Object;
			
			o.vseq = g_vseq;
			o.modifyFlag = modifyFlag;
			// 12.10.31 SE-233 Redmine#3578 対応 start
			o.date = g_date;	
			o.time = g_time;
			// 12.10.31 SE-233 Redmine#3578 対応 end
			
			// リスナーの削除
			view.stage.removeEventListener(KeyboardEvent.KEY_DOWN, onKeyDownHandler);
			
			view.navigator.pushView(MA041X, view.data, o);
		}
		
		/**********************************************************
		 * ヘッダワーク削除
		 * @author 12.09.06 SE-362
		 **********************************************************/
		public function delMA040X1(func: Function=null): void
		{
			// 引数情報
			cdsMA040X1_arg = new Object;
			cdsMA040X1_arg.func = func;
			
			stmt = new SQLStatement();
			stmt.sqlConnection =  singleton.sqlconnection;
			stmt.text= singleton.g_query_xml.entry.(@key == "delMA040X1");
			
			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, delMA040X1Result, false, 0, true);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler, false, 0, true);
			
			//実行
			stmt.execute();
		}
		
		public function delMA040X1Result(event: SQLEvent): void
		{	
			var w_arg: Object = cdsMA040X1_arg;
			
			stmt.removeEventListener(SQLEvent.RESULT, delMA040X1Result);				
			stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);				
			stmt=null;
			
			// 次関数実行
			if(w_arg.func)
				w_arg.func();
		}
		
		/**********************************************************
		 * ワークデータ存在チェック
		 * @author 12.09.06 SE-362
		 **********************************************************/
		private function chkMA040X1(func: Function): void
		{
			// 引数情報
			cdsMA040X1_arg = new Object;
			cdsMA040X1_arg.func = func;
			
			stmt = new SQLStatement();
			stmt.sqlConnection =  singleton.sqlconnection;
			stmt.text= singleton.g_query_xml.entry.(@key == "cdsMA040X3");
			stmt.itemClass = LJ001;
			
			// 得意先条件
			stmt.parameters[0] = g_cucd;
			stmt.parameters[1] = view.data.HDAT;
			
			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, chkMA040X1Result, false, 0, true);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler, false, 0, true);
			
			//実行
			stmt.execute();
		}
		
		public function chkMA040X1Result(event: SQLEvent): void
		{	
			var w_arg: Object = cdsMA040X1_arg;
			var result:SQLResult = stmt.getResult();
			
			stmt.removeEventListener(SQLEvent.RESULT, chkMA040X1Result);				
			stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);				
			stmt=null;
			
			arrMA040X1 = new ArrayCollection(result.data);
			
			// データが存在する場合、修正モード
			if(arrMA040X1 != null && arrMA040X1.length != 0){
				modifyFlag = 1;
			}else{
				modifyFlag = 0;
			}
			
			// 12.10.31 SE-233 Redmine#3578 対応 start
			if(modifyFlag > 0){
				g_date = arrMA040X1[0].HDAT;	
				g_time = arrMA040X1[0].HTIME;
			} else {
				g_date = arrSEIGYO[0].SDDAY;
				g_time = BasedMethod.getTime();
			}
			// 12.10.31 SE-233 Redmine#3578 対応 end
			
			// 12.11.06 SE-233 Redmine#3628 対応 start
			getEra(g_date);
			// 12.11.06 SE-233 Redmine#3628 対応 start
			
			// 次関数実行
			if(w_arg.func)
				w_arg.func();
		}
		
		/**********************************************************
		 * ヘッダワーク作成
		 * @author 12.09.06 SE-362
		 **********************************************************/
		public function wdsMA040X1(func: Function=null): void
		{
			// 引数情報
			cdsMA040X1_arg = new Object;
			cdsMA040X1_arg.func = func;
			
			stmt = new SQLStatement();
			stmt.sqlConnection =  singleton.sqlconnection;
			if(modifyFlag > 0){
				stmt.text= singleton.g_query_xml.entry.(@key == "wdsMA040X3");
				stmt.parameters[0] = g_cucd;
				stmt.parameters[1] = view.data.HDAT;
			}else{
				stmt.text= singleton.g_query_xml.entry.(@key == "wdsMA040X1");
				stmt.parameters[0] = g_cucd;
			}
			stmt.itemClass = W_MA040X;
			
			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, wdsMA040X1Result, false, 0, true);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler, false, 0, true);
			
			//実行
			stmt.execute();
		}
		
		public function wdsMA040X1Result(event: SQLEvent): void
		{	
			var w_arg: Object = cdsMA040X1_arg;
			var result:SQLResult = stmt.getResult();
			
			stmt.removeEventListener(SQLEvent.RESULT, wdsMA040X1Result);				
			stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);				
			stmt=null;
			
			// 次関数実行
			if(w_arg.func)
				w_arg.func();
		}
		
		/**********************************************************
		 * 訪問日更新
		 * @author 14.03.19 SE-233
		 **********************************************************/
		public function updMA040X5(func: Function=null): void
		{
			// 引数情報
			updMA040X5_arg = new Object;
			updMA040X5_arg.func = func;
			
			stmt = new SQLStatement();
			stmt.sqlConnection =  singleton.sqlconnection;
			stmt.text= singleton.g_query_xml.entry.(@key == "updMA040X5");
			
			// パラメタ情報
			stmt.parameters[0] = view.data.HDAT;
			stmt.parameters[1] = g_cucd;
			
			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, updMA040X5Result, false, 0, true);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler, false, 0, true);
			
			//実行
			stmt.execute();
		}
		
		public function updMA040X5Result(event: SQLEvent): void
		{	
			var w_arg: Object = updMA040X5_arg;
			var result:SQLResult = stmt.getResult();
			
			stmt.removeEventListener(SQLEvent.RESULT, updMA040X5Result);				
			stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);				
			stmt=null;
			
			// 次関数実行
			if(w_arg.func)
				w_arg.func();
		}
		
		/**********************************************************
		 * ヘッダ情報取得
		 * @author 12.09.06 SE-362
		 **********************************************************/
		public function cdsMA040X1(func: Function=null): void
		{
			// 引数情報
			cdsMA040X1_arg = new Object;
			cdsMA040X1_arg.func = func;
			
			stmt = new SQLStatement();
			stmt.sqlConnection =  singleton.sqlconnection;
			stmt.text= singleton.g_query_xml.entry.(@key == "cdsMA040X1");
			stmt.itemClass = W_MA040X;
			
			stmt.parameters[0] = g_cucd;
			
			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, cdsMA040X1Result, false, 0, true);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler, false, 0, true);
			
			//実行
			stmt.execute();
		}
		
		public function cdsMA040X1Result(event: SQLEvent): void
		{	
			var w_arg: Object = cdsMA040X1_arg;
			var result:SQLResult = stmt.getResult();
			
			stmt.removeEventListener(SQLEvent.RESULT, cdsMA040X1Result);				
			stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);				
			stmt=null;
			
			arrMA040X1 = new ArrayCollection(result.data);
			
			//伝票番号退避
			if(modifyFlag > 0){
				g_vseq = arrMA040X1[0].VNO;
			}else{
				//g_vseq = (Number(arrSEIGYO[0].HVNO) + 1).toString();
				g_vseq = getVNO("1");
			}
			
			// 次関数実行
			if(w_arg.func)
				w_arg.func();
		}
		
		/**********************************************************
		 * ヘッダワーク更新
		 * @author 12.09.06 SE-362
		 **********************************************************/
		public function updMA040X1(func: Function=null): void
		{
			// 引数情報
			cdsMA040X1_arg = new Object;
			cdsMA040X1_arg.func = func;
			
			stmt = new SQLStatement();
			stmt.sqlConnection =  singleton.sqlconnection;
			stmt.text= singleton.g_query_xml.entry.(@key == "updMA040X1");
			
			// パラメタ情報
			stmt.parameters[0]  = g_vseq;
			stmt.parameters[1]  = g_cucd;
			stmt.parameters[2]  = "";
			stmt.parameters[3]  = "";
			stmt.parameters[4]  = view.lcmbKBN.selectedItem.KBN;
			stmt.parameters[5]  = view.ma040x.CHGCD;
			stmt.parameters[6]  = view.ma040x.HTID;
			stmt.parameters[7]  = view.ma040x.MEDIBAL;
			stmt.parameters[8]  = view.ma040x.MEDIWITHTAX;
			stmt.parameters[9]  = view.ma040x.MEDIWITHOUTTAX;
			stmt.parameters[10] = view.ma040x.MEDIAMT;
			stmt.parameters[11] = view.ma040x.MEDITAX;
			stmt.parameters[12] = view.ma040x.MEDIDIS;
			stmt.parameters[13] = view.ma040x.STOREBAL;
			stmt.parameters[14] = view.ma040x.STOREWITHTAX;
			stmt.parameters[15] = view.ma040x.STOREWITHOUTTAX;
			stmt.parameters[16] = view.ma040x.STOREAMT;
			stmt.parameters[17] = view.ma040x.STORETAX;
			stmt.parameters[18] = view.ma040x.STOREDIS;
			stmt.parameters[19] = view.ma040x.DEVBAL;
			stmt.parameters[20] = view.ma040x.DEVWITHTAX;
			stmt.parameters[21] = view.ma040x.DEVWITHOUTTAX;
			stmt.parameters[22] = view.ma040x.DEVAMT;
			stmt.parameters[23] = view.ma040x.DEVTAX;
			stmt.parameters[24] = view.ma040x.DEVDIS;
			// 今回値引率
			// 12.10.11 SE-362 Redmine#3431対応 start
			//stmt.parameters[25] = view.ma040x.MEDIDISRT;
			//stmt.parameters[26] = view.ma040x.STOREDISRT;
			//stmt.parameters[27] = view.ma040x.DEVDISRT;
			stmt.parameters[25] = view.ma040x.MEDIDISF;
			stmt.parameters[26] = view.ma040x.STOREDISF;
			stmt.parameters[27] = view.ma040x.DEVDISF;
			stmt.parameters[28] = view.ma040x.MEDIDISRT;
			stmt.parameters[29] = view.ma040x.STOREDISRT;
			stmt.parameters[30] = view.ma040x.DEVDISRT;
			stmt.parameters[31] = view.data.HDAT;
			stmt.parameters[32] = g_cucd;
			// 12.10.11 SE-362 Redmine#3431対応 end
			
			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, updMA040X1Result, false, 0, true);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler, false, 0, true);
			
			//実行
			stmt.execute();
		}
		
		public function updMA040X1Result(event: SQLEvent): void
		{	
			var w_arg: Object = cdsMA040X1_arg;
			var result:SQLResult = stmt.getResult();
			
			stmt.removeEventListener(SQLEvent.RESULT, updMA040X1Result);				
			stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);				
			stmt=null;
			
			// 次関数実行
			if(w_arg.func)
				w_arg.func();
		}
		
		/**********************************************************
		 * 明細ワーク削除
		 * @author 12.09.06 SE-362
		 **********************************************************/
		public function delMA040X2(func: Function=null): void
		{
			// 引数情報
			cdsMA040X2_arg = new Object;
			cdsMA040X2_arg.func = func;
			
			stmt = new SQLStatement();
			stmt.sqlConnection =  singleton.sqlconnection;
			stmt.text= singleton.g_query_xml.entry.(@key == "delMA040X2");
			
			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, delMA040X2Result, false, 0, true);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler, false, 0, true);
			
			//実行
			stmt.execute();
		}
		
		public function delMA040X2Result(event: SQLEvent): void
		{	
			var w_arg: Object = cdsMA040X2_arg;
			
			stmt.removeEventListener(SQLEvent.RESULT, delMA040X2Result);				
			stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);				
			stmt=null;
			
			// 次関数実行
			if(w_arg.func)
				w_arg.func();
		}
		
		/**********************************************************
		 * 明細ワーク作成
		 * @author 12.09.06 SE-362
		 **********************************************************/
		public function wdsMA040X2(func: Function=null): void
		{
			// 引数情報
			cdsMA040X2_arg = new Object;
			cdsMA040X2_arg.func = func;
			
			stmt = new SQLStatement();
			stmt.sqlConnection =  singleton.sqlconnection;
			if(modifyFlag > 0){
				stmt.text= singleton.g_query_xml.entry.(@key == "wdsMA040X4");
			}else{
				stmt.text= singleton.g_query_xml.entry.(@key == "wdsMA040X2");
			}
			stmt.itemClass = W_MA041X;
			stmt.parameters[0] = g_cucd;
			
			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, wdsMA040X2Result, false, 0, true);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler, false, 0, true);
			
			//実行
			stmt.execute();
		}
		
		public function wdsMA040X2Result(event: SQLEvent): void
		{	
			var w_arg: Object = cdsMA040X2_arg;
			var result:SQLResult = stmt.getResult();
			
			stmt.removeEventListener(SQLEvent.RESULT, wdsMA040X2Result);				
			stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);				
			stmt=null;
			
			// 次関数実行
			if(w_arg.func)
				w_arg.func();
		}
		
		/**********************************************************
		 * 明細情報取得
		 * @author 12.09.06 SE-362
		 **********************************************************/
		public function cdsMA040X2(func: Function=null): void
		{
			// 引数情報
			cdsMA040X2_arg = new Object;
			cdsMA040X2_arg.func = func;
			
			stmt = new SQLStatement();
			stmt.sqlConnection =  singleton.sqlconnection;
			stmt.text= singleton.g_query_xml.entry.(@key == "cdsMA040X2");
			stmt.itemClass = W_MA041X;
			stmt.parameters[0] = g_cucd;
			
			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, cdsMA040X2Result, false, 0, true);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler, false, 0, true);
			
			//実行
			stmt.execute();
		}
		
		public function cdsMA040X2Result(event: SQLEvent): void
		{	
			var w_arg: Object = cdsMA040X2_arg;
			var result:SQLResult = stmt.getResult();
			
			stmt.removeEventListener(SQLEvent.RESULT, cdsMA040X2Result);				
			stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);				
			stmt=null;
			
			arrMA040X2 = new ArrayCollection(result.data);
			
			// 12.10.24 SE-233 Redmine#3535 start
			for(var i:int = 0; i < arrMA040X2.length; i++){
				arrMA040X2[i].VSEQ = (i + 1).toString();
			}
			// 12.10.24 SE-233 Redmine#3535 end
			
			// 次関数実行
			if(w_arg.func)
				w_arg.func();
		}
		
		/**********************************************************
		 * 明細ワーク更新
		 * @author 12.09.06 SE-362
		 **********************************************************/
		public function insMA040X2(func: Function=null): void
		{
			// 引数情報
			cdsMA040X2_arg = new Object;
			cdsMA040X2_arg.func = func;
			
			stmt = new SQLStatement();
			stmt.sqlConnection =  singleton.sqlconnection;
			stmt.text= singleton.g_query_xml.entry.(@key == "insMA040X2");
			
			var w_knvpno: String;		// 納品数
			var w_krvpno: String;		// 返品数
			var w_klvpno: String;		// 期限切数
			
			if(BasedMethod.asCurrency(arrMA040X2[g_updateCount].KUVPNO) < 0){
				w_knvpno = "0";
			}else{
				// 12.10.09 SE-362 Redmine#3442対応 start
				//				w_knvpno = arrMA040X2[g_updateCount].KUVPNO;
				w_knvpno = "0";
				// 12.10.09 SE-362 Redmine#3442対応 end
			}
			
			w_klvpno = "0";
			w_krvpno = "0";
			if(modifyFlag > 0){
				w_knvpno = arrMA040X2[g_updateCount].KNVPNO;
				w_krvpno = arrMA040X2[g_updateCount].KRVPNO;
				w_klvpno = arrMA040X2[g_updateCount].KLVPNO;
			}
			
			// 12.11.02 SE-233 Redmine#3601対応 start
			if(view.lcmbKBN.selectedItem.KBN == "13"){
				w_knvpno = "0";
				w_krvpno = arrMA040X2[g_updateCount].KZAN;
			}
			// 12.11.02 SE-233 Redmine#3601対応 end
			
			// パラメタ情報
			stmt.parameters[0]  = g_cucd;
			stmt.parameters[1]  = g_vseq;
			stmt.parameters[2]  = arrMA040X2[g_updateCount].VSEQ;
			stmt.parameters[3]  = arrMA040X2[g_updateCount].BCLS;
			stmt.parameters[4]  = arrMA040X2[g_updateCount].SCLS;
			stmt.parameters[5]  = arrMA040X2[g_updateCount].GDCLS;
			stmt.parameters[6]  = arrMA040X2[g_updateCount].MDCLS;
			stmt.parameters[7]  = arrMA040X2[g_updateCount].MDCLSNMS;
			stmt.parameters[8]  = arrMA040X2[g_updateCount].VJAN;
			stmt.parameters[9]  = arrMA040X2[g_updateCount].GDNM;
			stmt.parameters[10]  = arrMA040X2[g_updateCount].SAL;
			stmt.parameters[11]  = arrMA040X2[g_updateCount].TAXSAL;
			//			stmt.parameters[8]  = "0";
			// 12.10.23 SE-233 Redmine#3492対応 start
			//			stmt.parameters[9]  = BasedMethod.calcNum(arrMA040X2[g_updateCount].SAL, BasedMethod.calcNum(arrMA040X2[g_updateCount].KHVPNO, arrMA040X2[g_updateCount].KUVPNO), 2);
			stmt.parameters[12]  = BasedMethod.calcNum(arrMA040X2[g_updateCount].SAL, arrMA040X2[g_updateCount].KUVPNO, 2);
			//			stmt.parameters[10]  = BasedMethod.calcNum(arrMA040X2[g_updateCount].TAXSAL, BasedMethod.calcNum(arrMA040X2[g_updateCount].KHVPNO, arrMA040X2[g_updateCount].KUVPNO), 2);
			stmt.parameters[13]  = BasedMethod.calcNum(arrMA040X2[g_updateCount].TAXSAL, arrMA040X2[g_updateCount].KUVPNO, 2);
			// 12.10.23 SE-233 Redmine#3492対応 end
			stmt.parameters[14]  = arrMA040X2[g_updateCount].ZHVPNO;	//前回配置数
			stmt.parameters[15] = arrMA040X2[g_updateCount].KZAN;		//今回残数
			stmt.parameters[16] = arrMA040X2[g_updateCount].KUVPNO;		//今回使用数
			//			stmt.parameters[14] = arrMA040X2[g_updateCount].KNVPNO; 納品数の初期表示は使用数
			//			stmt.parameters[14] = arrMA040X2[g_updateCount].KUVPNO; 使用数がマイナスの場合、0
			stmt.parameters[17] = w_knvpno;								//今回納品数
			// 12.10.23 SE-233 Redmine#3478対応 start
			//			stmt.parameters[15] = arrMA040X2[g_updateCount].KRVPNO;
			stmt.parameters[18] = w_krvpno;								// 今回返品数
			// 12.10.23 SE-233 Redmine#3478対応 end
			stmt.parameters[19] = w_klvpno;								// 今回期限切数
			//			stmt.parameters[17] = BasedMethod.calcNum(arrMA040X2[g_updateCount].KHVPNO, arrMA040X2[g_updateCount].KUVPNO);
			//			stmt.parameters[17] = arrMA040X2[g_updateCount].ZHVPNO;
			// 12.10.23 SE-233 Redmine#3478対応 start
			//			stmt.parameters[17] = BasedMethod.calcNum(arrMA040X2[g_updateCount].ZHVPNO, arrMA040X2[g_updateCount].KUVPNO, 1);
			//			stmt.parameters[17] = BasedMethod.calcNum(arrMA040X2[g_updateCount].KNVPNO, BasedMethod.calcNum(arrMA040X2[g_updateCount].KZAN, w_krvpno, 1));
			stmt.parameters[20] = BasedMethod.calcNum(w_knvpno, BasedMethod.calcNum(arrMA040X2[g_updateCount].KZAN, BasedMethod.calcNum(w_krvpno, w_klvpno),1));		// 今回配置数
			// 12.10.23 SE-233 Redmine#3478対応 end
			stmt.parameters[21] = arrMA040X2[g_updateCount].GDCOM;
			stmt.parameters[22] = arrMA040X2[g_updateCount].BHISK;
			
			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, updMA040X2Result, false, 0, true);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler, false, 0, true);
			
			//実行
			stmt.execute();
		}
		
		public function updMA040X2Result(event: SQLEvent): void
		{	
			var w_arg: Object = cdsMA040X2_arg;
			var result:SQLResult = stmt.getResult();
			
			stmt.removeEventListener(SQLEvent.RESULT, updMA040X2Result);				
			stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);				
			stmt=null;
			
			// 次関数実行
			if(w_arg.func)
				w_arg.func();
		}
		
		/**********************************************************
		 * 制御マスタ更新
		 * @author 12.10.29 SE-300
		 **********************************************************/
		public function updMA040X3(func: Function=null): void
		{
			// 引数情報
			cdsMA040X3_arg = new Object;
			cdsMA040X3_arg.func = func;
			
			stmt = new SQLStatement();
			stmt.sqlConnection =  singleton.sqlconnection;
			stmt.text= singleton.g_query_xml.entry.(@key == "updMA040X3");
			
			// パラメタ情報
			// 入替伝票No
			stmt.parameters[0] = g_vseq;
			
			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, updMA040X3Result, false, 0, true);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler, false, 0, true);
			
			//実行
			stmt.execute();
		}
		
		public function updMA040X3Result(event: SQLEvent): void
		{	
			var w_arg: Object = cdsMA040X3_arg;
			var result:SQLResult = stmt.getResult();
			
			stmt.removeEventListener(SQLEvent.RESULT, updMA040X3Result);				
			stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);				
			stmt=null;
			
			// 次関数実行
			if(w_arg.func)
				w_arg.func();
		}
		
		private function stmtErrorHandler(event: SQLErrorEvent): void
		{
			//IEventDispatcher(event.currentTarget).removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);				
			
			// エラーの表示
			trace(event.error.message);
		}
		
		/**********************************************************
		 * 合計算出
		 * @author 12.07.03 SE-354
		 **********************************************************/
		public function calcSum(): void
		{
			// 12.11.01 SE-233 Redmine#3600対応 start
			view.edtMEDI1.text = BasedMethod.NumChk(view.edtMEDI1.text);
			view.edtSTORE1.text = BasedMethod.NumChk(view.edtSTORE1.text);
			view.edtDIV1.text = BasedMethod.NumChk(view.edtDIV1.text);
			// 12.11.01 SE-233 Redmine#3600対応 end
			
			// 合計項目
			// 前繰
			view.edtSUM1.text = BasedMethod.calcNum(BasedMethod.calcNum(view.edtMEDI1.text, view.edtSTORE1.text), view.edtDIV1.text);
			// 使用
			view.edtSUM2.text = BasedMethod.calcNum(BasedMethod.calcNum(view.edtMEDI2.text, view.edtSTORE2.text), view.edtDIV2.text);
			// 請求
			view.edtSUM3.text = BasedMethod.calcNum(BasedMethod.calcNum(view.edtMEDI3.text, view.edtSTORE3.text), view.edtDIV3.text);
			// 値引
			view.edtSUM5.text = BasedMethod.calcNum(BasedMethod.calcNum(view.edtMEDI5.text, view.edtSTORE5.text), view.edtDIV5.text);
			// 消費税
			view.edtSUM6.text = BasedMethod.calcNum(BasedMethod.calcNum(view.edtMEDI6.text, view.edtSTORE6.text), view.edtDIV6.text);
			// 値引率
			if(BasedMethod.asCurrency(view.edtSUM2.text) != 0){
				///				view.edtSUM4.text = Math.round(BasedMethod.asCurrency(view.edtSUM5.text)
				//									/ BasedMethod.asCurrency(view.edtSUM2.text) * 10000) / 100;
				view.edtSUM4.text = BasedMethod.calcNum(Math.round(BasedMethod.asCurrency(BasedMethod.calcNum(BasedMethod.calcNum(view.edtSUM5.text
					, view.edtSUM2.text, 3, 4), "10000", 2))).toString(), "100", 3, 5);
			}else{
				view.edtSUM4.text = "0";
			}
			
			///////// 仮計 /////////
			// 医薬品
			view.edtMEDI7.text = BasedMethod.calcNum(view.edtMEDI1.text,
				//					BasedMethod.calcNum(BasedMethod.calcNum(view.edtMEDI2.text,view.edtMEDI5.text,1),view.edtMEDI6.text));
				view.edtMEDI3.text);
			// 薬店
			view.edtSTORE7.text = BasedMethod.calcNum(view.edtSTORE1.text,
				//					BasedMethod.calcNum(BasedMethod.calcNum(view.edtSTORE2.text,view.edtSTORE5.text,1),view.edtSTORE6.text));
				view.edtSTORE3.text);
			// 医療具
			view.edtDIV7.text = BasedMethod.calcNum(view.edtDIV1.text,
				//					BasedMethod.calcNum(BasedMethod.calcNum(view.edtDIV2.text,view.edtDIV5.text,1),view.edtDIV6.text));
				view.edtDIV3.text);
			
			// 仮計合計
			view.edtSUM7.text = BasedMethod.calcNum(BasedMethod.calcNum(view.edtMEDI7.text, view.edtSTORE7.text), view.edtDIV7.text);
			
			// 使用額(税込)
			view.edtSUM8.text = BasedMethod.calcNum(BasedMethod.calcNum(view.edtMEDI8.text, view.edtSTORE8.text), view.edtDIV8.text);
		}
		
		/**********************************************************
		 * 医薬品　請求額クリック
		 * @author  12.09.06 SE-362
		 **********************************************************/
		public function edtMEDI3Click(): void
		{
			// 値引率入力画面立ち上げ
			popup = new MP040X();
			
			// 使用数
			popup.ctrl.argWITHOUTTAX = BasedMethod.asCurrency(view.edtMEDI2.text);
			// 請求額
			popup.ctrl.argAMT = BasedMethod.asCurrency(view.edtMEDI3.text); 
			// 値引率
			popup.ctrl.argDISCF = BasedMethod.asCurrency(view.edtMEDI4.text);
			// 値引額
			popup.ctrl.argDISC = BasedMethod.asCurrency(view.edtMEDI5.text);
			// 使用額（税込）
			popup.ctrl.argWITHTAX = BasedMethod.asCurrency(view.edtMEDI8.text);
			// 税率
			popup.ctrl.argTAX = BasedMethod.asCurrency(getTax_ret.tax);
			// 前回値引率
			//popup.ctrl.argDISCRT = BasedMethod.asCurrency(view.ma040x.MEDIDISRT);
			popup.ctrl.argDISCRT = BasedMethod.asCurrency(view.ma040x.MEDIDISF);
			
			PopUpManager.addPopUp(popup, view, true);
			PopUpManager.centerPopUp(popup);
			popup.addEventListener(FlexEvent.REMOVE, edtMEDI3Click_last, false, 0, true);
		}
		
		private function edtMEDI3Click_last(event: FlexEvent): void
		{
			//IEventDispatcher(event.currentTarget).removeEventListener(FlexEvent.REMOVE, edtMEDI3Click_last);				
			popup.removeEventListener(FlexEvent.REMOVE, edtMEDI3Click_last);
			
			// 請求額
			view.ma040x.MEDIAMT = popup.ctrl.rtnAMT.toString();
			// 値引額
			view.ma040x.MEDIDIS = popup.ctrl.rtnDISC.toString();
			// 値引率
			view.ma040x.MEDIDISF = popup.ctrl.rtnDISCF.toString();
			// 消費税
			//			view.edtMEDI6.text = Math.floor(Number(BasedMethod.calcNum(view.edtMEDI3.text, getTax_ret.tax, 2))).toString();
			view.ma040x.MEDITAX = BasedMethod.calcNum(view.edtMEDI3.text,
				BasedMethod.calcNum(view.edtMEDI3.text,BasedMethod.calcNum(BasedMethod.calcNum(BasedMethod.nvl(String(getTax_ret.tax), "0"),"100"),"100",3,3),3,0,2),1);
			
			// 今回値引率
			//view.ma040x.MEDIDISRT = popup.ctrl.rtnDISCRT.toString();
			// 合計算出
			calcSum();
		}
		
		/**********************************************************
		 * 薬店　請求額クリック
		 * @author  12.09.06 SE-362
		 **********************************************************/
		public function edtSTORE3Click(): void
		{
			// 値引率入力画面立ち上げ
			popup = new MP040X();
			
			// 使用数
			popup.ctrl.argWITHOUTTAX = BasedMethod.asCurrency(view.edtSTORE2.text);
			// 請求額
			popup.ctrl.argAMT = BasedMethod.asCurrency(view.edtSTORE3.text); 
			// 値引率
			popup.ctrl.argDISCF = BasedMethod.asCurrency(view.edtSTORE4.text);
			// 値引額
			popup.ctrl.argDISC = BasedMethod.asCurrency(view.edtSTORE5.text);
			// 使用額（税込）
			popup.ctrl.argWITHTAX = BasedMethod.asCurrency(view.edtSTORE8.text);
			// 税率
			popup.ctrl.argTAX = BasedMethod.asCurrency(getTax_ret.tax);
			// 前回値引率
			//popup.ctrl.argDISCRT = BasedMethod.asCurrency(view.ma040x.STOREDISRT);
			popup.ctrl.argDISCRT = BasedMethod.asCurrency(view.ma040x.STOREDISF);
			
			PopUpManager.addPopUp(popup, view, true);
			PopUpManager.centerPopUp(popup);
			popup.addEventListener(FlexEvent.REMOVE, edtSTORE3Click_last, false, 0, true);
		}
		
		private function edtSTORE3Click_last(event: FlexEvent): void
		{
			//IEventDispatcher(event.currentTarget).removeEventListener(FlexEvent.REMOVE, edtSTORE3Click_last);				
			popup.removeEventListener(FlexEvent.REMOVE, edtSTORE3Click_last);
			
			// 請求額
			view.ma040x.STOREAMT = popup.ctrl.rtnAMT.toString();
			// 値引額
			view.ma040x.STOREDIS = popup.ctrl.rtnDISC.toString();
			// 値引率
			view.ma040x.STOREDISF = popup.ctrl.rtnDISCF.toString();
			// 消費税
			//			view.edtSTORE6.text = Math.ceil(Number(BasedMethod.calcNum(view.edtSTORE3.text, getTax_ret.tax, 2))).toString();
			view.ma040x.STORETAX = BasedMethod.calcNum(view.edtSTORE3.text,
				BasedMethod.calcNum(view.edtSTORE3.text,BasedMethod.calcNum(BasedMethod.calcNum(BasedMethod.nvl(String(getTax_ret.tax), "0"),"100"),"100",3,3),3,0,2),1);
			// 今回値引率
			//view.ma040x.STOREDISRT = popup.ctrl.rtnDISCRT.toString();
			
			// 合計算出
			calcSum();
		}
		
		/**********************************************************
		 * 医療具　請求額クリック
		 * @author  12.09.06 SE-362
		 **********************************************************/
		public function edtDIV3Click(): void
		{
			// 値引率入力画面立ち上げ
			popup = new MP040X();
			
			// 使用数
			popup.ctrl.argWITHOUTTAX = BasedMethod.asCurrency(view.edtDIV2.text);
			// 請求額
			popup.ctrl.argAMT = BasedMethod.asCurrency(view.edtDIV3.text); 
			// 値引率
			popup.ctrl.argDISCF = BasedMethod.asCurrency(view.edtDIV4.text);
			// 値引額
			popup.ctrl.argDISC = BasedMethod.asCurrency(view.edtDIV5.text);
			// 使用額（税込）
			popup.ctrl.argWITHTAX = BasedMethod.asCurrency(view.edtDIV8.text);
			// 税率
			popup.ctrl.argTAX = BasedMethod.asCurrency(getTax_ret.tax);
			// 前回値引率
			//popup.ctrl.argDISCRT = BasedMethod.asCurrency(view.ma040x.DEVDISRT);
			popup.ctrl.argDISCRT = BasedMethod.asCurrency(view.ma040x.DEVDISF);
			
			PopUpManager.addPopUp(popup, view, true);
			PopUpManager.centerPopUp(popup);
			popup.addEventListener(FlexEvent.REMOVE, edtDIV3Click_last, false, 0, true);
		}
		
		private function edtDIV3Click_last(event: FlexEvent): void
		{
			//IEventDispatcher(event.currentTarget).removeEventListener(FlexEvent.REMOVE, edtDIV3Click_last);				
			popup.removeEventListener(FlexEvent.REMOVE, edtDIV3Click_last);
			
			// 請求額
			view.ma040x.DEVAMT = popup.ctrl.rtnAMT.toString();
			// 値引額
			view.ma040x.DEVDIS = popup.ctrl.rtnDISC.toString();
			// 値引率
			view.ma040x.DEVDISF = popup.ctrl.rtnDISCF.toString();
			// 消費税
			//			view.edtDIV6.text = Math.ceil(Number(BasedMethod.calcNum(view.edtDIV3.text, getTax_ret.tax, 2))).toString();
			view.ma040x.DEVTAX = BasedMethod.calcNum(view.edtDIV3.text,
				BasedMethod.calcNum(view.edtDIV3.text,BasedMethod.calcNum(BasedMethod.calcNum(BasedMethod.nvl(String(getTax_ret.tax), "0"),"100"),"100",3,3),3,0,2),1);
			// 今回値引率
			//view.ma040x.DEVDISRT = popup.ctrl.rtnDISCRT.toString();
			
			// 合計算出
			calcSum();
		}
		
		/**********************************************************
		 * btnMOTO クリック時処理.
		 * @author 12.07.03 SE-354
		 **********************************************************/
		public function btnMOTOClick(): void
		{
			// リスナーを削除
			view.stage.removeEventListener(KeyboardEvent.KEY_DOWN, onKeyDownHandler);
			
			view.navigator.pushView(MF010X, view.data);
		}
		
		/**********************************************************
		 * btnIMAGE クリック
		 * @author  12.09.06 SE-362
		 **********************************************************/
		public function btnIMAGEClick(): void
		{
			var popup: POPUP004 = new POPUP004();
			
			// 税率
			popup.ctrl.argTKCD = g_cucd;
			
			PopUpManager.addPopUp(popup, view, true);
			PopUpManager.centerPopUp(popup);
			
		}
		
		// 12.10.12 SE-362 Redmine#3435 対応 start
		protected function onKeyDownHandler(e:KeyboardEvent):void
		{
			//e.preventDefault();			
			if(e.keyCode == Keyboard.BACK)
			{
				e.preventDefault();
				btnRTNClick();
			}
		}
		// 12.10.12 SE-362 Redmine#3435 対応 end
		
		/**********************************************************
		 * btnRTN クリック時処理.
		 * @author 12.07.03 SE-354
		 **********************************************************/
		public function btnRTNClick(): void
		{
			// 12.10.12 SE-362 Redmine#3435 対応 start
			// 確認画面の表示
			singleton.sitemethod.dspJoinErrMsg("Q0027", btnRTNClick_last, "", "1", null, btnRTNClick_last2);
			// 12.10.12 SE-362 Redmine#3435 対応 end
		}
		
		// 12.10.12 SE-362 Redmine#3435 対応 start
		private function btnRTNClick_last(): void
		{
			view.stage.removeEventListener(KeyboardEvent.KEY_DOWN, onKeyDownHandler);
			
			insLF013(g_cucd,"配置薬入替終了");
			
			// 前画面に遷移
			view.navigator.popView();
		}
		
		private function btnRTNClick_last2(): void
		{
			// 無処理
		}
		// 12.10.12 SE-362 Redmine#3435 対応 end
		
		/**********************************************************
		 * btnSUM クリック時処理.
		 * @author 12.07.03 SE-354
		 **********************************************************/
		public function btnSUMClick(): void
		{
			var w_sum1: String;
			var w_sum2: String;
			var w_sum3: String;
			var w_taxsum1: String;
			var w_taxsum2: String;
			var w_taxsum3: String;
			
			// 集計
			if(arrMA040X2 != null && arrMA040X2.length != 0){
				for(var i:int = 0; i < arrMA040X2.length; i++){
					if(arrMA040X2[i].VJAN.slice(0, 1) == "1"){
						// 医薬品
						w_sum1 = BasedMethod.calcNum(w_sum1, arrMA040X2[i].AMT);
						w_taxsum1 = BasedMethod.calcNum(w_taxsum1, arrMA040X2[i].TAXAMT);
					}else if(arrMA040X2[i].VJAN.slice(0, 1) == "2"){
						// 薬店
						w_sum2 = BasedMethod.calcNum(w_sum2, arrMA040X2[i].AMT);
						w_taxsum2 = BasedMethod.calcNum(w_taxsum2, arrMA040X2[i].TAXAMT);
					}else if(arrMA040X2[i].VJAN.slice(0, 1) == "3"){
						// 医療具
						w_sum3 = BasedMethod.calcNum(w_sum3, arrMA040X2[i].AMT);
						w_taxsum3 = BasedMethod.calcNum(w_taxsum3, arrMA040X2[i].TAXAMT);
					}
				}
			}
			
			// 12.10.09 SE-362 Redmine#3452対応 start
			// 使用数が入力されていない場合、エラー
			//			if(BasedMethod.nvl(w_sum1, "0") =="0" && BasedMethod.nvl(w_sum2, "0") =="0" && BasedMethod.nvl(w_sum3, "0") =="0"){
			//				singleton.sitemethod.dspJoinErrMsg("E0004", null, "", "0", ["使用数"]);
			//				return;
			//			}
			// 12.10.09 SE-362 Redmine#3452対応 start
			
			// 残数を入力させない
			view.grp1.enabled = false;
			
			// 12.10.22 SE-233 Redmine#3474 対応 start
			view.btnSUM.enabled = false;
			view.btnCLEAR.enabled = true;
			view.btnPRINT.enabled = true;
			
			//view.edtMEDI1.enabled = true;
			//view.edtMEDI3.enabled = true;
			//view.edtSTORE1.enabled = true;
			//view.edtSTORE3.enabled = true;
			//view.edtDIV1.enabled = true;
			//view.edtDIV3.enabled = true;
			view.grp2.enabled = true;
			// 12.10.22 SE-233 Redmine#3474 対応 end
			view.btnMA042X.enabled = false;
			
			
			// 使用金額表示
			view.ma040x.MEDIWITHOUTTAX = BasedMethod.nvl(w_sum1, "0");
			view.ma040x.STOREWITHOUTTAX = BasedMethod.nvl(w_sum2, "0");
			view.ma040x.DEVWITHOUTTAX = BasedMethod.nvl(w_sum3, "0");
			
			// 使用金額税込表示
			view.ma040x.MEDIWITHTAX = BasedMethod.nvl(w_taxsum1, "0");
			view.ma040x.STOREWITHTAX = BasedMethod.nvl(w_taxsum2, "0");
			view.ma040x.DEVWITHTAX = BasedMethod.nvl(w_taxsum3, "0");
			
			// 12.10.30 SE-233 Redmine#3565 対応 start
			var w_amt1: String;
			var w_amt2: String;
			var w_amt3: String;
			var w_disc1: String;
			var w_disc2: String;
			var w_disc3: String;
			var w_discf1: String;
			var w_discf2: String;
			var w_discf3: String;
			var f: CustomNumberFormatter = new CustomNumberFormatter();
			
			// 請求額
			//w_amt1 = Math.floor(Number(BasedMethod.calcNum(BasedMethod.calcNum(view.edtMEDI8.text,BasedMethod.calcNum("100", view.ma040x.MEDIDISRT, 1), 2), "100", 3,0,1))).toString();
			// 値引額
			//w_disc1 = BasedMethod.calcNum(view.edtMEDI2.text,BasedMethod.calcNum(BasedMethod.calcNum(w_amt1, "100", 2),BasedMethod.calcNum("100", getTax_ret.tax), 3,0,2), 1); 
			w_disc1 = BasedMethod.calcNum(BasedMethod.calcNum(view.edtMEDI2.text,view.ma040x.MEDIDISRT,2),"100",3,0,1);
			
			// 値引率
			if(BasedMethod.asCurrency(view.edtMEDI2.text) != 0){
				//w_discf1 = f.numberFormat(BasedMethod.calcNum(Math.floor(Number(BasedMethod.calcNum(BasedMethod.calcNum(w_disc1, view.edtMEDI2.text, 3,4), "10000", 2))).toString(),"100",3,4,1), "0", "2", "0");
				w_discf1 = view.ma040x.MEDIDISRT;
			}else{
				w_discf1 = "0";
			}
			
			// 請求額
			w_amt1 = BasedMethod.calcNum(
				BasedMethod.calcNum(BasedMethod.calcNum(view.edtMEDI2.text,w_disc1,1),
					BasedMethod.calcNum(BasedMethod.nvl(String(getTax_ret.tax), "0"),"100"),2),"100",3,0,1);
			
			// 使用金額を超えた場合、使用金額に合わせる
			if(BasedMethod.asCurrency(w_amt1) > BasedMethod.asCurrency(w_taxsum1)){
				//				w_disc1 = BasedMethod.calcNum(w_taxsum1,w_amt1,1);
				w_disc1 = BasedMethod.calcNum(w_amt1,w_taxsum1,1);
				w_amt1 = w_taxsum1;
			}
			
			// 請求額
			view.ma040x.MEDIAMT = w_amt1;
			// 値引額
			view.ma040x.MEDIDIS = w_disc1;
			// 値引率
			view.ma040x.MEDIDISF = w_discf1;
			// 消費税
			//view.ma040x.MEDITAX = Math.floor(Number(BasedMethod.calcNum(BasedMethod.calcNum(view.edtMEDI3.text, BasedMethod.calcNum(getTax_ret.tax, "100"), 3, 4), getTax_ret.tax, 2))).toString();
			view.ma040x.MEDITAX = BasedMethod.calcNum(view.edtMEDI3.text,
				BasedMethod.calcNum(view.edtMEDI3.text,BasedMethod.calcNum(BasedMethod.calcNum(BasedMethod.nvl(String(getTax_ret.tax), "0"),"100"),"100",3,3),3,0,2),1);
			// 今回値引率
			//view.ma040x.MEDIDISRT = view.ma040x.MEDIDISRT;
			
			// 請求額
			//w_amt2 = Math.floor(Number(BasedMethod.calcNum(BasedMethod.calcNum(view.edtSTORE8.text,BasedMethod.calcNum("100", view.ma040x.STOREDISRT, 1), 2), "100", 3,0,1))).toString();
			// 値引額
			//w_disc2 = BasedMethod.calcNum(view.edtSTORE2.text,BasedMethod.calcNum(BasedMethod.calcNum(w_amt2, "100", 2),BasedMethod.calcNum("100", getTax_ret.tax), 3,0,2), 1); 
			w_disc2 = BasedMethod.calcNum(BasedMethod.calcNum(view.edtSTORE2.text,view.ma040x.STOREDISRT,2),"100",3,0,1);
			// 値引率
			if(BasedMethod.asCurrency(view.edtSTORE2.text) != 0){
				//w_discf2 = f.numberFormat(BasedMethod.calcNum(Math.floor(Number(BasedMethod.calcNum(BasedMethod.calcNum(w_disc2, view.edtSTORE2.text, 3,4), "10000", 2))).toString(),"100",3,4,1), "0", "2", "0");
				w_discf2 = view.ma040x.STOREDISRT;
			}else{
				w_discf2 = "0";
			}
			
			// 請求額
			w_amt2 = BasedMethod.calcNum(
				BasedMethod.calcNum(BasedMethod.calcNum(view.edtSTORE2.text,w_disc2,1),
					BasedMethod.calcNum(BasedMethod.nvl(String(getTax_ret.tax), "0"),"100"),2),"100",3,0,1);
			
			// 使用金額を超えた場合、使用金額に合わせる
			if(BasedMethod.asCurrency(w_amt2) > BasedMethod.asCurrency(w_taxsum2)){
				//				w_disc2 = BasedMethod.calcNum(w_taxsum2,w_amt2,1);
				w_disc2 = BasedMethod.calcNum(w_amt2,w_taxsum2,1);
				w_amt2 = w_taxsum2;
			}
			
			// 請求額
			view.ma040x.STOREAMT = w_amt2;
			// 値引額
			view.ma040x.STOREDIS = w_disc2;
			// 値引率
			view.ma040x.STOREDISF = w_discf2;
			// 消費税
			//view.ma040x.STORETAX = Math.floor(Number(BasedMethod.calcNum(BasedMethod.calcNum(view.edtSTORE3.text, BasedMethod.calcNum(getTax_ret.tax, "100"), 3, 4), getTax_ret.tax, 2))).toString();
			view.ma040x.STORETAX = BasedMethod.calcNum(view.edtSTORE3.text,
				BasedMethod.calcNum(view.edtSTORE3.text,BasedMethod.calcNum(BasedMethod.calcNum(BasedMethod.nvl(String(getTax_ret.tax), "0"),"100"),"100",3,3),3,0,2),1);
			// 今回値引率
			//view.ma040x.STOREDISRT = view.ma040x.STOREDISRT;
			
			// 請求額
			//w_amt3 = Math.floor(Number(BasedMethod.calcNum(BasedMethod.calcNum(view.edtDIV8.text,BasedMethod.calcNum("100", view.ma040x.DEVDISRT, 1), 2), "100", 3,0,1))).toString();
			// 値引額
			//w_disc3 = BasedMethod.calcNum(view.edtDIV2.text,BasedMethod.calcNum(BasedMethod.calcNum(w_amt3, "100", 2),BasedMethod.calcNum("100", getTax_ret.tax), 3,0,2), 1); 
			w_disc3 = BasedMethod.calcNum(BasedMethod.calcNum(view.edtDIV2.text,view.ma040x.DEVDISRT,2),"100",3,0,1);
			// 値引率
			if(BasedMethod.asCurrency(view.edtDIV2.text) != 0){
				//w_discf3 = f.numberFormat(BasedMethod.calcNum(Math.floor(Number(BasedMethod.calcNum(BasedMethod.calcNum(w_disc3, view.edtDIV2.text, 3,4), "10000", 2))).toString(),"100",3,4,1), "0", "2", "0");
				w_discf3 = view.ma040x.DEVDISRT;
			}else{
				w_discf3 = "0";
			}
			
			// 請求額
			w_amt3 = BasedMethod.calcNum(
				BasedMethod.calcNum(BasedMethod.calcNum(view.edtDIV2.text,w_disc3,1),
					BasedMethod.calcNum(BasedMethod.nvl(String(getTax_ret.tax), "0"),"100"),2),"100",3,0,1);
			
			// 使用金額を超えた場合、使用金額に合わせる
			if(BasedMethod.asCurrency(w_amt3) > BasedMethod.asCurrency(w_taxsum3)){
				//				w_disc3 = BasedMethod.calcNum(w_taxsum3,w_amt3,1);
				w_disc3 = BasedMethod.calcNum(w_amt3,w_taxsum3,1);
				w_amt3 = w_taxsum3;
			}
			
			// 請求額
			view.ma040x.DEVAMT = w_amt3;
			// 値引額
			view.ma040x.DEVDIS = w_disc3;
			// 値引率
			view.ma040x.DEVDISF = w_discf3;
			// 消費税
			//view.ma040x.DEVTAX = Math.floor(Number(BasedMethod.calcNum(BasedMethod.calcNum(view.edtDIV3.text, BasedMethod.calcNum(getTax_ret.tax, "100"), 3, 4), getTax_ret.tax, 2))).toString();
			view.ma040x.DEVTAX = BasedMethod.calcNum(view.edtDIV3.text,
				BasedMethod.calcNum(view.edtDIV3.text,BasedMethod.calcNum(BasedMethod.calcNum(BasedMethod.nvl(String(getTax_ret.tax), "0"),"100"),"100",3,3),3,0,2),1);
			// 今回値引率
			//view.ma040x.DEVDISRT = view.ma040x.DEVDISRT;
			// 12.10.30 SE-233 Redmine#3565 対応 end
			
			calcSum();
		}
		
		/**********************************************************
		 * btnCLEAR クリック時処理.
		 * @author 12.07.03 SE-354
		 **********************************************************/
		public function btnCLEARClick(): void
		{
			var w_zero: String = "0";
			
			// 残数入力制御解除
			view.grp1.enabled = true;
			
			// 12.10.22 SE-233 Redmine#3474 対応 start
			view.btnSUM.enabled = true;
			view.btnCLEAR.enabled = false;
			view.btnPRINT.enabled = false;
			
			//view.edtMEDI1.enabled = false;
			//view.edtMEDI3.enabled = false;
			//view.edtSTORE1.enabled = false;
			//view.edtSTORE3.enabled = false;
			//view.edtDIV1.enabled = false;
			//view.edtDIV3.enabled = false;
			view.grp2.enabled = false;
			// 12.10.22 SE-233 Redmine#3474 対応 end
			
			// 修正時
			if(modifyFlag > 0){
				view.btnMA042X.enabled = true;
			}
			
			// 使用金額、請求額、値引額、値引率、消費税を初期化
			//			view.edtMEDI2.text = w_zero;
			//			view.edtMEDI3.text = w_zero;
			//			view.edtMEDI4.text = w_zero;
			//			view.edtMEDI5.text = w_zero;
			//			view.edtMEDI6.text = w_zero;
			
			view.ma040x.MEDIWITHOUTTAX = w_zero;
			view.ma040x.MEDIWITHTAX = w_zero;
			view.ma040x.MEDIAMT = w_zero;
			view.ma040x.MEDIDISF = w_zero;
			view.ma040x.MEDIDIS = w_zero;
			view.ma040x.MEDITAX = w_zero;
			
			//			view.edtSTORE2.text = w_zero;
			//			view.edtSTORE3.text = w_zero;
			//			view.edtSTORE4.text = w_zero;
			//			view.edtSTORE5.text = w_zero;
			//			view.edtSTORE6.text = w_zero;
			
			view.ma040x.STOREWITHOUTTAX = w_zero;
			view.ma040x.STOREWITHTAX = w_zero;
			view.ma040x.STOREAMT = w_zero;
			view.ma040x.STOREDISF = w_zero;
			view.ma040x.STOREDIS = w_zero;
			view.ma040x.STORETAX = w_zero;
			
			//			view.edtDIV2.text = w_zero;
			//			view.edtDIV3.text = w_zero;
			//			view.edtDIV4.text = w_zero;
			//			view.edtDIV5.text = w_zero;
			//			view.edtDIV6.text = w_zero;
			
			view.ma040x.DEVWITHOUTTAX = w_zero;
			view.ma040x.DEVWITHTAX = w_zero;
			view.ma040x.DEVAMT = w_zero;
			view.ma040x.DEVDISF = w_zero;
			view.ma040x.DEVDIS = w_zero;
			view.ma040x.DEVTAX = w_zero;
			
			// 再計算
			calcSum();
		}
		
		/**********************************************************
		 * 帳票用配列作成
		 * @author 12.07.03 SE-354
		 **********************************************************/
		private function createPrintArray(func: Function): void
		{
			// 一括発行明細数
			var w_detail: int = 5;
			var w_length: int;
			var w_nodata: int;
			var i: int;
			
			arrPRINT = new ArrayCollection();
			
			if(arrMA040X2 != null && arrMA040X2.length != 0){
				w_length = arrMA040X2.length;
				//				w_nodata = w_length % w_detail;
			}else{
				return;
			}
			
			for(i=0; i < w_length; i++){
				if(BasedMethod.nvl(arrMA040X2[i].AMT, "0") != "0"){
					// 使用されているもの
					arrPRINT.addItem(arrMA040X2[i]);
				}
			}
			
			if(arrPRINT.length % w_detail != 0){
				w_nodata = w_detail - (arrPRINT.length % w_detail);
			}
			
			reportCount = Math.floor(arrPRINT.length / w_detail) + 1;
			if(arrPRINT.length % w_detail == 0){
				reportCount--;
			}
			
			
			for(i=0; i < w_nodata; i++){
				arrPRINT.addItem(new W_MA041X());
			}
			
			func();
		}
		
		/**********************************************************
		 * 帳票作成
		 * @author 12.07.03 SE-354
		 **********************************************************/
		private function printHeader(func: Function): void
		{
			var hts: HTTPService = new HTTPService();
			// 15.03.19 SE-233 start
			// 15.01.19 SE-233 start
			//			var params:Object = new Object();
			//			var pIndex: String;
			// 15.01.19 SE-233 end
			// 15.03.19 SE-233 end
			var w_tknm1: String;
			var w_tknm2: String;
			
			// 15.03.19 SE-233 start
			// 15.01.19 SE-233 start
			//			var nowDtail: int;
			//			var w_vlin1: String;
			//			var w_vlin2: String;
			//			var w_vlin3: String;
			//			var w_vlin4: String;
			//			var w_vlin5: String;
			//
			//			var ttl_bal: String;
			//			var w_bal: String;
			//			var w_dis: String;
			// 15.01.19 SE-233 end
			// 15.03.19 SE-233 end
			
			
			
			printHeader_arg = new Object;
			printHeader_arg.func = func;
			
			// 明細数セット
			currentCount = 0;
			//			reportCount = arrMA040X2.length;
			
			if(reportCount == 0){
				print_last();
				return;
			}
			
			var tknm: Object = BasedMethod.getTKNM(view.data.TKNM);
			
			hts.method = "GET";
			// 15.03.19 SE-233 start
			// 15.01.19 SE-233 start
			//			hts.addEventListener(ResultEvent.RESULT, printBody);
			//			hts.addEventListener(ResultEvent.RESULT, print_last);
			hts.addEventListener(ResultEvent.RESULT, printBody, false, 0, true);
			// 15.01.19 SE-233 end
			// 15.03.19 SE-233 end
			hts.addEventListener(FaultEvent.FAULT, htsFault, false, 0, true);
			hts.url = "http://localhost:8080/Format/Print";
			
			// 15.03.19 SE-233 start
			// 15.01.19 SE-233 start
			hts.send({
				"__format_archive_url": "file:///sdcard/print/me010x.spfmtz",
				"__format_archive_update": "updateWithoutError",
				"__format_id_number": "1",
				"TKCD": g_cucd,
				"TKNM1": tknm.tknm1,
				"TKNM2": tknm.tknm2,
				// 12.11.06 SE-233 Redmine#3628 対応 start
				"HDAT": getEra_ret.dat + " " + g_time,
				// 12.11.06 SE-233 Redmine#3628 対応 end
				"VNO": g_vseq,
				"CHGNM": arrCHGCD2[0].CHGNM,
				"HED1": arrSEIGYO[0].COSNM1,
				"HED2": arrSEIGYO[0].COSNM2,
				"HED3": arrSEIGYO[0].COSNM3,
				"ADR1": arrSEIGYO[0].ADD1,
				"ADR2": arrSEIGYO[0].ADD2,
				"ADR3": arrSEIGYO[0].ADD3,
				"ADR23": arrSEIGYO[0].ADD23,
				"TEL1": arrSEIGYO[0].TEL1,
				"TEL2": arrSEIGYO[0].TEL2,
				"TEL3": arrSEIGYO[0].TEL3,
				"(発行枚数)": "1"
			});
			/***
			 pIndex = currentCount.toString();
			 
			 params["__format_archive_url"] = "file:///sdcard/print/me010x.spfmtz";
			 params["__format_archive_update"] = "updateWithoutError";
			 params["__format_id_number[" + pIndex + "]"] = "1";
			 params["__format[" + pIndex + "].TKCD"] = g_cucd;
			 params["__format[" + pIndex + "].TKNM1"] = tknm.tknm1;
			 params["__format[" + pIndex + "].TKNM2"] = tknm.tknm2;
			 params["__format[" + pIndex + "].HDAT"] = getEra_ret.dat + " " + g_time;
			 params["__format[" + pIndex + "].VNO"] = g_vseq;
			 params["__format[" + pIndex + "].CHGNM"] = arrCHGCD2[0].CHGNM;
			 params["__format[" + pIndex + "].HED1"] = arrSEIGYO[0].COSNM1;
			 params["__format[" + pIndex + "].HED2"] = arrSEIGYO[0].COSNM2;
			 params["__format[" + pIndex + "].HED3"] = arrSEIGYO[0].COSNM3;
			 params["__format[" + pIndex + "].ADR1"] = arrSEIGYO[0].ADD1;
			 params["__format[" + pIndex + "].ADR2"] = arrSEIGYO[0].ADD2;
			 params["__format[" + pIndex + "].ADR3"] = arrSEIGYO[0].ADD3;
			 params["__format[" + pIndex + "].ADR23"] = arrSEIGYO[0].ADD23;
			 params["__format[" + pIndex + "].TEL1"] = arrSEIGYO[0].TEL1;
			 params["__format[" + pIndex + "].TEL2"] = arrSEIGYO[0].TEL2;
			 params["__format[" + pIndex + "].TEL3"] = arrSEIGYO[0].TEL3;
			 
			 for(currentCount=0; currentCount < reportCount; currentCount++){
			 
			 nowDtail = (currentCount + 1) * 5;
			 
			 if(BasedMethod.nvl(arrPRINT[nowDtail - 5].TKCD) != ""){
			 w_vlin1 = (nowDtail - 4).toString();
			 }else{
			 w_vlin1 = "";
			 }
			 if(BasedMethod.nvl(arrPRINT[nowDtail - 4].TKCD) != ""){
			 w_vlin2 = (nowDtail - 3).toString();
			 }else{
			 w_vlin2 = "";
			 }
			 if(BasedMethod.nvl(arrPRINT[nowDtail - 3].TKCD) != ""){
			 w_vlin3 = (nowDtail - 2).toString();
			 }else{
			 w_vlin3 = "";
			 }
			 if(BasedMethod.nvl(arrPRINT[nowDtail - 2].TKCD) != ""){
			 w_vlin4 = (nowDtail - 1).toString();
			 }else{
			 w_vlin4 = "";
			 }
			 if(BasedMethod.nvl(arrPRINT[nowDtail - 1].TKCD) != ""){
			 w_vlin5 = (nowDtail).toString();
			 }else{
			 w_vlin5 = "";
			 }
			 
			 pIndex = (currentCount + 1).toString();
			 
			 params["__format_id_number[" + pIndex + "]"] = "2";
			 params["__format[" + pIndex + "].VLIN1"] = w_vlin1;
			 params["__format[" + pIndex + "].VLIN2"] = w_vlin2;
			 params["__format[" + pIndex + "].VLIN3"] = w_vlin3;
			 params["__format[" + pIndex + "].VLIN4"] = w_vlin4;
			 params["__format[" + pIndex + "].VLIN5"] = w_vlin5;
			 params["__format[" + pIndex + "].CLS1"] = BasedMethod.nvl(arrPRINT[nowDtail - 5].MDCLSNMS);
			 params["__format[" + pIndex + "].CLS2"] = BasedMethod.nvl(arrPRINT[nowDtail - 4].MDCLSNMS);
			 params["__format[" + pIndex + "].CLS3"] = BasedMethod.nvl(arrPRINT[nowDtail - 3].MDCLSNMS);
			 params["__format[" + pIndex + "].CLS4"] = BasedMethod.nvl(arrPRINT[nowDtail - 2].MDCLSNMS);
			 params["__format[" + pIndex + "].CLS5"] = BasedMethod.nvl(arrPRINT[nowDtail - 1].MDCLSNMS);
			 params["__format[" + pIndex + "].GDNM1"] = BasedMethod.nvl(arrPRINT[nowDtail - 5].GDNM);
			 params["__format[" + pIndex + "].GDNM2"] = BasedMethod.nvl(arrPRINT[nowDtail - 4].GDNM);
			 params["__format[" + pIndex + "].GDNM3"] = BasedMethod.nvl(arrPRINT[nowDtail - 3].GDNM);
			 params["__format[" + pIndex + "].GDNM4"] = BasedMethod.nvl(arrPRINT[nowDtail - 2].GDNM);
			 params["__format[" + pIndex + "].GDNM5"] = BasedMethod.nvl(arrPRINT[nowDtail - 1].GDNM);
			 params["__format[" + pIndex + "].CST1"] = BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(arrPRINT[nowDtail - 5].TAXSAL, "1", "0", "0"));
			 params["__format[" + pIndex + "].CST2"] = BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(arrPRINT[nowDtail - 4].TAXSAL, "1", "0", "0"));
			 params["__format[" + pIndex + "].CST3"] = BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(arrPRINT[nowDtail - 3].TAXSAL, "1", "0", "0"));
			 params["__format[" + pIndex + "].CST4"] = BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(arrPRINT[nowDtail - 2].TAXSAL, "1", "0", "0"));
			 params["__format[" + pIndex + "].CST5"] = BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(arrPRINT[nowDtail - 1].TAXSAL, "1", "0", "0"));
			 params["__format[" + pIndex + "].VPNO1"] = BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(arrPRINT[nowDtail - 5].KUVPNO, "1", "0", "0"));
			 params["__format[" + pIndex + "].VPNO2"] = BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(arrPRINT[nowDtail - 4].KUVPNO, "1", "0", "0"));
			 params["__format[" + pIndex + "].VPNO3"] = BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(arrPRINT[nowDtail - 3].KUVPNO, "1", "0", "0"));
			 params["__format[" + pIndex + "].VPNO4"] = BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(arrPRINT[nowDtail - 2].KUVPNO, "1", "0", "0"));
			 params["__format[" + pIndex + "].VPNO5"] = BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(arrPRINT[nowDtail - 1].KUVPNO, "1", "0", "0"));
			 params["__format[" + pIndex + "].AMT1"] = BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(arrPRINT[nowDtail - 5].TAXAMT, "1", "0", "0"));
			 params["__format[" + pIndex + "].AMT2"] = BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(arrPRINT[nowDtail - 4].TAXAMT, "1", "0", "0"));
			 params["__format[" + pIndex + "].AMT3"] = BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(arrPRINT[nowDtail - 3].TAXAMT, "1", "0", "0"));
			 params["__format[" + pIndex + "].AMT4"] = BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(arrPRINT[nowDtail - 2].TAXAMT, "1", "0", "0"));
			 params["__format[" + pIndex + "].AMT5"] = BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(arrPRINT[nowDtail - 1].TAXAMT, "1", "0", "0"));
			 }			
			 
			 ttl_bal = "前回繰越額";
			 w_bal = view.edtSUM1.text;
			 if(BasedMethod.asCurrency(view.edtSUM1.text) == 0){
			 ttl_bal = "";
			 w_bal = "";
			 }
			 
			 // 値引額はお買上金額ー請求額とするように
			 w_dis = BasedMethod.calcNum(view.edtSUM8.text, view.edtSUM3.text, 1);
			 
			 pIndex = (currentCount + 1).toString();
			 
			 params["__format_id_number[" + pIndex + "]"] = "3";
			 params["__format[" + pIndex + "].UAMT"] = view.edtSUM8.text;
			 params["__format[" + pIndex + "].TTL_BAL"] = ttl_bal;
			 params["__format[" + pIndex + "].BAL"] = w_bal;
			 params["__format[" + pIndex + "].DIS"] = singleton.g_customnumberformatter.numberFormat(w_dis, "1", "0", "0");
			 params["__format[" + pIndex + "].AMT"] = singleton.g_customnumberformatter.numberFormat(BasedMethod.calcNum(view.edtSUM3.text, w_bal), "1", "0", "0");
			 params["__format[" + pIndex + "].TAX"] = view.edtSUM6.text;
			 params["__format[" + pIndex + "].BNKNM1"] = arrSEIGYO[0].BNKCD1;
			 params["__format[" + pIndex + "].BNKSNM1"] = arrSEIGYO[0].BNKSCD1;
			 params["__format[" + pIndex + "].VBNO1"] = arrSEIGYO[0].VBNO1;
			 params["__format[" + pIndex + "].BNKMEMO1"] = arrSEIGYO[0].BNKMEMO1;
			 params["__format[" + pIndex + "].BNKNM2"] = arrSEIGYO[0].BNKCD2;
			 params["__format[" + pIndex + "].BNKSNM2"] = arrSEIGYO[0].BNKSCD2;
			 params["__format[" + pIndex + "].VBNO2"] = arrSEIGYO[0].VBNO2;
			 params["__format[" + pIndex + "].BNKMEMO2"] = arrSEIGYO[0].BNKMEMO2;
			 params["__format[" + pIndex + "].BNKNM3"] = arrSEIGYO[0].BNKCD3;
			 params["__format[" + pIndex + "].BNKSNM3"] = arrSEIGYO[0].BNKSCD3;
			 params["__format[" + pIndex + "].VBNO3"] = arrSEIGYO[0].VBNO3;
			 params["__format[" + pIndex + "].BNKMEMO3"] = arrSEIGYO[0].BNKMEMO3;
			 params["__format[" + pIndex + "].TNO"] = getTNO();
			 
			 hts.send(params);
			 ***/
			// 15.01.19 SE-233 end			
			// 15.03.19 SE-233 end			
		}
		
		private function printBody(e: ResultEvent=null): void
		{
			if(e != null){
				// 印刷成功時処理
				if(e.result.response.result == "OK"){
					// 印刷失敗時 result == NG
				}else{
					singleton.sitemethod.dspJoinErrMsg(e.result.response.message,
						doF10Proc_last, "", "0", null, null, true);
					return;
				}
				//IEventDispatcher(e.currentTarget).removeEventListener(ResultEvent.RESULT, printBody);				
				//IEventDispatcher(e.currentTarget).removeEventListener(FaultEvent.FAULT, htsFault);				
			}
			
			var hts: HTTPService = new HTTPService();
			var nowDtail: int;
			
			hts.method = "GET";
			hts.addEventListener(ResultEvent.RESULT, printLoop, false, 0, true);
			hts.addEventListener(FaultEvent.FAULT, htsFault, false, 0, true);
			hts.url = "http://localhost:8080/Format/Print";
			
			nowDtail = (currentCount + 1) * 5;
			
			// 12.10.03 SE-362 明細がない場合は行NOを印字しない start
			var w_vlin1: String;
			var w_vlin2: String;
			var w_vlin3: String;
			var w_vlin4: String;
			var w_vlin5: String;
			
			if(BasedMethod.nvl(arrPRINT[nowDtail - 5].TKCD) != ""){
				w_vlin1 = (nowDtail - 4).toString();
			}else{
				w_vlin1 = "";
			}
			if(BasedMethod.nvl(arrPRINT[nowDtail - 4].TKCD) != ""){
				w_vlin2 = (nowDtail - 3).toString();
			}else{
				w_vlin2 = "";
			}
			if(BasedMethod.nvl(arrPRINT[nowDtail - 3].TKCD) != ""){
				w_vlin3 = (nowDtail - 2).toString();
			}else{
				w_vlin3 = "";
			}
			if(BasedMethod.nvl(arrPRINT[nowDtail - 2].TKCD) != ""){
				w_vlin4 = (nowDtail - 1).toString();
			}else{
				w_vlin4 = "";
			}
			if(BasedMethod.nvl(arrPRINT[nowDtail - 1].TKCD) != ""){
				w_vlin5 = (nowDtail).toString();
			}else{
				w_vlin5 = "";
			}
			// 12.10.03 SE-362 明細がない場合は行NOを印字しない end			
			
			
			hts.send({
				"__format_archive_url": "file:///sdcard/print/me010x.spfmtz",
				"__format_archive_update": "updateWithoutError",
				"__format_id_number": "2",
				"VLIN1": w_vlin1,
				"VLIN2": w_vlin2,
				"VLIN3": w_vlin3,
				"VLIN4": w_vlin4,
				"VLIN5": w_vlin5,
				"CLS1" : BasedMethod.nvl(arrPRINT[nowDtail - 5].MDCLSNMS),
				"CLS2" : BasedMethod.nvl(arrPRINT[nowDtail - 4].MDCLSNMS),
				"CLS3" : BasedMethod.nvl(arrPRINT[nowDtail - 3].MDCLSNMS),
				"CLS4" : BasedMethod.nvl(arrPRINT[nowDtail - 2].MDCLSNMS),
				"CLS5" : BasedMethod.nvl(arrPRINT[nowDtail - 1].MDCLSNMS),
				"GDNM1": BasedMethod.nvl(arrPRINT[nowDtail - 5].GDNM),
				"GDNM2": BasedMethod.nvl(arrPRINT[nowDtail - 4].GDNM),
				"GDNM3": BasedMethod.nvl(arrPRINT[nowDtail - 3].GDNM),
				"GDNM4": BasedMethod.nvl(arrPRINT[nowDtail - 2].GDNM),
				"GDNM5": BasedMethod.nvl(arrPRINT[nowDtail - 1].GDNM),
				"CST1" : BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(arrPRINT[nowDtail - 5].TAXSAL, "1", "0", "0")),
				"CST2" : BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(arrPRINT[nowDtail - 4].TAXSAL, "1", "0", "0")),
				"CST3" : BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(arrPRINT[nowDtail - 3].TAXSAL, "1", "0", "0")),
				"CST4" : BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(arrPRINT[nowDtail - 2].TAXSAL, "1", "0", "0")),
				"CST5" : BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(arrPRINT[nowDtail - 1].TAXSAL, "1", "0", "0")),
				"VPNO1": BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(arrPRINT[nowDtail - 5].KUVPNO, "1", "0", "0")),
				"VPNO2": BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(arrPRINT[nowDtail - 4].KUVPNO, "1", "0", "0")),
				"VPNO3": BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(arrPRINT[nowDtail - 3].KUVPNO, "1", "0", "0")),
				"VPNO4": BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(arrPRINT[nowDtail - 2].KUVPNO, "1", "0", "0")),
				"VPNO5": BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(arrPRINT[nowDtail - 1].KUVPNO, "1", "0", "0")),
				"AMT1" : BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(arrPRINT[nowDtail - 5].TAXAMT, "1", "0", "0")),
				"AMT2" : BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(arrPRINT[nowDtail - 4].TAXAMT, "1", "0", "0")),
				"AMT3" : BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(arrPRINT[nowDtail - 3].TAXAMT, "1", "0", "0")),
				"AMT4" : BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(arrPRINT[nowDtail - 2].TAXAMT, "1", "0", "0")),
				"AMT5" : BasedMethod.nvl(singleton.g_customnumberformatter.numberFormat(arrPRINT[nowDtail - 1].TAXAMT, "1", "0", "0")),
				"(発行枚数)": "1"
			});
		}
		
		private function printLoop(e: ResultEvent=null): void
		{
			// カウントアップ
			currentCount++;
			
			// 印刷成功時処理
			if(e.result.response.result == "OK"){
				// 印刷失敗時 result == NG
			}else{
				singleton.sitemethod.dspJoinErrMsg(e.result.response.message,
					doF10Proc_last, "", "0", null, null, true);
				return;
			}
			
			//IEventDispatcher(e.currentTarget).removeEventListener(ResultEvent.RESULT, printLoop);				
			//IEventDispatcher(e.currentTarget).removeEventListener(FaultEvent.FAULT, htsFault);				
			
			if(currentCount != reportCount)
				printBody();
			else
				printFutter();
		}
		
		private function printFutter(): void
		{
			var hts: HTTPService = new HTTPService();
			var ttl_bal: String;
			var w_bal: String;
			var w_dis: String;
			
			hts.method = "GET";
			hts.addEventListener(ResultEvent.RESULT, print_last, false, 0, true);
			hts.addEventListener(FaultEvent.FAULT, htsFault, false, 0, true);
			hts.url = "http://localhost:8080/Format/Print";
			
			ttl_bal = "前回繰越額";
			w_bal = view.edtSUM1.text;
			if(BasedMethod.asCurrency(view.edtSUM1.text) == 0){
				ttl_bal = "";
				w_bal = "";
			}
			
			// 値引額はお買上金額ー請求額とするように
			w_dis = BasedMethod.calcNum(view.edtSUM8.text, view.edtSUM3.text, 1);
			
			hts.send({
				"__format_archive_url": "file:///sdcard/print/me010x.spfmtz",
				"__format_archive_update": "updateWithoutError",
				"__format_id_number": "3",
				"UAMT"		: view.edtSUM8.text,
				"TTL_BAL"	: ttl_bal,
				"BAL"		: w_bal,
				//				"DIS"		: view.edtSUM5.text,  値引額はお買上金額ー請求額とするように
				"DIS"		: singleton.g_customnumberformatter.numberFormat(w_dis, "1", "0", "0"),
				"AMT"		: singleton.g_customnumberformatter.numberFormat(BasedMethod.calcNum(view.edtSUM3.text, w_bal), "1", "0", "0"),
				//				"TAX"		: view.edtSUM6.text,
				"TAX"		: view.edtSUM6.text,
				"BNKNM1"	: arrSEIGYO[0].BNKCD1,
				"BNKSNM1"	: arrSEIGYO[0].BNKSCD1,
				"VBNO1"	: arrSEIGYO[0].VBNO1,
				"BNKMEMO1"	: arrSEIGYO[0].BNKMEMO1,
				"BNKNM2"	: arrSEIGYO[0].BNKCD2,
				"BNKSNM2"	: arrSEIGYO[0].BNKSCD2,
				"VBNO2"	: arrSEIGYO[0].VBNO2,
				"BNKMEMO2"	: arrSEIGYO[0].BNKMEMO2,
				"BNKNM3"	: arrSEIGYO[0].BNKCD3,
				"BNKSNM3"	: arrSEIGYO[0].BNKSCD3,
				"VBNO3"	: arrSEIGYO[0].VBNO3,
				"BNKMEMO3"	: arrSEIGYO[0].BNKMEMO3,
				"TNO": getTNO(),
				"(発行枚数)": "1"
			});
		}
		
		private function print_last(e: ResultEvent=null): void
		{
			
			// 印刷成功時処理
			if(e.result.response.result == "OK"){
				// 印刷失敗時 result == NG
			}else{
				singleton.sitemethod.dspJoinErrMsg(e.result.response.message,
					doF10Proc_last, "", "0", null, null, true);
				return;
			}
			
			//IEventDispatcher(e.currentTarget).removeEventListener(ResultEvent.RESULT, print_last);				
			//IEventDispatcher(e.currentTarget).removeEventListener(FaultEvent.FAULT, htsFault);				
			
			printHeader_arg.func();
		}
		
		private function htsFault(e: FaultEvent): void
		{
			//			trace("printerror");
			//IEventDispatcher(e.currentTarget).removeEventListener(FaultEvent.FAULT, htsFault);				
			singleton.sitemethod.dspJoinErrMsg("印刷に失敗しました。サーバを起動してから再度実行して下さい。",
				doF10Proc_last, "", "0", null, null, true);
		}
		
		private function calcWITHTAX(amt: String, tax: String): String
		{
			return BasedMethod.calcNum(Math.ceil(BasedMethod.asCurrency(BasedMethod.calcNum(amt, tax, 2))).toString(), amt);
		}
	}
}