package views
{
	import flash.data.SQLResult;
	import flash.data.SQLStatement;
	import flash.events.Event;
	import flash.events.GeolocationEvent;
	import flash.events.KeyboardEvent;
	import flash.events.LocationChangeEvent;
	import flash.events.SQLErrorEvent;
	import flash.events.SQLEvent;
	import flash.geom.Rectangle;
	import flash.media.StageWebView;
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	import flash.sensors.Geolocation;
	import flash.ui.Keyboard;
	
	import modules.base.BasedMethod;
	import modules.custom.CustomIterator;
	import modules.custom.sub.Iterator;
	import modules.dto.HANYOU;
	import modules.dto.LM006;
	import modules.dto.LM009;
	import modules.sbase.SiteModule;
	
	import mx.collections.ArrayCollection;
	import mx.managers.PopUpManager;
	
	public class POPUP002Controller extends SiteModule
	{
		private var view:POPUP002;
		private var arrLM006: ArrayCollection;
		private var arrGDCLS: ArrayCollection;
		private var arrMDCLS: ArrayCollection;
		
		private var stmt: SQLStatement;
		
		public var LM006Record: Object;
		public var argHDAT: Object;
		
		public function POPUP002Controller()
		{
			super();
		}
		
		override public function init():void
		{
			// フォーム宣言
			view = _view as POPUP002;
			
			// 12.10.29 SE-233 Redmine#3561 対応 start
			// 制御ﾏｽﾀ情報取得
			cdsSEIGYO();
			// 税率取得
			// 14.03.03 SE-233 start
			//			getTax(arrSEIGYO[0].SDDAY);
			//			var w_date:Date = new Date();
			//			var w_sysdate:String;
			//			w_sysdate = singleton.g_customdateformatter.dateDisplayFormat(w_date);
			//			getTax(w_sysdate);
			// 14.03.03 SE-233 end
			// 12.10.29 SE-233 Redmine#3561 対応 end
			
			// 14.03.19 SE-233 start
			getTax(argHDAT.toString());
			// 14.03.19 SE-233 end
			
			// 変数の設定
			//g_prgid     = "MENUX";
			super.init();
		}
		
		override public function getComboData():void
		{
			cdsGDCLS();
			cdsMDCLS();
			
			super.getComboData();
		}
		
		override public function formShow(): void
		{
			doDSPProc();
		}
		
		//		override public function openDataSet_B():void
		//		{
		//			cdsLM006();
		//		}
		
		override public function setDispObject():void
		{
			//view.grdW01.dataProvider = arrLM006;
			cdsLM006();
			
			super.setDispObject();
		}
		
		/**********************************************************
		 * SQLiteのselect
		 * @author 12.05.10 SE-354
		 **********************************************************/ 
		public function cdsLM006(): void
		{
			// init処理前の場合は何もしない
			if(arrSEIGYO == null){
				return;
			}
			
			// 変数の初期化
			arrLM006 = null;
			
			stmt = new SQLStatement();
			//stmt.sqlConnection =  connection;
			stmt.sqlConnection =  singleton.sqlconnection;
			stmt.itemClass = LM006;
			
			// 12.10.29 SE-233 Redmine#3561 対応 start
			//var w_sql: String = "SELECT A.*,B.GDCLSNM,C.MDCLSNM,C.MDCLSNMS FROM LM006 A  LEFT OUTER JOIN LM013 B ON A.GDCLS = B.GDCLS LEFT OUTER JOIN LM014 C ON A.MDCLS = C.MDCLS WHERE 1=1 ";
			var w_sql: String = "SELECT A.*,B.GDCLSNM,C.MDCLSNM,C.MDCLSNMS,CAST((A.STCST*" + String(Number(getTax_ret.tax) + 100) + "/100) AS INT) STCST1 FROM LM006 A  LEFT OUTER JOIN LM013 B ON A.GDCLS = B.GDCLS LEFT OUTER JOIN LM014 C ON A.MDCLS = C.MDCLS WHERE 1=1 ";
			// 12.10.29 SE-233 Redmine#3561 対応 end
			
			if(BasedMethod.nvl(view.edtVJAN.text) != ""){
				w_sql = w_sql + " AND A.VJAN LIKE '" + view.edtVJAN.text + "%'";
			}
			if(BasedMethod.nvl(view.edtGDNM.text) != ""){
				w_sql = w_sql + " AND A.GDNM LIKE '" + view.edtGDNM.text + "%'";
			}
			if(BasedMethod.nvl(view.lcmbGDCLS.selectedItem.CODE) != ""){
				w_sql = w_sql + " AND A.GDCLS = '" + view.lcmbGDCLS.selectedItem.CODE + "'";
			}
			if(BasedMethod.nvl(view.lcmbMDCLS.selectedItem.CODE) != ""){
				w_sql = w_sql + " AND A.MDCLS = '" + view.lcmbMDCLS.selectedItem.CODE + "'";
			}
			if(!view.chkDCLS.selected){
				w_sql = w_sql + " AND COALESCE(A.DCLS,'$') <> '1' ";
			}
			
			w_sql = w_sql + " ORDER BY A.GDKNM";
			stmt.text = w_sql;
			
			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, cdsLM006Result);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);
			
			//実行
			stmt.execute();
		}
		
		/**********************************************************
		 * 商品分類コンボ取得
		 * @author 12.05.10 SE-354
		 **********************************************************/ 
		public function cdsGDCLS(): void
		{
			// 変数の初期化
			arrGDCLS = null;
			
			stmt = new SQLStatement();
			stmt.sqlConnection =  singleton.sqlconnection;
			stmt.itemClass = HANYOU;
			stmt.text= singleton.g_query_xml.entry.(@key == "qryGDCLS");
			
			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, cdsGDCLSResult);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);
			
			//実行
			stmt.execute();
		}
		/**********************************************************
		 * 医薬品分類コンボ取得
		 * @author 12.05.10 SE-354
		 **********************************************************/ 
		public function cdsMDCLS(): void
		{
			// 変数の初期化
			arrMDCLS = null;
			
			stmt = new SQLStatement();
			stmt.sqlConnection =  singleton.sqlconnection;
			stmt.itemClass = HANYOU;
			stmt.text= singleton.g_query_xml.entry.(@key == "qryMDCLS");
			
			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, cdsMDCLSResult);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);
			
			//実行
			stmt.execute();
		}
		protected function stmtErrorHandler(event:SQLErrorEvent):void
		{
			// TODO Auto-generated method stub
			// エラーの表示
			trace("SQLerr");
			doDSPProc_last();
		}
		
		/**
		 * SELECT処理結果
		 */
		private function cdsLM006Result(event:SQLEvent):void {
			
			var result:SQLResult = stmt.getResult();
			
			stmt.removeEventListener(SQLEvent.RESULT, cdsLM006Result);
			stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);
			stmt=null;
			
			arrLM006 = new ArrayCollection(result.data);
			
			view.grdW01.dataProvider = arrLM006;
			
			//super.openDataSet_B();
		}
		
		private function cdsGDCLSResult(event:SQLEvent):void {
			var result:SQLResult = stmt.getResult();
			
			stmt.removeEventListener(SQLEvent.RESULT, cdsGDCLSResult);
			stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);
			stmt=null;
			
			arrGDCLS = new ArrayCollection();
			
			// 一行空行を生成
			arrGDCLS.addItem(new HANYOU());
			
			// 一旦、ワーク配列にセット 
			//arrGDCLS = new ArrayCollection(result.data);
			for(var i:int=0; i < result.data.length; i++){
				arrGDCLS.addItem(result.data[i]);
			}
			
			view.lcmbGDCLS.selectedItem = arrGDCLS[0];
			view.lcmbGDCLS.dataProvider = arrGDCLS;
		}
		
		private function cdsMDCLSResult(event:SQLEvent):void {
			var result:SQLResult = stmt.getResult();
			
			stmt.removeEventListener(SQLEvent.RESULT, cdsMDCLSResult);
			stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);
			stmt=null;
			
			arrMDCLS = new ArrayCollection();
			
			// 一行空行を生成
			arrMDCLS.addItem(new HANYOU());
			
			// 一旦、ワーク配列にセット 
			//			arrMDCLS = new ArrayCollection(result.data);
			for(var i:int=0; i < result.data.length; i++){
				arrMDCLS.addItem(result.data[i]);
			}
			
			
			view.lcmbMDCLS.selectedItem = arrMDCLS[0];
			view.lcmbMDCLS.dataProvider = arrMDCLS;
		}
		
		/**********************************************************
		 * Backキー押下時処理.
		 * @author 12.08.22 SE-354
		 **********************************************************/
		protected function onKeyDownHandler(e:KeyboardEvent):void
		{
			e.preventDefault();
			
			btnCANCELClick();
		}
		
		/**********************************************************
		 * btnOK クリック時処理.
		 * @author 12.08.22 SE-354
		 **********************************************************/
		public function btnOKClick(): void
		{
			// 選択行情報を返す
			if(view.grdW01.selectedIndex!=-1){
				LM006Record = arrLM006[view.grdW01.selectedIndex];
			}
			
			PopUpManager.removePopUp(view);
		}
		
		/**********************************************************
		 * btnCANCEL クリック時処理.
		 * @author 12.08.22 SE-354
		 **********************************************************/
		public function btnCANCELClick(): void
		{
			PopUpManager.removePopUp(view);
		}
		
		public function btnGDCOMClick(): void
		{
			var popup: POPUP006 = new POPUP006();
			
			if(arrLM006 != null && arrLM006.length != 0){
				popup.ctrl.argVJAN = arrLM006[view.grdW01.selectedIndex].VJAN;
				PopUpManager.addPopUp(popup, view, true);
				PopUpManager.centerPopUp(popup);
			}
			
		}
	}
}