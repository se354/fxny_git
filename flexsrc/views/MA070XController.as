package views
{
	import flash.data.SQLConnection;
	import flash.data.SQLResult;
	import flash.data.SQLStatement;
	import flash.events.KeyboardEvent;
	import flash.events.SQLErrorEvent;
	import flash.events.SQLEvent;
	import flash.ui.Keyboard;
	
	import modules.base.BasedMethod;
	import modules.custom.CustomIterator;
	import modules.custom.sub.Iterator;
	import modules.dto.LF010;
	import modules.sbase.SiteModule;
	import modules.formatters.CustomNumberFormatter;
	
	import mx.collections.ArrayCollection;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	
	public class MA070XController extends SiteModule
	{
		private var view:MA070X;
		private var arrMA070X1: ArrayCollection;
		private var arrMA070X2: ArrayCollection;
		private var stmt: SQLStatement;
		private var connection:SQLConnection;
		private var g_updateCount: int;

		// 引数情報
		private var cdsMA070X_arg: Object;
		private var insMA070X_arg: Object;
		private var delMA070X_arg: Object;

		public function MA070XController()
		{
			super();
		}
		
		override public function init():void
		{
			// フォーム宣言
			view = _view as MA070X;

			// 初期表示設定
			
			arrMA070X1 = new ArrayCollection();	
			arrMA070X2 = new ArrayCollection();	
			
			view.stage.addEventListener(KeyboardEvent.KEY_DOWN, onKeyDownHandler, false, 0, true);
			
			super.init();
		}
		
		override public function formShow_last(): void
		{
			super.formShow_last();
			
			// 画面データ表示
			doDSPProc();
		}
		
		/**********************************************************
		 * ヘッダデータ取得
		 * @author  12.10.18 SE-233
		 **********************************************************/
		override public function openDataSet_H(): void
		{
			// 制御ﾏｽﾀ情報取得
			cdsSEIGYO(super.openDataSet_H);
		}
		

		/**********************************************************
		 * 画面項目セット
		 * @author SE-354 
		 * @date 12.04.27
		 **********************************************************/
		override public function setDispObject(): void
		{
			super.setDispObject();
			
			view.edtOHDAT.text = arrSEIGYO[0].SDDAY;
		}

		protected function onKeyDownHandler(e:KeyboardEvent):void
		{
			if(e.keyCode == Keyboard.BACK)
			{
				e.preventDefault();
				btnRTNClick();
			}
		}

		/**********************************************************
		 * 実行ボタンクリック時処理
		 * @author 12.11.27 SE-233
		 **********************************************************/ 
		public function btnUPDClick(): void
		{
			if (view.edtOHDAT.text == ""){
				singleton.sitemethod.dspJoinErrMsg("現訪問日を入力してください。",
					null, "", "0", null, null, true);
				return;
			}
			if (view.edtNHDAT.text == ""){
				singleton.sitemethod.dspJoinErrMsg("新訪問日を入力してください。",
					null, "", "0", null, null, true);
				return;
			}
			if (view.edtOHDAT.text == view.edtNHDAT.text){
				singleton.sitemethod.dspJoinErrMsg("同じ訪問日は指定できません。",
					null, "", "0", null, null, true);
				return;
			}
// 14.06.16 SE-233 start
			if (BasedMethod.DateChk(view.edtOHDAT.text) == false){
				singleton.sitemethod.dspJoinErrMsg("正しい日付を入力して下さい。",
					null, "", "0", null, null, true);
				return;
			}
			if (BasedMethod.DateChk(view.edtNHDAT.text) == false){
				singleton.sitemethod.dspJoinErrMsg("正しい日付を入力して下さい。",
					null, "", "0", null, null, true);
				return;
			}
// 14.06.16 SE-233 end
			
			// 変数クリア
			g_updateCount = 0;
			
			// 不在・未訪問データ取得
			cdsMA070X1(btnUPDClick_cld1);
		}
		
		private function btnUPDClick_cld1(): void
		{
			// 対象データが存在した場合
			if(arrMA070X1 != null && arrMA070X1.length != 0){
				// 新訪問日のデータ存在チェック
				cdsMA070X2(btnUPDClick_cld2);
			}else{
				// 該当レコードなし
				singleton.sitemethod.dspJoinErrMsg("E0020");
				return;
			}
		}

		private function btnUPDClick_cld2(): void
		{
			// 新訪問日のデータが存在した場合
			if(arrMA070X2 != null && arrMA070X2.length != 0){
				btnUPDClick_countCheck();
			}else{
				// 新訪問日のデータ追加
				insMA070X(btnUPDClick_countCheck);
			}
		}
		
		private function btnUPDClick_countCheck(): void
		{
			// カウントアップ
			g_updateCount++;
			
			if(g_updateCount < arrMA070X1.length){
				btnUPDClick_cld1();
			}else{
				btnUPDClick_cld3();
			}
		}
		
		private function btnUPDClick_cld3(): void
		{
			// 現訪問日の未訪問データ削除
			delMA070X(btnUPDClick_last);
		}
		private function btnUPDClick_last(): void
		{
			insLF013("","訪問計画繰越実行");

			// 正常に終了しました。
			singleton.sitemethod.dspJoinErrMsg("N0001");
		}
		
		/**********************************************************
		 * 実行ボタンクリック時処理
		 * @author 13.03.14 SE-233
		 **********************************************************/ 
		public function btnDELClick(): void
		{
			if (view.edtOHDAT.text == ""){
				singleton.sitemethod.dspJoinErrMsg("削除する訪問日を現訪問日に入力してください。",
					null, "", "0", null, null, true);
				return;
			}
			
			// 入力チェック
			singleton.sitemethod.dspJoinErrMsg("Q0101", btnDELClick_cld2, "", "1", [], btnDELClick_cld1);
		}
		
		private function btnDELClick_cld1(): void 
		{
			return;
		}

		private function btnDELClick_cld2(): void
		{
			// 現訪問日の未訪問データ削除
			delMA070X(btnDELClick_last);
		}
		
		private function btnDELClick_last(): void
		{
			insLF013("","訪問計画削除実行");
			
			// 正常に終了しました。
			singleton.sitemethod.dspJoinErrMsg("N0001");
		}
		
		/**********************************************************
		 * 訪問計画（不在）情報取得
		 * @author 12.11.27 SE-233
		 **********************************************************/
		public function cdsMA070X1(func: Function=null): void
		{
			// 引数情報
			cdsMA070X_arg = new Object;
			cdsMA070X_arg.func = func;
			
			stmt = new SQLStatement();
			stmt.sqlConnection = singleton.sqlconnection;
			stmt.parameters[0] = arrSEIGYO[0].CHGCD;
			stmt.parameters[1] = view.edtOHDAT.text;
			stmt.itemClass = LF010;
			stmt.text= singleton.g_query_xml.entry.(@key == "qryMA070X1");
			
			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, cdsMA070X1Result, false, 0, true);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler, false, 0, true);
			
			//実行
			stmt.execute();
		}
		
		public function cdsMA070X1Result(event: SQLEvent): void
		{
			var w_arg: Object = cdsMA070X_arg;
			var result:SQLResult = stmt.getResult();
			
			stmt.removeEventListener(SQLEvent.RESULT, cdsMA070X1Result);				
			stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);				
			stmt=null;
			
			arrMA070X1 = new ArrayCollection(result.data);

			// 次関数実行
			if(w_arg.func)
				w_arg.func();
		}
		
		/**********************************************************
		 * 訪問計画情報取得
		 * @author 12.11.27 SE-233
		 **********************************************************/
		public function cdsMA070X2(func: Function=null): void
		{
			// 引数情報
			cdsMA070X_arg = new Object;
			cdsMA070X_arg.func = func;
			
			stmt = new SQLStatement();
			stmt.sqlConnection = singleton.sqlconnection;
			stmt.parameters[0] = arrSEIGYO[0].CHGCD;
			stmt.parameters[1] = view.edtNHDAT.text;
			stmt.parameters[2]  = arrMA070X1[g_updateCount].TKCD;
			stmt.itemClass = LF010;
			stmt.text= singleton.g_query_xml.entry.(@key == "qryMA070X2");
			
			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, cdsMA070X2Result, false, 0, true);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler, false, 0, true);
			
			//実行
			stmt.execute();
		}
		
		public function cdsMA070X2Result(event: SQLEvent): void
		{
			var w_arg: Object = cdsMA070X_arg;
			var result:SQLResult = stmt.getResult();
			
			stmt.removeEventListener(SQLEvent.RESULT, cdsMA070X2Result);				
			stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);				
			stmt=null;
			
			arrMA070X2 = new ArrayCollection(result.data);
			
			// 次関数実行
			if(w_arg.func)
				w_arg.func();
		}

		/**********************************************************
		 * 訪問計画実績F更新
		 * @author 12.11.27 SE-233
		 **********************************************************/
		public function insMA070X(func: Function=null): void
		{
			// 引数情報
			insMA070X_arg = new Object;
			insMA070X_arg.func = func;
			
			stmt = new SQLStatement();
			stmt.sqlConnection =  singleton.sqlconnection;
			stmt.text= singleton.g_query_xml.entry.(@key == "insMA070X");
			
			// パラメタ情報
			stmt.parameters[0]  = arrSEIGYO[0].CHGCD;
			stmt.parameters[1]  = view.edtNHDAT.text;
			stmt.parameters[2]  = arrMA070X1[g_updateCount].TKCD;
			
			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, insMA070XResult, false, 0, true);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler, false, 0, true);
			
			//実行
			stmt.execute();
		}
		
		public function insMA070XResult(event: SQLEvent): void
		{	
			var w_arg: Object = insMA070X_arg;
			
			stmt.removeEventListener(SQLEvent.RESULT, insMA070XResult);				
			stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);				
			stmt=null;
			
			// 次関数実行
			if(w_arg.func)
				w_arg.func();
		}
		
		/**********************************************************
		 * 訪問計画実績F削除
		 * @author 12.11.27 SE-233
		 **********************************************************/
		public function delMA070X(func: Function=null): void
		{
			// 引数情報
			delMA070X_arg = new Object;
			delMA070X_arg.func = func;
			
			stmt = new SQLStatement();
			stmt.sqlConnection =  singleton.sqlconnection;
			stmt.text= singleton.g_query_xml.entry.(@key == "delMA070X");
			stmt.parameters[0]  = arrSEIGYO[0].CHGCD;
			stmt.parameters[1]  = view.edtOHDAT.text;
			
			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, delMA070XResult, false, 0, true);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler, false, 0, true);
			
			//実行
			stmt.execute();
		}
		
		public function delMA070XResult(event: SQLEvent): void
		{	
			var w_arg: Object = delMA070X_arg;
			
			stmt.removeEventListener(SQLEvent.RESULT, delMA070XResult);				
			stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);				
			stmt=null;
			
			// 次関数実行
			if(w_arg.func)
				w_arg.func();
		}
		
		protected function stmtErrorHandler(event:SQLErrorEvent):void
		{
			// TODO Auto-generated method stub
			// エラーの表示
			trace("SQLerr");
		}
		
		/**********************************************************
		 * btnRTN クリック時処理.
		 * @author 12.11.27 SE-233
		 **********************************************************/
		public function btnRTNClick(): void
		{
			view.stage.removeEventListener(KeyboardEvent.KEY_DOWN, onKeyDownHandler);

			// メニューに戻る
			view.navigator.popView();
		}
	}
}