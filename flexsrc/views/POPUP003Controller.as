package views
{
	import flash.data.SQLResult;
	import flash.data.SQLStatement;
	import flash.events.Event;
	import flash.events.GeolocationEvent;
	import flash.events.KeyboardEvent;
	import flash.events.LocationChangeEvent;
	import flash.events.SQLErrorEvent;
	import flash.events.SQLEvent;
	import flash.geom.Rectangle;
	import flash.media.StageWebView;
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	import flash.sensors.Geolocation;
	import flash.ui.Keyboard;
	
	import modules.base.BasedMethod;
	import modules.custom.CustomIterator;
	import modules.custom.sub.Iterator;
	import modules.dto.HANYOU;
	import modules.dto.LM009;
	import modules.dto.LM010;
	import modules.dto.LM006;
	import modules.dto.LM016;
	import modules.sbase.SiteModule;
	
	import mx.collections.ArrayCollection;
	import mx.managers.PopUpManager;
	
	public class POPUP003Controller extends SiteModule
	{
		private var view:POPUP003;
		private var arrLM010: ArrayCollection;
		public var LM010Record: Object;

//		private var g_chgcd: String;		// 担当者コード 
		private var g_tkcd: String;			// 得意先コード
		private var g_tknm: String;			// 得意先名
		private var g_area: String;			// 地区
//		private var g_shpym: String;		// 開始次回訪問予定
//		private var g_ehpym: String;		// 終了次回訪問予定
//		private var g_slhdat: String;		// 開始前回訪問日
//		private var g_elhdat: String;		// 終了前回訪問日
		private var g_sort: String;			// 並び順
		private var g_sql: String;
		
		private var arrAREA: ArrayCollection;
		private var stmt: SQLStatement;
		private var sortdata: ArrayCollection;
		private var arrTKCD: ArrayCollection;

		public var argAREA: String;				// 地区
		public var argDTAREA: String;				// 地区詳細
		public var argSHPYM: String;				// 開始訪問予定年月
		public var argEHPYM: String;				// 終了訪問予定年月
		public var argSLHDAT: String;				// 開始前回訪問年月
		public var argELHDAT: String;				// 開始前回訪問年月
		public var argESTAMT: String;				// 売上予測金額
		public var argIMPCLS: String;				// 重点区分
		public var argHTCLS: String;				// 訪問時間区分
		public var argTKCLS: String;				// 得意先区分
		public var argKURI: Boolean;				// 売掛残有のみか
		
		private var arrDTAREA: ArrayCollection;
		private var arrIMPCLS: ArrayCollection;
		private var arrHTCLS: ArrayCollection;
//		private var arrHTCLS: ArrayCollection = new ArrayCollection([
//			{CODE:"",NAME:"",COMBFIELD2:"未選択"},
//			{CODE:"1",NAME:"午前",COMBFIELD2:"午前"},
//			{CODE:"2",NAME:"午後",COMBFIELD2:"午後"},
//			{CODE:"3",NAME:"夕方",COMBFIELD2:"夕方"},
//			{CODE:"4",NAME:"夜",COMBFIELD2:"夜"}
//		]);
		
		private var arrTKCLS: ArrayCollection;
//		private var arrTKCLS: ArrayCollection = new ArrayCollection([
//			{CODE:"",NAME:"",COMBFIELD2:"未選択"},
//			{CODE:"1",NAME:"個人",COMBFIELD2:"個人"},
//			{CODE:"2",NAME:"法人",COMBFIELD2:"法人"},
//			{CODE:"3",NAME:"個人営業",COMBFIELD2:"個人営業"}
//		]);
		
		private var current_date: Date = new Date();
		
		private var arrYEAR: ArrayCollection = new ArrayCollection([
			{CODE:"",NAME:"",COMBFIELD2:"未選択"},
			{CODE:"1",NAME:(current_date.fullYear-1).toString(),COMBFIELD2:(current_date.fullYear-1).toString()},
			{CODE:"2",NAME:(current_date.fullYear).toString(),COMBFIELD2:(current_date.fullYear).toString()},
			{CODE:"3",NAME:(current_date.fullYear+1).toString(),COMBFIELD2:(current_date.fullYear+1).toString()}
		]);
		
		private var arrMONTH: ArrayCollection = new ArrayCollection([
			{CODE:"",NAME:"",COMBFIELD2:"未選択"},
			{CODE:"1",NAME:"01",COMBFIELD2:"01"},
			{CODE:"2",NAME:"02",COMBFIELD2:"02"},
			{CODE:"3",NAME:"03",COMBFIELD2:"03"},
			{CODE:"4",NAME:"04",COMBFIELD2:"04"},
			{CODE:"5",NAME:"05",COMBFIELD2:"05"},
			{CODE:"6",NAME:"06",COMBFIELD2:"06"},
			{CODE:"7",NAME:"07",COMBFIELD2:"07"},
			{CODE:"8",NAME:"08",COMBFIELD2:"08"},
			{CODE:"9",NAME:"09",COMBFIELD2:"09"},
			{CODE:"10",NAME:"10",COMBFIELD2:"10"},
			{CODE:"11",NAME:"11",COMBFIELD2:"11"},
			{CODE:"12",NAME:"12",COMBFIELD2:"12"}
		]);
		
		public function POPUP003Controller()
		{
			super();
		}
		
		override public function init():void
		{
			// フォーム宣言
			view = _view as POPUP003;

			view.lcmbHTCLS.dataProvider = arrHTCLS;
//			view.lcmbHTCLS.selectedItem = arrHTCLS[0];
			view.lcmbSHPYM_YEAR.dataProvider = arrYEAR;
			view.lcmbSHPYM_YEAR.selectedItem = arrYEAR[0];
			view.lcmbSHPYM_MONTH.dataProvider = arrMONTH;
			view.lcmbSHPYM_MONTH.selectedItem = arrMONTH[0];
			view.lcmbEHPYM_YEAR.dataProvider = arrYEAR;
			view.lcmbEHPYM_YEAR.selectedItem = arrYEAR[0];
			view.lcmbEHPYM_MONTH.dataProvider = arrMONTH;
			view.lcmbEHPYM_MONTH.selectedItem = arrMONTH[0];
			view.lcmbSLHDAT_YEAR.dataProvider = arrYEAR;
			view.lcmbSLHDAT_YEAR.selectedItem = arrYEAR[0];
			view.lcmbSLHDAT_MONTH.dataProvider = arrMONTH;
			view.lcmbSLHDAT_MONTH.selectedItem = arrMONTH[0];
			view.lcmbELHDAT_YEAR.dataProvider = arrYEAR;
			view.lcmbELHDAT_YEAR.selectedItem = arrYEAR[0];
			view.lcmbELHDAT_MONTH.dataProvider = arrMONTH;
			view.lcmbELHDAT_MONTH.selectedItem = arrMONTH[0];
			view.lcmbTKCLS.dataProvider = arrTKCLS;
//			view.lcmbTKCLS.selectedItem = arrTKCLS[0];
			
			// コンボデータ
			sortdata = new ArrayCollection([
				{SORTCD:"LHDAT DESC",SORTNM:"前回訪問日時　降順"},
				{SORTCD:"HPYM",SORTNM:"次回訪問予定"},
				{SORTCD:"TKAREA,DTAREA,TKZIP",SORTNM:"住所"},
				{SORTCD:"TKCD",SORTNM:"顧客コード"}
			]);

			// コンボセット
			view.lcmbSORT.selectedItem = sortdata[3];
			view.lcmbSORT.dataProvider = sortdata;
			cdsAREA();
			cdsTKCD();

			// 制御ﾏｽﾀ情報取得
			cdsSEIGYO();

			super.init();
		}
		
		override public function getComboData():void
		{
			super.getComboData();
		}
		
		override public function openComboData(): void
		{
//			if (BasedMethod.nvl(argAREA) != "")
//			{
//				cdsDTAREA();
//			}
//			else
//			{
				view.lcmbDTAREA.enabled = false;
//			}
			cdsIMPCLS();
			cdsHTCLS();
			cdsTKCLS();
			
			super.openComboData();
		}

		override public function formShow(): void
		{
			doDSPProc();
		}
		
//		override public function formShow_last(): void
//		{
//			//cdsLM010();
//		}
		
		override public function setDispObject():void
		{
			super.setDispObject();
			btnKENSAKClick();
		}
		
		/**********************************************************
		 * SQLiteのselect
		 * @author 12.09.19 SE-233
		 **********************************************************/ 
		public function cdsLM010(): void
		{
			// 変数の初期化
			arrLM010 = null;
			
			stmt = new SQLStatement();
			//stmt.sqlConnection =  connection;
			stmt.sqlConnection =  singleton.sqlconnection;
			stmt.itemClass = LM010;
			//stmt.text= "SELECT * FROM とくいさき WHERE HDAT=? AND AREA=?";
			//stmt.text= "SELECT * FROM LM010 WHERE TKAREA='01' AND HPYM='2012/06' AND TKCD LIKE ";
			//stmt.text= "SELECT * FROM LM010";

			g_sql="SELECT * FROM LM010 WHERE 1=1";

			if(g_tkcd!=""){
				g_sql = g_sql + " AND TKCD LIKE '" + g_tkcd + "%'";
			}
			// 顧客名
			if(g_tknm!=""){
				g_sql = g_sql + " AND TKNM LIKE '%" + g_tknm + "%'";
			}
			// 地区
			if(BasedMethod.nvl(g_area)!=""){
				g_sql = g_sql +  " AND TKAREA = '" + g_area + "'";
			}
			// 学校区
			if(argDTAREA!=""){
				g_sql = g_sql + " AND DTAREA = '" + argDTAREA + "'";
			}
			// 次回訪問予定年月
			if(argSHPYM!=""){
				g_sql = g_sql + " AND HPYM >= '" + argSHPYM + "'";
			}
			if(argEHPYM!=""){
				g_sql = g_sql + " AND HPYM <= '" + argEHPYM + "'";
			}
			// 前回訪問日			
			if(argSLHDAT!=""){
				g_sql = g_sql + " AND LHDAT >= '" + argSLHDAT + "/01'";
			}
			if(argELHDAT!=""){
				g_sql = g_sql + " AND LHDAT <= '" + argELHDAT + "/31'";
			}
			// 重点区分
			if(argIMPCLS!=""){
				g_sql = g_sql + " AND IMPCLS = '" + argIMPCLS + "'";
			}
			// 訪問時間
			if(argHTCLS!=""){
				g_sql = g_sql + " AND HTCLS = '" + argHTCLS + "'";
			}
			// 得意先区分
			if(argTKCLS!=""){
				g_sql = g_sql + " AND TKCLS = '" + argTKCLS + "'";
			}
			// 売掛残有のみ
			if (argKURI == true)
			{
				//g_sql = g_sql + " AND (HMEDIKURI + HSTOREKURI + HDEVKURI + GMEDIKURI + GSTOREKURI + GDEVKURI) > 0";
				g_sql = g_sql + " AND ((HMEDIKURI+HMEDIAMT+HMEDITAX-HMEDINY-HMEDIDIS-HMEDIKAS)+";
				g_sql = g_sql + "(HSTOREKURI+HSTOREAMT+HSTORETAX-HSTORENY-HSTOREDIS-HSTOREKAS)+";
				g_sql = g_sql + "(HDEVKURI+HDEVAMT+HDEVTAX-HDEVNY-HDEVDIS-HDEVKAS)+";
				g_sql = g_sql + "(GMEDIKURI+GMEDIAMT+GMEDITAX-GMEDINY-GMEDIDIS-GMEDIKAS)+";
				g_sql = g_sql + "(GSTOREKURI+GSTOREAMT+GSTORETAX-GSTORENY-GSTOREDIS-GSTOREKAS)+";
				g_sql = g_sql + "(GDEVKURI+GDEVAMT+GDEVTAX-GDEVNY-GDEVDIS-GDEVKAS)) > 0";
			}
			
			// 並び順
			if(g_sort!=""){
				g_sql = g_sql +  " ORDER BY " + g_sort;
			}
			stmt.text= g_sql;
			
			//stmt.parameters[0] = p_param1;
			//stmt.parameters[1] = p_param2;
			
			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, cdsLM010Result);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);
			
			//実行
			stmt.execute();
		}

		private function cdsLM010Result(event:SQLEvent):void {
			//データの取得に成功したら
			//getResult()でSQLResultを取得し
			//取得したデータをデータグリッドに反映します。
			//status += "  データグリッド更新";
			var result:SQLResult = stmt.getResult();
			
			stmt.removeEventListener(SQLEvent.RESULT, cdsLM010Result);
			stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);
			stmt=null;

			// 一旦、ワーク配列にセット 
			//w_arr = new ArrayCollection(result.data);
			arrLM010 = new ArrayCollection(result.data);
			
			view.grdW01.dataProvider = arrLM010;
			//view.grp1.dataProvider = w_arr;
			
			if(arrLM010 == null || arrLM010.length == 0){
				// 該当レコードなし
				//singleton.sitemethod.dspJoinErrMsg("E0020");
				return;
			}
			
		}
		
		protected function stmtErrorHandler(event:SQLErrorEvent):void
		{
			stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);
			stmt=null;

			// TODO Auto-generated method stub
			// エラーの表示
			trace("SQLerr");
		}
		
		/**
		 * SELECT処理結果
		 */
//		private function cdsLM010Result(event:SQLEvent):void {
//			//データの取得に成功したら
//			//getResult()でSQLResultを取得し
//			//取得したデータをデータグリッドに反映します。
//			//status += "  データグリッド更新";
//			var result:SQLResult = stmt.getResult();
//			
//			// 一旦、ワーク配列にセット 
//			//w_arr = new ArrayCollection(result.data);
//			arrLM010 = new ArrayCollection(result.data);
//
//			view.grdW01.dataProvider = arrLM010;
//			//view.grp1.dataProvider = w_arr;
//
//			if(arrLM010 == null || arrLM010.length == 0){
//				// 該当レコードなし
//				singleton.sitemethod.dspJoinErrMsg("E0020");
//				return;
//			}
//		
//		}
		
		/**********************************************************
		 * SQLiteのselect
		 * @author 12.09.19 SE-233
		 **********************************************************/ 
		public function cdsAREA(): void
		{
			// 変数の初期化
			arrAREA = null;
			
			stmt = new SQLStatement();
			stmt.sqlConnection =  singleton.sqlconnection;
			stmt.itemClass = HANYOU;
			stmt.text= singleton.g_query_xml.entry.(@key == "qryAREA");
			
			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, cdsAREAResult);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);
			
			//実行
			stmt.execute();
		}
		
		private function cdsAREAResult(event:SQLEvent):void 
		{
			var result:SQLResult = stmt.getResult();

			stmt.removeEventListener(SQLEvent.RESULT, cdsAREAResult);
			stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);
			stmt=null;

			arrAREA = new ArrayCollection();
			
			// 一行空行を生成
			arrAREA.addItem(new HANYOU());
			
			if (result.data == null)
			{
				singleton.sitemethod.dspJoinErrMsg("地区マスタデータが存在しません。先にデータダウンロードを行ってください。",
					null, "", "0", null, null, true);
				return;
			}
			
			// 一旦、ワーク配列にセット 
			//arrGDCLS = new ArrayCollection(result.data);
			for(var i:int=0; i < result.data.length; i++){
				arrAREA.addItem(result.data[i]);
			}
			view.lcmbAREA.selectedItem = arrAREA[0];
			view.lcmbAREA.dataProvider = arrAREA;
		}
		
		/**********************************************************
		 * SQLiteのselect
		 * @author 12.10.29 SE-233
		 **********************************************************/ 
		public function cdsTKCD(): void
		{
			// 変数の初期化
			arrTKCD = null;
			
			stmt = new SQLStatement();
			stmt.sqlConnection =  singleton.sqlconnection;
			stmt.itemClass = LM010;
			stmt.text= singleton.g_query_xml.entry.(@key == "qryCUCD3");
			
			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, cdsTKCDResult);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);
			
			//実行
			stmt.execute();
		}
		
		private function cdsTKCDResult(event:SQLEvent):void 
		{
			var result:SQLResult = stmt.getResult();

			stmt.removeEventListener(SQLEvent.RESULT, cdsTKCDResult);
			stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);
			stmt=null;
		
			arrTKCD = new ArrayCollection();
			
			// 一行空行を生成
			arrTKCD.addItem(new LM010());
			arrTKCD[0].TKCD = "";
			arrTKCD[0].TKNM = "未選択";
			
			//if (result.data == null)
			//{
			//	singleton.sitemethod.dspJoinErrMsg("地区マスタデータが存在しません。先にデータダウンロードを行ってください。",
			//		null, "", "0", null, null, true);
			//	return;
			//}
			
			// 一旦、ワーク配列にセット 
			for(var i:int=0; i < result.data.length; i++){
				arrTKCD.addItem(result.data[i]);
			}
			view.lcmbTKCD.selectedItem = arrTKCD[0];
			view.lcmbTKCD.dataProvider = arrTKCD;
		}
		
		/**********************************************************
		 * 地区詳細コンボ用select
		 * @author 12.09.07 SE-354
		 **********************************************************/ 
		public function cdsDTAREA(): void
		{
			// 変数の初期化
			arrDTAREA = null;
			
			stmt = new SQLStatement();
			stmt.sqlConnection =  singleton.sqlconnection;
			stmt.itemClass = HANYOU;
			stmt.text= singleton.g_query_xml.entry.(@key == "qryDTAREA3");
			
			stmt.parameters[0] = BasedMethod.nvl(g_area);
			//stmt.parameters[1] = p_param2;
			
			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, cdsDTAREAResult);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);
			
			//実行
			stmt.execute();
		}
		private function cdsDTAREAResult(event:SQLEvent):void 
		{
			//データの取得に成功したら
			//getResult()でSQLResultを取得し
			//取得したデータをデータグリッドに反映します。
			//status += "  データグリッド更新";
			var result:SQLResult = stmt.getResult();
			
			stmt.removeEventListener(SQLEvent.RESULT, cdsDTAREAResult);
			stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);
			stmt=null;
			
			arrDTAREA = new ArrayCollection();
			
			// 一行空行を生成
			arrDTAREA.addItem(new HANYOU());
			
			if (result.data != null)
			{
				// 一旦、ワーク配列にセット 
				//arrGDCLS = new ArrayCollection(result.data);
				for(var i:int=0; i < result.data.length; i++){
					arrDTAREA.addItem(result.data[i]);
				}
			}
			
			view.lcmbDTAREA.dataProvider = arrDTAREA;
			view.lcmbDTAREA.selectedItem = arrDTAREA[0];
		}
		
		/**********************************************************
		 * 重点区分コンボ用select
		 * @author 12.10.30 SE-354
		 **********************************************************/ 
		public function cdsIMPCLS(): void
		{
			// 変数の初期化
			arrIMPCLS = null;
			
			stmt = new SQLStatement();
			stmt.sqlConnection =  singleton.sqlconnection;
			stmt.itemClass = HANYOU;
			stmt.text= singleton.g_query_xml.entry.(@key == "qryIMPCLS2");
			
			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, cdsIMPCLSResult);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);
			
			//実行
			stmt.execute();
		}
		private function cdsIMPCLSResult(event:SQLEvent):void 
		{
			//データの取得に成功したら
			//getResult()でSQLResultを取得し
			//取得したデータをデータグリッドに反映します。
			//status += "  データグリッド更新";
			var result:SQLResult = stmt.getResult();
			
			stmt.removeEventListener(SQLEvent.RESULT, cdsIMPCLSResult);
			stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);
			stmt=null;
			
			arrIMPCLS = new ArrayCollection();
			
			// 一行空行を生成
			arrIMPCLS.addItem(new HANYOU());
			
			if (result.data != null)
			{
				// 一旦、ワーク配列にセット 
				//arrGDCLS = new ArrayCollection(result.data);
				for(var i:int=0; i < result.data.length; i++){
					arrIMPCLS.addItem(result.data[i]);
				}
			}
			
			view.lcmbIMPCLS.dataProvider = arrIMPCLS;
			
			if (argIMPCLS == "")
			{
				view.lcmbIMPCLS.selectedItem = arrIMPCLS[0];
			}
			else
			{
				var w_index: int = BasedMethod.getArrayIndex(arrIMPCLS, "CODE", argIMPCLS);
				view.lcmbIMPCLS.selectedItem = arrIMPCLS[w_index];
			}
			
			//execSetPLSQL();
		}
		
		/**********************************************************
		 * 訪問時間コンボ用select
		 * @author 12.11.15 SE-233
		 **********************************************************/ 
		public function cdsHTCLS(): void
		{
			// 変数の初期化
			arrHTCLS = null;
			
			stmt = new SQLStatement();
			stmt.sqlConnection =  singleton.sqlconnection;
			stmt.itemClass = HANYOU;
			stmt.text= singleton.g_query_xml.entry.(@key == "qryHTIMECD");
			
			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, cdsHTCLSResult);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);
			
			//実行
			stmt.execute();
		}
		private function cdsHTCLSResult(event:SQLEvent):void 
		{
			var result:SQLResult = stmt.getResult();
			
			stmt.removeEventListener(SQLEvent.RESULT, cdsHTCLSResult);
			stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);
			stmt=null;
			
			arrHTCLS = new ArrayCollection();
			
			// 一行空行を生成
			arrHTCLS.addItem(new HANYOU());
			
			if (result.data != null)
			{
				// 一旦、ワーク配列にセット 
				for(var i:int=0; i < result.data.length; i++){
					arrHTCLS.addItem(result.data[i]);
				}
			}
			
			view.lcmbHTCLS.dataProvider = arrHTCLS;
			
			if (argHTCLS == "")
			{
				view.lcmbHTCLS.selectedItem = arrHTCLS[0];
			}
			else
			{
				var w_index: int = BasedMethod.getArrayIndex(arrHTCLS, "CODE", argHTCLS);
				view.lcmbHTCLS.selectedItem = arrHTCLS[w_index];
			}
		}
		
		/**********************************************************
		 * 得意先区分コンボ用select
		 * @author 12.11.15 SE-233
		 **********************************************************/ 
		public function cdsTKCLS(): void
		{
			// 変数の初期化
			arrTKCLS = null;
			
			stmt = new SQLStatement();
			stmt.sqlConnection =  singleton.sqlconnection;
			stmt.itemClass = HANYOU;
			stmt.text= singleton.g_query_xml.entry.(@key == "qryTKCLS");
			
			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, cdsTKCLSResult);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);
			
			//実行
			stmt.execute();
		}
		private function cdsTKCLSResult(event:SQLEvent):void 
		{
			var result:SQLResult = stmt.getResult();
			
			stmt.removeEventListener(SQLEvent.RESULT, cdsTKCLSResult);
			stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);
			stmt=null;
			
			arrTKCLS = new ArrayCollection();
			
			// 一行空行を生成
			arrTKCLS.addItem(new HANYOU());
			
			if (result.data != null)
			{
				// 一旦、ワーク配列にセット 
				for(var i:int=0; i < result.data.length; i++){
					arrTKCLS.addItem(result.data[i]);
				}
			}
			
			view.lcmbTKCLS.dataProvider = arrTKCLS;
			
			if (argTKCLS == "")
			{
				view.lcmbTKCLS.selectedItem = arrTKCLS[0];
			}
			else
			{
				var w_index: int = BasedMethod.getArrayIndex(arrTKCLS, "CODE", argTKCLS);
				view.lcmbTKCLS.selectedItem = arrTKCLS[w_index];
			}
		}

		/**********************************************************
		 * Backキー押下時処理.
		 * @author 12.09.19 SE-233
		 **********************************************************/
		protected function onKeyDownHandler(e:KeyboardEvent):void
		{
			e.preventDefault();
			
			btnCANCELClick();
		}
		
		/**********************************************************
		 * btnKENSAK クリック時処理.
		 * @author 12.09.19 SE-233
		 **********************************************************/
		public function btnKENSAKClick():void
		{
			// init処理前の場合は何もしない
			if(arrSEIGYO == null){
				return;
			}
			
//			g_chgcd = arrSEIGYO[0].CHGCD;
//			//g_tkcd = view.edtTKCD.text;
			if(view.lcmbTKCD.selectedItem){
				g_tkcd = view.lcmbTKCD.selectedItem.TKCD;
			}else{
				g_tkcd = "";
			}
			g_tknm = view.edtTKNM.text;
			if(view.lcmbAREA.selectedItem){
				g_area = view.lcmbAREA.selectedItem.CODE;
			}else{
				g_area = "";
			}
//			g_shpym = view.edtSHPYM.text;
//			g_ehpym = view.edtEHPYM.text;
//			g_slhdat = view.edtSLHDAT.text;
//			g_elhdat = view.edtELHDAT.text;

	
			if(view.lcmbDTAREA.selectedItem){
				argDTAREA = BasedMethod.nvl(view.lcmbDTAREA.selectedItem.CODE);
			}else{
				argDTAREA = "";
			}
			
			if (BasedMethod.nvl(view.lcmbSHPYM_YEAR.selectedItem.CODE)!=""){
				argSHPYM = view.lcmbSHPYM_YEAR.selectedItem.NAME;
				if (BasedMethod.nvl(view.lcmbSHPYM_MONTH.selectedItem.CODE)!=""){
					argSHPYM = argSHPYM + "/" + view.lcmbSHPYM_MONTH.selectedItem.NAME;
				}else{
					argSHPYM = argSHPYM + "/01";
				}
			}else{
				argSHPYM = "";
			}

			if (BasedMethod.nvl(view.lcmbEHPYM_YEAR.selectedItem.CODE)!="") {
				argEHPYM = view.lcmbEHPYM_YEAR.selectedItem.NAME;
				if (BasedMethod.nvl(view.lcmbEHPYM_MONTH.selectedItem.CODE)!="") {
					argEHPYM = argEHPYM + "/" + view.lcmbEHPYM_MONTH.selectedItem.NAME;
				}else{
					argEHPYM = argEHPYM + "/12";
				}
			}else{
				argEHPYM = "";
			}
			
			if (BasedMethod.nvl(view.lcmbSLHDAT_YEAR.selectedItem.CODE)!="") {
				argSLHDAT = view.lcmbSLHDAT_YEAR.selectedItem.NAME;
				if (BasedMethod.nvl(view.lcmbSLHDAT_MONTH.selectedItem.CODE)!="") {
					argSLHDAT = argSLHDAT + "/" + view.lcmbSLHDAT_MONTH.selectedItem.NAME;
				}else{
					argSLHDAT = argSLHDAT + "/01";
				}
			}else{
				argSLHDAT = "";
			}
			
			if (BasedMethod.nvl(view.lcmbELHDAT_YEAR.selectedItem.CODE)!="") {
				argELHDAT = view.lcmbELHDAT_YEAR.selectedItem.NAME;
				if (BasedMethod.nvl(view.lcmbELHDAT_MONTH.selectedItem.CODE)!="") {
					argELHDAT = argELHDAT + "/" + view.lcmbELHDAT_MONTH.selectedItem.NAME;
				}else{
					argELHDAT = argELHDAT + "/12";
				}
			}else{
				argELHDAT = "";
			}
			
			if(view.lcmbIMPCLS.selectedItem){
				argIMPCLS = BasedMethod.nvl(view.lcmbIMPCLS.selectedItem.CODE);
			}else{
				argIMPCLS = "";
			}
			
			if(view.lcmbHTCLS.selectedItem){
				argHTCLS = BasedMethod.nvl(view.lcmbHTCLS.selectedItem.CODE);
			}else{
				argHTCLS = "";
			}
			
			if(view.lcmbTKCLS.selectedItem){
				argTKCLS = BasedMethod.nvl(view.lcmbTKCLS.selectedItem.CODE);
			}else{
				argTKCLS = "";
			}
			
			argKURI = view.chkKURI.selected;

			if(view.lcmbSORT.selectedItem){
				g_sort = view.lcmbSORT.selectedItem.SORTCD;
			}else{
				g_sort = "";
			}
			
			cdsLM010();
		}
		
		/**********************************************************
		 * btnOK クリック時処理.
		 * @author 12.09.19 SE-233
		 **********************************************************/
		public function btnOKClick(): void
		{
			// 選択行情報を返す
			if(view.grdW01.selectedIndex!=-1){
				LM010Record = arrLM010[view.grdW01.selectedIndex];
			}
			
			PopUpManager.removePopUp(view);
		}
		
		/**********************************************************
		 * btnCANCEL クリック時処理.
		 * @author 12.09.19 SE-233
		 **********************************************************/
		public function btnCANCELClick(): void
		{
			PopUpManager.removePopUp(view);
		}

		public function lcmbAREAExit(): void
		{
			g_area = view.lcmbAREA.selectedItem.CODE;
			cdsDTAREA();

			if (BasedMethod.nvl(g_area) != "")	{
				view.lcmbDTAREA.enabled = true;
			}else{
				view.lcmbDTAREA.enabled = false;
			}
			btnKENSAKClick();
		}
		
		public function lcmbDTAREAExit(): void
		{
			argDTAREA = view.lcmbDTAREA.selectedItem.CODE;
			btnKENSAKClick();
		}
		
		public function lcmbIMPCLSExit(): void
		{
			argIMPCLS = view.lcmbIMPCLS.selectedItem.CODE;
			btnKENSAKClick();
		}
		
		public function lcmbHTCLSExit(): void
		{
			argHTCLS = view.lcmbHTCLS.selectedItem.CODE;
			btnKENSAKClick();
		}
		
		public function lcmbTKCLSExit(): void
		{
			argTKCLS = view.lcmbTKCLS.selectedItem.CODE;
			btnKENSAKClick();
		}
		
		public function lcmbSHPYM_YEARExit(): void
		{
			if(view.lcmbSHPYM_YEAR.selectedItem.CODE==""){
				view.lcmbSHPYM_MONTH.selectedItem = arrMONTH[0];
			}
			btnKENSAKClick();
		}
		
		public function lcmbEHPYM_YEARExit(): void
		{
			if(view.lcmbEHPYM_YEAR.selectedItem.CODE==""){
				view.lcmbEHPYM_MONTH.selectedItem = arrMONTH[0];
			}
			btnKENSAKClick();
		}
		
		public function lcmbSLHDAT_YEARExit(): void
		{
			if(view.lcmbSLHDAT_YEAR.selectedItem.CODE==""){
				view.lcmbSLHDAT_MONTH.selectedItem = arrMONTH[0];
			}
			btnKENSAKClick();
		}
		
		public function lcmbELHDAT_YEARExit(): void
		{
			if(view.lcmbELHDAT_YEAR.selectedItem.CODE==""){
				view.lcmbELHDAT_MONTH.selectedItem = arrMONTH[0];
			}
			btnKENSAKClick();
		}
		
	}
}