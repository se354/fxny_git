package views
{
	
	import flash.data.SQLResult;
	import flash.data.SQLStatement;
	import flash.events.Event;
	import flash.events.GeolocationEvent;
	import flash.events.KeyboardEvent;
	import flash.events.LocationChangeEvent;
	import flash.events.SQLErrorEvent;
	import flash.events.SQLEvent;
	import flash.events.IEventDispatcher;
	import flash.geom.Rectangle;
	import flash.media.StageWebView;
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	import flash.sensors.Geolocation;
	import flash.ui.Keyboard;
	
	import modules.base.BasedMethod;
	import modules.custom.CustomEvent;
	import modules.custom.CustomIterator;
	import modules.custom.sub.Iterator;
	import modules.dto.LF010;
	import modules.dto.LM009;
	import modules.dto.LM010;
	import modules.sbase.SiteModule;
	
	import mx.collections.ArrayCollection;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	
	public class MA009XController extends SiteModule
	{
		private var view:MA009X;
		private var popup003: POPUP003;				// 得意先ポップアップ
		private var popup008: POPUP008;
		private var mp009x: MP009X;
		
		public var arrMA009X: ArrayCollection;
		private var stmt: SQLStatement;
		
		private var webView:StageWebView;
		
		private var geo: Geolocation;
		
		private var w_latitude: Number;
		private var w_longitude: Number;
		
		public var itemr: MA009XGridRenderer;
		public var w_index: int;
		
		private var g_time: String;			// 訪問時間
		
		private static const MAP_URL:String = "file:///android_asset/views/omap.html";
		
		public function MA009XController()
		{
			super();
		}
		
		override public function init():void
		{
			//super.init();
			
			// フォーム宣言
			view = _view as MA009X;
			
			// 変数の設定
			//g_prgid     = "MENUX";
			
			if (Geolocation.isSupported)
			{
				geo = new Geolocation();
				if (!geo.muted)
				{
					//geo.setRequestedUpdateInterval(1000);
					geo.addEventListener(GeolocationEvent.UPDATE, onGeoUpdate, false, 0, true);
				}
			}
			
			webView = new StageWebView();
			
			view.stage.addEventListener(KeyboardEvent.KEY_DOWN, onKeyDownHandler, false, 0, true);
			
			view.grp1.addEventListener(CustomEvent.BUTTON_CLICK, pushview2, false, 0, true);
			view.grp1.addEventListener(CustomEvent.INFO_BUTTON_CLICK, btnINFOClick, false, 0, true);
			
			super.init();
		}
		
		override public function formShow_last(): void
		{
			cdsSEIGYO(formShow_last_cld1);
		}
		
		
		private function formShow_last_cld1(): void
		{
			//cdsMA020X();
			//cdsAREA();
			if (arrSEIGYO.length != 0)
			{
				view.edtHDAT.text = arrSEIGYO[0].SDDAY;
			}
			
			cdsMA009X();
		}
		
		public function btnNOWClick(): void
		{
			webView.loadURL("javascript:centerLnglat(" + w_longitude + "," + w_latitude + ")");
		}
		
		private function onGeoUpdate(event:GeolocationEvent):void
		{
			// 緯度をセット
			w_latitude = event.latitude;
			
			// 経度をセット
			w_longitude = event.longitude;
		}
		
		public function pushview2(event: CustomEvent): void
		{
			/*
			var itemr: MA009XGridRenderer = MA009XGridRenderer(event.target);
			w_index = itemr.itemIndex;
			
			
			//this.webView.dispose();
			//webView.stage = null;
			
			//view.navigator.pushView(MA010X, [arrMA009X[w_index].TKNM, arrMA009X[w_index].TKCD]);
			view.navigator.pushView(MA010X, arrMA009X[w_index]);
			*/
			itemr = MA009XGridRenderer(event.target);
			w_index = itemr.itemIndex;
			
			mp009x = new MP009X();
			
			//popup008.ctrl.argDATA = arrMA009X[w_index];
			mp009x.ctrl.argABSENT = BasedMethod.nvl(arrMA009X[w_index].ABSENT, "0");
			mp009x.ctrl.argABTIME1 = BasedMethod.nvl(arrMA009X[w_index].ABTIME1);
			mp009x.ctrl.argABTIME2 = BasedMethod.nvl(arrMA009X[w_index].ABTIME2);
			mp009x.ctrl.argABTIME3 = BasedMethod.nvl(arrMA009X[w_index].ABTIME3);
			
			PopUpManager.addPopUp(mp009x, view, true);
			PopUpManager.centerPopUp(mp009x);
			
			mp009x.addEventListener(FlexEvent.REMOVE, pushview2_last, false, 0, true);
		}
		
		private function pushview2_last(event: FlexEvent): void
		{
			mp009x.removeEventListener(FlexEvent.REMOVE, pushview2_last);
			
			// OKボタン押下でポップアップを閉じたときのみORDERの修正
			if (mp009x.ctrl.okflg == true)
			{
				//trace("ret:" + mp009x.ctrl.argABSENT);
				
				if (mp009x.ctrl.argABSENT == "2"){
					insLF013(arrMA009X[w_index].TKCD,"訪問");
				}else if (mp009x.ctrl.argABSENT == "1"){
					insLF013(arrMA009X[w_index].TKCD,"不在");
				}
				
				if (mp009x.ctrl.argABSENT == "0")
				{
					arrMA009X[w_index].ABSENT = null;
				}
				else
				{
					arrMA009X[w_index].ABSENT = mp009x.ctrl.argABSENT;
				}
				
				itemr.changecolor();
				
				// 訪問選択時は更新しない
				if (mp009x.ctrl.argABSENT != "2"){
					//updateMA009X();
				}
				
				if (mp009x.ctrl.argABSENT == "0")
				{
					//arrMA009X[w_index].ABSENT = mp009x.ctrl.argABSENT;
					arrMA009X[w_index].ABTIME1 = "";
					arrMA009X[w_index].ABTIME2 = "";
					arrMA009X[w_index].ABTIME3 = "";
					updateMA009X();
				}
				else if (mp009x.ctrl.argABSENT == "1")
				{
					// 訪問時間取得
					g_time = BasedMethod.getTime();
					
					// 不在の場合、不在時間の設定
					if((BasedMethod.nvl(arrMA009X[w_index].ABTIME1) == "")||(BasedMethod.nvl(arrMA009X[w_index].ABTIME1) == g_time)){
						arrMA009X[w_index].ABTIME1 = g_time;
					}else if((BasedMethod.nvl(arrMA009X[w_index].ABTIME2) == "")||(BasedMethod.nvl(arrMA009X[w_index].ABTIME2) == g_time)){
						arrMA009X[w_index].ABTIME2 = g_time;
					}else if((BasedMethod.nvl(arrMA009X[w_index].ABTIME3) == "")||(BasedMethod.nvl(arrMA009X[w_index].ABTIME3) == g_time)){
						arrMA009X[w_index].ABTIME3 = g_time;
					}
					updateMA009X();
				}
				else if (mp009x.ctrl.argABSENT == "2")
				{
					//itemr.changecolor();
					
					view.navigator.pushView(MA010X, arrMA009X[w_index]);
				}
				//view.grp1.dataProvider = arrMA009X;
				
				// SharedObjectに表示順を保存
				//saveParam();
				
				mp009x = null;
				
				// 集計
				calcSum();
			}
		}
		
		/**********************************************************
		 * 検索ボタンクリック時処理
		 * @author 12.05.10 SE-354
		 **********************************************************/ 
		public function btnDSPClick(): void
		{
			// 訪問日は必須
			//			if (view.edtHDAT.text != "")
			//			{
			cdsMA009X();
			//			}
		}
		
		/**********************************************************
		 * 顧客追加ボタンクリック時処理
		 * @author 12.10.25 SE-354
		 **********************************************************/
		public function btnADDClick(): void
		{
			/*
			if (arrMA009X == null || arrMA009X.length == 0)
			{
			return;
			}
			*/
			// 2014.06.16 SE-233 start			
			if (view.edtHDAT.text == ""){
				singleton.sitemethod.dspJoinErrMsg("訪問日を入力してください。",
					null, "", "0", null, null, true);
				return;
			}
			if (BasedMethod.DateChk(view.edtHDAT.text) == false){
				singleton.sitemethod.dspJoinErrMsg("正しい訪問日を入力して下さい。",
					null, "", "0", null, null, true);
				return;
			}
			// 2014.06.16 SE-233 end
			
			// 得意先ポップアップ呼び出し
			popup003 = new POPUP003();
			
			// 入力画面立ち上げ					
			PopUpManager.addPopUp(popup003, view, true);
			PopUpManager.centerPopUp(popup003);
			popup003.addEventListener(FlexEvent.REMOVE, btnADDClick_last, false, 0, true);
		}
		
		private function btnADDClick_last(e: FlexEvent): void
		{
			popup003.removeEventListener(FlexEvent.REMOVE, btnADDClick_last);				
			if (popup003.ctrl.LM010Record)
			{
				if (BasedMethod.getArrayIndex(arrMA009X, "TKCD", popup003.ctrl.LM010Record.TKCD) == -1)
				{
					//arrMA009X.addItem(popup003.ctrl.LM010Record);
					
					// データインサート
					insMA009X(popup003.ctrl.LM010Record as LM010);
					
					btnDSPClick();
				}
			}
			popup003 = null;
		}
		
		public function btnINFOClick(event: CustomEvent): void
		{
			var itemr2: MA009XGridRenderer = MA009XGridRenderer(event.target);
			w_index = itemr2.itemIndex;
			
			popup008 = new POPUP008();
			
			popup008.ctrl.argDATA = arrMA009X[w_index];
			popup008.ctrl.argTKCD = arrMA009X[w_index].TKCD;
			popup008.ctrl.argTKNM = arrMA009X[w_index].TKNM;
			popup008.ctrl.argTKTEL = arrMA009X[w_index].TKTEL;
			popup008.ctrl.argTKMTEL = arrMA009X[w_index].TKMTEL;
			popup008.ctrl.argLHDAT = BasedMethod.nvl(arrMA009X[w_index].LHDAT) + " " + BasedMethod.nvl(arrMA009X[w_index].LHTIME);
			popup008.ctrl.argHSPAN = arrMA009X[w_index].HSPAN;
			popup008.ctrl.argHAMT = (BasedMethod.asCurrency(arrMA009X[w_index].HMEDIZAMT) + BasedMethod.asCurrency(arrMA009X[w_index].HSTOREZAMT) + BasedMethod.asCurrency(arrMA009X[w_index].HDEVZAMT)).toString();
			popup008.ctrl.argGAMT = (BasedMethod.asCurrency(arrMA009X[w_index].GMEDIZAMT) + BasedMethod.asCurrency(arrMA009X[w_index].GSTOREZAMT) + BasedMethod.asCurrency(arrMA009X[w_index].GDEVZAMT)).toString();
			popup008.ctrl.argIMPCLS = arrMA009X[w_index].IMPCLS;
			//popup008.ctrl.argHTCLS = event.target.data.HTCLS;
			popup008.ctrl.argMEMO = arrMA009X[w_index].MEMO;
			popup008.ctrl.argESTAMT = BasedMethod.nvl(arrMA009X[w_index].ESTAMT,"0");
			
			PopUpManager.addPopUp(popup008, view, true);
			PopUpManager.centerPopUp(popup008);
		}
		
		/**********************************************************
		 * SQLiteのselect(候補リスト)
		 * @author 12.05.10 SE-354
		 **********************************************************/ 
		public function cdsMA009X(nextfunc: Function=null): void
		{
			// 変数の初期化
			arrMA009X = null;
			
			// 2014.06.16 SE-233 start			
			if (view.edtHDAT.text == ""){
				singleton.sitemethod.dspJoinErrMsg("訪問日を入力してください。",
					null, "", "0", null, null, true);
				view.grp1.dataProvider = arrMA009X;
				return;
			}
			if (BasedMethod.DateChk(view.edtHDAT.text) == false){
				singleton.sitemethod.dspJoinErrMsg("正しい訪問日を入力して下さい。",
					null, "", "0", null, null, true);
				view.grp1.dataProvider = arrMA009X;
				return;
			}
			// 2014.06.16 SE-233 end
			
			
			stmt = new SQLStatement();
			//stmt.sqlConnection =  connection;
			stmt.sqlConnection =  singleton.sqlconnection;
			//stmt.itemClass = LM010;
			//stmt.text= "SELECT * FROM とくいさき WHERE HDAT=? AND AREA=?";
			//stmt.text= "SELECT * FROM LM010 WHERE TKAREA='01' AND HPYM='2012/06' AND TKCD LIKE ";
			//stmt.text= "SELECT * FROM LM010 WHERE ROWID <100";
			stmt.text= singleton.g_query_xml.entry.(@key == "qryMA009X");
			
			// CHGCD
			stmt.parameters[0] = view.edtHDAT.text;
			stmt.parameters[1] = view.edtHDAT.text;
			stmt.parameters[2] = view.edtHDAT.text;
			
			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, cdsMA009XResult, false, 0, true);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler, false, 0, true);
			
			//実行
			stmt.execute();
		}
		
		/**********************************************************
		 * データインサート処理
		 * @author 12.10.25 SE-354
		 **********************************************************/
		public function insMA009X(data: LM010): void
		{
			stmt = new SQLStatement();
			//stmt.sqlConnection =  connection;
			stmt.sqlConnection =  singleton.sqlconnection;
			
			stmt.text= singleton.g_query_xml.entry.(@key == "insMA020X2");
			
			// CHGCD
			stmt.parameters[0] = data.TKCHGCD;
			// HDAT
			stmt.parameters[1] = view.edtHDAT.text;
			// TKCD
			stmt.parameters[2] = data.TKCD;
			
			stmt.execute();
		}
		
		protected function stmtErrorHandler(event:SQLErrorEvent):void
		{
			// TODO Auto-generated method stub
			// エラーの表示
			trace("SQLerr");
		}
		
		/**
		 * SELECT処理結果
		 */
		private function cdsMA009XResult(event:SQLEvent):void {
			//データの取得に成功したら
			//getResult()でSQLResultを取得し
			//取得したデータをデータグリッドに反映します。
			//status += "  データグリッド更新";
			var result:SQLResult = stmt.getResult();
			
			//var w_arr: ArrayCollection;
			//var i: int;
			
			stmt.removeEventListener(SQLEvent.RESULT, cdsMA009XResult);				
			stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);				
			stmt=null;
			
			arrMA009X = new ArrayCollection(result.data);
			view.grp1.dataProvider = arrMA009X;
			
			// 集計
			calcSum();
			
			if (result.data == null)
			{
				//singleton.sitemethod.dspJoinErrMsg("E0020");
				view.edtHKEN.text = "0件";
				return;
			}
			
			view.edtHKEN.text = arrMA009X.length.toString() + "件";
		}
		
		/**********************************************************
		 * 合計算出処理
		 * @author 12.11.16 SE-233
		 **********************************************************/
		public function calcSum(): void
		{
			var w_absent2: Number = 0;
			var w_absent1: Number = 0;
			var w_estamt: Number = 0;
			var w_amt: Number = 0;
			var w_money: Number = 0;
			
			for(var i:int=0; i < arrMA009X.length; i++){
				if(arrMA009X[i].ABSENT=="2"){
					w_absent2++;	
					w_estamt += Number(arrMA009X[i].ESTAMT);
				}else if(arrMA009X[i].ABSENT=="1"){
					w_absent1++;	
				}
				w_amt += Number(arrMA009X[i].AMT);
				w_money += Number(arrMA009X[i].MONEY);
			}
			view.edtABSENT2.text = w_absent2.toString() + "件";
			view.edtABSENT1.text = w_absent1.toString() + "件";
			view.edtYSAMT.text = singleton.g_customnumberformatter.numberFormat(w_estamt.toString(), "1", "0", "0") + "円";
			view.edtJSAMT.text = singleton.g_customnumberformatter.numberFormat(w_amt.toString(), "1", "0", "0") + "円";
			view.edtMONEY.text = singleton.g_customnumberformatter.numberFormat(w_money.toString(), "1", "0", "0") + "円";
		}
		
		/**********************************************************
		 * btnMAP クリック時処理.
		 * @author 12.08.13 SE-354
		 **********************************************************/
		public function btnMAPClick(): void
		{
			webView.stage = view.stage;
			
			webView.viewPort = new Rectangle(0, 55, 1280, 740);
			webView.loadURL(MAP_URL);
			
			webView.addEventListener(Event.COMPLETE,onMapComplete, false, 0, true);
		}
		
		protected function onMapComplete(event:Event):void 
		{
			webView.removeEventListener(Event.COMPLETE,onMapComplete);
			
			webView.addEventListener(LocationChangeEvent.LOCATION_CHANGE,update, false, 0, true);
			
			/*
			// 地図の中心を現在地にする
			webView.loadURL("javascript:centerLnglat(" + w_longitude + "," + w_latitude + ")");
			*/
			
			
			if (arrMA009X != null && arrMA009X.length != 0)
			{
				// 2012.12.10 SE-233 start				
				//if (arrMA009X[0].LAT != null && arrMA009X[0].LAT != "0")
				//{
				//	// 地図の中心を一番上の顧客の住所にする
				//	webView.loadURL("javascript:centerLnglat(" + arrMA009X[0].LNG + "," + arrMA009X[0].LAT + ")");
				//}
				//else
				//{
				//	// 地図の中心を現在地にする
				//	webView.loadURL("javascript:centerLnglat(" + w_longitude + "," + w_latitude + ")");
				//}
				var w_latitude1: Number = w_latitude;
				var w_longitude1: Number = w_longitude;
				
				for (var ii:int=0; ii<arrMA009X.length; ii++){
					if (arrMA009X[ii].LAT != null && arrMA009X[ii].LAT != "0"){
						w_latitude1 = arrMA009X[ii].LAT;
						w_longitude1 = arrMA009X[ii].LNG;
						break;
					}
				}
				webView.loadURL("javascript:centerLnglat(" + w_longitude1 + "," + w_latitude1 + ")");
				// 2012.12.10 SE-233 end				
				
				for (var i:int=0; i<arrMA009X.length; i++)
				{
					if (arrMA009X[i].LAT != null)
					{
						webView.loadURL("javascript:addMarker(markers," + arrMA009X[i].LAT + "," + arrMA009X[i].LNG + ", '"+ arrMA009X[i].TKNM + "<BR>" + BasedMethod.nvl(arrMA009X[i].TKADD1) + BasedMethod.nvl(arrMA009X[i].TKADD2) + BasedMethod.nvl(arrMA009X[i].TKADD3) +"')");
					}
				}
			}
			else
			{
				// 地図の中心を現在地にする
				webView.loadURL("javascript:centerLnglat(" + w_longitude + "," + w_latitude + ")");
			}
			//webView.loadURL("javascript:addMarker(markers, 34.97349, 138.389533, '静鉄ホテルプレジオ 静岡駅北')");
			
		}
		
		protected function update(event:LocationChangeEvent):void
		{
			trace("LocationChange!");
		}
		
		/**********************************************************
		 * Backキー押下時処理.
		 * @author 12.08.22 SE-354
		 **********************************************************/
		protected function onKeyDownHandler(e:KeyboardEvent):void
		{
			if (e.keyCode == Keyboard.BACK)
			{
				if (webView.stage != null)
				{
					e.preventDefault();
					webView.stage = null;
				}
				//else
				//{
				//	e.preventDefault();
				
				//view.stage.removeEventListener(KeyboardEvent.KEY_DOWN, onKeyDownHandler);
				
				//	view.navigator.popView();
				//}
				//if (this.webView.)
				//{
				//webView.dispose();
				//e.preventDefault();
				
				//navigator.popView();
				//}
			}
		}
		
		/**********************************************************
		 * 訪問計画 登録処理.
		 * @author 12.07.03 SE-354
		 **********************************************************/
		public function btnDECClick(): void
		{
			//trace(view.grp2.dataProvider.length);
			checkDataSet();
		}
		
		override public function updateDataSet_H(): void
		{
			// 削除処理
			updateMA009X();
			
			// 確定処理
			//updateMA020X2();
		}
		
		/**********************************************************
		 * 訪問実績更新処理
		 * @author 12.09.11 SE-354
		 **********************************************************/
		public function updateMA009X(): void
		{
			stmt = new SQLStatement();
			//stmt.sqlConnection =  connection;
			stmt.sqlConnection =  singleton.sqlconnection;
			
			stmt.text= singleton.g_query_xml.entry.(@key == "updMA009X");
			//stmt.itemClass = LS001;
			
			// まずはLF010のDELETE
			
			
			var iterator:Iterator = new CustomIterator(arrMA009X);
			var w_LF010: Object = new Object();
			
			while (iterator.hasNext())
			{
				w_LF010 = iterator.next();
				
				// ABSENT
				stmt.parameters[0] = w_LF010.ABSENT;
				// ABTIME1
				stmt.parameters[1] = w_LF010.ABTIME1;
				// ABTIME2
				stmt.parameters[2] = w_LF010.ABTIME2;
				// ABTIME3
				stmt.parameters[3] = w_LF010.ABTIME3;
				// CHGCD
				//stmt.parameters[1] = w_LF010.TKCHGCD;
				stmt.parameters[4] = w_LF010.TKCHGCD;
				// HDAT
				//stmt.parameters[2] = view.edtHDAT.text;
				//stmt.parameters[5] = view.edtHDAT.text;
				stmt.parameters[5] = w_LF010.HDAT;
				// TKCD
				//stmt.parameters[3] = w_LF010.TKCD;
				stmt.parameters[6] = w_LF010.TKCD;
				
				stmt.execute();
			}
			
			//イベント登録
			//stmt.addEventListener(SQLEvent.RESULT, updateMA020X2Result);
			//stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);
			
			//実行
			//stmt.execute();
			
			// 「YYYY/MM/DDの訪問計画が作成されました。」
			//singleton.sitemethod.dspJoinErrMsg("I0004", btnRTNClick_last, "登録完了", "0", [view.edtHDAT.text + "の訪問計画"]);
			
			//trace("END");
		}
		
		
		/**********************************************************
		 * btnRTN クリック時処理.
		 * @author 12.07.03 SE-354
		 **********************************************************/
		public function btnRTNClick(): void
		{
			// 制御マスタメンテを終了します。よろしいですか？
			//singleton.sitemethod.dspJoinErrMsg();
			
			if (webView.stage != null)
			{
				webView.stage = null;
			}
			else
			{
				// メニューに戻る
				view.navigator.popView();
			}
			
		}
		
		public function btnRTNClick_last(): void
		{	
			// メニューに戻る
			view.navigator.popView();
		}
	}
}