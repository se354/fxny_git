package views
{
	import flash.data.SQLConnection;
	import flash.data.SQLResult;
	import flash.data.SQLStatement;
	import flash.display.DisplayObjectContainer;
	import flash.events.KeyboardEvent;
	import flash.events.SQLErrorEvent;
	import flash.events.SQLEvent;
	import flash.ui.Keyboard;
	import flash.utils.getTimer;
	
	import modules.base.BasedMethod;
	import modules.custom.AlertDialog;
	import modules.custom.CustomIterator;
	import modules.custom.sub.Iterator;
	import modules.dto.LM010;
	import modules.dto.MA060V01;
	import modules.formatters.CustomNumberFormatter;
	import modules.sbase.SiteMethod;
	import modules.sbase.SiteModule;
	
	import mx.collections.ArrayCollection;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	import mx.rpc.http.HTTPService;
	
	import spark.skins.spark.StackedFormHeadingSkin;
	
	/**********************************************************
	 * MA060X 提供数入力
	 * @author 12.09.06 SE-362
	 **********************************************************/
	public class MA060XController extends SiteModule
	{
		private var view:MA060X;
		private var g_cucd: String;			// 客先コード
		private var g_time: String;			// 訪問時間
		private var g_vseq: String;			// 伝票NO
		private var arrMA060X: ArrayCollection;
		private var stmt: SQLStatement;
		private var popup: MP060X;				// 提供数ポップアップ

		// 引数情報
		private var cdsMA060X_arg: Object;

		public function MA060XController()
		{
			super();
		}
		
		/**********************************************************
		 * 初期処理
		 * @author  12.09.06 SE-362
		 **********************************************************/
		override public function init():void
		{
			// フォーム宣言
			view = _view as MA060X;

			// 12.10.12 SE-362 Redmine#3435 対応 start
			//view.stage.addEventListener("keyDown", keyDownBack, false, 1);
			view.stage.addEventListener(KeyboardEvent.KEY_DOWN, onKeyDownHandler, false, 0, true);
			// 12.10.12 SE-362 Redmine#3435 対応 end

			super.init();
		}

		/**********************************************************
		 * 画面立ち上げ処理
		 * @author  12.09.06 SE-362
		 **********************************************************/
		override public function formShow_last():void
		{
			// 制御マスタ情報取得
			cdsSEIGYO(formShow_last2);
		}

		private function formShow_last2(): void
		{
			super.formShow_last();

			// 業務日付表示
			view.edtHDAT.text = arrSEIGYO[0].SDDAY;

			doDSPProc();
		}
		
		/**********************************************************
		 * ボディデータ取得
		 * @author  12.09.06 SE-362
		 **********************************************************/
		override public function openDataSet_B(): void
		{
			cdsMA060X(view.edtHDAT.text, super.openDataSet_B);

			insLF013("","提供数入力開始");
		}

		/**********************************************************
		 * 画面項目セット
		 * @author SE-354 
		 * @date 12.04.27
		 **********************************************************/
		override public function setDispObject(): void
		{
			super.setDispObject();
			
			var w_amt: String;
			var w_money: String;
			
			if(arrMA060X != null && arrMA060X.length != 0){
				// 表示処理
				view.grp1.dataProvider = arrMA060X;
				
				// ヘッダ合計情報表示
				view.edtCOUNT.text = arrMA060X.length.toString();	// 件数
				for(var i: int; i < arrMA060X.length; i++){
					w_amt = BasedMethod.calcNum(arrMA060X[i].AMT, w_amt);
					w_money = BasedMethod.calcNum(arrMA060X[i].MONEY, w_money);
				}
			}else{
				view.grp1.dataProvider = null;
				// 該当データなし
				//singleton.sitemethod.dspJoinErrMsg("E0020", doDSPProc_last);
				return;
			}
			
			view.edtAMT.text = w_amt;
			view.edtMONEY.text = w_money;
		}
		
		//////////////////////////個別処理//////////////////////////////
		/**********************************************************
		 * 訪問情報取得
		 * @author 12.09.06 SE-362
		 **********************************************************/
		public function cdsMA060X(dat: String, func: Function=null): void
		{
			// 引数情報
			cdsMA060X_arg = new Object;
			cdsMA060X_arg.func = func;
			
			stmt = new SQLStatement();
			stmt.sqlConnection =  singleton.sqlconnection;
			stmt.parameters[0] = dat;
			stmt.itemClass = MA060V01;
			stmt.text= singleton.g_query_xml.entry.(@key == "qryMA060X");
			
			
			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, cdsMA060XResult, false, 0, true);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler, false, 0, true);
			
			//実行
			stmt.execute();
		}
		
		public function cdsMA060XResult(event: SQLEvent): void
		{
			var w_arg: Object = cdsMA060X_arg;
			var result:SQLResult = stmt.getResult();
			
			stmt.removeEventListener(SQLEvent.RESULT, cdsMA060XResult);				
			stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);				
			stmt=null;
			
			arrMA060X = new ArrayCollection(result.data);
			
			// 次関数実行
			if(w_arg.func)
				w_arg.func();
		}
		
		private function stmtErrorHandler(event: SQLErrorEvent): void
		{
			// エラーの表示
			trace(event.error.message);
		}
		
		/**********************************************************
		 * 明細クリック
		 * @author 12.09.06 SE-362
		 **********************************************************/
		public function grp1Click(tkcd: String, tknm: String): void
		{
			// ポップアップ立ち上げ
			popup = new MP060X();
			
			// 引数情報
			popup.ctrl.argHDAT = view.edtHDAT.text;
			popup.ctrl.argTKCD = tkcd;
			popup.ctrl.argTKNM = tknm;
			
			// 入力画面立ち上げ					
			PopUpManager.addPopUp(popup, view, true);
			PopUpManager.centerPopUp(popup);
			popup.addEventListener(FlexEvent.REMOVE, grp1Click_last, false, 0, true);
		}

		private function grp1Click_last(e: FlexEvent): void
		{
			popup.removeEventListener(FlexEvent.REMOVE, grp1Click_last);
			popup = null;
			//画面更新
			cdsMA060X(view.edtHDAT.text, setDispObject);
		}
		
		/**********************************************************
		 * MAPClick
		 * @author 12.09.06 SE-362
		 **********************************************************/
		public function btnMAPClick(): void
		{
			
		}
		
		/**********************************************************
		 * btnMOTO クリック時処理.
		 * @author 12.07.03 SE-354
		 **********************************************************/
		public function btnMOTOClick(): void
		{
			view.stage.removeEventListener(KeyboardEvent.KEY_DOWN, onKeyDownHandler);
			
			view.navigator.pushView(MF010X);
		}

		// 12.10.12 SE-362 Redmine#3435 対応 start
		protected function onKeyDownHandler(e:KeyboardEvent):void
		{
			//e.preventDefault();			
			if(e.keyCode == Keyboard.BACK)
			{
				e.preventDefault();
				btnRTNClick();
			}
		}
		// 12.10.12 SE-362 Redmine#3435 対応 end

		/**********************************************************
		 * 戻るClick
		 * @author 12.09.06 SE-362
		 **********************************************************/
		public function btnRTNClick(): void
		{
			// 12.10.12 SE-362 Redmine#3435 対応 start
			// 確認画面の表示
			singleton.sitemethod.dspJoinErrMsg("Q0027", btnRTNClick_last, "", "1", null, btnRTNClick_last2);
			// 12.10.12 SE-362 Redmine#3435 対応 end
		}
		
		// 12.10.12 SE-362 Redmine#3435 対応 start
		private function btnRTNClick_last(): void
		{
			view.stage.removeEventListener(KeyboardEvent.KEY_DOWN, onKeyDownHandler);
			
			insLF013("","提供数入力終了");

			// 前画面に遷移
			view.navigator.popView();
		}
		
		private function btnRTNClick_last2(): void
		{
			// 無処理
		}
		// 12.10.12 SE-362 Redmine#3435 対応 end
		
	}
}