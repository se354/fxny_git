package views
{
	import flash.data.SQLResult;
	import flash.data.SQLStatement;
	import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.SQLErrorEvent;
	import flash.events.SQLEvent;
	import flash.ui.Keyboard;
	
	import modules.base.BasedMethod;
	import modules.dto.LM010;
	import modules.dto.LM015;
	import modules.dto.LM017;
	import modules.dto.LS010;
	import modules.sbase.SiteModule;
	
	import mx.collections.ArrayCollection;
	import mx.core.FlexGlobals;
	import mx.managers.PopUpManager;
	
	public class POPUP008Controller extends SiteModule
	{
		private var view:POPUP008;
		
		//public var argDATA: LM010;				// LM010型データ
		public var argDATA: Object;				// LM010型データ
		
		public var argTKCD: String;				// 得意先コード
		public var argTKNM: String;				// 顧客名
		public var argTKTEL: String;				// TEL
		public var argTKMTEL: String;				// 携帯
		public var argLHDAT: String;				// 前回訪問年月
		public var argHSPAN: String;				// 訪問間隔
		public var argHAMT: String;				// 配置薬前回売上
		public var argGAMT: String;				// 別売前回売上
		public var argESTAMT: String;				// 予測売上
		public var argIMPCLS: String;				// 重点区分
		public var argHTCLS: String;				// 訪問時間
		public var argMEMO: String;				// 備考
		
		private var arrIMPCLS: ArrayCollection;
		private var arrHTCLS: ArrayCollection;
		private var arrCUCD2: ArrayCollection;
		private var arrCLS: ArrayCollection;

		private var cdsCUCD2_arg: Object;
		private var cdsCLS_arg: Object;
		
		private var stmt: SQLStatement;
		
		public function POPUP008Controller()
		{
			super();
		}
		
		override public function init():void
		{
			// フォーム宣言
			view = _view as POPUP008;

			// 変数の設定
			//g_prgid     = "MENUX";
			super.init();
		}
		
		override public function getComboData():void
		{
			super.getComboData();
		}
		
		override public function openComboData(): void
		{
			cdsIMPCLS();
			cdsHTCLS();
			cdsCUCD2();
			cdsCLS();
			
			super.openComboData();
		}
		
		override public function formShow(): void
		{
			doDSPProc();
		}

		override public function setDispObject():void
		{
//			argHTCLS = argDATA.HTCLS;
			
			view.edtTKCD.text = argTKCD;
			view.edtTKNM.text = argTKNM;
			view.edtTKTEL.text = argTKTEL;
			view.edtTKMTEL.text = argTKMTEL;
			view.edtLHDAT.text = argLHDAT;
			view.edtHSPAN.text = argHSPAN;
			view.edtHAMT.text = argHAMT;
			view.edtGAMT.text = argGAMT;
			view.edtESTAMT.text = argESTAMT;
			//view.edtIMPCLS.text = argIMPCLS;
			
			if (arrIMPCLS.length != 0)
			{
				view.edtIMPCLS.text = arrIMPCLS[0].IMPCLSNM;
			}
			
			//if (BasedMethod.nvl(argHTCLS) != "")
			if (BasedMethod.nvl(argDATA.HTCLS) != "")
			{
//				if (argHTCLS == "1")
//				{
//					view.edtHTCLS.text = "午前";
//				}
//				else if (argHTCLS == "2")
//				{
//					view.edtHTCLS.text = "午後";
//				}
//				else if (argHTCLS == "3")
//				{
//					view.edtHTCLS.text = "夕方";
//				}
//				else if (argHTCLS == "4")
//				{
//					view.edtHTCLS.text = "夜";
//				}
				view.edtHTCLS.text = arrHTCLS[0].HTIMENM;
			}
			view.edtMEMO.text = argMEMO;

			if(arrCUCD2[0].KBN1=="1"){
				view.edtHCHU1.text = arrCLS[0].CLSNM1;
			}else{
				view.edtHCHU1.text = "";
			}
			if(arrCUCD2[0].KBN2=="1"){
				view.edtHCHU2.text = arrCLS[0].CLSNM2;
			}else{
				view.edtHCHU2.text = "";
			}
			if(arrCUCD2[0].KBN3=="1"){
				view.edtHCHU3.text = arrCLS[0].CLSNM3;
			}else{
				view.edtHCHU3.text = "";
			}
			if(arrCUCD2[0].KBN4=="1"){
				view.edtHCHU4.text = arrCLS[0].CLSNM4;
			}else{
				view.edtHCHU4.text = "";
			}
			if(arrCUCD2[0].KBN5=="1"){
				view.edtHCHU5.text = arrCLS[0].CLSNM5;
			}else{
				view.edtHCHU5.text = "";
			}
			if(arrCUCD2[0].KBN6=="1"){
				view.edtHCHU6.text = arrCLS[0].CLSNM6;
			}else{
				view.edtHCHU6.text = "";
			}
			if(arrCUCD2[0].KBN7=="1"){
				view.edtHCHU7.text = arrCLS[0].CLSNM7;
			}else{
				view.edtHCHU7.text = "";
			}
			if(arrCUCD2[0].KBN8=="1"){
				view.edtHCHU8.text = arrCLS[0].CLSNM8;
			}else{
				view.edtHCHU8.text = "";
			}
			
			super.setDispObject();
		}
		
		
		/**********************************************************
		 * Backキー押下時処理.
		 * @author 12.08.22 SE-354
		 **********************************************************/
		protected function onKeyDownHandler(e:KeyboardEvent):void
		{
			if(e.keyCode == Keyboard.BACK)
			{
				e.preventDefault();
				btnOKClick();
			}
		}
		
		/**********************************************************
		 * 重点区分コンボ用select
		 * @author 12.09.07 SE-354
		 **********************************************************/ 
		public function cdsIMPCLS(): void
		{
			// 変数の初期化
			arrIMPCLS = null;
			
			stmt = new SQLStatement();
			stmt.sqlConnection =  singleton.sqlconnection;
			stmt.itemClass = LM015;
			stmt.text= singleton.g_query_xml.entry.(@key == "qryIMPCLS");
			
			stmt.parameters[0] = BasedMethod.nvl(argDATA.IMPCLS);
			//stmt.parameters[1] = p_param2;
			
			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, cdsIMPCLSResult);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);
			
			//実行
			stmt.execute();
		}
		private function cdsIMPCLSResult(event:SQLEvent):void 
		{
			//データの取得に成功したら
			//getResult()でSQLResultを取得し
			//取得したデータをデータグリッドに反映します。
			//status += "  データグリッド更新";
			var result:SQLResult = stmt.getResult();
			
			stmt.removeEventListener(SQLEvent.RESULT, cdsIMPCLSResult);
			stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);
			stmt=null;
			
			/*
			if (result.data == null)
			{
				//trace("nodata");
			}
			*/
			arrIMPCLS = new ArrayCollection(result.data);
			
			//execSetPLSQL();
		}
		
		/**********************************************************
		 * 訪問時間コンボ用select
		 * @author 12.11.15 SE-233
		 **********************************************************/ 
		public function cdsHTCLS(): void
		{
			// 変数の初期化
			arrHTCLS = null;
			
			stmt = new SQLStatement();
			stmt.sqlConnection =  singleton.sqlconnection;
			stmt.itemClass = LM017;
			stmt.text= singleton.g_query_xml.entry.(@key == "qryHTIMECD2");
			
			stmt.parameters[0] = BasedMethod.nvl(argDATA.HTCLS);
			
			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, cdsHTCLSResult);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);
			
			//実行
			stmt.execute();
		}
		private function cdsHTCLSResult(event:SQLEvent):void 
		{
			var result:SQLResult = stmt.getResult();
			
			stmt.removeEventListener(SQLEvent.RESULT, cdsHTCLSResult);
			stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);
			stmt=null;
		
			arrHTCLS = new ArrayCollection(result.data);
		}

		/**********************************************************
		 * 得意先マスタ情報取得
		 * @author 13.01.29 SE-233
		 **********************************************************/
		public function cdsCUCD2(func: Function=null): void
		{
			// 引数情報
			cdsCUCD2_arg = new Object;
			cdsCUCD2_arg.func = func;
			
			stmt = new SQLStatement();
			stmt.sqlConnection =  singleton.sqlconnection;
			stmt.parameters[0] = argTKCD;
			stmt.itemClass = LM010;
			stmt.text= singleton.g_query_xml.entry.(@key == "qryCUCD2");
			
			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, cdsCUCD2Result);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);
			
			//実行
			stmt.execute();
		}
		
		public function cdsCUCD2Result(event: SQLEvent): void
		{
			var w_arg: Object = cdsCUCD2_arg;
			var result:SQLResult = stmt.getResult();
			
			stmt.removeEventListener(SQLEvent.RESULT, cdsCUCD2Result);
			stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);
			stmt=null;
		
			arrCUCD2 = new ArrayCollection(result.data);
			
			// 次関数実行
			if(w_arg.func)
				w_arg.func();
		}

		/**********************************************************
		 * 得意先区分Ｆ情報取得
		 * @author 13.01.29 SE-233
		 **********************************************************/
		public function cdsCLS(func: Function=null): void
		{
			// 引数情報
			cdsCLS_arg = new Object;
			cdsCLS_arg.func = func;
			
			stmt = new SQLStatement();
			stmt.sqlConnection =  singleton.sqlconnection;
			stmt.parameters[0] = 1;
			stmt.itemClass = LS010;
			stmt.text= singleton.g_query_xml.entry.(@key == "qryCLS");
			
			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, cdsCLSResult);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);
			
			//実行
			stmt.execute();
		}
		
		public function cdsCLSResult(event: SQLEvent): void
		{
			var w_arg: Object = cdsCLS_arg;
			var result:SQLResult = stmt.getResult();
			
			stmt.removeEventListener(SQLEvent.RESULT, cdsCLSResult);
			stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);
			stmt=null;
		
			arrCLS = new ArrayCollection(result.data);
			
			// 次関数実行
			if(w_arg.func)
				w_arg.func();
		}
		
		protected function stmtErrorHandler(event:SQLErrorEvent):void
		{
			// TODO Auto-generated method stub
			// エラーの表示
			trace("SQLerr");
		}

		/**********************************************************
		 * btnOK クリック時処理.
		 * @author 12.08.22 SE-354
		 **********************************************************/
		public function btnOKClick(): void
		{
			PopUpManager.removePopUp(view);
		}
		
		/**********************************************************
		 * 元帳 クリック時処理.
		 * @author 12.10.25 SE-354
		 **********************************************************/		
		public function btnMOTOClick(): void
		{
			/*
			mp010x = new MP010X();
			
			mp010x.ctrl.argDATA = argDATA;
			
			PopUpManager.addPopUp(mp010x, singleton.view as DisplayObject, true);
			PopUpManager.centerPopUp(mp010x);
			*/
			FlexGlobals.topLevelApplication.navigator.pushView(MF010X, argDATA);
			PopUpManager.removePopUp(view);
		}
		
	}
}