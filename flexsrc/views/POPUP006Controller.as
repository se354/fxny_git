package views
{
	import flash.data.SQLResult;
	import flash.data.SQLStatement;
	import flash.events.Event;
	import flash.events.GeolocationEvent;
	import flash.events.KeyboardEvent;
	import flash.events.LocationChangeEvent;
	import flash.events.SQLErrorEvent;
	import flash.events.SQLEvent;
	import flash.geom.Rectangle;
	import flash.media.StageWebView;
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	import flash.sensors.Geolocation;
	import flash.ui.Keyboard;
	
	import modules.base.BasedMethod;
	import modules.dto.HANYOU;
	import modules.dto.LM006;
	import modules.dto.MA042V02;
	import modules.sbase.SiteModule;
	import modules.formatters.CustomNumberFormatter;
	
	import mx.collections.ArrayCollection;
	import mx.managers.PopUpManager;
	import mx.charts.LinearAxis;
	
	public class POPUP006Controller extends SiteModule
	{
		private var view:POPUP006;
		private var arrPOPUP006: ArrayCollection;
		
		public var argVJAN: String;				// 商品コード
		
		public var LM006Record: Object;

		private var stmt: SQLStatement;
		
		private var arrMA042X4: ArrayCollection;	// 商品別配置履歴
		private var cdsMA042X_arg: Object;
		private var cdsPOPUP006_arg: Object;
		
		public function POPUP006Controller()
		{
			super();
		}
		
		override public function init():void
		{
			// フォーム宣言
			view = _view as POPUP006;

			view.edtVJAN.text = argVJAN;
			
			// 変数の設定
			//g_prgid     = "MENUX";
			super.init();
		}
		
		override public function formShow_last():void
		{
			super.formShow_last();
			
			doDSPProc();
		}

		override public function openDataSet_B():void
		{
			//cdsPOPUP006()
			cdsPOPUP006(openDataSet_B_cld1)
		}

		private function openDataSet_B_cld1():void
		{
			cdsMA042X3(super.openDataSet_B)
		}
		
		override public function setDispObject():void
		{
			super.setDispObject();

			if(arrPOPUP006 != null && arrPOPUP006.length != 0){
				// 商品名
				view.edtGDNM.text = arrPOPUP006[0].GDNM;
				// 医薬分類名
				view.edtMDCLSNM.text = arrPOPUP006[0].MDCLSNM;
				// 注意事項
				view.edtCOM.text = arrPOPUP006[0].ATTENT;
			}
			
		}

		/**********************************************************
		 * SQLiteのselect
		 * @author 12.05.10 SE-354
		 **********************************************************/ 
//		public function cdsPOPUP006(): void
		public function cdsPOPUP006(func: Function=null): void
		{
			// 変数の初期化
			arrPOPUP006 = null;
			cdsPOPUP006_arg = new Object;
			cdsPOPUP006_arg.func = func;
			
			stmt = new SQLStatement();
			//stmt.sqlConnection =  connection;
			stmt.sqlConnection =  singleton.sqlconnection;
			stmt.itemClass = LM006;
			stmt.text= singleton.g_query_xml.entry.(@key == "qryPOPUP006");
			
			stmt.parameters[0] = argVJAN;
			
			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, cdsPOPUP006Result);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);
			
			//実行
			stmt.execute();
		}

		protected function stmtErrorHandler(event:SQLErrorEvent):void
		{
			stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);
			stmt=null;

			// TODO Auto-generated method stub
			// エラーの表示
			trace("SQLerr");
			doDSPProc_last();
		}
		
		/**
		 * SELECT処理結果
		 */
		private function cdsPOPUP006Result(event:SQLEvent):void {
			
			var w_arg: Object = cdsPOPUP006_arg;
			var result:SQLResult = stmt.getResult();
			
			stmt.removeEventListener(SQLEvent.RESULT, cdsPOPUP006Result);
			stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);
			stmt=null;
			
			arrPOPUP006 = new ArrayCollection(result.data);
			
			// 次関数実行
			if(w_arg.func)
				w_arg.func();

			//super.openDataSet_B();
		}
		
		/**********************************************************
		 * グラフ
		 * @author 12.09.06 SE-362
		 **********************************************************/
		public function cdsMA042X3(func: Function=null): void
		{
			// 引数情報
			cdsMA042X_arg = new Object;
			cdsMA042X_arg.func = func;
			
			stmt = new SQLStatement();
			stmt.sqlConnection =  singleton.sqlconnection;
			stmt.text= singleton.g_query_xml.entry.(@key == "qryMA042X3");
			stmt.itemClass = MA042V02;
			stmt.parameters[0] = argVJAN;
			
			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, cdsMA042X3Result);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);
			
			//実行
			stmt.execute();
		}
		
		public function cdsMA042X3Result(event: SQLEvent): void
		{	
			var w_arg: Object = cdsMA042X_arg;
			var result:SQLResult = stmt.getResult();
			
			stmt.removeEventListener(SQLEvent.RESULT, cdsMA042X3Result);
			stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);
			stmt=null;
			
			arrMA042X4 = new ArrayCollection(result.data);
			
			view.lineChart.dataProvider = arrMA042X4;
			
			// 次関数実行
			if(w_arg.func)
				w_arg.func();
		}
		
		public function YLabel(cat:Object,pcat:Object,ax:LinearAxis): String
		{
			var _customnumberformatter: CustomNumberFormatter = new CustomNumberFormatter();
			var n:Number = Number(cat);
			var n_comma: String;
			
			n_comma = _customnumberformatter.numberFormat(n.toString(), "1", "0", "0");
			
			return  n_comma;
		}
		
		/**********************************************************
		 * Backキー押下時処理.
		 * @author 12.08.22 SE-354
		 **********************************************************/
		protected function onKeyDownHandler(e:KeyboardEvent):void
		{
			e.preventDefault();
			
			btnOKClick();
		}

		/**********************************************************
		 * btnOK クリック時処理.
		 * @author 12.08.22 SE-354
		 **********************************************************/
		public function btnOKClick(): void
		{
			PopUpManager.removePopUp(view);
		}
	}
}