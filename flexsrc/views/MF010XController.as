package views
{
	import flash.data.SQLConnection;
	import flash.data.SQLResult;
	import flash.data.SQLStatement;
	import flash.display.DisplayObjectContainer;
	import flash.events.SQLErrorEvent;
	import flash.events.SQLEvent;
	import flash.utils.getTimer;
	
	import modules.base.BasedMethod;
	import modules.custom.AlertDialog;
	import modules.custom.CustomIterator;
	import modules.custom.sub.Iterator;
	import modules.datagrid.DataGridColumnRenderer;
	import modules.dto.LF012;
	import modules.dto.LM009;
	import modules.dto.LM010;
	import modules.dto.LS010;
	import modules.dto.MF010V01;
	import modules.dto.MF010V02;
	import modules.dto.MF010V03;
	import modules.dto.MF010V04;
	import modules.dto.MF010V05;
	import modules.dto.MF010V06;
	import modules.formatters.CustomNumberFormatter;
	import modules.sbase.SiteMethod;
	import modules.sbase.SiteModule;
	
	import mx.collections.ArrayCollection;
	import mx.core.ClassFactory;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	import mx.rpc.http.HTTPService;
	
	/**********************************************************
	 * MF010X 顧客元帳
	 * @author 12.09.13 SE-233
	 **********************************************************/
	public class MF010XController extends SiteModule
	{
		private var view:MF010X;
		private var g_cucd: String;			// 客先コード
		private var g_time: String;			// 訪問時間
		private var g_vseq: String;			// 伝票NO
		private var g_area: String;			// 地区コード
		private var pop: POPUP003;				// 得意先ポップアップ
		private var arrMF010X1: ArrayCollection;
		private var arrMF010X2: ArrayCollection;
		private var arrMF010X3: ArrayCollection;
		private var arrMF010X3H: ArrayCollection;
		private var arrMF010X4: ArrayCollection;
		private var arrMF010X5: ArrayCollection;
		private var arrMF010X6: ArrayCollection;
		
		public var tabdata: ArrayCollection;
		private var arrCUCD: ArrayCollection;
		private var arrCUCD2: ArrayCollection;
		private var arrAREA: ArrayCollection;
		private var arrCLS: ArrayCollection;
		private var stmt: SQLStatement;
		
		// 引数情報
		private var cdsCUCD2_arg: Object;
		private var cdsAREA_arg: Object;
		private var cdsCLS_arg: Object;
		private var cdsMF010X1_arg: Object;
		private var cdsMF010X2_arg: Object;
		private var cdsMF010X3_arg: Object;
		private var cdsMF010X4_arg: Object;
		private var cdsMF010X5_arg: Object;
		private var cdsMF010X6_arg: Object;

		public function MF010XController()
		{
			super();
		}
		
		/********************************************************** 
		 * 初期処理
		 * @author 12.09.13 SE-233
		 **********************************************************/
		override public function init():void
		{
			super.init();
			
			// フォーム宣言
			view = _view as MF010X;

			// 変数設定
			if(view.data){
				g_cucd = String(view.data.TKCD);
			}else{
				g_cucd = "";
			}

			tabdata = new ArrayCollection([
				"訪問履歴","配置薬履歴","別売履歴","提供履歴","配置薬一覧"
			]);

			// タブ制御
			view.tab.dataProvider = tabdata;
			view.tab.selectedIndex = 0;
			view.group2.visible = false;
			view.group3.visible = false;
			view.group5.visible = false;
			view.group6.visible = false;

			if(g_cucd!=""){
				cdsCUCD2_DSP();
			}
		}

		/**********************************************************
		 * ヘッダデータ取得
		 * @author 12.09.13 SE-233
		 **********************************************************/
		override public function openDataSet_H(): void
		{
			// 制御ﾏｽﾀ情報取得
			cdsSEIGYO(openDataSet_H_cld1);
		}

		private function openDataSet_H_cld1(): void
		{
			cdsMF010X1(openDataSet_H_last);
		}
		
		private function openDataSet_H_last(): void
		{
			// 担当者名取得
			cdsCHGCD2(arrSEIGYO[0].CHGCD, super.openDataSet_H);
		}
		
		/**********************************************************
		 * ボディデータ取得
		 * @author 12.09.13 SE-233
		 **********************************************************/
		override public function openDataSet_B(): void
		{
			cdsMF010X2(openDataSet_B_cld1);
		}

		private function openDataSet_B_cld1(): void
		{
			cdsMF010X3H(openDataSet_B_cld2);
		}
		
		private function openDataSet_B_cld2(): void
		{
			cdsMF010X3(openDataSet_B_cld3);
		}
		
		private function openDataSet_B_cld3(): void
		{
			cdsMF010X4(openDataSet_B_cld4);
		}

		private function openDataSet_B_cld4(): void
		{
			cdsMF010X5(openDataSet_B_cld5);
		}
		
		private function openDataSet_B_cld5(): void
		{
			cdsMF010X6(openDataSet_B_last);
		}
		
		private function openDataSet_B_last(): void
		{
// 14.03.03 SE-233 start
//			getTax(arrSEIGYO[0].SDDAY, super.openDataSet_B);
			var w_date:Date = new Date();
			var w_sysdate:String;
			w_sysdate = singleton.g_customdateformatter.dateDisplayFormat(w_date);
			getTax(w_sysdate, super.openDataSet_B);
// 14.03.03 SE-233 end
		}
		
		/**********************************************************
		 * 画面項目セット
		 * @author 12.09.13 SE-233
		 **********************************************************/
		override public function setDispObject(): void
		{
			super.setDispObject();

			//売掛時点
			view.lblDOWDAT.text = arrSEIGYO[0].DOWDAT;
			
			// ヘッダ項目表示
			if(arrMF010X1 != null && arrMF010X1.length != 0){
				view.lblHMEDIKURI.text = arrMF010X1[0].HMEDIKURI_COMMA;
				view.lblHSTOREKURI.text = arrMF010X1[0].HSTOREKURI_COMMA;
				view.lblHDEVKURI.text = arrMF010X1[0].HDEVKURI_COMMA;
				view.lblGMEDIKURI.text = arrMF010X1[0].GMEDIKURI_COMMA;
				view.lblGSTOREKURI.text = arrMF010X1[0].GSTOREKURI_COMMA;
				view.lblGDEVKURI.text = arrMF010X1[0].GDEVKURI_COMMA;
			}

			// 明細情報
			view.grdW02.dataProvider = arrMF010X2;
			view.grdW03.dataProvider = arrMF010X3;
			view.grdW04.dataProvider = arrMF010X4;
			view.grdW05.dataProvider = arrMF010X5;
			view.grdW06.dataProvider = arrMF010X6;

			if(arrMF010X3H != null && arrMF010X3H.length != 0){
				// 配置薬履歴ヘッダ設定
				view.lblHBAYI01.headerText = BasedMethod.nvl(arrMF010X3H[0].HBAYI01);
				view.lblHBAYI02.headerText = BasedMethod.nvl(arrMF010X3H[0].HBAYI02);
				view.lblHBAYI03.headerText = BasedMethod.nvl(arrMF010X3H[0].HBAYI03);
				view.lblHBAYI04.headerText = BasedMethod.nvl(arrMF010X3H[0].HBAYI04);
				view.lblHBAYI05.headerText = BasedMethod.nvl(arrMF010X3H[0].HBAYI05);
				view.lblHBAYI06.headerText = BasedMethod.nvl(arrMF010X3H[0].HBAYI06);
				view.lblHBAYI07.headerText = BasedMethod.nvl(arrMF010X3H[0].HBAYI07);
				view.lblHBAYI08.headerText = BasedMethod.nvl(arrMF010X3H[0].HBAYI08);
				view.lblHBAYI09.headerText = BasedMethod.nvl(arrMF010X3H[0].HBAYI09);
				view.lblHBAYI10.headerText = BasedMethod.nvl(arrMF010X3H[0].HBAYI10);
				view.lblHBAYI11.headerText = BasedMethod.nvl(arrMF010X3H[0].HBAYI11);
				view.lblHBAYI12.headerText = BasedMethod.nvl(arrMF010X3H[0].HBAYI12);
				view.lblHBAYI13.headerText = BasedMethod.nvl(arrMF010X3H[0].HBAYI13);
				view.lblHBAYI14.headerText = BasedMethod.nvl(arrMF010X3H[0].HBAYI14);
				view.lblHBAYI15.headerText = BasedMethod.nvl(arrMF010X3H[0].HBAYI15);
				view.lblHBAYI16.headerText = BasedMethod.nvl(arrMF010X3H[0].HBAYI16);
				view.lblHBAYI17.headerText = BasedMethod.nvl(arrMF010X3H[0].HBAYI17);
				view.lblHBAYI18.headerText = BasedMethod.nvl(arrMF010X3H[0].HBAYI18);
				view.lblHBAYI19.headerText = BasedMethod.nvl(arrMF010X3H[0].HBAYI19);
				view.lblHBAYI20.headerText = BasedMethod.nvl(arrMF010X3H[0].HBAYI20);
				view.lblHBAYI21.headerText = BasedMethod.nvl(arrMF010X3H[0].HBAYI21);
				view.lblHBAYI22.headerText = BasedMethod.nvl(arrMF010X3H[0].HBAYI22);
				view.lblHBAYI23.headerText = BasedMethod.nvl(arrMF010X3H[0].HBAYI23);
				view.lblHBAYI24.headerText = BasedMethod.nvl(arrMF010X3H[0].HBAYI24);
				view.lblHBAYI25.headerText = BasedMethod.nvl(arrMF010X3H[0].HBAYI25);
				view.lblHBAYI26.headerText = BasedMethod.nvl(arrMF010X3H[0].HBAYI26);
				view.lblHBAYI27.headerText = BasedMethod.nvl(arrMF010X3H[0].HBAYI27);
				view.lblHBAYI28.headerText = BasedMethod.nvl(arrMF010X3H[0].HBAYI28);
				view.lblHBAYI29.headerText = BasedMethod.nvl(arrMF010X3H[0].HBAYI29);
				view.lblHBAYI30.headerText = BasedMethod.nvl(arrMF010X3H[0].HBAYI30);
			}
			
//			for(var i:int=0; i<view.grdW03.columnsLength; i++){
//				if(view.grdW03.columns.getItemAt(i).dataField.substr(0, 5) == "HBAYI"){
//					view.grdW03.columns.getItemAt(i).itemRenderer = new ClassFactory(DataGridColumnRenderer);
//				}
//			}
			
		}
		
		//////////////////////////個別処理//////////////////////////////

		/**********************************************************
		 * SQLiteのselect
		 * @author 12.09.13 SE-233
		 **********************************************************/ 
		public function cdsCUCD(): void
		{
			// 変数の初期化
			arrCUCD = null;
			
			stmt = new SQLStatement();
			//stmt.sqlConnection =  connection;
			stmt.sqlConnection =  singleton.sqlconnection;
			stmt.itemClass = LM010;
			//stmt.text= "SELECT * FROM とくいさき WHERE HDAT=? AND AREA=?";
			//stmt.text= "SELECT * FROM LM010 WHERE TKAREA='01' AND HPYM='2012/06' AND TKCD LIKE ";
			//stmt.text= "SELECT * FROM LM010 WHERE ROWID <100";
			stmt.text= singleton.g_query_xml.entry.(@key == "qryCUCD");
			
			//stmt.parameters[0] = p_param1;
			//stmt.parameters[1] = p_param2;
			
			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, cdsCUCDResult, false, 0, true);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler, false, 0, true);
			
			//実行
			stmt.execute();
		}
		
		private function cdsCUCDResult(event:SQLEvent):void 
		{
			//データの取得に成功したら
			//getResult()でSQLResultを取得し
			//取得したデータをデータグリッドに反映します。
			//status += "  データグリッド更新";
			var result:SQLResult = stmt.getResult();
			
			stmt.removeEventListener(SQLEvent.RESULT, cdsCUCDResult);				
			stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);				
			stmt=null;
			
			arrCUCD = new ArrayCollection(result.data);
			
			//view.lcmbCUCD.dataProvider = arrCUCD;
			
			//view.grp1.dataProvider = arrMA020X;
			
			//execSetPLSQL();
		}

		/**********************************************************
		 * 得意先マスタ情報取得
		 * @author 12.09.14 SE-233
		 **********************************************************/
		public function cdsCUCD2(func: Function=null): void
		{
			// 引数情報
			cdsCUCD2_arg = new Object;
			cdsCUCD2_arg.func = func;
			
			stmt = new SQLStatement();
			stmt.sqlConnection =  singleton.sqlconnection;
			stmt.parameters[0] = g_cucd;
			stmt.itemClass = LM010;
			stmt.text= singleton.g_query_xml.entry.(@key == "qryCUCD2");
			
			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, cdsCUCD2Result, false, 0, true);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler, false, 0, true);
			
			//実行
			stmt.execute();
		}
		
		public function cdsCUCD2Result(event: SQLEvent): void
		{
			var w_arg: Object = cdsCUCD2_arg;
			var result:SQLResult = stmt.getResult();
			
			stmt.removeEventListener(SQLEvent.RESULT, cdsCUCD2Result);				
			stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);				
			stmt=null;
			
			arrCUCD2 = new ArrayCollection(result.data);
			
			// 次関数実行
			if(w_arg.func)
				w_arg.func();
		}
		
		/**********************************************************
		 * 地区マスタ情報取得
		 * @author 12.09.20 SE-233
		 **********************************************************/
		public function cdsAREA(func: Function=null): void
		{
			// 引数情報
			cdsAREA_arg = new Object;
			cdsAREA_arg.func = func;
			
			stmt = new SQLStatement();
			stmt.sqlConnection =  singleton.sqlconnection;
			stmt.parameters[0] = g_area;
			stmt.itemClass = LM009;
			stmt.text= singleton.g_query_xml.entry.(@key == "qryAREA2");
			
			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, cdsAREAResult, false, 0, true);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler, false, 0, true);
			
			//実行
			stmt.execute();
		}
		
		public function cdsAREAResult(event: SQLEvent): void
		{
			var w_arg: Object = cdsAREA_arg;
			var result:SQLResult = stmt.getResult();
			
			stmt.removeEventListener(SQLEvent.RESULT, cdsAREAResult);				
			stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);				
			stmt=null;
			
			arrAREA = new ArrayCollection(result.data);
			
			// 次関数実行
			if(w_arg.func)
				w_arg.func();
		}
		
		/**********************************************************
		 * 得意先区分Ｆ情報取得
		 * @author 12.09.14 SE-233
		 **********************************************************/
		public function cdsCLS(func: Function=null): void
		{
			// 引数情報
			cdsCLS_arg = new Object;
			cdsCLS_arg.func = func;
			
			stmt = new SQLStatement();
			stmt.sqlConnection =  singleton.sqlconnection;
			stmt.parameters[0] = 1;
			stmt.itemClass = LS010;
			stmt.text= singleton.g_query_xml.entry.(@key == "qryCLS");
			
			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, cdsCLSResult, false, 0, true);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler, false, 0, true);
			
			//実行
			stmt.execute();
		}
		
		public function cdsCLSResult(event: SQLEvent): void
		{
			var w_arg: Object = cdsCLS_arg;
			var result:SQLResult = stmt.getResult();
			
			stmt.removeEventListener(SQLEvent.RESULT, cdsCLSResult);				
			stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);				
			stmt=null;
			
			arrCLS = new ArrayCollection(result.data);
			
			// 次関数実行
			if(w_arg.func)
				w_arg.func();
		}
		
		/**********************************************************
		 * 訪問履歴（ヘッダ部）情報取得
		 * @author 12.09.13 SE-233
		 **********************************************************/
		public function cdsMF010X1(func: Function=null): void
		{
			// 引数情報
			cdsMF010X1_arg = new Object;
			cdsMF010X1_arg.func = func;
			
			stmt = new SQLStatement();
			stmt.sqlConnection =  singleton.sqlconnection;
			stmt.parameters[0] = g_cucd;
			stmt.itemClass = MF010V01;
			stmt.text= singleton.g_query_xml.entry.(@key == "qryMF010X1");
			
			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, cdsMF010X1Result, false, 0, true);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler, false, 0, true);
			
			//実行
			stmt.execute();
		}
		
		public function cdsMF010X1Result(event: SQLEvent): void
		{
			var w_arg: Object = cdsMF010X1_arg;
			var result:SQLResult = stmt.getResult();
			
			stmt.removeEventListener(SQLEvent.RESULT, cdsMF010X1Result);				
			stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);				
			stmt=null;
			
			arrMF010X1 = new ArrayCollection(result.data);
			
			// 次関数実行
			if(w_arg.func)
				w_arg.func();
		}

		/**********************************************************
		 * 訪問履歴（明細部）情報取得
		 * @author 12.09.13 SE-233
		 **********************************************************/
		public function cdsMF010X2(func: Function=null): void
		{
			// 引数情報
			cdsMF010X2_arg = new Object;
			cdsMF010X2_arg.func = func;
			
			stmt = new SQLStatement();
			stmt.sqlConnection =  singleton.sqlconnection;
			stmt.parameters[0] = g_cucd;
			stmt.itemClass = MF010V02;
			stmt.text= singleton.g_query_xml.entry.(@key == "qryMF010X2");
			
			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, cdsMF010X2Result, false, 0, true);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler, false, 0, true);
			
			//実行
			stmt.execute();
		}
		
		public function cdsMF010X2Result(event: SQLEvent): void
		{
			var w_arg: Object = cdsMF010X2_arg;
			var result:SQLResult = stmt.getResult();
			
			stmt.removeEventListener(SQLEvent.RESULT, cdsMF010X2Result);				
			stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);				
			stmt=null;
			
			arrMF010X2 = new ArrayCollection(result.data);
			
			// 次関数実行
			if(w_arg.func)
				w_arg.func();
		}
		
		/**********************************************************
		 * 配置薬履歴（明細部）情報取得
		 * @author 12.11.22 SE-233
		 **********************************************************/
		public function cdsMF010X3H(func: Function=null): void
		{
			// 引数情報
			cdsMF010X3_arg = new Object;
			cdsMF010X3_arg.func = func;
			
			stmt = new SQLStatement();
			stmt.sqlConnection =  singleton.sqlconnection;
			stmt.parameters[0] = g_cucd;
			stmt.itemClass = MF010V03;
			stmt.text= singleton.g_query_xml.entry.(@key == "qryMF010X3H");
			
			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, cdsMF010X3HResult, false, 0, true);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler, false, 0, true);
			
			//実行
			stmt.execute();
		}
		
		public function cdsMF010X3HResult(event: SQLEvent): void
		{
			var w_arg: Object = cdsMF010X3_arg;
			var result:SQLResult = stmt.getResult();
			
			stmt.removeEventListener(SQLEvent.RESULT, cdsMF010X3HResult);				
			stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);				
			stmt=null;
			
			arrMF010X3H = new ArrayCollection(result.data);

			// 次関数実行
			if(w_arg.func)
				w_arg.func();
		}
		
		/**********************************************************
		 * 配置薬履歴（明細部）情報取得
		 * @author 12.09.13 SE-233
		 **********************************************************/
		public function cdsMF010X3(func: Function=null): void
		{
			// 引数情報
			cdsMF010X3_arg = new Object;
			cdsMF010X3_arg.func = func;
			
			stmt = new SQLStatement();
			stmt.sqlConnection =  singleton.sqlconnection;
			stmt.parameters[0] = g_cucd;
			stmt.itemClass = MF010V03;
			stmt.text= singleton.g_query_xml.entry.(@key == "qryMF010X3");
			
			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, cdsMF010X3Result, false, 0, true);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler, false, 0, true);
			
			//実行
			stmt.execute();
		}
		
		public function cdsMF010X3Result(event: SQLEvent): void
		{
			var w_arg: Object = cdsMF010X3_arg;
			var result:SQLResult = stmt.getResult();
			
			stmt.removeEventListener(SQLEvent.RESULT, cdsMF010X3Result);				
			stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);				
			stmt=null;
			
			arrMF010X3 = new ArrayCollection(result.data);
			
			// 次関数実行
			if(w_arg.func)
				w_arg.func();
		}
		
		/**********************************************************
		 * 別売履歴情報取得
		 * @author 12.09.13 SE-233
		 **********************************************************/
		public function cdsMF010X4(func: Function=null): void
		{
			// 引数情報
			cdsMF010X4_arg = new Object;
			cdsMF010X4_arg.func = func;
			
			stmt = new SQLStatement();
			stmt.sqlConnection =  singleton.sqlconnection;
			stmt.parameters[0] = g_cucd;
			stmt.itemClass = MF010V04;
			stmt.text= singleton.g_query_xml.entry.(@key == "qryMF010X4");
			
			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, cdsMF010X4Result, false, 0, true);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler, false, 0, true);
			
			//実行
			stmt.execute();
		}
		
		public function cdsMF010X4Result(event: SQLEvent): void
		{
			var w_arg: Object = cdsMF010X4_arg;
			var result:SQLResult = stmt.getResult();
			
			stmt.removeEventListener(SQLEvent.RESULT, cdsMF010X4Result);				
			stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);				
			stmt=null;
			
			arrMF010X4 = new ArrayCollection(result.data);
			
			// 次関数実行
			if(w_arg.func)
				w_arg.func();
		}
		
		/**********************************************************
		 * 提供履歴情報取得
		 * @author 12.11.14 SE-233
		 **********************************************************/
		public function cdsMF010X5(func: Function=null): void
		{
			// 引数情報
			cdsMF010X5_arg = new Object;
			cdsMF010X5_arg.func = func;
			
			stmt = new SQLStatement();
			stmt.sqlConnection =  singleton.sqlconnection;
			stmt.parameters[0] = g_cucd;
			stmt.itemClass = MF010V05;
			stmt.text= singleton.g_query_xml.entry.(@key == "qryMF010X5");
			
			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, cdsMF010X5Result, false, 0, true);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler, false, 0, true);
			
			//実行
			stmt.execute();
		}
		
		public function cdsMF010X5Result(event: SQLEvent): void
		{
			var w_arg: Object = cdsMF010X5_arg;
			var result:SQLResult = stmt.getResult();
			
			stmt.removeEventListener(SQLEvent.RESULT, cdsMF010X5Result);				
			stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);				
			stmt=null;
			
			arrMF010X5 = new ArrayCollection(result.data);
			
			// 次関数実行
			if(w_arg.func)
				w_arg.func();
		}

		/**********************************************************
		 * 配置薬一覧情報取得
		 * @author 12.11.16 SE-233
		 **********************************************************/
		public function cdsMF010X6(func: Function=null): void
		{
			// 引数情報
			cdsMF010X6_arg = new Object;
			cdsMF010X6_arg.func = func;
			
			stmt = new SQLStatement();
			stmt.sqlConnection =  singleton.sqlconnection;
			stmt.parameters[0] = g_cucd;
			stmt.itemClass = MF010V06;
			stmt.text= singleton.g_query_xml.entry.(@key == "qryMF010X6");
			
			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, cdsMF010X6Result, false, 0, true);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler, false, 0, true);
			
			//実行
			stmt.execute();
		}
		
		public function cdsMF010X6Result(event: SQLEvent): void
		{
			var w_arg: Object = cdsMF010X6_arg;
			var result:SQLResult = stmt.getResult();
			
			stmt.removeEventListener(SQLEvent.RESULT, cdsMF010X6Result);				
			stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);				
			stmt=null;
			
			arrMF010X6 = new ArrayCollection(result.data);
			
			// 次関数実行
			if(w_arg.func)
				w_arg.func();
		}
		
		private function stmtErrorHandler(event: SQLErrorEvent): void
		{
			// エラーの表示
			trace(event.error.message);
		}
		
		/********************************************************** 
		 * 得意先選択時
		 * @author 12.09.13 SE-233
		 **********************************************************/
		public function cdsCUCD2_DSP(): void
		{
			//g_cucd = view.lcmbCUCD.selectedItem.TKCD;
			view.edtCUCD.text = g_cucd;

			cdsCUCD2(openDataSet_CUCD_cld1);
		}

		private function openDataSet_CUCD_cld1(): void
		{
			g_area = arrCUCD2[0].TKAREA;

			cdsAREA(openDataSet_CUCD_cld2);
		}
		
		private function openDataSet_CUCD_cld2(): void
		{
			
			cdsCLS(openDataSet_CUCD_last);
		}
		
		private function openDataSet_CUCD_last(): void
		{
			if(arrCUCD2 != null && arrCUCD2.length != 0){
				view.edtCUNM.text = arrCUCD2[0].TKNM;
				view.edtZIP.text = arrCUCD2[0].TKZIP;
				view.edtADD1.text = arrCUCD2[0].TKADD1;
				view.edtADD2.text = arrCUCD2[0].TKADD2;
				view.edtADD3.text = arrCUCD2[0].TKADD3;
				view.edtTEL.text = arrCUCD2[0].TKTEL;
				view.edtFAX.text = arrCUCD2[0].TKFAX;
				view.edtMAIL.text = arrCUCD2[0].TKMAIL1;
				view.edtLHDAT.text = arrCUCD2[0].LHDAT;
				view.edtLHTIME.text = arrCUCD2[0].LHTIME;
				view.edtHSPAN.text = arrCUCD2[0].HSPAN;
				if(arrCUCD2[0].HSPAN!=0){
					view.edtTHSPAN.text = "定期訪問";
				}else{
					view.edtTHSPAN.text = "";
				}
				view.edtNHDAT.text = arrCUCD2[0].HPYM;
				view.edtBOXINF.text = arrCUCD2[0].BOXINF;
				// 12.10.19 SE-233 Redmine#3520 対応 start
				view.edtMEMO.text = arrCUCD2[0].MEMO;
				// 12.10.19 SE-233 Redmine#3520 対応 end

				if(arrCUCD2[0].KBN1=="1"){
					view.edtHCHU1.text = arrCLS[0].CLSNM1;
				}else{
					view.edtHCHU1.text = "";
				}
				if(arrCUCD2[0].KBN2=="1"){
					view.edtHCHU2.text = arrCLS[0].CLSNM2;
				}else{
					view.edtHCHU2.text = "";
				}
				if(arrCUCD2[0].KBN3=="1"){
					view.edtHCHU3.text = arrCLS[0].CLSNM3;
				}else{
					view.edtHCHU3.text = "";
				}
				if(arrCUCD2[0].KBN4=="1"){
					view.edtHCHU4.text = arrCLS[0].CLSNM4;
				}else{
					view.edtHCHU4.text = "";
				}
				if(arrCUCD2[0].KBN5=="1"){
					view.edtHCHU5.text = arrCLS[0].CLSNM5;
				}else{
					view.edtHCHU5.text = "";
				}
				if(arrCUCD2[0].KBN6=="1"){
					view.edtHCHU6.text = arrCLS[0].CLSNM6;
				}else{
					view.edtHCHU6.text = "";
				}
				if(arrCUCD2[0].KBN7=="1"){
					view.edtHCHU7.text = arrCLS[0].CLSNM7;
				}else{
					view.edtHCHU7.text = "";
				}
				if(arrCUCD2[0].KBN8=="1"){
					view.edtHCHU8.text = arrCLS[0].CLSNM8;
				}else{
					view.edtHCHU8.text = "";
				}
				if(arrAREA != null && arrAREA.length != 0)
					view.edtAREA.text = arrAREA[0].AREANM;
			}else{
				// 該当レコードなし
				singleton.sitemethod.dspJoinErrMsg("E0020");
				return;
			}
			doDSPProc();
		}
		
		/**********************************************************
		 * btnKEN クリック時処理.
		 * @author 12.09.13 SE-233
		 **********************************************************/
		public function btnKENClick(): void
		{

			// 得意先ポップアップ呼び出し
			pop = new POPUP003();
			
			// 入力画面立ち上げ					
			PopUpManager.addPopUp(pop, view, true);
			PopUpManager.centerPopUp(pop);
			pop.addEventListener(FlexEvent.REMOVE, btnKENClick_last, false, 0, true);
			
			//doDSPProc();
		}
		
		private function btnKENClick_last(e: FlexEvent): void
		{
			pop.removeEventListener(FlexEvent.REMOVE, btnKENClick_last);
			if(pop.ctrl.LM010Record){
				g_cucd = pop.ctrl.LM010Record.TKCD;
				cdsCUCD2_DSP();
			}
			pop = null;
		}
		
		/**********************************************************
		 * btnRTN クリック時処理.
		 * @author 12.09.13 SE-233
		 **********************************************************/
		public function btnRTNClick(): void
		{
			// 前画面に遷移
			view.navigator.popView();
		}
		
		/**********************************************************
		 * tabClick Tabクリック時処理.
		 * @author 12.09.13 SE-233
		 **********************************************************/
		public function tabClick(): void
		{
			if(view.tab.selectedIndex == 0){
				view.group1.visible = true;
				view.group2.visible = false;
				view.group3.visible = false;
				view.group5.visible = false;
				view.group6.visible = false;
			}else if(view.tab.selectedIndex == 1){
				view.group1.visible = false;
				view.group2.visible = true;
				view.group3.visible = false;
				view.group5.visible = false;
				view.group6.visible = false;
			}else if(view.tab.selectedIndex == 2){
				view.group1.visible = false;
				view.group2.visible = false;
				view.group3.visible = true;
				view.group5.visible = false;
				view.group6.visible = false;
			}else if(view.tab.selectedIndex == 3){
				view.group1.visible = false;
				view.group2.visible = false;
				view.group3.visible = false;
				view.group5.visible = true;
				view.group6.visible = false;
			}else{
				view.group1.visible = false;
				view.group2.visible = false;
				view.group3.visible = false;
				view.group5.visible = false;
				view.group6.visible = true;
			}
		}

		/**********************************************************
		 * btnIMAGE クリック
		 * @author  12.09.06 SE-362
		 **********************************************************/
		public function btnIMAGEClick(): void
		{
			var popup: POPUP004 = new POPUP004();
			
			// 税率
			popup.ctrl.argTKCD = g_cucd;
			
			PopUpManager.addPopUp(popup, view, true);
			PopUpManager.centerPopUp(popup);
			
		}
}
}