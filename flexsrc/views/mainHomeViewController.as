package views
{	
	import flash.desktop.NativeApplication;
	import flash.display.DisplayObjectContainer;
	import flash.events.StatusEvent;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	
	import jp.co.emori.android.AutoUpdater.AutoUpdater;
	
	import modules.custom.AlertDialog;
	import modules.custom.CustomBusyIndicator;
	import modules.sbase.SiteConst;
	import modules.sbase.SiteModule;
	
	import spark.components.IconItemRenderer;
	import spark.primitives.BitmapImage;
	import spark.skins.mobile.supportClasses.MobileSkin;
	
	public class mainHomeViewController extends SiteModule
	{
		private var view:mainHomeView;
		private var _busyInd: CustomBusyIndicator;
		
		private var updater:AutoUpdater;
		
		public function mainHomeViewController()
		{
			super();
		}
		
		override public function init():void
		{
			// フォーム宣言
			view = _view as mainHomeView;
			super.init();

//			_busyInd = new CustomBusyIndicator();
			
//			_busyInd.open(DisplayObjectContainer(view));

			createXML();
			
//			checkVersion();

//	var a: IconItemRenderer;

			// 行動履歴Ｆ作成処理（テーブルが存在しない場合に作成される）
			//creLF013();
		}
		
		private function checkVersion(): void
		{
			var appDescriptor:XML = NativeApplication.nativeApplication.applicationDescriptor;
			var ns:Namespace = appDescriptor.namespace();
			var appVersion: String = appDescriptor.ns::versionNumber;
			var url: String = SiteConst.G_VERSION_URL;
			
			// アプリ内部バージョン情報表示
			view.lblVER.text = "Ver." + appVersion;
			
			// 最新バージョン取得
			updater = new AutoUpdater();
			
			// イベント追加
			updater.addEventListener(StatusEvent.STATUS, checkVersion_last);

			// バージョンチェック
			updater.update(url, appVersion);
			
		}
		
		private function checkVersion_last(event: StatusEvent): void
		{
//			trace(event.code);
			
			// 最新版に更新された場合はログアウト
			if(event.code == "modified"){
//				trace("更新するよー");
				
				// アラート表示
				singleton.sitemethod.dspJoinErrMsg("I0006", null, "", "2");
				btnLOGOUTClick();
				return;
			}else if(event.code == "noconnected"){
//				singleton.sitemethod.dspJoinErrMsg("W0002", super.init);
//				return;
//				trace("接続できませんでした。");
			}

			if(singleton.sitemethod.Alert)
				singleton.sitemethod.Alert.close();
			
			super.init();	
		}
		
		override public function formShow(): void
		{
			CreateDataBase();
			
//			if(_busyInd)
//				_busyInd.close();
		}
		
		/**********************************************************
		 * ログアウト クリック時処理.
		 * @author 12.08.14 SE-354
		 **********************************************************/
		public function btnLOGOUTClick(): void
		{
			// ANE(アップデーター)の停止
			updater.stop();
			
			// アプリの終了処理
			NativeApplication.nativeApplication.exit();
		}

		private function createXML(): void
		{
			if(singleton.g_query_xml == null || singleton.g_msg_xml == null){
				var file1: File = File.applicationDirectory.resolvePath(SiteConst.G_QUERY_FILE);
				var file2: File = File.applicationDirectory.resolvePath(SiteConst.G_MSG_FILE); 
				
				var stream1: FileStream = new FileStream();
				var stream2: FileStream = new FileStream();
				
				// XMLファイル読み込み
				stream1.open(file1, FileMode.READ);
				stream2.open(file2, FileMode.READ);
				singleton.g_query_xml = XML(stream1.readUTFBytes(stream1.bytesAvailable));
				singleton.g_msg_xml = XML(stream2.readUTFBytes(stream2.bytesAvailable));
				stream1.close();
				stream2.close();
			}
		}
	}
}