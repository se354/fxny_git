package views
{
	import flash.data.SQLResult;
	import flash.data.SQLStatement;
	import flash.events.Event;
	import flash.events.GeolocationEvent;
	import flash.events.KeyboardEvent;
	import flash.events.LocationChangeEvent;
	import flash.events.SQLErrorEvent;
	import flash.events.SQLEvent;
	import flash.geom.Rectangle;
	import flash.media.StageWebView;
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	import flash.sensors.Geolocation;
	import flash.ui.Keyboard;
	
	import modules.base.BasedMethod;
	import modules.custom.CustomIterator;
	import modules.custom.sub.Iterator;
	import modules.dto.HANYOU;
	import modules.dto.LM006;
	import modules.dto.LM009;
	import modules.dto.LM016;
	import modules.sbase.SiteModule;
	
	import mx.collections.ArrayCollection;
	import mx.managers.PopUpManager;
	
	public class POPUP007Controller extends SiteModule
	{
		private var view:POPUP007;
		
		private var stmt: SQLStatement;
		
		public var argAREA: String;				// 地区
		public var argDTAREA: String;				// 地区詳細
		public var argSHPYM: String;				// 開始訪問予定年月
		public var argEHPYM: String;				// 終了訪問予定年月
		public var argSLHDAT: String;				// 開始前回訪問年月
		public var argELHDAT: String;				// 開始前回訪問年月
		public var argESTAMT: String;				// 売上予測金額
		public var argIMPCLS: String;				// 重点区分
		public var argHTCLS: String;				// 訪問時間区分
		public var argTKCLS: String;				// 得意先区分
		public var argKURI: Boolean;				// 売掛残有のみか
		
		private var arrDTAREA: ArrayCollection;
		private var arrIMPCLS: ArrayCollection;
		private var arrHTCLS: ArrayCollection;
//		private var arrHTCLS: ArrayCollection = new ArrayCollection([
//			{CODE:"",NAME:"",COMBFIELD2:"未選択"},
//			{CODE:"1",NAME:"午前",COMBFIELD2:"午前"},
//			{CODE:"2",NAME:"午後",COMBFIELD2:"午後"},
//			{CODE:"3",NAME:"夕方",COMBFIELD2:"夕方"},
//			{CODE:"4",NAME:"夜",COMBFIELD2:"夜"}
//		]);
		
		private var arrTKCLS: ArrayCollection;
//		private var arrTKCLS: ArrayCollection = new ArrayCollection([
//			{CODE:"",NAME:"",COMBFIELD2:"未選択"},
//			{CODE:"1",NAME:"個人",COMBFIELD2:"個人"},
//			{CODE:"2",NAME:"法人",COMBFIELD2:"法人"},
//			{CODE:"3",NAME:"個人営業",COMBFIELD2:"個人営業"}
//		]);
		
		private var current_date: Date = new Date();
		
		private var arrYEAR: ArrayCollection = new ArrayCollection([
			{CODE:"",NAME:"",COMBFIELD2:"未選択"},
			{CODE:"1",NAME:(current_date.fullYear-1).toString(),COMBFIELD2:(current_date.fullYear-1).toString()},
			{CODE:"2",NAME:(current_date.fullYear).toString(),COMBFIELD2:(current_date.fullYear).toString()},
			{CODE:"3",NAME:(current_date.fullYear+1).toString(),COMBFIELD2:(current_date.fullYear+1).toString()}
		]);
		
		private var arrMONTH: ArrayCollection = new ArrayCollection([
			{CODE:"",NAME:"",COMBFIELD2:"未選択"},
			{CODE:"1",NAME:"01",COMBFIELD2:"01"},
			{CODE:"2",NAME:"02",COMBFIELD2:"02"},
			{CODE:"3",NAME:"03",COMBFIELD2:"03"},
			{CODE:"4",NAME:"04",COMBFIELD2:"04"},
			{CODE:"5",NAME:"05",COMBFIELD2:"05"},
			{CODE:"6",NAME:"06",COMBFIELD2:"06"},
			{CODE:"7",NAME:"07",COMBFIELD2:"07"},
			{CODE:"8",NAME:"08",COMBFIELD2:"08"},
			{CODE:"9",NAME:"09",COMBFIELD2:"09"},
			{CODE:"10",NAME:"10",COMBFIELD2:"10"},
			{CODE:"11",NAME:"11",COMBFIELD2:"11"},
			{CODE:"12",NAME:"12",COMBFIELD2:"12"}
		]);
		
		public var okflg: Boolean;
		
		public function POPUP007Controller()
		{
			super();
		}
		
		override public function init():void
		{
			// フォーム宣言
			view = _view as POPUP007;
			
			
			
			view.lcmbHTCLS.dataProvider = arrHTCLS;
			view.edtESTAMT.text = argESTAMT;
			
//			if (BasedMethod.nvl(argHTCLS) == "")
//			{
//				view.lcmbHTCLS.selectedItem = arrHTCLS[0];
//			}
//			else
//			{
//				var w_index:int = BasedMethod.getArrayIndex(arrHTCLS, "CODE", argHTCLS);
//				if (w_index != -1)
//				{
//					view.lcmbHTCLS.selectedItem = arrHTCLS[w_index];
//				}
//			}
			
			view.lcmbSHPYM_YEAR.dataProvider = arrYEAR;
			view.lcmbSHPYM_MONTH.dataProvider = arrMONTH;
			
			if (BasedMethod.nvl(argSHPYM) == "")
			{
				view.lcmbSHPYM_YEAR.selectedItem = arrYEAR[0];
				view.lcmbSHPYM_MONTH.selectedItem = arrMONTH[0];
			}
			else
			{
				var y_index:int = BasedMethod.getArrayIndex(arrYEAR, "NAME", argSHPYM.substr(0,4));
				if (y_index != -1)
				{
					view.lcmbSHPYM_YEAR.selectedItem = arrYEAR[y_index];
				}
				else
				{
					view.lcmbSHPYM_YEAR.selectedItem = arrYEAR[0];
				}
				
				var m_index:int = BasedMethod.getArrayIndex(arrMONTH, "NAME", argSHPYM.substr(5,2));
				if (m_index != -1)
				{
					view.lcmbSHPYM_MONTH.selectedItem = arrMONTH[m_index];
				}
				else
				{
					view.lcmbSHPYM_MONTH.selectedItem = arrMONTH[0];
				}
			}
			
			view.lcmbEHPYM_YEAR.dataProvider = arrYEAR;
			view.lcmbEHPYM_MONTH.dataProvider = arrMONTH;
			
			if (BasedMethod.nvl(argEHPYM) == "")
			{
				view.lcmbEHPYM_YEAR.selectedItem = arrYEAR[0];
				view.lcmbEHPYM_MONTH.selectedItem = arrMONTH[0];
			}
			else
			{
				var y_index:int = BasedMethod.getArrayIndex(arrYEAR, "NAME", argEHPYM.substr(0,4));
				if (y_index != -1)
				{
					view.lcmbEHPYM_YEAR.selectedItem = arrYEAR[y_index];
				}
				else
				{
					view.lcmbEHPYM_YEAR.selectedItem = arrYEAR[0];
				}
				
				var m_index:int = BasedMethod.getArrayIndex(arrMONTH, "NAME", argEHPYM.substr(5,2));
				if (m_index != -1)
				{
					view.lcmbEHPYM_MONTH.selectedItem = arrMONTH[m_index];
				}
				else
				{
					view.lcmbEHPYM_MONTH.selectedItem = arrMONTH[0];
				}
			}
			
			view.lcmbSLHDAT_YEAR.dataProvider = arrYEAR;
			view.lcmbSLHDAT_MONTH.dataProvider = arrMONTH;
			
			if (BasedMethod.nvl(argSLHDAT) == "")
			{
				view.lcmbSLHDAT_YEAR.selectedItem = arrYEAR[0];
				view.lcmbSLHDAT_MONTH.selectedItem = arrMONTH[0];
			}
			else
			{
				var y_index:int = BasedMethod.getArrayIndex(arrYEAR, "NAME", argSLHDAT.substr(0,4));
				if (y_index != -1)
				{
					view.lcmbSLHDAT_YEAR.selectedItem = arrYEAR[y_index];
				}
				else
				{
					view.lcmbSLHDAT_YEAR.selectedItem = arrYEAR[0];
				}
				
				var m_index:int = BasedMethod.getArrayIndex(arrMONTH, "NAME", argSLHDAT.substr(5,2));
				if (m_index != -1)
				{
					view.lcmbSLHDAT_MONTH.selectedItem = arrMONTH[m_index];
				}
				else
				{
					view.lcmbSLHDAT_MONTH.selectedItem = arrMONTH[0];
				}
			}
			
			
			view.lcmbELHDAT_YEAR.dataProvider = arrYEAR;
			view.lcmbELHDAT_MONTH.dataProvider = arrMONTH;
			
			if (BasedMethod.nvl(argELHDAT) == "")
			{
				view.lcmbELHDAT_YEAR.selectedItem = arrYEAR[0];
				view.lcmbELHDAT_MONTH.selectedItem = arrMONTH[0];
			}
			else
			{
				var y_index:int = BasedMethod.getArrayIndex(arrYEAR, "NAME", argELHDAT.substr(0,4));
				if (y_index != -1)
				{
					view.lcmbELHDAT_YEAR.selectedItem = arrYEAR[y_index];
				}
				else
				{
					view.lcmbELHDAT_YEAR.selectedItem = arrYEAR[0];
				}
				
				var m_index:int = BasedMethod.getArrayIndex(arrMONTH, "NAME", argELHDAT.substr(5,2));
				if (m_index != -1)
				{
					view.lcmbELHDAT_MONTH.selectedItem = arrMONTH[m_index];
				}
				else
				{
					view.lcmbELHDAT_MONTH.selectedItem = arrMONTH[0];
				}
			}
			
			view.chkKURI.selected = argKURI;
			
			view.lcmbTKCLS.dataProvider = arrTKCLS;
			
//			if (BasedMethod.nvl(argTKCLS) == "")
//			{
//				view.lcmbTKCLS.selectedItem = arrTKCLS[0];
//			}
//			else
//			{
//				var w_index:int = BasedMethod.getArrayIndex(arrTKCLS, "CODE", argTKCLS);
//				if (w_index != -1)
//				{
//					view.lcmbTKCLS.selectedItem = arrTKCLS[w_index];
//				}
//			}

			// 変数の設定
			//g_prgid     = "MENUX";
			super.init();
		}
		
		override public function getComboData():void
		{
			super.getComboData();
		}
		
		override public function openComboData(): void
		{
			if (BasedMethod.nvl(argAREA) != "")
			{
				cdsDTAREA();
			}
			else
			{
				view.lcmbDTAREA.enabled = false;
			}
			cdsIMPCLS();
			cdsHTCLS();
			cdsTKCLS();
			
			super.openComboData();
		}
		
		override public function formShow(): void
		{
			doDSPProc();
		}
		

		override public function setDispObject():void
		{
			super.setDispObject();
		}
		
		/**********************************************************
		 * 地区詳細コンボ用select
		 * @author 12.09.07 SE-354
		 **********************************************************/ 
		public function cdsDTAREA(): void
		{
			// 変数の初期化
			arrDTAREA = null;
			
			stmt = new SQLStatement();
			stmt.sqlConnection =  singleton.sqlconnection;
			stmt.itemClass = HANYOU;
			stmt.text= singleton.g_query_xml.entry.(@key == "qryDTAREA3");
			
			stmt.parameters[0] = BasedMethod.nvl(argAREA);
			//stmt.parameters[1] = p_param2;
			
			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, cdsDTAREAResult);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);
			
			//実行
			stmt.execute();
		}
		private function cdsDTAREAResult(event:SQLEvent):void 
		{
			//データの取得に成功したら
			//getResult()でSQLResultを取得し
			//取得したデータをデータグリッドに反映します。
			//status += "  データグリッド更新";
			var result:SQLResult = stmt.getResult();
			
			stmt.removeEventListener(SQLEvent.RESULT, cdsDTAREAResult);
			stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);
			stmt=null;
			
			arrDTAREA = new ArrayCollection();
			
			// 一行空行を生成
			arrDTAREA.addItem(new HANYOU());
			
			if (result.data != null)
			{
				// 一旦、ワーク配列にセット 
				//arrGDCLS = new ArrayCollection(result.data);
				for(var i:int=0; i < result.data.length; i++){
					arrDTAREA.addItem(result.data[i]);
				}
			}
			
			view.lcmbDTAREA.dataProvider = arrDTAREA;
			
			if (argDTAREA == "")
			{
				view.lcmbDTAREA.selectedItem = arrDTAREA[0];
			}
			else
			{
				var w_index: int = BasedMethod.getArrayIndex(arrDTAREA, "CODE", argDTAREA);
				if (w_index != -1)
				{
					view.lcmbDTAREA.selectedItem = arrDTAREA[w_index];
				}
				else
				{
					view.lcmbDTAREA.selectedItem = arrDTAREA[0];
				}
			}
			
			//execSetPLSQL();
		}
		
		/**********************************************************
		 * 重点区分コンボ用select
		 * @author 12.10.30 SE-354
		 **********************************************************/ 
		public function cdsIMPCLS(): void
		{
			// 変数の初期化
			arrIMPCLS = null;
			
			stmt = new SQLStatement();
			stmt.sqlConnection =  singleton.sqlconnection;
			stmt.itemClass = HANYOU;
			stmt.text= singleton.g_query_xml.entry.(@key == "qryIMPCLS2");
			
			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, cdsIMPCLSResult);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);
			
			//実行
			stmt.execute();
		}
		private function cdsIMPCLSResult(event:SQLEvent):void 
		{
			//データの取得に成功したら
			//getResult()でSQLResultを取得し
			//取得したデータをデータグリッドに反映します。
			//status += "  データグリッド更新";
			var result:SQLResult = stmt.getResult();
			
			stmt.removeEventListener(SQLEvent.RESULT, cdsIMPCLSResult);
			stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);
			stmt=null;
			
			arrIMPCLS = new ArrayCollection();
			
			// 一行空行を生成
			arrIMPCLS.addItem(new HANYOU());
			
			if (result.data != null)
			{
				// 一旦、ワーク配列にセット 
				//arrGDCLS = new ArrayCollection(result.data);
				for(var i:int=0; i < result.data.length; i++){
					arrIMPCLS.addItem(result.data[i]);
				}
			}
			
			view.lcmbIMPCLS.dataProvider = arrIMPCLS;
			
			if (argIMPCLS == "")
			{
				view.lcmbIMPCLS.selectedItem = arrIMPCLS[0];
			}
			else
			{
				var w_index: int = BasedMethod.getArrayIndex(arrIMPCLS, "CODE", argIMPCLS);
				view.lcmbIMPCLS.selectedItem = arrIMPCLS[w_index];
			}
			
			//execSetPLSQL();
		}

		/**********************************************************
		 * 訪問時間コンボ用select
		 * @author 12.11.15 SE-233
		 **********************************************************/ 
		public function cdsHTCLS(): void
		{
			// 変数の初期化
			arrHTCLS = null;
			
			stmt = new SQLStatement();
			stmt.sqlConnection =  singleton.sqlconnection;
			stmt.itemClass = HANYOU;
			stmt.text= singleton.g_query_xml.entry.(@key == "qryHTIMECD");
			
			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, cdsHTCLSResult);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);
			
			//実行
			stmt.execute();
		}
		private function cdsHTCLSResult(event:SQLEvent):void 
		{
			var result:SQLResult = stmt.getResult();
			
			stmt.removeEventListener(SQLEvent.RESULT, cdsHTCLSResult);
			stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);
			stmt=null;
			
			arrHTCLS = new ArrayCollection();
			
			// 一行空行を生成
			arrHTCLS.addItem(new HANYOU());
			
			if (result.data != null)
			{
				// 一旦、ワーク配列にセット 
				for(var i:int=0; i < result.data.length; i++){
					arrHTCLS.addItem(result.data[i]);
				}
			}

			view.lcmbHTCLS.dataProvider = arrHTCLS;
			
			if (argHTCLS == "")
			{
				view.lcmbHTCLS.selectedItem = arrHTCLS[0];
			}
			else
			{
				var w_index: int = BasedMethod.getArrayIndex(arrHTCLS, "CODE", argHTCLS);
				view.lcmbHTCLS.selectedItem = arrHTCLS[w_index];
			}
		}
		
		/**********************************************************
		 * 得意先区分コンボ用select
		 * @author 12.11.15 SE-233
		 **********************************************************/ 
		public function cdsTKCLS(): void
		{
			// 変数の初期化
			arrTKCLS = null;
			
			stmt = new SQLStatement();
			stmt.sqlConnection =  singleton.sqlconnection;
			stmt.itemClass = HANYOU;
			stmt.text= singleton.g_query_xml.entry.(@key == "qryTKCLS");
			
			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, cdsTKCLSResult);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);
			
			//実行
			stmt.execute();
		}
		private function cdsTKCLSResult(event:SQLEvent):void 
		{
			var result:SQLResult = stmt.getResult();
			
			stmt.removeEventListener(SQLEvent.RESULT, cdsTKCLSResult);
			stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);
			stmt=null;
			
			arrTKCLS = new ArrayCollection();
			
			// 一行空行を生成
			arrTKCLS.addItem(new HANYOU());
			
			if (result.data != null)
			{
				// 一旦、ワーク配列にセット 
				for(var i:int=0; i < result.data.length; i++){
					arrTKCLS.addItem(result.data[i]);
				}
			}
			
			view.lcmbTKCLS.dataProvider = arrTKCLS;
			
			if (argTKCLS == "")
			{
				view.lcmbTKCLS.selectedItem = arrTKCLS[0];
			}
			else
			{
				var w_index: int = BasedMethod.getArrayIndex(arrTKCLS, "CODE", argTKCLS);
				view.lcmbTKCLS.selectedItem = arrTKCLS[w_index];
			}
		}
		
		protected function stmtErrorHandler(event:SQLErrorEvent):void
		{
			// TODO Auto-generated method stub
			// エラーの表示
			trace("SQLerr");
		}
		
		/**********************************************************
		 * Backキー押下時処理.
		 * @author 12.08.22 SE-354
		 **********************************************************/
		protected function onKeyDownHandler(e:KeyboardEvent):void
		{
			e.preventDefault();
			
			btnCANCELClick();
		}

		/**********************************************************
		 * btnOK クリック時処理.
		 * @author 12.08.22 SE-354
		 **********************************************************/
		public function btnOKClick(): void
		{
			if (checkSHPYM() == false)
			{
				singleton.sitemethod.dspJoinErrMsg("開始訪問予定年月を正しく入力してください。",
					null, "", "0", null, null, true);
				return;
			}
			
			if (checkEHPYM() == false)
			{
				singleton.sitemethod.dspJoinErrMsg("終了訪問予定年月を正しく入力してください。",
					null, "", "0", null, null, true);
				return;
			}
			
			if (checkSLHDAT() == false)
			{
				singleton.sitemethod.dspJoinErrMsg("開始前回訪問日を正しく入力してください。",
					null, "", "0", null, null, true);
				return;
			}
			
			if (checkELHDAT() == false)
			{
				singleton.sitemethod.dspJoinErrMsg("終了前回訪問日を正しく入力してください。",
					null, "", "0", null, null, true);
				return;
			}
				
				
			okflg = true;
			//argHPYM = view.edtHPYM.text;
			//argDTAREA = view.lcmbDTAREA.selectedItem.CODE;
			//argIMPCLS = view.lcmbIMPCLS.selectedItem.CODE;
			//argHTCLS = view.lcmbHTCLS.selectedItem.CODE;
			//argEHPYM = view.lcmbEHPYM_YEAR.selectedItem.NAME + "/" + view.lcmbEHPYM_MONTH.selectedItem.NAME;
			
			argKURI = view.chkKURI.selected;
			PopUpManager.removePopUp(view);
		}
		
		/**********************************************************
		 * btnCANCEL クリック時処理.
		 * @author 12.08.22 SE-354
		 **********************************************************/
		public function btnCANCELClick(): void
		{
			okflg = false;

			PopUpManager.removePopUp(view);
		}

		private function checkSHPYM(): Boolean
		{
			if ((BasedMethod.nvl(view.lcmbSHPYM_YEAR.selectedItem.CODE) == "") && (BasedMethod.nvl(view.lcmbSHPYM_MONTH.selectedItem.CODE) == ""))
			{
				argSHPYM = "";
				
				return true;
			}
			else if ((BasedMethod.nvl(view.lcmbSHPYM_YEAR.selectedItem.CODE) != "") && (BasedMethod.nvl(view.lcmbSHPYM_MONTH.selectedItem.CODE) != ""))
			{
				argSHPYM = view.lcmbSHPYM_YEAR.selectedItem.NAME + "/" + view.lcmbSHPYM_MONTH.selectedItem.NAME;
				
				return true;
			}
			else
			{
				return false;
			}
		}
		
		private function checkEHPYM(): Boolean
		{
			if ((BasedMethod.nvl(view.lcmbEHPYM_YEAR.selectedItem.CODE) == "") && (BasedMethod.nvl(view.lcmbEHPYM_MONTH.selectedItem.CODE) == ""))
			{
				argEHPYM = "";
				
				return true;
			}
			else if ((BasedMethod.nvl(view.lcmbEHPYM_YEAR.selectedItem.CODE) != "") && (BasedMethod.nvl(view.lcmbEHPYM_MONTH.selectedItem.CODE) != ""))
			{
				argEHPYM = view.lcmbEHPYM_YEAR.selectedItem.NAME + "/" + view.lcmbEHPYM_MONTH.selectedItem.NAME;
				
				return true;
			}
			else
			{
				return false;
			}
		}
		
		private function checkSLHDAT(): Boolean
		{
			if ((BasedMethod.nvl(view.lcmbSLHDAT_YEAR.selectedItem.CODE) == "") && (BasedMethod.nvl(view.lcmbSLHDAT_MONTH.selectedItem.CODE) == ""))
			{
				argSLHDAT = "";
				
				return true;
			}
			else if ((BasedMethod.nvl(view.lcmbSLHDAT_YEAR.selectedItem.CODE) != "") && (BasedMethod.nvl(view.lcmbSLHDAT_MONTH.selectedItem.CODE) != ""))
			{
				argSLHDAT = view.lcmbSLHDAT_YEAR.selectedItem.NAME + "/" + view.lcmbSLHDAT_MONTH.selectedItem.NAME;
				
				return true;
			}
			else
			{
				return false;
			}
		}
		
		private function checkELHDAT(): Boolean
		{
			if ((BasedMethod.nvl(view.lcmbELHDAT_YEAR.selectedItem.CODE) == "") && (BasedMethod.nvl(view.lcmbELHDAT_MONTH.selectedItem.CODE) == ""))
			{
				argELHDAT = "";
				
				return true;
			}
			else if ((BasedMethod.nvl(view.lcmbELHDAT_YEAR.selectedItem.CODE) != "") && (BasedMethod.nvl(view.lcmbELHDAT_MONTH.selectedItem.CODE) != ""))
			{
				argELHDAT = view.lcmbELHDAT_YEAR.selectedItem.NAME + "/" + view.lcmbELHDAT_MONTH.selectedItem.NAME;
				
				return true;
			}
			else
			{
				return false;
			}
		}
		
		public function lcmbDTAREAExit(): void
		{
			argDTAREA = view.lcmbDTAREA.selectedItem.CODE;
		}
		
		public function lcmbIMPCLSExit(): void
		{
			argIMPCLS = view.lcmbIMPCLS.selectedItem.CODE;
		}
		
		public function lcmbHTCLSExit(): void
		{
			argHTCLS = view.lcmbHTCLS.selectedItem.CODE;
		}
		
		public function lcmbTKCLSExit(): void
		{
			argTKCLS = view.lcmbTKCLS.selectedItem.CODE;
		}
		
		public function edtESTAMTExit(): void
		{
			view.edtESTAMT.text = BasedMethod.NumChk(view.edtESTAMT.text);
			argESTAMT = view.edtESTAMT.text;
		}
		
		public function lcmbSHPYM_YEARExit(): void
		{
			if(view.lcmbSHPYM_YEAR.selectedItem.CODE==""){
				view.lcmbSHPYM_MONTH.selectedItem = arrMONTH[0];
			}
		}
		
		public function lcmbEHPYM_YEARExit(): void
		{
			if(view.lcmbEHPYM_YEAR.selectedItem.CODE==""){
				view.lcmbEHPYM_MONTH.selectedItem = arrMONTH[0];
			}
		}
		
		public function lcmbSLHDAT_YEARExit(): void
		{
			if(view.lcmbSLHDAT_YEAR.selectedItem.CODE==""){
				view.lcmbSLHDAT_MONTH.selectedItem = arrMONTH[0];
			}
		}
		
		public function lcmbELHDAT_YEARExit(): void
		{
			if(view.lcmbELHDAT_YEAR.selectedItem.CODE==""){
				view.lcmbELHDAT_MONTH.selectedItem = arrMONTH[0];
			}
		}
		
	}
}