package views
{
	import flash.data.SQLResult;
	import flash.data.SQLStatement;
	import flash.events.Event;
	import flash.events.GeolocationEvent;
	import flash.events.KeyboardEvent;
	import flash.events.LocationChangeEvent;
	import flash.events.MouseEvent;
	import flash.events.SQLErrorEvent;
	import flash.events.SQLEvent;
	import flash.geom.Rectangle;
	import flash.media.StageWebView;
	import flash.net.SharedObject;
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	import flash.sensors.Geolocation;
	import flash.ui.Keyboard;
	
	import modules.base.BasedMethod;
	import modules.custom.CustomEvent;
	import modules.custom.CustomIterator;
	import modules.custom.sub.Iterator;
	import modules.dto.HANYOU;
	import modules.dto.LM009;
	import modules.dto.LM010;
	import modules.dto.LM016;
	import modules.sbase.SiteModule;
	
	import mx.collections.ArrayCollection;
	import mx.core.UIComponent;
	import mx.events.CollectionEvent;
	import mx.events.DragEvent;
	import mx.events.FlexEvent;
	import mx.managers.DragManager;
	import mx.managers.PopUpManager;
	import mx.utils.ObjectUtil;
	
	public class MA020XController extends SiteModule
	{
		private var view:MA020X;
		//public var popup001: POPUP001;
		private var arrPOPUP005: ArrayCollection;
		
		private var arrMA020X: ArrayCollection;
		public var arrMA020X2: ArrayCollection = new ArrayCollection();
		private var arrAREA: ArrayCollection;
		private var arrDTAREA: ArrayCollection;
		private var stmt: SQLStatement;
		
		private var popup005: POPUP005;
		private var popup007: POPUP007;
		private var popup008: POPUP008;
		
		public var argESTAMT: String;				// 予測売上
		
		public var argSHPYM: String;				// 開始訪問予定年月
		public var argEHPYM: String;				// 終了訪問予定年月
		public var argSLHDAT: String;				// 開始前回訪問日
		public var argELHDAT: String;				// 終了前回訪問日
		
		public var argDTAREA: String = "";				// 訪問予定年月
		public var argIMPCLS: String;				// 重点区分
		public var argHTCLS: String;				// 訪問時間区分
		public var argTKCLS: String;				// 得意先区分
		public var argKURI: Boolean;				// 売掛残有か
		
		private var webView:StageWebView;
		
		private var geo: Geolocation;
		
		public var _so: SharedObject;	// 入力データ保持用SharedObject
		
		private var w_latitude: Number;
		private var w_longitude: Number;
		
		private var w_ken: Number = 0;
		private var w_ken2: Number = 0;
		
		private var current_date: Date = new Date();
		
		private static const MAP_URL:String = "file:///android_asset/views/omap.html";
		
		public function MA020XController()
		{
			super();
		}
		
		override public function init():void
		{
			//super.init();
			
			// フォーム宣言
			view = _view as MA020X;
			
			// 変数の設定
			//g_prgid     = "MENUX";
			
			if (Geolocation.isSupported)
			{
				geo = new Geolocation();
				if (!geo.muted)
				{
					//geo.setRequestedUpdateInterval(1000);
					geo.addEventListener(GeolocationEvent.UPDATE, onGeoUpdate, false, 0, true);
				}
			}
			
			webView = new StageWebView();
			
			view.stage.addEventListener(KeyboardEvent.KEY_DOWN, onKeyDownHandler, false, 0, true);
			
			// 訪問計画ORDER BYをSharedObjectよりロード
			loadSO();
			
			super.init();
		}
		
		override public function formShow_last(): void
		{
			cdsSEIGYO(formShow_last_cld1);
		}
		
		
		private function formShow_last_cld1(): void
		{
			//cdsMA020X();
			//cdsAREA();
			
			var w_mm: String;
			
			if (arrSEIGYO.length != 0)
			{
				view.edtHDAT.text = arrSEIGYO[0].SDDAY;
				
				argSHPYM = "";
				if (current_date.date < 26)
				{
					
					// 25日以前は当月
					w_mm = "00" + (current_date.month + 1).toString();
					argEHPYM = (current_date.fullYear).toString() + "/" + w_mm.substr(w_mm.length - 2, 2);
					//					argEHPYM = (current_date.fullYear).toString() + "/" + (current_date.month + 1).toString();
				}
				else
				{
					//					var nextmonth:Date = new Date(current_date.fullYear, current_date.month+1, current_date.date);
					var nextmonth:Date = new Date(current_date.fullYear, current_date.month+1, 1);
					
					
					// 26日以降は翌月
					w_mm = "00" + (nextmonth.month + 1).toString();
					argEHPYM = (nextmonth.fullYear).toString() + "/" + w_mm.substr(w_mm.length - 2, 2);
					//					argEHPYM = (nextmonth.fullYear).toString() + "/" + (nextmonth.month + 1).toString();
				}
				
				argSLHDAT = "";
				argELHDAT = "";
				argIMPCLS = "";
				argHTCLS = "";
				argTKCLS = "";
				argESTAMT = "0";
				argKURI = false;
				cdsCHGCD2(arrSEIGYO[0].CHGCD, formShow_last_cld2);
				
				return;
			}
			
			super.formShow_last();
		}
		private function formShow_last_cld2(): void
		{
			view.title = "訪問計画作成" + ": " + arrCHGCD2[0].CHGNM;
			cdsMA020X();
			cdsMA020X2();
			insLF013("","訪問計画開始");
			super.formShow_last();
		}
		
		override public function openComboData(): void
		{
			cdsAREA();
			cdsDTAREA();
			
			super.openComboData();
		}
		
		public function btnNOWClick(): void
		{
			webView.loadURL("javascript:centerLnglat(" + w_longitude + "," + w_latitude + ")");
		}
		
		public function btnDSPClick(): void
		{
			/*
			// 地区は必須
			if (view.lcmbAREA.selectedItem == null)
			{
			return;
			}
			*/
			
			cdsMA020X();
		}
		
		private function onGeoUpdate(event:GeolocationEvent):void
		{
			// 緯度をセット
			w_latitude = event.latitude;
			
			// 経度をセット
			w_longitude = event.longitude;
		}
		
		/**********************************************************
		 * 地区コンボ用select
		 * @author 12.09.07 SE-354
		 **********************************************************/ 
		public function cdsAREA(): void
		{
			// 変数の初期化
			arrAREA = null;
			
			stmt = new SQLStatement();
			//stmt.sqlConnection =  connection;
			stmt.sqlConnection =  singleton.sqlconnection;
			//stmt.itemClass = LM009;
			stmt.itemClass = HANYOU;
			//stmt.text= "SELECT * FROM とくいさき WHERE HDAT=? AND AREA=?";
			//stmt.text= "SELECT * FROM LM010 WHERE TKAREA='01' AND HPYM='2012/06' AND TKCD LIKE ";
			//stmt.text= "SELECT * FROM LM010 WHERE ROWID <100";
			stmt.text= singleton.g_query_xml.entry.(@key == "qryAREA");
			
			//stmt.parameters[0] = p_param1;
			//stmt.parameters[1] = p_param2;
			
			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, cdsAREAResult, false, 0, true);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler, false, 0, true);
			
			//実行
			stmt.execute();
		}
		
		private function cdsAREAResult(event:SQLEvent):void 
		{
			//データの取得に成功したら
			//getResult()でSQLResultを取得し
			//取得したデータをデータグリッドに反映します。
			//status += "  データグリッド更新";
			var result:SQLResult = stmt.getResult();
			
			arrAREA = new ArrayCollection();
			
			// 一行空行を生成
			arrAREA.addItem(new HANYOU());
			
			if (result.data == null)
			{
				singleton.sitemethod.dspJoinErrMsg("地区マスタデータが存在しません。先にデータダウンロードを行ってください。",
					null, "", "0", null, null, true);
				return;
			}
			
			// 一旦、ワーク配列にセット 
			//arrGDCLS = new ArrayCollection(result.data);
			for(var i:int=0; i < result.data.length; i++){
				arrAREA.addItem(result.data[i]);
			}
			
			view.lcmbAREA.dataProvider = arrAREA;
			view.lcmbAREA.selectedItem = arrAREA[0];
			
			stmt.removeEventListener(SQLEvent.RESULT, cdsAREAResult);				
			stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);				
			stmt=null;
			//view.grp1.dataProvider = arrMA020X;
			
			//execSetPLSQL();
		}
		
		/**********************************************************
		 * 学校区select
		 * @author 12.11.08 SE-354
		 **********************************************************/ 
		public function cdsDTAREA(): void
		{
			// 変数の初期化
			arrDTAREA = null;
			
			stmt = new SQLStatement();
			//stmt.sqlConnection =  connection;
			stmt.sqlConnection =  singleton.sqlconnection;
			stmt.itemClass = LM016;
			stmt.text= singleton.g_query_xml.entry.(@key == "qryDTAREA2");
			
			//stmt.parameters[0] = p_param1;
			//stmt.parameters[1] = p_param2;
			
			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, cdsDTAREAResult, false, 0, true);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler, false, 0, true);
			
			//実行
			stmt.execute();
		}
		
		private function cdsDTAREAResult(event:SQLEvent):void 
		{
			//データの取得に成功したら
			//getResult()でSQLResultを取得し
			//取得したデータをデータグリッドに反映します。
			//status += "  データグリッド更新";
			var result:SQLResult = stmt.getResult();
			
			arrDTAREA = new ArrayCollection(result.data);
			
			stmt.removeEventListener(SQLEvent.RESULT, cdsDTAREAResult);				
			stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);				
			stmt=null;
		}
		
		public function getDTAREA(item:Object): String
		{
			//var w_index: int = BasedMethod.getArrayIndex(arrDTAREA, "AREADTAREA", (item.TKAREA + "-" + item.DTAREA));
			/*
			if (w_index != -1)
			{
			return item.TKADD + "(" + arrDTAREA[w_index].DTAREANM + ")"; 
			}
			else
			{
			return item.TKADD;
			}
			*/
			
			//			return "予測売上:" + singleton.g_customnumberformatter.numberFormat(item.ESTAMT, "1", "0", "0") + "円, " + "前回訪問日時:" + item.LHDAT + " "+ item.LHTIME;
			return "予測売上:" + singleton.g_customnumberformatter.numberFormat(BasedMethod.nvl(item.ESTAMT,"0"), "1", "0", "0") + "円, " + "前回訪問日時:" + BasedMethod.nvl(item.LHDAT) + " "+ BasedMethod.nvl(item.LHTIME);
			//argDTAREA = "";
			
			//trace("area change");
		}
		
		// リスト三段目の項目
		public function getThirdLine(item:Object): String
		{
			var w_index: int = BasedMethod.getArrayIndex(arrDTAREA, "AREADTAREA", (item.TKAREA + "-" + item.DTAREA));
			
			if (w_index != -1)
			{
				return item.TKADD + "(" + arrDTAREA[w_index].DTAREANM + ")"; 
			}
			else
			{
				return item.TKADD;
			}
			//argDTAREA = "";
			
			//trace("area change");
		}
		
		public function lcmbAREAExit(): void
		{
			argDTAREA = "";
			
			cdsMA020X();
			
			//trace("area change");
		}
		
		/**********************************************************
		 * SQLiteのselect(候補リスト)
		 * @author 12.05.10 SE-354
		 **********************************************************/ 
		public function cdsMA020X(nextfunc: Function=null): void
		{
			// 変数の初期化
			arrMA020X = null;
			
			// 2014.06.16 SE-233 start			
			if (view.edtHDAT.text == ""){
				singleton.sitemethod.dspJoinErrMsg("訪問日を入力してください。",
					null, "", "0", null, null, true);
				view.grp1.dataProvider = arrMA020X;
				return;
			}
			if (BasedMethod.DateChk(view.edtHDAT.text) == false){
				singleton.sitemethod.dspJoinErrMsg("正しい訪問日を入力して下さい。",
					null, "", "0", null, null, true);
				view.grp1.dataProvider = arrMA020X;
				return;
			}
			// 2014.06.16 SE-233 end
			
			var w_sql: String;
			
			stmt = new SQLStatement();
			//stmt.sqlConnection =  connection;
			stmt.sqlConnection =  singleton.sqlconnection;
			stmt.itemClass = LM010;
			//stmt.text= "SELECT * FROM とくいさき WHERE HDAT=? AND AREA=?";
			//stmt.text= "SELECT * FROM LM010 WHERE TKAREA='01' AND HPYM='2012/06' AND TKCD LIKE ";
			//stmt.text= "SELECT * FROM LM010 WHERE ROWID <100";
			w_sql = singleton.g_query_xml.entry.(@key == "qryMA020X");
			
			if (BasedMethod.nvl(view.lcmbAREA.selectedItem.CODE) != "")
			{
				// AREA
				//w_sql = w_sql + " AND LM010.TKAREA = '" + view.lcmbAREA.selectedItem.CODE + "'";
				w_sql = w_sql + " AND COALESCE(LM010.TKAREA,'00') = '" + view.lcmbAREA.selectedItem.CODE + "'";
				
				if (BasedMethod.nvl(argDTAREA) != "")
				{
					w_sql = w_sql + " AND LM010.DTAREA = '" + argDTAREA + "'";
				}
			}
			
			if (BasedMethod.nvl(argHTCLS) != "")
			{
				// 訪問時間区分
				w_sql = w_sql + " AND LM010.HTCLS = '" + argHTCLS + "'";
			}
			
			if (BasedMethod.nvl(argIMPCLS) != "")
			{
				// 重点区分
				w_sql = w_sql + " AND LM010.IMPCLS = '" + argIMPCLS + "'";
			}
			
			if (BasedMethod.nvl(argTKCLS) != "")
			{
				// 得意先区分
				w_sql = w_sql + " AND LM010.TKCLS = '" + argTKCLS + "'";
			}			
			
			// 次回訪問予定
			if ((BasedMethod.nvl(argSHPYM) == "") && (BasedMethod.nvl(argEHPYM) == ""))
			{
				//w_sql = w_sql + " AND LM010.HPYM IS NULL";
			}
			else if ((BasedMethod.nvl(argSHPYM) != "") && (BasedMethod.nvl(argEHPYM) == ""))
			{
				//w_sql = w_sql + " AND (LM010.HPYM IS NULL OR (LM010.HPYM >='" + argSHPYM + "'))";
				w_sql = w_sql + " AND COALESCE(LM010.HPYM,'9999/99') >='" + argSHPYM + "'";
			}
			else if ((BasedMethod.nvl(argSHPYM) == "") && (BasedMethod.nvl(argEHPYM) != ""))
			{
				//w_sql = w_sql + " AND (LM010.HPYM IS NULL OR (LM010.HPYM <='" + argEHPYM + "'))";
				w_sql = w_sql + " AND COALESCE(LM010.HPYM,'0000/00') <='" + argEHPYM + "'";
			}
			else
			{
				//w_sql = w_sql + " AND (LM010.HPYM IS NULL OR ((LM010.HPYM >='" + argSHPYM + "') AND (LM010.HPYM <='" + argEHPYM + "')))";
				w_sql = w_sql + " AND COALESCE(LM010.HPYM,'9999/99') >='" + argSHPYM + "' AND COALESCE(LM010.HPYM,'0000/00') <='" + argEHPYM + "'";
			}
			
			// 前回訪問日
			if (BasedMethod.nvl(argSLHDAT) != "")
			{
				var argSLHDAT_DATE: String = argSLHDAT + "/01";
				
				w_sql = w_sql + " AND LM010.LHDAT >='" + argSLHDAT_DATE + "'";
			}
			
			if (BasedMethod.nvl(argELHDAT) != "")
			{
				var argELHDAT_DATE: String = argELHDAT + "/31";
				
				w_sql = w_sql + " AND LM010.LHDAT <='" + argELHDAT_DATE + "'";
			}
			
			w_sql = w_sql + " AND LM010.ESTAMT >=" + argESTAMT + "";
			
			if (argKURI == true)
			{
				// 売掛残有時
				//w_sql = w_sql + " AND (LM010.HMEDIKURI + LM010.HSTOREKURI + LM010.HDEVKURI + LM010.GMEDIKURI + LM010.GSTOREKURI + LM010.GDEVKURI) > 0";
				w_sql = w_sql + " AND ((HMEDIKURI+HMEDIAMT+HMEDITAX-HMEDINY-HMEDIDIS-HMEDIKAS)+";
				w_sql = w_sql + "(HSTOREKURI+HSTOREAMT+HSTORETAX-HSTORENY-HSTOREDIS-HSTOREKAS)+";
				w_sql = w_sql + "(HDEVKURI+HDEVAMT+HDEVTAX-HDEVNY-HDEVDIS-HDEVKAS)+";
				w_sql = w_sql + "(GMEDIKURI+GMEDIAMT+GMEDITAX-GMEDINY-GMEDIDIS-GMEDIKAS)+";
				w_sql = w_sql + "(GSTOREKURI+GSTOREAMT+GSTORETAX-GSTORENY-GSTOREDIS-GSTOREKAS)+";
				w_sql = w_sql + "(GDEVKURI+GDEVAMT+GDEVTAX-GDEVNY-GDEVDIS-GDEVKAS)) > 0";
			}
			
			//w_sql = w_sql + " ORDER BY " + arrPOPUP005[0].DBNAME + "," + arrPOPUP005[1].DBNAME + "," + arrPOPUP005[2].DBNAME + "," + "TKCD";
			w_sql = w_sql + " ORDER BY " + arrPOPUP005[0].DBNAME;
			if(arrPOPUP005[0].DBNAME=="ESTAMT"){
				w_sql = w_sql + " DESC,";
			}else{
				w_sql = w_sql + ",";
			}
			w_sql = w_sql + arrPOPUP005[1].DBNAME;
			if(arrPOPUP005[1].DBNAME=="ESTAMT"){
				w_sql = w_sql + " DESC,";
			}else{
				w_sql = w_sql + ",";
			}
			w_sql = w_sql + arrPOPUP005[2].DBNAME;
			if(arrPOPUP005[2].DBNAME=="ESTAMT"){
				w_sql = w_sql + " DESC,";
			}else{
				w_sql = w_sql + ",";
			}
			w_sql = w_sql + "TKCD";
			
			//w_sql = w_sql + " AND (LM010.HPYM IS NULL OR LM010.HPYM<'" + argHPYM + "')";
			
			stmt.text = w_sql;
			
			// CHGCD
			//stmt.parameters[0] = arrSEIGYO[0].CHGCD;
			// HDAT
			stmt.parameters[0] = view.edtHDAT.text;
			
			// HPYM
			//stmt.parameters[1] = argHPYM;
			// ORDER
			//stmt.parameters[5] = arrPOPUP005[0].DBNAME;
			//stmt.parameters[6] = arrPOPUP005[1].DBNAME;
			//stmt.parameters[7] = arrPOPUP005[2].DBNAME;
			
			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, cdsMA020XResult, false, 0, true);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler, false, 0, true);
			
			//実行
			stmt.execute();
		}
		
		protected function stmtErrorHandler(event:SQLErrorEvent):void
		{
			// TODO Auto-generated method stub
			// エラーの表示
			trace("SQLerr");
		}
		
		/**
		 * SELECT処理結果
		 */
		private function cdsMA020XResult(event:SQLEvent):void {
			//データの取得に成功したら
			//getResult()でSQLResultを取得し
			//取得したデータをデータグリッドに反映します。
			//status += "  データグリッド更新";
			var result:SQLResult = stmt.getResult();
			
			//var w_arr: ArrayCollection;
			//var i: int;
			/*
			if (result.data == null)
			{
			trace("nodata");
			}
			*/
			
			// 一旦、ワーク配列にセット 
			//w_arr = new ArrayCollection(result.data);
			arrMA020X = new ArrayCollection(result.data);
			
			// LM010型に変換してセット
			/*
			arrMA020X = new ArrayCollection();
			for (i=0; i<w_arr.length; i++) {
			//arrMA020X.addItem(LM010(w_arr[i]));
			arrMA020X.addItem(w_arr[i]);
			}
			*/
			
			view.grp1.dataProvider = arrMA020X;
			
			view.edtHKEN2.text = arrMA020X.length.toString() + "件";
			
			stmt.removeEventListener(SQLEvent.RESULT, cdsMA020XResult);				
			stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);				
			stmt=null;
			
			w_ken2 = 0;
			changeData2();
			cdsMA020X2();
		}
		
		public function ClickList(event: CustomEvent): void
			//public function ClickList(value: String): void
		{
			
			popup008 = new POPUP008();
			
			popup008.ctrl.argDATA = event.target.data;
			popup008.ctrl.argTKCD = event.target.data.TKCD;
			popup008.ctrl.argTKNM = event.target.data.TKNM;
			popup008.ctrl.argTKTEL = event.target.data.TKTEL;
			popup008.ctrl.argTKMTEL = event.target.data.TKMTEL;
			popup008.ctrl.argLHDAT = BasedMethod.nvl(event.target.data.LHDAT) + " " + BasedMethod.nvl(event.target.data.LHTIME);
			popup008.ctrl.argHSPAN = event.target.data.HSPAN;
			popup008.ctrl.argHAMT = (BasedMethod.asCurrency(event.target.data.HMEDIZAMT) + BasedMethod.asCurrency(event.target.data.HSTOREZAMT) + BasedMethod.asCurrency(event.target.data.HDEVZAMT)).toString();
			popup008.ctrl.argGAMT = (BasedMethod.asCurrency(event.target.data.GMEDIZAMT) + BasedMethod.asCurrency(event.target.data.GSTOREZAMT) + BasedMethod.asCurrency(event.target.data.GDEVZAMT)).toString();
			popup008.ctrl.argESTAMT = BasedMethod.asCurrency(event.target.data.ESTAMT).toString();
			popup008.ctrl.argIMPCLS = event.target.data.IMPCLS;
			//popup008.ctrl.argHTCLS = event.target.data.HTCLS;
			popup008.ctrl.argMEMO = event.target.data.MEMO;
			
			//view.grp1.callLater(function():void {PopUpManager.addPopUp(popup008, view, true)});
			//view.grp1.callLater(function():void {PopUpManager.centerPopUp(popup008)});
			
			PopUpManager.addPopUp(popup008, view, true);
			PopUpManager.centerPopUp(popup008);
			//trace(event.target.data.TKNM);
			//trace(value);
		}
		
		public function dragOver(event: DragEvent): void
		{
			/*
			DragManager.acceptDragDrop(view.grp2);
			
			trace("over");
			
			DragManager.showFeedback(DragManager.MOVE);
			*/
		}
		
		public function dragComplete(event: DragEvent): void
		{
			/*
			trace("abefore:" + arrMA020X2.length);
			arrMA020X2.refresh();
			trace("aafter:" + arrMA020X2.length);
			*/
			//trace(view.getChildIndex(view.grp2));
			
			//確定
			view.callLater(checkDataSet);
		}
		
		public function dropComplete(event: DragEvent): void
		{
			//if (dragflg == true)
			//{
			var dragstartid: String = UIComponent(event.dragInitiator).id;
			
			// 右リスト内での並び替え実行時に通る
			if (dragstartid == "grp2")
			{
				// このあとDragCompleteEventも走るので重複実行になるため何もしない
			}
			else
			{
				/*
				trace("obefore:" + arrMA020X2.length);
				
				arrMA020X2.refresh();
				trace("oafter:" + arrMA020X2.length);
				*/
				//確定
				view.callLater(checkDataSet);
			}
		}
		
		/**********************************************************
		 * SQLiteのselect(訪問予定先)
		 * @author 12.09.11 SE-354
		 **********************************************************/ 
		public function cdsMA020X2(nextfunc: Function=null): void
		{
			// 変数の初期化
			//arrMA020X2 = null;
			
			stmt = new SQLStatement();
			//stmt.sqlConnection =  connection;
			stmt.sqlConnection =  singleton.sqlconnection;
			stmt.itemClass = LM010;
			//stmt.text= "SELECT * FROM とくいさき WHERE HDAT=? AND AREA=?";
			//stmt.text= "SELECT * FROM LM010 WHERE TKAREA='01' AND HPYM='2012/06' AND TKCD LIKE ";
			//stmt.text= "SELECT * FROM LM010 WHERE ROWID <100";
			stmt.text= singleton.g_query_xml.entry.(@key == "qryMA020X2");
			
			// CHGCD
			//stmt.parameters[0] = arrSEIGYO[0].CHGCD;
			// HDAT
			stmt.parameters[0] = view.edtHDAT.text;
			
			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, cdsMA020X2Result, false, 0, true);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler, false, 0, true);
			
			//実行
			stmt.execute();
		}
		
		private function cdsMA020X2Result(event:SQLEvent):void 
		{
			//データの取得に成功したら
			//getResult()でSQLResultを取得し
			//取得したデータをデータグリッドに反映します。
			//status += "  データグリッド更新";
			var result:SQLResult = stmt.getResult();
			
			//var w_arr: ArrayCollection;
			//var i: int;
			
			if (result.data == null)
			{
				//trace("nodata");
				view.edtHKEN.text = "0件";
			}
			
			// 一旦、ワーク配列にセット 
			//w_arr = new ArrayCollection(result.data);
			arrMA020X2 = new ArrayCollection(result.data);
			
			view.grp2.dataProvider = arrMA020X2;
			
			view.edtHKEN.text = arrMA020X2.length.toString() + "件";
			
			stmt.removeEventListener(SQLEvent.RESULT, cdsMA020X2Result);				
			stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);				
			stmt=null;
			
			w_ken = 0;
			changeData();
			//sumESTAMT();
		}
		
		
		public function insMA020X(nextfunc: Function=null): void
		{
			/*
			stmt = new SQLStatement();
			stmt.sqlConnection =  connection;
			
			var ite: Iterator = new CustomIterator(arrUA010X);
			var w_AUM01: AUM01 = new AUM01();
			
			while (ite.hasNext())
			{
			w_AUM01 = AUM01(ite.next());
			
			// SQL文の取得
			stmt.text= "INSERT INTO AUM01 (ISEQ, WSID, NAME) VALUES (?, ?, ?)";
			
			stmt.parameters[0] = w_AUM01.ISEQ;
			stmt.parameters[1] = w_AUM01.WSID;
			stmt.parameters[2] = w_AUM01.NAME;
			
			//実行
			stmt.execute();
			}
			*/
			// 次の処理？
			
		}
		
		
		/**********************************************************
		 * btnMAP クリック時処理.
		 * @author 12.08.13 SE-354
		 **********************************************************/
		public function btnMAPClick(): void
		{
			webView.stage = view.stage;
			
			webView.viewPort = new Rectangle(0, 55, 1280, 740);
			webView.loadURL(MAP_URL);
			
			webView.addEventListener(Event.COMPLETE,onMapComplete, false, 0, true);
		}
		
		protected function onMapComplete(event:Event):void 
		{
			webView.removeEventListener(Event.COMPLETE,onMapComplete);
			
			webView.addEventListener(LocationChangeEvent.LOCATION_CHANGE,update, false, 0, true); 
			
			//webView.loadURL("javascript:centerLnglat(" + w_longitude + "," + w_latitude + ")");
			
			if (arrMA020X2 != null && arrMA020X2.length != 0)
			{
				
				// 2012.12.10 SE-233 start				
				//if (arrMA020X2[0].LAT != null && arrMA020X2[0].LAT != "0")
				//{
				//	webView.loadURL("javascript:centerLnglat(" + arrMA020X2[0].LNG + "," + arrMA020X2[0].LAT + ")");
				//}
				//else
				//{
				//	webView.loadURL("javascript:centerLnglat(" + w_longitude + "," + w_latitude + ")");
				//}
				
				var w_latitude1: Number = w_latitude;
				var w_longitude1: Number = w_longitude;
				
				for (var ii:int=0; ii<arrMA020X2.length; ii++){
					if (arrMA020X2[ii].LAT != null && arrMA020X2[ii].LAT != "0"){
						w_latitude1 = arrMA020X2[ii].LAT;
						w_longitude1 = arrMA020X2[ii].LNG;
						break;
					}
				}
				webView.loadURL("javascript:centerLnglat(" + w_longitude1 + "," + w_latitude1 + ")");
				// 2012.12.10 SE-233 end				
				
				for (var i:int=0; i<arrMA020X2.length; i++)
				{
					if (arrMA020X2[i].LAT != null)
					{
						webView.loadURL("javascript:addMarker(markers," + arrMA020X2[i].LAT + "," + arrMA020X2[i].LNG + ", '"+ arrMA020X2[i].TKNM + "<BR>" + BasedMethod.nvl(arrMA020X2[i].TKADD1) + BasedMethod.nvl(arrMA020X2[i].TKADD2) + BasedMethod.nvl(arrMA020X2[i].TKADD3) +"')");
					}
				}
			}
			else
			{
				webView.loadURL("javascript:centerLnglat(" + w_longitude + "," + w_latitude + ")");
			}
			
		}
		
		protected function update(event:LocationChangeEvent):void
		{
			trace("LocationChange!");
		}
		
		/**********************************************************
		 * Backキー押下時処理.
		 * @author 12.08.22 SE-354
		 **********************************************************/
		protected function onKeyDownHandler(e:KeyboardEvent):void
		{
			if (e.keyCode == Keyboard.BACK)
			{
				if (webView.stage != null)
				{
					e.preventDefault();
					webView.stage = null;
				}
				//if (this.webView.)
				//{
				//webView.dispose();
				//e.preventDefault();
				
				//navigator.popView();
				//}
			}
		}
		
		/**********************************************************
		 * btnORDER クリック時処理.
		 * @author 12.07.03 SE-354
		 **********************************************************/
		public function btnORDERClick(): void
		{
			// 並び順ポップアップ起動
			popup005 = new POPUP005();
			
			// 12.10.22 #3522 一旦別配列にコピーしてセットしないとキャンセル時にもバインドされて連動して変わってしまう不具合修正 SE-354 start
			var arrCOPY: ArrayCollection = new ArrayCollection();
			
			for each ( var _myObj:Object in arrPOPUP005 ) 
			{
				arrCOPY.addItem(ObjectUtil.copy(_myObj));
			}
			
			// 表示順
			//popup005.ctrl.arrPOPUP005 = arrPOPUP005;
			popup005.ctrl.arrPOPUP005 = arrCOPY;
			// 12.10.22 #3522 一旦別配列にコピーしてセットしないとキャンセル時にもバインドされて連動して変わってしまう不具合修正 SE-354 end
			
			PopUpManager.addPopUp(popup005, view, true);
			PopUpManager.centerPopUp(popup005);
			popup005.addEventListener(FlexEvent.REMOVE, btnORDERClick_last, false, 0, true);
		}
		private function btnORDERClick_last(event: FlexEvent): void
		{
			// OKボタン押下でポップアップを閉じたときのみORDERの修正
			if (popup005.ctrl.okflg == true)
			{
				arrPOPUP005 = popup005.ctrl.arrPOPUP005;
				
				// SharedObjectに表示順を保存
				saveParam();
				cdsMA020X();
			}
		}
		
		/**********************************************************
		 * btnJOKEN クリック時処理.
		 * @author 12.07.03 SE-354
		 **********************************************************/
		public function btnJOKENClick(): void
		{
			// 条件入力ポップアップ起動
			popup007 = new POPUP007();
			
			// 表示順
			popup007.ctrl.argSHPYM = argSHPYM;
			popup007.ctrl.argEHPYM = argEHPYM;
			popup007.ctrl.argSLHDAT = argSLHDAT;
			popup007.ctrl.argELHDAT = argELHDAT;
			
			popup007.ctrl.argDTAREA = argDTAREA;
			popup007.ctrl.argAREA = BasedMethod.nvl(view.lcmbAREA.selectedItem.CODE);
			popup007.ctrl.argIMPCLS = argIMPCLS;
			popup007.ctrl.argHTCLS = argHTCLS;
			popup007.ctrl.argTKCLS = argTKCLS;
			popup007.ctrl.argESTAMT = argESTAMT;
			popup007.ctrl.argKURI = argKURI;
			
			PopUpManager.addPopUp(popup007, view, true);
			PopUpManager.centerPopUp(popup007);
			popup007.addEventListener(FlexEvent.REMOVE, btnJOKENClick_last, false, 0, true);
		}
		private function btnJOKENClick_last(event: FlexEvent): void
		{
			// OKボタン押下でポップアップを閉じたときのみORDERの修正
			if (popup007.ctrl.okflg == true)
			{
				argSHPYM = popup007.ctrl.argSHPYM;
				argEHPYM = popup007.ctrl.argEHPYM;
				argSLHDAT = popup007.ctrl.argSLHDAT;
				argELHDAT = popup007.ctrl.argELHDAT;
				
				argDTAREA = popup007.ctrl.argDTAREA;
				argIMPCLS = popup007.ctrl.argIMPCLS;
				argHTCLS = popup007.ctrl.argHTCLS;
				argTKCLS = popup007.ctrl.argTKCLS;
				argESTAMT = popup007.ctrl.argESTAMT;
				argKURI = popup007.ctrl.argKURI;
				
				cdsMA020X();
			}
		}
		
		
		/**********************************************************
		 * 訪問計画 登録処理.
		 * @author 12.07.03 SE-354
		 **********************************************************/
		public function btnDECClick(): void
		{
			//trace(view.grp2.dataProvider.length);
			checkDataSet();
		}
		
		override public function updateDataSet_H(): void
		{
			// 削除処理
			delMA020X2();
			
			
			// 確定処理
			//updateMA020X2();
		}
		
		public function delMA020X2(): void
		{
			stmt = new SQLStatement();
			//stmt.sqlConnection =  connection;
			stmt.sqlConnection =  singleton.sqlconnection;
			
			stmt.text= singleton.g_query_xml.entry.(@key == "delMA020X2");
			
			//stmt.parameters[0] = arrSEIGYO[0].CHGCD;
			stmt.parameters[0] = view.edtHDAT.text;			
			
			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, delMA020X2Result, false, 0, true);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler, false, 0, true);
			
			//実行
			stmt.execute();
		}
		
		private function delMA020X2Result(event:SQLEvent):void 
		{
			//データの取得に成功したら
			//getResult()でSQLResultを取得し
			//取得したデータをデータグリッドに反映します。
			//status += "  データグリッド更新";
			var result:SQLResult = stmt.getResult();
			
			//var w_arr: ArrayCollection;
			//var i: int;
			/*
			if (result.data == null)
			{
			trace("nodata");
			}
			*/
			// 一旦、ワーク配列にセット 
			//w_arr = new ArrayCollection(result.data);
			//arrMA020X = new ArrayCollection(result.data);
			
			//view.grp1.dataProvider = arrMA020X;
			
			updateMA020X2();
		}
		
		/**********************************************************
		 * 訪問計画確定処理
		 * @author 12.09.11 SE-354
		 **********************************************************/
		public function updateMA020X2(): void
		{
			stmt = new SQLStatement();
			//stmt.sqlConnection =  connection;
			stmt.sqlConnection =  singleton.sqlconnection;
			
			stmt.text= singleton.g_query_xml.entry.(@key == "insMA020X2");
			//stmt.itemClass = LS001;
			/*
			// 修正時はUPDATE
			if (isNew == false)
			{ 
			stmt.text= singleton.g_query_xml.entry.(@key == "updUB910X");
			
			stmt.parameters[0] = view.ls001.HTID;
			//stmt.parameters[2] = view.ls001.CHGCD;
			stmt.parameters[1] = view.lcmbCHGNM.selectedItem.CHGCD;
			//stmt.parameters[2] = view.ls001.SDDAY;
			stmt.parameters[2] = view.edtSDDAY.text;
			stmt.parameters[3] = view.ls001.HVNO;
			stmt.parameters[4] = view.ls001.BVNO;
			stmt.parameters[5] = view.ls001.NVNO;
			}
			// 新規時はINSERT
			else
			{
			stmt.text= singleton.g_query_xml.entry.(@key == "insUB910X");
			
			stmt.parameters[0] = "0";
			stmt.parameters[1] = view.ls001.HTID;
			//stmt.parameters[2] = view.ls001.CHGCD;
			stmt.parameters[2] = view.lcmbCHGNM.selectedItem.CHGCD;
			//stmt.parameters[3] = view.ls001.SDDAY;
			stmt.parameters[3] = view.edtSDDAY.text;
			stmt.parameters[4] = view.ls001.HVNO;
			stmt.parameters[5] = view.ls001.BVNO;
			stmt.parameters[6] = view.ls001.NVNO;
			}
			*/
			
			// まずはLF010のDELETE
			
			
			var iterator:Iterator = new CustomIterator(arrMA020X2);
			var w_LM010: LM010 = new LM010();
			
			while (iterator.hasNext())
			{
				w_LM010 = iterator.next();
				
				//実績なしのデータのみ追加
				if(BasedMethod.nvl(w_LM010.ABSENT)==""){
					// CHGCD
					//stmt.parameters[0] = w_LM010.TKCHGCD;
					stmt.parameters[0] = arrSEIGYO[0].CHGCD;
					// HDAT
					stmt.parameters[1] = view.edtHDAT.text;
					// TKCD
					stmt.parameters[2] = w_LM010.TKCD;
					
					stmt.execute();
				}
			}
			
			//イベント登録
			//stmt.addEventListener(SQLEvent.RESULT, updateMA020X2Result);
			//stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);
			
			//実行
			//stmt.execute();
			
			// 「YYYY/MM/DDの訪問計画が作成されました。」
			//singleton.sitemethod.dspJoinErrMsg("I0004", btnRTNClick_last, "登録完了", "0", [view.edtHDAT.text + "の訪問計画"]);
			
			//trace("END");
		}
		
		
		/**********************************************************
		 * btnRTN クリック時処理.
		 * @author 12.07.03 SE-354
		 **********************************************************/
		public function btnRTNClick(): void
		{
			// 制御マスタメンテを終了します。よろしいですか？
			//singleton.sitemethod.dspJoinErrMsg();
			
			insLF013("","訪問計画終了");
			
			if (webView.stage != null)
			{
				webView.stage = null;
			}
			else
			{
				// メニューに戻る
				view.navigator.popView();
			}
			
		}
		
		public function btnRTNClick_last(): void
		{
			// メニューに戻る
			view.navigator.popView();
		}
		
		public function changeData(): void
		{
			sumESTAMT();
		}
		
		public function changeData2(): void
		{
			sumESTAMT2();
		}
		
		public function loadSO(): void
		{
			_so = SharedObject.getLocal("so_input");
			
			if (_so.data.ma020xorder == null)
			{
				arrPOPUP005 = new ArrayCollection([
					{DBNAME:"TKAREA,DTAREA,TKZIP",VIEWNAME:"住所"},
					{DBNAME:"LHDAT",VIEWNAME:"前回訪問日"},
					{DBNAME:"ESTAMT",VIEWNAME:"予測金額"}
				]);
			}
			else
			{
				arrPOPUP005 = _so.data.ma020xorder;
			}
		}
		
		public function saveParam(): void
		{
			// レートなどのSharedObjectへの保存
			_so = SharedObject.getLocal("so_input");
			
			_so.data.ma020xorder = arrPOPUP005;
		}
		
		private function sumESTAMT(): void
		{
			if ((arrMA020X2 == null) || (arrMA020X2.length == 0))
			{
				view.edtESTAMT.text = "0円";
				w_ken = 0;
				view.edtHKEN.text = w_ken.toString() + "件";
			}
			else
			{
				if (w_ken != arrMA020X2.length) {
					var sum: Number = 0;
					for (var i:int=0; i<arrMA020X2.length; i++)
					{
						sum += BasedMethod.asCurrency(arrMA020X2[i].ESTAMT);
					}
					view.edtESTAMT.text = singleton.g_customnumberformatter.numberFormat(sum.toString(), "1", "0", "0")  + "円";
					w_ken = arrMA020X2.length;
					view.edtHKEN.text = w_ken.toString() + "件";
				}
			}
		}
		
		private function sumESTAMT2(): void
		{
			if ((arrMA020X == null) || (arrMA020X.length == 0))
			{
				view.edtESTAMT2.text = "0円";
				w_ken2 = 0;
				view.edtHKEN2.text = w_ken2.toString() + "件";
			}
			else
			{
				if (w_ken2 != arrMA020X.length) {
					var sum: Number = 0;
					for (var i:int=0; i<arrMA020X.length; i++)
					{
						sum += BasedMethod.asCurrency(arrMA020X[i].ESTAMT);
					}
					view.edtESTAMT2.text = singleton.g_customnumberformatter.numberFormat(sum.toString(), "1", "0", "0")  + "円";
					w_ken2 = arrMA020X.length;
					view.edtHKEN2.text = w_ken2.toString() + "件";
				}
			}
		}
	}
}