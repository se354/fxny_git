package views
{
	import flash.data.SQLResult;
	import flash.data.SQLStatement;
	import flash.events.Event;
	import flash.events.GeolocationEvent;
	import flash.events.KeyboardEvent;
	import flash.events.LocationChangeEvent;
	import flash.events.SQLErrorEvent;
	import flash.events.SQLEvent;
	import flash.geom.Rectangle;
	import flash.media.StageWebView;
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	import flash.sensors.Geolocation;
	import flash.ui.Keyboard;
	
	import modules.base.BasedMethod;
	import modules.custom.CustomIterator;
	import modules.custom.sub.Iterator;
	import modules.dto.LM009;
	import modules.dto.MP060V01;
	import modules.dto.W_MA040X;
	import modules.formatters.CustomNumberFormatter;
	import modules.sbase.SiteMethod;
	import modules.sbase.SiteModule;
	
	import mx.collections.ArrayCollection;
	import mx.managers.PopUpManager;
	
	/**********************************************************
	 * MP060X 提供数入力ポップアップ
	 * @author 12.09.06 SE-362
	 **********************************************************/
	public class MP060XController extends SiteModule
	{
		private var view:MP060X;
		private var arrMP060X: ArrayCollection;

		// 引数情報
		public var argTKCD: String;			// 得意先コード
		public var argTKNM: String;			// 得意先名
		public var argHDAT: String;			// 訪問日付

		private var g_vno: String;
		
		private var cdsMP060X_arg: Object;
		
		public function MP060XController()
		{
			super();
		}
		
		/**********************************************************
		 * 初期処理
		 * @author  12.09.06 SE-362
		 **********************************************************/
		override public function init():void
		{
			// フォーム宣言
			view = _view as MP060X;

			// 初期表示
			view.edtCUNM.text = argTKNM;

			super.init();
		}
		
		/**********************************************************
		 * 画面立ち上げ処理
		 * @author  12.09.06 SE-362
		 **********************************************************/
		override public function formShow_last():void
		{
			super.formShow_last();

			// 表示処理
			doDSPProc();
		}

		/**********************************************************
		 * ヘッダデータ取得
		 * @author  12.09.06 SE-362
		 **********************************************************/
		override public function openDataSet_H():void
		{
			cdsSEIGYO(super.openDataSet_H);	
		}
		
		/**********************************************************
		 * 画面項目セット
		 * @author SE-354 
		 * @date 12.04.27
		 **********************************************************/
		override public function setDispObject(): void
		{
			super.setDispObject();

			if(arrMP060X != null && arrMP060X.length != 0){
				view.grp1.dataProvider = arrMP060X;
			}else{
				// 該当データなし
				singleton.sitemethod.dspJoinErrMsg("E0020", doDSPProc_last);
				return;
			}
			
		}
		
		/**********************************************************
		 * ボディデータ取得
		 * @author  12.09.06 SE-362
		 **********************************************************/
		override public function openDataSet_B():void
		{
			cdsMP060X(super.openDataSet_B);
		}
		
		/**********************************************************
		 * 登録
		 * @author  12.09.06 SE-362
		 **********************************************************/
		override public function execDecPLSQL():void
		{
			// 一旦、削除
			delMP060X(execDecPLSQL_cld1);
		}

		private function execDecPLSQL_cld1(): void
		{
			var w_length: int;
			var w_sflg: int;
			var i: int;
			
			w_length = arrMP060X.length;
			w_sflg = 0;
			for(i=0; i < w_length; i++){
				if(arrMP060X[i].TVPNO != "0"){
					w_sflg = 1;
				}
			}
			
			//数量がある場合のみ追加
			if(w_sflg != 0){
				// データ挿入
				insMP060X(execDecPLSQL_cld2);
			}else{
				execDecPLSQL_last();
			}
		}
		
		private function execDecPLSQL_cld2(): void
		{
			// 制御マスタ別売伝票No更新
			updMP060X(execDecPLSQL_last);
		}
		
		private function execDecPLSQL_last(): void
		{
			singleton.sitemethod.dspJoinErrMsg("I0004", super.execDecPLSQL, "", "0", ["提供数"]);
		}
		
		override public function doF10Proc_last(): void
		{
			super.doF10Proc_last();
			
			// 画面クローズ
			btnCANClick();
		}
		
		/**********************************************************
		 * 商品情報
		 * @author 12.09.06 SE-362
		 **********************************************************/
		public function cdsMP060X(func: Function=null): void
		{
			// 引数情報
			cdsMP060X_arg = new Object;
			cdsMP060X_arg.func = func;
			
			Stmt = new SQLStatement();
			Stmt.sqlConnection =  singleton.sqlconnection;
			Stmt.itemClass = MP060V01;
			Stmt.text= singleton.g_query_xml.entry.(@key == "qryMP060X");
			Stmt.parameters[0] = argTKCD;
			Stmt.parameters[1] = argHDAT;
			
			//イベント登録
			Stmt.addEventListener(SQLEvent.RESULT, cdsMP060XResult, false, 0, true);
			Stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler, false, 0, true);
			
			//実行
			Stmt.execute();
		}
		
		public function cdsMP060XResult(event: SQLEvent): void
		{
			var w_arg: Object = cdsMP060X_arg;
			var result:SQLResult = Stmt.getResult();
			
			arrMP060X = new ArrayCollection(result.data);
			
			// 次関数実行
			if(w_arg.func)
				w_arg.func();
		}
		
		/**********************************************************
		 * 削除処理
		 * @author 12.09.06 SE-362
		 **********************************************************/
		public function delMP060X(func: Function=null): void
		{
			// 引数情報
			cdsMP060X_arg = new Object;
			cdsMP060X_arg.func = func;
			
			Stmt = new SQLStatement();
			Stmt.sqlConnection =  singleton.sqlconnection;
			Stmt.text= singleton.g_query_xml.entry.(@key == "delMP060X");
			Stmt.parameters[0] = argTKCD;
			Stmt.parameters[1] = argHDAT;
			
			//イベント登録
			Stmt.addEventListener(SQLEvent.RESULT, delMP060XResult, false, 0, true);
			Stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler, false, 0, true);
			
			//実行
			Stmt.execute();
		}
		
		public function delMP060XResult(event: SQLEvent): void
		{
			var w_arg: Object = cdsMP060X_arg;
			var result:SQLResult = Stmt.getResult();
			
			// 次関数実行
			if(w_arg.func)
				w_arg.func();
		}

		/**********************************************************
		 * 提供数データ挿入
		 * @author 12.09.06 SE-362
		 **********************************************************/
		public function insMP060X(func: Function=null): void
		{
			// 引数情報
			cdsMP060X_arg = new Object;
			cdsMP060X_arg.func = func;
			
			Stmt = new SQLStatement();
			Stmt.sqlConnection =  singleton.sqlconnection;
			Stmt.text= singleton.g_query_xml.entry.(@key == "insMP060X");
			
			var iterator:Iterator = new CustomIterator(arrMP060X);
			var w_mp060x: MP060V01 = new MP060V01();
			//var w_vno: String = BasedMethod.calcNum(arrSEIGYO[0].BVNO, "1");
			var w_vseq: int = 0;
			
			while (iterator.hasNext())
			{
				w_mp060x = iterator.next();
				
				// 伝票Seqカウントアップ
				w_vseq++;
				
				g_vno = getVNO("2");
				
				// 伝票No
				//Stmt.parameters[0] = w_vno;
				Stmt.parameters[0] = g_vno;
				// 伝票Seq
				Stmt.parameters[1] = w_vseq.toString();
				// 得意先コード	
				Stmt.parameters[2] = argTKCD;
				// 訪問日付
				Stmt.parameters[3] = argHDAT;
				// 担当者コード
				Stmt.parameters[4] = arrSEIGYO[0].CHGCD;
				// 端末番号
				Stmt.parameters[5] = arrSEIGYO[0].HTID;
				// 商品コード			
				Stmt.parameters[6] = w_mp060x.VJAN;
				// 提供数
				Stmt.parameters[7] = w_mp060x.TVPNO;
				// 訪問日付
				Stmt.parameters[8] = argHDAT;

				// 実行
				Stmt.execute();
			}
			
			BasedMethod.execNextFunction(func);
		}
		
		/**********************************************************
		 * 制御マスタ更新処理
		 * @author 12.09.06 SE-362
		 **********************************************************/
		public function updMP060X(func: Function=null): void
		{
			// 引数情報
			cdsMP060X_arg = new Object;
			cdsMP060X_arg.func = func;
			
			Stmt = new SQLStatement();
			Stmt.sqlConnection =  singleton.sqlconnection;
			Stmt.text= singleton.g_query_xml.entry.(@key == "updMP060X");
			//Stmt.parameters[0] = BasedMethod.calcNum(arrSEIGYO[0].BVNO, "1");
			Stmt.parameters[0] = g_vno;
			
			//イベント登録
			Stmt.addEventListener(SQLEvent.RESULT, updMP060XResult, false, 0, true);
			Stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler, false, 0, true);
			
			//実行
			Stmt.execute();
		}
		
		public function updMP060XResult(event: SQLEvent): void
		{
			var w_arg: Object = cdsMP060X_arg;
			var result:SQLResult = Stmt.getResult();
			
			// 次関数実行
			if(w_arg.func)
				w_arg.func();
		}

		private function stmtErrorHandler(event: SQLErrorEvent): void
		{
			// エラーの表示
			trace(event.error.message);
		}

		/**********************************************************
		 * キャンセル
		 * @author  12.09.06 SE-362
		 **********************************************************/
		public function btnCANClick(): void
		{
			PopUpManager.removePopUp(view);
		}
	}
}