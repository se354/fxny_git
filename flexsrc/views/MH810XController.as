package views
{
	
	import flash.data.SQLResult;
	import flash.data.SQLStatement;
	import flash.events.SQLErrorEvent;
	import flash.events.SQLEvent;
	
	import modules.base.BasedMethod;
	import modules.base.SRV;
	import modules.custom.CustomIterator;
	import modules.custom.sub.Iterator;
	import modules.dto.LF010;
	import modules.dto.LF013;
	import modules.dto.LJ001;
	import modules.dto.LJ002;
	import modules.dto.LJ003;
	import modules.dto.LJ004;
	import modules.dto.LJ005;
	import modules.dto.LJ012;
	import modules.dto.LM007;
	import modules.dto.LS001;
	import modules.dto.LS006;
	import modules.dto.LS013;
	import modules.sbase.SiteModule;
	
	import mx.collections.ArrayCollection;
	import mx.rpc.AsyncResponder;
	import mx.rpc.events.ResultEvent;
	
	public class MH810XController extends SiteModule
	{
		private var view:MH810X;
		//private var arrUB910X: ArrayCollection;
		private var arrCHGCD: ArrayCollection;
		private var arrLJ001: ArrayCollection;
		private var arrLJ002: ArrayCollection;
		private var arrLJ003: ArrayCollection;
		private var arrLJ004: ArrayCollection;
		private var arrLJ005: ArrayCollection;
		private var arrLJ012: ArrayCollection;
		private var arrLF010U: ArrayCollection;
		private var arrLF010U1: ArrayCollection;
		private var arrLF013: ArrayCollection;
		private var stmt: SQLStatement;
		private var g_updateCount: int;
		private var g_delCount: int;
		
		private var cdsLJ001_arg: Object;
		private var cdsLJ002_arg: Object;
		private var cdsLJ003_arg: Object;
		private var cdsLJ004_arg: Object;
		private var cdsLJ005_arg: Object;
		private var cdsLJ012_arg: Object;
		
		private var cdsLF010U_arg: Object;
		private var cdsLF013_arg: Object;
		
		private var w_htid: String;
		private var w_chgcd: String;
		private var w_dowdat: String;
		private var w_dowtim: String;
		private var w_upddat: String;
		private var w_updtim: String;
		private var w_htchgcd: String;
		
		private var w_sysdate: String;
		private var w_systime: String;
		
		private var w_tno: String;
		
		//public var arrS008: ArrayCollection;
		
		private var isNew: Boolean;
		
		public function MH810XController()
		{
			super();
		}
		
		override public function init():void
		{
			
			// フォーム宣言
			view = _view as MH810X;

			// 変数の設定
			//g_prgid     = "MENUX";
			super.init();
		}
		
		override public function formShow_last(): void
		{
			cdsSEIGYO(formShow_last_cld1);
		}
		
		
		private function formShow_last_cld1(): void
		{			
			//cdsMA020X();
			//cdsAREA();
			if (arrSEIGYO.length != 0)
			{
				w_htid = arrSEIGYO[0].HTID;
				w_chgcd = arrSEIGYO[0].CHGCD;
				w_htchgcd = arrSEIGYO[0].HTCHGCD;

				w_upddat = arrSEIGYO[0].UPDDAT;
				w_updtim = arrSEIGYO[0].UPDTIM;
				
				w_dowdat = arrSEIGYO[0].DOWDAT;
				w_dowtim = arrSEIGYO[0].DOWTIM;

				w_tno = arrSEIGYO[0].TNO;
				
				view.edtUPDDAT.text = w_upddat;
				view.edtUPDTIM.text = w_updtim;
				
				view.edtDOWDAT.text = w_dowdat;
				view.edtDOWTIM.text = w_dowtim;
			}
		}
		
		/**********************************************************
		 * openDataSet_H データの取得.
		 * @author 12.08.30 SE-354
		 **********************************************************/
		/*
		override public function openDataSet_H(): void
		{
			//cdsS008();
		}
		*/
		
		public function btnDECClick(): void
		{
			// 端末ＩＤ、入力担当者、端末担当者チェック
			if((w_htid == "")||(w_chgcd == "")||(w_htchgcd == "")){
				singleton.sitemethod.dspJoinErrMsg("E0615");
				return;
			}
			
			singleton.sitemethod.dspJoinErrMsg("Q0017", btnDECClick_last, "", "1", null, btnDECClick_last2);
			
//			view.btnDEC.enabled = false;

//			view.chkLJ001.selected = false;
//			view.chkLJ002.selected = false;
//			view.chkLJ003.selected = false;
//			view.chkLJ004.selected = false;
//			view.chkLJ005.selected = false;
//			view.chkLJ012.selected = false;
//			view.chkLS001.selected = false;
			
//			insLF013("","アップロード開始");

			//checkDataSet();
//			doF10Proc();
		}
		
		private function btnDECClick_last(): void
		{
			view.btnDEC.enabled = false;
			
			view.chkLJ001.selected = false;
			view.chkLJ002.selected = false;
			view.chkLJ003.selected = false;
			view.chkLJ004.selected = false;
			view.chkLJ005.selected = false;
			view.chkLJ012.selected = false;
			view.chkLS001.selected = false;
			
			insLF013("","アップロード開始");
			
			doF10Proc();
		}

		private function btnDECClick_last2(): void
		{

		}

		/**********************************************************
		 * btnRTN クリック時処理.
		 * @author 12.07.03 SE-354
		 **********************************************************/
		public function btnRTNClick(): void
		{
			// 制御マスタメンテを終了します。よろしいですか？
			//singleton.sitemethod.dspJoinErrMsg();
			view.navigator.popView();
		}
		
		public function btnRTNClick_last(): void
		{
			// メニューに戻る
			view.navigator.popView();
		}
		
		override public function updateDataSet_B(): void
		{
			cdsLJ001(updateDataSet_B_cld1);
		}
		
		private function updateDataSet_B_cld1(): void
		{
			view.chkLJ001.selected = true;
			
			cdsLJ002(updateDataSet_B_cld2);
		}
		
		private function updateDataSet_B_cld2(): void
		{
			view.chkLJ002.selected = true;
			
			cdsLJ003(updateDataSet_B_cld3);
		}
		
		private function updateDataSet_B_cld3(): void
		{
			view.chkLJ003.selected = true;
			
			cdsLJ004(updateDataSet_B_cld4);
		}
		
		private function updateDataSet_B_cld4(): void
		{
			view.chkLJ004.selected = true;
			
			cdsLJ005(updateDataSet_B_cld5);
		}
		
		private function updateDataSet_B_cld5(): void
		{
			view.chkLJ005.selected = true;
			
			cdsLJ012(updateDataSet_B_cld6);
		}
		
		private function updateDataSet_B_cld6(): void
		{
			view.chkLJ012.selected = true;
			
			cdsLF010U(updateDataSet_B_cld7);
		}
		
		private function updateDataSet_B_cld7(): void
		{
			view.chkLF010.selected = true;
			
			cdsLF013(updateDataSet_B_cld8);
		}
		
		private function updateDataSet_B_cld8(): void
		{
			view.chkLF013.selected = true;

// 15.11.18 SE-233 start			
			updateLS001(updateDataSet_B_cld9);
		}
		
		private function updateDataSet_B_cld9(): void
		{
			if (w_tno != "0") {
				updateS013(super.updateDataSet_B);
				return;
			}
// 15.11.18 SE-233 end
			
			super.updateDataSet_B();
		}

		/**********************************************************
		 * プロシージャ実行
		 * @author 12.08.30 SE-354
		 **********************************************************/ 
		override public function execDecPLSQL(): void
		{
			var w_arg: ArrayCollection;
			
			// (引数のセット)
			// procBA140DEC={CALL BA140DEC(?,?,?,?,?,?,?,?,?,?)}
			
			w_arg = new ArrayCollection([
				["in",  "string", w_htid],	// 0:P_HTID
				["in",  "string", w_chgcd],				// 1:P_CHGCD
				["out", "number", ""],				// 4:W_FLG
				["out", "string", ""],				// 5:W_MSG1
				["out", "string", ""]				// 6:W_MSG2
			]);
			
			// (実行)
			execPLSQL2("sitedata", "procMD810S", w_arg, execDecPLSQL_last);
//			execPLSQL("sitedata", "procMD810S", w_arg, execDecPLSQL_last);
		}
		
		public function execDecPLSQL_last(): void
		{
			var w_msg1: String;
			var w_msg2: String;
			
			var obj_arg: Object = new Object();
			
//			updateLF010U(execDecPLSQL_last_cld100);
// 15.11.18 SE-233 start
//			updateLS001(execDecPLSQL_last_cld1);
			execDecPLSQL_last_cld1();
// 15.11.18 SE-233 end
			
			/*
			// 正常終了で返ってきたとき
			if (arrExecPlsql[0] == 0) 
			{
				updateLF010U(execDecPLSQL_last_cld1);
				
			}
			else
			{
				
				w_msg1  = arrExecPlsql[1];
				w_msg2  = arrExecPlsql[2];
				
				//singleton.sitemethod.dspJoinErrMsg(w_msg1, w_msg2, 3);
				
				//doF10Proc_last2();
				//trace("procerr");
				singleton.sitemethod.dspJoinErrMsg(w_msg1, null, "エラー");
				return;
			}
			*/
		}

//		private function execDecPLSQL_last_cld100(): void
//		{
//			updateLF010U(execDecPLSQL_last_cld1);
//		}
		
		private function execDecPLSQL_last_cld1(): void
		{
//			updateLS001(execDecPLSQL_last_cld2);
			deleteLF010U(execDecPLSQL_last_cld1a);
		}
		

		private function execDecPLSQL_last_cld1a(): void
		{
			g_delCount = 0;
			cdsLF010U1(execDecPLSQL_last_cld1b);
		}
		
		private function execDecPLSQL_last_cld1b(): void
		{
			if(arrLF010U1 != null && arrLF010U1.length != 0){
				deleteLF010U1(arrLF010U1[g_delCount].TKCD, execDecPLSQL_last_cld1c);
			}else{
				execDecPLSQL_last_cld2();
			}
		}
		
		private function execDecPLSQL_last_cld1c(): void
		{
			g_delCount++;
			
			if(arrLF010U1.length > g_delCount){
				execDecPLSQL_last_cld1b();
			}else{
				execDecPLSQL_last_cld2();
			}
		}
		
		private function execDecPLSQL_last_cld2(): void
		{
// 15.11.18 SE-233 start			
//			if (w_tno != "0") {
//				updateS013(execDecPLSQL_last_cld3);
//			}else{
				execDecPLSQL_last_cld3();
//			}
// 15.11.18 SE-233 end
		}
		
		private function execDecPLSQL_last_cld3(): void
		{
			delWORK(execDecPLSQL_last_cld4);
		}
		
		
		// ダウンロード
		
		private function execDecPLSQL_last_cld4(): void
		{
			view.chkLS001.selected = true;
			
			cdsW_LS006(execDecPLSQL_last_cld5);
		}
		
		private function execDecPLSQL_last_cld5(): void
		{
			view.chkLS006.selected = true;
			
			cdsW_LS010(execDecPLSQL_last_cld6);
		}
		
		private function execDecPLSQL_last_cld6(): void
		{
			view.chkLS010.selected = true;
			
			cdsW_LS014(execDecPLSQL_last_cld7);
		}
		
		private function execDecPLSQL_last_cld7(): void
		{
			view.chkLS014.selected = true;
			
			cdsW_LM006(execDecPLSQL_last_cld71);
		}
		
		private function execDecPLSQL_last_cld71(): void
		{
			view.chkLM006.selected = true;
			
			cdsW_LM008(execDecPLSQL_last_cld8);
		}
		
		private function execDecPLSQL_last_cld8(): void
		{
			view.chkLM008.selected = true;
			
			cdsW_LM009(execDecPLSQL_last_cld9);
		}
		
		private function execDecPLSQL_last_cld9(): void
		{
			view.chkLM009.selected = true;
			
			cdsW_LM010(execDecPLSQL_last_cld10);
		}
		
		private function execDecPLSQL_last_cld10(): void
		{
			view.chkLM010.selected = true;
			
			cdsW_LM012(execDecPLSQL_last_cld11);
		}
		
		private function execDecPLSQL_last_cld11(): void
		{
			view.chkLM012.selected = true;
			
			cdsW_LM013(execDecPLSQL_last_cld12);
		}
		
		private function execDecPLSQL_last_cld12(): void
		{
			view.chkLM013.selected = true;
			
			cdsW_LM014(execDecPLSQL_last_cld13);
		}
		
		private function execDecPLSQL_last_cld13(): void
		{
			view.chkLM014.selected = true;
			
			cdsW_LM015(execDecPLSQL_last_cld14);
		}
		
		private function execDecPLSQL_last_cld14(): void
		{
			view.chkLM015.selected = true;
			
			cdsW_LM016(execDecPLSQL_last_cld15);
		}
		
		private function execDecPLSQL_last_cld15(): void
		{
			view.chkLM016.selected = true;
			
			cdsW_LM017(execDecPLSQL_last_cld16);
		}
		
		private function execDecPLSQL_last_cld16(): void
		{
			view.chkLM017.selected = true;
			
			cdsW_LM018(execDecPLSQL_last_cld17);
		}
		
		private function execDecPLSQL_last_cld17(): void
		{
			view.chkLM018.selected = true;
			
			cdsW_LF001(execDecPLSQL_last_cld18);
		}
		
		private function execDecPLSQL_last_cld18(): void
		{
			view.chkLF001.selected = true;
			
			cdsW_LF002(execDecPLSQL_last_cld19);
		}
		
		private function execDecPLSQL_last_cld19(): void
		{
			view.chkLF002.selected = true;
			
			cdsW_LF003(execDecPLSQL_last_cld20);
		}
		
		private function execDecPLSQL_last_cld20(): void
		{
			view.chkLF003.selected = true;
			g_updateCount = 0;
			
			cdsW_LF004(execDecPLSQL_last_cld21);
		}
		
		private function execDecPLSQL_last_cld21(): void
		{
			if (g_updateCount == 0) {
				g_updateCount = 1;
				view.chkLF004.selected = true;
			}
			cdsW_LF010(execDecPLSQL_last_cld21a);
		}
		private function execDecPLSQL_last_cld21a(): void
		{
			// カウントアップ
			g_updateCount++;
			
			if(g_updateCount > 10){
				execDecPLSQL_last_cld22();
			}else{
				execDecPLSQL_last_cld21();
			}
		}
		
		private function execDecPLSQL_last_cld22(): void
		{
			view.chkLF0101.selected = true;
			
			cdsW_LF012(execDecPLSQL_last_cld23);
		}

		private function execDecPLSQL_last_cld23(): void
		{
			view.chkLF012.selected = true;

			var w_arg: ArrayCollection;
			
			// (引数のセット)
			w_arg = new ArrayCollection([
				["in",  "string", w_htchgcd],	// 0:P_HTID
				["in",  "string", w_chgcd],				// 1:P_CHGCD
				["out", "number", ""],				// 4:W_FLG
				["out", "string", ""],				// 5:W_MSG1
				["out", "string", ""]				// 6:W_MSG2
			]);
			
			// (実行)
			execPLSQL2("sitedata", "procMD810SD", w_arg, execDecPLSQL_last_cld24);
		}
		
		private function execDecPLSQL_last_cld24(): void
		{
			updateLF010U(execDecPLSQL_last_cld25);
		}
		
		private function execDecPLSQL_last_cld25(): void
		{
			updateLS001A(execDecPLSQL_last_cld99);
		}
		
		private function execDecPLSQL_last_cld99(): void
		{
			view.chkDEL.selected = true;
			
			w_upddat = w_sysdate;
			w_updtim = w_systime;
			
			view.edtUPDDAT.text = w_upddat;
			view.edtUPDTIM.text = w_updtim;

//			insLF013("","アップロード終了");
			
			// 正常に終了しました。
			singleton.sitemethod.dspJoinErrMsg("N0001", super.execDecPLSQL);
		}
		
		/**********************************************************
		 * SQLiteのselect
		 * @author 12.05.10 SE-354
		 **********************************************************/ 
		public function cdsLJ001(nextfunc: Function=null): void
		{
			// 変数の初期化
			//arrLJ001 = null;
			cdsLJ001_arg = new Object;
			cdsLJ001_arg.nextfunc = nextfunc;
			
			stmt = new SQLStatement();
			//stmt.sqlConnection =  connection;
			stmt.sqlConnection =  singleton.sqlconnection;
			stmt.itemClass = LJ001;
			stmt.text= singleton.g_query_xml.entry.(@key == "qryLJ001");
			
			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, cdsLJ001Result, false, 0, true);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler, false, 0, true);
			
			//実行
			stmt.execute();
		}
		
		private function cdsLJ001Result(event:SQLEvent):void 
		{
			var result:SQLResult = stmt.getResult();
			
			stmt.removeEventListener(SQLEvent.RESULT, cdsLJ001Result);				
			stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);				
			stmt=null;
			
			if (result.data == null)
			{
				trace("nodata");
				//singleton.sitemethod.dspJoinErrMsg("E0020");
				
				var w_arg: Object = cdsLJ001_arg;
				// 次関数実行
				if(w_arg.nextfunc)
				{
					BasedMethod.execNextFunction(w_arg.nextfunc);
				}
				
				return;
			}
			
			arrLJ001 = new ArrayCollection(result.data);
			
			updateW_LJ001();
		}
		
		/**********************************************************
		 * updateW_LJ001 
		 * @author 12.08.30 SE-354
		 **********************************************************/ 
		public function updateW_LJ001(): void
		{
			// 変数の初期化
			token = null;
			
			var srv: SRV = new SRV("sitedata");
			
			// データの更新
			token = srv.srv.updateW_LJ001(
				arrLJ001,
				w_htid
			);
			
			// 実行結果より、戻り値を取得
			token.addResponder(new AsyncResponder(
				
				// 成功時の処理
				function(e:ResultEvent, obj:Object=null):void
				{					
					
					var w_arg: Object = cdsLJ001_arg;
					// 次関数実行
					if(w_arg.nextfunc)
					{
						BasedMethod.execNextFunction(w_arg.nextfunc);
					}
				},
				
				// 失敗時の処理
				srv.TokenFaultEvent
			));
		}
		
		public function cdsLJ002(nextfunc: Function=null): void
		{
			// 変数の初期化
			//arrLJ001 = null;
			cdsLJ002_arg = new Object;
			cdsLJ002_arg.nextfunc = nextfunc;
			
			stmt = new SQLStatement();
			//stmt.sqlConnection =  connection;
			stmt.sqlConnection =  singleton.sqlconnection;
			stmt.itemClass = LJ002;
			stmt.text= singleton.g_query_xml.entry.(@key == "qryLJ002");
			
			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, cdsLJ002Result, false, 0, true);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler, false, 0, true);
			
			//実行
			stmt.execute();
		}
		
		private function cdsLJ002Result(event:SQLEvent):void 
		{
			var result:SQLResult = stmt.getResult();
			
			stmt.removeEventListener(SQLEvent.RESULT, cdsLJ002Result);				
			stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);				
			stmt=null;
			
			if (result.data == null)
			{
				trace("nodata");
				
				var w_arg: Object = cdsLJ002_arg;
				// 次関数実行
				if(w_arg.nextfunc)
				{
					BasedMethod.execNextFunction(w_arg.nextfunc);
				}
				
				return;
			}
			
			arrLJ002 = new ArrayCollection(result.data);
			
			updateW_LJ002();
		}
		
		/**********************************************************
		 * updateW_LJ002
		 * @author 12.08.30 SE-354
		 **********************************************************/ 
		public function updateW_LJ002(): void
		{
			
			// 変数の初期化
			token = null;
			
			var srv: SRV = new SRV("sitedata");
			
			// データの更新
			token = srv.srv.updateW_LJ002(
				arrLJ002,
				w_htid
			);
			
			// 実行結果より、戻り値を取得
			token.addResponder(new AsyncResponder(
				
				// 成功時の処理
				function(e:ResultEvent, obj:Object=null):void
				{					
					
					var w_arg: Object = cdsLJ002_arg;
					// 次関数実行
					if(w_arg.nextfunc)
					{
						BasedMethod.execNextFunction(w_arg.nextfunc);
					}
				},
				
				// 失敗時の処理
				srv.TokenFaultEvent
			));
		}
		
		public function cdsLJ003(nextfunc: Function=null): void
		{
			// 変数の初期化
			cdsLJ003_arg = new Object;
			cdsLJ003_arg.nextfunc = nextfunc;
			
			stmt = new SQLStatement();
			//stmt.sqlConnection =  connection;
			stmt.sqlConnection =  singleton.sqlconnection;
			stmt.itemClass = LJ003;
			stmt.text= singleton.g_query_xml.entry.(@key == "qryLJ003");
			
			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, cdsLJ003Result, false, 0, true);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler, false, 0, true);
			
			//実行
			stmt.execute();
		}
		private function cdsLJ003Result(event:SQLEvent):void 
		{
			var result:SQLResult = stmt.getResult();
			
			stmt.removeEventListener(SQLEvent.RESULT, cdsLJ003Result);				
			stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);				
			stmt=null;
			
			if (result.data == null)
			{
				trace("nodata");
				
				var w_arg: Object = cdsLJ003_arg;
				// 次関数実行
				if(w_arg.nextfunc)
				{
					BasedMethod.execNextFunction(w_arg.nextfunc);
				}
				
				return;
			}
			
			arrLJ003 = new ArrayCollection(result.data);
			
			updateW_LJ003();
		}
		/**********************************************************
		 * updateW_LJ003
		 * @author 12.08.30 SE-354
		 **********************************************************/ 
		public function updateW_LJ003(): void
		{
			
			// 変数の初期化
			token = null;
			
			var srv: SRV = new SRV("sitedata");
			
			// データの更新
			token = srv.srv.updateW_LJ003(
				arrLJ003,
				w_htid
			);
			
			// 実行結果より、戻り値を取得
			token.addResponder(new AsyncResponder(
				
				// 成功時の処理
				function(e:ResultEvent, obj:Object=null):void
				{					
					
					var w_arg: Object = cdsLJ003_arg;
					// 次関数実行
					if(w_arg.nextfunc)
					{
						BasedMethod.execNextFunction(w_arg.nextfunc);
					}
				},
				
				// 失敗時の処理
				srv.TokenFaultEvent
			));
		}
		
		public function cdsLJ004(nextfunc: Function=null): void
		{
			// 変数の初期化
			cdsLJ004_arg = new Object;
			cdsLJ004_arg.nextfunc = nextfunc;
			
			stmt = new SQLStatement();
			//stmt.sqlConnection =  connection;
			stmt.sqlConnection =  singleton.sqlconnection;
			stmt.itemClass = LJ004;
			stmt.text= singleton.g_query_xml.entry.(@key == "qryLJ004");
			
			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, cdsLJ004Result, false, 0, true);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler, false, 0, true);
			
			//実行
			stmt.execute();
		}
		private function cdsLJ004Result(event:SQLEvent):void 
		{
			var result:SQLResult = stmt.getResult();
			
			stmt.removeEventListener(SQLEvent.RESULT, cdsLJ004Result);				
			stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);				
			stmt=null;
			
			if (result.data == null)
			{
				trace("nodata");
				
				var w_arg: Object = cdsLJ004_arg;
				// 次関数実行
				if(w_arg.nextfunc)
				{
					BasedMethod.execNextFunction(w_arg.nextfunc);
				}
				
				return;
			}
			
			arrLJ004 = new ArrayCollection(result.data);
			
			updateW_LJ004();
		}
		/**********************************************************
		 * updateW_LJ004
		 * @author 12.08.30 SE-354
		 **********************************************************/ 
		public function updateW_LJ004(): void
		{
			
			// 変数の初期化
			token = null;
			
			var srv: SRV = new SRV("sitedata");
			
			// データの更新
			token = srv.srv.updateW_LJ004(
				arrLJ004,
				w_htid
			);
			
			// 実行結果より、戻り値を取得
			token.addResponder(new AsyncResponder(
				
				// 成功時の処理
				function(e:ResultEvent, obj:Object=null):void
				{					
					
					var w_arg: Object = cdsLJ004_arg;
					// 次関数実行
					if(w_arg.nextfunc)
					{
						BasedMethod.execNextFunction(w_arg.nextfunc);
					}
				},
				
				// 失敗時の処理
				srv.TokenFaultEvent
			));
		}
		
		public function cdsLJ005(nextfunc: Function=null): void
		{
			// 変数の初期化
			cdsLJ005_arg = new Object;
			cdsLJ005_arg.nextfunc = nextfunc;
			
			stmt = new SQLStatement();
			//stmt.sqlConnection =  connection;
			stmt.sqlConnection =  singleton.sqlconnection;
			stmt.itemClass = LJ005;
			stmt.text= singleton.g_query_xml.entry.(@key == "qryLJ005");
			
			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, cdsLJ005Result, false, 0, true);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler, false, 0, true);
			
			//実行
			stmt.execute();
		}
		private function cdsLJ005Result(event:SQLEvent):void 
		{
			var result:SQLResult = stmt.getResult();
			
			stmt.removeEventListener(SQLEvent.RESULT, cdsLJ005Result);				
			stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);				
			stmt=null;
			
			if (result.data == null)
			{
				trace("nodata");
				
				var w_arg: Object = cdsLJ005_arg;
				// 次関数実行
				if(w_arg.nextfunc)
				{
					BasedMethod.execNextFunction(w_arg.nextfunc);
				}
				
				return;
			}
			
			arrLJ005 = new ArrayCollection(result.data);
			
			updateW_LJ005();
		}
		/**********************************************************
		 * updateW_LJ005
		 * @author 12.08.30 SE-354
		 **********************************************************/ 
		public function updateW_LJ005(): void
		{
			
			// 変数の初期化
			token = null;
			
			var srv: SRV = new SRV("sitedata");
			
			// データの更新
			token = srv.srv.updateW_LJ005(
				arrLJ005,
				w_htid
			);
			
			// 実行結果より、戻り値を取得
			token.addResponder(new AsyncResponder(
				
				// 成功時の処理
				function(e:ResultEvent, obj:Object=null):void
				{					
					
					var w_arg: Object = cdsLJ005_arg;
					// 次関数実行
					if(w_arg.nextfunc)
					{
						BasedMethod.execNextFunction(w_arg.nextfunc);
					}
				},
				
				// 失敗時の処理
				srv.TokenFaultEvent
			));
		}
		
		public function cdsLJ012(nextfunc: Function=null): void
		{
			// 変数の初期化
			cdsLJ012_arg = new Object;
			cdsLJ012_arg.nextfunc = nextfunc;
			
			stmt = new SQLStatement();
			//stmt.sqlConnection =  connection;
			stmt.sqlConnection =  singleton.sqlconnection;
			stmt.itemClass = LJ012;
			stmt.text= singleton.g_query_xml.entry.(@key == "qryLJ012");
			
			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, cdsLJ012Result, false, 0, true);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler, false, 0, true);
			
			//実行
			stmt.execute();
		}
		private function cdsLJ012Result(event:SQLEvent):void 
		{
			var result:SQLResult = stmt.getResult();
			
			stmt.removeEventListener(SQLEvent.RESULT, cdsLJ012Result);				
			stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);				
			stmt=null;
			
			if (result.data == null)
			{
				trace("nodata");
				
				var w_arg: Object = cdsLJ012_arg;
				// 次関数実行
				if(w_arg.nextfunc)
				{
					BasedMethod.execNextFunction(w_arg.nextfunc);
				}
				
				return;
			}
			
			arrLJ012 = new ArrayCollection(result.data);
			
			updateW_LJ012();
		}
		/**********************************************************
		 * updateW_LJ012
		 * @author 12.08.30 SE-354
		 **********************************************************/ 
		public function updateW_LJ012(): void
		{
			
			// 変数の初期化
			token = null;
			
			var srv: SRV = new SRV("sitedata");
			
			// データの更新
			token = srv.srv.updateW_LJ012(
				arrLJ012,
				w_htid
			);
			
			// 実行結果より、戻り値を取得
			token.addResponder(new AsyncResponder(
				
				// 成功時の処理
				function(e:ResultEvent, obj:Object=null):void
				{					
					
					var w_arg: Object = cdsLJ012_arg;
					// 次関数実行
					if(w_arg.nextfunc)
					{
						BasedMethod.execNextFunction(w_arg.nextfunc);
					}
				},
				
				// 失敗時の処理
				srv.TokenFaultEvent
			));
		}
		
		public function cdsLF010U(nextfunc: Function=null): void
		{
			// 変数の初期化
			//arrLJ001 = null;
			cdsLF010U_arg = new Object;
			cdsLF010U_arg.nextfunc = nextfunc;
			
			stmt = new SQLStatement();
			//stmt.sqlConnection =  connection;
			stmt.sqlConnection =  singleton.sqlconnection;
			stmt.itemClass = LF010;
			stmt.text= singleton.g_query_xml.entry.(@key == "qryLF010U");
			if(arrSEIGYO[0].UPDDAT == ""){
				stmt.parameters[0] = arrSEIGYO[0].SDDAY;
			}else{
				stmt.parameters[0] = arrSEIGYO[0].UPDDAT;
			}
			
			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, cdsLF010UResult, false, 0, true);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler, false, 0, true);
			
			//実行
			stmt.execute();
		}
		
		private function cdsLF010UResult(event:SQLEvent):void 
		{
			var result:SQLResult = stmt.getResult();
			
			stmt.removeEventListener(SQLEvent.RESULT, cdsLF010UResult);				
			stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);				
			stmt=null;
			
			if (result.data == null)
			{
				trace("nodata");
				
				var w_arg: Object = cdsLF010U_arg;
				// 次関数実行
				if(w_arg.nextfunc)
				{
					BasedMethod.execNextFunction(w_arg.nextfunc);
				}
				
				return;
			}
			
			arrLF010U = new ArrayCollection(result.data);
			
			updateW_LF010U();
		}
		/**********************************************************
		 * updateW_LF010U
		 * @author 12.08.30 SE-354
		 **********************************************************/ 
		public function updateW_LF010U(): void
		{
			
			// 変数の初期化
			token = null;
			
			var srv: SRV = new SRV("sitedata");
			
			// データの更新
			token = srv.srv.updateW_LF010U(
				arrLF010U,
				w_htid
			);
			
			// 実行結果より、戻り値を取得
			token.addResponder(new AsyncResponder(
				
				// 成功時の処理
				function(e:ResultEvent, obj:Object=null):void
				{					
					
					var w_arg: Object = cdsLF010U_arg;
					// 次関数実行
					if(w_arg.nextfunc)
					{
						BasedMethod.execNextFunction(w_arg.nextfunc);
					}
				},
				
				// 失敗時の処理
				srv.TokenFaultEvent
			));
		}

		public function cdsLF013(nextfunc: Function=null): void
		{
			// 変数の初期化
			cdsLF013_arg = new Object;
			cdsLF013_arg.nextfunc = nextfunc;
			
			stmt = new SQLStatement();
			//stmt.sqlConnection =  connection;
			stmt.sqlConnection =  singleton.sqlconnection;
			stmt.itemClass = LF013;
			stmt.text= singleton.g_query_xml.entry.(@key == "qryLF013");
			
			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, cdsLF013Result, false, 0, true);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler, false, 0, true);
			
			//実行
			stmt.execute();
		}
		
		private function cdsLF013Result(event:SQLEvent):void 
		{
			var result:SQLResult = stmt.getResult();
			
			stmt.removeEventListener(SQLEvent.RESULT, cdsLF013Result);				
			stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);				
			stmt=null;
			
			if (result.data == null)
			{
				trace("nodata");
				
				var w_arg: Object = cdsLF013_arg;
				// 次関数実行
				if(w_arg.nextfunc)
				{
					BasedMethod.execNextFunction(w_arg.nextfunc);
				}
				
				return;
			}
			
			arrLF013 = new ArrayCollection(result.data);
			
			updateW_LF013();
		}
		/**********************************************************
		 * updateW_LF013
		 * @author 12.12.21 SE-233
		 **********************************************************/ 
		public function updateW_LF013(): void
		{
			
			// 変数の初期化
			token = null;
			
			var srv: SRV = new SRV("sitedata");
			
			// データの更新
			token = srv.srv.updateW_LF013(
				arrLF013,
				w_htid
			);
			
			// 実行結果より、戻り値を取得
			token.addResponder(new AsyncResponder(
				
				// 成功時の処理
				function(e:ResultEvent, obj:Object=null):void
				{					
					
					var w_arg: Object = cdsLF013_arg;
					// 次関数実行
					if(w_arg.nextfunc)
					{
						BasedMethod.execNextFunction(w_arg.nextfunc);
					}
				},
				
				// 失敗時の処理
				srv.TokenFaultEvent
			));
		}
		
		/**********************************************************
		 * 制御マスタへのアップデート
		 * @author 12.08.30 SE-354
		 **********************************************************/ 
		public function updateLS001(nextfunc: Function=null): void
		{
			stmt = new SQLStatement();
			//stmt.sqlConnection =  connection;
			stmt.sqlConnection =  singleton.sqlconnection;
			
			stmt.text= singleton.g_query_xml.entry.(@key == "updLS001U");
			
			var w_date:Date = new Date();
			w_sysdate = singleton.g_customdateformatter.dateDisplayFormat_Full(w_date);
			w_systime = singleton.g_customdateformatter.timeDisplayFormat(w_date);
			
			stmt.parameters[0] = w_sysdate;
			stmt.parameters[1] = w_systime;
			
			stmt.execute();
			
			// 次関数を実行
			BasedMethod.execNextFunction(nextfunc);
		}
		
		/**********************************************************
		 * updateS013
		 * @author 12.11.08 SE-300
		 **********************************************************/ 
		public function updateS013(nextfunc: Function=null): void
		{
			var arrS013: ArrayCollection = new ArrayCollection();
			var objLS013: LS013 = new LS013;

			objLS013.HTID = w_htid;
			objLS013.CHGCD = w_chgcd;
			objLS013.UPDDAT = w_sysdate;
			objLS013.UPDTIME = w_systime.substr(0,5);
			objLS013.TNO = w_tno;
			
			//arrS013 = new ArrayCollection([
			//	{HTID:w_htid,CHGCD:w_chgcd,UPDDAT:w_sysdate,UPDTIME:w_systime,TNO:w_tno}
			//]);
			
			arrS013.addItem(objLS013);
			
			// 変数の初期化
			token = null;
			
			var srv: SRV = new SRV("sitedata");
			
			// データの更新
			token = srv.srv.updateS013(
				arrS013,
				w_htid
			);
			
			// 実行結果より、戻り値を取得
			token.addResponder(new AsyncResponder(
				
				// 成功時の処理
				function(e:ResultEvent, obj:Object=null):void
				{					
					BasedMethod.execNextFunction(nextfunc);
				},
				
				// 失敗時の処理
				srv.TokenFaultEvent
			));
		}
		
		/**********************************************************
		 * 訪問計画実績ファイルのアップデート
		 * @author 12.08.30 SE-354
		 **********************************************************/ 
		public function updateLF010U(nextfunc: Function=null): void
		{
			stmt = new SQLStatement();
			//stmt.sqlConnection =  connection;
			stmt.sqlConnection =  singleton.sqlconnection;
			
			stmt.text= singleton.g_query_xml.entry.(@key == "updLF010U");
			
			stmt.parameters[0] = w_sysdate;

			stmt.execute();
			
			// 次関数を実行
			BasedMethod.execNextFunction(nextfunc);
		}

		/**********************************************************
		 * W_LS008取得及び制御マスタへのアップデート
		 * @author 13.04.19 SE-233
		 **********************************************************/ 
		public function updateLS001A(nextfunc: Function=null): void
		{
			// 変数の初期化
			token = null;
			
			var srv: SRV = new SRV("sitedata");
			
			// データの取得
			token = srv.srv.cdsW_LS008(w_htid);
			
			// 実行結果より、戻り値を取得
			token.addResponder(new AsyncResponder(
				
				// 成功時の処理
				function(e:ResultEvent, obj:Object=null):void
				{
					var w_arr: ArrayCollection;
					var i: int;
					
					// 一旦、ワーク配列にセット
					w_arr = new ArrayCollection(); 
					w_arr = e.result;
					
					
					stmt = new SQLStatement();
					//stmt.sqlConnection =  connection;
					stmt.sqlConnection =  singleton.sqlconnection;
					
					// トランザクション開始
					stmt.sqlConnection.begin();
					
					stmt.text= singleton.g_query_xml.entry.(@key == "updLS001A");
					
					for (i=0; i<w_arr.length; i++) {
						
						stmt.parameters[0] = w_arr[i].COSNM1;
						stmt.parameters[1] = w_arr[i].COSNM2;
						stmt.parameters[2] = w_arr[i].COSNM3;
						stmt.parameters[3] = w_arr[i].ADD1_1;
						stmt.parameters[4] = w_arr[i].ADD2_1;
						stmt.parameters[5] = w_arr[i].ADD3_1;
						stmt.parameters[6] = w_arr[i].TEL1;
						stmt.parameters[7] = w_arr[i].TEL2;
						stmt.parameters[8] = w_arr[i].TEL3;
						stmt.parameters[9] = w_arr[i].BNKCD1;
						stmt.parameters[10] = w_arr[i].BNKSCD1;
						stmt.parameters[11] = w_arr[i].VBNO1;
						stmt.parameters[12] = w_arr[i].BNKMEMO1;
						stmt.parameters[13] = w_arr[i].BNKCD2;
						stmt.parameters[14] = w_arr[i].BNKSCD2;
						stmt.parameters[15] = w_arr[i].VBNO2;
						stmt.parameters[16] = w_arr[i].BNKMEMO2;
						stmt.parameters[17] = w_arr[i].BNKCD3;
						stmt.parameters[18] = w_arr[i].BNKSCD3;
						stmt.parameters[19] = w_arr[i].VBNO3;
						stmt.parameters[20] = w_arr[i].BNKMEMO3;
						stmt.parameters[21] = w_arr[i].ADD1_2;
						stmt.parameters[22] = w_arr[i].ADD2_2;
						stmt.parameters[23] = w_arr[i].ADD3_2;
						
						stmt.execute();
					}
					
					// コミット
					stmt.sqlConnection.commit();
					
					// 次関数を実行
					BasedMethod.execNextFunction(nextfunc);
				},
				
				// 失敗時の処理
				srv.TokenFaultEvent
			));
		}
		
		/**********************************************************
		 * 訪問計画実績ファイル　今日より前の未訪問の予定削除
		 * @author 13.03.14 SE-233
		 **********************************************************/ 
		public function deleteLF010U(nextfunc: Function=null): void
		{
			stmt = new SQLStatement();

			stmt.sqlConnection =  singleton.sqlconnection;
			
			stmt.text= singleton.g_query_xml.entry.(@key == "delLF010U");
			
			stmt.parameters[0] = w_sysdate;
			
			stmt.execute();
			
			// 次関数を実行
			BasedMethod.execNextFunction(nextfunc);
		}
		
		public function cdsLF010U1(nextfunc: Function=null): void
		{
			// 変数の初期化
			cdsLF010U_arg = new Object;
			cdsLF010U_arg.nextfunc = nextfunc;
			
			stmt = new SQLStatement();
			//stmt.sqlConnection =  connection;
			stmt.sqlConnection =  singleton.sqlconnection;
			stmt.itemClass = LF010;
			stmt.text= singleton.g_query_xml.entry.(@key == "qryLF010U1");
			stmt.parameters[0] = w_sysdate;
			
			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, cdsLF010U1Result, false, 0, true);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler, false, 0, true);
			
			//実行
			stmt.execute();
		}
		
		private function cdsLF010U1Result(event:SQLEvent):void 
		{
			var result:SQLResult = stmt.getResult();
			var w_arg: Object = cdsLF010U_arg;
			
			stmt.removeEventListener(SQLEvent.RESULT, cdsLF010U1Result);				
			stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);				
			stmt=null;
			
			if (result.data == null)
			{
				trace("nodata");
				
				// 次関数実行
				if(w_arg.nextfunc)
				{
					BasedMethod.execNextFunction(w_arg.nextfunc);
				}
				
				return;
			}
			
			arrLF010U1 = new ArrayCollection(result.data);
			
			BasedMethod.execNextFunction(w_arg.nextfunc);
		}

		/**********************************************************
		 * 訪問計画実績ファイル　未訪問の予定削除
		 * @author 13.03.28 SE-233
		 **********************************************************/ 
		public function deleteLF010U1(p_tkcd: String, nextfunc: Function=null): void
		{
			stmt = new SQLStatement();
			
			stmt.sqlConnection =  singleton.sqlconnection;
			
			stmt.text= singleton.g_query_xml.entry.(@key == "delLF010U1");
			
			stmt.parameters[0] = w_sysdate;
			stmt.parameters[1] = p_tkcd;
			
			stmt.execute();
			
			// 次関数を実行
			BasedMethod.execNextFunction(nextfunc);
		}
		
		public function delWORK(nextfunc: Function=null): void
		{
			stmt = new SQLStatement();
			//stmt.sqlConnection =  connection;
			stmt.sqlConnection =  singleton.sqlconnection;
			
			stmt.text= singleton.g_query_xml.entry.(@key == "delLJ001");
			
			stmt.execute();
			
			stmt.text= singleton.g_query_xml.entry.(@key == "delLJ002");
			
			stmt.execute();

			stmt.text= singleton.g_query_xml.entry.(@key == "delLJ003");
			
			stmt.execute();
			
			stmt.text= singleton.g_query_xml.entry.(@key == "delLJ004");
			
			stmt.execute();
			
			stmt.text= singleton.g_query_xml.entry.(@key == "delLJ005");
			
			stmt.execute();
			
			stmt.text= singleton.g_query_xml.entry.(@key == "delLJ012");
			
			stmt.execute();
			
			stmt.text= singleton.g_query_xml.entry.(@key == "delLF013");
			
			stmt.execute();
			
			// 次関数を実行
			BasedMethod.execNextFunction(nextfunc);
		}
		
		/**********************************************************
		 * 税マスタのダウンロード及びSQLiteへのインサート
		 * @author 12.08.30 SE-354
		 **********************************************************/ 
		public function cdsW_LS006(nextfunc: Function=null): void
		{
			// 変数の初期化
			token = null;
			
			var srv: SRV = new SRV("sitedata");
			
			// データの取得
			token = srv.srv.cdsW_LS006(w_htid);
			
			// 実行結果より、戻り値を取得
			token.addResponder(new AsyncResponder(
				
				// 成功時の処理
				function(e:ResultEvent, obj:Object=null):void
				{
					var w_arr: ArrayCollection;
					var i: int;
					
					// 一旦、ワーク配列にセット
					w_arr = new ArrayCollection(); 
					w_arr = e.result;
					
					
					stmt = new SQLStatement();
					//stmt.sqlConnection =  connection;
					stmt.sqlConnection =  singleton.sqlconnection;
					
					// トランザクション開始
					stmt.sqlConnection.begin();
					
					//insert前に削除
					stmt.text= singleton.g_query_xml.entry.(@key == "delLS006");
					stmt.execute();
					
					// コミット
					stmt.sqlConnection.commit();
					
					// トランザクション開始
					stmt.sqlConnection.begin();
					
					stmt.text= singleton.g_query_xml.entry.(@key == "insLS006");
					
					for (i=0; i<w_arr.length; i++) {
						//arrLS006.addItem(LS006(w_arr[i]));
						
						// TAXCLS
						stmt.parameters[0] = w_arr[i].TAXCLS;
						// TAXCLSNM
						stmt.parameters[1] = w_arr[i].TAXCLSNM;
						// TAXRT
						stmt.parameters[2] = w_arr[i].TAXRT;
						stmt.parameters[3] = w_arr[i].KJNDY;
						stmt.parameters[4] = w_arr[i].TAXRTN;
						
						stmt.execute();
					}
					
					// コミット
					stmt.sqlConnection.commit();
					
					// 次関数を実行
					BasedMethod.execNextFunction(nextfunc);
				},
				
				// 失敗時の処理
				srv.TokenFaultEvent
			));
		}
		
		/**********************************************************
		 * 得意先区分ダウンロード及びSQLiteへのインサート
		 * @author 12.08.30 SE-354
		 **********************************************************/ 
		public function cdsW_LS010(nextfunc: Function=null): void
		{
			// 変数の初期化
			token = null;
			
			var srv: SRV = new SRV("sitedata");
			
			// データの取得
			token = srv.srv.cdsW_LS010(w_htid);
			
			// 実行結果より、戻り値を取得
			token.addResponder(new AsyncResponder(
				
				// 成功時の処理
				function(e:ResultEvent, obj:Object=null):void
				{
					var w_arr: ArrayCollection;
					var i: int;
					
					// 一旦、ワーク配列にセット
					w_arr = new ArrayCollection(); 
					w_arr = e.result;
					
					
					stmt = new SQLStatement();
					//stmt.sqlConnection =  connection;
					stmt.sqlConnection =  singleton.sqlconnection;
					
					// トランザクション開始
					stmt.sqlConnection.begin();
					
					//insert前に削除
					stmt.text= singleton.g_query_xml.entry.(@key == "delLS010");
					stmt.execute();
					
					// コミット
					stmt.sqlConnection.commit();
					
					// トランザクション開始
					stmt.sqlConnection.begin();
					
					stmt.text= singleton.g_query_xml.entry.(@key == "insLS010");
					
					for (i=0; i<w_arr.length; i++) {
						
						// CLS
						stmt.parameters[0] = w_arr[i].CLS;
						stmt.parameters[1] = w_arr[i].CLSNM1;
						stmt.parameters[2] = w_arr[i].CLSNM2;
						stmt.parameters[3] = w_arr[i].CLSNM3;
						stmt.parameters[4] = w_arr[i].CLSNM4;
						stmt.parameters[5] = w_arr[i].CLSNM5;
						stmt.parameters[6] = w_arr[i].CLSNM6;
						stmt.parameters[7] = w_arr[i].CLSNM7;
						stmt.parameters[8] = w_arr[i].CLSNM8;
						
						stmt.execute();
					}
					
					// コミット
					stmt.sqlConnection.commit();
					
					// 次関数を実行
					BasedMethod.execNextFunction(nextfunc);
				},
				
				// 失敗時の処理
				srv.TokenFaultEvent
			));
		}
		
		/**********************************************************
		 * 和暦年号ダウンロード及びSQLiteへのインサート
		 * @author 12.11.05 SE-300
		 **********************************************************/ 
		public function cdsW_LS014(nextfunc: Function=null): void
		{
			// 変数の初期化
			token = null;
			
			var srv: SRV = new SRV("sitedata");
			
			// データの取得
			token = srv.srv.cdsW_LS014(w_htid);
			
			// 実行結果より、戻り値を取得
			token.addResponder(new AsyncResponder(
				
				// 成功時の処理
				function(e:ResultEvent, obj:Object=null):void
				{
					var w_arr: ArrayCollection;
					var i: int;
					
					// 一旦、ワーク配列にセット
					w_arr = new ArrayCollection(); 
					w_arr = e.result;
					
					
					stmt = new SQLStatement();
					//stmt.sqlConnection =  connection;
					stmt.sqlConnection =  singleton.sqlconnection;
					
					// トランザクション開始
					stmt.sqlConnection.begin();
					
					//insert前に削除
					stmt.text= singleton.g_query_xml.entry.(@key == "delLS014");
					stmt.execute();
					
					// コミット
					stmt.sqlConnection.commit();
					
					// トランザクション開始
					stmt.sqlConnection.begin();
					
					stmt.text= singleton.g_query_xml.entry.(@key == "insLS014");
					
					for (i=0; i<w_arr.length; i++) {
						
						stmt.parameters[0] = w_arr[i].CLS;
						stmt.parameters[1] = w_arr[i].ERA;
						stmt.parameters[2] = w_arr[i].SDAT;
						stmt.parameters[3] = w_arr[i].FLG;
						stmt.parameters[4] = w_arr[i].ERAS;
						
						stmt.execute();
					}
					
					// コミット
					stmt.sqlConnection.commit();
					
					// 次関数を実行
					BasedMethod.execNextFunction(nextfunc);
				},
				
				// 失敗時の処理
				srv.TokenFaultEvent
			));
		}
		
		/**********************************************************
		 * 商品マスタダウンロード及びSQLiteへのインサート
		 * @author 12.08.30 SE-354
		 **********************************************************/ 
		public function cdsW_LM006(nextfunc: Function=null): void
		{
			// 変数の初期化
			token = null;
			
			var srv: SRV = new SRV("sitedata");
			
			// データの取得
			token = srv.srv.cdsW_LM006(w_htid);
			
			// 実行結果より、戻り値を取得
			token.addResponder(new AsyncResponder(
				
				// 成功時の処理
				function(e:ResultEvent, obj:Object=null):void
				{
					var w_arr: ArrayCollection;
					var i: int;
					
					// 一旦、ワーク配列にセット
					w_arr = new ArrayCollection(); 
					w_arr = e.result;
					
					
					stmt = new SQLStatement();
					//stmt.sqlConnection =  connection;
					stmt.sqlConnection =  singleton.sqlconnection;
					
					// トランザクション開始
					stmt.sqlConnection.begin();
					
					//insert前に削除
					stmt.text= singleton.g_query_xml.entry.(@key == "delLM006");
					stmt.execute();
					
					// コミット
					stmt.sqlConnection.commit();
					
					// トランザクション開始
					stmt.sqlConnection.begin();
					
					stmt.text= singleton.g_query_xml.entry.(@key == "insLM006");
					
					for (i=0; i<w_arr.length; i++) {
						
						stmt.parameters[0] = w_arr[i].VJAN;
						stmt.parameters[1] = w_arr[i].GDNM;
						stmt.parameters[2] = w_arr[i].STCST;
						stmt.parameters[3] = w_arr[i].DCLS;
						stmt.parameters[4] = w_arr[i].GDCLS;
						stmt.parameters[5] = w_arr[i].MDCLS;
						stmt.parameters[6] = w_arr[i].OFCLS;
						stmt.parameters[7] = w_arr[i].GDKNM;
						stmt.parameters[8] = w_arr[i].GDCOM;
						stmt.parameters[9] = w_arr[i].ATTENT;
						stmt.parameters[10] = w_arr[i].MEMO;
						stmt.parameters[11] = w_arr[i].DISRT;
						stmt.parameters[12] = w_arr[i].DISCST;
						
						stmt.execute();
					}
					
					// コミット
					stmt.sqlConnection.commit();
					
					// 次関数を実行
					BasedMethod.execNextFunction(nextfunc);
				},
				
				// 失敗時の処理
				srv.TokenFaultEvent
			));
		}
		
		/**********************************************************
		 * 担当者別在庫マスタダウンロード及びSQLiteへのインサート
		 * @author 12.08.30 SE-354
		 **********************************************************/ 
		public function cdsW_LM008(nextfunc: Function=null): void
		{
			// 変数の初期化
			token = null;
			
			var srv: SRV = new SRV("sitedata");
			
			// データの取得
			token = srv.srv.cdsW_LM008(w_htid);
			
			// 実行結果より、戻り値を取得
			token.addResponder(new AsyncResponder(
				
				// 成功時の処理
				function(e:ResultEvent, obj:Object=null):void
				{
					var w_arr: ArrayCollection;
					var i: int;
					
					// 一旦、ワーク配列にセット
					w_arr = new ArrayCollection(); 
					w_arr = e.result;
					
					
					stmt = new SQLStatement();
					//stmt.sqlConnection =  connection;
					stmt.sqlConnection =  singleton.sqlconnection;
					
					// トランザクション開始
					stmt.sqlConnection.begin();
					
					stmt.text= singleton.g_query_xml.entry.(@key == "insLM008");
					
					for (i=0; i<w_arr.length; i++) {
						
						stmt.parameters[0] = w_arr[i].CHGCD;
						stmt.parameters[1] = w_arr[i].VJAN;
						stmt.parameters[2] = w_arr[i].VPNO;
						stmt.parameters[3] = w_arr[i].MVPNO;
						
						stmt.execute();
					}
					
					// コミット
					stmt.sqlConnection.commit();
					
					// 次関数を実行
					BasedMethod.execNextFunction(nextfunc);
				},
				
				// 失敗時の処理
				srv.TokenFaultEvent
			));
		}
		
		/**********************************************************
		 * 地区マスタダウンロード及びSQLiteへのインサート
		 * @author 12.08.30 SE-354
		 **********************************************************/ 
		public function cdsW_LM009(nextfunc: Function=null): void
		{
			// 変数の初期化
			token = null;
			
			var srv: SRV = new SRV("sitedata");
			
			// データの取得
			token = srv.srv.cdsW_LM009(w_htid);
			
			// 実行結果より、戻り値を取得
			token.addResponder(new AsyncResponder(
				
				// 成功時の処理
				function(e:ResultEvent, obj:Object=null):void
				{
					var w_arr: ArrayCollection;
					var i: int;
					
					// 一旦、ワーク配列にセット
					w_arr = new ArrayCollection(); 
					w_arr = e.result;
					
					
					stmt = new SQLStatement();
					//stmt.sqlConnection =  connection;
					stmt.sqlConnection =  singleton.sqlconnection;
					
					// トランザクション開始
					stmt.sqlConnection.begin();
					
					//insert前に削除
					stmt.text= singleton.g_query_xml.entry.(@key == "delLM009");
					stmt.execute();
					
					// コミット
					stmt.sqlConnection.commit();
					
					// トランザクション開始
					stmt.sqlConnection.begin();
					
					stmt.text= singleton.g_query_xml.entry.(@key == "insLM009");
					
					for (i=0; i<w_arr.length; i++) {
						
						stmt.parameters[0] = w_arr[i].AREA;
						stmt.parameters[1] = w_arr[i].AREANM;
						
						stmt.execute();
					}
					
					// コミット
					stmt.sqlConnection.commit();
					
					// 次関数を実行
					BasedMethod.execNextFunction(nextfunc);
				},
				
				// 失敗時の処理
				srv.TokenFaultEvent
			));
		}
		
		/**********************************************************
		 * 得意先マスタダウンロード及びSQLiteへのインサート
		 * @author 12.08.30 SE-354
		 **********************************************************/ 
		public function cdsW_LM010(nextfunc: Function=null): void
		{
			// 変数の初期化
			token = null;
			
			var srv: SRV = new SRV("sitedata");
			
			// データの取得
			token = srv.srv.cdsW_LM010(w_htchgcd);
			
			// 実行結果より、戻り値を取得
			token.addResponder(new AsyncResponder(
				
				// 成功時の処理
				function(e:ResultEvent, obj:Object=null):void
				{
					var w_arr: ArrayCollection;
					var i: int;
					
					// 一旦、ワーク配列にセット
					w_arr = new ArrayCollection(); 
					w_arr = e.result;
					
					
					stmt = new SQLStatement();
					//stmt.sqlConnection =  connection;
					stmt.sqlConnection =  singleton.sqlconnection;
					
					// トランザクション開始
					stmt.sqlConnection.begin();
					
					stmt.text= singleton.g_query_xml.entry.(@key == "insLM010");
					
					for (i=0; i<w_arr.length; i++) {
						
						stmt.parameters[0] = w_arr[i].TKCD;
						stmt.parameters[1] = w_arr[i].TKNM;
						stmt.parameters[2] = w_arr[i].TKKNM;
						stmt.parameters[3] = w_arr[i].TKNMS;
						stmt.parameters[4] = w_arr[i].TKZIP;
						stmt.parameters[5] = w_arr[i].TKADD1;
						stmt.parameters[6] = w_arr[i].TKADD2;
						stmt.parameters[7] = w_arr[i].TKADD3;
						stmt.parameters[8] = w_arr[i].TKTEL;
						stmt.parameters[9] = w_arr[i].TKMTEL;
						stmt.parameters[10] = w_arr[i].TKFAX;
						stmt.parameters[11] = w_arr[i].TKMAIL1;
						stmt.parameters[12] = w_arr[i].TKAREA;
						stmt.parameters[13] = w_arr[i].TKCHGCD;
						stmt.parameters[14] = w_arr[i].MEMO;
						stmt.parameters[15] = w_arr[i].BOXINF;
						stmt.parameters[16] = w_arr[i].HSPAN;
						stmt.parameters[17] = w_arr[i].LHDAT;
						stmt.parameters[18] = w_arr[i].LHTIME;
						stmt.parameters[19] = w_arr[i].KBN1;
						stmt.parameters[20] = w_arr[i].KBN2;
						stmt.parameters[21] = w_arr[i].KBN3;
						stmt.parameters[22] = w_arr[i].KBN4;
						stmt.parameters[23] = w_arr[i].KBN5;
						stmt.parameters[24] = w_arr[i].KBN6;
						stmt.parameters[25] = w_arr[i].KBN7;
						stmt.parameters[26] = w_arr[i].KBN8;
						stmt.parameters[27] = w_arr[i].HMEDIYAMT;
						stmt.parameters[28] = w_arr[i].HMEDIZAMT;
						stmt.parameters[29] = w_arr[i].HMEDIKURI;
						stmt.parameters[30] = w_arr[i].HMEDIAMT;
						stmt.parameters[31] = w_arr[i].HMEDITAX;
						stmt.parameters[32] = w_arr[i].HMEDINY;
						stmt.parameters[33] = w_arr[i].HMEDIDIS;
						stmt.parameters[34] = w_arr[i].HMEDIKAS;
						stmt.parameters[35] = w_arr[i].HMEDIRTAX;
						stmt.parameters[36] = w_arr[i].HSTOREYAMT;
						stmt.parameters[37] = w_arr[i].HSTOREZAMT;
						stmt.parameters[38] = w_arr[i].HSTOREKURI;
						stmt.parameters[39] = w_arr[i].HSTOREAMT;
						stmt.parameters[40] = w_arr[i].HSTORETAX;
						stmt.parameters[41] = w_arr[i].HSTORENY;
						stmt.parameters[42] = w_arr[i].HSTOREDIS;
						stmt.parameters[43] = w_arr[i].HSTOREKAS;
						stmt.parameters[44] = w_arr[i].HSTORERTAX;
						stmt.parameters[45] = w_arr[i].HDEVYAMT;
						stmt.parameters[46] = w_arr[i].HDEVZAMT;
						stmt.parameters[47] = w_arr[i].HDEVKURI;
						stmt.parameters[48] = w_arr[i].HDEVAMT;
						stmt.parameters[49] = w_arr[i].HDEVTAX;
						stmt.parameters[50] = w_arr[i].HDEVNY;
						stmt.parameters[51] = w_arr[i].HDEVDIS;
						stmt.parameters[52] = w_arr[i].HDEVKAS;
						stmt.parameters[53] = w_arr[i].HDEVRTAX;
						stmt.parameters[54] = w_arr[i].GMEDIYAMT;
						stmt.parameters[55] = w_arr[i].GMEDIZAMT;
						stmt.parameters[56] = w_arr[i].GMEDIKURI;
						stmt.parameters[57] = w_arr[i].GMEDIAMT;
						stmt.parameters[58] = w_arr[i].GMEDITAX;
						stmt.parameters[59] = w_arr[i].GMEDINY;
						stmt.parameters[60] = w_arr[i].GMEDIDIS;
						stmt.parameters[61] = w_arr[i].GMEDIKAS;
						stmt.parameters[62] = w_arr[i].GMEDIRTAX;
						stmt.parameters[63] = w_arr[i].GSTOREYAMT;
						stmt.parameters[64] = w_arr[i].GSTOREZAMT;
						stmt.parameters[65] = w_arr[i].GSTOREKURI;
						stmt.parameters[66] = w_arr[i].GSTOREAMT;
						stmt.parameters[67] = w_arr[i].GSTORETAX;
						stmt.parameters[68] = w_arr[i].GSTORENY;
						stmt.parameters[69] = w_arr[i].GSTOREDIS;
						stmt.parameters[70] = w_arr[i].GSTOREKAS;
						stmt.parameters[71] = w_arr[i].GSTORERTAX;
						stmt.parameters[72] = w_arr[i].GDEVYAMT;
						stmt.parameters[73] = w_arr[i].GDEVZAMT;
						stmt.parameters[74] = w_arr[i].GDEVKURI;
						stmt.parameters[75] = w_arr[i].GDEVAMT;
						stmt.parameters[76] = w_arr[i].GDEVTAX;
						stmt.parameters[77] = w_arr[i].GDEVNY;
						stmt.parameters[78] = w_arr[i].GDEVDIS;
						stmt.parameters[79] = w_arr[i].GDEVKAS;
						stmt.parameters[80] = w_arr[i].GDEVRTAX;
						stmt.parameters[81] = w_arr[i].HPYM;
						stmt.parameters[82] = w_arr[i].HPW;
						stmt.parameters[83] = w_arr[i].VNO;
						stmt.parameters[84] = w_arr[i].LATEF;
						stmt.parameters[85] = w_arr[i].ENDF;
						stmt.parameters[86] = w_arr[i].EDAT;
						stmt.parameters[87] = w_arr[i].PLANF;
						stmt.parameters[88] = w_arr[i].LAT;
						stmt.parameters[89] = w_arr[i].LNG;
						stmt.parameters[90] = w_arr[i].GEOHASH;
						stmt.parameters[91] = w_arr[i].IMPCLS;
						//stmt.parameters[92] = w_arr[i].IMPCLS2;
						stmt.parameters[92] = w_arr[i].TKCLS;
						stmt.parameters[93] = w_arr[i].BIRTH;
						stmt.parameters[94] = w_arr[i].MAP;
						stmt.parameters[95] = w_arr[i].KKB;
						stmt.parameters[96] = w_arr[i].KKBCOM;
						stmt.parameters[97] = w_arr[i].HMEDIZREZ;
						stmt.parameters[98] = w_arr[i].HSTOREZREZ;
						stmt.parameters[99] = w_arr[i].HDEVZREZ;
						stmt.parameters[100] = w_arr[i].GMEDIZREZ;
						stmt.parameters[101] = w_arr[i].GSTOREZREZ;
						stmt.parameters[102] = w_arr[i].GDEVZREZ;
						stmt.parameters[103] = w_arr[i].HTCLS;
						stmt.parameters[104] = w_arr[i].DTAREA;
						stmt.parameters[105] = w_arr[i].MEDIDISRT;
						stmt.parameters[106] = w_arr[i].STOREDISRT;
						stmt.parameters[107] = w_arr[i].DEVDISRT;
						stmt.parameters[108] = w_arr[i].TKRANK;
						stmt.parameters[109] = w_arr[i].ESTAMT;
						
						stmt.execute();
					}
					// コミット
					stmt.sqlConnection.commit();
					
					// 次関数を実行
					BasedMethod.execNextFunction(nextfunc);
				},
				
				// 失敗時の処理
				srv.TokenFaultEvent
			));
		}
		
		/**********************************************************
		 * 配置薬マスタダウンロード及びSQLiteへのインサート
		 * @author 12.08.30 SE-354
		 **********************************************************/ 
		public function cdsW_LM012(nextfunc: Function=null): void
		{
			// 変数の初期化
			token = null;
			
			var srv: SRV = new SRV("sitedata");
			
			// データの取得
			token = srv.srv.cdsW_LM012(w_htchgcd);
			
			// 実行結果より、戻り値を取得
			token.addResponder(new AsyncResponder(
				
				// 成功時の処理
				function(e:ResultEvent, obj:Object=null):void
				{
					var w_arr: ArrayCollection;
					var i: int;
					var w_tkcd: String;
					
					// 一旦、ワーク配列にセット
					w_arr = new ArrayCollection(); 
					w_arr = e.result;
					
					
					stmt = new SQLStatement();
					//stmt.sqlConnection =  connection;
					stmt.sqlConnection =  singleton.sqlconnection;
					
					// トランザクション開始
					stmt.sqlConnection.begin();
					
					w_tkcd = "";
					for (i=0; i<w_arr.length; i++) {
						if(w_tkcd != w_arr[i].TKCD){
							w_tkcd = w_arr[i].TKCD;
							//insert前に削除
							stmt.text= singleton.g_query_xml.entry.(@key == "delLM0121");
							stmt.parameters[0] = w_arr[i].TKCD;
							stmt.execute();
						}
					}			
					// コミット
					stmt.sqlConnection.commit();
					
					// トランザクション開始
					stmt.sqlConnection.begin();
					
					stmt.text= singleton.g_query_xml.entry.(@key == "insLM012");
					
					for (i=0; i<w_arr.length; i++) {
						
						stmt.parameters[0] = w_arr[i].TKCD;
						stmt.parameters[1] = w_arr[i].VJAN;
						stmt.parameters[2] = w_arr[i].VPNO;
						stmt.parameters[3] = w_arr[i].IRRADD;
						stmt.parameters[4] = w_arr[i].BHISK;
						
						stmt.execute();
					}
					
					// コミット
					stmt.sqlConnection.commit();
					
					// 次関数を実行
					BasedMethod.execNextFunction(nextfunc);
				},
				
				// 失敗時の処理
				srv.TokenFaultEvent
			));
		}
		
		/**********************************************************
		 * 商品分類マスタダウンロード及びSQLiteへのインサート
		 * @author 12.08.30 SE-354
		 **********************************************************/ 
		public function cdsW_LM013(nextfunc: Function=null): void
		{
			// 変数の初期化
			token = null;
			
			var srv: SRV = new SRV("sitedata");
			
			// データの取得
			token = srv.srv.cdsW_LM013(w_htid);
			
			// 実行結果より、戻り値を取得
			token.addResponder(new AsyncResponder(
				
				// 成功時の処理
				function(e:ResultEvent, obj:Object=null):void
				{
					var w_arr: ArrayCollection;
					var i: int;
					
					// 一旦、ワーク配列にセット
					w_arr = new ArrayCollection(); 
					w_arr = e.result;
					
					
					stmt = new SQLStatement();
					//stmt.sqlConnection =  connection;
					stmt.sqlConnection =  singleton.sqlconnection;
					
					// トランザクション開始
					stmt.sqlConnection.begin();
					
					//insert前に削除
					stmt.text= singleton.g_query_xml.entry.(@key == "delLM013");
					stmt.execute();
					
					// コミット
					stmt.sqlConnection.commit();
					
					// トランザクション開始
					stmt.sqlConnection.begin();
					
					stmt.text= singleton.g_query_xml.entry.(@key == "insLM013");
					
					for (i=0; i<w_arr.length; i++) {
						
						stmt.parameters[0] = w_arr[i].GDCLS;
						stmt.parameters[1] = w_arr[i].GDCLSNM;
						
						stmt.execute();
					}
					
					// コミット
					stmt.sqlConnection.commit();
					
					// 次関数を実行
					BasedMethod.execNextFunction(nextfunc);
				},
				
				// 失敗時の処理
				srv.TokenFaultEvent
			));
		}
		
		/**********************************************************
		 * 医薬分類マスタダウンロード及びSQLiteへのインサート
		 * @author 12.08.30 SE-354
		 **********************************************************/ 
		public function cdsW_LM014(nextfunc: Function=null): void
		{
			// 変数の初期化
			token = null;
			
			var srv: SRV = new SRV("sitedata");
			
			// データの取得
			token = srv.srv.cdsW_LM014(w_htid);
			
			// 実行結果より、戻り値を取得
			token.addResponder(new AsyncResponder(
				
				// 成功時の処理
				function(e:ResultEvent, obj:Object=null):void
				{
					var w_arr: ArrayCollection;
					var i: int;
					
					// 一旦、ワーク配列にセット
					w_arr = new ArrayCollection(); 
					w_arr = e.result;
					
					
					stmt = new SQLStatement();
					//stmt.sqlConnection =  connection;
					stmt.sqlConnection =  singleton.sqlconnection;
					
					// トランザクション開始
					stmt.sqlConnection.begin();
					
					//insert前に削除
					stmt.text= singleton.g_query_xml.entry.(@key == "delLM014");
					stmt.execute();
					
					// コミット
					stmt.sqlConnection.commit();
					
					// トランザクション開始
					stmt.sqlConnection.begin();
					
					stmt.text= singleton.g_query_xml.entry.(@key == "insLM014");
					
					for (i=0; i<w_arr.length; i++) {
						
						stmt.parameters[0] = w_arr[i].MDCLS;
						stmt.parameters[1] = w_arr[i].MDCLSNM;
						stmt.parameters[2] = w_arr[i].MDCLSNMS;
						
						stmt.execute();
					}
					
					// コミット
					stmt.sqlConnection.commit();
					
					// 次関数を実行
					BasedMethod.execNextFunction(nextfunc);
				},
				
				// 失敗時の処理
				srv.TokenFaultEvent
			));
		}
		
		/**********************************************************
		 * 重点区分マスタダウンロード及びSQLiteへのインサート
		 * @author 12.11.01 SE-300
		 **********************************************************/ 
		public function cdsW_LM015(nextfunc: Function=null): void
		{
			// 変数の初期化
			token = null;
			
			var srv: SRV = new SRV("sitedata");
			
			// データの取得
			token = srv.srv.cdsW_LM015(w_htid);
			
			// 実行結果より、戻り値を取得
			token.addResponder(new AsyncResponder(
				
				// 成功時の処理
				function(e:ResultEvent, obj:Object=null):void
				{
					var w_arr: ArrayCollection;
					var i: int;
					
					// 一旦、ワーク配列にセット
					w_arr = new ArrayCollection(); 
					w_arr = e.result;
					
					
					stmt = new SQLStatement();
					//stmt.sqlConnection =  connection;
					stmt.sqlConnection =  singleton.sqlconnection;
					
					// トランザクション開始
					stmt.sqlConnection.begin();
					
					//insert前に削除
					stmt.text= singleton.g_query_xml.entry.(@key == "delLM015");
					stmt.execute();
					
					// コミット
					stmt.sqlConnection.commit();
					
					// トランザクション開始
					stmt.sqlConnection.begin();
					
					stmt.text= singleton.g_query_xml.entry.(@key == "insLM015");
					
					for (i=0; i<w_arr.length; i++) {
						
						stmt.parameters[0] = w_arr[i].IMPCLS;
						stmt.parameters[1] = w_arr[i].IMPCLSNM;
						
						stmt.execute();
					}
					
					// コミット
					stmt.sqlConnection.commit();
					
					// 次関数を実行
					BasedMethod.execNextFunction(nextfunc);
				},
				
				// 失敗時の処理
				srv.TokenFaultEvent
			));
		}
		
		/**********************************************************
		 * 地区詳細マスタダウンロード及びSQLiteへのインサート
		 * @author 12.11.01 SE-300
		 **********************************************************/ 
		public function cdsW_LM016(nextfunc: Function=null): void
		{
			// 変数の初期化
			token = null;
			
			var srv: SRV = new SRV("sitedata");
			
			// データの取得
			token = srv.srv.cdsW_LM016(w_htid);
			
			// 実行結果より、戻り値を取得
			token.addResponder(new AsyncResponder(
				
				// 成功時の処理
				function(e:ResultEvent, obj:Object=null):void
				{
					var w_arr: ArrayCollection;
					var i: int;
					
					// 一旦、ワーク配列にセット
					w_arr = new ArrayCollection(); 
					w_arr = e.result;
					
					
					stmt = new SQLStatement();
					//stmt.sqlConnection =  connection;
					stmt.sqlConnection =  singleton.sqlconnection;
					
					// トランザクション開始
					stmt.sqlConnection.begin();
					
					//insert前に削除
					stmt.text= singleton.g_query_xml.entry.(@key == "delLM016");
					stmt.execute();
					
					// コミット
					stmt.sqlConnection.commit();
					
					// トランザクション開始
					stmt.sqlConnection.begin();
					
					stmt.text= singleton.g_query_xml.entry.(@key == "insLM016");
					
					for (i=0; i<w_arr.length; i++) {
						
						stmt.parameters[0] = w_arr[i].AREA;
						stmt.parameters[1] = w_arr[i].DTAREA;
						stmt.parameters[2] = w_arr[i].DTAREANM;
						
						stmt.execute();
					}
					
					// コミット
					stmt.sqlConnection.commit();
					
					// 次関数を実行
					BasedMethod.execNextFunction(nextfunc);
				},
				
				// 失敗時の処理
				srv.TokenFaultEvent
			));
		}
		
		/**********************************************************
		 * 訪問時間マスタダウンロード及びSQLiteへのインサート
		 * @author 12.11.12 SE-300
		 **********************************************************/ 
		public function cdsW_LM017(nextfunc: Function=null): void
		{
			// 変数の初期化
			token = null;
			
			var srv: SRV = new SRV("sitedata");
			
			// データの取得
			token = srv.srv.cdsW_LM017(w_htid);
			
			// 実行結果より、戻り値を取得
			token.addResponder(new AsyncResponder(
				
				// 成功時の処理
				function(e:ResultEvent, obj:Object=null):void
				{
					var w_arr: ArrayCollection;
					var i: int;
					
					// 一旦、ワーク配列にセット
					w_arr = new ArrayCollection(); 
					w_arr = e.result;
					
					stmt = new SQLStatement();
					//stmt.sqlConnection =  connection;
					stmt.sqlConnection =  singleton.sqlconnection;
					
					// トランザクション開始
					stmt.sqlConnection.begin();
					
					//insert前に削除
					stmt.text= singleton.g_query_xml.entry.(@key == "delLM017");
					stmt.execute();
					
					// コミット
					stmt.sqlConnection.commit();
					
					// トランザクション開始
					stmt.sqlConnection.begin();
					
					stmt.text= singleton.g_query_xml.entry.(@key == "insLM017");
					
					for (i=0; i<w_arr.length; i++) {
						
						stmt.parameters[0] = w_arr[i].HTIMECD;
						stmt.parameters[1] = w_arr[i].HTIMENM;
						
						stmt.execute();
					}
					
					// コミット
					stmt.sqlConnection.commit();
					
					// 次関数を実行
					BasedMethod.execNextFunction(nextfunc);
				},
				
				// 失敗時の処理
				srv.TokenFaultEvent
			));
		}
		
		/**********************************************************
		 * 得意先区分マスタダウンロード及びSQLiteへのインサート
		 * @author 12.11.12 SE-300
		 **********************************************************/ 
		public function cdsW_LM018(nextfunc: Function=null): void
		{
			// 変数の初期化
			token = null;
			
			var srv: SRV = new SRV("sitedata");
			
			// データの取得
			token = srv.srv.cdsW_LM018(w_htid);
			
			// 実行結果より、戻り値を取得
			token.addResponder(new AsyncResponder(
				
				// 成功時の処理
				function(e:ResultEvent, obj:Object=null):void
				{
					var w_arr: ArrayCollection;
					var i: int;
					
					// 一旦、ワーク配列にセット
					w_arr = new ArrayCollection(); 
					w_arr = e.result;
					
					stmt = new SQLStatement();
					//stmt.sqlConnection =  connection;
					stmt.sqlConnection =  singleton.sqlconnection;
					
					// トランザクション開始
					stmt.sqlConnection.begin();
					
					//insert前に削除
					stmt.text= singleton.g_query_xml.entry.(@key == "delLM018");
					stmt.execute();
					
					// コミット
					stmt.sqlConnection.commit();
					
					// トランザクション開始
					stmt.sqlConnection.begin();
					
					stmt.text= singleton.g_query_xml.entry.(@key == "insLM018");
					
					for (i=0; i<w_arr.length; i++) {
						
						stmt.parameters[0] = w_arr[i].TKCLS;
						stmt.parameters[1] = w_arr[i].TKCLSNM;
						
						stmt.execute();
					}
					
					// コミット
					stmt.sqlConnection.commit();
					
					// 次関数を実行
					BasedMethod.execNextFunction(nextfunc);
				},
				
				// 失敗時の処理
				srv.TokenFaultEvent
			));
		}
		
		/**********************************************************
		 * 配置薬販売履歴FAダウンロード及びSQLiteへのインサート
		 * @author 12.08.30 SE-354
		 **********************************************************/ 
		public function cdsW_LF001(nextfunc: Function=null): void
		{
			// 変数の初期化
			token = null;
			
			var srv: SRV = new SRV("sitedata");
			
			// データの取得
			token = srv.srv.cdsW_LF001(w_htchgcd,w_htchgcd,"0","0");
			
			// 実行結果より、戻り値を取得
			token.addResponder(new AsyncResponder(
				
				// 成功時の処理
				function(e:ResultEvent, obj:Object=null):void
				{
					var w_arr: ArrayCollection;
					var i: int;
					
					// 一旦、ワーク配列にセット
					w_arr = new ArrayCollection(); 
					w_arr = e.result;
					
					
					stmt = new SQLStatement();
					//stmt.sqlConnection =  connection;
					stmt.sqlConnection =  singleton.sqlconnection;
					
					// トランザクション開始
					stmt.sqlConnection.begin();
					
					stmt.text= singleton.g_query_xml.entry.(@key == "insLF001");
					
					for (i=0; i<w_arr.length; i++) {
						
						stmt.parameters[0] = w_arr[i].TKCD;
						stmt.parameters[1] = w_arr[i].HDAT;
						stmt.parameters[2] = w_arr[i].HTIME;
						stmt.parameters[3] = w_arr[i].WITHTAX;
						stmt.parameters[4] = w_arr[i].WITHOUTTAX;
						stmt.parameters[5] = w_arr[i].AMT;
						stmt.parameters[6] = w_arr[i].TAX;
						stmt.parameters[7] = w_arr[i].MONEY;
						stmt.parameters[8] = w_arr[i].DIS;
						stmt.parameters[9] = w_arr[i].KAS;
						stmt.parameters[10] = w_arr[i].RTAX;
						
						stmt.execute();
					}
					
					// コミット
					stmt.sqlConnection.commit();
					
					// 次関数を実行
					BasedMethod.execNextFunction(nextfunc);
				},
				
				// 失敗時の処理
				srv.TokenFaultEvent
			));
		}
		
		/**********************************************************
		 * 配置薬販売履歴FBダウンロード及びSQLiteへのインサート
		 * @author 12.08.30 SE-354
		 **********************************************************/ 
		public function cdsW_LF002(nextfunc: Function=null): void
		{
			// 変数の初期化
			token = null;
			
			var srv: SRV = new SRV("sitedata");
			
			// データの取得
			token = srv.srv.cdsW_LF002(w_htchgcd,w_htchgcd,"0","0");
			
			// 実行結果より、戻り値を取得
			token.addResponder(new AsyncResponder(
				
				// 成功時の処理
				function(e:ResultEvent, obj:Object=null):void
				{
					var w_arr: ArrayCollection;
					var i: int;
					
					// 一旦、ワーク配列にセット
					w_arr = new ArrayCollection(); 
					w_arr = e.result;
					
					
					stmt = new SQLStatement();
					//stmt.sqlConnection =  connection;
					stmt.sqlConnection =  singleton.sqlconnection;
					
					// トランザクション開始
					stmt.sqlConnection.begin();
					
					stmt.text= singleton.g_query_xml.entry.(@key == "insLF002");
					
					for (i=0; i<w_arr.length; i++) {
						
						stmt.parameters[0] = w_arr[i].TKCD;
						stmt.parameters[1] = w_arr[i].HDAT;
						stmt.parameters[2] = w_arr[i].HTIME;
						stmt.parameters[3] = w_arr[i].VJAN;
						stmt.parameters[4] = w_arr[i].GDNM;
						stmt.parameters[5] = w_arr[i].VPNO;
						
						stmt.execute();
					}
					
					// コミット
					stmt.sqlConnection.commit();
					
					// 次関数を実行
					BasedMethod.execNextFunction(nextfunc);
				},
				
				// 失敗時の処理
				srv.TokenFaultEvent
			));
		}
		
		/**********************************************************
		 * 配置薬販売履歴FBダウンロード及びSQLiteへのインサート
		 * @author 12.08.30 SE-354
		 **********************************************************/ 
		public function cdsW_LF003(nextfunc: Function=null): void
		{
			// 変数の初期化
			token = null;
			
			var srv: SRV = new SRV("sitedata");
			
			// データの取得
			token = srv.srv.cdsW_LF003(w_htchgcd,w_htchgcd,"0","0");
			
			// 実行結果より、戻り値を取得
			token.addResponder(new AsyncResponder(
				
				// 成功時の処理
				function(e:ResultEvent, obj:Object=null):void
				{
					var w_arr: ArrayCollection;
					var i: int;
					
					// 一旦、ワーク配列にセット
					w_arr = new ArrayCollection(); 
					w_arr = e.result;
					
					
					stmt = new SQLStatement();
					//stmt.sqlConnection =  connection;
					stmt.sqlConnection =  singleton.sqlconnection;
					
					// トランザクション開始
					stmt.sqlConnection.begin();
					
					stmt.text= singleton.g_query_xml.entry.(@key == "insLF003");
					
					for (i=0; i<w_arr.length; i++) {
						
						stmt.parameters[0] = w_arr[i].TKCD;
						stmt.parameters[1] = w_arr[i].HDAT;
						stmt.parameters[2] = w_arr[i].HTIME;
						stmt.parameters[3] = w_arr[i].VJAN;
						stmt.parameters[4] = w_arr[i].GDNM;
						stmt.parameters[5] = w_arr[i].VPNO;
						stmt.parameters[6] = w_arr[i].AMT;
						stmt.parameters[7] = w_arr[i].DISC;
						stmt.parameters[8] = w_arr[i].SAMT;
						stmt.parameters[9] = w_arr[i].NAMT;
						stmt.parameters[10] = w_arr[i].NKAMT;
						stmt.parameters[11] = w_arr[i].BAL;
						stmt.parameters[12] = w_arr[i].VLIN; 
						
						stmt.execute();
					}
					
					// コミット
					stmt.sqlConnection.commit();
					
					// 次関数を実行
					BasedMethod.execNextFunction(nextfunc);
				},
				
				// 失敗時の処理
				srv.TokenFaultEvent
			));
		}
		
		/**********************************************************
		 * サービス品提供履歴FBダウンロード及びSQLiteへのインサート
		 * @author 12.11.12 SE-300
		 **********************************************************/ 
		public function cdsW_LF004(nextfunc: Function=null): void
		{
			// 変数の初期化
			token = null;
			
			var srv: SRV = new SRV("sitedata");
			
			// データの取得
			token = srv.srv.cdsW_LF004(w_htchgcd,w_htchgcd,"0");
			
			// 実行結果より、戻り値を取得
			token.addResponder(new AsyncResponder(
				
				// 成功時の処理
				function(e:ResultEvent, obj:Object=null):void
				{
					var w_arr: ArrayCollection;
					var i: int;
					
					// 一旦、ワーク配列にセット
					w_arr = new ArrayCollection(); 
					w_arr = e.result;
					
					
					stmt = new SQLStatement();
					//stmt.sqlConnection =  connection;
					stmt.sqlConnection =  singleton.sqlconnection;
					
					// トランザクション開始
					stmt.sqlConnection.begin();
					
					stmt.text= singleton.g_query_xml.entry.(@key == "insLF004");
					
					for (i=0; i<w_arr.length; i++) {
						
						stmt.parameters[0] = w_arr[i].TKCD;
						stmt.parameters[1] = w_arr[i].HDAT;
						stmt.parameters[2] = w_arr[i].HTIME;
						stmt.parameters[3] = w_arr[i].VJAN;
						stmt.parameters[4] = w_arr[i].GDNM;
						stmt.parameters[5] = w_arr[i].VPNO;
						
						stmt.execute();
					}
					
					// コミット
					stmt.sqlConnection.commit();
					
					// 次関数を実行
					BasedMethod.execNextFunction(nextfunc);
				},
				
				// 失敗時の処理
				srv.TokenFaultEvent
			));
		}
		
		/**********************************************************
		 * 配置薬販売履歴FBダウンロード及びSQLiteへのインサート
		 * @author 12.08.30 SE-354
		 **********************************************************/ 
		public function cdsW_LF010(nextfunc: Function=null): void
		{
			// 変数の初期化
			token = null;
			
			var srv: SRV = new SRV("sitedata");
			
			// データの取得
			token = srv.srv.cdsW_LF010(w_htid,w_htchgcd,"2", g_updateCount.toString(), arrSEIGYO[0].SDDAY);
			
			// 実行結果より、戻り値を取得
			token.addResponder(new AsyncResponder(
				
				// 成功時の処理
				function(e:ResultEvent, obj:Object=null):void
				{
					var w_arr: ArrayCollection;
					var i: int;
					
					// 一旦、ワーク配列にセット
					w_arr = new ArrayCollection(); 
					w_arr = e.result;
					
					
					stmt = new SQLStatement();
					//stmt.sqlConnection =  connection;
					stmt.sqlConnection =  singleton.sqlconnection;
					
					// トランザクション開始
					stmt.sqlConnection.begin();
					
					stmt.text= singleton.g_query_xml.entry.(@key == "insLF010");
					
					for (i=0; i<w_arr.length; i++) {
			
						// 当日以降分のみ対象
						//if(w_arr[i].HDAT > arrSEIGYO[0].SDDAY) {

							stmt.parameters[0] = w_arr[i].CHGCD;
							stmt.parameters[1] = w_arr[i].HDAT;
							stmt.parameters[2] = w_arr[i].TKCD;
							stmt.parameters[3] = w_arr[i].ABSENT;
							stmt.parameters[4] = w_arr[i].HHTIME;
							stmt.parameters[5] = w_arr[i].BHTIME;
							stmt.parameters[6] = w_arr[i].NHTIME;
							stmt.parameters[7] = w_arr[i].COM;
							stmt.parameters[8] = w_arr[i].ABTIME1;
							stmt.parameters[9] = w_arr[i].ABTIME2;
							stmt.parameters[10] = w_arr[i].ABTIME3;
							stmt.parameters[11] = w_arr[i].UPDFLG;

							stmt.execute();
						//}
					}
					
					// コミット
					stmt.sqlConnection.commit();
					
					// 次関数を実行
					BasedMethod.execNextFunction(nextfunc);
				},
				
				// 失敗時の処理
				srv.TokenFaultEvent
			));
		}
		
		/**********************************************************
		 * 配置薬履歴(照会用)Fダウンロード及びSQLiteへのインサート
		 * @author 12.10.30 SE-300
		 **********************************************************/ 
		public function cdsW_LF012(nextfunc: Function=null): void
		{
			// 変数の初期化
			token = null;
			
			var srv: SRV = new SRV("sitedata");
			
			// データの取得
			token = srv.srv.cdsW_LF012(w_htid,w_htchgcd,"0");
			
			// 実行結果より、戻り値を取得
			token.addResponder(new AsyncResponder(
				
				// 成功時の処理
				function(e:ResultEvent, obj:Object=null):void
				{
					var w_arr: ArrayCollection;
					var i: int;
					var w_tkcd: String;
					
					// 一旦、ワーク配列にセット
					w_arr = new ArrayCollection(); 
					w_arr = e.result;
					
					stmt = new SQLStatement();
					//stmt.sqlConnection =  connection;
					stmt.sqlConnection =  singleton.sqlconnection;
					
					// トランザクション開始
					stmt.sqlConnection.begin();
					
					w_tkcd = "";
					for (i=0; i<w_arr.length; i++) {
						if(w_tkcd != w_arr[i].TKCD){
							w_tkcd = w_arr[i].TKCD;
							//insert前に削除
							stmt.text= singleton.g_query_xml.entry.(@key == "delLF0121");
							stmt.parameters[0] = w_arr[i].TKCD;
							stmt.execute();
						}
					}			
					// コミット
					stmt.sqlConnection.commit();
					
					// トランザクション開始
					stmt.sqlConnection.begin();
					
					stmt.text= singleton.g_query_xml.entry.(@key == "insLF012");
					
					for (i=0; i<w_arr.length; i++) {
						
						stmt.parameters[0] = w_arr[i].TKCD;
						stmt.parameters[1] = w_arr[i].LINNO;
						stmt.parameters[2] = w_arr[i].TITNM;
						stmt.parameters[3] = w_arr[i].HBAYI01;
						stmt.parameters[4] = w_arr[i].HBAYI02;
						stmt.parameters[5] = w_arr[i].HBAYI03;
						stmt.parameters[6] = w_arr[i].HBAYI04;
						stmt.parameters[7] = w_arr[i].HBAYI05;
						stmt.parameters[8] = w_arr[i].HBAYI06;
						stmt.parameters[9] = w_arr[i].HBAYI07;
						stmt.parameters[10] = w_arr[i].HBAYI08;
						stmt.parameters[11] = w_arr[i].HBAYI09;
						stmt.parameters[12] = w_arr[i].HBAYI10;
						stmt.parameters[13] = w_arr[i].HBAYI11;
						stmt.parameters[14] = w_arr[i].HBAYI12;
						stmt.parameters[15] = w_arr[i].HBAYI13;
						stmt.parameters[16] = w_arr[i].HBAYI14;
						stmt.parameters[17] = w_arr[i].HBAYI15;
						stmt.parameters[18] = w_arr[i].HBAYI16;
						stmt.parameters[19] = w_arr[i].HBAYI17;
						stmt.parameters[20] = w_arr[i].HBAYI18;
						stmt.parameters[21] = w_arr[i].HBAYI19;
						stmt.parameters[22] = w_arr[i].HBAYI20;
						stmt.parameters[23] = w_arr[i].HBAYI21;
						stmt.parameters[24] = w_arr[i].HBAYI22;
						stmt.parameters[25] = w_arr[i].HBAYI23;
						stmt.parameters[26] = w_arr[i].HBAYI24;
						stmt.parameters[27] = w_arr[i].HBAYI25;
						stmt.parameters[28] = w_arr[i].HBAYI26;
						stmt.parameters[29] = w_arr[i].HBAYI27;
						stmt.parameters[30] = w_arr[i].HBAYI28;
						stmt.parameters[31] = w_arr[i].HBAYI29;
						stmt.parameters[32] = w_arr[i].HBAYI30;
						
						stmt.execute();
					}
					
					// コミット
					stmt.sqlConnection.commit();
					
					// 次関数を実行
					BasedMethod.execNextFunction(nextfunc);
				},
				
				// 失敗時の処理
				srv.TokenFaultEvent
			));
		}
		
		protected function stmtErrorHandler(event:SQLErrorEvent):void
		{
			// エラーの表示
			trace("SQLite処理でエラー");
		}
		
/*
		public function updateUB910X(): void
		{
			stmt = new SQLStatement();
			//stmt.sqlConnection =  connection;
			stmt.sqlConnection =  singleton.sqlconnection;
			//stmt.itemClass = LS001;
			
			// 修正時はUPDATE
			if (isNew == false)
			{ 
				stmt.text= singleton.g_query_xml.entry.(@key == "updUB910X");
				
				stmt.parameters[0] = view.ls001.HTID;
				//stmt.parameters[2] = view.ls001.CHGCD;
				stmt.parameters[1] = view.lcmbCHGNM.selectedItem.CHGCD;
				//stmt.parameters[2] = view.ls001.SDDAY;
				stmt.parameters[2] = view.edtSDDAY.text;
				stmt.parameters[3] = view.ls001.HVNO;
				stmt.parameters[4] = view.ls001.BVNO;
				stmt.parameters[5] = view.ls001.NVNO;
			}
			// 新規時はINSERT
			else
			{
				stmt.text= singleton.g_query_xml.entry.(@key == "insUB910X");
				
				stmt.parameters[0] = "0";
				stmt.parameters[1] = view.ls001.HTID;
				//stmt.parameters[2] = view.ls001.CHGCD;
				stmt.parameters[2] = view.lcmbCHGNM.selectedItem.CHGCD;
				//stmt.parameters[3] = view.ls001.SDDAY;
				stmt.parameters[3] = view.edtSDDAY.text;
				stmt.parameters[4] = view.ls001.HVNO;
				stmt.parameters[5] = view.ls001.BVNO;
				stmt.parameters[6] = view.ls001.NVNO;
			}
			
			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, updateUB910XResult);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);
			
			//実行
			stmt.execute();
		}
*/		
		private function updateUB910XResult(event:SQLEvent):void 
		{
			super.updateDataSet_H();
		}
	}
}