package views
{
	
	import flash.data.SQLResult;
	import flash.data.SQLStatement;
	import flash.events.SQLErrorEvent;
	import flash.events.SQLEvent;
	
	import modules.base.BasedMethod;
	import modules.base.SRV;
	import modules.custom.CustomIterator;
	import modules.custom.sub.Iterator;
	import modules.dto.LM007;
	import modules.dto.LS001;
	import modules.dto.LS006;
	import modules.dto.LJ001;
	import modules.dto.LJ002;
	import modules.dto.LJ003;
	import modules.dto.LJ004;
	import modules.dto.LJ005;
	import modules.dto.LJ012;
	import modules.sbase.SiteModule;
	
	import mx.collections.ArrayCollection;
	import mx.rpc.AsyncResponder;
	import mx.rpc.events.ResultEvent;
	
	public class MH820XController extends SiteModule
	{
		private var view:MH820X;
		//private var arrUB910X: ArrayCollection;
		private var arrCHGCD: ArrayCollection;
		private var stmt: SQLStatement;
		private var g_updateCount: int;
		
		private var w_htid: String;
		private var w_chgcd: String;
		private var w_htchgcd: String;
		private var w_dowdat: String;
		private var w_dowtim: String;
		
		private var w_sysdate: String;
		private var w_systime: String;
		
		//public var arrS008: ArrayCollection;
		
		private var isNew: Boolean;

		private var arrLJ001: ArrayCollection;
		private var arrLJ002: ArrayCollection;
		private var arrLJ003: ArrayCollection;
		private var arrLJ004: ArrayCollection;
		private var arrLJ005: ArrayCollection;
		private var arrLJ012: ArrayCollection;

		private var cdsLJ001_arg: Object;
		private var cdsLJ002_arg: Object;
		private var cdsLJ003_arg: Object;
		private var cdsLJ004_arg: Object;
		private var cdsLJ005_arg: Object;
		private var cdsLJ012_arg: Object;
		
		public function MH820XController()
		{
			super();
		}
		
		override public function init():void
		{
			
			// フォーム宣言
			view = _view as MH820X;

			// 変数の設定
			//g_prgid     = "MENUX";
			super.init();
		}
		
		override public function formShow_last(): void
		{
			cdsSEIGYO(formShow_last_cld1);
		}
		private function formShow_last_cld1(): void
		{
			cdsLJ001(formShow_last_cld2);
		}
		private function formShow_last_cld2(): void
		{
			cdsLJ002(formShow_last_cld3);
		}
		private function formShow_last_cld3(): void
		{
			cdsLJ003(formShow_last_cld4);
		}
		private function formShow_last_cld4(): void
		{
			cdsLJ004(formShow_last_cld5);
		}
		private function formShow_last_cld5(): void
		{
			cdsLJ005(formShow_last_cld6);
		}
		private function formShow_last_cld6(): void
		{
			cdsLJ012(formShow_last_cld7);
		}
		private function formShow_last_cld7(): void
		{
			
			//cdsMA020X();
			//cdsAREA();
			if (arrSEIGYO.length != 0)
			{
				w_htid = arrSEIGYO[0].HTID;
				w_chgcd = arrSEIGYO[0].CHGCD;
				w_htchgcd = arrSEIGYO[0].HTCHGCD;
				
//				if (arrSEIGYO[0].DOWDAT != null)
//				{
					w_dowdat = arrSEIGYO[0].DOWDAT;
					w_dowtim = arrSEIGYO[0].DOWTIM;
					
					view.edtDOWDAT.text = w_dowdat;
					view.edtDOWTIM.text = w_dowtim;
//				}
//				else
//				{
//					w_dowdat = "1900/01/01";
//					w_dowtim = "00:00:00";
//				}
				
				view.edtUPDDAT.text = arrSEIGYO[0].UPDDAT;
				view.edtUPDTIM.text = arrSEIGYO[0].UPDTIM;
			}
		}
		
		/**********************************************************
		 * openDataSet_H データの取得.
		 * @author 12.08.30 SE-354
		 **********************************************************/
		override public function openDataSet_H(): void
		{
			cdsLJ001(openDataSet_H_cld1);
		}
		private function openDataSet_H_cld1(): void
		{
			cdsLJ002(openDataSet_H_cld2);
		}
		private function openDataSet_H_cld2(): void
		{
			cdsLJ003(openDataSet_H_cld3);
		}
		private function openDataSet_H_cld3(): void
		{
			cdsLJ004(openDataSet_H_cld4);
		}
		private function openDataSet_H_cld4(): void
		{
			cdsLJ005(openDataSet_H_cld5);
		}
		private function openDataSet_H_cld5(): void
		{
			cdsLJ012(super.openDataSet_H);
		}
		

		/**********************************************************
		 * setDispObject 画面項目セット.
		 * @author 12.08.30 SE-354
		 **********************************************************/
		/*
		override public function setDispObject(): void
		{
			
		}
		*/
		
		public function btnDECClick(): void
		{

//			singleton.sitemethod.dspJoinErrMsg("Q0017", btnDECClick_cld1, "", "1", null, btnDECClick_last2);
			btnDECClick_cld1();

//			var w_date:Date = new Date();
//			w_sysdate = singleton.g_customdateformatter.dateDisplayFormat_Full(w_date);
//			w_systime = singleton.g_customdateformatter.timeDisplayFormat(w_date);

//			view.btnDEC.enabled = false;
			
//			view.chkLS006.selected = false;
//			view.chkLS010.selected = false;
//			view.chkLS014.selected = false;
//			view.chkLM006.selected = false;
//			view.chkLM007.selected = false;
//			view.chkLM008.selected = false;
//			view.chkLM009.selected = false;
//			view.chkLM010.selected = false;
//			view.chkLM012.selected = false;
//			view.chkLM013.selected = false;
//			view.chkLM014.selected = false;
//			view.chkLM015.selected = false;
//			view.chkLM016.selected = false;
//			view.chkLM017.selected = false;
//			view.chkLM018.selected = false;
//			view.chkLF001.selected = false;
//			view.chkLF002.selected = false;
//			view.chkLF003.selected = false;
//			view.chkLF004.selected = false;
////			view.chkLF007.selected = false;
//			view.chkLF010.selected = false;
//			view.chkLF011.selected = false;
//			view.chkLF012.selected = false;
//			view.chkLS001.selected = false;

//			insLF013("","ダウンロード開始");
			
////			checkDataSet();
//			doF10Proc();
		}

		private function btnDECClick_cld1(): void
		{
			// 端末ＩＤ、入力担当者、端末担当者チェック
			if((w_htid == "")||(w_chgcd == "")||(w_htchgcd == "")){
				singleton.sitemethod.dspJoinErrMsg("E0615");
				return;
			}
			
			// ジャーナルにデータが存在する場合			
			if( (arrLJ001 != null && arrLJ001.length != 0)||
				(arrLJ002 != null && arrLJ002.length != 0)||
				(arrLJ003 != null && arrLJ003.length != 0)||
				(arrLJ004 != null && arrLJ004.length != 0)||
				(arrLJ005 != null && arrLJ005.length != 0)||
				(arrLJ012 != null && arrLJ012.length != 0) ){
				// 入力データが残っています。アップロードを実行して下さい。
				singleton.sitemethod.dspJoinErrMsg("W0003");
				return;
			}else{
				singleton.sitemethod.dspJoinErrMsg("Q0017", btnDECClick_last, "", "1", null, btnDECClick_last2);
			}
		}
		
		private function btnDECClick_cld2(): void
		{
			// 入力データを削除しますか？
			singleton.sitemethod.dspJoinErrMsg("T0002", btnDECClick_cld3, "", "1", null, btnDECClick_last);
		}
		
		private function btnDECClick_cld3(): void
		{
			// ジャーナル削除
			delJNL(btnDECClick_last);
		}
		
		private function btnDECClick_last(): void
		{
			var w_date:Date = new Date();
			w_sysdate = singleton.g_customdateformatter.dateDisplayFormat_Full(w_date);
			w_systime = singleton.g_customdateformatter.timeDisplayFormat(w_date);
			
			view.btnDEC.enabled = false;
			
			view.chkLS006.selected = false;
			view.chkLS010.selected = false;
			view.chkLS014.selected = false;
			view.chkLM006.selected = false;
			view.chkLM007.selected = false;
			view.chkLM008.selected = false;
			view.chkLM009.selected = false;
			view.chkLM010.selected = false;
			view.chkLM012.selected = false;
			view.chkLM013.selected = false;
			view.chkLM014.selected = false;
			view.chkLM015.selected = false;
			view.chkLM016.selected = false;
			view.chkLM017.selected = false;
			view.chkLM018.selected = false;
			view.chkLF001.selected = false;
			view.chkLF002.selected = false;
			view.chkLF003.selected = false;
			view.chkLF004.selected = false;
			view.chkLF010.selected = false;
			view.chkLF011.selected = false;
			view.chkLF012.selected = false;
			view.chkLS001.selected = false;
			
			insLF013("","ダウンロード開始");
			
			doF10Proc();
		}
		
		private function btnDECClick_last2(): void
		{
			
		}

		/**********************************************************
		 * btnRTN クリック時処理.
		 * @author 12.07.03 SE-354
		 **********************************************************/
		public function btnRTNClick(): void
		{
			// 制御マスタメンテを終了します。よろしいですか？
			//singleton.sitemethod.dspJoinErrMsg();
			view.navigator.popView();
		}
		
		public function btnRTNClick_last(): void
		{
			// メニューに戻る
			view.navigator.popView();
		}
		
		/**********************************************************
		 * プロシージャ実行
		 * @author 12.08.30 SE-354
		 **********************************************************/ 
		override public function execDecPLSQL(): void
		{
			var w_arg: ArrayCollection;

			
			cdsW_LS006(execDecPLSQL_last_cld2);
			
			
			// (引数のセット)
			// procBA140DEC={CALL BA140DEC(?,?,?,?,?,?,?,?,?,?)}

// 2013.01.10 SE-233 プロシージャを起動しない 		
//			w_arg = new ArrayCollection([
//				["in",  "string", BasedMethod.nvl(w_htid)],	// 0:P_HTID
//				//["in",  "string", BasedMethod.nvl(w_chgcd)],				// 1:P_CHGCD
//				["in",  "string", BasedMethod.nvl(w_htchgcd)],				// 1:P_CHGCD
//				["in",  "string", BasedMethod.nvl(w_dowdat)],		// 2:P_HREV
//				["in",  "string", BasedMethod.nvl(w_dowtim)],			// 3:P_HVER
//				["out", "number", ""],				// 4:W_FLG
//				["out", "string", ""],				// 5:W_MSG1
//				["out", "string", ""],				// 6:W_MSG2
//				["in", "string", "1"]				// 7:P_SCLS
//			]);
//			
//			// (実行)
//			execPLSQL("sitedata", "procMD820S", w_arg, execDecPLSQL_last);
		}
		
		public function execDecPLSQL_last(): void
		{
			var w_msg1: String;
			var w_msg2: String;
			
			var obj_arg: Object = new Object();
			
			// 正常終了で返ってきたとき
			if (arrExecPlsql[0] == 0) 
			{
				//execDecPLSQL2();
				cdsW_LS006(execDecPLSQL_last_cld2);
			}
			else
			{
				
				w_msg1  = arrExecPlsql[1];
				w_msg2  = arrExecPlsql[2];
				
				//singleton.sitemethod.dspJoinErrMsg(w_msg1, w_msg2, 3);
				
				//doF10Proc_last2();
				//trace("procerr");
				//singleton.sitemethod.dspJoinErrMsg(w_msg1, null, "エラー");
				singleton.sitemethod.dspJoinErrMsg(w_msg1, doF10Proc_last, "エラー");
				return;
			}
		}
		
		public function execDecPLSQL2(): void
		{
			var w_arg: ArrayCollection;
			
			// (引数のセット)
			// procBA140DEC={CALL BA140DEC(?,?,?,?,?,?,?,?,?,?)}
			
			w_arg = new ArrayCollection([
				["in",  "string", BasedMethod.nvl(w_htid)],	// 0:P_HTID
				//["in",  "string", BasedMethod.nvl(w_chgcd)],				// 1:P_CHGCD
				["in",  "string", BasedMethod.nvl(w_htchgcd)],				// 1:P_CHGCD
				["in",  "string", BasedMethod.nvl(w_dowdat)],		// 2:P_HREV
				["in",  "string", BasedMethod.nvl(w_dowtim)],			// 3:P_HVER
				["out", "number", ""],				// 4:W_FLG
				["out", "string", ""],				// 5:W_MSG1
				["out", "string", ""],				// 6:W_MSG2
				["in", "string", "1"]				// 7:P_SCLS
			]);
			
			// (実行)
			execPLSQL("sitedata", "procMD821S", w_arg, execDecPLSQL_last2);
		}
		
		public function execDecPLSQL_last2(): void
		{
			var w_msg1: String;
			var w_msg2: String;
			
			var obj_arg: Object = new Object();
			
			// 正常終了で返ってきたとき
			if (arrExecPlsql[0] == 0) 
			{
				cdsW_LS006(execDecPLSQL_last_cld2);
			}
			else
			{
				
				w_msg1  = arrExecPlsql[1];
				w_msg2  = arrExecPlsql[2];
				
				//singleton.sitemethod.dspJoinErrMsg(w_msg1, w_msg2, 3);
				
				//doF10Proc_last2();
				//trace("procerr");
				//singleton.sitemethod.dspJoinErrMsg(w_msg1, null, "エラー");
				singleton.sitemethod.dspJoinErrMsg(w_msg1, doF10Proc_last, "エラー");
				return;
			}
		}
		
		private function execDecPLSQL_last_cld2(): void
		{
			view.chkLS006.selected = true;
			
			cdsW_LS010(execDecPLSQL_last_cld21);
		}
		
		private function execDecPLSQL_last_cld21(): void
		{
			view.chkLS010.selected = true;
			
			cdsW_LS014(execDecPLSQL_last_cld3);
		}
		
		private function execDecPLSQL_last_cld3(): void
		{
			view.chkLS014.selected = true;
			
			cdsW_LM006(execDecPLSQL_last_cld4);
		}
		
		private function execDecPLSQL_last_cld4(): void
		{
			view.chkLM006.selected = true;
			
			cdsW_LM007(execDecPLSQL_last_cld5);
		}
		
		private function execDecPLSQL_last_cld5(): void
		{
			view.chkLM007.selected = true;
			
			cdsW_LM008(execDecPLSQL_last_cld6);
		}
		
		private function execDecPLSQL_last_cld6(): void
		{
			view.chkLM008.selected = true;
			
			cdsW_LM009(execDecPLSQL_last_cld7);
		}
		
		private function execDecPLSQL_last_cld7(): void
		{
			view.chkLM009.selected = true;
			
			cdsW_LM010(execDecPLSQL_last_cld8);
		}
		
		private function execDecPLSQL_last_cld8(): void
		{
			view.chkLM010.selected = true;
			
			cdsW_LM012(execDecPLSQL_last_cld9);
		}
		
		private function execDecPLSQL_last_cld9(): void
		{
			view.chkLM012.selected = true;
			
			cdsW_LM013(execDecPLSQL_last_cld10);
		}
		
		private function execDecPLSQL_last_cld10(): void
		{
			view.chkLM013.selected = true;
			
			cdsW_LM014(execDecPLSQL_last_cld101);
		}
		
		private function execDecPLSQL_last_cld101(): void
		{
			view.chkLM014.selected = true;
			
			cdsW_LM015(execDecPLSQL_last_cld102);
		}
		
		private function execDecPLSQL_last_cld102(): void
		{
			view.chkLM015.selected = true;
			
			cdsW_LM016(execDecPLSQL_last_cld103);
		}
		
		private function execDecPLSQL_last_cld103(): void
		{
			view.chkLM016.selected = true;
			
			cdsW_LM017(execDecPLSQL_last_cld104);
		}
		
		private function execDecPLSQL_last_cld104(): void
		{
			view.chkLM017.selected = true;
			g_updateCount = 0;
			
			cdsW_LM018(execDecPLSQL_last_cld11);
		}
		
		private function execDecPLSQL_last_cld11(): void
		{
			if (g_updateCount == 0) {
				g_updateCount = 1;
				view.chkLM018.selected = true;
			}
			cdsW_LF001(execDecPLSQL_last_cld11a);
		}
		private function execDecPLSQL_last_cld11a(): void
		{
			// カウントアップ
			g_updateCount++;
			
			if(g_updateCount > 10){
				g_updateCount = 0;
				execDecPLSQL_last_cld12();
			}else{
				execDecPLSQL_last_cld11();
			}
		}
		
		
		private function execDecPLSQL_last_cld12(): void
		{
			if (g_updateCount == 0) {
				g_updateCount = 1;
				view.chkLF001.selected = true;
			}
			cdsW_LF002(execDecPLSQL_last_cld12a);
		}
		private function execDecPLSQL_last_cld12a(): void
		{
			// カウントアップ
			g_updateCount++;
			
			if(g_updateCount > 10){
				g_updateCount = 0;
				execDecPLSQL_last_cld13();
			}else{
				execDecPLSQL_last_cld12();
			}
		}
		
		private function execDecPLSQL_last_cld13(): void
		{
			if (g_updateCount == 0) {
				g_updateCount = 1;
				view.chkLF002.selected = true;
			}
			cdsW_LF003(execDecPLSQL_last_cld13a);
		}
		private function execDecPLSQL_last_cld13a(): void
		{
			// カウントアップ
			g_updateCount++;
			
			if(g_updateCount > 10){
				execDecPLSQL_last_cld14();
			}else{
				execDecPLSQL_last_cld13();
			}
		}
		
		private function execDecPLSQL_last_cld14(): void
		{
			view.chkLF003.selected = true;
			g_updateCount = 0;
			
			cdsW_LF004(execDecPLSQL_last_cld15);
		}
		
		private function execDecPLSQL_last_cld15(): void
		{
			if (g_updateCount == 0) {
				g_updateCount = 1;
				view.chkLF004.selected = true;
			}
			cdsW_LF010(execDecPLSQL_last_cld15a);
		}
		private function execDecPLSQL_last_cld15a(): void
		{
			// カウントアップ
			g_updateCount++;
			
			if(g_updateCount > 10){
				execDecPLSQL_last_cld16();
			}else{
				execDecPLSQL_last_cld15();
			}
		}
		
		private function execDecPLSQL_last_cld16(): void
		{
			view.chkLF010.selected = true;
			
			cdsW_LF011(execDecPLSQL_last_cld17);
		}
		
		private function execDecPLSQL_last_cld17(): void
		{
			view.chkLF011.selected = true;
			
			cdsW_LF012(execDecPLSQL_last_cld18);
		}
		
		private function execDecPLSQL_last_cld18(): void
		{
			view.chkLF012.selected = true;
			
			updateLS001(execDecPLSQL_last_cld19);
		}
		
		private function execDecPLSQL_last_cld19(): void
		{
			view.chkLS001.selected = true;
			
			w_dowdat = w_sysdate;
			w_dowtim = w_systime;
			
			view.edtDOWDAT.text = w_sysdate;
			view.edtDOWTIM.text = w_systime;

			insLF013("","ダウンロード終了");
			
			super.execDecPLSQL();
		}
		
		/**********************************************************
		 * 配置薬売上JA
		 * @author 12.12.26 SE-233
		 **********************************************************/
		public function cdsLJ001(func: Function=null): void
		{
			// 引数情報
			cdsLJ001_arg = new Object;
			cdsLJ001_arg.func = func;
			
			stmt = new SQLStatement();
			stmt.sqlConnection =  singleton.sqlconnection;
			stmt.text= singleton.g_query_xml.entry.(@key == "qryLJ001");
			stmt.itemClass = LJ001;
			
			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, cdsLJ001Result, false, 0, true);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler, false, 0, true);
			
			//実行
			stmt.execute();
		}
		
		public function cdsLJ001Result(event: SQLEvent): void
		{	
			var w_arg: Object = cdsLJ001_arg;
			var result:SQLResult = stmt.getResult();
			
			stmt.removeEventListener(SQLEvent.RESULT, cdsLJ001Result);				
			stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);				
			stmt=null;
			
			arrLJ001 = new ArrayCollection(result.data);
			
			// 次関数実行
			if(w_arg.func)
				w_arg.func();
		}
		
		/**********************************************************
		 * 配置薬売上JB
		 * @author 12.12.26 SE-233
		 **********************************************************/
		public function cdsLJ002(func: Function=null): void
		{
			// 引数情報
			cdsLJ002_arg = new Object;
			cdsLJ002_arg.func = func;
			
			stmt = new SQLStatement();
			stmt.sqlConnection =  singleton.sqlconnection;
			stmt.text= singleton.g_query_xml.entry.(@key == "qryLJ002");
			stmt.itemClass = LJ002;
			
			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, cdsLJ002Result, false, 0, true);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler, false, 0, true);
			
			//実行
			stmt.execute();
		}
		
		public function cdsLJ002Result(event: SQLEvent): void
		{	
			var w_arg: Object = cdsLJ002_arg;
			var result:SQLResult = stmt.getResult();
			
			stmt.removeEventListener(SQLEvent.RESULT, cdsLJ002Result);				
			stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);				
			stmt=null;
			
			arrLJ002 = new ArrayCollection(result.data);
			
			// 次関数実行
			if(w_arg.func)
				w_arg.func();
		}
		
		/**********************************************************
		 * 別売JA
		 * @author 12.12.26 SE-233
		 **********************************************************/
		public function cdsLJ003(func: Function=null): void
		{
			// 引数情報
			cdsLJ003_arg = new Object;
			cdsLJ003_arg.func = func;
			
			stmt = new SQLStatement();
			stmt.sqlConnection =  singleton.sqlconnection;
			stmt.text= singleton.g_query_xml.entry.(@key == "qryLJ003");
			stmt.itemClass = LJ003;
			
			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, cdsLJ003Result, false, 0, true);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler, false, 0, true);
			
			//実行
			stmt.execute();
		}
		
		public function cdsLJ003Result(event: SQLEvent): void
		{	
			var w_arg: Object = cdsLJ003_arg;
			var result:SQLResult = stmt.getResult();
			
			stmt.removeEventListener(SQLEvent.RESULT, cdsLJ003Result);				
			stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);				
			stmt=null;
			
			arrLJ003 = new ArrayCollection(result.data);
			
			// 次関数実行
			if(w_arg.func)
				w_arg.func();
		}
		
		/**********************************************************
		 * 別売JB
		 * @author 12.12.26 SE-233
		 **********************************************************/
		public function cdsLJ004(func: Function=null): void
		{
			// 引数情報
			cdsLJ004_arg = new Object;
			cdsLJ004_arg.func = func;
			
			stmt = new SQLStatement();
			stmt.sqlConnection =  singleton.sqlconnection;
			stmt.text= singleton.g_query_xml.entry.(@key == "qryLJ004");
			stmt.itemClass = LJ004;
			
			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, cdsLJ004Result, false, 0, true);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler, false, 0, true);
			
			//実行
			stmt.execute();
		}
		
		public function cdsLJ004Result(event: SQLEvent): void
		{	
			var w_arg: Object = cdsLJ004_arg;
			var result:SQLResult = stmt.getResult();
			
			stmt.removeEventListener(SQLEvent.RESULT, cdsLJ004Result);				
			stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);				
			stmt=null;
			
			arrLJ004 = new ArrayCollection(result.data);
			
			// 次関数実行
			if(w_arg.func)
				w_arg.func();
		}

		/**********************************************************
		 * 入金J
		 * @author 12.12.26 SE-233
		 **********************************************************/
		public function cdsLJ005(func: Function=null): void
		{
			// 引数情報
			cdsLJ005_arg = new Object;
			cdsLJ005_arg.func = func;
			
			stmt = new SQLStatement();
			stmt.sqlConnection =  singleton.sqlconnection;
			stmt.text= singleton.g_query_xml.entry.(@key == "qryLJ005");
			stmt.itemClass = LJ005;
			
			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, cdsLJ005Result, false, 0, true);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler, false, 0, true);
			
			//実行
			stmt.execute();
		}
		
		public function cdsLJ005Result(event: SQLEvent): void
		{	
			var w_arg: Object = cdsLJ005_arg;
			var result:SQLResult = stmt.getResult();
			
			stmt.removeEventListener(SQLEvent.RESULT, cdsLJ005Result);				
			stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);				
			stmt=null;
			
			arrLJ005 = new ArrayCollection(result.data);
			
			// 次関数実行
			if(w_arg.func)
				w_arg.func();
		}
		
		/**********************************************************
		 * 提供J
		 * @author 12.12.26 SE-233
		 **********************************************************/
		public function cdsLJ012(func: Function=null): void
		{
			// 引数情報
			cdsLJ012_arg = new Object;
			cdsLJ012_arg.func = func;
			
			stmt = new SQLStatement();
			stmt.sqlConnection =  singleton.sqlconnection;
			stmt.text= singleton.g_query_xml.entry.(@key == "qryLJ012");
			stmt.itemClass = LJ012;
			
			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, cdsLJ012Result, false, 0, true);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler, false, 0, true);
			
			//実行
			stmt.execute();
		}
		
		public function cdsLJ012Result(event: SQLEvent): void
		{	
			var w_arg: Object = cdsLJ012_arg;
			var result:SQLResult = stmt.getResult();
			
			stmt.removeEventListener(SQLEvent.RESULT, cdsLJ012Result);				
			stmt.removeEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);				
			stmt=null;
			
			arrLJ012 = new ArrayCollection(result.data);
			
			// 次関数実行
			if(w_arg.func)
				w_arg.func();
		}
		
		/**********************************************************
		 * ジャーナル削除
		 * @author 12.12.26 SE-233
		 **********************************************************/
		public function delJNL(nextfunc: Function=null): void
		{
			stmt = new SQLStatement();
			stmt.sqlConnection =  singleton.sqlconnection;
			
			stmt.text= singleton.g_query_xml.entry.(@key == "delLJ001");
			
			stmt.execute();
			
			stmt.text= singleton.g_query_xml.entry.(@key == "delLJ002");
			
			stmt.execute();
			
			stmt.text= singleton.g_query_xml.entry.(@key == "delLJ003");
			
			stmt.execute();
			
			stmt.text= singleton.g_query_xml.entry.(@key == "delLJ004");
			
			stmt.execute();
			
			stmt.text= singleton.g_query_xml.entry.(@key == "delLJ005");
			
			stmt.execute();
			
			stmt.text= singleton.g_query_xml.entry.(@key == "delLJ012");
			
			stmt.execute();
			
			// 次関数を実行
			BasedMethod.execNextFunction(nextfunc);
		}
		
		/**********************************************************
		 * 税マスタのダウンロード及びSQLiteへのインサート
		 * @author 12.08.30 SE-354
		 **********************************************************/ 
		public function cdsW_LS006(nextfunc: Function=null): void
		{
			// 変数の初期化
			token = null;
			
			var srv: SRV = new SRV("sitedata");
			
			// データの取得
			token = srv.srv.cdsW_LS006(w_htid);
			
			// 実行結果より、戻り値を取得
			token.addResponder(new AsyncResponder(
				
				// 成功時の処理
				function(e:ResultEvent, obj:Object=null):void
				{
					var w_arr: ArrayCollection;
					var i: int;
					
					// 一旦、ワーク配列にセット
					w_arr = new ArrayCollection(); 
					w_arr = e.result;
					
					
					stmt = new SQLStatement();
					//stmt.sqlConnection =  connection;
					stmt.sqlConnection =  singleton.sqlconnection;
					
					// トランザクション開始
					stmt.sqlConnection.begin();
					
					//insert前に削除
					stmt.text= singleton.g_query_xml.entry.(@key == "delLS006");
					stmt.execute();
					
					// コミット
					stmt.sqlConnection.commit();
					
					// トランザクション開始
					stmt.sqlConnection.begin();
					
					stmt.text= singleton.g_query_xml.entry.(@key == "insLS006");
					
					for (i=0; i<w_arr.length; i++) {
						//arrLS006.addItem(LS006(w_arr[i]));
						
						// TAXCLS
						stmt.parameters[0] = w_arr[i].TAXCLS;
						// TAXCLSNM
						stmt.parameters[1] = w_arr[i].TAXCLSNM;
						// TAXRT
						stmt.parameters[2] = w_arr[i].TAXRT;
						stmt.parameters[3] = w_arr[i].KJNDY;
						stmt.parameters[4] = w_arr[i].TAXRTN;
						
						stmt.execute();
					}
					
					// コミット
					stmt.sqlConnection.commit();
					
					// 次関数を実行
					BasedMethod.execNextFunction(nextfunc);
				},
				
				// 失敗時の処理
				srv.TokenFaultEvent
			));
		}
		
		/**********************************************************
		 * 得意先区分ダウンロード及びSQLiteへのインサート
		 * @author 12.08.30 SE-354
		 **********************************************************/ 
		public function cdsW_LS010(nextfunc: Function=null): void
		{
			// 変数の初期化
			token = null;
			
			var srv: SRV = new SRV("sitedata");
			
			// データの取得
			token = srv.srv.cdsW_LS010(w_htid);
			
			// 実行結果より、戻り値を取得
			token.addResponder(new AsyncResponder(
				
				// 成功時の処理
				function(e:ResultEvent, obj:Object=null):void
				{
					var w_arr: ArrayCollection;
					var i: int;
					
					// 一旦、ワーク配列にセット
					w_arr = new ArrayCollection(); 
					w_arr = e.result;
					
					
					stmt = new SQLStatement();
					//stmt.sqlConnection =  connection;
					stmt.sqlConnection =  singleton.sqlconnection;
					
					// トランザクション開始
					stmt.sqlConnection.begin();
					
					//insert前に削除
					stmt.text= singleton.g_query_xml.entry.(@key == "delLS010");
					stmt.execute();
					
					// コミット
					stmt.sqlConnection.commit();
					
					// トランザクション開始
					stmt.sqlConnection.begin();
					
					stmt.text= singleton.g_query_xml.entry.(@key == "insLS010");
					
					for (i=0; i<w_arr.length; i++) {
						
						// CLS
						stmt.parameters[0] = w_arr[i].CLS;
						stmt.parameters[1] = w_arr[i].CLSNM1;
						stmt.parameters[2] = w_arr[i].CLSNM2;
						stmt.parameters[3] = w_arr[i].CLSNM3;
						stmt.parameters[4] = w_arr[i].CLSNM4;
						stmt.parameters[5] = w_arr[i].CLSNM5;
						stmt.parameters[6] = w_arr[i].CLSNM6;
						stmt.parameters[7] = w_arr[i].CLSNM7;
						stmt.parameters[8] = w_arr[i].CLSNM8;
						
						stmt.execute();
					}
					
					// コミット
					stmt.sqlConnection.commit();
					
					// 次関数を実行
					BasedMethod.execNextFunction(nextfunc);
				},
				
				// 失敗時の処理
				srv.TokenFaultEvent
			));
		}
		
		/**********************************************************
		 * 和暦年号ダウンロード及びSQLiteへのインサート
		 * @author 12.11.05 SE-300
		 **********************************************************/ 
		public function cdsW_LS014(nextfunc: Function=null): void
		{
			// 変数の初期化
			token = null;
			
			var srv: SRV = new SRV("sitedata");
			
			// データの取得
			token = srv.srv.cdsW_LS014(w_htid);
			
			// 実行結果より、戻り値を取得
			token.addResponder(new AsyncResponder(
				
				// 成功時の処理
				function(e:ResultEvent, obj:Object=null):void
				{
					var w_arr: ArrayCollection;
					var i: int;
					
					// 一旦、ワーク配列にセット
					w_arr = new ArrayCollection(); 
					w_arr = e.result;
					
					
					stmt = new SQLStatement();
					//stmt.sqlConnection =  connection;
					stmt.sqlConnection =  singleton.sqlconnection;
					
					// トランザクション開始
					stmt.sqlConnection.begin();
					
					//insert前に削除
					stmt.text= singleton.g_query_xml.entry.(@key == "delLS014");
					stmt.execute();
					
					// コミット
					stmt.sqlConnection.commit();
					
					// トランザクション開始
					stmt.sqlConnection.begin();
					
					stmt.text= singleton.g_query_xml.entry.(@key == "insLS014");
					
					for (i=0; i<w_arr.length; i++) {
						
						stmt.parameters[0] = w_arr[i].CLS;
						stmt.parameters[1] = w_arr[i].ERA;
						stmt.parameters[2] = w_arr[i].SDAT;
						stmt.parameters[3] = w_arr[i].FLG;
						stmt.parameters[4] = w_arr[i].ERAS;
						
						stmt.execute();
					}
					
					// コミット
					stmt.sqlConnection.commit();
					
					// 次関数を実行
					BasedMethod.execNextFunction(nextfunc);
				},
				
				// 失敗時の処理
				srv.TokenFaultEvent
			));
		}
		
		/**********************************************************
		 * 商品マスタダウンロード及びSQLiteへのインサート
		 * @author 12.08.30 SE-354
		 **********************************************************/ 
		public function cdsW_LM006(nextfunc: Function=null): void
		{
			// 変数の初期化
			token = null;
			
			var srv: SRV = new SRV("sitedata");
			
			// データの取得
			token = srv.srv.cdsW_LM006(w_htid);
			
			// 実行結果より、戻り値を取得
			token.addResponder(new AsyncResponder(
				
				// 成功時の処理
				function(e:ResultEvent, obj:Object=null):void
				{
					var w_arr: ArrayCollection;
					var i: int;
					
					// 一旦、ワーク配列にセット
					w_arr = new ArrayCollection(); 
					w_arr = e.result;
					
					
					stmt = new SQLStatement();
					//stmt.sqlConnection =  connection;
					stmt.sqlConnection =  singleton.sqlconnection;
					
					// トランザクション開始
					stmt.sqlConnection.begin();
					
					//insert前に削除
					stmt.text= singleton.g_query_xml.entry.(@key == "delLM006");
					stmt.execute();
					
					// コミット
					stmt.sqlConnection.commit();
					
					// トランザクション開始
					stmt.sqlConnection.begin();
					
					stmt.text= singleton.g_query_xml.entry.(@key == "insLM006");
					
					for (i=0; i<w_arr.length; i++) {
						
						stmt.parameters[0] = w_arr[i].VJAN;
						stmt.parameters[1] = w_arr[i].GDNM;
						stmt.parameters[2] = w_arr[i].STCST;
						stmt.parameters[3] = w_arr[i].DCLS;
						stmt.parameters[4] = w_arr[i].GDCLS;
						stmt.parameters[5] = w_arr[i].MDCLS;
						stmt.parameters[6] = w_arr[i].OFCLS;
						stmt.parameters[7] = w_arr[i].GDKNM;
						stmt.parameters[8] = w_arr[i].GDCOM;
						stmt.parameters[9] = w_arr[i].ATTENT;
						stmt.parameters[10] = w_arr[i].MEMO;
						stmt.parameters[11] = w_arr[i].DISRT;
						stmt.parameters[12] = w_arr[i].DISCST;
						
						stmt.execute();
					}
					
					// コミット
					stmt.sqlConnection.commit();
					
					// 次関数を実行
					BasedMethod.execNextFunction(nextfunc);
				},
				
				// 失敗時の処理
				srv.TokenFaultEvent
			));
		}
		
		/**********************************************************
		 * 担当者マスタダウンロード及びSQLiteへのインサート
		 * @author 12.08.30 SE-354
		 **********************************************************/ 
		public function cdsW_LM007(nextfunc: Function=null): void
		{
			// 変数の初期化
			token = null;
			
			var srv: SRV = new SRV("sitedata");
			
			// データの取得
			token = srv.srv.cdsW_LM007(w_htid);
			
			// 実行結果より、戻り値を取得
			token.addResponder(new AsyncResponder(
				
				// 成功時の処理
				function(e:ResultEvent, obj:Object=null):void
				{
					var w_arr: ArrayCollection;
					var i: int;
					
					// 一旦、ワーク配列にセット
					w_arr = new ArrayCollection(); 
					w_arr = e.result;
					
					
					stmt = new SQLStatement();
					//stmt.sqlConnection =  connection;
					stmt.sqlConnection =  singleton.sqlconnection;
					
					// トランザクション開始
					stmt.sqlConnection.begin();
					
					//insert前に削除
					stmt.text= singleton.g_query_xml.entry.(@key == "delLM007");
					stmt.execute();
					
					// コミット
					stmt.sqlConnection.commit();
					
					// トランザクション開始
					stmt.sqlConnection.begin();
					
					stmt.text= singleton.g_query_xml.entry.(@key == "insLM007");
					
					for (i=0; i<w_arr.length; i++) {
						
						stmt.parameters[0] = w_arr[i].CHGCD;
						stmt.parameters[1] = w_arr[i].CHGNM;
						
						stmt.execute();
					}

					// コミット
					stmt.sqlConnection.commit();
					
					// 次関数を実行
					BasedMethod.execNextFunction(nextfunc);
				},
				
				// 失敗時の処理
				srv.TokenFaultEvent
			));
		}
		
		/**********************************************************
		 * 担当者別在庫マスタダウンロード及びSQLiteへのインサート
		 * @author 12.08.30 SE-354
		 **********************************************************/ 
		public function cdsW_LM008(nextfunc: Function=null): void
		{
			// 変数の初期化
			token = null;
			
			var srv: SRV = new SRV("sitedata");
			
			// データの取得
			token = srv.srv.cdsW_LM008(w_htid);
			
			// 実行結果より、戻り値を取得
			token.addResponder(new AsyncResponder(
				
				// 成功時の処理
				function(e:ResultEvent, obj:Object=null):void
				{
					var w_arr: ArrayCollection;
					var i: int;
					
					// 一旦、ワーク配列にセット
					w_arr = new ArrayCollection(); 
					w_arr = e.result;
					
					
					stmt = new SQLStatement();
					//stmt.sqlConnection =  connection;
					stmt.sqlConnection =  singleton.sqlconnection;
					
					// トランザクション開始
					stmt.sqlConnection.begin();
					
					//insert前に削除
					stmt.text= singleton.g_query_xml.entry.(@key == "delLM008");
					stmt.execute();
					
					// コミット
					stmt.sqlConnection.commit();
					
					// トランザクション開始
					stmt.sqlConnection.begin();
					
					stmt.text= singleton.g_query_xml.entry.(@key == "insLM008");
					
					for (i=0; i<w_arr.length; i++) {
						
						stmt.parameters[0] = w_arr[i].CHGCD;
						stmt.parameters[1] = w_arr[i].VJAN;
						stmt.parameters[2] = w_arr[i].VPNO;
						stmt.parameters[3] = w_arr[i].MVPNO;
						
						stmt.execute();
					}
					
					// コミット
					stmt.sqlConnection.commit();
					
					// 次関数を実行
					BasedMethod.execNextFunction(nextfunc);
				},
				
				// 失敗時の処理
				srv.TokenFaultEvent
			));
		}
		
		/**********************************************************
		 * 地区マスタダウンロード及びSQLiteへのインサート
		 * @author 12.08.30 SE-354
		 **********************************************************/ 
		public function cdsW_LM009(nextfunc: Function=null): void
		{
			// 変数の初期化
			token = null;
			
			var srv: SRV = new SRV("sitedata");
			
			// データの取得
			token = srv.srv.cdsW_LM009(w_htid);
			
			// 実行結果より、戻り値を取得
			token.addResponder(new AsyncResponder(
				
				// 成功時の処理
				function(e:ResultEvent, obj:Object=null):void
				{
					var w_arr: ArrayCollection;
					var i: int;
					
					// 一旦、ワーク配列にセット
					w_arr = new ArrayCollection(); 
					w_arr = e.result;
					
					
					stmt = new SQLStatement();
					//stmt.sqlConnection =  connection;
					stmt.sqlConnection =  singleton.sqlconnection;
					
					// トランザクション開始
					stmt.sqlConnection.begin();
					
					//insert前に削除
					stmt.text= singleton.g_query_xml.entry.(@key == "delLM009");
					stmt.execute();
					
					// コミット
					stmt.sqlConnection.commit();
					
					// トランザクション開始
					stmt.sqlConnection.begin();
					
					stmt.text= singleton.g_query_xml.entry.(@key == "insLM009");
					
					for (i=0; i<w_arr.length; i++) {
						
						stmt.parameters[0] = w_arr[i].AREA;
						stmt.parameters[1] = w_arr[i].AREANM;
						
						stmt.execute();
					}
					
					// コミット
					stmt.sqlConnection.commit();
					
					// 次関数を実行
					BasedMethod.execNextFunction(nextfunc);
				},
				
				// 失敗時の処理
				srv.TokenFaultEvent
			));
		}
		
		/**********************************************************
		 * 得意先マスタダウンロード及びSQLiteへのインサート
		 * @author 12.08.30 SE-354
		 **********************************************************/ 
		public function cdsW_LM010(nextfunc: Function=null): void
		{
			// 変数の初期化
			token = null;
			
			var srv: SRV = new SRV("sitedata");
			
			// データの取得
			//token = srv.srv.cdsW_LM010(w_htid);
			token = srv.srv.cdsM010W(w_htid, w_chgcd);
			
			// 実行結果より、戻り値を取得
			token.addResponder(new AsyncResponder(
				
				// 成功時の処理
				function(e:ResultEvent, obj:Object=null):void
				{
					var w_arr: ArrayCollection;
					var i: int;
					
					// 一旦、ワーク配列にセット
					w_arr = new ArrayCollection(); 
					w_arr = e.result;
					
					
					stmt = new SQLStatement();
					//stmt.sqlConnection =  connection;
					stmt.sqlConnection =  singleton.sqlconnection;
					
					// トランザクション開始
					stmt.sqlConnection.begin();
					
					//insert前に削除
					stmt.text= singleton.g_query_xml.entry.(@key == "delLM010");
					stmt.execute();
					
					// コミット
					stmt.sqlConnection.commit();
					
					// トランザクション開始
					stmt.sqlConnection.begin();
					
					stmt.text= singleton.g_query_xml.entry.(@key == "insLM010");
					
					for (i=0; i<w_arr.length; i++) {
						
						stmt.parameters[0] = w_arr[i].TKCD;
						stmt.parameters[1] = w_arr[i].TKNM;
						stmt.parameters[2] = w_arr[i].TKKNM;
						stmt.parameters[3] = w_arr[i].TKNMS;
						stmt.parameters[4] = w_arr[i].TKZIP;
						stmt.parameters[5] = w_arr[i].TKADD1;
						stmt.parameters[6] = w_arr[i].TKADD2;
						stmt.parameters[7] = w_arr[i].TKADD3;
						stmt.parameters[8] = w_arr[i].TKTEL;
						stmt.parameters[9] = w_arr[i].TKMTEL;
						stmt.parameters[10] = w_arr[i].TKFAX;
						stmt.parameters[11] = w_arr[i].TKMAIL1;
						stmt.parameters[12] = w_arr[i].TKAREA;
						stmt.parameters[13] = w_arr[i].TKCHGCD;
						stmt.parameters[14] = w_arr[i].MEMO;
						stmt.parameters[15] = w_arr[i].BOXINF;
						stmt.parameters[16] = w_arr[i].HSPAN;
						stmt.parameters[17] = w_arr[i].LHDAT;
						stmt.parameters[18] = w_arr[i].LHTIME;
						stmt.parameters[19] = w_arr[i].KBN1;
						stmt.parameters[20] = w_arr[i].KBN2;
						stmt.parameters[21] = w_arr[i].KBN3;
						stmt.parameters[22] = w_arr[i].KBN4;
						stmt.parameters[23] = w_arr[i].KBN5;
						stmt.parameters[24] = w_arr[i].KBN6;
						stmt.parameters[25] = w_arr[i].KBN7;
						stmt.parameters[26] = w_arr[i].KBN8;
						stmt.parameters[27] = w_arr[i].HMEDIYAMT;
						stmt.parameters[28] = w_arr[i].HMEDIZAMT;
						stmt.parameters[29] = w_arr[i].HMEDIKURI;
						stmt.parameters[30] = w_arr[i].HMEDIAMT;
						stmt.parameters[31] = w_arr[i].HMEDITAX;
						stmt.parameters[32] = w_arr[i].HMEDINY;
						stmt.parameters[33] = w_arr[i].HMEDIDIS;
						stmt.parameters[34] = w_arr[i].HMEDIKAS;
						stmt.parameters[35] = w_arr[i].HMEDIRTAX;
						stmt.parameters[36] = w_arr[i].HSTOREYAMT;
						stmt.parameters[37] = w_arr[i].HSTOREZAMT;
						stmt.parameters[38] = w_arr[i].HSTOREKURI;
						stmt.parameters[39] = w_arr[i].HSTOREAMT;
						stmt.parameters[40] = w_arr[i].HSTORETAX;
						stmt.parameters[41] = w_arr[i].HSTORENY;
						stmt.parameters[42] = w_arr[i].HSTOREDIS;
						stmt.parameters[43] = w_arr[i].HSTOREKAS;
						stmt.parameters[44] = w_arr[i].HSTORERTAX;
						stmt.parameters[45] = w_arr[i].HDEVYAMT;
						stmt.parameters[46] = w_arr[i].HDEVZAMT;
						stmt.parameters[47] = w_arr[i].HDEVKURI;
						stmt.parameters[48] = w_arr[i].HDEVAMT;
						stmt.parameters[49] = w_arr[i].HDEVTAX;
						stmt.parameters[50] = w_arr[i].HDEVNY;
						stmt.parameters[51] = w_arr[i].HDEVDIS;
						stmt.parameters[52] = w_arr[i].HDEVKAS;
						stmt.parameters[53] = w_arr[i].HDEVRTAX;
						stmt.parameters[54] = w_arr[i].GMEDIYAMT;
						stmt.parameters[55] = w_arr[i].GMEDIZAMT;
						stmt.parameters[56] = w_arr[i].GMEDIKURI;
						stmt.parameters[57] = w_arr[i].GMEDIAMT;
						stmt.parameters[58] = w_arr[i].GMEDITAX;
						stmt.parameters[59] = w_arr[i].GMEDINY;
						stmt.parameters[60] = w_arr[i].GMEDIDIS;
						stmt.parameters[61] = w_arr[i].GMEDIKAS;
						stmt.parameters[62] = w_arr[i].GMEDIRTAX;
						stmt.parameters[63] = w_arr[i].GSTOREYAMT;
						stmt.parameters[64] = w_arr[i].GSTOREZAMT;
						stmt.parameters[65] = w_arr[i].GSTOREKURI;
						stmt.parameters[66] = w_arr[i].GSTOREAMT;
						stmt.parameters[67] = w_arr[i].GSTORETAX;
						stmt.parameters[68] = w_arr[i].GSTORENY;
						stmt.parameters[69] = w_arr[i].GSTOREDIS;
						stmt.parameters[70] = w_arr[i].GSTOREKAS;
						stmt.parameters[71] = w_arr[i].GSTORERTAX;
						stmt.parameters[72] = w_arr[i].GDEVYAMT;
						stmt.parameters[73] = w_arr[i].GDEVZAMT;
						stmt.parameters[74] = w_arr[i].GDEVKURI;
						stmt.parameters[75] = w_arr[i].GDEVAMT;
						stmt.parameters[76] = w_arr[i].GDEVTAX;
						stmt.parameters[77] = w_arr[i].GDEVNY;
						stmt.parameters[78] = w_arr[i].GDEVDIS;
						stmt.parameters[79] = w_arr[i].GDEVKAS;
						stmt.parameters[80] = w_arr[i].GDEVRTAX;
						stmt.parameters[81] = w_arr[i].HPYM;
						stmt.parameters[82] = w_arr[i].HPW;
						stmt.parameters[83] = w_arr[i].VNO;
						stmt.parameters[84] = w_arr[i].LATEF;
						stmt.parameters[85] = w_arr[i].ENDF;
						stmt.parameters[86] = w_arr[i].EDAT;
						stmt.parameters[87] = w_arr[i].PLANF;
						stmt.parameters[88] = w_arr[i].LAT;
						stmt.parameters[89] = w_arr[i].LNG;
						stmt.parameters[90] = w_arr[i].GEOHASH;
						stmt.parameters[91] = w_arr[i].IMPCLS;
						//stmt.parameters[92] = w_arr[i].IMPCLS2;
						stmt.parameters[92] = w_arr[i].TKCLS;
						stmt.parameters[93] = w_arr[i].BIRTH;
						stmt.parameters[94] = w_arr[i].MAP;
						stmt.parameters[95] = w_arr[i].KKB;
						stmt.parameters[96] = w_arr[i].KKBCOM;
						stmt.parameters[97] = w_arr[i].HMEDIZREZ;
						stmt.parameters[98] = w_arr[i].HSTOREZREZ;
						stmt.parameters[99] = w_arr[i].HDEVZREZ;
						stmt.parameters[100] = w_arr[i].GMEDIZREZ;
						stmt.parameters[101] = w_arr[i].GSTOREZREZ;
						stmt.parameters[102] = w_arr[i].GDEVZREZ;
						stmt.parameters[103] = w_arr[i].HTCLS;
						stmt.parameters[104] = w_arr[i].DTAREA;
						stmt.parameters[105] = w_arr[i].MEDIDISRT;
						stmt.parameters[106] = w_arr[i].STOREDISRT;
						stmt.parameters[107] = w_arr[i].DEVDISRT;
						stmt.parameters[108] = w_arr[i].TKRANK;
						stmt.parameters[109] = w_arr[i].ESTAMT;
						
						stmt.execute();
					}
					// コミット
					stmt.sqlConnection.commit();
					
					// 次関数を実行
					BasedMethod.execNextFunction(nextfunc);
				},
				
				// 失敗時の処理
				srv.TokenFaultEvent
			));
		}
		
		/**********************************************************
		 * 配置薬マスタダウンロード及びSQLiteへのインサート
		 * @author 12.08.30 SE-354
		 **********************************************************/ 
		public function cdsW_LM012(nextfunc: Function=null): void
		{
			// 変数の初期化
			token = null;
			
			var srv: SRV = new SRV("sitedata");
			
			// データの取得
			//token = srv.srv.cdsW_LM012(w_htid);
			token = srv.srv.cdsM012W(w_htid, w_chgcd);
			
			// 実行結果より、戻り値を取得
			token.addResponder(new AsyncResponder(
				
				// 成功時の処理
				function(e:ResultEvent, obj:Object=null):void
				{
					var w_arr: ArrayCollection;
					var i: int;
					
					// 一旦、ワーク配列にセット
					w_arr = new ArrayCollection(); 
					w_arr = e.result;
					
					
					stmt = new SQLStatement();
					//stmt.sqlConnection =  connection;
					stmt.sqlConnection =  singleton.sqlconnection;
					
					// トランザクション開始
					stmt.sqlConnection.begin();
					
					//insert前に削除
					stmt.text= singleton.g_query_xml.entry.(@key == "delLM012");
					stmt.execute();
					
					// コミット
					stmt.sqlConnection.commit();
					
					// トランザクション開始
					stmt.sqlConnection.begin();
					
					stmt.text= singleton.g_query_xml.entry.(@key == "insLM012");
					
					for (i=0; i<w_arr.length; i++) {
						
						stmt.parameters[0] = w_arr[i].TKCD;
						stmt.parameters[1] = w_arr[i].VJAN;
						stmt.parameters[2] = w_arr[i].VPNO;
						stmt.parameters[3] = w_arr[i].IRRADD;
						stmt.parameters[4] = w_arr[i].BHISK;
						
						stmt.execute();
					}
					
					// コミット
					stmt.sqlConnection.commit();
					
					// 次関数を実行
					BasedMethod.execNextFunction(nextfunc);
				},
				
				// 失敗時の処理
				srv.TokenFaultEvent
			));
		}
		
		/**********************************************************
		 * 商品分類マスタダウンロード及びSQLiteへのインサート
		 * @author 12.08.30 SE-354
		 **********************************************************/ 
		public function cdsW_LM013(nextfunc: Function=null): void
		{
			// 変数の初期化
			token = null;
			
			var srv: SRV = new SRV("sitedata");
			
			// データの取得
			token = srv.srv.cdsW_LM013(w_htid);
			
			// 実行結果より、戻り値を取得
			token.addResponder(new AsyncResponder(
				
				// 成功時の処理
				function(e:ResultEvent, obj:Object=null):void
				{
					var w_arr: ArrayCollection;
					var i: int;
					
					// 一旦、ワーク配列にセット
					w_arr = new ArrayCollection(); 
					w_arr = e.result;
					
					
					stmt = new SQLStatement();
					//stmt.sqlConnection =  connection;
					stmt.sqlConnection =  singleton.sqlconnection;
					
					// トランザクション開始
					stmt.sqlConnection.begin();
					
					//insert前に削除
					stmt.text= singleton.g_query_xml.entry.(@key == "delLM013");
					stmt.execute();
					
					// コミット
					stmt.sqlConnection.commit();
					
					// トランザクション開始
					stmt.sqlConnection.begin();
					
					stmt.text= singleton.g_query_xml.entry.(@key == "insLM013");
					
					for (i=0; i<w_arr.length; i++) {
						
						stmt.parameters[0] = w_arr[i].GDCLS;
						stmt.parameters[1] = w_arr[i].GDCLSNM;
						
						stmt.execute();
					}
					
					// コミット
					stmt.sqlConnection.commit();
					
					// 次関数を実行
					BasedMethod.execNextFunction(nextfunc);
				},
				
				// 失敗時の処理
				srv.TokenFaultEvent
			));
		}
		
		/**********************************************************
		 * 医薬分類マスタダウンロード及びSQLiteへのインサート
		 * @author 12.08.30 SE-354
		 **********************************************************/ 
		public function cdsW_LM014(nextfunc: Function=null): void
		{
			// 変数の初期化
			token = null;
			
			var srv: SRV = new SRV("sitedata");
			
			// データの取得
			token = srv.srv.cdsW_LM014(w_htid);
			
			// 実行結果より、戻り値を取得
			token.addResponder(new AsyncResponder(
				
				// 成功時の処理
				function(e:ResultEvent, obj:Object=null):void
				{
					var w_arr: ArrayCollection;
					var i: int;
					
					// 一旦、ワーク配列にセット
					w_arr = new ArrayCollection(); 
					w_arr = e.result;
					
					
					stmt = new SQLStatement();
					//stmt.sqlConnection =  connection;
					stmt.sqlConnection =  singleton.sqlconnection;
					
					// トランザクション開始
					stmt.sqlConnection.begin();
					
					//insert前に削除
					stmt.text= singleton.g_query_xml.entry.(@key == "delLM014");
					stmt.execute();
					
					// コミット
					stmt.sqlConnection.commit();
					
					// トランザクション開始
					stmt.sqlConnection.begin();
					
					stmt.text= singleton.g_query_xml.entry.(@key == "insLM014");
					
					for (i=0; i<w_arr.length; i++) {
						
						stmt.parameters[0] = w_arr[i].MDCLS;
						stmt.parameters[1] = w_arr[i].MDCLSNM;
						stmt.parameters[2] = w_arr[i].MDCLSNMS;
						
						stmt.execute();
					}
					
					// コミット
					stmt.sqlConnection.commit();
					
					// 次関数を実行
					BasedMethod.execNextFunction(nextfunc);
				},
				
				// 失敗時の処理
				srv.TokenFaultEvent
			));
		}
		
		/**********************************************************
		 * 重点区分マスタダウンロード及びSQLiteへのインサート
		 * @author 12.11.01 SE-300
		 **********************************************************/ 
		public function cdsW_LM015(nextfunc: Function=null): void
		{
			// 変数の初期化
			token = null;
			
			var srv: SRV = new SRV("sitedata");
			
			// データの取得
			token = srv.srv.cdsW_LM015(w_htid);
			
			// 実行結果より、戻り値を取得
			token.addResponder(new AsyncResponder(
				
				// 成功時の処理
				function(e:ResultEvent, obj:Object=null):void
				{
					var w_arr: ArrayCollection;
					var i: int;
					
					// 一旦、ワーク配列にセット
					w_arr = new ArrayCollection(); 
					w_arr = e.result;
					
					
					stmt = new SQLStatement();
					//stmt.sqlConnection =  connection;
					stmt.sqlConnection =  singleton.sqlconnection;
					
					// トランザクション開始
					stmt.sqlConnection.begin();
					
					//insert前に削除
					stmt.text= singleton.g_query_xml.entry.(@key == "delLM015");
					stmt.execute();
					
					// コミット
					stmt.sqlConnection.commit();
					
					// トランザクション開始
					stmt.sqlConnection.begin();
					
					stmt.text= singleton.g_query_xml.entry.(@key == "insLM015");
					
					for (i=0; i<w_arr.length; i++) {
						
						stmt.parameters[0] = w_arr[i].IMPCLS;
						stmt.parameters[1] = w_arr[i].IMPCLSNM;
						
						stmt.execute();
					}
					
					// コミット
					stmt.sqlConnection.commit();
					
					// 次関数を実行
					BasedMethod.execNextFunction(nextfunc);
				},
				
				// 失敗時の処理
				srv.TokenFaultEvent
			));
		}
		
		/**********************************************************
		 * 地区詳細マスタダウンロード及びSQLiteへのインサート
		 * @author 12.11.01 SE-300
		 **********************************************************/ 
		public function cdsW_LM016(nextfunc: Function=null): void
		{
			// 変数の初期化
			token = null;
			
			var srv: SRV = new SRV("sitedata");
			
			// データの取得
			token = srv.srv.cdsW_LM016(w_htid);
			
			// 実行結果より、戻り値を取得
			token.addResponder(new AsyncResponder(
				
				// 成功時の処理
				function(e:ResultEvent, obj:Object=null):void
				{
					var w_arr: ArrayCollection;
					var i: int;
					
					// 一旦、ワーク配列にセット
					w_arr = new ArrayCollection(); 
					w_arr = e.result;
					
					
					stmt = new SQLStatement();
					//stmt.sqlConnection =  connection;
					stmt.sqlConnection =  singleton.sqlconnection;
					
					// トランザクション開始
					stmt.sqlConnection.begin();
					
					//insert前に削除
					stmt.text= singleton.g_query_xml.entry.(@key == "delLM016");
					stmt.execute();
					
					// コミット
					stmt.sqlConnection.commit();
					
					// トランザクション開始
					stmt.sqlConnection.begin();
					
					stmt.text= singleton.g_query_xml.entry.(@key == "insLM016");
					
					for (i=0; i<w_arr.length; i++) {
						
						stmt.parameters[0] = w_arr[i].AREA;
						stmt.parameters[1] = w_arr[i].DTAREA;
						stmt.parameters[2] = w_arr[i].DTAREANM;
						
						stmt.execute();
					}
					
					// コミット
					stmt.sqlConnection.commit();
					
					// 次関数を実行
					BasedMethod.execNextFunction(nextfunc);
				},
				
				// 失敗時の処理
				srv.TokenFaultEvent
			));
		}
		
		/**********************************************************
		 * 訪問時間マスタダウンロード及びSQLiteへのインサート
		 * @author 12.11.12 SE-300
		 **********************************************************/ 
		public function cdsW_LM017(nextfunc: Function=null): void
		{
			// 変数の初期化
			token = null;
			
			var srv: SRV = new SRV("sitedata");
			
			// データの取得
			token = srv.srv.cdsW_LM017(w_htid);
			
			// 実行結果より、戻り値を取得
			token.addResponder(new AsyncResponder(
				
				// 成功時の処理
				function(e:ResultEvent, obj:Object=null):void
				{
					var w_arr: ArrayCollection;
					var i: int;
					
					// 一旦、ワーク配列にセット
					w_arr = new ArrayCollection(); 
					w_arr = e.result;
										
					stmt = new SQLStatement();
					//stmt.sqlConnection =  connection;
					stmt.sqlConnection =  singleton.sqlconnection;
					
					// トランザクション開始
					stmt.sqlConnection.begin();
					
					//insert前に削除
					stmt.text= singleton.g_query_xml.entry.(@key == "delLM017");
					stmt.execute();
					
					// コミット
					stmt.sqlConnection.commit();
					
					// トランザクション開始
					stmt.sqlConnection.begin();
					
					stmt.text= singleton.g_query_xml.entry.(@key == "insLM017");
					
					for (i=0; i<w_arr.length; i++) {
						
						stmt.parameters[0] = w_arr[i].HTIMECD;
						stmt.parameters[1] = w_arr[i].HTIMENM;
						
						stmt.execute();
					}
					
					// コミット
					stmt.sqlConnection.commit();
					
					// 次関数を実行
					BasedMethod.execNextFunction(nextfunc);
				},
				
				// 失敗時の処理
				srv.TokenFaultEvent
			));
		}
		
		/**********************************************************
		 * 得意先区分マスタダウンロード及びSQLiteへのインサート
		 * @author 12.11.12 SE-300
		 **********************************************************/ 
		public function cdsW_LM018(nextfunc: Function=null): void
		{
			// 変数の初期化
			token = null;
			
			var srv: SRV = new SRV("sitedata");
			
			// データの取得
			token = srv.srv.cdsW_LM018(w_htid);
			
			// 実行結果より、戻り値を取得
			token.addResponder(new AsyncResponder(
				
				// 成功時の処理
				function(e:ResultEvent, obj:Object=null):void
				{
					var w_arr: ArrayCollection;
					var i: int;
					
					// 一旦、ワーク配列にセット
					w_arr = new ArrayCollection(); 
					w_arr = e.result;
					
					stmt = new SQLStatement();
					//stmt.sqlConnection =  connection;
					stmt.sqlConnection =  singleton.sqlconnection;
					
					// トランザクション開始
					stmt.sqlConnection.begin();
					
					//insert前に削除
					stmt.text= singleton.g_query_xml.entry.(@key == "delLM018");
					stmt.execute();
					
					// コミット
					stmt.sqlConnection.commit();
					
					// トランザクション開始
					stmt.sqlConnection.begin();
					
					stmt.text= singleton.g_query_xml.entry.(@key == "insLM018");
					
					for (i=0; i<w_arr.length; i++) {
						
						stmt.parameters[0] = w_arr[i].TKCLS;
						stmt.parameters[1] = w_arr[i].TKCLSNM;
						
						stmt.execute();
					}
					
					// コミット
					stmt.sqlConnection.commit();
					
					// 次関数を実行
					BasedMethod.execNextFunction(nextfunc);
				},
				
				// 失敗時の処理
				srv.TokenFaultEvent
			));
		}
		
		/**********************************************************
		 * 配置薬販売履歴FAダウンロード及びSQLiteへのインサート
		 * @author 12.08.30 SE-354
		 **********************************************************/ 
		public function cdsW_LF001(nextfunc: Function=null): void
		{
			// 変数の初期化
			token = null;
			
			var srv: SRV = new SRV("sitedata");
			
			// データの取得
			token = srv.srv.cdsW_LF001(w_htid,w_htchgcd,"1", g_updateCount.toString());
			
			// 実行結果より、戻り値を取得
			token.addResponder(new AsyncResponder(
				
				// 成功時の処理
				function(e:ResultEvent, obj:Object=null):void
				{
					var w_arr: ArrayCollection;
					var i: int;
					
					// 一旦、ワーク配列にセット
					w_arr = new ArrayCollection(); 
					w_arr = e.result;
					
					
					stmt = new SQLStatement();
					//stmt.sqlConnection =  connection;
					stmt.sqlConnection =  singleton.sqlconnection;
					
					if (g_updateCount == 1) {
						
						// トランザクション開始
						stmt.sqlConnection.begin();
						
						// insert前に削除
						stmt.text= singleton.g_query_xml.entry.(@key == "delLF001");
						stmt.execute();
						
						// コミット
						stmt.sqlConnection.commit();
					}	
				
					// トランザクション開始
					stmt.sqlConnection.begin();
					
					stmt.text= singleton.g_query_xml.entry.(@key == "insLF001");
					
					for (i=0; i<w_arr.length; i++) {
						
						stmt.parameters[0] = w_arr[i].TKCD;
						stmt.parameters[1] = w_arr[i].HDAT;
						stmt.parameters[2] = w_arr[i].HTIME;
						stmt.parameters[3] = w_arr[i].WITHTAX;
						stmt.parameters[4] = w_arr[i].WITHOUTTAX;
						stmt.parameters[5] = w_arr[i].AMT;
						stmt.parameters[6] = w_arr[i].TAX;
						stmt.parameters[7] = w_arr[i].MONEY;
						stmt.parameters[8] = w_arr[i].DIS;
						stmt.parameters[9] = w_arr[i].KAS;
						stmt.parameters[10] = w_arr[i].RTAX;
						
						stmt.execute();
					}
					
					// コミット
					stmt.sqlConnection.commit();
					
					// 次関数を実行
					BasedMethod.execNextFunction(nextfunc);
				},
				
				// 失敗時の処理
				srv.TokenFaultEvent
			));
		}
		
		/**********************************************************
		 * 配置薬販売履歴FBダウンロード及びSQLiteへのインサート
		 * @author 12.08.30 SE-354
		 **********************************************************/ 
		public function cdsW_LF002(nextfunc: Function=null): void
		{
			// 変数の初期化
			token = null;
			
			var srv: SRV = new SRV("sitedata");
			
			// データの取得
			token = srv.srv.cdsW_LF002(w_htid,w_htchgcd,"1", g_updateCount.toString());
			
			// 実行結果より、戻り値を取得
			token.addResponder(new AsyncResponder(
				
				// 成功時の処理
				function(e:ResultEvent, obj:Object=null):void
				{
					var w_arr: ArrayCollection;
					var i: int;
					
					// 一旦、ワーク配列にセット
					w_arr = new ArrayCollection(); 
					w_arr = e.result;
					
					
					stmt = new SQLStatement();
					//stmt.sqlConnection =  connection;
					stmt.sqlConnection =  singleton.sqlconnection;
					
					if (g_updateCount == 1) {
						
						// トランザクション開始
						stmt.sqlConnection.begin();
						
						// insert前に削除
						stmt.text= singleton.g_query_xml.entry.(@key == "delLF002");
						stmt.execute();

						// コミット
						stmt.sqlConnection.commit();
					}
					
					// トランザクション開始
					stmt.sqlConnection.begin();
					
					stmt.text= singleton.g_query_xml.entry.(@key == "insLF002");
					
					for (i=0; i<w_arr.length; i++) {
						
						stmt.parameters[0] = w_arr[i].TKCD;
						stmt.parameters[1] = w_arr[i].HDAT;
						stmt.parameters[2] = w_arr[i].HTIME;
						stmt.parameters[3] = w_arr[i].VJAN;
						stmt.parameters[4] = w_arr[i].GDNM;
						stmt.parameters[5] = w_arr[i].VPNO;
						
						stmt.execute();
					}
					
					// コミット
					stmt.sqlConnection.commit();
					
					// 次関数を実行
					BasedMethod.execNextFunction(nextfunc);
				},
				
				// 失敗時の処理
				srv.TokenFaultEvent
			));
		}
		
		/**********************************************************
		 * 配置薬販売履歴FBダウンロード及びSQLiteへのインサート
		 * @author 12.08.30 SE-354
		 **********************************************************/ 
		public function cdsW_LF003(nextfunc: Function=null): void
		{
			// 変数の初期化
			token = null;
			
			var srv: SRV = new SRV("sitedata");
			
			// データの取得
			token = srv.srv.cdsW_LF003(w_htid,w_htchgcd,"1", g_updateCount.toString());
			
			// 実行結果より、戻り値を取得
			token.addResponder(new AsyncResponder(
				
				// 成功時の処理
				function(e:ResultEvent, obj:Object=null):void
				{
					var w_arr: ArrayCollection;
					var i: int;
					
					// 一旦、ワーク配列にセット
					w_arr = new ArrayCollection(); 
					w_arr = e.result;
					
					
					stmt = new SQLStatement();
					//stmt.sqlConnection =  connection;
					stmt.sqlConnection =  singleton.sqlconnection;

					if (g_updateCount == 1) {
						
						// トランザクション開始
						stmt.sqlConnection.begin();
						
						// insert前に削除
						stmt.text= singleton.g_query_xml.entry.(@key == "delLF003");
						stmt.execute();
						
						// コミット
						stmt.sqlConnection.commit();
						
					}
					
					// トランザクション開始
					stmt.sqlConnection.begin();
					
					stmt.text= singleton.g_query_xml.entry.(@key == "insLF003");
					
					for (i=0; i<w_arr.length; i++) {
						
						stmt.parameters[0] = w_arr[i].TKCD;
						stmt.parameters[1] = w_arr[i].HDAT;
						stmt.parameters[2] = w_arr[i].HTIME;
						stmt.parameters[3] = w_arr[i].VJAN;
						stmt.parameters[4] = w_arr[i].GDNM;
						stmt.parameters[5] = w_arr[i].VPNO;
						stmt.parameters[6] = w_arr[i].AMT;
						stmt.parameters[7] = w_arr[i].DISC;
						stmt.parameters[8] = w_arr[i].SAMT;
						stmt.parameters[9] = w_arr[i].NAMT;
						stmt.parameters[10] = w_arr[i].NKAMT;
						stmt.parameters[11] = w_arr[i].BAL;
						stmt.parameters[12] = w_arr[i].VLIN; 
						
						stmt.execute();
					}
					
					// コミット
					stmt.sqlConnection.commit();
					
					// 次関数を実行
					BasedMethod.execNextFunction(nextfunc);
				},
				
				// 失敗時の処理
				srv.TokenFaultEvent
			));
		}
		
		/**********************************************************
		 * サービス品提供履歴FBダウンロード及びSQLiteへのインサート
		 * @author 12.11.12 SE-300
		 **********************************************************/ 
		public function cdsW_LF004(nextfunc: Function=null): void
		{
			// 変数の初期化
			token = null;
			
			var srv: SRV = new SRV("sitedata");
			
			// データの取得
			token = srv.srv.cdsW_LF004(w_htid,w_htchgcd,"1");
			
			// 実行結果より、戻り値を取得
			token.addResponder(new AsyncResponder(
				
				// 成功時の処理
				function(e:ResultEvent, obj:Object=null):void
				{
					var w_arr: ArrayCollection;
					var i: int;
					
					// 一旦、ワーク配列にセット
					w_arr = new ArrayCollection(); 
					w_arr = e.result;
					
					
					stmt = new SQLStatement();
					//stmt.sqlConnection =  connection;
					stmt.sqlConnection =  singleton.sqlconnection;
					
					// トランザクション開始
					stmt.sqlConnection.begin();
					
					// insert前に削除
					stmt.text= singleton.g_query_xml.entry.(@key == "delLF004");
					stmt.execute();
					
					// コミット
					stmt.sqlConnection.commit();
					
					// トランザクション開始
					stmt.sqlConnection.begin();
					
					stmt.text= singleton.g_query_xml.entry.(@key == "insLF004");
					
					for (i=0; i<w_arr.length; i++) {
						
						stmt.parameters[0] = w_arr[i].TKCD;
						stmt.parameters[1] = w_arr[i].HDAT;
						stmt.parameters[2] = w_arr[i].HTIME;
						stmt.parameters[3] = w_arr[i].VJAN;
						stmt.parameters[4] = w_arr[i].GDNM;
						stmt.parameters[5] = w_arr[i].VPNO;
						
						stmt.execute();
					}
					
					// コミット
					stmt.sqlConnection.commit();
					
					// 次関数を実行
					BasedMethod.execNextFunction(nextfunc);
				},
				
				// 失敗時の処理
				srv.TokenFaultEvent
			));
		}
		
//		/**********************************************************
//		 * 月別平均実績Fダウンロード及びSQLiteへのインサート
//  	 * @author 12.08.30 SE-354
//		 **********************************************************/ 
//		public function cdsW_LF007(nextfunc: Function=null): void
//		{
//			// 変数の初期化
//			token = null;
//			
//			var srv: SRV = new SRV("sitedata");
//			
//			// データの取得
//			token = srv.srv.cdsW_LF007(w_htid);
//			
//			// 実行結果より、戻り値を取得
//			token.addResponder(new AsyncResponder(
//				
//				// 成功時の処理
//				function(e:ResultEvent, obj:Object=null):void
//				{
//					var w_arr: ArrayCollection;
//					var i: int;
//					
//					// 一旦、ワーク配列にセット
//					w_arr = new ArrayCollection(); 
//					w_arr = e.result;
//					
//					
//					stmt = new SQLStatement();
//					//stmt.sqlConnection =  connection;
//					stmt.sqlConnection =  singleton.sqlconnection;
//					
//					// トランザクション開始
//					stmt.sqlConnection.begin();
//					
//					stmt.text= singleton.g_query_xml.entry.(@key == "insLF007");
//					
//					for (i=0; i<w_arr.length; i++) {
//						
//						stmt.parameters[0] = w_arr[i].TKCD;
//						stmt.parameters[1] = w_arr[i].SMON;
//						stmt.parameters[2] = w_arr[i].AMT;
//						
//						stmt.execute();
//					}
//					
//					// コミット
//					stmt.sqlConnection.commit();
//					
//					// 次関数を実行
//					BasedMethod.execNextFunction(nextfunc);
//				},
//				
//				// 失敗時の処理
//				srv.TokenFaultEvent
//			));
//		}
		
		/**********************************************************
		 * 配置薬販売履歴FBダウンロード及びSQLiteへのインサート
		 * @author 12.08.30 SE-354
		 **********************************************************/ 
		public function cdsW_LF010(nextfunc: Function=null): void
		{
			// 変数の初期化
			token = null;
			
			var srv: SRV = new SRV("sitedata");
			
			// データの取得
			token = srv.srv.cdsW_LF010(w_htid,w_htchgcd,"1", g_updateCount.toString(), arrSEIGYO[0].SDDAY);
			
			// 実行結果より、戻り値を取得
			token.addResponder(new AsyncResponder(
				
				// 成功時の処理
				function(e:ResultEvent, obj:Object=null):void
				{
					var w_arr: ArrayCollection;
					var i: int;
					
					// 一旦、ワーク配列にセット
					w_arr = new ArrayCollection(); 
					w_arr = e.result;
					
					
					stmt = new SQLStatement();
					//stmt.sqlConnection =  connection;
					stmt.sqlConnection =  singleton.sqlconnection;
					
					if (g_updateCount == 1) {
						
						// トランザクション開始
						stmt.sqlConnection.begin();
						
						// insert前に削除
						stmt.text= singleton.g_query_xml.entry.(@key == "delLF010");
						stmt.parameters[0] = arrSEIGYO[0].SDDAY;
						stmt.execute();
						
						// コミット
						stmt.sqlConnection.commit();
					}
					
					// トランザクション開始
					stmt.sqlConnection.begin();
					
					stmt.text= singleton.g_query_xml.entry.(@key == "insLF010");
					
					for (i=0; i<w_arr.length; i++) {
						
						stmt.parameters[0] = w_arr[i].CHGCD;
						stmt.parameters[1] = w_arr[i].HDAT;
						stmt.parameters[2] = w_arr[i].TKCD;
						stmt.parameters[3] = w_arr[i].ABSENT;
						stmt.parameters[4] = w_arr[i].HHTIME;
						stmt.parameters[5] = w_arr[i].BHTIME;
						stmt.parameters[6] = w_arr[i].NHTIME;
						stmt.parameters[7] = w_arr[i].COM;
						stmt.parameters[8] = w_arr[i].ABTIME1;
						stmt.parameters[9] = w_arr[i].ABTIME2;
						stmt.parameters[10] = w_arr[i].ABTIME3;

						// 不在≠nullの場合アップロード済みにする
						if(w_arr[i].ABSENT != null) {
							stmt.parameters[11] = "1";
						}else{
							stmt.parameters[11] = "0";
						}
						
						stmt.execute();
					}
					
					// コミット
					stmt.sqlConnection.commit();
					
					// 次関数を実行
					BasedMethod.execNextFunction(nextfunc);
				},
				
				// 失敗時の処理
				srv.TokenFaultEvent
			));
		}
		
		/**********************************************************
		 * 商品別配置薬実績Fダウンロード及びSQLiteへのインサート
		 * @author 12.08.30 SE-354
		 **********************************************************/ 
		public function cdsW_LF011(nextfunc: Function=null): void
		{
			// 変数の初期化
			token = null;
			
			var srv: SRV = new SRV("sitedata");
			
			// データの取得
			token = srv.srv.cdsW_LF011(w_htid,w_htchgcd,"1");
			
			// 実行結果より、戻り値を取得
			token.addResponder(new AsyncResponder(
				
				// 成功時の処理
				function(e:ResultEvent, obj:Object=null):void
				{
					var w_arr: ArrayCollection;
					var i: int;
					
					// 一旦、ワーク配列にセット
					w_arr = new ArrayCollection(); 
					w_arr = e.result;
					
					
					stmt = new SQLStatement();
					//stmt.sqlConnection =  connection;
					stmt.sqlConnection =  singleton.sqlconnection;
					
					// トランザクション開始
					stmt.sqlConnection.begin();
					
					// insert前に削除
					stmt.text= singleton.g_query_xml.entry.(@key == "delLF011");
					stmt.execute();
					
					// コミット
					stmt.sqlConnection.commit();
					
					// トランザクション開始
					stmt.sqlConnection.begin();
					
					stmt.text= singleton.g_query_xml.entry.(@key == "insLF011");
					
					for (i=0; i<w_arr.length; i++) {
						
						stmt.parameters[0] = w_arr[i].VJAN;
						stmt.parameters[1] = w_arr[i].SMON;
						stmt.parameters[2] = w_arr[i].REPVPNO;
						
						stmt.execute();
					}
					
					// コミット
					stmt.sqlConnection.commit();
					
					// 次関数を実行
					BasedMethod.execNextFunction(nextfunc);
				},
				
				// 失敗時の処理
				srv.TokenFaultEvent
			));
		}
		
		/**********************************************************
		 * 配置薬履歴(照会用)Fダウンロード及びSQLiteへのインサート
		 * @author 12.10.30 SE-300
		 **********************************************************/ 
		public function cdsW_LF012(nextfunc: Function=null): void
		{
			// 変数の初期化
			token = null;
			
			var srv: SRV = new SRV("sitedata");
			
			// データの取得
			token = srv.srv.cdsW_LF012(w_htid,w_htchgcd,"1");
			
			// 実行結果より、戻り値を取得
			token.addResponder(new AsyncResponder(
				
				// 成功時の処理
				function(e:ResultEvent, obj:Object=null):void
				{
					var w_arr: ArrayCollection;
					var i: int;
					
					// 一旦、ワーク配列にセット
					w_arr = new ArrayCollection(); 
					w_arr = e.result;
					
					stmt = new SQLStatement();
					//stmt.sqlConnection =  connection;
					stmt.sqlConnection =  singleton.sqlconnection;

					// トランザクション開始
					stmt.sqlConnection.begin();

					// insert前に削除
					stmt.text= singleton.g_query_xml.entry.(@key == "delLF012");
					stmt.execute();

					// コミット
					stmt.sqlConnection.commit();

					// トランザクション開始
					stmt.sqlConnection.begin();
					
					stmt.text= singleton.g_query_xml.entry.(@key == "insLF012");
					
					for (i=0; i<w_arr.length; i++) {
						
						stmt.parameters[0] = w_arr[i].TKCD;
						stmt.parameters[1] = w_arr[i].LINNO;
						stmt.parameters[2] = w_arr[i].TITNM;
						stmt.parameters[3] = w_arr[i].HBAYI01;
						stmt.parameters[4] = w_arr[i].HBAYI02;
						stmt.parameters[5] = w_arr[i].HBAYI03;
						stmt.parameters[6] = w_arr[i].HBAYI04;
						stmt.parameters[7] = w_arr[i].HBAYI05;
						stmt.parameters[8] = w_arr[i].HBAYI06;
						stmt.parameters[9] = w_arr[i].HBAYI07;
						stmt.parameters[10] = w_arr[i].HBAYI08;
						stmt.parameters[11] = w_arr[i].HBAYI09;
						stmt.parameters[12] = w_arr[i].HBAYI10;
						stmt.parameters[13] = w_arr[i].HBAYI11;
						stmt.parameters[14] = w_arr[i].HBAYI12;
						stmt.parameters[15] = w_arr[i].HBAYI13;
						stmt.parameters[16] = w_arr[i].HBAYI14;
						stmt.parameters[17] = w_arr[i].HBAYI15;
						stmt.parameters[18] = w_arr[i].HBAYI16;
						stmt.parameters[19] = w_arr[i].HBAYI17;
						stmt.parameters[20] = w_arr[i].HBAYI18;
						stmt.parameters[21] = w_arr[i].HBAYI19;
						stmt.parameters[22] = w_arr[i].HBAYI20;
						stmt.parameters[23] = w_arr[i].HBAYI21;
						stmt.parameters[24] = w_arr[i].HBAYI22;
						stmt.parameters[25] = w_arr[i].HBAYI23;
						stmt.parameters[26] = w_arr[i].HBAYI24;
						stmt.parameters[27] = w_arr[i].HBAYI25;
						stmt.parameters[28] = w_arr[i].HBAYI26;
						stmt.parameters[29] = w_arr[i].HBAYI27;
						stmt.parameters[30] = w_arr[i].HBAYI28;
						stmt.parameters[31] = w_arr[i].HBAYI29;
						stmt.parameters[32] = w_arr[i].HBAYI30;
						
						stmt.execute();
					}
					
					// コミット
					stmt.sqlConnection.commit();
					
					// 次関数を実行
					BasedMethod.execNextFunction(nextfunc);
				},
				
				// 失敗時の処理
				srv.TokenFaultEvent
			));
		}
		
		/**********************************************************
		 * W_LS008取得及び制御マスタへのアップデート
		 * @author 12.08.30 SE-354
		 **********************************************************/ 
		public function updateLS001(nextfunc: Function=null): void
		{
			// 変数の初期化
			token = null;
			
			var srv: SRV = new SRV("sitedata");
			
			// データの取得
			token = srv.srv.cdsW_LS008(w_htid);
			
			// 実行結果より、戻り値を取得
			token.addResponder(new AsyncResponder(
				
				// 成功時の処理
				function(e:ResultEvent, obj:Object=null):void
				{
					var w_arr: ArrayCollection;
					var i: int;
					
					// 一旦、ワーク配列にセット
					w_arr = new ArrayCollection(); 
					w_arr = e.result;
					
					
					stmt = new SQLStatement();
					//stmt.sqlConnection =  connection;
					stmt.sqlConnection =  singleton.sqlconnection;
					
					// トランザクション開始
					stmt.sqlConnection.begin();
					
					stmt.text= singleton.g_query_xml.entry.(@key == "updLS001D");
					
					for (i=0; i<w_arr.length; i++) {
						
						stmt.parameters[0] = w_sysdate;
						stmt.parameters[1] = w_systime;
						stmt.parameters[2] = w_arr[i].COSNM1;
						stmt.parameters[3] = w_arr[i].COSNM2;
						stmt.parameters[4] = w_arr[i].COSNM3;
						stmt.parameters[5] = w_arr[i].ADD1_1;
						stmt.parameters[6] = w_arr[i].ADD2_1;
						stmt.parameters[7] = w_arr[i].ADD3_1;
						stmt.parameters[8] = w_arr[i].TEL1;
						stmt.parameters[9] = w_arr[i].TEL2;
						stmt.parameters[10] = w_arr[i].TEL3;
						stmt.parameters[11] = w_arr[i].BNKCD1;
						stmt.parameters[12] = w_arr[i].BNKSCD1;
						stmt.parameters[13] = w_arr[i].VBNO1;
						stmt.parameters[14] = w_arr[i].BNKMEMO1;
						stmt.parameters[15] = w_arr[i].BNKCD2;
						stmt.parameters[16] = w_arr[i].BNKSCD2;
						stmt.parameters[17] = w_arr[i].VBNO2;
						stmt.parameters[18] = w_arr[i].BNKMEMO2;
						stmt.parameters[19] = w_arr[i].BNKCD3;
						stmt.parameters[20] = w_arr[i].BNKSCD3;
						stmt.parameters[21] = w_arr[i].VBNO3;
						stmt.parameters[22] = w_arr[i].BNKMEMO3;
						stmt.parameters[23] = w_arr[i].ADD1_2;
						stmt.parameters[24] = w_arr[i].ADD2_2;
						stmt.parameters[25] = w_arr[i].ADD3_2;
						
						stmt.execute();
					}
					
					// コミット
					stmt.sqlConnection.commit();
					
					// 次関数を実行
					//BasedMethod.execNextFunction(nextfunc);
					singleton.sitemethod.dspJoinErrMsg("N0001", nextfunc);
				},
				
				// 失敗時の処理
				srv.TokenFaultEvent
			));
		}
		
		protected function stmtErrorHandler(event:SQLErrorEvent):void
		{
			// エラーの表示
			trace("SQLerr");
		}
		
/*
		public function updateUB910X(): void
		{
			stmt = new SQLStatement();
			//stmt.sqlConnection =  connection;
			stmt.sqlConnection =  singleton.sqlconnection;
			//stmt.itemClass = LS001;
			
			// 修正時はUPDATE
			if (isNew == false)
			{ 
				stmt.text= singleton.g_query_xml.entry.(@key == "updUB910X");
				
				stmt.parameters[0] = view.ls001.HTID;
				//stmt.parameters[2] = view.ls001.CHGCD;
				stmt.parameters[1] = view.lcmbCHGNM.selectedItem.CHGCD;
				//stmt.parameters[2] = view.ls001.SDDAY;
				stmt.parameters[2] = view.edtSDDAY.text;
				stmt.parameters[3] = view.ls001.HVNO;
				stmt.parameters[4] = view.ls001.BVNO;
				stmt.parameters[5] = view.ls001.NVNO;
			}
			// 新規時はINSERT
			else
			{
				stmt.text= singleton.g_query_xml.entry.(@key == "insUB910X");
				
				stmt.parameters[0] = "0";
				stmt.parameters[1] = view.ls001.HTID;
				//stmt.parameters[2] = view.ls001.CHGCD;
				stmt.parameters[2] = view.lcmbCHGNM.selectedItem.CHGCD;
				//stmt.parameters[3] = view.ls001.SDDAY;
				stmt.parameters[3] = view.edtSDDAY.text;
				stmt.parameters[4] = view.ls001.HVNO;
				stmt.parameters[5] = view.ls001.BVNO;
				stmt.parameters[6] = view.ls001.NVNO;
			}
			
			//イベント登録
			stmt.addEventListener(SQLEvent.RESULT, updateUB910XResult);
			stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler);
			
			//実行
			stmt.execute();
		}
*/		
		private function updateUB910XResult(event:SQLEvent):void 
		{
			super.updateDataSet_H();
		}
	}
}