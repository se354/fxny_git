package views
{
	import flash.data.SQLResult;
	import flash.data.SQLStatement;
	import flash.events.Event;
	import flash.events.GeolocationEvent;
	import flash.events.KeyboardEvent;
	import flash.events.LocationChangeEvent;
	import flash.events.SQLErrorEvent;
	import flash.events.SQLEvent;
	import flash.geom.Rectangle;
	import flash.media.StageWebView;
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	import flash.sensors.Geolocation;
	import flash.ui.Keyboard;
	
	import modules.base.BasedMethod;
	import modules.sbase.SiteModule;
	
	import mx.collections.ArrayCollection;
	import mx.managers.PopUpManager;
	
	public class POPUP005Controller extends SiteModule
	{
		private var view:POPUP005;
		public var arrPOPUP005: ArrayCollection;
		
		//public var argVJAN: String;				// 商品コード
		
		
		public var okflg: Boolean;
		
		public function POPUP005Controller()
		{
			super();
		}
		
		override public function init():void
		{
			// フォーム宣言
			view = _view as POPUP005;

			view.grp1.dataProvider = arrPOPUP005;
			
			okflg = false;
			
			// 変数の設定
			//g_prgid     = "MENUX";
			super.init();
		}
		/*
		override public function openDataSet_B():void
		{
			cdsPOPUP005()
		}

		override public function setDispObject():void
		{
			super.setDispObject();
		}
		*/

		protected function stmtErrorHandler(event:SQLErrorEvent):void
		{
			// TODO Auto-generated method stub
			// エラーの表示
			trace("SQLerr");
			doDSPProc_last();
		}
		
		
		/**********************************************************
		 * Backキー押下時処理.
		 * @author 12.08.22 SE-354
		 **********************************************************/
		protected function onKeyDownHandler(e:KeyboardEvent):void
		{
			e.preventDefault();
			
			btnOKClick();
		}

		/**********************************************************
		 * btnOK クリック時処理.
		 * @author 12.08.22 SE-354
		 **********************************************************/
		public function btnOKClick(): void
		{
			okflg = true;
			
			PopUpManager.removePopUp(view);
		}
		
		/**********************************************************
		 * btnキャンセル クリック時処理.
		 * @author 12.08.22 SE-354
		 **********************************************************/
		public function btnCANCELClick(): void
		{
			okflg = false;
			
			PopUpManager.removePopUp(view);
		}
	}
}