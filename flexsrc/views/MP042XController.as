package views
{
	import flash.data.SQLResult;
	import flash.data.SQLStatement;
	import flash.events.Event;
	import flash.events.GeolocationEvent;
	import flash.events.KeyboardEvent;
	import flash.events.LocationChangeEvent;
	import flash.events.SQLErrorEvent;
	import flash.events.SQLEvent;
	import flash.geom.Rectangle;
	import flash.media.StageWebView;
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	import flash.sensors.Geolocation;
	import flash.ui.Keyboard;
	
	import modules.base.BasedMethod;
	import modules.custom.CustomIterator;
	import modules.custom.sub.Iterator;
	import modules.datagrid.DataGridColumnRenderer;
	import modules.dto.LF012;
	import modules.dto.LM009;
	import modules.formatters.CustomNumberFormatter;
	import modules.sbase.SiteModule;
	
	import mx.collections.ArrayCollection;
	import mx.core.ClassFactory;
	import mx.managers.PopUpManager;
	
	/**********************************************************
	 * MP042X 配置薬履歴ポップアップ
	 * @author 12.09.06 SE-362
	 **********************************************************/
	public class MP042XController extends SiteModule
	{
		private var view:MP042X;
		private var arrMP042XH: ArrayCollection;
		private var arrMP042X: ArrayCollection;
		private var cdsMP042X_arg: Object;
		
		public var argTKCD: String;
		
		public function MP042XController()
		{
			super();
		}
		
		/**********************************************************
		 * 初期処理
		 * @author  12.09.06 SE-362
		 **********************************************************/
		override public function init():void
		{
			// フォーム宣言
			view = _view as MP042X;

			super.init();
		}
		
		override public function formShow_last():void
		{
			super.formShow_last();
			
			doDSPProc();
		}
		
		override public function openDataSet_B():void
		{
			cdsMP042XH(openDataSet_B_cld1);
		}

		private function openDataSet_B_cld1(): void
		{
			cdsMP042X(super.openDataSet_B);
		}
		
		override public function setDispObject():void
		{
			super.setDispObject();
			
			if(arrMP042X != null && arrMP042X.length != 0){
				view.grdW01.dataProvider = arrMP042X;

				if(arrMP042XH != null && arrMP042XH.length != 0){
					// 配置薬履歴ヘッダ設定
					view.lblHBAYI01.headerText = BasedMethod.nvl(arrMP042XH[0].HBAYI01);
					view.lblHBAYI02.headerText = BasedMethod.nvl(arrMP042XH[0].HBAYI02);
					view.lblHBAYI03.headerText = BasedMethod.nvl(arrMP042XH[0].HBAYI03);
					view.lblHBAYI04.headerText = BasedMethod.nvl(arrMP042XH[0].HBAYI04);
					view.lblHBAYI05.headerText = BasedMethod.nvl(arrMP042XH[0].HBAYI05);
					view.lblHBAYI06.headerText = BasedMethod.nvl(arrMP042XH[0].HBAYI06);
					view.lblHBAYI07.headerText = BasedMethod.nvl(arrMP042XH[0].HBAYI07);
					view.lblHBAYI08.headerText = BasedMethod.nvl(arrMP042XH[0].HBAYI08);
					view.lblHBAYI09.headerText = BasedMethod.nvl(arrMP042XH[0].HBAYI09);
					view.lblHBAYI10.headerText = BasedMethod.nvl(arrMP042XH[0].HBAYI10);
					view.lblHBAYI11.headerText = BasedMethod.nvl(arrMP042XH[0].HBAYI11);
					view.lblHBAYI12.headerText = BasedMethod.nvl(arrMP042XH[0].HBAYI12);
					view.lblHBAYI13.headerText = BasedMethod.nvl(arrMP042XH[0].HBAYI13);
					view.lblHBAYI14.headerText = BasedMethod.nvl(arrMP042XH[0].HBAYI14);
					view.lblHBAYI15.headerText = BasedMethod.nvl(arrMP042XH[0].HBAYI15);
					view.lblHBAYI16.headerText = BasedMethod.nvl(arrMP042XH[0].HBAYI16);
					view.lblHBAYI17.headerText = BasedMethod.nvl(arrMP042XH[0].HBAYI17);
					view.lblHBAYI18.headerText = BasedMethod.nvl(arrMP042XH[0].HBAYI18);
					view.lblHBAYI19.headerText = BasedMethod.nvl(arrMP042XH[0].HBAYI19);
					view.lblHBAYI20.headerText = BasedMethod.nvl(arrMP042XH[0].HBAYI20);
					view.lblHBAYI21.headerText = BasedMethod.nvl(arrMP042XH[0].HBAYI21);
					view.lblHBAYI22.headerText = BasedMethod.nvl(arrMP042XH[0].HBAYI22);
					view.lblHBAYI23.headerText = BasedMethod.nvl(arrMP042XH[0].HBAYI23);
					view.lblHBAYI24.headerText = BasedMethod.nvl(arrMP042XH[0].HBAYI24);
					view.lblHBAYI25.headerText = BasedMethod.nvl(arrMP042XH[0].HBAYI25);
					view.lblHBAYI26.headerText = BasedMethod.nvl(arrMP042XH[0].HBAYI26);
					view.lblHBAYI27.headerText = BasedMethod.nvl(arrMP042XH[0].HBAYI27);
					view.lblHBAYI28.headerText = BasedMethod.nvl(arrMP042XH[0].HBAYI28);
					view.lblHBAYI29.headerText = BasedMethod.nvl(arrMP042XH[0].HBAYI29);
					view.lblHBAYI30.headerText = BasedMethod.nvl(arrMP042XH[0].HBAYI30);
				}
				
//				for(var i:int=0; i<view.grdW01.columnsLength; i++){
//					if(view.grdW01.columns.getItemAt(i).dataField.substr(0, 5) == "HBAYI"){
//						view.grdW01.columns.getItemAt(i).itemRenderer = new ClassFactory(DataGridColumnRenderer);
//					}
//				}
			}else{
				singleton.sitemethod.dspJoinErrMsg("E0020");
			}
		}

		private function cdsMP042XH(func: Function): void
		{
			// 引数情報
			cdsMP042X_arg = new Object;
			cdsMP042X_arg.func = func;
			
			Stmt = new SQLStatement();
			Stmt.sqlConnection =  singleton.sqlconnection;
			Stmt.parameters[0] = argTKCD;
			Stmt.itemClass = LF012;
			Stmt.text= singleton.g_query_xml.entry.(@key == "qryMF010X3H");
			
			
			//イベント登録
			Stmt.addEventListener(SQLEvent.RESULT, cdsMP042XHResult, false, 0, true);
			Stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler, false, 0, true);
			
			//実行
			Stmt.execute();
		}
		
		public function cdsMP042XHResult(event: SQLEvent): void
		{
			var w_arg: Object = cdsMP042X_arg;
			var result:SQLResult = Stmt.getResult();
			
			arrMP042XH = new ArrayCollection(result.data);
			
			// 次関数実行
			if(w_arg.func)
				w_arg.func();
		}
		
		private function cdsMP042X(func: Function): void
		{
			// 引数情報
			cdsMP042X_arg = new Object;
			cdsMP042X_arg.func = func;
			
			Stmt = new SQLStatement();
			Stmt.sqlConnection =  singleton.sqlconnection;
			Stmt.parameters[0] = argTKCD;
			Stmt.itemClass = LF012;
			Stmt.text= singleton.g_query_xml.entry.(@key == "qryMF010X3");
			
			
			//イベント登録
			Stmt.addEventListener(SQLEvent.RESULT, cdsMP042XResult, false, 0, true);
			Stmt.addEventListener(SQLErrorEvent.ERROR, stmtErrorHandler, false, 0, true);
			
			//実行
			Stmt.execute();
		}
		
		private function stmtErrorHandler(event: SQLErrorEvent): void
		{
			// エラーの表示
			trace(event.error.message);
		}

		public function cdsMP042XResult(event: SQLEvent): void
		{
			var w_arg: Object = cdsMP042X_arg;
			var result:SQLResult = Stmt.getResult();
			
			arrMP042X = new ArrayCollection(result.data);
			
			// 次関数実行
			if(w_arg.func)
				w_arg.func();
		}
		
		/**********************************************************
		 * 閉じる
		 * @author  12.09.06 SE-362
		 **********************************************************/
		public function btnCLOSEClick():void
		{
			// 画面クローズ
			PopUpManager.removePopUp(view);
		}
	}
}